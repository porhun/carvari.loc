<? include "elements/header.php" ?>
<div class="main_content checkout-page">
    <h1>Редагувати дані</h1>
    <div class="padded_block">
        <div class="tab_wrapper">
            <div class="tab_content">
                <div class="input_block">
                    <label>Ім'я</label>
                    <input type="text">
                </div>
                <div class="input_block wrong">
                    <label>Прізвище</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="input_block">
                    <label>E-mail</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="input_block">
                    <label>Пароль</label>
                    <input type="password">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="input_block">
                    <label>Телефон</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <h2>
                    Доставка
                </h2>
                <div class="input_block">
                    <label>Місто</label>
                    <div class="select_wrap">
                        <select>
                            <option>Київська область</option>
                            <option>Дніпропетровська область</option>
                            <option>Харківська область</option>
                            <option>Житомирська область</option>
                        </select>
                    </div>
                    <i class="icon">
                        <svg>
                            <use xlink:href="#dropdown_arrow">
                        </svg>
                    </i>
                </div>
                <div class="input_block">
                    <label>Місто</label>
                    <div class="select_wrap">
                        <select>
                            <option>Київ</option>
                            <option>Дніпропетровськ</option>
                            <option>Харків</option>
                            <option>Житомир</option>
                        </select>
                    </div>
                    <i class="icon">
                        <svg>
                            <use xlink:href="#dropdown_arrow">
                        </svg>
                    </i>
                </div>
                <div class="input_block">
                    <label>Вулиця</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="input_block">
                    <label>Будинок</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="input_block">
                    <label>Квартира</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="btn_holder">
                    <a href="#" class="button big">Зберегти</a>
                </div>
            </div>
        </div>
    </div>
</div>
<? include "elements/footer.php" ?>
