<? include "elements/header.php" ?>
    <div class="main_content body trends-page">
        <div class="padded_block">
            <h1>Карта Carvari</h1>
        </div>
        <div class="carvari_card">
            <div class="card_number">5823&nbsp;&nbsp;&nbsp;4905&nbsp;&nbsp;&nbsp;6007&nbsp;&nbsp;&nbsp;1873</div>
            <div class="holder_name">Константин Константинопольский</div>
            <div class="holder_phone">8(097) 653-03-72</div>
        </div>
        <div class="slider_container slider">
            <div class="one_entry toggled in">
                <a href="#">
                    <img src="../img/sample/cover3.jpg" alt="" class="full-screen-img cover">
                    <h2>Знижки на літнє взуття</h2>
                </a>
                <div class="padded_block">
                    <div class="post is_post">
                        <p>Тільки з 3 по 8 червня у вас є чудова можливість зробити покупки за ще більш вигідною ціною!</p>
                        <p>
                            На чоловічі та жіночі кеди, сліпони, туфлі, напівчеревики, черевики, кросівки, балетки, мокасини,
                            які мають текстильний верх або перфорацію, а також на сумки, барсетки та клатчі діє знижка 35%.
                        </p>
                    </div>

                    <div class="attrs mb20">
                        29 липня 2015
                    </div>
                </div>
            </div>
            <div class="one_entry toggled in">
                <a href="#">
                    <img src="../img/sample/cover3.jpg" alt="" class="full-screen-img cover">
                    <h2>Знижки на літнє взуття</h2>
                </a>
                <div class="padded_block">
                    <div class="post is_post">
                        <p>Тільки з 3 по 8 червня у вас є чудова можливість зробити покупки за ще більш вигідною ціною!</p>
                        <p>
                            На чоловічі та жіночі кеди, сліпони, туфлі, напівчеревики, черевики, кросівки, балетки, мокасини,
                            які мають текстильний верх або перфорацію, а також на сумки, барсетки та клатчі діє знижка 35%.
                        </p>
                    </div>

                    <div class="attrs mb20">
                        29 липня 2015
                    </div>
                </div>
            </div>
            <div class="one_entry toggled in">
                <a href="#">
                    <img src="../img/sample/cover3.jpg" alt="" class="full-screen-img cover">
                    <h2>Знижки на літнє взуття</h2>
                </a>
                <div class="padded_block">
                    <div class="post is_post">
                        <p>Тільки з 3 по 8 червня у вас є чудова можливість зробити покупки за ще більш вигідною ціною!</p>
                        <p>
                            На чоловічі та жіночі кеди, сліпони, туфлі, напівчеревики, черевики, кросівки, балетки, мокасини,
                            які мають текстильний верх або перфорацію, а також на сумки, барсетки та клатчі діє знижка 35%.
                        </p>
                    </div>

                    <div class="attrs mb20">
                        29 липня 2015
                    </div>
                </div>
            </div>
            <div class="one_entry toggled in">
                <span href="#">
                    <img src="../img/sample/cover3.jpg" alt="" class="full-screen-img cover">
                    <h2>Знижки на літнє взуття</h2>
                </span>
                <div class="padded_block">
                    <div class="post is_post">
                        <p>Тільки з 3 по 8 червня у вас є чудова можливість зробити покупки за ще більш вигідною ціною!</p>
                        <p>
                            На чоловічі та жіночі кеди, сліпони, туфлі, напівчеревики, черевики, кросівки, балетки, мокасини,
                            які мають текстильний верх або перфорацію, а також на сумки, барсетки та клатчі діє знижка 35%.
                        </p>
                    </div>

                    <div class="attrs mb20">
                        29 липня 2015
                    </div>
                </div>
            </div>
        </div>
    </div>
<? include "elements/footer.php" ?>