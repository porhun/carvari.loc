<? include "elements/header.php" ?>
    <div class="main_content body map-page">
        <div class="padded_block">
            <h1>Магазини в мiстах України</h1>
        </div>

        <div class="padded_block">
            <div class="city_chooser">
                <div class="input_block">
                    <div class="select_wrap">
                        <select>
                            <option>Днепропетровск</option>
                            <option>Львов</option>
                            <option>Одесса</option>
                        </select>
                    </div>
                    <i class="icon">
                        <svg>
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dropdown_arrow">
                            </use></svg>
                    </i>
                </div>
            </div>

            <div class="slider">
                <div class="slide store_block" data-lat="48.45" data-long="35.04">
                    <div class="title">ТРК «МОСТ –Сіті центр»</div>
                    <div class="info" data-lat="55.760374" data-lng="37.62175">
                        вул. Глінки, 2<br/>
                        +38 (056) 790-29-25<br/>
                        з10:00 до 22:00<br/>
                    </div>
                </div>

                <div class="slide store_block" data-lat="48.55" data-long="35.14">
                    <div class="title">ТЦ «Passage»</div>
                    <div class="info" data-lat="54.760374" data-lng="36.62175">
                        вул. К. Маркса, 50<br/>
                        +3 (8056) 376-08-83<br/>
                        з 10:00 до 22:00<br/>
                    </div>
                </div>

                <div class="slide store_block" data-lat="48.65" data-long="35.24">
                    <div class="title">ТЦ Променада</div>
                    <div class="info" data-lat="53.760374" data-lng="35.62175">
                        вул. Овруцька, 18<br/>
                        +38 (067) 63-46-252<br/>
                        з 10:00 до 21:00<br/>
                    </div>
                </div>

                <div class="slide store_block" data-lat="48.55" data-long="35.14">
                    <div class="title">ТРЦ «Dream Town»</div>
                    <div class="info">
                        пр. Оболонский 1б<br/>
                        +38 (044) 426-86-72<br/>
                        з 10:00 до 22:00<br/>
                    </div>
                </div>

                <div class="slide store_block" data-lat="48.45" data-long="35.04">
                    <div class="title">ТРЦ Sky Mall</div>
                    <div class="info">
                        пр. Генерала Ватутина, 2-т<br/>
                        +38 (044) 371-19-44<br/>
                        з 10:00 до 22:00<br/>
                    </div>
                </div>

                <div class="slide store_block" data-lat="48.45" data-long="35.04">
                    <div class="title">Магазин Luciano Carvari</div>
                    <div class="info">
                        пр. Леніна, 149<br/>
                        +38 (061) 213-56-35<br/>
                        з 10:00 до 20:00<br/>
                    </div>
                </div>
            </div>

            <div class="map_holder">
                <div id="is_map">

                </div>
            </div>

            <script src="https://maps.googleapis.com/maps/api/js"></script>
            <!-- Скрипт карты гружу с десктопа, потому что сейчас предполагается идентичный функционал по общей механике-->
            <script src="/web/js/map.js"></script>

        </div>
    </div>
<? include "elements/footer.php" ?>