<? include "elements/header.php" ?>
    <div class="main_content body product-page">

        <div class="padded_block go_back">
            <a href="/web_mobile/html/02_list.php">
                <i class="icon">
                    <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#go_back_icon">
                        </use></svg>
                </i>
            </a>
        </div>

        <div class="product_image">
            <div class="slider">
                <div class="slide">
                    <img src="../img/sample/product_tapok.png">
                </div>
                <div class="slide">
                    <img src="../img/sample/product_tapok1.png">
                </div>
                <div class="slide">
                    <img src="../img/sample/product_tapok.png">
                </div>
                <div class="slide">
                    <img src="../img/sample/product_tapok1.png">
                </div>
            </div>
        </div>


        <div class="padded_block product_card">
<!--            <div class="title">-->
<!--                Босонiжки-->
<!--            </div>-->
            <h2>Босонiжки</h2>

            <div class="prices">
                <div class="inline-block cur_price">
                    2 385 грн
                </div>
                <div class="inline-block old_price">
                    1 670 грн
                </div>
            </div>
            <div class="attrs">
                <div class="attr-block">WH1506A99KA1026T1</div>
                <div class="attr-block">Штучна шкiра</div>
            </div>
            <div class="sizes">
                <div class="attr-block">Розмiр</div>
                <div class="sizes_holder clearfix">
                    <span><a href="#">35</a></span>
                    <span><a href="#">35,5</a></span>
                    <span><a href="#">36</a></span>
                    <span><a href="#" class="active">37</a></span>
                    <span><a href="#">38</a></span>
                    <span><a href="#">38,5</a></span>
                    <span><a href="#">39</a></span>
                    <span><a href="#">39,5</a></span>
                    <span><a href="#">40</a></span>
                    <span><a href="#">41</a></span>
                </div>
            </div>
            <div class="action">
                <a href="#" class="button order_btn">Замовити</a>
            </div>
        </div>
    </div>
<? include "elements/footer.php" ?>