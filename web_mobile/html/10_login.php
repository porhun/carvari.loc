<? include "elements/header.php" ?>
<div class="main_content checkout-page">
    <h1>Увійти в аккаунт</h1>
    <div class="padded_block">
        <div class="tab_wrapper">
            <div class="tab_content">
                <div class="input_block">
                    <label>Телефон</label>
                    <input type="text">
                </div>
                <div class="input_block">
                    <label>Пароль</label>
                    <input type="password">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="btn_holder login">
<!--                    <input type="button" class="button big" value="Увійти">-->
                    <a href="#" class="button big">Увійти</a>
                    <a class="remind_pass" href="#">Нагадати пароль</a>
                </div>
            </div>
        </div>
    </div>
</div>
<? include "elements/footer.php" ?>
