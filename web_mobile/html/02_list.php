<? include "elements/header.php" ?>
<div class="main_content list-page">
    <div class="filter_wrapper">
        <div class="filter">
            Фільтр
            <i class="icon">
                <svg>
                    <use xlink:href="#dropdown_arrow">
                </svg>
            </i>
        </div>
    </div>
    <h1>Босоніжки</h1>
    <a class="big_link not_so_big list" href="#">
        <div class="big_img_link">
            <img src="../img/temp/list_img.png" alt="">
        </div>
    </a>
    <div class="filter_items padded_block">
        <ul class="js_filter_list">
            <li>
                <a href="#">36,5</a>
            </li>
            <li>
                <a href="#">Балетки</a>
            </li>
            <li>
                <a href="#">Чорний</a>
            </li>
            <li>
                <a href="#">ШкУра</a>
            </li>
            <li>
                <a href="#">Еще чето</a>
            </li>
            <li>
                <a href="#">Какой то длинный фильтр</a>
            </li>
            <li>
                <a href="#">Чорний</a>
            </li>
            <li>
                <a href="#">Еще одна шкУра</a>
            </li>
        </ul>
    </div>
    <div class="item_list padded_block">
        <ul>
            <li>
                <a href="#">
                    <img src="../img/sample/product_tapok1.png" alt="">
                    <div class="descr">
                        <span class="title">Босоніжки</span>
                        <span class="new_price">1 999 грн</span>
                        <span class="old_price">2 000 грн</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="../img/sample/product_tapok1.png" alt="">
                    <div class="descr">
                        <span class="title">Босоніжки</span>
                        <span class="new_price">1 999 грн</span>
                        <span class="old_price">2 000 грн</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="../img/sample/product_tapok1.png" alt="">
                    <div class="descr">
                        <span class="title">Босоніжки</span>
                        <span class="new_price">1 999 грн</span>
                        <span class="old_price">2 000 грн</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="#">
                    <img src="../img/sample/product_tapok1.png" alt="">
                    <div class="descr">
                        <span class="title">Босоніжки</span>
                        <span class="new_price">1 999 грн</span>
                        <span class="old_price">2 000 грн</span>
                    </div>
                </a>
            </li>
        </ul>
        <a class="button order_btn" href="#">Показати ще</a>
    </div>
    <div class="filter_list_wrapper">
        <div class="filter_list" style="display: none;">
            <div class="filter_title">Відобразити</div>
            <ul class="dd_content">
                <li><a href="#">Новинки</a></li>
                <li><a href="#">Від дешевих до дорогих</a></li>
                <li><a href="#">Від дорогих до дешевих</a></li>
            </ul>
            <hr>
            <div class="filter_title sizes">Розмір</div>
            <ul class="sizes clearfix">
                <li><a href="#">35</a></li>
                <li><a href="#" class="active">36</a></li>
                <li><a href="#">37</a></li>
                <li><a href="#">38</a></li>
                <li><a href="#">39</a></li>
                <li><a href="#">39</a></li>
                <li><a href="#">39</a></li>
            </ul>
            <hr>
            <div class="filter_title">Вид</div>
            <ul class="dd_content">
                <li><a href="#">Балетки</a></li>
                <li><a href="#">Босоніжки</a></li>
                <li><a href="#">Ботильйони</a></li>
                <li><a href="#">Черевики</a></li>
                <li><a href="#">Кросівки</a></li>
                <li><a href="#">Мокасини</a></li>
                <li><a href="#">Сабо</a></li>
                <li><a href="#">Балетки</a></li>
                <li><a href="#">Босоніжки</a></li>
                <li><a href="#">Ботильйони</a></li>
                <li><a href="#">Черевики</a></li>
                <li><a href="#">Кросівки</a></li>
                <li><a href="#">Збагойные</a></li>
                <li><a href="#">Сабоги</a></li>
            </ul>
            <hr>
            <div class="filter_title">Что то еще</div>
            <ul class="dd_content">
                <li><a href="#">Пункт</a></li>
                <li><a href="#">Не пункт</a></li>
                <li><a href="#">Может быть пункт</a></li>
                <li><a href="#">Может быть пункт</a></li>
                <li><a href="#">Может быть пункт</a></li>
            </ul>
        </div>
        <div class="mobile_height_spike"></div>
    </div>
    <div class="filter_btns_wrapper">
        <a class="button" href="#">Показати</a>
        <a class="clear_link" href="#">Очистити</a>
    </div>
</div>
<? include "elements/footer.php" ?>
