$(document).ready(function(){

    $('select').on('focus',function() {
        var curScroll = $(window).scrollTop();
        $('html, body').animate({scrollTop:curScroll},1);
        $('.body_wrapper').on('touchstart',function(){
            $('select').blur();
        });
    });
    $('input').on('focus',function() {
        if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
            $('.header').addClass('none')
        }
            $('.body_wrapper').on('touchstart',function(){
            $('input').blur();
            $('.header').removeClass('none');
        });
    });

    var cur_scroll_top;

    $('.js_menu_item').click(function(){
        if($(this).hasClass('active')){
            $('.body_wrapper').css({'overflow': 'initial', 'height': 'auto'});
            $(window).scrollTop(cur_scroll_top);
            $('.dropdown_menu_wrapper').css('height', '0px');
            $(this).removeClass('active');
        }
        else{
            $('.dd_content').html($(this).find('ul').html());

            cur_scroll_top = $(window).scrollTop();
            $(window).scrollTop(0);
            var menu_height = $('.dropdown_menu > ul > li').length*45 + 175;
            if( menu_height > $('.mobile_height_spike').height() ){
                var max_height = menu_height;
            }
            else{
                var max_height = $('.mobile_height_spike').height();
            }
            $('.body_wrapper').css({'overflow': 'hidden', 'height': max_height+'px'});
            $('.dropdown_menu').css({'height': '100%','display':'block'});
            $('.dropdown_menu_wrapper').css('height', '100%');
            $('.js_menu_item.active').removeClass('active');
            $(this).addClass('active');
        }
    });

    $('.filter_wrapper').click(function(){
        if($(this).hasClass('active')){
            $('.body_wrapper').css({'overflow': 'initial', 'height': 'auto'});

            $(window).scrollTop(cur_scroll_top);

            $('.filter_list_wrapper').css('height', '50px');
            $(this).removeClass('active');
            $('.header').css('display','block');
            $('.filter_wrapper').removeAttr('style');
            if ($(window).width()>480) {
                $('.filter_wrapper').css('top','45px');
            } else {
                $('.filter_wrapper').css('top','67px');
            }
            $('.filter_btns_wrapper').css('display','none');
            $('.filter').removeClass('active')
        }
        else{
            cur_scroll_top = $(window).scrollTop();
            $('.header').css('display','none');
            //$('.filter_wrapper').css('top','0px');
            $('.filter_wrapper').attr('style', 'top:0px!important');
            $('.filter_btns_wrapper').css('display','block');

            $(window).scrollTop(0);
            $('.filter').addClass('active');

            //var menu_height = $('.filter_list > li').length*90 + 350;
            var menu_height = ($('.filter_list li').length*42) + ($('.filter_title').length*30);

            if( menu_height > $('.mobile_height_spike').height() ){
                var max_height = menu_height;
            }
            else{
                var max_height = $('.mobile_height_spike').height();
            }

            $('.body_wrapper').css({'overflow': 'hidden', 'height': max_height+'px'});

            $('.filter_list').css({'height': '100%','display':'block'});
            $('.filter_list_wrapper').css('height', '100%');

            $(this).addClass('active');
        }
    });

    if ($('.main_content.checkout-page').length) {
        setTimeout(function() {
            $('input:radio').styler();
        }, 100);
    }

    $('.js_discount_input').on('keyup change',function(){
        if ($(this).val() != "") {
            $('.discount_button').css('display','block')
        } else {
            $('.discount_button').css('display','none')
        }
    });


    $('.profile_nav_link').on('click',function(){
        if (!$(this).hasClass('active')) {
            $('.profile_nav_link.active').removeClass('active');
            $(this).addClass('active');
            $('.tab_content').fadeOut(200);
            var tab = $(this).attr('data-tab');
            setTimeout(function(){
                $('#'+tab).fadeIn(200);
            },200)
        }
    });

    if($('.filter_wrapper').length) {
        $(window).resize(function(){
            if ($('.header').height()>45) {
                $('.filter_wrapper').css('top','67px');
            } else {
                $('.filter_wrapper').css('top','45px');
            }
            if ($('.header').css('display')=='none') {
                $('.filter_wrapper').attr('style', 'top:0px!important');
            }
        })
    }




    $(window).scroll(function(){
       if ( $('body').scrollTop() > 300){
            $('.slide_to_top').removeClass('hidden');
       }else{
           $('.slide_to_top').addClass('hidden');
       }
    });

    $('.slide_to_top').on('click', function(){
        $('HTML, BODY').animate({scrollTop: 0}, 500);
    });

    if( $('.body').hasClass('product-page') ){
        $('.product_image .slider').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });

        //if( $('.sizes_holder span').length > 7 ){
        //    $('.sizes_holder').slick({
        //        slidesToShow: 5,
        //        infinite: false,
        //        arrows: false,
        //        slidesToScroll: 1,
        //        swipeToSlide: true,
        //        dots: true,
        //        variableWidth: true,
        //        slide: 'span'
        //    });
        //}

    }

    if( $('.body').hasClass('trends-page') ){
        $('.slider_container').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });
    }

    if(  $('.body').hasClass('map-page')   ){
        var slider_timer = false;

        $('.slider').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });

        $('.slider').on('afterChange', function(slick, slider){
            clearTimeout(slider_timer);
            var $curSlide = $('.slider .store_block[data-slick-index="'+slider.currentSlide+'"]'),
                lat = $curSlide.attr('data-lat'),
                long = $curSlide.attr('data-long');

            console.log($curSlide);
            console.log(lat +', '+long);

            slider_timer = setTimeout(function(){
                console.log('Здесь будет рилод карты с задержкой, чтобы не плодить вызовы');
                load_map(lat, long);
            }, 1000)
        });


    }
});