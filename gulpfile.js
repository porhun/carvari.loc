'use strict';

var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var less = require('gulp-less');
var notify = require('gulp-notify');
var cleanCSS = require('gulp-clean-css');
var util = require('gulp-util');

var config = {
    dev: !util.env.dev,
    css: {
        src: './themes/carvari-theme/less/',
        dest: './themes/carvari-theme/css/'
    },
    css_mob: {
        src: './themes/carvari-theme/mobile/less/',
        dest: './themes/carvari-theme/mobile/css/'
    }
};


gulp.task('less', function () {
  return gulp.src(config.css.src + 'main.less')
    .pipe(config.dev ? util.noop() : sourcemaps.init({loadMaps: true}))
    .pipe(less())
    .on('error', notify.onError())
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'Safari >= 5', 'iOS >= 4', 'ie >= 9'],
      cascade: false
    }))
    .pipe(cleanCSS({
        advanced: false,
        processImport: false,
        keepSpecialComments: 0
    }))
    .pipe(config.dev ? util.noop() : sourcemaps.write())
    .pipe(gulp.dest(config.css.dest));
});

gulp.task('less:mob', function () {
    return gulp.src(config.css_mob.src + 'main.less')
    .pipe(config.dev ? util.noop() : sourcemaps.init({loadMaps: true}))
    .pipe(less())
    .on('error', notify.onError())
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(cleanCSS({
        advanced: false,
        processImport: false,
        keepSpecialComments: 0
    }))
    .pipe(config.dev ? util.noop() : sourcemaps.write())
    .pipe(gulp.dest(config.css_mob.dest));
});

gulp.task('watch', function () {
  gulp.watch([config.css.src + '**/*.less'], ['less']);
  gulp.watch([config.css_mob.src + '**/*.less'], ['less:mob']);
});

gulp.task('default', ['less', 'watch']);
