
{include file="$tpl_dir./errors.tpl"}

<div class="main_content list">
	{if isset($category)}
		{if $category->id AND $category->active}

			<div class="filters">
				<div class="filters_wrapper">
					<div class="min_width">
						<div class="filter_list clearfix">
							<div class="filters_exp">
								<div class="title">{l s='Фільтр:'}</div>
								<div class="filter_block" id="filters_dynamic">
										<div class="empty_filter dynamic">{l s='Нічого не вибрано'}</div>

										{*<div class="filter_name">*}
											{*<a href="" class="filt_item">asas</a>*}
										{*</div>*}
										{*<div class="filter_name dynamic">*}
											{*<span class="filt_item">Босоніжки</span>*}
										{*</div>*}
										{*<div class="filter_name dynamic">*}
											{*<span class="filt_item">Шкіра</span>*}
										{*</div>*}
										{*<div class="filter_name dynamic">*}
											{*<span class="filt_item">Білий</span>*}
										{*</div>*}
									<div class="filter_name price_filt">
										<div class="filter_items" id="price-sort">
											<span class="filt_item price_sort" id="sort-type-name">
												{if $orderby eq 'position'}
                                                    {l s='Рекомендовані'}
												{elseif $orderby eq 'date_add'}
                                                    {l s='Новинки'}
												{elseif $orderby eq 'price' AND $orderway eq 'asc'}
                                                    {l s='Cпочатку дешевше'}
												{elseif $orderby eq 'price' AND $orderway eq 'desc'}
                                                    {l s='Спочатку дорожче'}
												{/if}
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="table_exp" style="display: none">
								<!--                        <div class="filter_exp_title clearfix">-->
								<!--                            <div class="filter_exp title ">Матеріал</div>-->
								<!--                            <div class="filter_exp title ">Колір</div>-->
								<!--                            <div class="filter_exp title ">Відобразити</div>-->
								<!--                        </div>-->
								<div class="filter_exp_cont">

									{hook h='displayLeftColumn' mod='blocklayered'}
									{include file="$tpl_dir./product-sort.tpl"}


									<div class="btn_holder">
										<span class="button small js_show_filter">{l s='Показати'}</span>
										<a href="#/" class="clean_filter">{l s='Очистити'}</a>
									</div>
								</div>
							</div>
						</div>

						<div class="expand_filter_btn">
							<i class="icon expand_filter">
								<svg pointer-events="all">
									<use xlink:href="#dropdown_arrow">
								</svg>
							</i>
						</div>
					</div>
					<div class="filters_wrapper__line"></div>
				</div>
			</div>

			<div class="top_content">
				<h1>{$category->h1tag|escape:'html':'UTF-8'}{if isset($categoryNameComplement)}&nbsp;{$categoryNameComplement|escape:'html':'UTF-8'}{/if}</h1>
				{include file="$tpl_dir./breadcrumb.tpl"}
			</div>

			{*{if isset($subcategories)}*}
				{*{if (isset($display_subcategories) && $display_subcategories eq 1) || !isset($display_subcategories) }*}
					{*<!-- Subcategories -->*}
					{*<div id="subcategories">*}
						{*<p class="subcategory-heading">{l s='Subcategories'}</p>*}
						{*<ul class="clearfix">*}
							{*{foreach from=$subcategories item=subcategory}*}
								{*<li>*}
									{*<div class="subcategory-image">*}
										{*<a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">*}
											{*{if $subcategory.id_image}*}
												{*<img class="replace-2x" src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'medium_default')|escape:'html':'UTF-8'}" alt="" width="{$mediumSize.width}" height="{$mediumSize.height}" />*}
											{*{else}*}
												{*<img class="replace-2x" src="{$img_cat_dir}{$lang_iso}-default-medium_default.jpg" alt="" width="{$mediumSize.width}" height="{$mediumSize.height}" />*}
											{*{/if}*}
										{*</a>*}
									{*</div>*}
									{*<h5><a class="subcategory-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'}</a></h5>*}
									{*{if $subcategory.description}*}
										{*<div class="cat_desc">{$subcategory.description}</div>*}
									{*{/if}*}
								{*</li>*}
							{*{/foreach}*}
						{*</ul>*}
					{*</div>*}
				{*{/if}*}
			{*{/if}*}

			{if $products}
				<div class="item_list">
					<div id="layered_ajax_loader"></div>
					{include file="./product-list.tpl" products=$products}
                    {hook h='showmoreproducts'}
				</div>
				<div class="content_sortPagiBar">
						{*{include file="./product-compare.tpl" paginationId='bottom'}*}
						{include file="./pagination.tpl" paginationId='bottom'}
				</div>
			{else}
				<div class="item_list">
					<div class="no_content">{l s='Товар у дорозi. Завiтайте трохи згодом.'}</div>
				</div>
			{/if}

			<div class="clearfix"></div>
			{if $scenes || $category->description}
			<div class="container">
				<div class="content_scene_cat">
					<div class="content_scene is_post">
						<!-- Scenes -->
						{include file="$tpl_dir./scenes.tpl" scenes=$scenes}
						{if $category->description}
							<div class="cat_desc rte">
								{if Tools::strlen($category->description) > 350}
									{* <div id="category_description_short">{$description_short}</div> 
									<div id="category_description_full" class="unvisible" style="display: none">{$category->description}</div> *}
									<div id="category_description_full" class="visible">{$category->description}</div>
									{* <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a> *}
								{else}
									<div>{$category->description}</div>
								{/if}


								{*{l s='More'}*}
								{*<div>{$category->description}</div>*}
							</div>
						{/if}

					</div>
				</div>
			</div>
			{/if}
			{hook h="displaySeoText" mod="ffcategoryseo"}
			{*{if $category->id_image}*}
				{*<div class="big_img">*}
					{*<div class="temp_img" style="background:url({$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}) 100% 50% / cover no-repeat;"></div>*}
				{*</div>*}
			{*{/if}*}

		{elseif $category->id}
			<p class="alert alert-warning">{l s='This category is currently unavailable.'}</p>
		{/if}
	{/if}

</div>


{include file="$tpl_dir./errors.tpl"}





