{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registred Trademark & Property of PrestaShop SA
*}
{*<pre>*}
	{*{$filters|var_dump}*}
{if $category->name != 'Чоловiкам' && $category->name != 'Жiнкам'}
	{assign var="hideCategory" value=true}
{/if}

{if $nbr_filterBlocks != 0}
	<div id="layered_block_left" class="block fleft">
		<form action="#" id="layered_form">
			{if $mobile_device}
				{foreach from=$filters item=filter name=filterForeach}
					{if $filter.name == 'Size' || $filter.name == 'Розмір' || $filter.name == 'Розмiр'}
						{$filter.is_size = 1}
					{/if}
					{if isset($filter.values)}
						<hr>
						<div class="filter_title {if $filter.is_size} sizes{/if}">{$filter.name|escape:'html':'UTF-8'}</div>
						<ul id="ul_layered_{$filter.type}_{$filter.id_key}" class="{if $filter.is_size == 1} sizes clearfix{else} dd_content{/if}">
							{if !isset($filter.slider)}
								{if $filter.filter_type == 0}
									{foreach from=$filter.values key=id_value item=value name=fe}
										{if $value.nbr || !$hide_0_values}
											<li>
												{if isset($filter.is_color_group) && $filter.is_color_group}
													{*<input class="color-option {if isset($value.checked) && $value.checked}on{/if} {if !$value.nbr}disable{/if}" type="button" name="layered_{$filter.type_lite}_{$id_value}" data-rel="{$id_value}_{$filter.id_key}" id="layered_id_attribute_group_{$id_value}" {if !$value.nbr}disabled="disabled"{/if} style="background: {if isset($value.color)}{if file_exists($smarty.const._PS_ROOT_DIR_|cat:"/img/co/$id_value.jpg")}url(img/co/{$id_value}.jpg){else}{$value.color}{/if}{else}#CCC{/if};" />*}
													{if isset($value.checked) && $value.checked}<input type="hidden" name="layered_{$filter.type_lite}_{$id_value}" value="{$id_value}" />{/if}
												{else}
													<input type="checkbox" class="none" name="layered_{$filter.type_lite}_{$id_value}" id="layered_{$filter.type_lite}{if $id_value || $filter.type == 'quantity'}_{$id_value}{/if}" value="{$id_value}{if $filter.id_key}_{$filter.id_key}{/if}"{if isset($value.checked)} checked="checked"{/if}{if !$value.nbr} disabled="disabled"{/if} />
												{/if}
												<label for="layered_{$filter.type_lite}_{$id_value}"{if !$value.nbr} class="disabled"{else}{if isset($filter.is_color_group) && $filter.is_color_group} name="layered_{$filter.type_lite}_{$id_value}" class="layered_color" data-rel="{$id_value}_{$filter.id_key}"{/if}{/if}>
													{if !$value.nbr}
														{$value.name|escape:'html':'UTF-8'}{if $layered_show_qties}<span> ({$value.nbr})</span>{/if}
													{else}
														<a class="item{if isset($value.checked) && $value.checked} active{/if}" href="{$value.link}"{if $value.rel|trim != ''} data-rel="{$value.rel}"{/if}>{$value.name|escape:'html':'UTF-8'}{if $layered_show_qties}<span> ({$value.nbr})</span>{/if}</a>
													{/if}
												</label>
											</li>
										{/if}
									{/foreach}
								{/if}
							{/if}
						</ul>
					{/if}
				{/foreach}
			{else}
				{foreach from=$filters item=filter name=filterForeach}
					{if isset($filter.values) && !($filter.name=='Категорії' && isset($hideCategory)) && !($filter.name=='Стиль' && !isset($hideCategory))}
						<div class="filter_wrap">
							<div class="filter_exp title{if $smarty.foreach.filterForeach.index == 0} size{/if}">{$filter.name|escape:'html':'UTF-8'}</div>
							<div class="filter_exp js_list_over{if $smarty.foreach.filterForeach.index == 0} size{/if}">
								<ul id="ul_layered_{$filter.type}_{$filter.id_key}" class="filter_options">
									{if !isset($filter.slider)}
										{if $filter.filter_type == 0}
											{foreach from=$filter.values key=id_value item=value name=fe}
												{if $value.nbr || !$hide_0_values}
													<li class="nomargin {if $smarty.foreach.fe.index >= $filter.filter_show_limit}hiddable{/if} col-lg-12">
														{if isset($filter.is_color_group) && $filter.is_color_group}
															{*<input class="color-option {if isset($value.checked) && $value.checked}on{/if} {if !$value.nbr}disable{/if}" type="button" name="layered_{$filter.type_lite}_{$id_value}" data-rel="{$id_value}_{$filter.id_key}" id="layered_id_attribute_group_{$id_value}" {if !$value.nbr}disabled="disabled"{/if} style="background: {if isset($value.color)}{if file_exists($smarty.const._PS_ROOT_DIR_|cat:"/img/co/$id_value.jpg")}url(img/co/{$id_value}.jpg){else}{$value.color}{/if}{else}#CCC{/if};" />*}
															{if isset($value.checked) && $value.checked}<input type="hidden" name="layered_{$filter.type_lite}_{$id_value}" value="{$id_value}" />{/if}
														{else}
															<input type="checkbox" class="none" name="layered_{$filter.type_lite}_{$id_value}" id="layered_{$filter.type_lite}{if $id_value || $filter.type == 'quantity'}_{$id_value}{/if}" value="{$id_value}{if $filter.id_key}_{$filter.id_key}{/if}"{if isset($value.checked)} checked="checked"{/if}{if !$value.nbr} disabled="disabled"{/if} />
														{/if}
														<label for="layered_{$filter.type_lite}_{$id_value}"{if !$value.nbr} class="disabled"{else}{if isset($filter.is_color_group) && $filter.is_color_group} name="layered_{$filter.type_lite}_{$id_value}" class="layered_color" data-rel="{$id_value}_{$filter.id_key}"{/if}{/if}>
															{if !$value.nbr}
																{$value.name|escape:'html':'UTF-8'}{if $layered_show_qties}<span> ({$value.nbr})</span>{/if}
															{else}
																<a class="item{if isset($value.checked) && $value.checked} active{/if}" href="{$value.link}"{if $value.rel|trim != ''} data-rel="{$value.rel}"{/if}>{$value.name|escape:'html':'UTF-8'}{if $layered_show_qties}<span> ({$value.nbr})</span>{/if}</a>
															{/if}
														</label>
													</li>
												{/if}
											{/foreach}
										{/if}

									{/if}
								</ul>
							</div>
						</div>
					{/if}
				{/foreach}

			{/if}

			<input type="hidden" name="id_category_layered" value="{$id_category_layered}" />
			{foreach from=$filters item=filter}
				{if $filter.type_lite == 'id_attribute_group' && isset($filter.is_color_group) && $filter.is_color_group && $filter.filter_type != 2}
					{foreach from=$filter.values key=id_value item=value}
						{if isset($value.checked)}
							<input type="hidden" name="layered_id_attribute_group_{$id_value}" value="{$id_value}_{$filter.id_key}" />
						{/if}
					{/foreach}
				{/if}
			{/foreach}
		</form>

		<div id="layered_ajax_loader" style="display: none;">
			<p>
				<img src="{$img_ps_dir}loader.gif" alt="" />
				<br />{l s='Loading...' mod='blocklayered'}
			</p>
		</div>
	</div>
{else}
	<div id="layered_block_left" class="block">
		<div class="block_content">
			<form action="#" id="layered_form">
				<input type="hidden" name="id_category_layered" value="{$id_category_layered}" />
			</form>
		</div>
		<div style="display: none;">
			<p>
				<img src="{$img_ps_dir}loader.gif" alt="" />
				<br />{l s='Loading...' mod='blocklayered'}
			</p>
		</div>
	</div>
{/if}
{if $nbr_filterBlocks != 0}
{strip}
	{if version_compare($smarty.const._PS_VERSION_,'1.5','>')}
		{addJsDef param_product_url='#'|cat:$param_product_url}
	{else}
		{addJsDef param_product_url=''}
	{/if}
	{addJsDef blocklayeredSliderName=$blocklayeredSliderName}

	{if isset($filters) && $filters|@count}
		{addJsDef filters=$filters}
	{/if}
{/strip}
{/if}
