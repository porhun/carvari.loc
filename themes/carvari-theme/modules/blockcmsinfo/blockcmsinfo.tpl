{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $info|@count > 0}
	<!-- MODULE Block cmsinfo -->
<div id="cmsinfo_block">
{*		<div class="information_block  is_post">
			<div id="information-text" class="information_text js_hide">
				<div class="js_information_text_box">
					{foreach from=$infos item=info}
						{$info.text}
					{/foreach}
		</div>
				<span class="text-box-gradient"></span>
	</div>
			<span id="information-more" class="information_more cp" style="display: none;">{l s='Розгорнути' mod='blockcmsinfo'}</span>
		</div>*}


		{* <div class="col-xs-12 col-sm-12">{$info.text}</div> *}
		<div id="cmsinfo-block"><div class="is_post">{$info}</div></div>

	</div>
	{literal}
		<style>
		#cmsinfo_block {
		width: 1000px;
		margin: 0 auto;
		}

		#cmsinfo-block {
		padding: 0 10px;
		}

		#cmsinfo-block .is_post {
		margin:0 10px;
		}

		#cmsinfo-block .column {
		width: 50%;
		float: left;
		padding:0 22px 0 0;
		}
		</style>
	{/literal}
	<!-- /MODULE Block cmsinfo -->
{/if}
