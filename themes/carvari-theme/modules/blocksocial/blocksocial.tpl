{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{*<section id="social_block">*}

<ul class="social_block">
    {if isset($facebook_url) && $facebook_url != ''}
	<li class="social_block__item">
		<a href="{$facebook_url|escape:html:'UTF-8'}" target="_blank"
		   class="social_block__images social_block__images--fb">
				<span>{l s='Facebook' mod='blocksocial'}</span>
                <svg id="385984f5-aef3-4a6c-9985-20ea0a832b51" data-name="16"
                     xmlns="http://www.w3.org/2000/svg" viewBox="0 0 99.04 99.04">
                    <path d="M85.15,42.89a49.52,49.52,0,1,1-35,14.5,49.33,49.33,0,0,1,35-14.5Zm32.3,17.25a45.71,45.71,0,1,0,13.35,32.27,45.62,45.62,0,0,0-13.35-32.27Z"
                          transform="translate(-35.64 -42.89)"/>
                    <path d="M88.94,82.29V78.23a2.13,2.13,0,0,1,2.22-2.41h5.68V67.15H89c-8.69,0-10.64,6.42-10.64,10.58v4.55h-5V92.41h5.1v25.28H88.55V92.41H96l.33-4,.6-6.14Z"
                          transform="translate(-35.64 -42.89)"/>
</svg>
		</a>
	</li>
    {/if}
    {if isset($instagram_url) && $instagram_url != ''}
	<li class="social_block__item">
		<a href="{$instagram_url|escape:html:'UTF-8'}" target="_blank"
		   class="social_block__images social_block__images--inst">
				<span>
                    {l s='Instagram' mod='blocksocial'}
				</span>
			<svg id="1e1e6610-e863-462d-8ffb-0ba9c4ea946c" data-name="16"
				 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 99.04 99.04">
				<path d="M211.09,42.89a49.52,49.52,0,1,1-35,14.5,49.28,49.28,0,0,1,35-14.5Zm32.3,17.25a45.71,45.71,0,1,0,13.35,32.27,45.62,45.62,0,0,0-13.35-32.27Z"
					  transform="translate(-161.57 -42.89)"/>
				<path d="M198.42,92.41a12.68,12.68,0,1,1,12.67,12.69,12.7,12.7,0,0,1-12.67-12.69Zm25.77-24.81H198a11.77,11.77,0,0,0-11.74,11.74v26.18A11.78,11.78,0,0,0,198,117.25h26.18a11.77,11.77,0,0,0,11.73-11.73V79.33a11.75,11.75,0,0,0-11.73-11.74ZM198,70.53h26.18a8.81,8.81,0,0,1,8.8,8.8v26.18a8.81,8.81,0,0,1-8.8,8.8H198a8.81,8.81,0,0,1-8.8-8.8V79.33a8.81,8.81,0,0,1,8.8-8.8Zm28.79,3.7a2.58,2.58,0,1,0,2.58,2.58,2.57,2.57,0,0,0-2.58-2.58Zm.3,18.18a16,16,0,1,0-16,16,16,16,0,0,0,16-16Z"
					  transform="translate(-161.57 -42.89)"/>
			</svg>
		</a>
	</li>
    {/if}
    {if isset($youtube_url) && $youtube_url != ''}
	<li class="social_block__item">
		<a href="{$youtube_url|escape:html:'UTF-8'}"
		   class="social_block__images social_block__images--yt">
				<span>
                    {l s='Youtube' mod='blocksocial'}
				</span>
			<svg id="137af273-6e8e-4d1e-a7cf-a9dbf1b53275" data-name="16"
				 xmlns="http://www.w3.org/2000/svg" viewBox="0 0 99.04 99.06">
				<path d="M588.91,165.92a49.52,49.52,0,1,1-35,14.5,49.45,49.45,0,0,1,35-14.5Zm32.27,17.25a45.65,45.65,0,1,0,13.38,32.3,45.48,45.48,0,0,0-13.38-32.3Z"
					  transform="translate(-539.4 -165.91)"/>
				<path d="M613.7,204.9s-.52-3.65-2-5.24a7.24,7.24,0,0,0-5-2.25c-7.07-.52-17.68-.52-17.68-.52h-.06s-10.58,0-17.66.52a7.21,7.21,0,0,0-5.07,2.25c-1.51,1.59-2,5.24-2,5.24a80.53,80.53,0,0,0-.52,8.5v4a79.19,79.19,0,0,0,.52,8.5s.52,3.65,2,5.26c1.92,2.11,4.44,2,5.59,2.25,4,.41,17.19.55,17.19.55s10.61-.05,17.68-.55a7.12,7.12,0,0,0,5-2.25c1.51-1.62,2-5.26,2-5.26a83.81,83.81,0,0,0,.49-8.5v-4c0-4.22-.49-8.5-.49-8.5Zm-16.34,10-13.68,7.37V207.45l6.2,3.37Z"
					  transform="translate(-539.4 -165.91)"/>
			</svg>
		</a>
	</li>
    {/if}
</ul>


{**}
	{*<ul class="social_block">*}
		{*{if isset($facebook_url) && $facebook_url != ''}*}
			{*<li class="facebook social_block__item">*}
				{*<a class="_blank" href="{$facebook_url|escape:html:'UTF-8'}">*}
					{*<span>{l s='Facebook' mod='blocksocial'}</span>*}
                    {*<img class="social_block__images" src="{$tpl_uri}img/social/facebook.svg" alt="Facebook">*}
				{*</a>*}
			{*</li>*}
		{*{/if}*}
        {*{if isset($vk_url) && $vk_url != ''}*}
			{*<li class="vk social_block__item">*}
				{*<a class="_blank" href="{$vk_url|escape:html:'UTF-8'}">*}
					{*<span>{l s='Vkontakte' mod='blocksocial'}</span>*}
				{*</a>*}
			{*</li>*}
        {*{/if}*}
		{*{if isset($twitter_url) && $twitter_url != ''}*}
			{*<li class="twitter social_block__item">*}
				{*<a class="_blank" href="{$twitter_url|escape:html:'UTF-8'}">*}
					{*<span>{l s='Twitter' mod='blocksocial'}</span>*}
				{*</a>*}
			{*</li>*}
		{*{/if}*}
		{*{if isset($rss_url) && $rss_url != ''}*}
			{*<li class="rss social_block__item">*}
				{*<a class="_blank" href="{$rss_url|escape:html:'UTF-8'}">*}
					{*<span>{l s='RSS' mod='blocksocial'}</span>*}
				{*</a>*}
			{*</li>*}
		{*{/if}*}
        {*{if isset($youtube_url) && $youtube_url != ''}*}
        	{*<li class="youtube social_block__item">*}
        		{*<a class="_blank" href="{$youtube_url|escape:html:'UTF-8'}">*}
        			{*<span>{l s='Youtube' mod='blocksocial'}</span>*}
                    {*<img class="social_block__images" src="{$tpl_uri}img/social/youtube.svg" alt="Youtube">*}
        		{*</a>*}
        	{*</li>*}
        {*{/if}*}
        {*{if isset($google_plus_url) && $google_plus_url != ''}*}
        	{*<li class="google-plus social_block__item">*}
        		{*<a class="_blank" href="{$google_plus_url|escape:html:'UTF-8'}">*}
        			{*<span>{l s='Google Plus' mod='blocksocial'}</span>*}
        		{*</a>*}
        	{*</li>*}
        {*{/if}*}
        {*{if isset($pinterest_url) && $pinterest_url != ''}*}
        	{*<li class="pinterest social_block__item">*}
        		{*<a class="_blank" href="{$pinterest_url|escape:html:'UTF-8'}">*}
        			{*<span>{l s='Pinterest' mod='blocksocial'}</span>*}
        		{*</a>*}
        	{*</li>*}
        {*{/if}*}
        {*{if isset($vimeo_url) && $vimeo_url != ''}*}
        	{*<li class="vimeo social_block__item">*}
        		{*<a class="_blank" href="{$vimeo_url|escape:html:'UTF-8'}">*}
        			{*<span>{l s='Vimeo' mod='blocksocial'}</span>*}
        		{*</a>*}
        	{*</li>*}
        {*{/if}*}
        {*{if isset($instagram_url) && $instagram_url != ''}*}
        	{*<li class="instagram social_block__item">*}
        		{*<a class="_blank" href="{$instagram_url|escape:html:'UTF-8'}">*}
        			{*<span>{l s='Instagram' mod='blocksocial'}</span>*}
                    {*<img class="social_block__images" src="{$tpl_uri}img/social/instagram.svg" alt="Instagram">*}
        		{*</a>*}
        	{*</li>*}
        {*{/if}*}
	{*</ul>*}
    {*<h4>{l s='Follow us' mod='blocksocial'}</h4>*}
{*</section>*}
{*<div class="clearfix"></div>*}
