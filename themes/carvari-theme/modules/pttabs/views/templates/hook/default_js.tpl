{*
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *}

<script type="text/javascript">
    //<![CDATA[
    jQuery(document).ready(function ($) {
        ;
        (function (element) {
            var $element = $(element),
				$tab = $('.tab-item', $element),
				ajax_url = baseDir + 'modules/pttabs/pttabs_ajax.php',
				$items_content = $('.list-items', $element),
				module_id = $('.list-tabs-wrap', $element).attr('data-moduleid'),
				$list_items_inner = $('.list-items-inner', $items_content);

            $tab.on('click.tab', function () {
                var $this = $(this);
                if ($this.hasClass('active')) return false;
                $tab.removeClass('active');
                $this.addClass('active');
                var items_active = $this.attr('data-content');
                var _items_active = $(items_active, $element);
				var category_id = $this.attr('data-category');
                $items_content.removeClass('active');
                _items_active.addClass('active');	
				var loaded = _items_active.hasClass('loaded');
				if(!loaded){
					_items_active.addClass('loaded');
					$.ajax({
						type: 'POST',
						url: ajax_url,
						data: {
							pttabs_moduleid: module_id,
							pttabs_is_ajax: 1,
							categoryid: category_id
						},
						success: function (data) {
							if (data.items_markup != '') {
								$(_items_active).html(data.items_markup);
							}
						},
						dataType: 'json'
					});				
				}
            });
        })('#{$tag_id|escape:'html':'UTF-8'}');
    });
    //]]>
</script>