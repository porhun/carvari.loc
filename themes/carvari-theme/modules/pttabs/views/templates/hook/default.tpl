{*
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *}


{if isset($list) && !empty($list)}

    <div class="tabs">
        <ul class="tabs__caption caption">
        {foreach from=$list item=items name=tabsCaption}
            <li class="caption__item{if $smarty.foreach.tabsCaption.first} active{/if}">{$items->title_module[$cookie->id_lang]}</li>
        {/foreach}
        </ul>


        {foreach from=$list item=items name=tabsContent}
        <div class="tabs__content{if $smarty.foreach.tabsContent.first} active{/if}">
            {$_list = $items->products}

            {math equation='rand()' assign='rand'}
            {assign var='randid' value="now"|strtotime|cat:$rand}
            {assign var="tag_id" value="pt_tabs_{$items->id_module}_{$randid}"}

            <div id="{$tag_id|escape:'html':'UTF-8'}" class="pt-tabs-container">
                <div class="pt-tabs-container-inner">
                    <!--Begin Content-->
                    <div class="list-items-wrap">
                        {foreach $_list as $item}
                            {assign var="child_items" value=(isset($item['child']))?$item['child']:''}
                            {assign var="active" value=(isset($item['sel']) && $item['sel'] == 'sel')?' active':''}
                            {assign var="tab_all" value=($item['id_category'] == "*")?' items-category-all':' items-category-'|cat:$item['id_category']}
                            <div class="list-items {$active|escape:'html':'UTF-8'} {$tab_all|escape:'html':'UTF-8'}">
                                {if !empty($child_items)}
                                    {include file="./product.tpl"}
                                {else}
                                    <div class="image-loading"></div>
                                {/if}
                            </div>
                        {/foreach}
                    </div>
                    <!-- End Tabs-->
                </div>
                {include file="./default_js.tpl"}
            </div>
        </div>
        {/foreach}
    </div>

{/if}