{*
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *}

{if !isset($items_params)}
    {assign var="items_params" value=$items->params}
{/if}
{if !empty($child_items)}
	{math equation='rand()' assign='rand'}
	{assign var='randid' value="now"|strtotime|cat:$rand}
	{assign var="content_id" value="list_items_{$items->id_module}_{$randid}"}	
	<ul class="list-items-inner product_list_1 owl-carousel" id="{$content_id}">
    {foreach $child_items as $product}
		<li class="ajax_block_product">
			<div class="product-container">
				<div class="left-block">
					{if $product.id_image}
						<div class="product-image-container">
							<a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{' '|implode:[$product.name, $product.reference]|escape:'html':'UTF-8'}" {$product._target} >
								{assign var="src" value=($items->params.image_size != 'none') ? {$link->getImageLink($product.link_rewrite, $product.id_image, $items_params.image_size)|escape:'html'} :  {$link->getImageLink($product.link_rewrite, $product.id_image)|escape:'html'}}
								<img src="{$src}" alt="{$product.legend|escape:html:'UTF-8'}"/>
							</a>

							{if $items_params.show_new == 1 && isset($product.condition) && $product.condition == 'new'}
								<a class="new-box" href="{$product.link|escape:'html':'UTF-8'}">
									<span class="new-label">{l s='New' mod='ptslider'}</span>
								</a>
							{/if}

							{if $items_params.show_sale == 1 && isset($product.on_sale) && $product.on_sale && isset($product.show_price) && $product.show_price && !$PS_CATALOG_MODE}
								<a class="sale-box" href="{$product.link|escape:'html':'UTF-8'}">
									<span class="sale-label">{l s='Sale!' mod='ptslider'}</span>
								</a>
							{/if}											
							
							{if isset($quick_view) && $quick_view}
								<a class="quick-view" href="{$product.link|escape:'html':'UTF-8'}" rel="{$product.link|escape:'html':'UTF-8'}">
									<span>{l s='Quick view' mod='ptslider'}</span>
								</a>
							{/if}
						</div>
					{/if}
				</div>
				
				<div class="right-block">
					{if $items_params.show_name}
						<div class="product-name">
							<a href="{$product.link|escape:'html':'UTF-8'}" title="{' '|implode:[$product.name, $product.reference]|escape:'html':'UTF-8'}" {$product._target}>
								{' '|implode:[$product.title, $product.reference]|escape:'html':'UTF-8'}
							</a>
						</div>
					{/if}
					{if $items_params.show_description}
						<p class="product-desc">
							{$product.desc}
						</p>
					{/if}
					{if $items_params.show_reviews}
						{hook h='displayProductListReviews' product=$product}
					{/if}									
					{if $items_params.show_price}
						<div class="content_price">
							{if (!$PS_CATALOG_MODE AND ((isset($product.show_price) && $product.show_price) || (isset($product.available_for_order) && $product.available_for_order)))}
								<div itemprop="offers" itemscope
									 itemtype="http://schema.org/Offer"
									 class="content_price">
									{if isset($product.show_price) && $product.show_price && !isset($restricted_country_mode)}
										<span itemprop="price" class="price product-price">
									{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
										</span>
										<meta itemprop="priceCurrency" content="{$currency->iso_code}"/>
										{if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
											{hook h="displayProductPriceBlock" product=$product type="old_price"}
											<span class="old-price product-price">
									{displayWtPrice p=$product.price_without_reduction}
								</span>
											{hook h="displayProductPriceBlock" id_product=$product.id_product type="old_price"}
											{*{if $product.specific_prices.reduction_type == 'percentage'}*}
												{*<span class="price-percent-reduction">-{$product.specific_prices.reduction * 100}*}
													{*%</span>*}
											{*{/if}*}
										{/if}
										{hook h="displayProductPriceBlock" product=$product type="price"}
										{hook h="displayProductPriceBlock" product=$product type="unit_price"}
									{/if}
								</div>
							{/if}
						</div>
					{/if}
					
					{if $items_params.show_addtocart || $items_params.show_wishlist || $items_params.show_compare}
						<div class="button-container">
							{if $items_params.show_addtocart}
								{if ($product.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $product.available_for_order && !isset($restricted_country_mode) && $product.minimal_quantity <= 1 && $product.customizable != 2 && !$PS_CATALOG_MODE}
									{if (!isset($product.customization_required) || !$product.customization_required) && ($product.allow_oosp || $product.quantity > 0)}
										{if isset($static_token)}
											<a class="button ajax_add_to_cart_button btn btn-default"
											   href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$product.id_product|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}"
											   rel="nofollow"
											   title="{l s='Add to cart' mod='pttabs'}"
											   data-id-product="{$product.id_product|intval}">
												<span>{l s='Add to cart' mod='pttabs'}</span>
											</a>
										{else}
											<a class="button ajax_add_to_cart_button btn btn-default"
											   href="{$link->getPageLink('cart',false, NULL, 'add=1&amp;id_product={$product.id_product|intval}', false)|escape:'html':'UTF-8'}"
											   rel="nofollow"
											   title="{l s='Add to cart' mod='pttabs'}"
											   data-id-product="{$product.id_product|intval}">
												<span>{l s='Add to cart' mod='pttabs'}</span>
											</a>
										{/if}
									{else}
										<span class="button ajax_add_to_cart_button btn btn-default disabled">
											<span>{l s='Add to cart' mod='pttabs'}</span>
										</span>
									{/if}
								{/if}
							{/if}
							{if $items_params.show_wishlist || $items_params.show_compare}
								<div class="functional-buttons clearfix">
									{if $items_params.show_wishlist}
										{hook h='displayProductListFunctionalButtons' product=$product}
									{/if}
									{if $items_params.show_compare && isset($comparator_max_item) && $comparator_max_item}
										<div class="compare">
											<a class="add_to_compare"
											   href="{$product.link|escape:'html':'UTF-8'}"
											   data-id-product="{$product.id_product}">{l s='Add to Compare' mod='ptslider'}</a>
										</div>
									{/if}
								</div>
							{/if}
						</div>
					{/if}
				</div>								
			</div>
		</li>
    {/foreach}
	</ul>
	
	{assign var="nav" value=($items_params.show_nav == '1')?'true':'false'}			
	{assign var="dots" value=($items_params.show_page == '1')?'true':'false'}
	{assign var="autoplay" value=($items_params.auto == '1')?'true':'false'}
	{assign var="pause" value=($items_params.pause == '1')?'true':'false'}	
	<script type="text/javascript">
		jQuery('#{$content_id}').owlCarousel({
			margin: 20,
			loop: true,
			autoplay: {$autoplay},
			autoplayHoverPause: {$pause},	
			dots: {$dots},					
			nav: {$nav},
			navText: [ '', '' ],
			slideBy: {$items_params.step},
			autoplayTimeout: {$items_params.interval},
			smartSpeed: {$items_params.duration},
			responsive: {
				0:{
					items:{$items_params.col_xs}
				},
				768:{
					items:{$items_params.col_sm}
				},
				992:{
					items:{$items_params.col_md}
				},
				1200:{
					items:{$items_params.col_lg}
				}
			}				   
		});
	</script>	
{/if}
