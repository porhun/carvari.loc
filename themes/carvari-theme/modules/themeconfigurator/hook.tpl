{if isset($htmlitems) && $htmlitems}
	{if $mobile_device}
		{if $hook == 'GenderBanner'}
			<div class="main-page__stage clearfix">
				{foreach name=items from=$htmlitems item=hItem}
					<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="img_link big"><span class="big_title">{$hItem.title|escape:'htmlall':'UTF-8'}</span><div class="img_container"><img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" /></div></a>
				{/foreach}
			</div>
		{elseif $hook == 'BigBanner'}
			<div class="main-page__single">
				{foreach name=items from=$htmlitems item=hItem}
					<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="img_link small">
						<div class="img_container">
							<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" />
						</div>
						<span class="find_pair_text">{*<span class="big_text">{$hItem.title|escape:'htmlall':'UTF-8'}</span>*}{*<span class="small_text">{$hItem.html}</span>*}</span>
					</a>
				{/foreach}
			</div>
		{elseif $hook == 'SmallBanners'}
			<div class="main-page__stage">
				{foreach name=items from=$htmlitems item=hItem}
					<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="img_link small"><div class="img_container"><img class="big_img" src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" />{*<span class="title">{$hItem.title|escape:'htmlall':'UTF-8'}</span>*}</div></a>
				{/foreach}
			</div>
		{/if}
	{else}
		{if $hook == 'GenderBanner'}
			<div class="choose_gender_container clearfix">
				{foreach name=items from=$htmlitems item=hItem}
					<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="choose_gender">
						<div class="title">{$hItem.title|escape:'htmlall':'UTF-8'}</div>
						<div class="img_container">
							<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img img-responsive" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>

						</div>
					</a>
				{/foreach}
			</div>
		{elseif $hook == 'BigBanner'}
{foreach name=items from=$htmlitems item=hItem}
	<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="find_pair">
		<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img img-responsive" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
		{*<span class="find_pair_text">
			<span class="big_text">{$hItem.title|escape:'htmlall':'UTF-8'}</span>
			<span class="small_text">{$hItem.html}</span>
		</span>*}
	</a>
{/foreach}
{elseif $hook == 'SmallBanners'}
<div class="clearfix">
	{foreach name=items from=$htmlitems item=hItem}<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="nav_blocks {if $smarty.foreach.items.iteration ==1} first{elseif $smarty.foreach.items.iteration == 2} second{/if}"><img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img img-responsive" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/><span class="title">{$hItem.title|escape:'htmlall':'UTF-8'}</span></a>{/foreach}
</div>
{else}
			<div id="htmlcontent_{$hook|escape:'htmlall':'UTF-8'}"{if $hook == 'footer'} class="footer-block col-xs-12 col-sm-4"{/if}>
				<ul class="htmlcontent-home clearfix row">
					{foreach name=items from=$htmlitems item=hItem}
						{if $hook == 'left' || $hook == 'right'}
							<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-12">
						{else}
							<li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'} col-xs-4">
						{/if}
								{if $hItem.url}
									<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
								{/if}
									{if $hItem.image}
										<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img {if $hook == 'left' || $hook == 'right'}img-responsive{/if}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
									{/if}
									{if $hItem.title && $hItem.title_use == 1}
										<h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
									{/if}
									{if $hItem.html}
										<div class="item-html">
											{$hItem.html}
										</div>
									{/if}
								{if $hItem.url}
									</a>
								{/if}
							</li>
					{/foreach}
				</ul>
			</div>
		{/if}
	{/if}
{/if}