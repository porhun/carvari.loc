<div class="subscribe_form footer_subscribe__form subscribe clearfix">
	<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
		<div class="subscribe__title">
            {l s='Реєструйтеся та будьте в курсі нових акцій' mod='blocknewsletter'}
		</div>
		<input type="text" class="subscribe__input js--subscribe__input subscribe_popup__input" placeholder="your@email.com" id="newsletter-popup-input">
		<div class="subscribe__notification subscribe_form__notification" style="display: none;"></div>
		<button type="submit" class="subscribe__submit sub_woman" id="newsletter-submit">
            {l s='Жінка' mod='blocknewsletter'}
		</button>
		<button type="submit" class="subscribe__submit sub_man" id="newsletter-submit">
            {l s='Чоловік' mod='blocknewsletter'}
		</button>
		<input type="hidden" name="action" value="0" />
		<input type="hidden" name="gender" value="" id="gender" value="0" />
	</form>
</div>






