<div class="subscribe_popup">
	<div class="subscribe_popup__title">
		{l s='Долучайся до e-mail розсилки та отримай знижку на покупки' mod='blocknewsletter'}
	</div>
	<div class="subscribe_popup__form">
		<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
			<input type="text" class="subscribe_popup__input" placeholder="your@email.com" id="newsletter-popup-input">
			<input type="submit" value="{l s='Підписатися' mod='blocknewsletter'}" class="subscribe_popup__submit" id="newsletter-popup-submit">
		</form>
	</div>
	<div class="subscribe_popup__notification" style="display: none;"></div>
	<div class="subscribe_popup__close"></div>
</div>
