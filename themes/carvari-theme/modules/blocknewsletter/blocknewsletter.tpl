{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block Newsletter module-->
<div class="subscribe_block">
	<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
		<span class="">{l s='Підпишіться на наші новини' mod='blocknewsletter'}</span>
		<input type="text" id="newsletter-input" name="email" class="email_input ph" value="{if isset($msg) && $msg}{$msg}{elseif isset($value) && $value}{$value}{/if}" placeholder="{l s='Ваш e-mail' mod='blocknewsletter'}">
		<button class="sub_button" type="submit" id="newsletter-submit">
			<i class="icon">
				OK
				<!-- <svg>
					<use xlink:href="#subscribe_icon">
				</svg> -->
			</i>
		</button>
	</form>
</div>
<div>
	<span id="newsletter_error" class="error_msg"></span>
	<span class="newsletter_success_msg"></span>
</div>
{hook h="displayBlockNewsletterBottom" from='blocknewsletter'}

<!-- /Block Newsletter module-->
{strip}
{if isset($msg) && $msg}
{addJsDef msg_newsl=$msg|@addcslashes:'\''}
{/if}
{if isset($nw_error)}
{addJsDef nw_error=$nw_error}
{/if}
{addJsDefL name=placeholder_blocknewsletter}{l s='Enter your e-mail' mod='blocknewsletter' js=1}{/addJsDefL}
{if isset($msg) && $msg}
	{addJsDefL name=alert_blocknewsletter}{l s='Newsletter : %1$s' sprintf=$msg js=1 mod="blocknewsletter"}{/addJsDefL}
{/if}
{/strip}