<script>
	var currentBlog = '{if $is_category}category{else}home{/if}';
</script>
{if $mobile_device}
	<div class="main_content body trends-page">
		<div class="padded_block">
			<h1>{l s='Блог' mod='ph_simpleblog'}</h1>
		</div>

		<div id="trends-list">
			{foreach from=$posts item=post}
				<div class="one_entry toggled">
					<a href="{$post.url}">
						<img src="{$post.banner_thumb}" alt="" class="full-screen-img cover">
						<h2>{$post.meta_title}</h2>
					</a>
					<div class="padded_block">
						<div class="post">
							{$post.short_content}
						</div>

						<div class="attrs">
							{$post.category}, {$post.date_add|date_format:Configuration::get('PH_BLOG_DATEFORMAT')}
						</div>
					</div>
				</div>
			{/foreach}
		</div>

		<div class="action">
			<a class="button js-show-more" data-offset="2" id="trends-more">{l s='Показати ще' mod='ph_simpleblog'}</a>
		</div>
	</div>
{else}

	<div class="main_content trends">
		<h1>{l s='Блог' mod='ph_simpleblog'}</h1>
        {include file="$tpl_dir./breadcrumb.tpl"}
		{if Configuration::get('PH_BLOG_DISPLAY_BREADCRUMBS')}
			{capture name=path}
				<a href="{ph_simpleblog::getLink()}">{l s='Blog' mod='ph_simpleblog'}</a>
				{if $is_category eq true}
					<span class="navigation-pipe">{$navigationPipe}</span>{$blogCategory->name}
				{/if}
			{/capture}
			{if !$is_16}{include file="$tpl_dir./breadcrumb.tpl"}{/if}
		{/if}

		{if isset($posts) && count($posts) > 0}
			<div class="ph_simpleblog simpleblog-{if $is_category}category{else}home{/if}">
				{*{if $is_category eq true}*}
				{*<h1>{$blogCategory->name}</h1>*}

				{*{if Configuration::get('PH_BLOG_DISPLAY_CATEGORY_IMAGE') && isset($blogCategory->image)}*}
				{*<div class="simpleblog-category-image">*}
				{*<img src="{$blogCategory->image}" alt="{$blogCategory->name}" class="img-responsive" />*}
				{*</div>*}
				{*{/if}*}

				{*{if !empty($blogCategory->description) && Configuration::get('PH_BLOG_DISPLAY_CAT_DESC')}*}
				{*<div class="ph_cat_description">*}
				{*{$blogCategory->description}*}
				{*</div>*}
				{*{/if}*}
				{*{else}*}
				{*<h1>{$blogMainTitle}</h1>*}
				{*{/if}*}


				<div class="blogs_list">
					{foreach from=$posts item=post}

						{*{assign var='cols' value='col-md-6 col-xs-6 col-ms-12'}*}

						{*{if $columns eq '3'}*}
						{*{assign var='cols' value='col-md-4 col-xs-4 col-ms-12'}*}
						{*{/if}*}

						{*{if $columns eq '4'}*}
						{*{assign var='cols' value='col-md-3 col-xs-3 col-ms-12'}*}
						{*{/if}*}

						<a href="{$post.url}" class="blog_link">
							{*<div class="img_holder">*}
							<img src="{$post.banner_thumb}" alt="{$post.meta_title}" class="img-responsive" />
							{*</div>*}
							<div class="blog_title">{$post.meta_title}</div>
							<div class="blog_text">{$post.short_content}</div>
							<div class="blog_date">{$post.category}, {$post.date_add|date_format:Configuration::get('PH_BLOG_DATEFORMAT')}</div>
						</a>

						{*<div class="simpleblog-post-item {if $blogLayout eq 'grid'}{$cols}{else}col-md-12{/if}">*}


						{*<div class="post-item">*}

						{*{if isset($post.banner) && Configuration::get('PH_BLOG_DISPLAY_THUMBNAIL')}*}
						{*<div class="post-thumbnail">*}
						{*<a href="{$post.url}" title="{l s='Permalink to' mod='ph_simpleblog'} {$post.meta_title}">*}
						{*{if $blogLayout eq 'full'}*}
						{*<img src="{$post.banner_wide}" alt="{$post.meta_title}" class="img-responsive" />*}
						{*{else}*}
						{*<img src="{$post.banner_thumb}" alt="{$post.meta_title}" class="img-responsive" />*}
						{*{/if}*}
						{*</a>*}
						{*</div>*}
						{*{/if}*}

						{*<div class="post-content">*}
						{*<h2>*}
						{*<a href="{$post.url}" title="{$post.meta_title}">{$post.meta_title}</a>*}
						{*</h2>*}
						{*{if Configuration::get('PH_BLOG_DISPLAY_DESCRIPTION')}*}
						{*{$post.short_content}*}
						{*{/if}*}
						{*</div>*}

						{*<div class="post-additional-info">*}
						{*{if Configuration::get('PH_BLOG_DISPLAY_DATE')}*}
						{*<span class="post-date">*}
						{*{l s='Posted on:' mod='ph_simpleblog'} {$post.date_add|date_format:Configuration::get('PH_BLOG_DATEFORMAT')}*}
						{*</span>*}
						{*{/if}*}

						{*{if $is_category eq false && Configuration::get('PH_BLOG_DISPLAY_CATEGORY')}*}
						{*<span class="post-category">*}
						{*{l s='Posted in:' mod='ph_simpleblog'} <a href="{$post.category_url}" title="">{$post.category}</a>*}
						{*</span>*}
						{*{/if}*}

						{*{if isset($post.author) && !empty($post.author) && Configuration::get('PH_BLOG_DISPLAY_AUTHOR')}*}
						{*<span class="post-author">*}
						{*{l s='Author:' mod='ph_simpleblog'} {$post.author}*}
						{*</span>*}
						{*{/if}*}

						{*{if isset($post.tags) && $post.tags && Configuration::get('PH_BLOG_DISPLAY_TAGS')}*}
						{*<span class="post-tags clear">*}
						{*{l s='Tags:' mod='ph_simpleblog'}*}
						{*{foreach from=$post.tags item=tag name='tagsLoop'}*}
						{*{$tag}{if !$smarty.foreach.tagsLoop.last}, {/if}*}
						{*{/foreach}*}
						{*</span>*}
						{*{/if}*}
						{*</div><!-- .additional-info -->*}
						{*</div>*}
						{*</div><!-- .simpleblog-post-item -->*}

					{/foreach}


				</div>
				<div class="clearfix"></div>


				{if $is_category}
					{include file="./pagination.tpl" rewrite=$blogCategory->link_rewrite type='category'}
				{else}
					{include file="./pagination.tpl" rewrite=false type=false}
				{/if}
			</div><!-- .ph_simpleblog -->
		</div>
		<script>
			$(window).load(function() {
				$('body').addClass('simpleblog simpleblog-'+currentBlog);
			});
			{if $blogLayout eq 'grid'}
			$(window).load(  SimpleBlogEqualHeight  );
			$(window).resize(SimpleBlogEqualHeight);

			function SimpleBlogEqualHeight()
			{
				var mini = 0;
				$('.simpleblog-post-item .post-item').each(function(){
					if(parseInt($(this).css('height')) > mini )
					{
						mini = parseInt($(this).css('height'));
					}
				});

				$('.simpleblog-post-item .post-item').css('height',mini+40);
			}
			{/if}
		</script>
	{else}
		<p class="warning alert alert-warning">{l s='There are no posts' mod='ph_simpleblog'}</p>
	{/if}

{/if}

