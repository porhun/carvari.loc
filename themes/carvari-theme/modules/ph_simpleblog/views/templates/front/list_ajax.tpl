{foreach from=$posts item=post}
	<div class="one_entry toggled">
		<a href="{$post.url}">
			<img src="{$post.banner_thumb}" alt="" class="full-screen-img cover">
			<h2>{$post.meta_title}</h2>
		</a>
		<div class="padded_block">
			<div class="post">
				{$post.short_content}
			</div>

			<div class="attrs">
				{$post.category}, {$post.date_add|date_format:Configuration::get('PH_BLOG_DATEFORMAT')}
			</div>
		</div>
	</div>
{/foreach}
