{if $posts && $posts|count}
    <div class="main-page__middle_area">
        {foreach from=$posts item=post name='ph_simpleblog_posts'}
            <div class="middle_area__post post js-parent">
                <a href="{$post.url}" class="post__post-link">
                    <div class="img-wrap">
                        <div class="post__image post-photo">
                            <img src="{$post.banner_thumb}" class="post-photo__img" alt="{$post.meta_title}">
                        </div>
                        <span class="post__brand">L’CARVARI</span>
                    </div>
                    <h3 class="post__title"><span class="title__text">{$post.meta_title}</span> {if $post.author}<span class="title__name">{$post.author}</span>{/if}</h3>
                </a>
                {if $post.short_content}
                    <div class="post__text p-text js-text">
                        <div class="inf-text-box">{$post.short_content}</div>
                        <div class="p-text__gradient"></div>
                    </div>
                    {*If short post content is bigger than 260 characters, we display slider to show more text info*}
                    {*{if $post.short_content|count_characters > 260}
                        <div class="js-open-text-btn"></div>
                    {/if}*}
                {/if}
            </div>
        {/foreach}
    </div>
{/if}