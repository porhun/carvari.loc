{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2016 PrestaShop SA
* @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*}
<!-- Block languages module -->
{if count($languages) > 1}
	<div class="language">
		{foreach from=$languages|@array_reverse:true key=k item=language name="languages"}
            {if $language.iso_code == $lang_iso}
			<a class="language__point language__point--active"
			   title="{$language.name|escape:'html':'UTF-8'}"
			   rel="alternate" hreflang="{$language.iso_code|escape:'html':'UTF-8'}"
			>
			{else}
			<a class="language__point{if $language.iso_code == $lang_iso} language__point--active{/if}"
			   href="{$link->getLanguageLink($language.id_lang)|escape:'html':'UTF-8'}"
			   title="{$language.name|escape:'html':'UTF-8'}"
			   rel="alternate" hreflang="{$language.iso_code|escape:'html':'UTF-8'}"
			>
			{/if}
			{*{if $language.iso_code != 'ru'}*}
			{*<div class="language__flag">*}
				{*<img src="{$tpl_uri}/img/langs/{$language.iso_code|escape:'html':'UTF-8'}.gif" alt="" class="language__flag_img">*}
			{*</div>*}
			{*{/if}*}
			<div class="language__title">
                {if $language.iso_code === 'uk'}
                    {$language.iso_code = 'УКР'}
				{elseif $language.iso_code === 'ru'}
                    {$language.iso_code = 'РУС'}
                {/if}
                    {$language.iso_code|escape:'html':'UTF-8'}
			</div>
			</a>
		{/foreach}
	</div>
{/if}
<!-- /Block languages module -->
