{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if count($categoryProducts) > 0 && $categoryProducts !== false}
<div class="row row-full related_products">
	<div class="col-md-6">
		<div class="related_title">
			{l s='Схожi моделi' mod='productscategory'}
		</div>
		<div class="slider">

			{foreach from=$categoryProducts item='categoryProduct' name=categoryProduct}
			<div class="slide">

				<a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}">
					<img src="{$link->getImageLink($categoryProduct.link_rewrite, $categoryProduct.id_image, 'product_slide')|escape:'html':'UTF-8'}" alt=""/>
					<span>
						{if isset($categoryProduct.specific_prices) && $categoryProduct.specific_prices
						&& ($categoryProduct.displayed_price|number_format:2 !== $categoryProduct.price_without_reduction|number_format:2)}

							{*<span class="price special-price">*}
								{convertPrice price=$categoryProduct.displayed_price}
							{*</span>*}
							{*{if $categoryProduct.specific_prices.reduction && $categoryProduct.specific_prices.reduction_type == 'percentage'}*}
							{*<span class="price-percent-reduction small">-{$categoryProduct.specific_prices.reduction * 100}%</span>*}
							{*{/if}*}
							{*<span class="old-price">{displayWtPrice p=$categoryProduct.price_without_reduction}</span>*}

						{else}
							{*<span class="price">*}
								{convertPrice price=$categoryProduct.displayed_price}
							{*</span>*}
						{/if}
					</span>
				</a>
				</div>
			{/foreach}
		</div>
	</div>
</div>
{/if}
