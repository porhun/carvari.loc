{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
    {if !isset($priceDisplayPrecision)}
        {assign var='priceDisplayPrecision' value=2}
    {/if}
    {if !$priceDisplay || $priceDisplay == 2}
        {assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, 6)}
        {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
    {elseif $priceDisplay == 1}
        {assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, 6)}
        {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
    {/if}

<div class="main_content product-page">
			<div class="row row-full this_product">
				<div class="js_product_zoom_holder_wrap">
					<div class="js_product_zoom_holder"></div>
				</div>
				<div class="col-md-4 left_column">
					<div class="navigation fbc clearfix">
						<a href="{$return_link}" onclick="window.history.go(-1); return false;" class="follow_back">{l s='Назад'}</a>
                        {include file="$tpl_dir./breadcrumb.tpl"}
					</div>
					<div class="product_image">
                        {if isset($video_url) && trim($video_url) != ''}
                            <div class="product_image__video js-product_image__video">
                                <iframe width="648" height="364" src="https://www.youtube.com/embed/{$video_url}?rel=0&controls=0&showinfo=0&autoplay=1&loop=1&playlist={$video_url}" frameborder="0" gesture="media" allowfullscreen></iframe>
                            </div>
                        {/if}
                        {if $have_image}
                            <a href="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" class="js_product_image_zoom_holder">
                                {*<img itemprop="image" src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}" title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{$product->name|escape:'html':'UTF-8'}{/if}" width="{$largeSize.width}" height="{$largeSize.height}"/>*}
                                <img
		                                itemprop="image"
		                                src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')|escape:'html':'UTF-8'}"
		                                {*title="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{' '|implode:[$product->name, $product->reference]|escape:'html':'UTF-8'}{/if}" alt="{if !empty($cover.legend)}{$cover.legend|escape:'html':'UTF-8'}{else}{' '|implode:[$product->name, $product->reference]|escape:'html':'UTF-8'}{/if}" height="{$largeSize.height}"*}
		                                class="js_product_image_zoom"
		                                data-zoom-src="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'thickbox_default')|escape:'html':'UTF-8'}"
                                />
                            </a>
                        {else}
                            <img itemprop="image" src="{$img_prod_dir}{$lang_iso}-default-large_default.jpg" id="bigpic" alt="" title="{' '|implode:[$product->name, $product->reference]|escape:'html':'UTF-8'}" width="{$largeSize.width}" height="{$largeSize.height}"/>
                        {/if}
                    </div>

                    {if isset($images) && count($images) > 0}

                        <!-- thumbnails -->
                            <div class="product_images">
                                <div class="slider">
                                    {if isset($video_url) && trim($video_url) != ''}
                                        <div class="slide">
                                            <a href="#" class="video_thumbnails_block js-video_thumbnails_block">
                                                <img itemprop="image" src="{$tpl_uri}img/video_thumbnail.jpg" title="{if isset($video_title)}{$video_title}{/if}">
                                            </a>
                                        </div>
                                    {/if}
                                            {if isset($images)}
                                                {foreach from=$images item=image name=thumbnails}
                                                    {assign var=imageIds value="`$product->id`-`$image.id_image`"}
                                                    {if !empty($image.legend)}
                                                        {assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
                                                    {else}
                                                        {assign var=imageTitle value=' '|implode:[$product->name, $product->reference]|escape:'html':'UTF-8'}
                                                    {/if}
                                                    <div class="slide">
                                                        <a
		                                                        data-ind="{$smarty.foreach.thumbnails.index}"
		                                                        class="{if $smarty.foreach.thumbnails.index==0}active {/if}gallery-link js-gallery-link"
		                                                        href="{$link->getImageLink($product->link_rewrite, $imageIds, 'thickbox_default')|escape:'html':'UTF-8'}"
		                                                        data-img-src="{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}"
		                                                        title="{$imageTitle}">
                                                            <img class="img-responsive" id="thumb_{$image.id_image}" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'cart_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}"{if isset($cartSize)} height="{$cartSize.height}" width="{$cartSize.width}"{/if} itemprop="image" />
                                                        </a>
                                                    </div>
                                                {/foreach}
                                            {/if}
                                </div>
                            </div>

                        <!-- end thumbnails -->

                    {/if}
                    <!-- description_short -->
                    {if isset($product->description_short)}
                        <div class="product_description_short">
                            {$product->description_short}
                        </div>
                    {/if}
                    <!-- description_short -->
				</div>

				<div class="col-md-2 right_column">
					<div class="product_head">
						<h1>{' '|implode:[$product->name, $product->reference]|escape:'html':'UTF-8'}</h1>
						<div class="price_block">
							<div class="price inline-block" id="our_price_display">{convertPrice price=$productPrice|floatval}</div>
                            {if $productPriceWithoutReduction > $productPrice}<div class="old_price inline-block">{convertPrice price=$productPriceWithoutReduction|floatval}</div>{/if}
                            {if $product->specificPrice.reduction_type == 'percentage' && $product->specificPrice.reduction * 100 >= Configuration::get('PS_PRODUCT_DISCOUNT')|intval}
                                <span class="discount-percentage"><span>-{(($product->specificPrice.reduction * 100)/5)|ceil*5}%</span></span>
                            {/if}
						</div>
                        {hook h="extraRight" mod="loyalty"}
					</div>

                    {*<p id="availability_statut"{if !$PS_STOCK_MANAGEMENT || ($product->quantity <= 0 && !$product->available_later && $allow_oosp) || ($product->quantity > 0 && !$product->available_now) || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none;"{/if}>*}
                        {*<span id="availability_label">{l s='Availability:'}</span>*}
                        {*<span id="availability_value" class="label{if $product->quantity <= 0 && !$allow_oosp} label-danger{elseif $product->quantity <= 0} label-warning{else} label-success{/if}">{if $product->quantity <= 0}{if $PS_STOCK_MANAGEMENT && $allow_oosp}{$product->available_later}{else}{l s='This product is no longer in stock'}{/if}{elseif $PS_STOCK_MANAGEMENT}{$product->available_now}{/if}</span>*}
                        {*<br/>*}
                        {*<br/>*}
                    {*</p>*}

                    {*{if $PS_STOCK_MANAGEMENT}*}
                        {*{if !$product->is_virtual}{hook h="displayProductDeliveryTime" product=$product}{/if}*}
                        {*<p class="warning_inline" id="last_quantities"{if ($product->quantity > $last_qties || $product->quantity <= 0) || $allow_oosp || !$product->available_for_order || $PS_CATALOG_MODE} style="display: none"{/if} >{l s='Warning: Last items in stock!'}</p>*}
                        {*<br/>*}
                    {*{/if}*}
                    {*<p id="availability_date"{if ($product->quantity > 0) || !$product->available_for_order || $PS_CATALOG_MODE || !isset($product->available_date) || $product->available_date < $smarty.now|date_format:'%Y-%m-%d'} style="display: none;"{/if}>*}
                        {*<span id="availability_date_label">{l s='Availability date:'}</span>*}
                        {*<span id="availability_date_value">{if Validate::isDate($product->available_date)}{dateFormat date=$product->available_date full=false}{/if}</span>*}
                        {*<br/>*}
                    {*</p>*}
                    <!-- Out of stock hook -->
                    <div id="oosHook"{if $product->quantity > 0} style="display: none;"{/if}>
                        {$HOOK_PRODUCT_OOS}
                    </div>

                    <form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
                        <!-- hidden datas -->
                        <p class="hidden">
                            <input type="hidden" name="token" value="{$static_token}" />
                            <input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
                            <input type="hidden" name="add" value="1" />
                            <input type="hidden" name="id_product_attribute" id="idCombination" value="" />
                        </p>
                                <!-- quantity wanted -->
                                <!-- minimal quantity wanted -->
                                {if isset($groups)}
                                    <!-- attributes -->
                                    <div id="attributes">
                                        <div class="clearfix"></div>
                                        {foreach from=$groups key=id_attribute_group item=group}
                                            {if $group.attributes|@count}
                                                <fieldset class="attribute_fieldset">
                                                    {*<label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'}&nbsp;</label>*}
                                                    <div class="title-md">{$group.name|escape:'html':'UTF-8'}:</div>
                                                    {assign var="groupName" value="group_$id_attribute_group"}
                                                    <div class="attribute_list">
                                                        {if ($group.group_type == 'select')}
                                                            <select name="{$groupName}" id="group_{$id_attribute_group|intval}" class="attribute_select no-print">
                                                                {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                                    <option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
                                                                {/foreach}
                                                            </select>
                                                        {elseif ($group.group_type == 'color')}
                                                            <ul id="color_to_pick_list" class="clearfix">
                                                                {assign var="default_colorpicker" value=""}
                                                                {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                                    {assign var='img_color_exists' value=file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                                    <li{if $group.default == $id_attribute} class="selected"{/if}>
                                                                        <a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" class="color_pick{if ($group.default == $id_attribute)} selected{/if}"{if !$img_color_exists && isset($colors.$id_attribute.value) && $colors.$id_attribute.value} style="background:{$colors.$id_attribute.value|escape:'html':'UTF-8'};"{/if} title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
                                                                            {if $img_color_exists}
                                                                                <img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
                                                                            {/if}
                                                                        </a>
                                                                    </li>
                                                                    {if ($group.default == $id_attribute)}
                                                                        {$default_colorpicker = $id_attribute}
                                                                    {/if}
                                                                {/foreach}
                                                            </ul>
                                                            <input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
                                                        {elseif ($group.group_type == 'radio')}
                                                            <div class="sizes_block">
                                                                <ul class="clearfix">
                                                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                                        <li{if ($group.default == $id_attribute)} class="active"{/if} >
                                                                            <input type="radio" class="attribute_radio none" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if} />
                                                                            <a href="#">{$group_attribute|escape:'html':'UTF-8'}</a>
                                                                        </li>
                                                                    {/foreach}
                                                                </ul>
                                                            </div>
                                                        {/if}
                                                    </div> <!-- end attribute_list -->
                                                </fieldset>
                                            {/if}
                                        {/foreach}
                                            <!-- Size - info -->
                                        <div>
                                        <a class="size_info_link"  href="#size-info" title="информация о размерах">Як дізнатися свій розмір<span class="arrows">→</span></a>
                                        </div>
                                        <div style="display:none">
                                            <div id="size-info">
                                                <div class="help_inner_content">
                                                    <h1>Определение длины стопы</h1>
                                                    <div><img src="/img/foot_img.png" alt="Определение длины стопы" width="699" height="383" />
                                                        <p class="a"><br />Для определения длины стопы измерьте расстояние между самыми удаленными точками на&nbsp;чертеже (точка&nbsp;А и&nbsp;точка&nbsp;Б), как показано на&nbsp;рисунке. Измерьте обе ноги и&nbsp;выберите большую длину. Округлите полученный результат до&nbsp;5&nbsp;мм и&nbsp;найдите свой размер в&nbsp;таблице.</p>
                                                        <h3>Размер мужской обуви</h3>
                                                        <table>
                                                            <thead>
                                                            <tr class="helpSizeTitle">
                                                                <td class="w110"><em><strong>Система</strong></em></td>
                                                                <td colspan="15"><em><strong>Размер</strong></em></td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td class="first_td"><strong>ISO 3355-77 (см)</strong></td>
                                                                <td><strong>25</strong></td>
                                                                <td><strong>25,5</strong></td>
                                                                <td><strong>26</strong></td>
                                                                <td><strong>26,5</strong></td>
                                                                <td><strong>27</strong></td>
                                                                <td><strong>27,5</strong></td>
                                                                <td><strong>28</strong></td>
                                                                <td><strong>28,5</strong></td>
                                                                <td><strong>29</strong></td>
                                                                <td><strong>29,5</strong></td>
                                                                <td><strong>30</strong></td>
                                                                <td><strong>30,5</strong></td>
                                                                <td><strong>31</strong></td>
                                                                <td><strong>31,5</strong></td>
                                                                <td> </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>Европа</strong></td>
                                                                <td>39</td>
                                                                <td>39,5</td>
                                                                <td>40</td>
                                                                <td>40,5</td>
                                                                <td>41</td>
                                                                <td>41,5</td>
                                                                <td>42</td>
                                                                <td>42,5</td>
                                                                <td>43</td>
                                                                <td>43,5</td>
                                                                <td>44</td>
                                                                <td>44,5</td>
                                                                <td>45</td>
                                                                <td>45,5</td>
                                                                <td> </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>Англия</strong></td>
                                                                <td>5</td>
                                                                <td>5,5</td>
                                                                <td>6</td>
                                                                <td>6,5</td>
                                                                <td>7</td>
                                                                <td>7,5</td>
                                                                <td>8</td>
                                                                <td>8,5</td>
                                                                <td>9</td>
                                                                <td>9,5</td>
                                                                <td>10</td>
                                                                <td>10,5</td>
                                                                <td>11</td>
                                                                <td>11,5</td>
                                                                <td> </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>США</strong></td>
                                                                <td>6</td>
                                                                <td>6,5</td>
                                                                <td>7</td>
                                                                <td>7,5</td>
                                                                <td>8</td>
                                                                <td>8,5</td>
                                                                <td>9</td>
                                                                <td>9,5</td>
                                                                <td>10</td>
                                                                <td>10,5</td>
                                                                <td>11</td>
                                                                <td>11,5</td>
                                                                <td>12</td>
                                                                <td class="w35">12,5</td>
                                                                <td class="w35"> </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div>
                                                        <h3><br />Размер женской обуви</h3>
                                                        <table>
                                                            <thead>
                                                            <tr class="helpSizeTitle">
                                                                <td class="w110"><em><strong>Система</strong></em></td>
                                                                <td colspan="14"><em><strong>Размер</strong></em></td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td class="first_td"><strong>ISO 3355-77 (см)</strong></td>
                                                                <td><strong>21</strong></td>
                                                                <td><strong>21,5</strong></td>
                                                                <td><strong>22</strong></td>
                                                                <td><strong>22,5</strong></td>
                                                                <td><strong>23</strong></td>
                                                                <td><strong>23,5</strong></td>
                                                                <td><strong>24</strong></td>
                                                                <td><strong>24,5</strong></td>
                                                                <td><strong>25</strong></td>
                                                                <td><strong>25,5</strong></td>
                                                                <td><strong>26</strong></td>
                                                                <td><strong>26,5</strong></td>
                                                                <td><strong>27</strong></td>
                                                                <td><strong>27,5</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>Европа</strong></td>
                                                                <td>35</td>
                                                                <td>35,5</td>
                                                                <td>36</td>
                                                                <td>36,5</td>
                                                                <td>37</td>
                                                                <td>37,5</td>
                                                                <td>38</td>
                                                                <td>38,5</td>
                                                                <td>39</td>
                                                                <td>39,5</td>
                                                                <td>40</td>
                                                                <td>40,5</td>
                                                                <td>41</td>
                                                                <td>41,5</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>Англия</strong></td>
                                                                <td>2</td>
                                                                <td>2,5</td>
                                                                <td>3</td>
                                                                <td>3,5</td>
                                                                <td>4</td>
                                                                <td>4,5</td>
                                                                <td>5</td>
                                                                <td>5,5</td>
                                                                <td>6</td>
                                                                <td>6,5</td>
                                                                <td>7</td>
                                                                <td>7,5</td>
                                                                <td>8</td>
                                                                <td>8,5</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>США</strong></td>
                                                                <td>5</td>
                                                                <td>5.5</td>
                                                                <td>6</td>
                                                                <td>6.5</td>
                                                                <td>7</td>
                                                                <td>7.5</td>
                                                                <td>8</td>
                                                                <td>8.5</td>
                                                                <td>9</td>
                                                                <td>9.5</td>
                                                                <td>10</td>
                                                                <td>10.5</td>
                                                                <td>11</td>
                                                                <td class="w35">11,5</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <p> </p>
                                                        <h3>Размер детской обуви</h3>
                                                        <table>
                                                            <thead>
                                                            <tr class="helpSizeTitle">
                                                                <td class="w110"><em><strong>Система</strong></em></td>
                                                                <td colspan="16"><em><strong>Размер</strong></em></td>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td class="first_td"><strong>ISO 3355-77 (см)</strong></td>
                                                                <td><strong>12</strong></td>
                                                                <td><strong>12,5</strong></td>
                                                                <td><strong>13</strong></td>
                                                                <td><strong>13,5</strong></td>
                                                                <td><strong>14</strong></td>
                                                                <td><strong>14,5</strong></td>
                                                                <td><strong>15</strong></td>
                                                                <td><strong>15,5</strong></td>
                                                                <td><strong>16</strong></td>
                                                                <td><strong>16,5</strong></td>
                                                                <td><strong>17</strong></td>
                                                                <td><strong>17,5</strong></td>
                                                                <td><strong>18</strong></td>
                                                                <td><strong>18,5</strong></td>
                                                                <td><strong>19</strong></td>
                                                                <td><strong>19,5</strong></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>Европа</strong></td>
                                                                <td>18</td>
                                                                <td>19</td>
                                                                <td>20</td>
                                                                <td>21</td>
                                                                <td>22</td>
                                                                <td>23</td>
                                                                <td>24</td>
                                                                <td>25</td>
                                                                <td>26</td>
                                                                <td>27</td>
                                                                <td>28</td>
                                                                <td>29</td>
                                                                <td>30</td>
                                                                <td>31</td>
                                                                <td>32</td>
                                                                <td>33</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>Англия</strong></td>
                                                                <td>2</td>
                                                                <td>3</td>
                                                                <td>4</td>
                                                                <td>4,5</td>
                                                                <td>5</td>
                                                                <td>6</td>
                                                                <td>7</td>
                                                                <td>8</td>
                                                                <td>8,5</td>
                                                                <td>9</td>
                                                                <td>10</td>
                                                                <td>11</td>
                                                                <td>11,5</td>
                                                                <td>12</td>
                                                                <td>13</td>
                                                                <td>13,5</td>
                                                            </tr>
                                                            <tr>
                                                                <td class="first_td"><strong>США</strong></td>
                                                                <td>1</td>
                                                                <td>1,5</td>
                                                                <td>3</td>
                                                                <td>4</td>
                                                                <td>5</td>
                                                                <td>5,5</td>
                                                                <td>6</td>
                                                                <td>7</td>
                                                                <td>8</td>
                                                                <td>9</td>
                                                                <td>9,5</td>
                                                                <td>10</td>
                                                                <td>11</td>
                                                                <td class="w35">12</td>
                                                                <td>12,5</td>
                                                                <td>13</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <p> </p>
                                                    </div>
                                                    <p class="mt15">Нужна помощь?<br />Вы&nbsp;всегда сможете связаться с&nbsp;нашей <a class="customerServiceBtn" href="/contact-us/">клиентской службой</a></p>
                                                </div>
                                            </div>
                                        </div>
                                            <!-- End Size - info -->
                                    </div> <!-- end attributes -->
                                {/if}
                                {*<div{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE} class="unvisible"{/if}>*}
                        <div>
                            <div id="add_to_cart" class="no-print">
                                {if !$PS_CATALOG_MODE && $product->available_for_order && ($product->allow_oosp || $product->quantity > 0 || $product->quantity_all_versions && $product->quantity_all_versions > 0 || (isset($restricted_country_mode) && !$restricted_country_mode))}
                                    {if $in_cart}
                                        <button type="submit" class="btn-lg button btn-outline order_button added">
                                            <span>{l s='У кошику'}</span>
                                        </button>
                                    {else}
                                        <button type="submit" class="btn-lg button2 order_button">
                                            <span data-inbasket-translate="{l s='Купити зараз'}">{l s='Купити зараз'}</span>
                                        </button>
                                    {/if}
                                    <div id="quantity_wanted_p" class="none">
                                        <input type="text" min="1" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
                                    </div>
                                {/if}
                            </div>
                            {if $product->quantity > 0}
                            <div>
                                {hook h="displayProductButtons" mod="tmoneclickorder"}
                            </div>
                            {/if}
                        </div>
                    </form>
					{*<div class="order_button">*}
						{*<div class="m-b-sm">*}
                            {*<a href="#" class="btn-lg" id="buy_product" data-inbasket-translate="У кошику">Замовити</a>*}
                        {*</div>*}
					{*</div>*}

                    <div class="clearfix"></div>
                    {if isset($features) && $features}
                    <div class="attr_block m-b-lg">
                        <div class="title-md">{l s='Деталi:'}</div>
                        <div class="attr_list">
                            <div class="row">
                                <div class="col-md-1">{l s='Артикул:'}</div><div class="col-md-1">{$product->reference|escape:'html':'UTF-8'}</div>
                            </div>
                            {foreach from=$features item=feature}
                                    {if isset($feature.value) && $feature.name!='Стиль'}
                                        <div class="row">
                                            <div class="col-md-1">{$feature.name|escape:'html':'UTF-8'}:</div><div class="col-md-1">{$feature.value|escape:'html':'UTF-8'}</div>
                                        </div>
                                    {/if}
                            {/foreach}
                        </div>
                    </div>
                    {/if}
                    {hook h="displayLabelBlock" mod="blocklabelprivatbank"}
				</div>
			</div>

            {if isset($HOOK_PRODUCT_FOOTER) && $HOOK_PRODUCT_FOOTER}{$HOOK_PRODUCT_FOOTER}{/if}
    <!-- description -->
    {if isset($product->description)}
        <div class="product_description is_post">
            {$product->description}
        </div>
    {/if}
    <!-- end description -->
        <div class="clearfix"></div>





</div>
<div class="product_popup" id="product_popup">
    <div class="popup_wrapper">
        <a href="#" class="popup_closer"></a>

        <div class="popup_content popup-slider">

        </div>
    </div>
</div>
{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
	{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
	{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
{addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
{addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
{addJsDef attribute_anchor_separator=$attribute_anchor_separator|escape:'quotes':'UTF-8'}
{addJsDef attributesCombinations=$attributesCombinations}
{addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
{if isset($combinations) && $combinations}
	{addJsDef combinations=$combinations}
	{addJsDef combinationsFromController=$combinations}
	{addJsDef displayDiscountPrice=$display_discount_price}
	{addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
{/if}
{if isset($combinationImages) && $combinationImages}
	{addJsDef combinationImages=$combinationImages}
{/if}
{addJsDef customizationId=$id_customization}
{addJsDef customizationFields=$customizationFields}
{addJsDef default_eco_tax=$product->ecotax|floatval}
{addJsDef displayPrice=$priceDisplay|intval}
{addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
{if isset($cover.id_image_only)}
	{addJsDef idDefaultImage=$cover.id_image_only|intval}
{else}
	{addJsDef idDefaultImage=0}
{/if}
{addJsDef img_ps_dir=$img_ps_dir}
{addJsDef img_prod_dir=$img_prod_dir}
{addJsDef id_product=$product->id|intval}
{addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
{addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
{addJsDef minimalQuantity=$product->minimal_quantity|intval}
{addJsDef noTaxForThisProduct=$no_tax|boolval}
{if isset($customer_group_without_tax)}
	{addJsDef customerGroupWithoutTax=$customer_group_without_tax|boolval}
{else}
	{addJsDef customerGroupWithoutTax=false}
{/if}
{if isset($group_reduction)}
	{addJsDef groupReduction=$group_reduction|floatval}
{else}
	{addJsDef groupReduction=false}
{/if}
{addJsDef oosHookJsCodeFunctions=Array()}
{addJsDef productHasAttributes=isset($groups)|boolval}
{addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
{addJsDef productPriceTaxIncluded=($product->getPriceWithoutReduct(false)|default:'null' - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcluded=($product->getPrice(false, null, 6, null, false, false) - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcl=($product->getPrice(false, null, 6, null, false, false)|floatval)}
{addJsDef productBasePriceTaxIncl=($product->getPrice(true, null, 6, null, false, false)|floatval)}
{addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
{addJsDef productAvailableForOrder=$product->available_for_order|boolval}
{addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
{addJsDef productPrice=$productPrice|floatval}
{addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
{addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
{addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
{if $product->specificPrice && $product->specificPrice|@count}
	{addJsDef product_specific_price=$product->specificPrice}
{else}
	{addJsDef product_specific_price=array()}
{/if}
{if $display_qties == 1 && $product->quantity}
	{addJsDef quantityAvailable=$product->quantity}
{else}
	{addJsDef quantityAvailable=0}
{/if}
{addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
	{addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
{else}
	{addJsDef reduction_percent=0}
{/if}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
	{addJsDef reduction_price=$product->specificPrice.reduction|floatval}
{else}
	{addJsDef reduction_price=0}
{/if}
{if $product->specificPrice && $product->specificPrice.price}
	{addJsDef specific_price=$product->specificPrice.price|floatval}
{else}
	{addJsDef specific_price=0}
{/if}
{addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
{addJsDef stock_management=$PS_STOCK_MANAGEMENT|intval}
{addJsDef taxRate=$tax_rate|floatval}
{addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
{addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
{addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
{addJsDefL name='product_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='product_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
{/strip}
{/if}
