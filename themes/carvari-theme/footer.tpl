{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $page_name != 'index'}
    </div> {*//.container *}
{/if}
</div> {*//.content_wrapper *}

{if $page_name == 'order-opc' and !$mobile_device}
    {*Футер для order-opc*}
{else}
{if !isset($content_only) || !$content_only}
    <div class="footer">
        <div class="footer_row">
            <div class="footer_info">
                <a href="{$base_dir}" class="footer_logo">
                    <span class="footer_logo__brand">L’CARVARI</span>
                    <span class="footer_logo__year">{$smarty.now|date_format:'Y'}</span>
                </a>
                <div class="footer_slogan">
                    {l s='ДЕЛАЕМ ЭСТЕТИКУ МОДНОЙ'}
                </div>
                {hook h='displaySocials' mod='blocksocial'}
                <ul class="work_time">
                    <li class="work_time__item">
                        {l s='Будні - з 9:00 по 19:00'}
                    </li>
                    <li class="work_time__item">
                        {l s='Субота - з 10:00 по 18:00'}
                    </li>
                    <li class="work_time__item">
                        {l s='Неділя - вихідний'}
                    </li>
                </ul>
                {if $footerPhone && $footerPhone|count}
                    <div class="footer_phone">
                        <span class="footer__icon footer__icon--phone"></span>
                        {$footerPhone}
                    </div>
                {/if}
            </div>
            <div class="footer_menu">
                <div class="footer_menu__title">
                    {l s='КАТАЛОГ'}
                </div>
                <ul class="footer_menu__list">
                    <li>
                        <a href="/zhinkam-2952/" class="footer_menu__item">{l s='Жiнкам'}</a>
                    </li>
                    <li>
                        <a href="/cholovikam-2977/" class="footer_menu__item">{l s='Чоловiкам'}</a>
                    </li>
                    <li>
                        <a href="/actions-3041/" class="footer_menu__item">{l s='Акції'}</a>
                    </li>
                    <li>
                        <a href="/newstuff-3042/" class="footer_menu__item">{l s='Новинки' }</a>
                    </li>
                    <li>
                        <a href="/blog/" class="footer_menu__item">{l s='Блог'}</a>
                    </li>
                </ul>
            </div>
            <div class="footer_menu">
                <div class="footer_menu__title">
                    {l s='КЛІЄНТУ'}
                </div>
                    {hook h='displayFooter' mod='ffcarvarimenu'}
                {*<ul class="footer_menu__list">*}
                    {*<li>*}
                        {*<a href="/stores/" class="footer_menu__item">{l s='Контакти'}</a>*}
                    {*</li>*}
                    {*<li>*}
                        {*<a href="/content/pro-magazin-6/" class="footer_menu__item">{l s='Про магазин'}</a>*}
                    {*</li>*}
                    {*<li>*}
                        {*<a href="/content/oplata-7/" class="footer_menu__item">{l s='Оплата'}</a>*}
                    {*</li>*}
                    {*<li>*}
                        {*<a href="/content/garantiya-8/" class="footer_menu__item">{l s='Гарантія'}</a>*}
                    {*</li>*}
                    {*<li>*}
                        {*<a href="/content/delivery-1/" class="footer_menu__item">{l s='Доставка'}</a>*}
                    {*</li>*}
                    {*<li>*}
                        {*<a href="/content/obmin-ta-povernennya-9/" class="footer_menu__item">{l s='Обмін та повернення'}</a>*}
                    {*</li>*}
                {*</ul>*}
            </div>
            <div class="footer_menu">
                <div class="footer_menu__title">
                    {l s='КАБІНЕТ'}
                </div>
                <ul class="footer_menu__list">
                    <li>
                        <a href="/authentication/" class="footer_menu__item">{l s='Вхід в кабінет'}</a>
                    </li>
                    <li>
                        <a href="/quick-order/" class="footer_menu__item">{l s='Кошик'}</a>
                    </li>
                    <li>
                        <a href="/content/club-lcarvari-15/" class="footer_menu__item">{l s='Клуб L`Carvari'}</a>
                    </li>
                </ul>
            </div>
            <div class="footer_subscribe">
                {hook h="displaySubscribeForm" from='blocknewsletter'}
                <div class="payments">
                    <div class="payments_title">
                        <span class="footer__icon footer__icon--lock"></span> {l s='Гарантия безопасной покупки'}
                    </div>
                    <ul class="payments_list">
                        <li class="payments_list__item">
                            <img src="{$tpl_uri}img/icon/payments/visa.png" alt="visa">
                        </li>
                        <li class="payments_list__item">
                            <img src="{$tpl_uri}img/icon/payments/mastercard.png" alt="mastercard">
                        </li>
                        <li class="payments_list__item">
                            <img src="{$tpl_uri}img/icon/payments/privat24.png" alt="privat24">
                        </li>
                        <li class="payments_list__item">
                            <img src="{$tpl_uri}img/icon/payments/liqpay.png" alt="liqpay">
                        </li>
                        <li class="payments_list__item">
                            <img src="{$tpl_uri}img/icon/payments/np.png" alt="np">
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <hr class="footer_row footer_row--hr">
        <div class="footer_row footer_row--around">
            <div class="footer_copyright footer_copyright--left">
                <p>© 2009 - {$smarty.now|date_format:'Y'} L'CARVARI</p>
                <div class="forforce">
                    {l s='зроблено в'}
                    <a href="http://forforce.com/" target="_blank"></a>
                </div>
            </div>
            <div class="footer_copyright footer_copyright--right">
                <div class="copyright">
                    <p class="copyright__text">
                        {l s='Мережа фірмових магазинів взуття та аксесуарів. Всі права на матеріали, розміщені на сайті, охороняються відповідно до законодавства України Матеріали не можуть бути використані без письмового дозволу'}
                    </p>
                    <p class="copyright__author">{l s='ФОП Подопригора.'}</p>
                </div>
            </div>
        </div>
    </div>
    </div><!-- #page -->
{/if}
{/if}
{include file="$tpl_dir./global.tpl"}
<!-- Products remarketing -->
{if $page_name != 'product'}
    {hook h='displayFooterRemarketing' mod='ffproductremarketing'}
{/if}
<!-- End Products remarketing -->
{hook h="displaypokupon" mod='ffpokupon'}
</body>
</html>
