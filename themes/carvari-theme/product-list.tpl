{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $mobile_device}
	{if !$more_products}
		<ul class="product-list">
	{/if}
			{if isset($products) && $products}
				<!-- Products list -->
				{foreach from=$products item=product name=products}
                    {if !$PS_CATALOG_MODE && $product.available_for_order && ($product.quantity > 0 || $product.quantity_all_versions && $product.quantity_all_versions > 0 || $product.allow_oosp)}
						<li>
							<a href="{$product.link|escape:'html':'UTF-8'}">
								<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'large_default')|escape:'html':'UTF-8'}" alt="">
								<div class="descr">
									<span class="title">{' '|implode:[$product.name, $product.reference]|escape:'html':'UTF-8'}</span>
									<span class="new_price">
								{if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
										<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
							</span>
                                    {if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                                        {*{hook h="displayProductPriceBlock" product=$product type="old_price"}*}
										<span class="old_price">
													{displayWtPrice p=$product.price_without_reduction}
												</span>
                                    {/if}
                                    {if $product.specific_prices.reduction_type == 'percentage' && $product.specific_prices.reduction * 100 >= Configuration::get('PS_PRODUCT_DISCOUNT')|intval}
										<span class="discount-percentage"><span>-{(($product.specific_prices.reduction * 100)/5)|ceil*5}%</span></span>
                                    {/if}
								</div>
							</a>
						</li>
                    {/if}
				{/foreach}
			{/if}
	{if !$more_products}
	</ul>
	{/if}
{else}

	<ul class="items product-list">
		{if isset($products) && $products}
			<!-- Products list -->
			{foreach from=$products item=product name=products}

                {if !$PS_CATALOG_MODE && $product.available_for_order && ($product.quantity > 0 || $product.quantity_all_versions && $product.quantity_all_versions > 0 || $product.allow_oosp)}
					<li class="product-item js--product-item" data-id_product="{$product.id_product}">
						{assign var='productImgs' value=Product::getProductImages($product.id_product)}
						<ul class="product-item_list">
							{foreach from=$productImgs item=productImg name=productImgs}
								{assign var=imageIds value="`$product.id_product`-`$productImg.id_image`"}
								<li class="product-item_list__item js--product-item_list__item {if $smarty.foreach.productImgs.iteration ==2}show{/if}">

									<img class="product-item_list__img js--product-item_list__img" data-number="{$smarty.foreach.productImgs.iteration}" src="{$link->getImageLink($product.link_rewrite, $imageIds, 'home_default')}" />
								</li>
							{/foreach}
						</ul>
						<a href="{$product.link|escape:'html':'UTF-8'}" class="product-item__link item">
							<img class="product-item__img_big js--product-item__img_big" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" data-start_images="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="">
							<span class="name"
								  title="{' '|implode:[$product.name, $product.reference]|escape:'html':'UTF-8'}">
							{' '|implode:[$product.name, $product.reference]|escape:'html':'UTF-8'}
						</span>
							<span class="item_price">
							<span class="price">
								{*{hook h="displayProductPriceBlock" product=$product type="before_price"}*}
                                {if !$priceDisplay}{convertPrice price=$product.price}{else}{convertPrice price=$product.price_tax_exc}{/if}
								<meta itemprop="priceCurrency" content="{$currency->iso_code}" />
							</span>
                                {if isset($product.specific_prices) && $product.specific_prices && isset($product.specific_prices.reduction) && $product.specific_prices.reduction > 0}
                                    {*{hook h="displayProductPriceBlock" product=$product type="old_price"}*}
									<span class="old_price">
									{displayWtPrice p=$product.price_without_reduction}
								</span>
                                {/if}
                                {if $product.specific_prices.reduction_type == 'percentage' && $product.specific_prices.reduction * 100 >= Configuration::get('PS_PRODUCT_DISCOUNT')|intval}
									{*<span class=""><span>-{$product.specific_prices.reduction * 100}%</span></span>*}
									<span class="discount-percentage"><span>-{(($product.specific_prices.reduction * 100)/5)|ceil*5}%</span></span>
                                {/if}
						</span>
						</a>
					</li>

                {/if}
			{/foreach}
		{else}
			<li>
				<p class="warning">{l s='There are no products.' mod='blocklayered'}</p>
			</li>
		{/if}
	</ul>
{/if}