<!DOCTYPE html>
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if} xmlns="http://www.w3.org/1999/xhtml">
<head>
    {hook h='googletagmanagerafterheader' mod='ffgoogletagmanager'}
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>{$meta_title|escape:'html':'UTF-8'}</title>
    {if isset($meta_description) AND $meta_description}
        <meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
    {/if}
    {if isset($meta_keywords) AND $meta_keywords}
        <meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
    {/if}
    <meta name="generator" content="Carvarishop" />
    <meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
    <!--        <meta content="width=640, initial-scale=0.5, user-scalable=no" name="viewport">-->
    <!--        <meta content="width=640, initial-scale=1, user-scalable=no" name="viewport">-->
    <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no" />


    <link rel="icon" href="/favicon.ico?{$img_update_time}" type="image/x-icon" />
    <link rel="shortcut icon" href="/favicon.ico?{$img_update_time}" type="image/x-icon" />

    {if isset($css_files)}
        {foreach from=$css_files key=css_uri item=media}
            <link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
        {/foreach}
    {/if}
    <link rel="stylesheet" href="/themes/carvari-theme/mobile/css/main.css">
    <script type="text/javascript" src="/themes/carvari-theme/js/EventEmitter.js"></script>
    {if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
        {$js_def}
        {foreach from=$js_files item=js_uri}
            <script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
        {/foreach}
    {/if}
    {$HOOK_HEADER}
    <script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/74657f20a4ad1b3b1bb68a180bec9ab4_1.js" async></script>
</head>
<body {if isset($page_name)}class="page_{$page_name}"{/if}>
{hook h='googletagmanagerafterbody' mod='ffgoogletagmanager'}
<!-- Google Tag Manager -->
{hook h='displayEcommerceAnalytics' mod='ffecommerceanalytics'}
<!-- End Google Tag Manager -->
<!-- Products remarketing -->
{hook h='displayProductRemarketing' mod='ffproductremarketing'}
<!-- End Products remarketing -->
<div class="body_wrapper">
    <div class="content_wrapper">
    <svg xmlns="http://www.w3.org/2000/svg" style="display:none">
        <symbol id="dropdown_arrow" viewBox="0 0 18 10">
            <polyline fill="none" stroke-width="3" stroke-miterlimit="10" points="1,1 9,9 17,1 "/>
        </symbol>
        <symbol id="go_back_icon" viewBox="0 0 21.576 33.25">
            <line stroke-width="3" stroke-miterlimit="10" x1="1375.11" y1="203.956" x2="1360" y2="219.066"/>
            <line stroke-width="3" stroke-miterlimit="10" x1="1376.115" y1="232.961" x2="1360" y2="216.845"/>
            <line fill="none" stroke="#373636" stroke-width="3" stroke-miterlimit="10" x1="15.11" y1="1.956" x2="0" y2="17.067"/>
            <line fill="none" stroke="#373636" stroke-width="3" stroke-miterlimit="10" x1="16.115" y1="30.961" x2="0" y2="14.845"/>
        </symbol>
        <symbol id="cart_icon" viewBox="0 0 29.833 33">
            <g>
                <path stroke="none" d="M26.538,9.299c-0.78,3.733-2.704,13.2-2.704,15.701c0,0.806,0.516,4.111,0.822,6H5.012
		c0.306-1.889,0.822-5.194,0.822-6c0-2.501-1.924-11.968-2.704-15.701C5.896,9.872,11.736,11,14.834,11S23.772,9.872,26.538,9.299
		 M27.834,8c0,0-9,2-13,2s-13-2-13-2s3,14,3,17c0,1-1,7-1,7h22c0,0-1-6-1-7C24.834,22,27.834,8,27.834,8L27.834,8z"/>
            </g>
            <path fill="none" stroke-linecap="square" stroke-miterlimit="10" d="M9.834,14l-1-5c0,0-1-7,6-7s6,7,6,7l-1,5"/>
            <circle stroke-linecap="square" stroke-miterlimit="10" cx="9.834" cy="14" r="1"/>
            <circle stroke-linecap="square" stroke-miterlimit="10" cx="19.834" cy="14" r="1"/>
        </symbol>
        <symbol id="subscribe_icon" viewBox="0 0 33 33">
            <rect x="0" y="0" width="33" height="33"/>
            <polyline fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" points="9,18 14,22 23,9 "/>
        </symbol>
        <symbol id="close_icon" viewBox="0 0 11 11">
            <path d="M0.881,0l4.28,4.381L9.442,0l0.88,0.88L6.021,5.261l4.301,4.38l-0.88,0.881L5.161,6.142l-4.28,4.381
		L0,9.642l4.281-4.38L0,0.88L0.881,0z"/>
        </symbol>

    </svg>
        {if $page_name == 'order-opc'}
            <div class="header header--short" id="header" >
                <div class="padded_block">
                    <a href="{if $active_url_lang}{$active_url_lang}{/if}/" class="header__logo">
                        <img src="{$tpl_uri}img/carvari_logo.svg" alt="">
                    </a>
                    <strong class="header__phone">
                        0 800 211 009
                    </strong>
                </div>
            </div>
        {else}
            <div class="header" {if $page_name != 'index'} id="header" {/if} >
                <div class="logo_holder">
                    {if $page_name == 'index'}
                        <a class="logo" href="{if $active_url_lang}{$active_url_lang}{/if}/"></a>
                    {/if}
                </div>
                {if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
                {hook h='displayMobileTop' mod='blockcart'}
            </div>
        {/if}
    <div class="dropdown_menu_wrapper">
        <div class="dropdown_menu" style="display: none;">
            <ul class="dd_content">

            </ul>
            <div class="select_lang">{hook h="displayLangTop" mod='blocklanguages'}</div>

        </div>
        <div class="mobile_height_spike"></div>
    </div>
{$template}
    </div>
{if $page_name != 'order-opc'}
    <div class="footer">
        <div class="footer_row">
            <div class="footer_info">
                <a href="/" class="footer_logo">
                    <span class="footer_logo__brand">L’CARVARI</span>
                    <span class="footer_logo__year">{$smarty.now|date_format:'Y'}</span>
                </a>
                <div class="footer_slogan">
                    {l s='ДЕЛАЕМ ЭСТЕТИКУ МОДНОЙ'}
                </div>
                {hook h='displaySocials' mod='blocksocial'}
                <ul class="work_time">
                    <li class="work_time__item">
                        {l s='Будні - з 9:00 по 19:00'}
                    </li>
                    <li class="work_time__item">
                        {l s='Субота - з 10:00 по 18:00'}
                    </li>
                    <li class="work_time__item">
                        {l s='Неділя - вихідний'}
                    </li>
                </ul>
                {if $footerPhone && $footerPhone|count}
                    <div class="footer_phone">
                        <span class="footer__icon footer__icon--phone"></span> {$footerPhone}
                    </div>
                {/if}
            </div>
        </div>
        <hr class="footer_row footer_row--hr">
        <div class="footer_row footer_row--column">
            <div class="footer_copyright">
                <p>© 2009 - {$smarty.now|date_format:'Y'} L'CARVARI</p>
            </div>
            <div class="footer_copyright">
                <div class="copyright">
                    <p class="copyright__text">
                        {l s='Мережа фірмових магазинів взуття та аксесуарів. Всі права на матеріали, розміщені на сайті, охороняються відповідно до законодавства України Матеріали не можуть бути використані без письмового дозволу'}
                    </p>
                    <p class="copyright__author">{l s='ФОП Подопригора.'}</p>
                </div>

                <div class="forforce">
                    {l s='зроблено в'}
                    <a href="http://forforce.com/" target="_blank"></a>
                </div>
            </div>
        </div>
    </div>
{/if}
</div>

<div class="slide_to_top hidden">
    <i class="icon">
        <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dropdown_arrow">
            </use></svg>
    </i>
</div>
{include file="$tpl_dir./global.tpl"}
<!-- Products remarketing -->
{if $page_name != 'product'}
    {hook h='displayFooterRemarketing' mod='ffproductremarketing'}
{/if}
<!-- End Products remarketing -->
{hook h="displaypokupon" mod='ffpokupon'}
</body>
</html>
