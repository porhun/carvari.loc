{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="main_content checkout-page">
	<h1>{l s='Увійти в аккаунт'}</h1>
	<div class="padded_block">
		<div class="tab_wrapper">
			<div class="tab_content">
				<div class="auth_descr">
					{hook h='blockPosition1' mod='posstatickblocks'}
				</div>
				<div id="login_form_content">
					<form action="{$link->getPageLink('authentication', true, NULL, "back=my-account")|escape:'html':'UTF-8'}" method="post" id="login_form_ff">
						<div class="input_block">
							<label>{l s='Телефон'}</label>
							<input class="validate" type="tel" id="login_phone" name="login_phone_ff" placeholder="+38" data-validate="isPhoneNumber" value="{if $login_phone}{$login_phone}{else}+38{/if}" />
							<span id="login_phone_errors" class="error_msg"></span>
						</div>
						<div class="input_block">
							<label>{l s='Пароль'}</label>
							<input class="validate" type="password" id="login_passwd" name="login_passwd_ff" data-validate="isPasswd"  />
							<span id="login_passwd_errors" class="error_msg"></span>
						</div>
						<div class="btn_holder login">
							<input type="submit" id="SubmitLoginFf2" class="button big" value="Увійти">
							<a class="remind_pass" href="{$link->getPageLink('password', true)|escape:'html':'UTF-8'}"  id="recovery-button">{l s='Нагадати пароль'}</a>
						</div>
					</form>
				</div>

			</div>

		</div>
	</div>

</div>

{if $login_phone}
	{addJsDef login_phone=$login_phone}
{/if}
