<div class="main_content list-page">
    <div class="filter_wrapper">
        <div class="fleft">
            <div class="filter" style="width: 60px;">
                {l s='Фільтр'}
                <i class="icon">
                    <svg>
                        <use xlink:href="#dropdown_arrow">
                    </svg>
                </i>
            </div>
        </div>

        <div class="filter_name price_filt fright">
            <div class="filter_items" id="price-sort" style="margin-top: 0px !important; margin-right: 15px;">
									<span class="filt_item price_sort" id="sort-type-name">
                                        {if $orderby eq 'position'}
                                            {l s='Рекомендовані'}
										{elseif $orderby eq $orderbydefault}
                                            {l s='Новинки'}
                                        {elseif $orderby eq 'price' AND $orderway eq 'asc'}
                                            {l s='Спочатку дешевше'}
                                        {elseif $orderby eq 'price' AND $orderway eq 'desc'}
                                            {l s='Спочатку дорожче'}
                                        {/if}
									</span>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <h1>{$category->h1tag|escape:'html':'UTF-8'}{if isset($categoryNameComplement)}&nbsp;{$categoryNameComplement|escape:'html':'UTF-8'}{/if}</h1>
    {if $category->id_image}
    <a class="big_link not_so_big list" href="#">
        <div class="big_img_link">
                <img src="{$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}" alt="">
        </div>
    </a>
    {/if}

    <div class="filter_items padded_block" >
        <ul class="js_filter_list filters_dynamic_mobile" id="filters_dynamic">

        </ul>
    </div>
    <div class="item_list padded_block">

        <div id="layered_ajax_loader"></div>

        {if !$products}
            <div class="no_content">{l s='Товар у дорозi. Завiтайте трохи згодом.'}</div>
        {/if}
        {include file="./../product-list.tpl" products=$products}

        <div class="content_sortPagiBar">
            {*{include file="./product-compare.tpl" paginationId='bottom'}*}
            {include file="./../pagination.tpl" paginationId='bottom'}
        </div>


        {*<a class="button order_btn" href="#">Показати ще</a>*}
    </div>
    <div class="filter_list_wrapper">
        <div class="filter_list" style="display: none;">


            {include file="./product-sort.tpl"}
            {hook h='displayLeftColumn' mod='blocklayered'}


        </div>
        <div class="mobile_height_spike"></div>
    </div>
    <div class="filter_btns_wrapper">
        <span class="button show_filter">{l s='Показати'}</span>
        <a class="clear_link" href="{$category->link_rewrite}-{$category->id}#/">{l s='Очистити'}</a>
    </div>
</div>
