window.eeGlobal = new EventEmitter();

$(document).ready(function(){

    $('select').on('focus',function() {
        var curScroll = $(window).scrollTop();
        $('html, body').animate({scrollTop:curScroll},1);
        $('.body_wrapper').on('touchstart',function(){
            $('select').blur();
        });
    });
    // $('input').on('focus',function() {
    //     if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
    //         $('.header').addClass('none')
    //     }
    //         $('.body_wrapper').on('touchstart',function(){
    //         $('input').blur();
    //         $('.header').removeClass('none');
    //     });
    // });

    var cur_scroll_top;

    $('.js_menu_item').click(function(){
        if($(this).hasClass('active')){
            $('.body_wrapper').css({'overflow': 'initial', 'height': 'auto'});
            $('body').css({'overflow': 'initial'});
            $(window).scrollTop(cur_scroll_top);
            $('.dropdown_menu_wrapper').css('height', '0px');
            $(this).removeClass('active');
        }
        else{
            $('.dd_content').html($(this).find('ul').html());
            cur_scroll_top = $(window).scrollTop();
            $(window).scrollTop(0);
            var menu_height = $('.dropdown_menu > ul > li').length*45 + 175;
            if( menu_height > $('.mobile_height_spike').height() ){
                var max_height = menu_height;
            }
            else{
                var max_height = $('.mobile_height_spike').height();
            }
            $('.body_wrapper').css({'overflow': 'hidden', 'height': max_height+'px'});
            $('body').css({'overflow': 'hidden'});
            $('.dropdown_menu').css({'height': '100%','display':'block'});
            $('.dropdown_menu_wrapper').css('height', '100%');
            $('.js_menu_item.active').removeClass('active');
            $(this).addClass('active');
        }
    });

    $('.show_filter').click(function(){
        $('.filter_wrapper').trigger('click');
    });

    $('.filter_wrapper').click(function(){
        if($(this).hasClass('active')){
            $('.body_wrapper').css({'overflow': 'initial', 'height': 'auto'});

            $(window).scrollTop(cur_scroll_top);

            $('.filter_list_wrapper').css('height', '50px');
            $(this).removeClass('active');
            $('.header').css('display','block');
            $('.filter_wrapper').removeAttr('style');
            if ($(window).width()>480) {
                $('.filter_wrapper').css('top','45px');
            } else {
                $('.filter_wrapper').css('top','67px');
            }
            $('.filter_btns_wrapper').css('display','none');
            $('.filter').removeClass('active')
        }
        else{
            cur_scroll_top = $(window).scrollTop();
            $('.header').css('display','none');
            //$('.filter_wrapper').css('top','0px');
            $('.filter_wrapper').attr('style', 'top:0px!important');
            $('.filter_btns_wrapper').css('display','block');

            $(window).scrollTop(0);
            $('.filter').addClass('active');

            //var menu_height = $('.filter_list > li').length*90 + 350;
            var menu_height = ($('.filter_list li').length*42) + ($('.filter_title').length*30);

            if( menu_height > $('.mobile_height_spike').height() ){
                var max_height = menu_height;
            }
            else{
                var max_height = $('.mobile_height_spike').height();
            }

            $('.body_wrapper').css({'overflow': 'hidden', 'height': max_height+'px'});

            $('.filter_list').css({'height': '100%','display':'block'});
            $('.filter_list_wrapper').css('height', '100%');

            $(this).addClass('active');
        }
    });

    if ($('.main_content.checkout-page').length) {
        setTimeout(function() {
            $('input:radio').styler();
        }, 100);
    }

    // $('.js_discount_input').on('keyup change',function(){
    //     if ($(this).val() != "") {
    //         $('.discount_button').css('display','block')
    //     } else {
    //         $('.discount_button').css('display','none')
    //     }
    // });


    $('.profile_nav_link').on('click',function(){
        if (!$(this).hasClass('active')) {
            $('.profile_nav_link.active').removeClass('active');
            $(this).addClass('active');
            $('.tab_content').fadeOut(200);
            var tab = $(this).attr('data-tab');
            setTimeout(function(){
                $('#'+tab).fadeIn(200);
            },200)
        }
    });

    if($('.filter_wrapper').length) {
        $(window).resize(function(){
            if ($('.header').height()>45) {
                $('.filter_wrapper').css('top','67px');
            } else {
                $('.filter_wrapper').css('top','45px');
            }
            if ($('.header').css('display')=='none') {
                $('.filter_wrapper').attr('style', 'top:0px!important');
            }
        })
    }


    $('body').on('click', '.accessories_menu_item > a', function (e) {
        var openClass = 'accessories_menu_item_link--open';
        e.preventDefault();
        $('.accessories_dop_menu').slideToggle({
            duration: 300,
            queue: false
        });
        if ($(this).hasClass(openClass)) {
            $(this).removeClass(openClass);
        } else {
            $(this).addClass(openClass);
        }
    });




    $(window).scroll(function(){
       if ( $('body').scrollTop() > 300){
            $('.slide_to_top').removeClass('hidden');
       }else{
           $('.slide_to_top').addClass('hidden');
       }
    });

    $('.slide_to_top').on('click', function(){
        $('HTML, BODY').animate({scrollTop: 0}, 500);
    });

    if( $('.body').hasClass('product-page') ){
        $('.product_image .slider').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });

        //if( $('.sizes_holder span').length > 7 ){
        //    $('.sizes_holder').slick({
        //        slidesToShow: 5,
        //        infinite: false,
        //        arrows: false,
        //        slidesToScroll: 1,
        //        swipeToSlide: true,
        //        dots: true,
        //        variableWidth: true,
        //        slide: 'span'
        //    });
        //}

    }

    if( $('.body').hasClass('trends-page') ){
        $('.slider_container').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });
    }

    if(  $('.body').hasClass('map-page')   ){
        var slider_timer = false;

        //$('.slider:first').slick({
        //    slidesToShow: 1,
        //    arrows: false,
        //    dots: true,
        //    slidesToScroll: 1,
        //    swipeToSlide: true
        //});

        $('.slider').on('afterChange', function(slick, slider){
            clearTimeout(slider_timer);
            var $curSlide = $('.slider .store_block[data-slick-index="'+slider.currentSlide+'"] div.info'),
                lat = $curSlide.attr('data-lat'),
                long = $curSlide.attr('data-lng');

            //console.log(lat);
            //console.log(lat +', '+long);

            $curSlide.trigger('click');

            //slider_timer = setTimeout(function(){
            //    console.log('Здесь будет рилод карты с задержкой, чтобы не плодить вызовы');
            //    load_map(lat, long);
            //}, 1000)
        });


    }

    function inputMask() {
        if ($().mask) {
            if ($('#login_phone, #phone')) {
                $('#login_phone, #phone').mask('+38 (000) 000-00-00');
            }
        } else {
            console.log('mask not found')
        }
    }
    inputMask();

    function showMobileSearch() {
        var $searchHolder = $('.js_mobile_search_holder'),
            $mobileSearchOpen = $('.js_mobile_search_open'),
            $mobileSearchClose = $('.js_mobile_search_close'),
            $menu = $('.header .menu'),
            $autocompleter = $('#search_query_top'),
            openClass = 'mobile_search_holder--open';

        if ($searchHolder && $searchHolder.length) {
            $mobileSearchOpen.click(function () {
                if (!$searchHolder.hasClass(openClass)) {
                    $searchHolder.addClass(openClass);
                    $autocompleter.focus();
                }
            });
            $mobileSearchClose.click(function () {
                if ($searchHolder.hasClass(openClass)) {
                    $searchHolder.removeClass(openClass);
                }
            });
        }
    }
    showMobileSearch();
    if ( window.isMobile && navigator.userAgent.indexOf("Safari") > -1 ) {
        $('body').addClass('safari');
    }

    function productVideo() {
        if ($('.js-video_link_block').length) {
            $('.js-video_link_block').on('click', function(){
                setTimeout(function(){
                    $('.product_image__video').fadeIn(650);
                }, 300)
            });
        }
        if ($('.js-show_video-link').length) {
            var lang = $('html')[0].lang;
            var video = 'Переглянути вiдео';
            var foto = 'Переглянути фото';
            var myTextBtn = $('.js-show_video-link');

            $(myTextBtn).on('click', function(){
                $(".js-product_image__video iframe")[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}','*');

                var textToggle = $(this);
                $(textToggle).toggleClass('open');
                if($(textToggle).hasClass('open')){
                    $('.js-product_image__video').removeClass('js-hide');
                    $('.slider').addClass('js-hide');
                    if(lang === 'uk-uk'){
                        $(this).text(foto);
                    }else{
                        $(this).text('Посмотреть фото');
                    }
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                }else{
                    $('.js-product_image__video').addClass('js-hide');
                    $('.slider').removeClass('js-hide');
                    if(lang === 'uk-uk'){
                        $(this).text(video);
                    }else {
                        $(this).text('Посмотреть видео');
                    }
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                }
            });
        }
    }
    productVideo();
});
