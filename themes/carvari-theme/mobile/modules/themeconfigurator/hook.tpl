{if isset($htmlitems) && $htmlitems}
	{if $hook == 'GenderBanner'}
		<div class="main-page__stage clearfix">
			{foreach name=items from=$htmlitems item=hItem}
				<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="img_link big">
					<span class="big_title">{$hItem.title|escape:'htmlall':'UTF-8'}</span>
					<div class="img_container">
						<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" />
					</div>
				</a>
			{/foreach}
		</div>
	{elseif $hook == 'BigBanner'}
		<div class="main-page__single">
			{foreach name=items from=$htmlitems item=hItem}
				<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="img_link small">
					<div class="img_container">
						<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" />
					</div>
					<span class="find_pair_text">
						<span class="big_text">{$hItem.title|escape:'htmlall':'UTF-8'}</span>
						<span class="small_text">{$hItem.html}</span>
					</span>
				</a>
			{/foreach}
		</div>
	{elseif $hook == 'SmallBanners'}
		<div class="main-page__stage">
			{foreach name=items from=$htmlitems item=hItem}
				<a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="img_link small">
					<div>
						<img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" />
						<span class="title">{$hItem.title|escape:'htmlall':'UTF-8'}</span>
					</div>
				</a>
			{/foreach}
		</div>
	{/if}
{/if}
