{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Our stores'}{/capture}

	<div class="main_content body map-page">
		<div class="padded_block">
			<h1>{l s='Магазини в мiстах України'}</h1>
		</div>

		<div class="padded_block">
			<div class="city_chooser">
				<div class="input_block">
					<div class="select_wrap">
						<select id="city-chooser">
							<option value="all">{l s='Усі'}</option>
							{foreach from=$stores_by_city key=store_city item=storess name=Storess}
								<option value="panel{$smarty.foreach.Storess.iteration}">{$store_city}</option>
							{/foreach}
						</select>
					</div>
					<i class="icon">
						<svg>
							<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dropdown_arrow">
							</use></svg>
					</i>
				</div>
			</div>

			{foreach from=$stores_by_city item=stores name=Stores}
				<div class="slider" id="panel{$smarty.foreach.Stores.iteration}" style="display: none;"
					 {*{if $smarty.foreach.Stores.iteration!=1}style="display: none;"{/if}*}
						>
					{foreach from=$stores item=store}
						<div class="slide store_block"
							 data-lat="48.45"
							 data-long="35.04"
							 {if $store.img}data-img1="{$store.img}" data-img1-full="{$store.imgfull}"{/if}
							 {if $store.img1}data-img2="{$store.img1}" data-img2-full="{$store.imgfull1}"{/if}
							 {if $store.img2}data-img3="{$store.img2}" data-img3-full="{$store.imgfull2}"{/if}
								>
							<div class="title">{$store.name}</div>
							<div class="info" data-lat="{$store.latitude}" data-lng="{$store.longitude}">
								{$store.address1}<br/>
								{$store.phone}<br/>
								{$store.hours_uns[0]}<br/>
							</div>
						</div>
					{/foreach}
				</div>
			{/foreach}



			<div class="map_holder">
				<div id="is_map">

				</div>
			</div>
		</div>
	</div>

<script src="https://maps.googleapis.com/maps/api/js"></script>
{*<script src="/themes/carvari-theme/js/map.js"></script>*}
{strip}
{*{addJsDef map=''}*}
{addJsDef stores_markers=$stores_by_city}
{*{addJsDef infoWindow=''}*}
{*{addJsDef locationSelect=''}*}
{*{addJsDef defaultLat=$defaultLat}*}
{*{addJsDef defaultLong=$defaultLong}*}
{*{addJsDef hasStoreIcon=$hasStoreIcon}*}
{*{addJsDef distance_unit=$distance_unit}*}
{*{addJsDef img_store_dir=$img_store_dir}*}
{*{addJsDef img_ps_dir=$img_ps_dir}*}
{*{addJsDef searchUrl=$searchUrl}*}
{*{addJsDef logo_store=$logo_store}*}
{*{addJsDefL name=translation_1}{l s='No stores were found. Please try selecting a wider radius.' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_2}{l s='store found -- see details:' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_3}{l s='stores found -- view all results:' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_4}{l s='Phone:' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_5}{l s='Get directions' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_6}{l s='Not found' js=1}{/addJsDefL}*}
{/strip}

