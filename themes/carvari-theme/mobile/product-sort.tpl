{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{*<div class="filter_title">Відобразити</div>*}
{*<ul class="dd_content">*}
{*<li><a href="#">Новинки</a></li>*}
{*<li><a href="#">Від дешевих до дорогих</a></li>*}
{*<li><a href="#">Від дорогих до дешевих</a></li>*}
{*</ul>*}

{if isset($orderby) AND isset($orderway)}
{* On 1.5 the var request is setted on the front controller. The next lines assure the retrocompatibility with some modules *}
	{if !isset($request)}
		<!-- Sort products -->
		{if isset($smarty.get.id_category) && $smarty.get.id_category}
			{assign var='request' value=$link->getPaginationLink('category', $category, false, true)
	}	{elseif isset($smarty.get.id_manufacturer) && $smarty.get.id_manufacturer}
			{assign var='request' value=$link->getPaginationLink('manufacturer', $manufacturer, false, true)}
		{elseif isset($smarty.get.id_supplier) && $smarty.get.id_supplier}
			{assign var='request' value=$link->getPaginationLink('supplier', $supplier, false, true)}
		{else}
			{assign var='request' value=$link->getPaginationLink(false, false, false, true)}
		{/if}
	{/if}
	{if $page_name == 'best-sales' && (!isset($smarty.get.orderby) || empty($smarty.get.orderby))}{$orderby = ''}{$orderbydefault = ''}{/if}
	<div class="filter_wrap">
			<div class="filter_title">{l s='Відобразити'}</div>
			<div class="filter_exp">
				<form id="productsSortForm{if isset($paginationId)}_{$paginationId}{/if}" action="{$request|escape:'html':'UTF-8'}" class="productsSortForm ">
					<input class="selectProductSort" type="hidden" id="selectProductSort{if isset($paginationId)}_{$paginationId}{/if}" value="{if $page_name != 'best-sales'}{$orderbydefault|escape:'html':'UTF-8'}:{$orderwaydefault|escape:'html':'UTF-8'}{/if}">
					<ul class="filter_options">
						<li><span class="sort_item item active {if $orderby eq $orderbydefault AND $orderway eq 'asc'} active{/if}" value="date_add:desc">{l s='Новинки'}</span></li>
						<li><span class="sort_item item{if $orderby eq 'price' AND $orderway eq 'asc'} active{/if}" value="price:asc">{l s='Спочатку дешевше'}</span></li>
						<li><span class="sort_item item{if $orderby eq 'price' AND $orderway eq 'desc'} active{/if}" value="price:desc">{l s='Спочатку дорожче'}</span></li>
					</ul>
				</form>
			</div>
	</div>
	<!-- /Sort products -->
	{if !isset($paginationId) || $paginationId == ''}
		{addJsDef request=$request}
	{/if}
{/if}
