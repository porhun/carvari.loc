{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $ajax_call}
	{if $orders && count($orders)}
		{foreach from=$orders item=order name=myLoop}
			<div class="one_order">
				<div class="attr"><div>{l s='Дата'}</div><div>{dateFormat date=$order.date_add full=0}</div></div>
				<div class="attr clearfix">
					<div>{l s='Вид'}'</div>
					<div>
						{foreach from=$order.order_details item=ord_detail}
							{$ord_detail.product_name}<br>
						{/foreach}
					</div>
				</div>
				<div class="attr clearfix">
					<div>{l s='Артикул'}</div>
					<div>
						{foreach from=$order.order_details item=ord_detail}
							{$ord_detail.product_reference}<br>
						{/foreach}
					</div>
				</div>
				<div class="attr clearfix">
					<div>{l s='Сума, грн'}</div>
					<div>
						{displayPrice price=$order.total_paid currency=$order.id_currency no_utf8=false convert=false}
					</div>
				</div>
				<div class="attr clearfix">
					<div>{l s='Адреса'}</div>
					<div>{$order.address}</div>
				</div>
				<div class="attr clearfix">
					<div>{l s='Статус'}</div>
					<div>{$order.order_state|escape:'html':'UTF-8'}</div>
				</div>
			</div>
		{/foreach}
	{/if}
{else}
	<div class="main_content body history-page">

		<div class="profile_nav">
			{$HOOK_CUSTOMER_ACCOUNT}
				<span class="profile_nav_link" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/my-account'">{l s='Персональні дані'}
					<span class="underline"></span>
				</span>
				<span class="profile_nav_link active" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/order-history'">{l s='Статуси замовлень'}
					<span class="underline"></span>
				</span>


		</div>


			<h1>{l s='Статуси замовлень'}</h1>

			{include file="$tpl_dir./errors.tpl"}

			{if $slowValidation}
				<p class="alert alert-warning">{l s='If you have just placed an order, it may take a few minutes for it to be validated. Please refresh this page if your order is missing.'}</p>
			{/if}





			{if $orders && count($orders)}


				<div class="padded_block" id="orders-list">

					{foreach from=$orders item=order name=myLoop}
						<div class="one_order">
							<div class="attr clearfix"><div>{l s='Дата'}</div><div>{dateFormat date=$order.date_add full=0}</div></div>
							<div class="attr clearfix">
								<div>{l s='Вид'}</div>
								<div>
									{foreach from=$order.order_details item=ord_detail}
										{$ord_detail.product_name}<br>
									{/foreach}
								</div>
							</div>
							<div class="attr clearfix">
								<div>{l s='Артикул'}</div>
								<div>
									{foreach from=$order.order_details item=ord_detail}
										{$ord_detail.product_reference}<br>
									{/foreach}
								</div>
							</div>
							<div class="attr clearfix">
								<div>{l s='Сума, грн'}</div>
								<div>
									{displayPrice price=$order.total_paid currency=$order.id_currency no_utf8=false convert=false}
								</div>
							</div>
							<div class="attr clearfix">
								<div>{l s='Адреса'}</div>
								<div>{$order.address}</div>
							</div>
							<div class="attr clearfix">
								<div>{l s='Статус'}</div>
								<div>{$order.order_state|escape:'html':'UTF-8'}</div>
							</div>
						</div>
					{/foreach}
				</div>
				{if $orders_all > $limit_mobile }
				<div class="padded_block">
					<div class="action">
						<button class="button" data-offset="1" id="orders-more">{l s='Показати ще'}</button>
					</div>
				</div>
                {/if}
			{else}
				<p class="alert alert-warning">{l s='У вас ще немає замовлень.'}</p>
			{/if}

	</div>
{/if}
