{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{include file="$tpl_dir./errors.tpl"}
{if $errors|@count == 0}
    {if !isset($priceDisplayPrecision)}
        {assign var='priceDisplayPrecision' value=2}
    {/if}
    {if !$priceDisplay || $priceDisplay == 2}
        {assign var='productPrice' value=$product->getPrice(true, $smarty.const.NULL, 6)}
        {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(false, $smarty.const.NULL)}
    {elseif $priceDisplay == 1}
        {assign var='productPrice' value=$product->getPrice(false, $smarty.const.NULL, 6)}
        {assign var='productPriceWithoutReduction' value=$product->getPriceWithoutReduct(true, $smarty.const.NULL)}
    {/if}


    <div class="main_content body product-page">


        <div class="padded_block go_back">
            <a href="javascript:history.go(-1)">
                <i class="icon">
                    <svg>
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#go_back_icon">
                        </use></svg>
                </i>
            </a>
        </div>

                    <h1>{' '|implode:[$product->name, $product->reference]|escape:'html':'UTF-8'}</h1>

        {if isset($images) && count($images) > 0}

        <!-- thumbnails -->
        <div class="product_image">
            <div class="slider">
                {if isset($video_url) && trim($video_url) != ''}
                    <div class="slide">
                        <div class="product_image__video js-product_image__video">
                            <iframe src="https://www.youtube.com/embed/{$video_url}?rel=0&controls=0&showinfo=0&autoplay=1&loop=1&playlist={$video_url}" frameborder="0" gesture="media" allowfullscreen></iframe>
                        </div>
                    </div>
                {/if}
                {if isset($images)}
                    {foreach from=$images item=image name=thumbnails}
                        {assign var=imageIds value="`$product->id`-`$image.id_image`"}
                        {if !empty($image.legend)}
                            {assign var=imageTitle value=$image.legend|escape:'html':'UTF-8'}
                        {else}
                            {assign var=imageTitle value=' '|implode:[$product->name, $product->reference]|escape:'html':'UTF-8'}
                        {/if}
                        <div class="slide">
                            <img class="img-responsive" src="{$link->getImageLink($product->link_rewrite, $imageIds, 'large_default')|escape:'html':'UTF-8'}" alt="{$imageTitle}" title="{$imageTitle}" itemprop="image" />
                        </div>
                    {/foreach}
                {/if}
            </div>
        </div>

        <!-- end thumbnails -->

        {/if}

        <div class="padded_block product_card">
            <!--            <div class="title">-->
            <!--                Босонiжки-->
            <!--            </div>-->


            <div class="prices">
            {if $productPriceWithoutReduction > $productPrice}<div id="spec-mob" class="inline-block old_price">{convertPrice price=$productPriceWithoutReduction|floatval}</div>{/if}
                <div class="inline-block cur_price" id="our_price_display">
                    {convertPrice price=$productPrice|floatval}
                </div>
                {if $product->specificPrice.reduction_type == 'percentage' && $product->specificPrice.reduction * 100 >= Configuration::get('PS_PRODUCT_DISCOUNT')|intval}
                    <span class="discount-percentage"><span>-{(($product->specificPrice.reduction * 100)/5)|ceil*5}%</span></span>
                {/if}
            </div>

                        <form id="buy_block"{if $PS_CATALOG_MODE && !isset($groups) && $product->quantity > 0} class="hidden"{/if} action="{$link->getPageLink('cart')|escape:'html':'UTF-8'}" method="post">
                <!-- hidden datas -->
                <p class="hidden">
                    <input type="hidden" name="token" value="{$static_token}" />
                    <input type="hidden" name="id_product" value="{$product->id|intval}" id="product_page_product_id" />
                    <input type="hidden" name="add" value="1" />
                    <input type="hidden" name="id_product_attribute" id="idCombination" value="" />
                </p>
                <!-- quantity wanted -->
                <!-- minimal quantity wanted -->
                {if isset($groups)}
                    <!-- attributes -->
                    <div id="attributes">
                        <div class="clearfix"></div>
                        {foreach from=$groups key=id_attribute_group item=group}
                            {if $group.attributes|@count}
                                <br/>
                                <fieldset class="attribute_fieldset">
                                    {*<label class="attribute_label" {if $group.group_type != 'color' && $group.group_type != 'radio'}for="group_{$id_attribute_group|intval}"{/if}>{$group.name|escape:'html':'UTF-8'}&nbsp;</label>*}
                                    <div class="attr-block">{$group.name|escape:'html':'UTF-8'}:</div>
                                    {assign var="groupName" value="group_$id_attribute_group"}
                                    <div class="attribute_list">
                                        {if ($group.group_type == 'select')}
                                            <select name="{$groupName}" id="group_{$id_attribute_group|intval}" class="attribute_select no-print">
                                                {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                    <option value="{$id_attribute|intval}"{if (isset($smarty.get.$groupName) && $smarty.get.$groupName|intval == $id_attribute) || $group.default == $id_attribute} selected="selected"{/if} title="{$group_attribute|escape:'html':'UTF-8'}">{$group_attribute|escape:'html':'UTF-8'}</option>
                                                {/foreach}
                                            </select>
                                        {elseif ($group.group_type == 'color')}
                                            <ul id="color_to_pick_list" class="clearfix">
                                                {assign var="default_colorpicker" value=""}
                                                {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                    {assign var='img_color_exists' value=file_exists($col_img_dir|cat:$id_attribute|cat:'.jpg')}
                                                    <li{if $group.default == $id_attribute} class="selected"{/if}>
                                                        <a href="{$link->getProductLink($product)|escape:'html':'UTF-8'}" id="color_{$id_attribute|intval}" name="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" class="color_pick{if ($group.default == $id_attribute)} selected{/if}"{if !$img_color_exists && isset($colors.$id_attribute.value) && $colors.$id_attribute.value} style="background:{$colors.$id_attribute.value|escape:'html':'UTF-8'};"{/if} title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}">
                                                            {if $img_color_exists}
                                                                <img src="{$img_col_dir}{$id_attribute|intval}.jpg" alt="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" title="{$colors.$id_attribute.name|escape:'html':'UTF-8'}" width="20" height="20" />
                                                            {/if}
                                                        </a>
                                                    </li>
                                                    {if ($group.default == $id_attribute)}
                                                        {$default_colorpicker = $id_attribute}
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                            <input type="hidden" class="color_pick_hidden" name="{$groupName|escape:'html':'UTF-8'}" value="{$default_colorpicker|intval}" />
                                        {elseif ($group.group_type == 'radio')}

                                            <div class="sizes_block">

                                                <ul class="clearfix">
                                                    {foreach from=$group.attributes key=id_attribute item=group_attribute}
                                                        <li{if ($group.default == $id_attribute)} class="active"{/if} >
                                                            <input type="radio" class="attribute_radio none" name="{$groupName|escape:'html':'UTF-8'}" value="{$id_attribute}" {if ($group.default == $id_attribute)} checked="checked"{/if} />
                                                            <a href="#">{$group_attribute|escape:'html':'UTF-8'}</a>
                                                        </li>
                                                    {/foreach}
                                                </ul>
                                            </div>

                                        {/if}
                                    </div> <!-- end attribute_list -->
                                </fieldset>
                            {/if}
                        {/foreach}
                    </div> <!-- end attributes -->
                {/if}
                {*<div{if (!$allow_oosp && $product->quantity <= 0) || !$product->available_for_order || (isset($restricted_country_mode) && $restricted_country_mode) || $PS_CATALOG_MODE} class="unvisible"{/if}>*}
                {*{if isset($video_url) && trim($video_url) != ''}*}
                    {*<div class="btn show_video js-show_video-link">{l s='Посмотреть видео'}*}
                        {*<a href="#" class="video_link_block js-video_link_block ">{l s='Посмотреть видео'}</a>*}
                    {*</div>*}
                {*{/if}*}
                <div>
                <div id="add_to_cart" class="no-print" style="text-align: center;">
                    {if !$PS_CATALOG_MODE && $product->available_for_order && ($product->quantity > 0 || $product->quantity_all_versions && $product->quantity_all_versions > 0 || $product->allow_oosp)}
                        {if $in_cart}
                            <button type="submit" class="button btn-outline order_button added">
                                <span>{l s='У кошику'}</span>
                            </button>
                        {else}
                            <button type="submit" class="button button2 order_button">
                                <span data-inbasket-translate="{l s='Купити зараз'}">{l s='Купити зараз'}</span>
                            </button>
                        {/if}
                        <div id="quantity_wanted_p" class="none">
                            <input type="text" min="1" name="qty" id="quantity_wanted" class="text" value="{if isset($quantityBackup)}{$quantityBackup|intval}{else}{if $product->minimal_quantity > 1}{$product->minimal_quantity}{else}1{/if}{/if}" />
                        </div>
                    {/if}
                    </div>
                    <div style="text-align: center;">
                        {hook h="displayProductButtons" mod="tmoneclickorder"}
                    </div>
                </div>
            </form>

            {hook h="extraRight" mod="loyalty"}


 <div class="clearfix"></div>
               {* <div class="attrs">
 <div class="attr-block">{$product->reference}</div> *}
                    {if isset($features) && $features}
                    <div class="attr_block m-b-lg">
                        <div class="title-md">{l s='Деталi:'}</div>
                        <div class="attr_list">
                            <div class="row">
                                <div id="col-md-1">{l s='Артикул:'}<span id="col-md-2"><b>{$product->reference|escape:'html':'UTF-8'}</b></span></div>
                            </div>
                            {foreach from=$features item=feature}
                                    {if isset($feature.value) && $feature.name!='Стиль'}
                                        <div class="row">
                                            <div id="col-md-1">{$feature.name|escape:'html':'UTF-8'}:<span id="col-md-2"><b>{$feature.value|escape:'html':'UTF-8'}</b></span></div>
                                        </div>
                                    {/if}
                            {/foreach}
                        </div>
                    </div>
                        {hook h="displayLabelBlock" mod="blocklabelprivatbank"}
                       {* {foreach from=$features item=feature}
                            {if isset($feature.value) && $feature.name!='Стиль'}
                                <div class="attr-block">{$feature.name|escape:'html':'UTF-8'}: {$feature.value|escape:'html':'UTF-8'}</div>
                            {/if}
                        {/foreach}*}
                    {/if}
               {* </div> *}

                    <div class="clearfix"></div>







        </div>
    </div>


<div class="product_popup" id="product_popup">
    <div class="popup_wrapper">
        <a href="#" class="popup_closer"></a>

        <div class="popup_content popup-slider">

        </div>
    </div>
</div>
{strip}
{if isset($smarty.get.ad) && $smarty.get.ad}
	{addJsDefL name=ad}{$base_dir|cat:$smarty.get.ad|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{if isset($smarty.get.adtoken) && $smarty.get.adtoken}
	{addJsDefL name=adtoken}{$smarty.get.adtoken|escape:'html':'UTF-8'}{/addJsDefL}
{/if}
{addJsDef allowBuyWhenOutOfStock=$allow_oosp|boolval}
{addJsDef availableNowValue=$product->available_now|escape:'quotes':'UTF-8'}
{addJsDef availableLaterValue=$product->available_later|escape:'quotes':'UTF-8'}
{addJsDef attribute_anchor_separator=$attribute_anchor_separator|escape:'quotes':'UTF-8'}
{addJsDef attributesCombinations=$attributesCombinations}
{addJsDef currentDate=$smarty.now|date_format:'%Y-%m-%d %H:%M:%S'}
{if isset($combinations) && $combinations}
	{addJsDef combinations=$combinations}
	{addJsDef combinationsFromController=$combinations}
	{addJsDef displayDiscountPrice=$display_discount_price}
	{addJsDefL name='upToTxt'}{l s='Up to' js=1}{/addJsDefL}
{/if}
{if isset($combinationImages) && $combinationImages}
	{addJsDef combinationImages=$combinationImages}
{/if}
{addJsDef customizationId=$id_customization}
{addJsDef customizationFields=$customizationFields}
{addJsDef default_eco_tax=$product->ecotax|floatval}
{addJsDef displayPrice=$priceDisplay|intval}
{addJsDef ecotaxTax_rate=$ecotaxTax_rate|floatval}
{if isset($cover.id_image_only)}
	{addJsDef idDefaultImage=$cover.id_image_only|intval}
{else}
	{addJsDef idDefaultImage=0}
{/if}
{addJsDef img_ps_dir=$img_ps_dir}
{addJsDef img_prod_dir=$img_prod_dir}
{addJsDef id_product=$product->id|intval}
{addJsDef jqZoomEnabled=$jqZoomEnabled|boolval}
{addJsDef maxQuantityToAllowDisplayOfLastQuantityMessage=$last_qties|intval}
{addJsDef minimalQuantity=$product->minimal_quantity|intval}
{addJsDef noTaxForThisProduct=$no_tax|boolval}
{if isset($customer_group_without_tax)}
	{addJsDef customerGroupWithoutTax=$customer_group_without_tax|boolval}
{else}
	{addJsDef customerGroupWithoutTax=false}
{/if}
{if isset($group_reduction)}
	{addJsDef groupReduction=$group_reduction|floatval}
{else}
	{addJsDef groupReduction=false}
{/if}
{addJsDef oosHookJsCodeFunctions=Array()}
{addJsDef productHasAttributes=isset($groups)|boolval}
{addJsDef productPriceTaxExcluded=($product->getPriceWithoutReduct(true)|default:'null' - $product->ecotax)|floatval}
{addJsDef productPriceTaxIncluded=($product->getPriceWithoutReduct(false)|default:'null' - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcluded=($product->getPrice(false, null, 6, null, false, false) - $product->ecotax)|floatval}
{addJsDef productBasePriceTaxExcl=($product->getPrice(false, null, 6, null, false, false)|floatval)}
{addJsDef productBasePriceTaxIncl=($product->getPrice(true, null, 6, null, false, false)|floatval)}
{addJsDef productReference=$product->reference|escape:'html':'UTF-8'}
{addJsDef productAvailableForOrder=$product->available_for_order|boolval}
{addJsDef productPriceWithoutReduction=$productPriceWithoutReduction|floatval}
{addJsDef productPrice=$productPrice|floatval}
{addJsDef productUnitPriceRatio=$product->unit_price_ratio|floatval}
{addJsDef productShowPrice=(!$PS_CATALOG_MODE && $product->show_price)|boolval}
{addJsDef PS_CATALOG_MODE=$PS_CATALOG_MODE}
{if $product->specificPrice && $product->specificPrice|@count}
	{addJsDef product_specific_price=$product->specificPrice}
{else}
	{addJsDef product_specific_price=array()}
{/if}
{if $display_qties == 1 && $product->quantity}
	{addJsDef quantityAvailable=$product->quantity}
{else}
	{addJsDef quantityAvailable=0}
{/if}
{addJsDef quantitiesDisplayAllowed=$display_qties|boolval}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'percentage'}
	{addJsDef reduction_percent=$product->specificPrice.reduction*100|floatval}
{else}
	{addJsDef reduction_percent=0}
{/if}
{if $product->specificPrice && $product->specificPrice.reduction && $product->specificPrice.reduction_type == 'amount'}
	{addJsDef reduction_price=$product->specificPrice.reduction|floatval}
{else}
	{addJsDef reduction_price=0}
{/if}
{if $product->specificPrice && $product->specificPrice.price}
	{addJsDef specific_price=$product->specificPrice.price|floatval}
{else}
	{addJsDef specific_price=0}
{/if}
{addJsDef specific_currency=($product->specificPrice && $product->specificPrice.id_currency)|boolval} {* TODO: remove if always false *}
{addJsDef stock_management=$PS_STOCK_MANAGEMENT|intval}
{addJsDef taxRate=$tax_rate|floatval}
{addJsDefL name=doesntExist}{l s='This combination does not exist for this product. Please select another combination.' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMore}{l s='This product is no longer in stock' js=1}{/addJsDefL}
{addJsDefL name=doesntExistNoMoreBut}{l s='with those attributes but is available with others.' js=1}{/addJsDefL}
{addJsDefL name=fieldRequired}{l s='Please fill in all the required fields before saving your customization.' js=1}{/addJsDefL}
{addJsDefL name=uploading_in_progress}{l s='Uploading in progress, please be patient.' js=1}{/addJsDefL}
{addJsDefL name='product_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='product_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
{/strip}
{/if}