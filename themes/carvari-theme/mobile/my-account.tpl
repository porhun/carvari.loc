{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='My account'}{/capture}

<div class="main_content checkout-page clearfix">
    <div class="profile_nav">
        {$HOOK_CUSTOMER_ACCOUNT}
            <span class="profile_nav_link active" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/my-account'">{l s='Персональні дані'}
                <span class="underline"></span>
            </span>
            <span class="profile_nav_link" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/order-history'">{l s='Статуси замовлень'}
                <span class="underline"></span>
            </span>


    </div>
    <h1>{l s='Редагувати дані'}</h1>
    <div class="padded_block">
        <div class="tab_wrapper">
            <div class="tab_content">
                {include file="$tpl_dir./errors.tpl"}
                {if isset($confirmation) && $confirmation}
                    <p class="alert alert-success">
                        {l s='Your personal information has been successfully updated.'}
                        {if isset($pwd_changed)}<br />{l s='Your password has been sent to your email:'} {$email}{/if}
                    </p>

                {/if}
                <br/>
                <form action="" method="post">
                    <div class="input_block">
                        <label>{l s='Ім\'я'}</label>
                        <input class="is_required validate" type="text" value="{$customer->firstname}" data-validate="isName" type="text" id="firstname" name="firstname">
                    </div>
                    <div class="input_block">
                        <label>{l s='Прізвище'}</label>
                        <input class="is_required validate" type="text" value="{$customer->lastname}" data-validate="isName" type="text" id="lastname" name="lastname">
                    </div>
                    <div class="input_block">
                        <label>{l s='Дата народження'}</label>
                        <br/>
                        <select name="days" id="days" class="form-control width_auto">
                            <option value="">-</option>
                            {foreach from=$days item=v}
                                <option value="{$v}" {if ($sl_day == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                            {/foreach}
                        </select>
                        <select id="months" name="months" class="form-control width_auto">
                            <option value="">-</option>
                            {foreach from=$months key=k item=v}
                                <option value="{$k}" {if ($sl_month == $k)}selected="selected"{/if}>{l s=$v}&nbsp;</option>
                            {/foreach}
                        </select>
                        <select id="years" name="years" class="form-control width_auto">
                            <option value="">-</option>
                            {foreach from=$years item=v}
                                <option value="{$v}" {if ($sl_year == $v)}selected="selected"{/if}>{$v}&nbsp;&nbsp;</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="input_block">
                        <label>{l s='Телефон'}</label>
                        <input class="is_required validate" type="text" value="{$customer->phone_login}" data-validate="isPhoneNumber" disabled>
                    </div>
                    <div class="input_block">
                        <label>{l s='E-mail'}</label>
                        <input class="is_required validate" type="text" value="{$customer->email}" data-validate="isEmail" type="email" name="email" id="email">
                    </div>
                    <div class="input_block">
                        <label class="required">{l s='Поточний пароль'}</label>
                        <input class="is_required validate" type="password" type="password" data-validate="isPasswd" name="old_passwd" id="old_passwd">
                    </div>
                    <div class="input_block">
                        <label>{l s='Новий пароль'}</label>
                        <input class="is_required validate" type="password" type="password" data-validate="isPasswd" name="passwd" id="passwd">
                    </div>
                    <h2>
                        {l s='Доставка'}
                    </h2>
                    <div class="input_block">
                        <label>{l s='Область'}</label>
                        <select class="is_required" name="address[id_state]">
                            <option></option>
                            {foreach from=$states item=state}
                                <option value="{$state.id_state}" {if $customer_address.id_state == $state.id_state}selected{/if}>{$state.name}</option>
                            {/foreach}
                        </select>
                    </div>
                    <div class="input_block">
                        <label>{l s='Місто'}</label>
                        <input class="is_required validate" type="text" value="{$customer_address.city}" data-validate="isCityName" name="address[city]">
                    </div>
                    <div class="input_block">
                        <label>{l s='Вулиця'}</label>
                        <input class="is_required validate" type="text" value="{$address1.0}" data-validate="isAddress" name="address[street]">
                    </div>
                    <div class="input_block">
                        <label>{l s='Будинок'}</label>
                        <input class="is_required validate" type="text" value="{$address1.1}" data-validate="isAddress" name="address[house]">
                    </div>
                    <div class="input_block">
                        <label>{l s='Квартира'}</label>
                        <input class="is_required validate" type="text" value="{$address1.2}" data-validate="isAddress" name="address[flat]">
                    </div>
                    <div class="btn_holder">
                        <!--                        remove class disable to undisable, disable not work at all, just style-->
                        <input type="hidden" name="token" value="{$token}" />
                        <input type="submit" name="submitAccount" class="button big" value="{l s='Зберегти зміни'}">
                    </div>
                </form>

                {*<div class="row addresses-lists">*}
                    {*<div class="col-xs-12 col-sm-6 col-lg-4">*}
                        {*<ul class="myaccount-link-list">*}
                            {*{if $has_customer_an_address}*}
                            {*<li><a href="{$link->getPageLink('address', true)|escape:'html':'UTF-8'}" title="{l s='Add my first address'}"><i class="icon-building"></i><span>{l s='Add my first address'}</span></a></li>*}
                            {*{/if}*}
                            {*<li><a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='Orders'}"><i class="icon-list-ol"></i><span>{l s='Order history and details'}</span></a></li>*}
                            {*{if $returnAllowed}*}
                                {*<li><a href="{$link->getPageLink('order-follow', true)|escape:'html':'UTF-8'}" title="{l s='Merchandise returns'}"><i class="icon-refresh"></i><span>{l s='My merchandise returns'}</span></a></li>*}
                            {*{/if}*}
                            {*<li><a href="{$link->getPageLink('order-slip', true)|escape:'html':'UTF-8'}" title="{l s='Credit slips'}"><i class="icon-file-o"></i><span>{l s='My credit slips'}</span></a></li>*}
                            {*<li><a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='Addresses'}"><i class="icon-building"></i><span>{l s='My addresses'}</span></a></li>*}
                            {*<li><a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Information'}"><i class="icon-user"></i><span>{l s='My personal information'}</span></a></li>*}
                        {*</ul>*}
                    {*</div>*}
                {*{if $voucherAllowed || isset($HOOK_CUSTOMER_ACCOUNT) && $HOOK_CUSTOMER_ACCOUNT !=''}*}
                    {*<div class="col-xs-12 col-sm-6 col-lg-4">*}
                        {*<ul class="myaccount-link-list">*}
                            {*{if $voucherAllowed}*}
                                {*<li><a href="{$link->getPageLink('discount', true)|escape:'html':'UTF-8'}" title="{l s='Vouchers'}"><i class="icon-barcode"></i><span>{l s='My vouchers'}</span></a></li>*}
                            {*{/if}*}
                            {*{$HOOK_CUSTOMER_ACCOUNT}*}
                        {*</ul>*}
                    {*</div>*}
                {*{/if}*}
                {*</div>*}
                {**}
                {*<ul class="footer_links clearfix">*}
                {*<li><a class="btn btn-default button button-small" href="{$base_dir}" title="{l s='Home'}"><span><i class="icon-chevron-left"></i> {l s='Home'}</span></a></li>*}
                {*</ul>*}

            </div>
        </div>
    </div>
</div>