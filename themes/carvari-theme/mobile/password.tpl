{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="main_content checkout-page">
	{capture name=path}<a href="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}" title="{l s='Authentication'}" rel="nofollow">{l s='Authentication'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Forgot your password'}{/capture}

	<div class="padded_block">
		<h1>{l s='Нагадати пароль'}</h1>




	{if isset($confirmation) && $confirmation == 1}
	<p class="alert alert-success">
		{*{l s='Your password has been successfully reset and a confirmation has been sent to your email address:'} {if isset($customer_email)}{$customer_email|escape:'html':'UTF-8'|stripslashes}{/if}*}
        {l s='Новий пароль вислано Вам на телефон:'} {$customer_phone}
	</p>
	<div class="btn_holder reminder">
		<a href="{if $active_url_lang}{$active_url_lang}{/if}/authentication" class="button big">{l s='Увiйти'}</a>
	</div>
	{elseif isset($confirmation) && $confirmation == 2}
	<p class="alert alert-success">
		{*{l s='A confirmation email has been sent to your address:'} {if isset($customer_email)}{$customer_email|escape:'html':'UTF-8'|stripslashes}{/if}*}
        {l s='Лист підтвердження вислано Вам на пошту:'}
	</p>
	{else}
	<p>
		{*{l s='Please enter the email address you used to register. We will then send you a new password. '}*}
        {l s='Будь ласка, введіть телефон на який зареестровано Ваш профіль. Ми надішлемо Вам новий пароль.'}
	</p>
	{*<form action="{$request_uri|escape:'html':'UTF-8'}" method="post" class="std" id="form_forgotpassword">*}

		{*<fieldset>*}
			{*<div class="form-group">*}
				{*<label for="email">{l s='Email address'}</label>*}
				{*<input class="form-control" type="email" id="email" name="email" value="{if isset($smarty.post.email)}{$smarty.post.email|escape:'html':'UTF-8'|stripslashes}{/if}" />*}
			{*</div>*}
			{*<p class="submit">*}
				{*<button type="submit" class="btn btn-default button button-medium"><span>{l s='Retrieve Password'}<i class="icon-chevron-right right"></i></span></button>*}
			{*</p>*}
		{*</fieldset>*}
	{*</form>*}

		<br/>
		<div class="tab_wrapper">
			<div id="tab1" class="tab_content">

				<div id="opc_login_errors" class="cnt"></div>

				<form action="{$request_uri|escape:'html':'UTF-8'}" method="post" class="std" id="form_forgotpassword">
					<!-- Error return block -->
					{*<div id="opc_login_errors" class="alert alert-danger" style="display:none;"></div>*}
					<!-- END Error return block -->
					<div class="input_block{if $errors} wrong{/if}">
						<label>{l s='Телефон'}</label>
						<input class="validate" type="tel" id="phone" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone|escape:'html':'UTF-8'|stripslashes}{else}{if $login_phone}{$login_phone}{else}+380{/if}{/if}" data-validate="isPhoneNumber" />
						{if $errors}<span id="opc_login_errors" class="error_msg" style="display: inline-block;">{$errors[0]}</span>{/if}
					</div>
					<div class="btn_holder login">
						<input type="submit" id="SubmitLoginFf2" class="button big" value="Нагадати пароль">
						<a class="remind_pass" href="{$link->getPageLink('authentication')|escape:'html':'UTF-8'}{if $login_phone}?login_phone={$login_phone}{/if}" title="{l s='Back to Login'}" rel="nofollow">{l s='До форми входу'}</a>
					</div>
					<div class="clearfix"></div>

				</form>

			</div>
		</div>

	{/if}

	</div>
</div>
