{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<div class="main_content profile clearfix">
	<div class="min_width">

		<div class="clearfix">
			<h1>{l s='My account'}</h1>
			<a class="logout_link" href="{if $active_url_lang}{$active_url_lang}{/if}/?mylogout=">{l s='Вийти'}</a>
		</div>


		<div class="profile_nav">
			{$HOOK_CUSTOMER_ACCOUNT}
            <span class="profile_nav_link" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/my-account'">{l s='Персональні дані'}
                <span class="underline"></span>
            </span>
            <span class="profile_nav_link active" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/order-history'">{l s='Статуси замовлень'}
                <span class="underline"></span>
            </span>


		</div>

		{include file="$tpl_dir./errors.tpl"}

		{if $slowValidation}
			<p class="alert alert-warning">{l s='If you have just placed an order, it may take a few minutes for it to be validated. Please refresh this page if your order is missing.'}</p>
		{/if}
		{if $orders && count($orders)}
			<div class="tab_wrapper">
				<div id="tab2" class="tab_content">
					<table class="order_status">
						<thead>
						<tr>
							<th width="90">{l s='Дата'}</th>
							<th width="165">{l s='Вид'}</th>
							<th width="130">{l s='Артикул'}</th>
							<th width="90">{l s='Сума, грн'}</th>
							<th width="315">{l s='Адреса'}</th>
							<th width="90">{l s='Статус'}</th>
						</tr>
						</thead>
						<tbody>
						{foreach from=$orders item=order name=myLoop}
							<tr>
								<td width="90">{dateFormat date=$order.date_add full=0}</td>
								<td width="165">
									{foreach from=$order.order_details item=ord_detail}
										{$ord_detail.product_name}<br>
									{/foreach}
								</td>
								<td width="130">
									{foreach from=$order.order_details item=ord_detail}
										{$ord_detail.product_reference}<br>
									{/foreach}
								</td>
								<td width="90">{displayPrice price=$order.total_paid currency=$order.id_currency no_utf8=false convert=false}</td>
								<td width="315">{$order.address}</td>
								<td width="90">{$order.order_state|escape:'html':'UTF-8'}</td>
							</tr>
						{/foreach}

						</tbody>
					</table>
				</div>
			</div>
		{else}
			<p class="alert alert-warning">{l s='У вас ще немає замовлень.'}</p>
		{/if}

	</div>
</div>