{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Your shopping cart'}{/capture}



<h1>{l s='Замовлення'}</h1>

{assign var='current_step' value='summary'}
{include file="$tpl_dir./order-steps.tpl"}
{include file="$tpl_dir./errors.tpl"}

{if isset($empty)}
	<p class="alert alert-warning">{l s='Your shopping cart is empty.'}</p>
{elseif $PS_CATALOG_MODE}
	<p class="alert alert-warning">{l s='This store has not accepted your new order.'}</p>
{else}
	{*<p id="emptyCartWarning" class="alert alert-warning unvisible">{l s='Your shopping cart is empty.'}</p>*}
	{assign var='total_discounts_num' value="{if $total_discounts != 0}1{else}0{/if}"}
	{assign var='use_show_taxes' value="{if $use_taxes && $show_taxes}2{else}0{/if}"}
	{assign var='total_wrapping_taxes_num' value="{if $total_wrapping != 0}1{else}0{/if}"}
	{* eu-legal *}
	{hook h="displayBeforeShoppingCartBlock"}


	<div class="order_list">
		<ul class="order_items">


			{foreach $products as $product}
				{if $product.is_virtual == 0}
					{assign var='have_non_virtual_products' value=true}
				{/if}
				{assign var='productId' value=$product.id_product}
				{assign var='productAttributeId' value=$product.id_product_attribute}
				{assign var='quantityDisplayed' value=0}
				{assign var='odd' value=($odd+1)%2}
				{assign var='ignoreProductLast' value=isset($customizedDatas.$productId.$productAttributeId) || count($gift_products)}
				{* Display the product line *}
				{*{include file="$tpl_dir./shopping-cart-product-line.tpl" productLast=$product@last productFirst=$product@first}*}
				{* Then the customized datas ones*}
				<li id="product_{$product.id_product}_{$product.id_product_attribute}_{if $quantityDisplayed > 0}nocustom{else}0{/if}_{$product.id_address_delivery|intval}{if !empty($product.gift)}_gift{/if}">
					<input type="hidden" value="{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}{$customizedDatas.$productId.$productAttributeId|@count}{else}{$product.cart_quantity-$quantityDisplayed}{/if}" name="quantity_{$product.id_product}_{$product.id_product_attribute}_{if $quantityDisplayed > 0}nocustom{else}0{/if}_{$product.id_address_delivery|intval}_hidden" />
					<div class="item">
						<div class="item_img">
							<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category, null, null, $product.id_shop, $product.id_product_attribute)|escape:'html':'UTF-8'}">
								<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" alt="">
							</a>
						</div>
						<div class="item_descr">
							<div class="title"><a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category, null, null, $product.id_shop, $product.id_product_attribute)|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></div>
							<div class="code">{$product.reference}</div>
							<div class="size">{$product.attributes|escape:'html':'UTF-8'|@replace: ',':'<br/>'}</div>
							<div class="price">{convertPrice price=$product.price}</div>
						</div>
						<div class="delete_item">
							<a rel="nofollow" title="{l s='Delete'}" class="cart_quantity_delete" id="{$product.id_product}_{$product.id_product_attribute}_{if $quantityDisplayed > 0}nocustom{else}0{/if}_{$product.id_address_delivery|intval}" href="{$link->getPageLink('cart', true, NULL, "delete=1&amp;id_product={$product.id_product|intval}&amp;ipa={$product.id_product_attribute|intval}&amp;id_address_delivery={$product.id_address_delivery|intval}&amp;token={$token_cart}")|escape:'html':'UTF-8'}">
								<i class="icon">
									<svg>
										<use xlink:href="#close_icon">
									</svg>
								</i>
							</a>
						</div>
					</div>
				</li>
			{/foreach}








		</ul>
		{*{if $voucherAllowed}*}
			{if isset($errors_discount) && $errors_discount}
				<ul class="alert alert-danger">
					{foreach $errors_discount as $k=>$error}
						<li>{$error|escape:'html':'UTF-8'}</li>
					{/foreach}
				</ul>
			{/if}
			<form action="{if $opc}{$link->getPageLink('order-opc', true)}{else}{$link->getPageLink('order', true)}{/if}" method="post" id="voucher">
				<fieldset>
					<div class="discount">
						<div class="text">{l s='Використовуй купон на знижку'}</div>
						{*<input placeholder="Одноразовий шестизначний код" class="js_discount_input" type="text">*}
						<input type="text" class="discount_name form-control" id="discount_name" name="discount_name" value="{if isset($discount_name) && $discount_name}{$discount_name}{/if}" />
						<input type="hidden" name="submitDiscount" />
						<a href="#" class="discount_button">
							<i class="icon">
								<svg>
									<use xlink:href="#subscribe_icon">
								</svg>
							</i>
						</a>
					</div>

				</fieldset>
			</form>
		{*{/if}*}


		<div class="total">
			<div class="price_text clearfix">
				<span class="price_title">{l s='Стоимость'}</span>
				<span class="price" id="total_product">{displayPrice price=$total_products}</span>
			</div>

			{if $total_shipping_tax_exc <= 0 && !isset($virtualCart)}
				<div class="price_text clearfix{if !$opc && (!isset($cart->id_address_delivery) || !$cart->id_address_delivery)} unvisible{/if}">
					<span class="price_title">{l s='Total shipping'}</span>
					<span class="price" id="total_shipping">{l s='Free Shipping!'}</span>
				</div>
			{else}
				<div class="price_text clearfix{if $total_shipping_tax_exc <= 0} unvisible{/if}">
					<span class="price_title">{l s='Total shipping'}</span>
					<span class="price" id="total_shipping" >{displayPrice price=$total_shipping_tax_exc}</span>
				</div>
			{/if}
			<div class="price_text clearfix">
				<span class="price_title">{l s='Всього'}</span>
				<span class="price">--</span>
			</div>
			<div class="price_text to_pay clearfix">
				<span class="price_title">{l s='До сплати'}</span>
				<span class="price" id="total_price">{displayPrice price=$total_price_without_tax}</span>
			</div>
		</div>
	</div>




	{if $show_option_allow_separate_package}
	<p>
		<label for="allow_seperated_package" class="checkbox inline">
			<input type="checkbox" name="allow_seperated_package" id="allow_seperated_package" {if $cart->allow_seperated_package}checked="checked"{/if} autocomplete="off"/>
			{l s='Send available products first'}
		</label>
	</p>
	{/if}


	<div id="HOOK_SHOPPING_CART">{$HOOK_SHOPPING_CART}</div>
	<div class="clear"></div>
	<div class="cart_navigation_extra">
		<div id="HOOK_SHOPPING_CART_EXTRA">{if isset($HOOK_SHOPPING_CART_EXTRA)}{$HOOK_SHOPPING_CART_EXTRA}{/if}</div>
	</div>
{strip}
{addJsDef deliveryAddress=$cart->id_address_delivery|intval}
{addJsDefL name=txtProduct}{l s='product' js=1}{/addJsDefL}
{addJsDefL name=txtProducts}{l s='products' js=1}{/addJsDefL}
{/strip}
{/if}
