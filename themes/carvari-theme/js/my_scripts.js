/**
 * Created by sergg on 23.07.2015.
 */
function vd($value){
    console.log($value);
}

$(document).ready(function(){

    if ( $('.main_content').hasClass('history-page') ) {
        $('#orders-more').on('click', function(){
            var offset = $(this).attr('data-offset');
            // console.log('here');
            var query = $.ajax({
                type: 'POST',
                url: baseDir + 'order-history/',
                data: 'ajax=1&offset=' + offset,
                dataType: 'html',
                beforeSend: function() {
                },
                success: function(json) {
                    var response = JSON.parse(json);
                    if (response.continue) {
                        $('#orders-list').append(response.orders_list);
                        $('#orders-more').attr('data-offset', parseInt( $('#orders-more').attr('data-offset') )+1);
                    } else {
                        $('#orders-list').append(response.orders_list);
                        $('#orders-more').attr('data-offset', parseInt( $('#orders-more').attr('data-offset') )+1);
                        $('#orders-more').hide();
                    }
                }
            });
        })

    }

    if ( $('.main_content').hasClass('profile') ) {
        $('.profile_nav > span').on ('click', function() {
            $('.tab_wrapper').prepend('<span><img src="/themes/carvari-theme/img/preloader.gif"></span>');
        });

    }

    if ( $('.main_content').hasClass('product-page') ) {

        //Кнопка купить
        $('#add_to_cart .order_button').on('click', function(e){
            // ga('send', 'event', { eventCategory: 'Корзина', eventAction: 'Клик'});

            if( ! $(this).hasClass('btn-outline') ){
                $(this).addClass('btn-outline');
                $(this).find('span').text($(this).find('span').attr('data-inbasket-translate'))
            }
        });

        $('.sizes_block li a').on('click', function(event){
            event.preventDefault();
            $(this).parent('li').find('input').attr('checked','checked');
            $(this).parent('li').find('input').click();
            $('.sizes_block li.active').removeClass('active');
            $(this).parent('li').addClass('active');
        });
    }

    var baseUrl;
    if($('html')[0].lang === 'ru-ru'){
        var getUrl = window.location;
        baseUrl = '/' + getUrl.pathname.split('/')[1];
    }else{
        baseUrl = '';
    }

    if ( $('body#authentication') ) {
        $('#recovery-button').on( 'click', function(event){
            event.preventDefault();
            var phone = $('#login_phone').attr('value');
            if (phone != ''){
                location.href = baseUrl+'/password-recovery/?login_phone='+phone;
            } else {
                location.href = baseUrl+'/password-recovery/';
            }
        })
    }

    $('#login_form_ff').on('submit', function(event) {
        event.preventDefault();
        var log = $('input[name="login_phone_ff"]').val();
        var pass = $('input[name="login_passwd_ff"]').val();
        $.ajax({
            url: '/authentication/?rand=' + new Date().getTime(),
            type: 'post',
            data: '&SubmitLoginFf=true&ajax=true&phone='+  log +'&passwd='+ pass,
            dataType: 'json',
            beforeSend: function() {
                $('#login_form_content #login_phone_errors').hide();
                $('#login_form_content #login_passwd_errors').hide();
                $('#login_form_content #login_phone_errors').parent().removeClass('wrong');
                $('#login_form_content #login_passwd_errors').parent().removeClass('wrong');
            },
            complete: function() {

            },
            success: function(json) {
                var static_token = json.token;
                if (json.hasError) {

                    if(json.errors.phone)
                    {
                        $('#login_form_content #login_phone_errors').html(json.errors.phone).slideDown('slow');
                        $('#login_form_content #login_phone_errors').parent().addClass('wrong');
                    }
                    if(json.errors.password)
                    {
                        $('#login_form_content #login_passwd_errors').html(json.errors.password).slideDown('slow');
                        $('#login_form_content #login_passwd_errors').parent().addClass('wrong');
                    }

                }else{
                    if ( $('#orderform').length ) {
                        window.location.reload();
                    } else {
                        window.location.href = baseUrl+'/carvaricard';
                    }
                }

            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });



});
