/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
$(document).ready(function() {

    $('.subscribe_popup__close').on('click', function () {
        $.ajax({
            type: 'POST',
            url: baseDir + 'modules/blocknewsletter/ajax.php',
            data: 'action=popup_close',
            dataType: 'html',
            beforeSend: function() {

            },
            success: function(json) {
                response = JSON.parse(json);
                if(response.status == 'ok'){
                    $('.subscribe_popup').hide();
                }

            }
        });
    });

    $('.subscribe_block').on('submit', function(event){
        event.preventDefault();
        var $subscribe_block = $('.subscribe_block');
        var $email_input = $subscribe_block.find('.email_input');
        var email_input_val = $email_input.val();
        var $subscribe_success = $('.newsletter_success_msg');
        if (email_input_val && email_input_val.length || email_input_val == '')
        {
            var query = $.ajax({
                type: 'POST',
                url: baseDir + 'modules/blocknewsletter/ajax.php',
                data: 'action=0&email=' + email_input_val,
                dataType: 'html',
                beforeSend: function() {
                    $('#newsletter_error.error_msg').hide();
                    $('#newsletter-input').removeClass('wrong');
                },
                success: function(json) {
                    var response = JSON.parse(json);
                    if(response.status == 'error'){
                        $('#newsletter-input').addClass('wrong');
                        $('#newsletter_error').html(response.msg).slideDown('slow');
                    } else {
                        $email_input.val('');
                        $subscribe_success.html(response.msg);
                        $subscribe_success.fadeIn();
                        setTimeout(function () {
                            $subscribe_success.fadeOut();
                        }, 3000);
                    }
                }
            });
        }
    });

    $('.subscribe_popup__form').on('submit', function(e){
        e.preventDefault();
        var $popup_form = $(e.target);
        var email_input_val = $popup_form.find('.subscribe_popup__input').val();
        if (email_input_val && email_input_val.length || email_input_val == '')
        {
            var query = $.ajax({
                type: 'POST',
                url: baseDir + 'modules/blocknewsletter/ajax.php',
                data: 'action=0&popup=1&email=' + email_input_val,
                dataType: 'html',
                beforeSend: function() {
                    $('.subscribe_popup .subscribe_popup__notification').hide();
                },
                success: function(json) {
                    console.log(json);
                    response = JSON.parse(json);
                    if(response.status == 'error'){
                        $('.subscribe_popup .subscribe_popup__notification').html(response.msg).slideDown('slow');
                    } else {
                        $('.subscribe_popup .subscribe_popup__notification').html(response.msg).slideDown('slow', function () {
                            setTimeout(function(){
                                $.ajax({
                                    type: 'POST',
                                    url: baseDir + 'modules/blocknewsletter/ajax.php',
                                    data: 'action=popup_close',
                                    dataType: 'html',
                                    beforeSend: function() {

                                    },
                                    success: function(json) {
                                        response = JSON.parse(json);
                                        if(response.status == 'ok'){
                                            $('.subscribe_popup').hide();
                                        }

                                    }
                                });
                            }, 3000);
                        });
                    }
                }
            });
        }
    });

    $('.sub_man').click(function () {
        $('#gender').val('man');
    });
    $('.sub_woman').click(function () {
        $('#gender').val('woman');
    });

    $('.subscribe_form').on('submit', function(e){
        e.preventDefault();
        var $popup_form = $(e.target);
        var email_input_val = $popup_form.find('.subscribe_popup__input').val();
        var gender =  $('#gender').val();
        var $messageBlock = $('.subscribe_form .subscribe_form__notification');

        if (email_input_val && email_input_val.length || email_input_val == '')
        {
            var query = $.ajax({
                type: 'POST',
                url: baseDir + 'modules/blocknewsletter/ajax.php',
                data: 'action=0&email=' + email_input_val+ '&gender=' + gender,
                dataType: 'html',
                beforeSend: function() {
                    $messageBlock.hide();
                    $messageBlock.removeClass('error');
                    $messageBlock.removeClass('success');
                },
                success: function(json) {
                    response = JSON.parse(json);
                    if(response.status == 'error'){
                        $messageBlock.addClass('error');
                        $messageBlock.html(response.msg).slideDown('slow');
                    } else {
                        $messageBlock.addClass('success');
                        $messageBlock.html(response.msg).slideDown('slow');
                    }
                }
            });
        }
    });
});
