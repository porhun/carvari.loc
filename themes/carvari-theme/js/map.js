var map,geocoder,marker,currMarker,infowindow,contentString,myIcon;
function load_map(lat, long, firstload) {
    var mapCanvas = document.getElementById('is_map');
    var mapOptions = {
        center: new google.maps.LatLng(lat, long),
        zoom: 6,
        disableDefaultUI: true,
        //scrollwheel: false,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "saturation": "2"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels",
                "stylers": [
                    {
                        "saturation": "-28"
                    },
                    {
                        "lightness": "-10"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                featureType: "administrative.country",
                elementType: "geometry.stroke",
                stylers: [
                    { visibility: "on" }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#363636"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "saturation": "-1"
                    },
                    {
                        "lightness": "-12"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "lightness": "-31"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "lightness": "-74"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "lightness": "65"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry",
                "stylers": [
                    {
                        "lightness": "-15"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": "0"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "saturation": "0"
                    },
                    {
                        "lightness": "-9"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "lightness": "-14"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "lightness": "-35"
                    },
                    {
                        "gamma": "1"
                    },
                    {
                        "weight": "1.39"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "lightness": "-19"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "lightness": "46"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "lightness": "-13"
                    },
                    {
                        "weight": "1.23"
                    },
                    {
                        "invert_lightness": true
                    },
                    {
                        "visibility": "simplified"
                    },
                    {
                        "hue": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        //"color": "#c4c4c4"
                        "color": "#b0b0b0"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ],
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };


    //marker = new google.maps.Marker({
    //    position: new google.maps.LatLng(48.4656225, 35.0114423),
    //    map: map,
    //    icon: myIcon,
    //    optimized: false,
    //    flat: true
    //});



    if (firstload==1) {

        map = new google.maps.Map(mapCanvas, mapOptions);
        geocoder = new google.maps.Geocoder();
        myIcon = new google.maps.MarkerImage("/themes/carvari-theme/img/facebook_placeholder_marker.svg", null, null, null, new google.maps.Size(29, 40));

        $.each(stores_markers, function(k,v) {
            $.each(v, function (k, h) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(h.latitude, h.longitude),
                    map: map,
                    icon: myIcon,
                    optimized: false,
                    flat: true
                });

                google.maps.event.addListener(marker, 'click', function (event) {
                    currMarker = this;
                    var marker_lat = this.getPosition().lat();
                    var $elem = $('[data-lat^="'+marker_lat+'"]').closest('.store_block');
                    $elem.trigger('click');
                    var value_for_select = $elem.closest('.tab-pane').attr('id');
                    $('#city-chooser').val(value_for_select).trigger('refresh').trigger('change')
                });
            })
        });

        $('.tab-pane').removeClass('none');
        $('.info_layer').mCustomScrollbar();


    }


    //contentString =
    //
    //    '<div id="content">'+
    //    '<div class="imgs">'+
    //    '<a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>'+
    //    '<a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>'+
    //    '<a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>'+
    //    '</div>'+
    //    '</div>';
    //
    infowindow = new google.maps.InfoWindow({
        content: ''
    });
}

function setMarker(latlng){
    marker = new google.maps.Marker({
        position: latlng,
        map: map,
        icon: myIcon,
        optimized: false,
        flat: true
    })
}

$(document).ready(function() {


    $('.map_block select').styler();
    load_map(49.0680195, 33.6462376,1);

    $('.store_block').on('click',function(){
        infowindow.close();
        $('.store_block').removeClass('active');
        $(this).addClass('active');
        var lat = $(this).find('.info').attr('data-lat');
        var lng = $(this).find('.info').attr('data-lng');
        setMarker( new google.maps.LatLng(lat,lng) );
        map.panTo( new google.maps.LatLng(lat,lng) );
        map.setZoom(17);
        if ( $(this).attr('data-img1') || $(this).attr('data-img2') || $(this).attr('data-img3') ) {

        contentString =
            '<div id="content"><div class="imgs">';
            if ($(this).attr('data-img1')) {
                contentString  =  contentString + '<a class="stores_preview" href="'+$(this).attr('data-img1-full')+'"><img src="'+$(this).attr('data-img1')+'" width="150px" alt=""/></a>';
            }
            if ($(this).attr('data-img2')) {
                contentString  =  contentString + '<a class="stores_preview" href="'+$(this).attr('data-img2-full')+'"><img src="'+$(this).attr('data-img2')+'" width="150px" alt=""/></a>';
            }
            if ($(this).attr('data-img2')) {
                contentString  =  contentString + '<a class="stores_preview" href="'+$(this).attr('data-img3-full')+'"><img src="'+$(this).attr('data-img3')+'" width="150px" alt=""/></a>';
            }
            //'<a class="stores_preview" href="'+$(this).attr('data-img1')+'"><img src="'+$(this).attr('data-img1')+'" width="200px" alt=""/></a>'+
            //'<a class="stores_preview" href="'+$(this).attr('data-img2')+'"><img src="'+$(this).attr('data-img2')+'" width="200px" alt=""/></a>'+
            //'<a class="stores_preview" href="'+$(this).attr('data-img3')+'"><img src="'+$(this).attr('data-img3')+'" width="200px" alt=""/></a>';
            contentString  =  contentString + '</div></div>';

            infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            infowindow.open(map, marker);

        }

        $("a.stores_preview").fancybox();
    });

    if (isMobile) {
        $('#city-chooser').change(function(e) {
            panel_id = $(this).attr('value');

            if ( infowindow ) { infowindow.close(); }
            if (panel_id == 'all') {
                $('.map-page .slider').hide();
                map.setZoom(6);
                map.panTo({
                    lat: 49.530127,
                    lng: 30.397271
                });
                return true;
            } else {
                var address = $(this).find('option[value="'+panel_id+'"]').html();
                //console.log(address);
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setZoom(12);
                        map.setCenter(results[0].geometry.location);
                    } else {
                        console.log("Geocode was not successful for the following reason: " + status);
                    }
                });
            }
            $('.slider').hide();
            $('#'+panel_id).show();

            if (!$('#'+panel_id).hasClass('slick-initialized')){

                $('#'+panel_id).slick({
                    slidesToShow: 1,
                    arrows: false,
                    dots: true,
                    slidesToScroll: 1,
                    swipeToSlide: true
                });

            }

        });
    } else {
        $('#city-chooser').on('change',function(e) {
            panel_id = $(this).attr('value');

            if ( infowindow ) { infowindow.close(); }
            if (panel_id == 'all') {
                $('.tab-content > div').removeClass('none');
                map.setZoom(6);
                map.panTo({
                    lat: 49.0680195,
                    lng: 33.6462376
                });
                return true;
            }

            $('.tab-pane').addClass('none');
            $('#'+panel_id).removeClass('none');
            $('#city_name').css('display','inline').html($('option[value='+panel_id+']').html());
        });
    }


});