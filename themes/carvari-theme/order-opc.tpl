{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $opc}
	{assign var="back_order_page" value="order-opc.php"}
	{else}
	{assign var="back_order_page" value="order.php"}
{/if}

{if $PS_CATALOG_MODE}
	{capture name=path}{l s='Your shopping cart'}{/capture}
	<h2 id="cart_title">{l s='Your shopping cart'}</h2>
	<p class="alert alert-warning">{l s='Your new order was not accepted.'}</p>
{else}
	<div class="main_content checkout">
		<div class="min_width clearfix">
			{if $productNumber}
				<div class="left_content">
					<h1>{l s='Реєстрація та офрмлення'}</h1>
					<div class="profile_nav">
						<span data-tab="tab1" class="profile_nav_link ">{l s='Постійний покупець'}
							<span class="underline"></span>
						</span>
						<span data-tab="tab2" class="profile_nav_link active">{l s='Купую вперше'}
							<span class="underline"></span>
						</span>
					</div>
					<div class="tab_wrapper">
						<div id="tab1" class="tab_content"  style="display: none;">

							<form action="{$link->getPageLink('authentication', true, NULL, "back=order-opc")|escape:'html':'UTF-8'}" method="post" id="login_form">
								<!-- Error return block -->
								{*<div id="opc_login_errors" class="alert alert-danger" style="display:none;"></div>*}
								<!-- END Error return block -->
								<div class="input_block">
									<label>{l s='Телефон'}</label>
									<input class="validate" type="tel" id="login_phone" name="phone" value="+38" data-validate="isPhoneNumber" />
								</div>
								<div class="input_block">
									<label>{l s='Пароль'}</label>
									<input class="validate" type="password" id="login_passwd" name="login_passwd" data-validate="isPasswd" />
									<span id="opc_login_errors" class="error_msg">{l s='Error'}</span>
								</div>
								<div class="btn_holder login">
									<input type="button" id="SubmitLoginFf" class="button big" value="Увійти">
									<a href="{$link->getPageLink('password', true)|escape:'html':'UTF-8'}">{l s='Нагадати пароль'}</a>
								</div>
							</form>
						</div>
						<div id="tab2" class="tab_content">

							{include file="$tpl_dir./order-opc-new-account.tpl"}

							<!-- Carrier -->
							{include file="$tpl_dir./order-carrier.tpl"}
							<!-- END Carrier -->

							<!-- Payment -->
							{include file="$tpl_dir./order-payment.tpl"}
							<!-- END Payment -->


							<div class="btn_holder">
								<input type="button" class="button big" value="{l s='Підтвердити'}">
							</div>
						</div>
					</div>
				</div>
				<div class="right_content">
					{include file="$tpl_dir./shopping-cart.tpl"}
				</div>




				<div class="clearfix"></div>

				<br/>
				<br/>
				<br/>
				<br/>





				<!-- Shopping Cart -->


				<!-- End Shopping Cart -->
				{*{if $is_logged AND !$is_guest}*}
					{*{include file="$tpl_dir./order-address.tpl"}*}
				{*{else}*}
					{*<!-- Create account / Guest account / Login block -->*}
					{*{include file="$tpl_dir./order-opc-new-account.tpl"}*}
					{*<!-- END Create account / Guest account / Login block -->*}
				{*{/if}*}
				{*<!-- Carrier -->*}
				{*{include file="$tpl_dir./order-carrier.tpl"}*}
				{*<!-- END Carrier -->*}

				{*<!-- Payment -->*}
				{*{include file="$tpl_dir./order-payment.tpl"}*}
				{*<!-- END Payment -->*}
			{else}
				{capture name=path}{l s='Your shopping cart'}{/capture}
				<h2 class="page-heading">{l s='Your shopping cart'}</h2>
				{include file="$tpl_dir./errors.tpl"}
				<p class="alert alert-warning">{l s='Your shopping cart is empty.'}</p>
			{/if}
		</div>
	</div>
{strip}
{addJsDef imgDir=$img_dir}
{addJsDef authenticationUrl=$link->getPageLink("authentication", true)|escape:'quotes':'UTF-8'}
{addJsDef orderOpcUrl=$link->getPageLink("order-opc", true)|escape:'quotes':'UTF-8'}
{addJsDef historyUrl=$link->getPageLink("history", true)|escape:'quotes':'UTF-8'}
{addJsDef guestTrackingUrl=$link->getPageLink("guest-tracking", true)|escape:'quotes':'UTF-8'}
{addJsDef addressUrl=$link->getPageLink("address", true, NULL, "back={$back_order_page}")|escape:'quotes':'UTF-8'}
{addJsDef orderProcess='order-opc'}
{addJsDef guestCheckoutEnabled=$PS_GUEST_CHECKOUT_ENABLED|intval}
{addJsDef displayPrice=$priceDisplay}
{addJsDef taxEnabled=$use_taxes}
{addJsDef conditionEnabled=$conditions|intval}
{addJsDef vat_management=$vat_management|intval}
{addJsDef errorCarrier=$errorCarrier|@addcslashes:'\''}
{addJsDef errorTOS=$errorTOS|@addcslashes:'\''}
{addJsDef checkedCarrier=$checked|intval}
{addJsDef addresses=array()}
{addJsDef isVirtualCart=$isVirtualCart|intval}
{addJsDef isPaymentStep=$isPaymentStep|intval}
{addJsDefL name=txtWithTax}{l s='(tax incl.)' js=1}{/addJsDefL}
{addJsDefL name=txtWithoutTax}{l s='(tax excl.)' js=1}{/addJsDefL}
{addJsDefL name=txtHasBeenSelected}{l s='has been selected' js=1}{/addJsDefL}
{addJsDefL name=txtNoCarrierIsSelected}{l s='No carrier has been selected' js=1}{/addJsDefL}
{addJsDefL name=txtNoCarrierIsNeeded}{l s='No carrier is needed for this order' js=1}{/addJsDefL}
{addJsDefL name=txtConditionsIsNotNeeded}{l s='You do not need to accept the Terms of Service for this order.' js=1}{/addJsDefL}
{addJsDefL name=txtTOSIsAccepted}{l s='The service terms have been accepted' js=1}{/addJsDefL}
{addJsDefL name=txtTOSIsNotAccepted}{l s='The service terms have not been accepted' js=1}{/addJsDefL}
{addJsDefL name=txtThereis}{l s='There is' js=1}{/addJsDefL}
{addJsDefL name=txtErrors}{l s='Error(s)' js=1}{/addJsDefL}
{addJsDefL name=txtDeliveryAddress}{l s='Delivery address' js=1}{/addJsDefL}
{addJsDefL name=txtInvoiceAddress}{l s='Invoice address' js=1}{/addJsDefL}
{addJsDefL name=txtModifyMyAddress}{l s='Modify my address' js=1}{/addJsDefL}
{addJsDefL name=txtInstantCheckout}{l s='Instant checkout' js=1}{/addJsDefL}
{addJsDefL name=txtSelectAnAddressFirst}{l s='Please start by selecting an address.' js=1}{/addJsDefL}
{addJsDefL name=txtFree}{l s='Free' js=1}{/addJsDefL}

{capture}{if $back}&mod={$back|urlencode}{/if}{/capture}
{capture name=addressUrl}{$link->getPageLink('address', true, NULL, 'back='|cat:$back_order_page|cat:'?step=1'|cat:$smarty.capture.default)|escape:'quotes':'UTF-8'}{/capture}
{addJsDef addressUrl=$smarty.capture.addressUrl}
{capture}{'&multi-shipping=1'|urlencode}{/capture}
{addJsDef addressMultishippingUrl=$smarty.capture.addressUrl|cat:$smarty.capture.default}
{capture name=addressUrlAdd}{$smarty.capture.addressUrl|cat:'&id_address='}{/capture}
{addJsDef addressUrlAdd=$smarty.capture.addressUrlAdd}
{addJsDef opc=$opc|boolval}
{capture}<h3 class="page-subheading">{l s='Your billing address' js=1}</h3>{/capture}
{addJsDefL name=titleInvoice}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<h3 class="page-subheading">{l s='Your delivery address' js=1}</h3>{/capture}
{addJsDefL name=titleDelivery}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{capture}<a class="button button-small btn btn-default" href="{$smarty.capture.addressUrlAdd}" title="{l s='Update' js=1}"><span>{l s='Update' js=1}<i class="icon-chevron-right right"></i></span></a>{/capture}
{addJsDefL name=liUpdate}{$smarty.capture.default|@addcslashes:'\''}{/addJsDefL}
{/strip}
{/if}
