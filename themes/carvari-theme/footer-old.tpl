{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if $page_name != 'index'}
</div> {*//.container *}
{/if}

{if !isset($content_only) || !$content_only}
					{*</div><!-- #center_column -->*}
					{*{if isset($right_column_size) && !empty($right_column_size)}*}
						{*<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>*}
					{*{/if}*}
					{*</div><!-- .row -->*}
				{*</div><!-- #columns -->*}
			{*</div><!-- .columns-container -->*}
			{*{if isset($HOOK_FOOTER)}*}
				{*<!-- Footer -->*}
				{*<div class="footer-container">*}
					{*<footer id="footer"  class="container">*}
						{*<div class="row">{$HOOK_FOOTER}</div>*}
					{*</footer>*}
				{*</div><!-- #footer -->*}
			{*{/if}*}
    <div class="footer_wrapper">
        <div class="footer">
            <div class="min_width footer_min_width">
                <div class="footer_top">
                    <div class="left_foot_block">
                        <div class="footer_column footer_column--1">
                            <div class="logo_holder">
                                <a href="{$base_dir}" class="logo">
                                    <span class="brand">L’CARVARI</span>
                                    <span class="year">{$smarty.now|date_format:'Y'}</span>
                                </a>
                                <span class="logo_slogan">{l s='делаем эстетику модной'}</span>
                            </div>
		                    {hook h='displaySocials' mod='blocksocial'}
                            <div class="schedule">
                                <div class="schedule__item">{l s='Будні - з 9:00 по 19:00'}</div>
                                <div class="schedule__item">{l s='Субота - з 10:00 по 18:00'}</div>
                                <div class="schedule__item">{l s='Неділя - вихідний'}</div>
                            </div>
                            <div class="footer_phone">
                                <span class="icon icon--phone"></span>
                                0 800 211 009
                            </div>
                        </div>
                        <div class="footer_column footer_column--2">
                            <ul class="footer_menu_column">
                                <li class="footer_menu_column__item">
                                    <span class="footer_menu_title">ЗАГЛАВИЕ</span>
                                    <ul class="footer_menu_list">
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Личный кабинет</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Контакты</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Оплата</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Доставка</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Магазины</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Гарантия</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Обмен</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer_menu_column__item">
                                    <span class="footer_menu_title">ЗАГЛАВИЕ</span>
                                    <ul class="footer_menu_list">
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Личный кабинет</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Контакты</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Оплата</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Доставка</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Магазины</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Гарантия</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Обмен</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="footer_menu_column__item">
                                    <span class="footer_menu_title">ЗАГЛАВИЕ</span>
                                    <ul class="footer_menu_list">
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Личный кабинет</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Контакты</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Оплата</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Доставка</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Магазины</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Гарантия</a>
                                        </li>
                                        <li class="footer_menu_list__item">
                                            <a href="#" class="footer_menu_list__link">Обмен</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="foot_right_block">
                        <div class="footer_subscribe_holder">
                            <div class="footer_subscribe">
                                <div class="footer_subscribe__title">
			                        {l s='Реєструйтеся та будьте в курсі нових акцій'}
                                </div>
                                <form action="{$link->getPageLink('index', true)|escape:'html'}" method="post" class="footer_subscribe__form">
                                    <input type="text" class="footer_subscribe__input" placeholder="your@email.com" >
                                    <div class="footer_subscribe__controls">
                                        <button type="submit" class="footer_subscribe__submit">
                                            {l s='Жінка' mod='blocknewsletter'}
                                        </button>
                                        <button type="submit" class="footer_subscribe__submit">
                                            {l s='Чоловік' mod='blocknewsletter'}
                                        </button>
                                    </div>
                                </form>
                                <div class="footer_subscribe__notification" style="display: none;"></div>
                            </div>
                        </div>
                        <div class="footer_payments">
                            <div class="footer_payments__title">
                                <span class="icon icon--lock"></span>
                                {l s='Гарантия безопасной покупки'}
                            </div>
                            <ul class="footer_payments_list">
                                <li class="footer_payments_list__item">
                                    <img src="{$tpl_uri}img/icon/payments/visa.png" alt="visa" class="footer_payments_list__img">
                                </li>
                                <li class="footer_payments_list__item">
                                    <img src="{$tpl_uri}img/icon/payments/mastercard.png" alt="mastercard" class="footer_payments_list__img">
                                </li>
                                <li class="footer_payments_list__item">
                                    <img src="{$tpl_uri}img/icon/payments/privat24.png" alt="privat24" class="footer_payments_list__img">
                                </li>
                                <li class="footer_payments_list__item">
                                    <img src="{$tpl_uri}img/icon/payments/liqpay.png" alt="liqpay" class="footer_payments_list__img">
                                </li>
                                <li class="footer_payments_list__item">
                                    <img src="{$tpl_uri}img/icon/payments/np.png" alt="np" class="footer_payments_list__img">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="footer_copyright">
                    <div class="fleft">
                        <p>© 2009 - {$smarty.now|date_format:'Y'} {l s='L\'CARVARI'}</p>
                        <div class="forforce">
		                    {l s='зроблено в' mod='blocksocial'}
                            <a href="http://forforce.com/" target="_blank"></a>
                        </div>
                    </div>
                    <div class="fright footer_copyright__text">
                        <p>{l s='Мережа фірмових магазинів взуття та аксесуарів. Всі права на матеріали, розміщені на сайті, охороняються відповідно  до  законодавства  України  Матеріали  не  можуть  бути  використані  без  письмового дозволу'}</p>
                        <p class="footer_copyright__fop">{l s='ФОП Подопригора.'}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
	</body>
</html>