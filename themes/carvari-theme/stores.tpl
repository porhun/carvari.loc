{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Our stores'}{/capture}



<!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHYfXB4pYh-IQ4U8A8p7gShMtkF8VXQmg&sensor=false&v=3.exp&signed_in=true&language=en"></script>-->




<div class="body map-page">
	<div class="main_content">

		{*{$stores_by_city|var_dump}*}

		<div>
		{*<div class="min_width">*}
			<h1>{l s='Нашi магазини в мiстах України'}<span id="city_name">{$store_name}</span></h1>
			<div class="map_block">
				{*<div class="imgs">*}
					{*<a class="stores_preview" href="717-large_default/botinki-zhenskie-l-karvari-43254499s.jpg"><img src="717-large_default/botinki-zhenskie-l-karvari-43254499s.jpg" alt=""/></a>*}
					{*<a class="stores_preview" href="719/botinki-zhenskie-l-karvari-43254499s.jpg"><img src="719/botinki-zhenskie-l-karvari-43254499s.jpg" alt=""/></a>*}
					{*<a class="stores_preview" href="717-large_default/botinki-zhenskie-l-karvari-43254499s.jpg"><img src="717-large_default/botinki-zhenskie-l-karvari-43254499s.jpg" alt=""/></a>*}
				{*</div>*}
				<div id="is_map"></div>
				<div class="info_layer">

					<select class="city_chooser" id="city-chooser">
						<option value="all">{l s='Усі'}</option>
						{foreach from=$stores_by_city key=store_city item=storess name=Storess}
							<option value="panel{$smarty.foreach.Storess.iteration}">{$store_city}</option>
						{/foreach}
					</select>

					<div class="tab-content">
						{foreach from=$stores_by_city item=stores name=Stores}
							<div role="tabpanel" class="tab-pane" id="panel{$smarty.foreach.Stores.iteration}">
								<div class="js-stores">
									{foreach from=$stores item=store}
										<div class="store_block"
											 data-img1="{$store.img}" data-img1-full="{$store.imgfull}"
											 data-img2="{$store.img1}" data-img2-full="{$store.imgfull1}"
											 data-img3="{$store.img2}" data-img3-full="{$store.imgfull2}"
											>
											<div class="title">{$store.name}</div>
											<div class="info" data-lat="{$store.latitude}" data-lng="{$store.longitude}">
												{$store.address1}<br/>
												{$store.phone}<br/>
												{$store.hours_uns[0]}<br/>
											</div>
										</div>
									{/foreach}

								</div>
							</div>
						{/foreach}
					</div>
				</div>
			</div>
		</div>
	</div>
    {hook h="displayBlockBottomHome" mod="blockcmsinfo"}
</div>
<script src="https://maps.googleapis.com/maps/api/js?language=uk-ua"></script>
{*<script src="/themes/carvari-theme/js/map.js"></script>*}
{strip}
{*{addJsDef map=''}*}
{addJsDef stores_markers=$stores_by_city}
{*{addJsDef infoWindow=''}*}
{*{addJsDef locationSelect=''}*}
{*{addJsDef defaultLat=$defaultLat}*}
{*{addJsDef defaultLong=$defaultLong}*}
{*{addJsDef hasStoreIcon=$hasStoreIcon}*}
{*{addJsDef distance_unit=$distance_unit}*}
{*{addJsDef img_store_dir=$img_store_dir}*}
{*{addJsDef img_ps_dir=$img_ps_dir}*}
{*{addJsDef searchUrl=$searchUrl}*}
{*{addJsDef logo_store=$logo_store}*}
{*{addJsDefL name=translation_1}{l s='No stores were found. Please try selecting a wider radius.' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_2}{l s='store found -- see details:' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_3}{l s='stores found -- view all results:' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_4}{l s='Phone:' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_5}{l s='Get directions' js=1}{/addJsDefL}*}
{*{addJsDefL name=translation_6}{l s='Not found' js=1}{/addJsDefL}*}
{/strip}

