{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{*{if isset($branding_enabled) && $branding_enabled}*}
	{*{$branding = array()}*}
	{*{$branding.link = $branding_url} *}{*ссылка на бренд*}
	{*{$branding.image = $branding_img} *}{*ссылка на изображение для брендирования*}
	{*{$branding.color = '#FFF'} *}{*Фон подложки, если нужно*}
	{*{$branding.transparency = false} *}{*Виден ли фон брендирования сквозь блоки (или же блоки основного контента лежат на сплошном белом фоне)*}
	{*{$branding.enabled = true}*}
{*{/if}*}

{*<div class="branding">*}
	{*{if isset($branding) && $branding.enabled}*}
		{*<a href="{$branding.link}" class="branding__brand_layer" style="background-image: url({$branding.image}); {if $branding.color}background-color: {$branding.color};{/if}">*}

		{*</a>*}
	{*{/if}*}
	{*<div class="branding__holder {if isset($branding) && $branding.transparency} branding__holder--transparent{/if}">*}
	<div class="slider-wrap">
		{hook h="displaySlider" module="minicslider"}
	</div>
	<div class="point_none">
		<div class="main_content none-padding">
			<div class="wrapp_index_page main-page">

			{hook h="displayStatics5" module="pttabs"}
			{hook h="displayStatics6" module="pttabs"}
			
			{*{hook h="displayGenderBanner" module="themeconfigurator"}*}
			{hook h="displayBigBanner" module="themeconfigurator"}
			{hook h="displaySmallBanners" module="themeconfigurator"}
			{*{if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}*}
			{*{if isset($HOOK_HOME_TAB) && $HOOK_HOME_TAB|trim}*}
			{*<ul id="home-page-tabs" class="nav nav-tabs clearfix">*}
			{*{$HOOK_HOME_TAB}*}
			{*</ul>*}
			{*{/if}*}
			{*<div class="tab-content">{$HOOK_HOME_TAB_CONTENT}</div>*}
			{*{/if}*}

                {*Blog posts block*}
                {hook h='displaySimpleBlogPosts' mod='ph_simpleblog'}

                {*Subscriиe middle block*}
                <div class="middle_area__post post email middle_area__post_last">
                    <h4 class="email__title">{l s='Розсилка'}</h4>
                    <p class="email__text">
                        <span class="email__text_first">{l s='Новини'}</span>
                        <span class="email__text_second">{l s='зі світу моди, останні тренди та кращі акції!'}</span>
                    </p>
                    {hook h='blockFooter1' mod='posstatickfooter'}
                </div>
                <div class="clearfix"></div>
                <div class="grey-line"></div>

			{if isset($HOOK_HOME) && $HOOK_HOME|trim}
				<div class="clearfix">
					{$HOOK_HOME}
				</div>
			{/if}
                {hook h="displayBlockBottomHome" mod="blockcmsinfo"}
			{* ниже подкючается файл с версткой картинок из инстаграмма *}
			{*количество .instagram_list__point должно быть кратным 3-м*}
			{*{include file="web/html/11_instagram.php"}*}

			</div>
		</div>
	</div>
{*</div>*}




