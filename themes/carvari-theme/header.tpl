{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		{hook h='googletagmanagerafterheader' mod='ffgoogletagmanager'}
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="Carvarishop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		{*<meta name="viewport" content="width=device-width, minimum-scale=0.25, maximum-scale=1.6, initial-scale=1.0" />*}
		<meta name="apple-mobile-web-app-capable" content="yes" />
		<meta name="p:domain_verify" content="23ae5d5660374d81ad3be2fa9f9af49e"/>
        {foreach from=$languages key=k item=language name="languages"}
			<link rel="alternate" hreflang="{$language.iso_code|escape:'html':'UTF-8'}" href="{$link->getLanguageLink($language.id_lang)|escape:htmlall}"/>
        {/foreach}
		<link rel="icon" type="image/vnd.microsoft.icon" href="/favicon.ico?{$img_update_time}" />
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
			{/foreach}
			<link rel="stylesheet" href="/themes/carvari-theme/css/main.css" type="text/css" media="all">
		{/if}
		<script type="text/javascript" src="/themes/carvari-theme/js/EventEmitter.js"></script>
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			{$js_def}
			{foreach from=$js_files item=js_uri}
			<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
			{/foreach}
		{/if}
		{$HOOK_HEADER}
			<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://fonts.googleapis.com/css?family=Open+Sans:300,600&amp;subset=latin,latin-ext" type="text/css" media="all" />
		<!--[if IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		{*{literal}*}
			{*<!-- Facebook Pixel Code -->*}
			{*<script>*}
                {*!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?*}
                    {*n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;*}
                    {*n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;*}
                    {*t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,*}
                    {*document,'script','https://connect.facebook.net/en_US/fbevents.js');*}
                {*fbq('init', '416946418697016'); // Insert your pixel ID here.*}
                {*fbq('track', 'PageView');*}
			{*</script>*}
			{*<noscript><img height="1" width="1" style="display:none"*}
						   {*src="https://www.facebook.com/tr?id=416946418697016&ev=PageView&noscript=1"*}
				{*/></noscript>*}
			{*<!-- DO NOT MODIFY -->*}
			{*<!-- End Facebook Pixel Code -->*}
		{*{/literal}*}
		<script charset="UTF-8" src="//cdn.sendpulse.com/28edd3380a1c17cf65b137fe96516659/js/push/74657f20a4ad1b3b1bb68a180bec9ab4_1.js" async></script>
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} class="{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{else} show-left-column{/if}{if $hide_right_column} hide-right-column{else} hide-right-column{/if}{if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}">
	<!-- Google Tag Manager -->
    {hook h='googletagmanagerafterbody' mod='ffgoogletagmanager'}
	<!-- End Google Tag Manager -->
    {*{hook h='displayEcommerceAnalytics' mod='ffecommerceanalytics'}*}
	<!-- Products remarketing -->
    {hook h='displayProductRemarketing' mod='ffproductremarketing'}
	<!-- End Products remarketing -->
	{if !isset($content_only) || !$content_only}
		{if isset($restricted_country_mode) && $restricted_country_mode}
			<div id="restricted-country">
				<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
			</div>
		{/if}
	<div class="body_wrapper">
		<div class="content_wrapper">{*//закрывается в footer.tpl *}
            {if $page_name == 'order-opc' and !$mobile_device}
				<div class="header header--short">
					<div class="container">
						<div class="header__logo-wrap">
							<a href="{$base_dir}" class="head_logo hovered_logo">
								<div class="bird">
									<div class="body"></div>
									<div class="legs"></div>
								</div>
								<span class="l"></span>
								<div class="dot"></div>
								<span class="c"></span>
								<span class="a"></span>
								<span class="r"></span>
								<span class="v"></span>
								<span class="a2"></span>
								<span class="r2"></span>
								<span class="i"></span>
							</a>
							<div class="dot js_dot"></div>
						</div>
						<div class="header__info-wrap info-wrap">
							<p class="info-wrap__title">{l s='Консультації за телефоном:'}</p>
							<strong class="info-wrap__phone">
								0 800 211 009
							</strong>
							<div class="info-wrap__call">
								<span class="info-wrap__serious">
									<i class="info-wrap__icon"></i>
                                    {l s='Графік роботи кол-центру:'}
								</span>
								<div class="call_center">
									<p class="call_center__small_text">*{l s='Дзвінки з усіх  мобільних  та стаціонарних операторів України - безкоштовні.'}</p>
									<p class="info-wrap__serious">
										<i class="info-wrap__icon"></i>
                                        {l s='Графік роботи кол-центру:'}
									</p>
									<ul class="call_center__time">
										<li><strong>{l s='с 9:00 до 19:00'}</strong></li>
										<li>{l s='Субота: с 10:00 до 18:00'}</li>
										<li>{l s='Неділя: Вихідний'}</li>
									</ul>
									<span class="info-wrap__serious">{l s='Увага! Замовлення через сайт приймаються цілодобово.'}</span>
								</div>
							</div>
						</div>
					</div>
				</div>
            {else}
                {hook h="displayPopup" mod="blocknewsletter"}
				<div class="header">
				<div class="header_min_width header__logo-wrap">
					{if $shop_phones && $shop_phones|count}
					<div class="header__left phones">
						{foreach from=$shop_phones item=phone_item name='header_shop_phones'}
							<span class="phones__phone">{$phone_item}</span>
                        {/foreach}
					</div>
					{/if}
					{*<span style="float:left; padding:30px 0; font-size: 14px; color:#999">Сайт працює в тестовому режимі</span>*}
					{if $page_name == 'index'}
						<div class="head_logo hovered_logo">
					{else}
						<a href="{$base_dir}" class="head_logo hovered_logo">
					{/if}
						<div class="bird">
							<div class="body"></div>
							<div class="legs"></div>
						</div>
						<span class="l"></span>
						<div class="dot"></div>
						<span class="c"></span>
						<span class="a"></span>
						<span class="r"></span>
						<span class="v"></span>
						<span class="a2"></span>
						<span class="r2"></span>
						<span class="i"></span>
					{if $page_name == 'index'}
						</div>
					{else}
						</a>
					{/if}
					<div class="dot js_dot"></div>
				</div>

				<div class="header__menu-wrap">
					<div class="header_min_width">
						<div class="min_width_head">
							{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
						</div>
						<div class="header__right right-bl">
                            {hook h="displayLangTop" mod='blocklanguages'}
                            {hook h='displaySearch' mod='blocksearch'}
							<span><a href="{if $active_url_lang}{$active_url_lang}{/if}/carvaricard/" class="right-bl__login">{l s='Вхід'}</a></span>

                                {hook h='displayHeaderCart' mod='blockcart'}

						</div>
					</div>
				</div>
				
			</div>
			{/if}
		<div class="dropdown_menu dropdown_menu--large fixed" style="display: none;">
			<div class="js_menu_cont_inner inline-block">

			</div>
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" style="display:none">
			<symbol id="dropdown_arrow" viewBox="0 0 18 10">
				<polyline fill="none" stroke-width="3" stroke-miterlimit="10" points="1,1 9,9 17,1 "/>
			</symbol>
			<symbol id="facebook_icon" viewBox="0 0 13.833 25">
				<path d="M0.833,9h3V7.278V6.221V6.045c0-1.019,0.587-2.591,1.326-3.563C5.938,1.451,7.292,0.75,9.132,0.75
	c2.997,0,4.401,0.427,4.401,0.427L13.009,4.7c0,0-1.246-0.284-2.168-0.284c-0.926,0-2.008,0.328-2.008,1.254v0.375v1.233V9h4.37
	l-0.746,4H8.833v11h-5V13h-3V9L0.833,9z"/>
			</symbol>
			<symbol id="vk_icon" viewBox="0 0 19.833 25">
				<path d="M18.702,17.32c0,1.084-0.205,2.036-0.615,2.856s-0.962,1.499-1.655,2.036
			c-0.82,0.645-1.721,1.104-2.703,1.377C12.748,23.863,11.5,24,9.986,24H1.833V2h7.127c1.602,0,2.814,0.155,3.641,0.277
			c0.824,0.122,1.599,0.426,2.321,0.816c0.781,0.42,1.364,1.005,1.751,1.708c0.385,0.703,0.578,1.525,0.578,2.443
			c0,1.064-0.474,2.018-1.007,2.848c-0.531,0.83-1.411,1.446-2.411,1.837v0.117c2,0.283,2.634,0.862,3.444,1.736
			S18.702,15.836,18.702,17.32z M11.686,8.355c0-0.352-0.091-0.713-0.271-1.084s-0.461-0.64-0.842-0.806
			C10.211,6.31,9.783,6.13,9.291,6.116C8.797,6.101,8.063,6,7.086,6H6.833v5h0.56c0.938,0,1.594-0.109,1.97-0.128
			s0.765-0.171,1.165-0.357c0.439-0.205,0.742-0.513,0.908-0.875S11.686,8.834,11.686,8.355z M13.063,17.218
			c0-0.674-0.137-1.201-0.41-1.582s-0.684-0.669-1.23-0.864c-0.332-0.127-0.789,0.034-1.369,0.019C9.471,14.776,8.692,15,7.716,15
			H6.833v5h0.165c1.426,0,2.412-0.009,2.959-0.029s1.104-0.151,1.67-0.395c0.498-0.215,0.861-0.53,1.092-0.945
			C12.947,18.216,13.063,17.745,13.063,17.218z"/>
			</symbol>
			<symbol id="tt_icon" viewBox="0 0 28.833 25">
				<path d="M27,4.485c-0.951,0.422-1.973,0.707-3.045,0.835c1.094-0.656,1.936-1.695,2.33-2.933
	c-1.023,0.607-2.158,1.048-3.367,1.287C21.951,2.644,20.574,2,19.049,2c-2.928,0-5.302,2.373-5.302,5.301
	c0,0.415,0.048,0.82,0.138,1.208C9.479,8.288,5.571,6.178,2.958,2.97C2.501,3.753,2.24,4.664,2.24,5.635
	c0,1.839,0.752,3.462,2.176,4.412c-0.87-0.027-1.583-0.265-2.583-0.663c0,0.022,0,0.045,0,0.067c0,2.568,2.009,4.711,4.433,5.199
	c-0.443,0.121-0.82,0.186-1.304,0.186c-0.342,0-0.63-0.033-0.952-0.095c0.673,2.106,2.655,3.639,4.974,3.681
	c-1.813,1.422-4.088,2.27-6.572,2.27c-0.428,0-0.843-0.025-1.259-0.074C3.5,22.122,6.288,23,9.282,23
	c9.752,0,15.086-8.079,15.086-15.084c0-0.229-0.003-0.458-0.015-0.686C25.39,6.483,26.289,5.549,27,4.485z"/>
			</symbol>
			<symbol id="yt_icon" viewBox="0 0 29.833 25">
				<path d="M24.833,3h-20c-2.2,0-4,1.8-4,4v12c0,2.2,1.8,4,4,4h20c2.201,0,4-1.8,4-4V7C28.833,4.8,27.035,3,24.833,3z
	 M11.833,18V8l8.98,5L11.833,18z"/>
			</symbol>
			<symbol id="cart_icon" viewBox="0 0 29.833 33">
				<g>
					<path stroke="none" d="M26.538,9.299c-0.78,3.733-2.704,13.2-2.704,15.701c0,0.806,0.516,4.111,0.822,6H5.012
		c0.306-1.889,0.822-5.194,0.822-6c0-2.501-1.924-11.968-2.704-15.701C5.896,9.872,11.736,11,14.834,11S23.772,9.872,26.538,9.299
		 M27.834,8c0,0-9,2-13,2s-13-2-13-2s3,14,3,17c0,1-1,7-1,7h22c0,0-1-6-1-7C24.834,22,27.834,8,27.834,8L27.834,8z"/>
				</g>
				<path fill="none" stroke-linecap="square" stroke-miterlimit="10" d="M9.834,14l-1-5c0,0-1-7,6-7s6,7,6,7l-1,5"/>
				<circle stroke-linecap="square" stroke-miterlimit="10" cx="9.834" cy="14" r="1"/>
				<circle stroke-linecap="square" stroke-miterlimit="10" cx="19.834" cy="14" r="1"/>
			</symbol>
			<symbol id="subscribe_icon" viewBox="0 0 33 33">
				<rect x="0" y="0" width="33" height="33"/>
				<polyline fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" points="9,18 14,22 23,9 "/>
			</symbol>
			<symbol id="close_icon" viewBox="0 0 11 11">
				<path d="M0.881,0l4.28,4.381L9.442,0l0.88,0.88L6.021,5.261l4.301,4.38l-0.88,0.881L5.161,6.142l-4.28,4.381
		L0,9.642l4.281-4.38L0,0.88L0.881,0z"/>
			</symbol>

		</svg>

{/if}

	{$branding_enabled = false} {*брендирование*}

{if isset($branding_enabled) && $branding_enabled}
	{$branding = array()}
	{$branding.link = $branding_url} {*ссылка на бренд*}
	{$branding.image_top = 'http://placehold.it/1440x320/333'} {*ссылка на изображение вверху*}
	{$branding.image_left = 'http://placehold.it/300x1000/003'} {*ссылка на изображение слева*}
	{$branding.image_right = 'http://placehold.it/300x1000/030'} {*ссылка на изображение справа*}
	{$branding.color = '#FFF'} {*Фон подложки, если нужно*}

	<div class="branding_holder" style="{if $branding.color}background-color: {$branding.color};{/if}">
		<a href="{$branding.link}" class="branding_holder__link">
			<div class="full_container">
				<div class="brand_tc">
					<img src="{$branding.image_top}" alt="{$branding.link}">
				</div>
				<div class="brand_left">
					<img src="{$branding.image_left}" alt="{$branding.link}">
				</div>
				<div class="brand_right">
					<img src="{$branding.image_right}" alt="{$branding.link}">
				</div>
			</div>
		</a>
	</div>
{/if}

{if $page_name != 'index'}
	<div class="container{if $page_name=='category'} big{/if}"> {*//закрывается в footer.tpl *}
{/if}
