/**
 * Created by sergg on 23.07.2015.
 */
$(document).ready(function(){
    function vd($value){
        console.log($value);
    }

    $('#newsletter-submit').on('click', function(event){
        event.preventDefault();
        if ($('#newsletter-input').val() != '')
        {
            var query = $.ajax({
                type: 'POST',
                url: baseDir + 'modules/blocknewsletter/ajax.php',
                data: 'action=0&email=' + $('#newsletter-input').val(),
                dataType: 'html',
                success: function(json) {
                    response = JSON.parse(json);
                    $('#newsletter-input').attr('value', response.msg);
                }
            });
        }

    })
});