<!-- Block mymodule -->
{*{$panel}*}
<div id="uLogin" data-ulogin="display=buttons;fields=first_name,last_name,email,photo,photo_big;optional=phone;redirect_uri={$ulogin_redirect_uri};" class="wrap fbloginblock-connects text-center">
    <div class="auth-page-txt-before-logins">{l s='Connect with:' mod='ulogin'}</div>
    <a class="facebook custom-social-button-all custom-social-button-2" title="Facebook" data-uloginbutton="facebook">
        <i class="fa fa-facebook">F</i>
    </a>
    <a class="google custom-social-button-all custom-social-button-2" title="Google" data-uloginbutton="googleplus">
        <i class="fa fa-google-plus">G+</i>
    </a>
    <a class="instagram custom-social-button-all custom-social-button-2" title="Instagram" data-uloginbutton="instagram">
        <i class="fa fa-instagram">I</i>
    </a>


    {*<a class="linkedin custom-social-button-all custom-social-button-2" title="LinkedIn" data-uloginbutton="linkedin">*}
        {*<i class="fa fa-linkedin"></i>*}
    {*</a>*}

    {*<a class="twitter custom-social-button-all custom-social-button-2" title="Twitter" data-uloginbutton="twitter">*}
        {*<i class="fa fa-twitter"></i>*}
    {*</a>*}
    <div class="auth-page-txt-info-block">{l s='You can use any of the login buttons above to automatically create an account on our shop.' mod='ulogin'}</div>
</div>
<!-- /Block mymodule -->
{*{strip}*}
    {*{addJsDef ulogin_message=$ulogin_message|@addcslashes:'\''}*}
    {*{if isset($ulogin_message) && $ulogin_message}*}
        {*{addJsDefL name=alert_ulogin}{l s='uLogin : %1$s' sprintf=$ulogin_message js=1 mod="ulogin"}{/addJsDefL}*}
    {*{/if}*}
{*{/strip}*}
{if isset($message)}
    {$message}
{/if}
