{**
* 2002-2016 TemplateMonster
*
* TM One Click Order
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}
<h2 style="text-align: center;">{l s='Thanks for choosing us' mod='tmoneclickorder'}</h2>
<p style="text-align: center;">{l s='Our operator will contact you' mod='tmoneclickorder'}</p>
<p style="text-align: center;">
  <button class="btn btn-default">{l s='Close' mod='tmoneclickorder'}</button>
</p>