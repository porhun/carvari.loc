<?php
/**
 * 2002-2016 TemplateMonster
 *
 * TM One Click Order
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2016 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

require_once('../../../../config/config.inc.php');
require_once('../../../../init.php');
require_once('../../tmoneclickorder.php');

if (Tools::getValue('preorderForm')) {
    preorderForm();
} elseif (Tools::getValue('preorderSubmit')) {
    preorderSubmit();
}

exit;

/**
 * Get preorder form
 */
function preorderForm()
{
    $tmoneclickorder = new Tmoneclickorder();
    $iso = '';
    if(isset($_POST['isoLang']))
        $iso = $_POST['isoLang'];

    if (!$form = $tmoneclickorder->renderPreorderForm($iso)) {
        die(Tools::jsonEncode(array('status' => false)));
    }

    die(Tools::jsonEncode(array('status' => true, 'form' => $form, 'isoLang' => $iso)));
}

/**
 * On preorder submit
 */
function preorderSubmit()
{
    $tmoneclickorder = new Tmoneclickorder();
    $context = Context::getContext();

    $customer = Tools::jsonDecode(Tools::getValue('customer'));
    if (!$tmoneclickorder->validateCustomerInfo($customer)) {
        die(Tools::jsonEncode(
            array(
                'status' => false,
                'errors' => $tmoneclickorder->getErrors(true)
            )
        ));
    }

    $id_cart = $context->cookie->id_cart;
    $products = array();

    if (Tools::getValue('page_name') == 'product') {
        $products = array(Tools::jsonDecode(Tools::getValue('product'), true));
        $id_cart = false;
    }

    if (!$tmoneclickorder->createPreorder($customer, $id_cart, $products)) {
        die(Tools::jsonEncode(
            array(
                'status' => false
            )
        ));
    }
    $id_lang = '';
    if(isset($_POST['isoLang']))
        $id_lang = $_POST['isoLang'];

    if($id_lang == 'ru'){
        $content = '<h2 style="text-align: center;">Спасибо, что выбрали нас</h2><p style="text-align: center;">Наш оператор свяжется с вами в ближайшее время</p>';
    } else{
        $content = '<h2 style="text-align: center;">Дякуємо, що обрали нас</h2><p style="text-align: center;">Наш оператор зв\'яжеться з вами найближчим часом</p>';
    }
//    $content = ConfigurationCore::get('TMONECLICKORDER_SUCCESS_DESCRIPTION', $tmoneclickorder->id_lang);

    die(Tools::jsonEncode(
        array(
            'status' => true,
            'content' => $content
        )
    ));
}
