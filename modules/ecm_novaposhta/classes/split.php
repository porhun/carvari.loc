<?php
//НоваПошта
class split extends exec_ {
	
	public static
	function duplicateorder($oldorder, $order_detail){
		$order = new Order();
		foreach($oldorder as $key => $value){
			$order->$key = $value;
		}
		self::correct($oldorder, $order_detail, $order);
		// Creating order
		$order->add();
		
		$sql = "UPDATE `"._DB_PREFIX_."order_detail` 
			SET `id_order` = '".$order->id."'
			WHERE `id_order_detail` = '".$order_detail['id_order_detail']."'
			";
		Db::getInstance()->Execute($sql);

		$order_carrier = new OrderCarrier();
		$order_carrier->id_order = (int)$order->id;
		$order_carrier->id_carrier = (int)$order->id_carrier;
		$order_carrier->weight = (float)$order->getTotalWeight();
		$order_carrier->shipping_cost_tax_excl = (float)$order->total_shipping_tax_excl;
		$order_carrier->shipping_cost_tax_incl = (float)$order->total_shipping_tax_incl;
		$order_carrier->add();
		
		$sql = "SELECT * FROM `"._DB_PREFIX_."order_history` WHERE `id_order` = '".$order_detail['id_order']."'";
		$order_hist = Db::getInstance()->ExecuteS($sql);
		
		$order_history = new OrderHistory();
		foreach($order_hist[0] as $key => $value){
			$order_history->$key = $value;
		}
		$order_history->id_order = (int)$order->id;
		// Creating order_history
		$order_history->add();
		exec::copyOrder($order->id, true);
		exec::CostByOrder($order->id);
		$customer = new Customer($order->id_customer);
		$order = new Order($order->id);
		self::addCart($order);
		Hook::exec('newOrder', ['order' => $order, 'customer' => $customer], Module::getModuleIdByName('loyalty'));
	}

	public static
	function correctorder($oldorder, $order_detail){
		$order = new Order();
		foreach($oldorder as $key => $value){
			$order->$key = $value;
		}
		$order->id = $oldorder['id_order'];
		self::correct($oldorder, $order_detail, $order);
		$result = $order->update();
		self::updateCart($order);
		exec::copyOrder($order->id, true);
		exec::CostByOrder($order->id);
		$customer = new Customer($order->id_customer);
		$order = new Order($order->id);
		Hook::exec('newOrder', ['order' => $order, 'customer' => $customer], Module::getModuleIdByName('loyalty'));
	}

	public static
	function splitorder($id_order){
		$sql = "SELECT * FROM `"._DB_PREFIX_."orders` WHERE `id_order` = '$id_order'";
		$order = Db::getInstance()->ExecuteS($sql);
		$sql = "SELECT * FROM `"._DB_PREFIX_."order_detail` WHERE `id_order` = '$id_order'";
		$order_details = Db::getInstance()->ExecuteS($sql);
		
		$old_order = new Order($id_order);
		$old_order->setCurrentState(Configuration::get('PS_OS_CANCELED'), Tools::getValue('employee'));
		
		$first = false;
		foreach($order_details as $order_detail){
			if($first){
				self::correctorder ($order[0], $order_detail);
				$first = false;
				continue;
			}
			self::duplicateorder($order[0], $order_detail);
		}
		$customer = new Customer($old_order->id_customer);
		Hook::exec('UpdateOrderStatus', ['order' => $old_order, 'customer' => $customer], Module::getModuleIdByName('loyalty'));
	}
	
	public static
	function correct($oldorder, $order_detail, &$order){
		$order->total_products      = $order_detail['total_price_tax_incl'];
		$order->total_products_wt   = $order_detail['total_price_tax_incl'];
		$order->total_paid_tax_excl = $order_detail['total_price_tax_incl'] + $oldorder['total_shipping_tax_excl'];
		$order->total_paid_tax_incl = $order_detail['total_price_tax_incl'] + $oldorder['total_shipping_tax_incl'];
		$order->total_paid          = $order_detail['total_price_tax_incl'] + $oldorder['total_shipping'];
	}
	
	public static
	function addCart($order){
		$cart = new Cart();
		$cart->id_address_delivery = $order->id_address_delivery;
		$cart->id_address_invoice = $order->id_address_invoice;
		$cart->id_currency = $order->id_currency;
		$cart->id_lang = $order->id_lang;
		$cart->id_carrier = $order->id_carrier;
		$cart->id_customer = $order->id_customer;
		$cart->id_shop = $order->id_shop;
		$cart->add();
		$order->id_cart = $cart->id;
		$order->update();
		$order_products = $order->getProductsDetail();
		$cart->updateQty($order_products[0]['product_quantity'], $order_products[0]['product_id'], $order_products[0]['product_attribute_id']);
		$cart->update();
		
	}
	
	public static
	function updateCart($order){
		$cart = new Cart($order->id_cart);
		$order_products = $order->getProductsDetail();
		$order_product = $order_products[0]['product_id'].'&'.$order_products[0]['product_attribute_id'];
		$cart_products = $cart->getProducts();
		foreach ($cart_products as $product){
			if ($product['id_product'].'&'.$product['id_product_attribute'] != $order_product)
			$cart->deleteProduct($product['id_product'], $product['id_product_attribute']);
		}
		$cart->update();
		
		
	}
	
	
}
