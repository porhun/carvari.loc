<?php

class AdminContactMsgController extends ModuleAdminController
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->context = Context::getContext();
        $this->table = 'ffshopcontact_message';
        $this->className = 'ShopContactMsg';
        $this->identifier = 'id_customer_message';

        $this->lang = false;
        $this->explicitSelect = false;
        $this->deleted = false;

        $this->actions= array('edit', 'delete');
        $this->show_form_cancel_button = false;
        $this->_use_found_rows = true;

        $this->addRowAction('view');
        $this->addRowAction('delete');

        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );

        $this->fields_options = array(
            'contact' => array(
                'title' =>    $this->l('Settings'),
                'fields' =>    array(
                    'FF_SHOPCONTACT_ADMIN_EMAIL' => array(
                        'title' => $this->l('Admin email'),
                        'type' => 'text',
                        'class' => 'fixed-width-xxl',
                    ),
                    'FF_SHOPCONTACT_SEND_SWICH' => array(
                        'title' => $this->l('Send email'),
                        'type' => 'bool'
                    )
                ),
                'submit' => array('title' => $this->l('Save'))
            ),
        );

        parent::__construct();
    }

    public function renderList()
    {
        $this->_select = ' a.`name`';
        $this->_join = ' LEFT JOIN '._DB_PREFIX_.'ffshopcontact o ON (o.`id_shop` = a.`id_shop`)';
//        $this->_group = ' GROUP BY a.`phone`';
        $this->_orderWay = 'DESC';

        $this->fields_list = array(
            'id_customer_message' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'type' => 'text',
            ),
            'name' => array(
                'title' => $this->l('Name'),
                'filter_key' => 'a!name',
                'type' => 'text',
            ),
            'email' => array(
                'title' => $this->l('Email'),
                'filter_key' => 'a!email',
                'type' => 'text',
            ),
            'phone' => array(
                'title' => $this->l('Phone number'),
                'filter_key' => 'a!phone',
                'type' => 'text',
            ),
            'message' => array(
                'title' => $this->l('Messages'),
                'filter_key' => 'message',
                'maxlength' => 40,
                'type' => 'text',
            ),
            'date_add' => array(
                'title' => $this->l('Add message'),
                'havingFilter' => true,
                'type' => 'datetime',
            ),
        );

        return parent::renderList();
    }

    public function renderForm()
    {
        $this->fields_form = array(
            'legend' => array(
                'title' => $this->l('Contacts'),
                'icon' => 'icon-envelope-alt'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'name' => 'name',
                    'col'  =>  3,
                    'label' => $this->l('Customer:'),
                ),
                array(
                    'type' => 'text',
                    'name' => 'email',
                    'col'  =>  3,
                    'label' => $this->l('Customer E-mail:'),
                ),
                array(
                    'type' => 'text',
                    'name' => 'phone',
                    'col'  =>  3,
                    'label' => $this->l('Customer phone'),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Customer message:'),
                    'name' => 'message',
                    'col' => 6,
                    'rows' => 14,
                    'class' => 'field_description'
                ),
            ),
//            'active' => array(
//                'title' => $this->l('Status'),
//                'align' => 'center',
//                'active' => 'status',
//                'type' => 'bool',
//                'class' => 'fixed-width-sm',
//                'orderby' => false,
//                'search' => false,
//                'ajax' => true,
//            ),
            'buttons' => array(
                array(
                    'icon' => 'process-icon-back icon',
                    'title' => $this->l('Back'),
                    'href' => $this->context->link->getAdminLink('AdminContactMsg', false).'&token='.Tools::getAdminTokenLite('AdminContactMsg'),
                )
            )

        );

        return parent::renderForm();
    }

    public function initToolbar()
    {
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
    }

}
