<?php

class ShopContactMsg extends ObjectModel {

    /** @var string Name */
    public $name;
    public $email;
    public $phone;
    public $message;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'ffshopcontact_message',
        'primary' => 'id_customer_message',
        'fields' => array(
            'name'=>   array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 100),
            'email'    =>   array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 100),
            'phone'    =>   array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'required' => true, 'size' => 13),
            'message'  =>   array('type' => self::TYPE_STRING, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 16777216),
            'id_shop'  =>	array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
            'date_add' =>   array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
        ),
    );

}