<?php

class ShopContact extends ObjectModel {

    public $id;
    public $id_shop;
    public $poscode;
    public $country;
    public $city;
    public $state;
    public $address;
    public $maps_lat;
    public $maps_long;
    public $emails;
    public $phones;
    public $work_time;
    public $text;

    public static $definition = array(
        'table'                         => 'ffshopcontact',
        'primary'                       => 'id',
        'multilang'                     => true,
        'fields'                        => array(
            'id_shop'                   => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),

            // Lang fields
            'poscode'                   => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'country'                   => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isAddress', 'size' => 255),
            'city'                      => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCityName', 'size' => 255),
            'state'                     => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isAddress', 'size' => 255),
            'address'                   => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isAddress', 'size' => 255),
            'maps_lat'                  => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'maps_long'                 => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'emails'                    => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'phones'                    => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'work_time'                 => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'text'                      => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
        ),
    );

}