<?php
/**
 * Created by PhpStorm.
 * User: dkostrub
 * Date: 30.08.2017
 * Time: 9:58
 */
if (!defined('_PS_VERSION_'))
    exit;

require_once __DIR__.'/models/ShopContactMsg.php';

class FfShopContacts extends Module
{
    private static $module_tab = array(
        'class_name' => 'AdminContactMsg',
        'name' => array(
            'default' => 'Feedback',
            'en' => 'Feedback',
            'ru' => 'Обратная связь',
            'uk' => 'Обратная связь',
        ),
        'module' => false,
        'id_parent' => false
    );
    private static $module_languages;
    private   $secure_key    = 'f$b$39b23s7$e7a92vh42V$D29215f4xc1$9Db';
    protected $_extra_html   = '';
    protected $contact_tpl   = 'contact-us.tpl';
    public $to;
    public $errors =[];
    const INSTALL_SQL_FILE   = 'sql/install.php';
    const UNINSTALL_SQL_FILE = 'sql/uninstall.php';

    public function __construct()
    {
        $this->name = 'ffshopcontacts';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Shop contact block');
        $this->description = $this->l('Allows you to add additional information about your store\'s.');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');

        self::$module_languages = Language::getLanguages();
    }

    public function install($keep = false)
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !( $keep ? true : $this->installSql()) ||
            !$this->registerHook('displayHome') ||
            !$this->registerHook('displayShopContact') ||
            !$this->registerHook('header') ||
            !$this->createTab() ||
            !Configuration::updateValue('FF_SHOPCONTACT_ADMIN_EMAIL', 'info@carvari.com') ||
            !Configuration::updateValue('FF_SHOPCONTACT_SEND_SWICH', 1)
        )
            return false;

        $this->cleanClassCache();

        return true;
    }

    public function uninstall($keep = false)
    {
        if (!parent::uninstall() ||
            !( $keep ? true : $this->uninstallSql()) ||
            !$this->deleteTab() ||
            !Configuration::deleteByName('FF_SHOPCONTACT_ADMIN_EMAIL') ||
            !Configuration::deleteByName('FF_SHOPCONTACT_SEND_SWICH')
        )
            return false;

        return true;
    }

    public function reset()
    {
        if (!$this->uninstall(true))
            return false;
        if (!$this->install(true))
            return false;
        return true;
    }

    protected function installSql()
    {
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ffshopcontact` (
            `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_shop` INT(10) UNSIGNED NOT NULL ,			
            PRIMARY KEY (`id`))
            ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ffshopcontact_lang` (
            `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `id_lang`      int(11) UNSIGNED NOT NULL,
            `poscode`      int(11) NOT NULL,
            `country`      VARCHAR(255) NOT NULL,
            `city`         VARCHAR(255) NOT NULL,
            `state`        VARCHAR(255) NOT NULL,
            `address`      VARCHAR(255) NOT NULL,
            `maps_lat`     VARCHAR(255) NOT NULL,
            `maps_long`    VARCHAR(255) NOT NULL,
            `emails`       VARCHAR(255) NOT NULL,
            `phones`       VARCHAR(255) NOT NULL,
            `work_time`    VARCHAR(255) NOT NULL,
            `text`         VARCHAR(255) NOT NULL,
            PRIMARY KEY (`id`, `id_lang`))
            ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ffshopcontact_message`(
            `id_customer_message` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `name`           VARCHAR(100) NOT NULL,
            `email`               VARCHAR(100) NOT NULL,
            `phone`               VARCHAR(32) NOT NULL,
            `message`             longtext,
            `id_shop`             int(10) DEFAULT 1,
            `date_add` timestamp   DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY(`id_customer_message`))
            ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );

        Db::getInstance()->insert('ffshopcontact',
            array(
                'id' => 1,
                'id_shop' => (int)$this->context->shop->id,
            )
        );

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            Db::getInstance()->insert('ffshopcontact_lang',
                array(
                    'id' => 1,
                    'id_lang' => $language['id_lang'],
                    'poscode' => '49000',
                    'country' => 'Украина',
                    'city' => 'Днепр',
                    'state' => 'Днепропетровская область',
                    'address' => 'ул. Свердлова, 37 ',
                    'maps_lat' => '48.4636585',
                    'maps_long' => '35.0217708',
                    'emails' => 'info@carvari.com, carvari@carvari.com',
                    'phones' => '0 800 211 009, 067 641 94 90, 073 641 94 90, 095 290 24 90',
                    'work_time' => '10:00-11:00',
                    'text' => 'Lorem ipsum dolor sit amet',
                )
            );
        }
        return true;
    }

    protected function uninstallSql()
    {
        $sql = $this->getTableSql(self::UNINSTALL_SQL_FILE);
        return $this->performSql($sql);
    }

    protected function getTableSql($sqlFile)
    {
        $sql = require (dirname(__FILE__).'/'.$sqlFile);
        if (!$sql) {
            return [];
        }
        return $sql;
    }

    protected function performSql(array $sqlQueries)
    {
        $dbInstance = Db::getInstance();
        foreach ($sqlQueries as $query) {
            if (!$dbInstance->execute($query)) {
                return false;
            }
        }

        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache)) {
            unlink($class_cache);
        }
    }

    public function createTab()
    {
        if ($this->tabExists())
            return true;

        $parent_id = Tab::getIdFromClassName('AdminParentCustomer');

        if (!$parent_id) {
            $this->_errors[] = $this->l('Invalid parent tab selected');
            return false;
        }

        $tab = new Tab();

        foreach (self::$module_tab as $k => $v) {
            if (property_exists($tab, $k)) {
                switch ($k) {
                    case 'name':
                        $v = self::getMultilangField($v);
                        break;
                    case 'module':
                        $v = $this->name;
                        break;
                    case 'id_parent':
                        $v = $parent_id;
                        break;
                }
                $tab->{$k} = $v;
            }
        }

        if (!$tab->add()) {
            $this->_errors[] = $this->l('Unable to create Product label tab');
            return false;
        }

        return true;
    }

    private static function getTabId()
    {
        return Tab::getIdFromClassName(self::$module_tab['class_name']);
    }

    private function deleteTab()
    {
        $tab = self::getTabId();

        if ($tab && Validate::isLoadedObject($tab_obj = new Tab((int)$tab))) {
            if ($tab_obj->delete()) {
                return true;
            }

            $this->_errors[] = $this->l('Unable to remove Product label tab');

            return false;
        }

        return false;
    }

    private static function tabExists()
    {
        return self::getTabId();
    }

    public function getMultilangField($field_array)
    {
        $languages = self::getLanguages();
        $prepared = array();

        foreach ($languages as $language) {
            if( isset($field_array[$language['iso_code']]))
                $prepared[$language['id_lang']] = $field_array[$language['iso_code']];
            else
                $prepared[$language['id_lang']] = $field_array['default'];
        }

        return $prepared;
    }

    public static function getLanguages()
    {
        if (!is_array(self::$module_languages))
            self::$module_languages = Language::getLanguages();

        return self::$module_languages;
    }

    public function getContent()
    {
        $this->postProcess();

        if (Tools::getIsset('updateffshopcontact'))
            return $this->displayForm();

        return $this->generateList();
    }

    public function displayForm()
    {
        require_once __DIR__.'/models/ShopContact.php';

        $contact_item  = new ShopContact((int)Tools::getValue('id'));

        $fields_form = array('');

        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Настройка модуля'),
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'contact[id]',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Zip/postal Code'),
                    'name' => 'poscode',
                    'lang' => true,
                    'size' => '10'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Country'),
                    'name' => 'country',
                    'lang' => true,
                    'size' => '10'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('City'),
                    'name' => 'city',
                    'lang' => true,
                    'size' => '10'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('State'),
                    'name' => 'state',
                    'lang' => true,
                    'size' => '10'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Address'),
                    'name' => 'address',
                    'lang' => true,
                    'size' => '10'
                ),
                array(
                    'label' => $this->l('Default latitude'),
                    'hint' => $this->l('Used for the initial position of the map.'),
                    'type' => 'text',
                    'lang' => true,
                    'size' => '10',
                    'name' => 'maps_lat'
                ),
                array(
                    'label' => $this->l('Default longitude'),
                    'hint' => $this->l('Used for the initial position of the map.'),
                    'type' => 'text',
                    'size' => '10',
                    'lang' => true,
                    'name' => 'maps_long'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('E-mails'),
                    'hint' => $this->l('Specify the e-mail with a comma.'),
                    'lang' => true,
                    'size' => '10',
                    'name' => 'emails'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Phones'),
                    'hint' => $this->l('Specify the phones with a comma.'),
                    'lang' => true,
                    'size' => '10',
                    'name'=> 'phones'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Time work store'),
                    'lang' => true,
                    'size' => '10',
                    'name' => 'work_time'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Text'),
                    'lang' => true,
                    'size' => '10',
                    'name' => 'text'
                ),
            ),

            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'import'
            )

        );

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int)($language['id_lang'] == $default_lang);
        }
        $helper->languages = $languages;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        $helper->title = $this->l('Edit Contact item');
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->show_cancel_button = true;

        $helper->submit_action = 'submitContact';
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        $helper->fields_value['contact[id]'] = $contact_item->id;
        $helper->fields_value['poscode'] = $contact_item->poscode;
        $helper->fields_value['country'] = $contact_item->country;
        $helper->fields_value['city'] = $contact_item->city;
        $helper->fields_value['state'] = $contact_item->state;
        $helper->fields_value['address'] = $contact_item->address;
        $helper->fields_value['maps_lat'] = $contact_item->maps_lat;
        $helper->fields_value['maps_long'] = $contact_item->maps_long;
        $helper->fields_value['emails'] = $contact_item->emails;
        $helper->fields_value['phones'] = $contact_item->phones;
        $helper->fields_value['work_time'] = $contact_item->work_time;
        $helper->fields_value['text'] = $contact_item->text;

        return $helper->generateForm($fields_form);
    }

    private function generateList()
    {
        $fields_list = array(
            'country' => array(
                'title' => $this->l('Country'),
//                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
            'city' => array(
                'title' => $this->l('City'),
//                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
            'state' => array(
                'title' => $this->l('State'),
//                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
            'address' => array(
                'title' => $this->l('Address'),
                'type' => 'text',
                'search' => false,
            ),
            'emails' => array(
                'title' => $this->l('E-mail'),
//                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
            'phones' => array(
                'title' => $this->l('Phones'),
//                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
            'work_time' => array(
                'title' => $this->l('Work time'),
//                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
        );

        $total_count = Db::getInstance()->getValue('SELECT count(*) FROM `'._DB_PREFIX_.'ffshopcontact`');
        $fields_values = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'ffshopcontact` m LEFT JOIN `'._DB_PREFIX_.'ffshopcontact_lang` ml ON ml.`id`=m.`id` AND ml.`id_lang`='.(int)Configuration::get('PS_LANG_DEFAULT'));

        $helper = new HelperList();

        $helper->shopLinkType = '';

        $helper->actions = array('edit');

        $helper->identifier = 'id';
        $helper->show_toolbar = true;

        $helper->listTotal = $total_count;
        $helper->title = $this->l('Top menu items');
        $helper->table = 'ffshopcontact';

        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper->generateList( $fields_values , $fields_list);
    }

    protected function postProcess()
    {
        $redirect = 0;
        if (Tools::isSubmit('submitContact')){
            require_once __DIR__.'/models/ShopContact.php';

            $contact = Tools::getValue('contact');
            $contact_item = new ShopContact((int)$contact['id']);

            /* Multilingual fields */
            $class_vars = get_class_vars(get_class($contact_item));

            $fields = array();
            if (isset($class_vars['definition']['fields'])) {
                $fields = $class_vars['definition']['fields'];
            }

            foreach ($fields as $field => $params) {
                if (array_key_exists('lang', $params) && $params['lang']) {
                    foreach (Language::getIDs(false) as $id_lang) {
                        if (Tools::isSubmit($field.'_'.(int)$id_lang)) {
                            $contact_item->{$field}[(int)$id_lang] = Tools::getValue($field.'_'.(int)$id_lang);
                        }
                    }
                }
            }

            $contact_item->save();

            $redirect = 1;
            $this->clearCache();
        }

        if($redirect)
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
                $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }

    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;

        return $cache_id;
    }

    protected function clearCache()
    {
        $this->_clearCache($this->contact_tpl);
    }

    function smcf_token($s)
    {
        return md5("smcf-" . $s . date("WY"));
    }

    public function send_request()
    {
        $this->to = Configuration::get('FF_SHOPCONTACT_ADMIN_EMAIL');
        $subject = 'Feedback';

        $data = Tools::getValue('data');
        $name = trim($data['name']);
        $email = trim($data['email']);
        $phone = $data['phone'];
        $message = $data['message'];
        $token = $data['token'];
        $iso_lang = Tools::getValue('iso_lang');

        if (!$this->validateCustomerFields()) {
            die(Tools::jsonEncode(['hasError' => true, 'errors' => $this->errors]));
        } else {
            if ($token === $this->smcf_token($this->to) && Configuration::get('FF_SHOPCONTACT_SEND_SWICH')) {
                $this->smcf_send($name, $phone, $email, $message, $subject);
            }

            $phone = preg_replace('/[. ()-]*/','',$phone);
            $this->saveMessage($name, $email, $phone, $message);
            if ($iso_lang == 'uk'){
                die(Tools::jsonEncode(['hasError' => false, 'errors' => '', 'msg' => $this->display(__FILE__, 'message.tpl', $this->getCacheId())]));
            } else {
                die(Tools::jsonEncode(['hasError' => false, 'errors' => '', 'msg' => $this->display(__FILE__, 'message-ru.tpl', $this->getCacheId())]));
            }
        }

    }

    public function smcf_send($name, $phone, $email, $message, $subject)
    {
        // Set and wordwrap message body
        $body = "Имя: $name\n\n";
        $body .= " Телефон: $phone\n\n";
        $body .= " Email: $email\n\n";
        $body .= " Сообщение: $message\n\n";
        $body = wordwrap($body, 70);

        // Send phone
        $configuration = Configuration::getMultiple(array(
            'PS_SHOP_EMAIL',
            'PS_MAIL_METHOD',
            'PS_MAIL_SERVER',
            'PS_MAIL_USER',
            'PS_MAIL_PASSWD',
            'PS_SHOP_NAME',
            'PS_MAIL_SMTP_ENCRYPTION',
            'PS_MAIL_SMTP_PORT',
            'PS_MAIL_METHOD',
            'PS_MAIL_TYPE'
        ));

        if (!isset($configuration['PS_MAIL_SMTP_ENCRYPTION'])) $configuration['PS_MAIL_SMTP_ENCRYPTION'] = 'off';
        if (!isset($configuration['PS_MAIL_SMTP_PORT'])) $configuration['PS_MAIL_SMTP_PORT'] = 25;
        if (!isset($from)) $from = $configuration['PS_SHOP_EMAIL'];

        $smtpChecked = ($configuration['PS_MAIL_METHOD'] == 2);

        $result = Mail::sendMailTest(
            Tools::htmlentitiesUTF8($smtpChecked),
            Tools::htmlentitiesUTF8($configuration['PS_MAIL_SERVER']),
            Tools::htmlentitiesUTF8($body),
            Tools::htmlentitiesUTF8($subject),
            Tools::htmlentitiesUTF8('text/html'),
            Tools::htmlentitiesUTF8($this->to),
            $from,
            Tools::htmlentitiesUTF8($configuration['PS_MAIL_USER']),
            Tools::htmlentitiesUTF8($configuration['PS_MAIL_PASSWD']),
            Tools::htmlentitiesUTF8($configuration['PS_MAIL_SMTP_PORT']),
            Tools::htmlentitiesUTF8($configuration['PS_MAIL_SMTP_ENCRYPTION'])
        );

        if (!$result) {
            echo $this->display(__FILE__, 'callback_die.tpl');
        }
    }

    /**
     * Validate customer fields
     *
     * @return bool True if all fields valid
     */
    public function validateCustomerFields()
    {
        $this->cleanErrors();

        $data = Tools::getValue('data');
        $name = trim($data['name']);
        $email = trim($data['email']);
        $phone = $data['phone'];
        $message = $data['message'];
        $iso_lang = Tools::getValue('iso_lang');

        if ($iso_lang == 'uk') {
            if ($name == '') {
                $this->errors['name'][] = Tools::displayError('Customer name is empty');
            } else if (!Validate::isName($name)) {
                $this->errors['name'][] = Tools::displayError('Customer name is not valid');
            }

            if ($phone == '') {
                $this->errors['phone'][] = Tools::displayError('Customer phone is empty');
            } else if (!Validate::isPhoneNumber($phone)) {
                $this->errors['phone'][] = Tools::displayError('Customer phone is not valid');
            }

            if ($email == '') {
                $this->errors['email'][] = Tools::displayError('Customer email is empty');
            } else if (!Validate::isEmail($email)) {
                $this->errors['email'][] = Tools::displayError('Customer email is not valid');
            }

            if ($message == '') {
                $this->errors['message'][] = Tools::displayError('The message cannot be blank');
            } else if (!Validate::isCleanHtml($message)) {
                $this->errors['message'][] = Tools::displayError('Invalid message');
            }

            if (count($this->errors) > 0) {
                return false;
            }
        } else {
            if ($name == '') {
                $this->errors['name'][] = Tools::displayError('Имя не заполнено');
            } else if (!Validate::isName($name)) {
                $this->errors['name'][] = Tools::displayError('Не верный формат имени');
            }

            if ($phone == '') {
                $this->errors['phone'][] = Tools::displayError('Телефон не заполнен');
            } else if (!Validate::isPhoneNumber($phone)) {
                $this->errors['phone'][] = Tools::displayError('Не верный формат телефона');
            }

            if ($email == '') {
                $this->errors['email'][] = Tools::displayError('E-mail не заполнен');
            } else if (!Validate::isEmail($email)) {
                $this->errors['email'][] = Tools::displayError('Не верный формат email');
            }

            if ($message == '') {
                $this->errors['message'][] = Tools::displayError('Сообщение не заполнено');
            } else if (!Validate::isCleanHtml($message)) {
                $this->errors['message'][] = Tools::displayError('Не верный формат сообщения');
            }

            if (count($this->errors) > 0) {
                return false;
            }
        }

        return true;
    }

    /**
     * Clean module errors
     */
    public function cleanErrors()
    {
        $this->_errors = array();
    }

    protected function saveMessage($name, $email, $phone, $message)
    {
        $sql = 'INSERT INTO `'._DB_PREFIX_.'ffshopcontact_message` (`name`, `email`, `phone`, `message`)
                VALUES (
                "' . pSQL($name) . '",
                "' . pSQL($email) . '",
                "' . pSQL($phone) . '",
                "' . pSQL($message) . '"
                )';

        return Db::getInstance()->execute($sql);
    }

    public function hookHeader($params)
    {
        $this->context->controller->addJS($this->getPathUri().'/views/js/ffshopcontacts.js');
        $this->context->controller->addJqueryPlugin('fancybox');
    }

    public function hookDisplayShopContact($params)
    {
        $mlt = false;
        $lang_id = Context::getContext()->language->id;
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        if ($defaultLang != $lang_id && count(Language::getLanguages('true'))>1){
            $mlt = true;
        }
        $contacts = DB::getInstance()->getRow(
            'SELECT * FROM `' . _DB_PREFIX_ . 'ffshopcontact` m 
             LEFT JOIN `'._DB_PREFIX_.'ffshopcontact_lang` ml 
             ON ml.`id`=m.`id` 
             AND ml.`id_lang`='.(int)$lang_id.' 
             WHERE m.`id`=1');

        $this->context->smarty->assign(array(
            'ffshopcontacts' => $contacts,
            'mlt' => $mlt,
            'to' => $this->smcf_token(Configuration::get('FF_SHOPCONTACT_ADMIN_EMAIL'))
        ));

        return $this->display(__FILE__, $this->contact_tpl, $this->getCacheId());
    }

}