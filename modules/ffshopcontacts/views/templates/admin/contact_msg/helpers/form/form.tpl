{extends file="helpers/form/form.tpl"}
{block name="input"}
	{if $input.type == 'text'}
		<input type="text" name="name" id="name" value="{$fields_value[$input.name]}" class="">
	{/if}
    {if $input.type == 'textarea'}
		<textarea name="message" id="message" class="field_description" rows="14">{$fields_value[$input.name]}</textarea>
    {/if}
{/block}

