<div class="thanks-fancy">
    <div class="fancy-title">
        {l s='Дякуємо, що зв\'язалися з нами.' mod='ffshopcontacts'}
    </div>
    <div class="fancy-text">
        {l s='Наш менеджер зв\'яжеться з Вами в найкоротші терміни.' mod='ffshopcontacts'}
    </div>
</div>