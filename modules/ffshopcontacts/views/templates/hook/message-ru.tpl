<div class="thanks-fancy">
    <div class="fancy-title">
        {l s='Спасибо, что связались с нами' mod='ffshopcontacts'}
    </div>
    <div class="fancy-text">
        {l s='Наш менеджер свяжется с Вами в ближайшее время.' mod='ffshopcontacts'}
    </div>
</div>