{*{$postcode}
{$city}
{$state}
{$address}
{$latitude}
{$longitude}
{foreach from=$emails item=email}
    {$email|escape:'html':'UTF-8'}
{/foreach}
{foreach from=$phones item=phone}
    {$phone|escape:'html':'UTF-8'}
{/foreach}
{foreach from=$work_times item=work_time}
    {$work_time|escape:'html':'UTF-8'}
{/foreach}
{$text}*}

{assign var=email value=","|explode:$ffshopcontacts.emails|strip:''}
{assign var=phone value=","|explode:$ffshopcontacts.phones}
{assign var=work_time value=","|explode:$ffshopcontacts.work_time|strip:''}

{*{addJsDefL name='ffShopContactLatitude'}{$ffshopcontacts.maps_lat}{/addJsDefL}*}
{*{addJsDefL name='ffShopContactLongitude'}{$ffshopcontacts.maps_long}{/addJsDefL}*}

<div class="page page--contacts main_content" itemscope itemtype="https://schema.org/Organization">
	<h1 class="h1">{l s='Контакты' mod='ffshopcontacts'}</h1>

	{if $ffshopcontacts.address || $email || $phone || $work_time}

		<div class="contacts_left">
			{if $ffshopcontacts.address}
				<div class="contacts_title">
					{l s='Адрес' mod='ffshopcontacts'}
				</div>
				<div class="contacts_text contacts_address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<p itemprop="addressLocality">{$ffshopcontacts.city}, {$ffshopcontacts.state},</p>
					<p>
						<span itemprop="streetAddress">{$ffshopcontacts.address}</span>{if $ffshopcontacts.poscode}, <span itemprop="postalCode">{$ffshopcontacts.poscode}</span>{/if}
					</p>
				</div>
			{/if}

			{if $email && $email|count}
				<hr>
				<div class="contacts_title">
					{l s='Почта' mod='ffshopcontacts'}
				</div>
				<div class="contacts_text">
					{foreach from=$email item=email_item}
						<a href="mailto:{$email_item|escape:'html':'UTF-8'}" itemprop="email"
								class="contacts_link">{$email_item|escape:'html':'UTF-8'}</a>
					{/foreach}
				</div>
			{/if}

			{if $phone && $phone|count}
				<hr>
				<div class="contacts_title">
					{l s='Телефоны' mod='ffshopcontacts'}
				</div>
				<div class="contacts_text">
					{foreach from=$phone item=phone_item}
						<a href="tel:{$phone_item|escape:'html':'UTF-8'|strip:''}" itemprop="telephone"
								class="contacts_link">{$phone_item|escape:'html':'UTF-8'}</a>
					{/foreach}
				</div>
			{/if}

			{if $work_time && $work_time|count}
				<hr>
				<div class="contacts_title">
					{l s='График работы' mod='ffshopcontacts'}
				</div>
				<div class="contacts_text">
					{foreach from=$work_time item=work_time_item}
						<div class="contacts_text__schedule">
							{$work_time_item}
						</div>
					{/foreach}
				</div>
			{/if}

            {if $ffshopcontacts.text}
				<hr>
				{*<div class="contacts_title">*}
                    {*{l s='График работы' mod='ffshopcontacts'}*}
				{*</div>*}
				<div class="contacts_text contacts_address">
					<p>{$ffshopcontacts.text}</p>
				</div>
            {/if}
		</div>

	{/if}
	<div class="contacts_right" id="contact-container">
		<div id="contact_map" style="height: 500px;" class="contact_map"></div>

		{include file="$tpl_dir./errors.tpl"}
		<form id="contact" action="" method="post" class="contact-form-box" enctype="multipart/form-data">
			<h3 class="page-subheading">{l s='Send a message' mod='ffshopcontacts'}</h3>
			<h4>{l s='Contact us today, and get reply with in 24 hours!' mod='ffshopcontacts'}</h4>
			<div class="input-form">
				<fieldset class="form-group">
					<label class="form-control-label" for="name">{l s='Your name' mod='ffshopcontacts'}</label>
					<input class="form-control validate" id="user-name" type="text" name="name" placeholder="{l s='Your name' mod='ffshopcontacts'}"
						   data-validate="isName" value="" tabindex="1" />
					<span id="name_errors" class="error_msg"></span>
				</fieldset>
				<fieldset class="form-group">
					<label class="form-control-label" for="email">{l s='Email address' mod='ffshopcontacts'}</label>
					<input class="form-control validate" type="email" id="email" name="email" placeholder="{l s='Your Email Address' mod='ffshopcontacts'}"
						   data-validate="isEmail" value="" tabindex="2" />
					<span id="email_errors" class="error_msg"></span>
				</fieldset>
				<fieldset class="form-group">
					<label class="form-control-label" for="tel">{l s='Your Phone Number' mod='ffshopcontacts'}</label>
					<input class="form-control validate" type="tel" id="phone" name="phone" placeholder="+380"
						   data-validate="isPhoneNumber" value="" tabindex="3" />
					<span id="phone_errors" class="error_msg"></span>
				</fieldset>
			</div>
			<div class="textarea-form">
				<fieldset class="form-group">
					<label class="form-control-label" for="message">{l s='Message' mod='ffshopcontacts'}</label>
					<textarea class="form-control" id="message" name="message" placeholder="{l s='Type your Message Here....' mod='ffshopcontacts'}"
							  tabindex="4"></textarea>
					<span id="message_errors" class="error_msg"></span>
				</fieldset>
			</div>
			<div class="submit">
				<fieldset>
					<button type="submit" name="submitContactMessage" id="submitContactMessage" class="button btn btn-default button-medium contact-send" data-submit="Sending">
						<span>{l s='Send' mod='ffshopcontacts'}</span>
					</button>
					<input type='hidden' id="token" name='token' value='{$to}'/>
					<span id="contact_loader" class="opc-spinner" style="display: none;"><img src="{$module_dir}views/img/preloader.gif"></span>
				</fieldset>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
    var ffShopContactLatitude = {$ffshopcontacts.maps_lat};
    var ffShopContactLongitude = {$ffshopcontacts.maps_long};
	{literal}
    var map;
    function initMap() {
		map = new google.maps.Map(document.getElementById('contact_map'), {
			zoom: 17,
			center: {lat: parseFloat(ffShopContactLatitude), lng: parseFloat(ffShopContactLongitude)},
			scaleControl: false,
			zoomControl: false,
			mapTypeControl: false
		});

		var image = function() {
			return '../../themes/carvari-theme/img/icon/marker.png';
		};

		var marker = new google.maps.Marker({
			position: {lat: parseFloat(ffShopContactLatitude), lng: parseFloat(ffShopContactLongitude)},
			map: map,
			icon: image(),
			size: new google.maps.Size(58, 57)
		});
    }
	{/literal}
</script>

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDTCEgxio96sTnsOSs-Nue20_qyhP8HPT0&callback=initMap"></script>