$(document).ready(function(){

    $(document).on('click', '#contact-container .contact-send', function (e){
        e.preventDefault();
        // var ajaxData = $('#contact-container form').serialize();
        var phone_val = cleanNonDigitChars($('#phone').val());
        var ajaxData = {
            name: $('#user-name').val(),
            email: $('#email').val(),
            phone: $('#phone').val(),
            message: $('#message').val(),
            token: $('#token').val()
        };

        $.ajax({
            type: 'POST',
            headers: {"cache-control": "no-cache"},
            url: baseDir + 'modules/ffshopcontacts/ajax.php',
            async: true,
            cache: false,
            dataType: 'JSON',
            data: {data: ajaxData, action: 'send', iso_lang: iso_lang},
            beforeSend: function() {
                $('.error_msg').hide();
                $('.error_msg').parent().removeClass('wrong');
                $('#submitContactMessage').prop('disabled', 'disabled').addClass('disabled');
                $('#contact_loader').slideDown('slow');
            },
            success: function (json) {
                $('#submitContactMessage').removeProp('disabled').removeClass('disabled');
                $('#contact_loader').slideUp('slow');

                if (json.hasError) {
                    if(json.errors.name) {
                        $('#name_errors').html(json.errors.name).slideDown('slow');
                        $('#name_errors').parent().addClass('wrong');
                    }
                    if(json.errors.email) {
                        $('#email_errors').html(json.errors.email).slideDown('slow');
                        $('#email_errors').parent().addClass('wrong');
                    }
                    if(json.errors.phone) {
                        $('#phone_errors').html(json.errors.phone).slideDown('slow');
                        $('#phone_errors').parent().addClass('wrong');
                    }
                    if(json.errors.message) {
                        $('#message_errors').html(json.errors.message).slideDown('slow');
                        $('#message_errors').parent().addClass('wrong');
                    }

                    return false;

                } else {
                    $.fancybox({
                        content   : json.msg,
                        width     : '30%',
                        height    : '8%',
                        padding   : '5px',
                        autoDimensions: false,
                        autoSize: true,
                        closeClick: false,
                        openEffect: 'elastic',
                        closeEffect: 'fade',
                        helpers: { overlay: { css: { 'background': 'rgba(0, 0, 0, 0.65)' } } },
                        afterShow : function() {
                            $('#contact').find('input[type=text], input[type=email], input[type=tel], textarea').val('');
                        },
                        afterClose: function () {
                            return;
                        },
                        onClosed : function () {
                            $("body").css({'overflow-y':'visible'});
                        }
                    });
                }
            },
            error: function () {
                alert('Oops! Something goes wrong.');
            }
        });

    });

});

function cleanNonDigitChars(string) {
    if (!string || !string.length) {
        return '';
    }
    return string.replace(new RegExp('[^\+0-9]+', 'g'), '');
}
