<?php

$sql = [];

$sql[] = 'DROP TABLE IF EXISTS '.bqSQL(_DB_PREFIX_).'ffshopcontact';
$sql[] = 'DROP TABLE IF EXISTS '.bqSQL(_DB_PREFIX_).'ffshopcontact_lang';
$sql[] = 'DROP TABLE IF EXISTS '.bqSQL(_DB_PREFIX_).'ffshopcontact_message';

return $sql;