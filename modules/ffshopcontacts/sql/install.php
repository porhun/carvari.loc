<?php

$sql = [];

$sql[] = 'CREATE TABLE IF NOT EXISTS '.bqSQL(_DB_PREFIX_).'ffshopcontact(
    `id_editorial` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `id_shop`      int(11) UNSIGNED NOT NULL,
    PRIMARY KEY(`id_editorial`)
) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS '.bqSQL(_DB_PREFIX_).'ffshopcontact_lang(
    `id_editorial` int(11) UNSIGNED NOT NULL,
    `id_lang`      int(11) UNSIGNED NOT NULL,
    `poscode`      int(11) NOT NULL,
    `country`      VARCHAR(255) NOT NULL,
    `city`         VARCHAR(255) NOT NULL,
    `state`        VARCHAR(255) NOT NULL,
    `address`      VARCHAR(255) NOT NULL,
    `maps_lat`     VARCHAR(255) NOT NULL,
    `maps_long`    VARCHAR(255) NOT NULL,
    `emails`       VARCHAR(255) NOT NULL,
    `phones`       VARCHAR(255) NOT NULL,
    `work_time`    VARCHAR(255) NOT NULL,
    `text`         VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id_editorial`, `id_lang`)
) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS '.bqSQL(_DB_PREFIX_).'ffshopcontact_message(
    `id_customer_message` int(11) UNSIGNED NOT NULL,
    `$name`          VARCHAR(100) NOT NULL,
    `email`               VARCHAR(100) NOT NULL,
    `phone`               VARCHAR(32) NOT NULL,
    `message`             longtext,
    `id_shop`             int(10) DEFAULT 1,
    `date_add` timestamp DEFAULT current_timestamp,
    PRIMARY KEY(`id_customer_message`)
) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

return $sql;