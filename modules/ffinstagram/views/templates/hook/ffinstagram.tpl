{if isset($posts)}
    <div>
        <div class="instagram_title clearfix">
            <div class="fleft"><span class="instagram"></span> {l s='Instagram: l.carvari' mod='ffinstagram'}</div>
            <div class="fright"><a href="https://www.instagram.com/l.carvari/" class="btn">{l s='Підписатися' mod='ffinstagram'}</a></div>
        </div>
        <div class="clearfix"></div>
        <ul class="instagram_list">
        {foreach from=$posts item=post}
            <li class="instagram_list__point">
                <a href="{$post->link}" class="instagram_list__link" target="_blank">
                    <img src="{$post->images->standard_resolution->url}"  class="instagram_list__img">
                </a>
            </li>
        {/foreach}
        </ul>
        <div class="clearfix"></div>
    </div>
{/if}



