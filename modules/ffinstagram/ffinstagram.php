<?php

if (!defined('_PS_VERSION_'))
    exit;

class Ffinstagram extends Module
{
    public function __construct()
    {
        $this->name = 'ffinstagram';
        $this->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'ForForce';
        $this->module_key = '26da8c46ef0d150dba825e840423be89';
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Instagram images');
        $this->description = $this->l('Display Instagram images');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        return parent::install() &&
        $this->registerHook('displayHeader') &&
        $this->registerHook('displayHome') &&
        Configuration::updateValue('ffinstagram_id', 'test') &&
        Configuration::updateValue('ffinstagram_secret', 'test') &&
        Configuration::updateValue('ffinstagram_limit', '3') &&
        Configuration::updateValue('ffinstagram_token', '1413083421.5b9e1e6.1c663ee16a0d4b13a1651346458e1bf0');
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('ffinstagram_id') ||
            !Configuration::deleteByName('ffinstagram_secret') ||
            !Configuration::deleteByName('ffinstagram_token') ||
            !Configuration::deleteByName('ffinstagram_limit')
        )
            return false;
        return true;
    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('submit' . $this->name)) {
            $this->_clearCache('ffinstagram.tpl', $this->getCacheId( 'ffinstagram|'.date('z-G') ));
            
            $id = strval(Tools::getValue('ffinstagram_id'));
            $secret = strval(Tools::getValue('ffinstagram_secret'));
            $token = strval(Tools::getValue('ffinstagram_token'));
            $limit= strval(Tools::getValue('ffinstagram_limit'));
            Configuration::updateValue('ffinstagram_id', $id);
            Configuration::updateValue('ffinstagram_secret', $secret);
            Configuration::updateValue('ffinstagram_limit', $limit);
            Configuration::updateValue('ffinstagram_token', $token);
            $output .= $this->displayConfirmation($this->l('Settings updated'));
        }
        return $output . $this->displayForm();
    }


    public function displayForm()
    {
        // Get default Language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('User Id:'),
                    'name' => 'ffinstagram_id',
                    'size' => '100',
//                    'desc' => $this->l('Get Your Instagram Access Token and USER ID: http://www.pinceladasdaweb.com.br/instagram/access-token/')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('User Secret:'),
                    'name' => 'ffinstagram_secret',
                    'size' => '100',
//                    'desc' => $this->l('Get Your Instagram Access Token and USER ID: http://www.pinceladasdaweb.com.br/instagram/access-token/')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Access token:'),
                    'name' => 'ffinstagram_token',
                    'size' => '100',
                    'desc' => $this->l('You need to get your own access token from Instagram.')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Number of Images:'),
                    'name' => 'ffinstagram_limit',
                    'size' => '100',
                    'desc' => $this->l('The number display images.')
                ),
                array(
                    'type' => 'html',
//                    'label' => $this->l('Get access token:'),
                    'html_content' =>
                        (Configuration::get('ffinstagram_id') && Configuration::get('ffinstagram_secret')) ?
                            '<a href="https://api.instagram.com/oauth/authorize/?client_id='.Configuration::get('ffinstagram_id').'&redirect_uri='._PS_BASE_URL_.'/modules/'.$this->name.'/ajax.php&response_type=code" class="btn btn-success btn-lg">GET ACCESS TOKEN</a>'
                        :
                            '<label class="label label-danger">Please enter valid "User Id" and "User Secret"</label>',

                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );

        $helper = new HelperForm();

        // Module, t    oken and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title = $this->displayName;

        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit' . $this->name;

        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        // Load current value
        $helper->fields_value['ffinstagram_id'] = Configuration::get('ffinstagram_id');
        $helper->fields_value['ffinstagram_secret'] = Configuration::get('ffinstagram_secret');
        $helper->fields_value['ffinstagram_token'] = Configuration::get('ffinstagram_token');
        $helper->fields_value['ffinstagram_limit'] = Configuration::get('ffinstagram_limit');
        return $helper->generateForm($fields_form);
    }

    public function getAccessToken()
    {
        if ($code = Tools::getValue('code')){
            $result = $this->apiSendRequest('https://api.instagram.com/oauth/access_token',[
                'client_id' => Configuration::get('ffinstagram_id'),
                'client_secret' => Configuration::get('ffinstagram_secret'),
                'grant_type' => 'authorization_code',
                'redirect_uri' => _PS_BASE_URL_.'/modules/'.$this->name.'/ajax.php',
                'code' => $code,
            ]);

            if ($result){
                if(isset($result['access_token'])){
                    $this->_clearCache('ffinstagram.tpl', $this->getCacheId( 'ffinstagram|'.date('z-G') ));
                    Configuration::updateValue('ffinstagram_token', pSQL($result['access_token']));
                    die('Success. <a href="'._PS_BASE_URL_.'">Go to Main Page</a>');
                } else {
                    d($this->curl_error);
                }
            }
        }
    }

    public function _clearCache($template, $cache_id = null, $compile_id = null)
    {
        return parent::_clearCache($template, $cache_id, $compile_id);
    }

    // Display module
    public function hookDisplayHome($params)
    {

        if ($this->context->getMobileDevice())
            return '';

        if (!$this->isCached('ffinstagram.tpl', $this->getCacheId( 'ffinstagram|'.date('z-G') ))) {

            require_once 'lib/Instagram.php';
            require_once 'lib/InstagramException.php';

            $instagram = new Instagram(array(
                'apiKey' => Configuration::get('ffinstagram_id'),
                'apiSecret' => Configuration::get('ffinstagram_secret'),
                'apiCallback' => 'http://carvari.loc/'
            ));

            $instagram->setAccessToken(Configuration::get('ffinstagram_token'));
            $data = $instagram->getUserMedia('self', 9);

            if (isset($data->data) && count($data->data)) {
//            d($data->data);
                $this->smarty->assign(['posts' => $data->data]);
            }
        }

        return $this->display(__FILE__, 'ffinstagram.tpl', $this->getCacheId( 'ffinstagram|'.date('z-G') ));
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path . 'css/ffinstagram.css', 'all');
//        $this->context->controller->addJS($this->_path . 'js/ffinstagram.js', 'all');
    }

    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;


        return $cache_id;
    }

    protected function apiSendRequest($url, array $json_value = null)
    {

//        d($json_value);

        $ch = curl_init();

        if ($json_value) {

            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json_value);
        }

        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        if ($output = curl_exec($ch)) {
//            d($output);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($output, 0, $header_size);
            $body = substr($output, $header_size);
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $return = json_decode($body, true);
            if ($status == 200) {
                return $return;
            } else {
                $this->curl_error = $return;
                return false;
            }
        } else {
            $this->curl_error = 'Превышено время ожидания ответа от сервера: ' . $url;
            return false;
        }

    }
}
