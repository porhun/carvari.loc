<?php
if (!defined('_PS_VERSION_'))
    exit;

class Ffcarvaricourier extends CarrierModule
{
    public function __construct()
    {
        $this->name = 'ffcarvaricourier';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Carvari Courier - доставка курьером Carvari');
        $this->description = $this->l('Carvari Courier - доставка курьером Carvari.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
            $carrierConfig = array(
                'name' => 'Курьером Карвари',
                'id_tax_rules_group' => 0, // We do not apply thecarriers tax
                'active' => true,
                'is_free' => false,
                'deleted' => 0,
                'shipping_handling' => false,
                'range_behavior' => 0,
                'delay' => array(
                    'uk' => 'Описание Кур\'єром до дверей ',
                    'en' => 'Description Carvari courier',
                    Language::getIsoById(Configuration::get
                    ('PS_LANG_DEFAULT')) => 'Описание курьером карвари'),
                'id_zone' => 1, // Area where the carrier operates
                'is_module' => true, // We specify that it is a module
                'shipping_external' => false,
                'external_module_name' => $this->name, // We specify the name of the module
                'need_range' => true // We specify that we want the calculations for the ranges
                // that are configured in the back office
            );

        if($id_carrier = self::installExternalCarrier($carrierConfig))
            Configuration::updateValue('FF_CARVARI_CARRIER_ID', (int)$id_carrier);

        copy(dirname(__FILE__) . '/img/carvari.jpg', _PS_SHIP_IMG_DIR_ . '/' . (int) $id_carrier . '.jpg');

        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !$this->installSql() ||
            !$this->registerHook('displayCarrierList') ||
            !$this->registerHook('updateCarrier')
            )
            return false;
        $this->cleanClassCache();

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->uninstallSql())
            return false;

//        Configuration::deleteByName('FF_CARVARI_CARRIER_ID');

        // Delete External Carrier
        $carrier = new Carrier((int)(Configuration::get('FF_CARVARI_CARRIER_ID')));

        // If external carrier is default set other one as default
        if (Configuration::get('PS_CARRIER_DEFAULT') == (int)($carrier->id))
        {
            global $cookie;
            $carriersD = Carrier::getCarriers($cookie->id_lang, true, false, false, NULL, PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
            foreach($carriersD as $carrierD)
                if ($carrierD['active'] AND !$carrierD['deleted'] AND ($carrierD['name'] != $this->_config['name']))
                    Configuration::updateValue('PS_CARRIER_DEFAULT', $carrierD['id_carrier']);
        }

        // Then delete Carrier
        $carrier->deleted = 1;
        if (!$carrier->update())
            return false;

        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache)) {
            unlink($class_cache);
        }
    }

    private function installSql()
    {
        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffcarvaricourier` (
			`id_order` INT NOT NULL,
			`address` TEXT,
		PRIMARY KEY (`id_order`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        return true;
    }

    private function uninstallSql()
    {
        return true;
    }

    private static function installExternalCarrier($config)
    {
        $carrier = new Carrier();
        $carrier->name = $config['name'];
        $carrier->id_tax_rules_group = $config['id_tax_rules_group'];
        $carrier->id_zone = $config['id_zone'];
        $carrier->active = $config['active'];
        $carrier->is_free = $config['is_free'];
        $carrier->deleted = $config['deleted'];
        $carrier->delay = $config['delay'];
        $carrier->shipping_handling = $config['shipping_handling'];
        $carrier->range_behavior = $config['range_behavior'];
        $carrier->is_module = $config['is_module'];
        $carrier->shipping_external = $config['shipping_external'];
        $carrier->external_module_name = $config['external_module_name'];
        $carrier->need_range = $config['need_range'];

        $languages = Language::getLanguages(true);
        foreach ($languages as $language)
        {
            if ($language['iso_code'] == 'uk')
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == 'en')
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')))
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
        }

        if ($carrier->add())
        {
            $groups = Group::getGroups(true);
            foreach ($groups as $group)
                Db::getInstance()->insert(_DB_PREFIX_.'carrier_group', array('id_carrier' => (int)($carrier->id), 'id_group' => (int)($group['id_group'])));

            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '100000';
            $rangePrice->add();

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '100000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $zone)
            {
                Db::getInstance()->insert(_DB_PREFIX_.'carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])));
                Db::getInstance()->insert(_DB_PREFIX_.'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => (int)($rangePrice->id), 'id_range_weight' => NULL, 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), true);
                Db::getInstance()->insert(_DB_PREFIX_.'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => NULL, 'id_range_weight' => (int)($rangeWeight->id), 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), true);
            }

            // Copy Logo
//            if (!copy(dirname(__FILE__).'/carrier.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg'))
//                return false;

            // Return ID Carrier
            return (int)($carrier->id);
        }

        return false;
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        if ($this->id_carrier == (int)(Configuration::get('FF_CARVARI_CARRIER_ID')))
            return $shipping_cost;
        // If the carrier is not known, you can return false, the carrier won't appear in the order process
        return false;
    }

    public function getOrderShippingCostExternal($params)
    {
        return $this->getOrderShippingCost($params, 0);
    }

    public function hookDisplayCarrierList($arg)
    {
        $is_mobile = 'not_mobile';

        if ($arg['cart']->id_carrier != Configuration::get('FF_CARVARI_CARRIER_ID'))
            return false;

        if ( $this->context->getMobileDevice()){
            $is_mobile = 'mobile';
        }

        return $this->display(__FILE__, 'courier_carrier.tpl', $this->getCacheId($is_mobile));
    }

    public function hookupdateCarrier($params)
    {
        if ((int)($params['id_carrier']) == (int)(Configuration::get('FF_CARVARI_CARRIER_ID')))
            Configuration::updateValue('FF_CARVARI_CARRIER_ID', (int)($params['carrier']->id));
    }
}