{if !$robots}
    <div class="module_error alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>{l s='Error: You don\'t have a robots file, generate one from the SEO & URLs page'}
    </div>
{else}

    <style type="text/css" media="screen">
        #robotsfile {
            margin: 0;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
        }
    </style>

    <form action="{$smarty.server.REQUEST_URI}" method="post" class="defaultForm form-horizontal" enctype="multipart/form-data">
        <div class="panel">
            <div class="panel-heading">{l s='Settings'}</div>

            <div class="form-group">
                <label class="control-label col-lg-3" style="font-weight:bold;text-align:left;margin-left:15px;">{l s='Robots.txt file'}</label>
                <div class="col-lg-4" style="position:relative;height: 300px;width:100%">
                    <div id="robotsfile">{$robots}</div>
                    <textarea name="robotsfile"></textarea>
                </div>
            </div>

            <script>
                var editor = ace.edit("robotsfile");
                editor.setTheme("ace/theme/chrome");
                editor.getSession().setMode("ace/mode/apache_conf");
                var textarea = $('textarea[name="robotsfile"]').hide();
                textarea.text(editor.getSession().getValue());
                editor.getSession().on('change', function(){
                    textarea.text(editor.getSession().getValue());
                });
            </script>

            <p class="center"><input type="submit" name="submitUpdate" value="{l s='Save and regenerate'}" class="btn btn-default"></p>

        </div>
    </form>
{/if}