<?php

if (!defined('_PS_VERSION_'))
	exit;

class Ffrobotstxteditor extends Module
{
	protected $_errors = [];
	protected $_html = '';

	    public function __construct()
    {
        $this->name = 'ffrobotstxteditor';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'dkostrub';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Robots.txt editor');
        $this->description = $this->l('Allows you to edit the robots.txt file.');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');
    }

    public function install()
    {
        if (!parent::install())
            return false;
        $this->cleanClassCache();
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache))
            unlink($class_cache);
    }

	public function getContent()
    {
        $this->postProcess();
        $this->displayForm();

        return $this->_html;
    }

    private function displayForm()
    {
        $this->context->controller->addJS($this->_path.'views/js/ace.js', 'all');
        $this->context->smarty->assign('robots', $this->getRobots());

        return $this->_html .= $this->context->smarty->fetch(__DIR__ . '/views/templates/ffrobotstxteditor.tpl');
    }

    private function postProcess()
    {
        if (Tools::isSubmit('submitUpdate'))
        {
            if(!is_writable(_PS_ROOT_DIR_.'/robots.txt'))
                $this->_errors[] = $this->l('Error: Your robots file is not writable');
//            else if((bool)Configuration::get('PS_USE_HTMLPURIFIER'))
//                $this->_errors[] = $this->l('Error: You have to disable the HTML purifier');
            else
            {
                $fh = fopen(_PS_ROOT_DIR_.'/robots.txt', 'w+');
                fwrite($fh, Tools::getValue('robotsfile'));
                fclose($fh);
                $this->_html .= $this->displayConfirmation($this->l('Robots rewritten!'));
            }
            if ($this->_errors)
                $this->_html .= $this->displayError(implode($this->_errors, '<br />'));
        }
    }

	private function getRobots()
	{
		if(!file_exists(_PS_ROOT_DIR_.'/robots.txt'))
			return false;
		else if(!is_writable(_PS_ROOT_DIR_.'/robots.txt'))
			return false;
		else return file_get_contents(_PS_ROOT_DIR_.'/robots.txt');
	}

}