<?php
/**
 * Cart Reminder
 *
 * @category advertising_marketing
 * @author    Timactive - Romain DE VERA
 * @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra
 * @version 1.0.0
 * @license   Commercial license
 *
 *************************************
 **         CART REMINDER            *
 **          V 1.0.0                 *
 *************************************
 *  _____ _            ___       _   _
 * |_   _(_)          / _ \     | | (_)
 *   | |  _ _ __ ___ / /_\ \ ___| |_ ___   _____
 *   | | | | '_ ` _ \|  _  |/ __| __| \ \ / / _ \
 *   | | | | | | | | | | | | (__| |_| |\ V /  __/
 *   \_/ |_|_| |_| |_\_| |_/\___|\__|_| \_/ \___|
 *
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 *
 * TAEgMailImage is a class to represent a MailImage element store
 * in this file data/egmail/egmails.xml
 */

class TAEgMailImage
{
    /**
     * Background color hexadecimal or RGB
     * @var bgcolor
     */
    public $bgcolor;
    /**
     * Image url
     * @var url
     */
    public $url;

    /**
     * Build TAEgMailImage object by xml dom
     *
     * @param $imgdom
     * @return TAEgMailImage
     */
    public static function getFromDom($imgdom)
    {
        $image = new TAEgMailImage();
        $image->bgcolor = (string)$imgdom->bgcolor;
        $image->url = (string)$imgdom->url;
        return $image;
    }
}
