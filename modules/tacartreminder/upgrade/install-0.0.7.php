<?php
/**
 * Cart Reminder
 *
 * @category advertising_marketing
 * @author    Timactive - Romain DE VERA
 * @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra
 * @version 1.0.0
 * @license   Commercial license
 *
 *************************************
 **         CART REMINDER            *
 **          V 1.0.0                 *
 *************************************
 *  _____ _            ___       _   _
 * |_   _(_)          / _ \     | | (_)
 *   | |  _ _ __ ___ / /_\ \ ___| |_ ___   _____
 *   | | | | '_ ` _ \|  _  |/ __| __| \ \ / / _ \
 *   | | | | | | | | | | | | (__| |_| |\ V /  __/
 *   \_/ |_|_| |_| |_\_| |_/\___|\__|_| \_/ \___|
 *
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_0_0_7($object, $install = false)
{
    $result = false;
    if (($object->active || $install)) {
        $result = true;
        $result &= (bool)Db::getInstance()->Execute(
            '
		ALTER TABLE `'._DB_PREFIX_.'ta_cartreminder_mail_template_lang`
						ADD COLUMN `title` VARCHAR(255) NULL AFTER `subject`'
        );
        $result &= (bool)Db::getInstance()->Execute(
            '
		UPDATE `'._DB_PREFIX_.'ta_cartreminder_mail_template_lang` mtl
			SET mtl.`title` = mtl.`subject`'
        );
    }

    return $result;
}
