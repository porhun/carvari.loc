<?php
/**
 * Cart Reminder
 *
 * @category advertising_marketing
 * @author    Timactive - Romain DE VERA
 * @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra
 * @version 1.0.0
 * @license   Commercial license
 *
 *************************************
 **         CART REMINDER            *
 **          V 1.0.2                 *
 *************************************
 *  _____ _            ___       _   _
 * |_   _(_)          / _ \     | | (_)
 *   | |  _ _ __ ___ / /_\ \ ___| |_ ___   _____
 *   | | | | '_ ` _ \|  _  |/ __| __| \ \ / / _ \
 *   | | | | | | | | | | | | (__| |_| |\ V /  __/
 *   \_/ |_|_| |_| |_\_| |_/\___|\__|_| \_/ \___|
 *
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_0_2($object, $install = false)
{
    $result = false;
    if (($object->active || $install)) {
        $result = true;
        if (!Configuration::updateValue('TA_CARTR_CODE_FORMAT', 'LLLNLNLNLNLNLNLNLNLL') ||
            !Configuration::updateValue('TA_CARTR_AUTO_ADD_CR', 0)
        ) {
            $result = false;
        }
    }

    return $result;
}
