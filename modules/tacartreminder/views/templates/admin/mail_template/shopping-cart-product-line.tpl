{*
 * Cart Reminder
 * 
 *    @category advertising_marketing
 *    @author    Timactive - Romain DE VERA
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         CART REMINDER            *
 **          V 1.0.0                 *
 *************************************
 *  _____ _            ___       _   _           
 * |_   _(_)          / _ \     | | (_)          
 *   | |  _ _ __ ___ / /_\ \ ___| |_ ___   _____ 
 *   | | | | '_ ` _ \|  _  |/ __| __| \ \ / / _ \
 *   | | | | | | | | | | | | (__| |_| |\ V /  __/
 *   \_/ |_|_| |_| |_\_| |_/\___|\__|_| \_/ \___|
 *                                              
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 * Template to display product line in shopping cart
 * this template is used to render shopping cart in email
 * A developper can be override this file with your template
 *}
	<tr style="padding: 0; text-align: left; vertical-align: top;">
		<th style="Margin: 0; color: #000; font-family: Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 19px; margin: 0; padding: 0; text-align: left;">
			<a class="text-center small-text-center" style="Margin: 0; margin: 0; padding: 0; text-align: left; text-decoration: none;" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category, null, $id_lang, $product.id_shop)|escape:'html':'UTF-8'}?utm_source=CartReminder&utm_medium=email&utm_campaign={$mail_template}&utm_term={$time}&utm_content={$product.link_rewrite}-{$product.ean13}-{$product.id_product}">
				<center data-parsed="" style="min-width: 532px; width: 100%;">
					<img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'product_slide')|escape:'html':'UTF-8'}" alt="{$product.name|escape:'html':'UTF-8'}" {if isset($smallSize)}width="{$smallSize.width|intval}" height="{$smallSize.height|intval}" {/if} />
				</center>
			</a>

			<h3 class="item-name text-center small-text-center" style="Margin: 0; Margin-bottom: 0; color: #474747; font-family: Arial,sans-serif; font-size: 33px; font-weight: 700; line-height: 36px; margin: 0; margin-bottom: 0; padding: 0; text-align: center; word-wrap: normal;">{$product.name|escape:'html':'UTF-8'}</h3>

			<p class="item-price text-center small-text-center" style="Margin: 0; Margin-bottom: 10px; color: #474747; font-family: Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 19px; margin: 0; margin-bottom: 10px; margin-top: 15px; padding: 0; text-align: center;">
					{if !empty($product.gift)}
						{l s='Gift!' mod='tacartreminder'}
					{else}
						{if !$priceDisplay}
							<span class="product-price {if isset($product.is_discounted) && $product.is_discounted}product-discounted{/if}">{convertPrice price=$product.price_wt}</span>
						{else}
							<span class="product-price {if isset($product.is_discounted) && $product.is_discounted}product-discounted{/if}">{convertPrice price=$product.price}</span>
						{/if}
						{if isset($product.is_discounted) && $product.is_discounted}
							{assign var='priceReductonPercent' value=(($product.price_without_specific_price - $product.price_wt)/$product.price_without_specific_price) * 100 * -1}
							{if $priceReductonPercent|round < 0}
								&nbsp;
								<span class="item-old-price" style="color: #9b959e; font-size: 20px; text-decoration: line-through; vertical-align: top;">{convertPrice price=$product.price_without_specific_price}</span>
							{/if}
						{/if}
					{/if}
			</p>
		</th>
		<th class="expander" style="Margin: 0; color: #000; font-family: Arial,sans-serif; font-size: 14px; font-weight: 400; line-height: 19px; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0;"></th>
	</tr>

