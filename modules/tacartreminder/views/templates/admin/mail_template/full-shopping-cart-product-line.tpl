{*
 * Cart Reminder
 * 
 *    @category advertising_marketing
 *    @author    Timactive - Romain DE VERA
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         CART REMINDER            *
 **          V 1.0.0                 *
 *************************************
 *  _____ _            ___       _   _           
 * |_   _(_)          / _ \     | | (_)          
 *   | |  _ _ __ ___ / /_\ \ ___| |_ ___   _____ 
 *   | | | | '_ ` _ \|  _  |/ __| __| \ \ / / _ \
 *   | | | | | | | | | | | | (__| |_| |\ V /  __/
 *   \_/ |_|_| |_| |_\_| |_/\___|\__|_| \_/ \___|
 *                                              
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 * template to display product line in shopping cart
 * this template is used to render shopping cart in email
 * A developper can be override this file with your template
 *}
<tr class="product-line">
	<td style="border:1px solid #D6D4D4;">
		<table class="table">
			<tr>
				<td width="10">&nbsp;</td>
				<td>
					<a href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category, null, $id_lang, $product.id_shop, $product.id_product_attribute)|escape:'html':'UTF-8'}"><img src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'small_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:'html':'UTF-8'}" {if isset($smallSize)}width="{$smallSize.width|intval}" height="{$smallSize.height|intval}" {/if} /></a>
						{$product.reference|escape:'html':'UTF-8'}
				</td>
				<td width="10">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td style="border:1px solid #D6D4D4;">
		<table class="table">
				<tr>
					<td width="10">&nbsp;</td>
					<td>
						<span class="product_name">
							{$product.name|escape:'html':'UTF-8'}{if isset($product.attributes) && $product.attributes}&nbsp;-&nbsp;{$product.attributes|escape:'html':'UTF-8'}{/if}
						</span>
					</td>
				<td width="10">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td style="border:1px solid #D6D4D4;">
		<table class="table">
			<tr>
				<td width="10">&nbsp;</td>
				<td align="right">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{if !empty($product.gift)}
							{l s='Gift!' mod='tacartreminder'}
						{else}
            				{if !$priceDisplay}
								<span class="price">{convertPrice price=$product.price_wt}</span>
							{else}
               	 				<span class="price">{convertPrice price=$product.price}</span>
							{/if}
							{if isset($product.is_discounted) && $product.is_discounted}
                    			{assign var='priceReductonPercent' value=(($product.price_without_specific_price - $product.price_wt)/$product.price_without_specific_price) * 100 * -1}
								{if $priceReductonPercent|round < 0}
								&nbsp;<span class="discount-percent">{$priceReductonPercent|floatval|round|string_format:"%d"}%</span>
								&nbsp;
								<span class="price-old"><s>{convertPrice price=$product.price_without_specific_price}</s></span>
								{/if}
							{/if}
						{/if}
					</font>
				</td>
				<td width="10">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td style="border:1px solid #D6D4D4;">
			<table class="table">
				<tr>
					<td width="10">&nbsp;</td>
					<td align="right">
						<span class="product-quantity">
							{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}
								{$product.customizationQuantityTotal|intval}
							{else}
								{$product.cart_quantity|intval - $quantityDisplayed|intval}
							{/if}
						</span>
					</td>
					<td width="10">&nbsp;</td>
				</tr>
			</table>
	</td>
	<td style="border:1px solid #D6D4D4;">
		<table class="table">
			<tr>
				<td width="10">&nbsp;</td>
				<td align="right">
					{if !empty($product.gift)}
						{l s='Gift!' mod='tacartreminder'}
					{else}
						{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}
							{if !$priceDisplay}
								{displayPrice price=$product.total_customization_wt}{else}{displayPrice price=$product.total_customization}
							{/if}
						{else}
							{if !$priceDisplay}
								{displayPrice price=$product.total_wt}{else}{displayPrice price=$product.total}
							{/if}
						{/if}
					{/if}
				</td>
				<td width="10">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>