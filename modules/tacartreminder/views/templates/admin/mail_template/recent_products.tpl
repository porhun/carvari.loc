{if isset($products) && $products|count}
    <table>
        <tbody>
        {foreach from=$products item=product}
            {include file="$tpl_mt_path/shopping-cart-product-line.tpl"}
        {/foreach}
        </tbody>
    </table>
{/if}