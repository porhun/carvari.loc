{*
 * Cart Reminder
 * 
 *    @category advertising_marketing
 *    @author    Timactive - Romain DE VERA
 *    @copyright Copyright (c) TIMACTIVE 2014 - Romain De Véra
 *    @version 1.0.0
 *    @license   Commercial license
 *
 *************************************
 **         CART REMINDER            *
 **          V 1.0.0                 *
 *************************************
 *  _____ _            ___       _   _           
 * |_   _(_)          / _ \     | | (_)          
 *   | |  _ _ __ ___ / /_\ \ ___| |_ ___   _____ 
 *   | | | | '_ ` _ \|  _  |/ __| __| \ \ / / _ \
 *   | | | | | | | | | | | | (__| |_| |\ V /  __/
 *   \_/ |_|_| |_| |_\_| |_/\___|\__|_| \_/ \___|
 *                                              
 * +
 * + Languages: EN, FR
 * + PS version: 1.5,1.6
 * template to display all element in shopping
 * this template is used to render shopping cart in email
 * A developper can be override this file with your template
 *}
<table class="table table-recap" bgcolor="#ffffff" style="width:100%;border-collapse:collapse"><!-- Title -->
<thead>
	<tr>
		<th style="border:1px solid #D6D4D4;background-color:#fbfbfb;font-family:Arial;color:#333;font-size:13px;padding:10px">{l s='Reference' mod='tacartreminder'}</th>
		<th style="border:1px solid #D6D4D4;background-color:#fbfbfb;font-family:Arial;color:#333;font-size:13px;padding:10px">{l s='Product' mod='tacartreminder'}</th>
		<th style="border:1px solid #D6D4D4;background-color:#fbfbfb;font-family:Arial;color:#333;font-size:13px;padding:10px">{l s='Unit price' mod='tacartreminder'}</th>
		<th style="border:1px solid #D6D4D4;background-color:#fbfbfb;font-family:Arial;color:#333;font-size:13px;padding:10px">{l s='Quantity' mod='tacartreminder'}</th>
		<th style="border:1px solid #D6D4D4;background-color:#fbfbfb;font-family:Arial;color:#333;font-size:13px;padding:10px">{l s='Total price' mod='tacartreminder'}</th>
	</tr>
</thead>
<tbody>
<tr>
<td colspan="5" style="border:1px solid #D6D4D4;color:#777;padding:7px 0">
{assign var='have_non_virtual_products' value=false}
{foreach $products as $product}
	{if $product.is_virtual == 0}
		{assign var='have_non_virtual_products' value=true}						
	{/if}
	{assign var='productId' value=$product.id_product}
	{assign var='productAttributeId' value=$product.id_product_attribute}
	{assign var='quantityDisplayed' value=0}
	{assign var='ignoreProductLast' value=isset($customizedDatas.$productId.$productAttributeId) || count($gift_products)}
	{* Display the product line *}
	{include file="$tpl_mt_path/shopping-cart-product-line.tpl" id_lang=$id_lang}
	{* Then the customized datas ones*}
	{if isset($customizedDatas.$productId.$productAttributeId)}
		{foreach $customizedDatas.$productId.$productAttributeId[$product.id_address_delivery] as $id_customization=>$customization}
			<tr>
				<td></td>
				<td colspan="3">
					{foreach $customization.datas as $type => $custom_data}
						{if $type == $CUSTOMIZE_FILE}
								<ul>
									{foreach $custom_data as $picture}
										<li><img src="{$pic_dir|escape:'quotes':'UTF-8'}{$picture.value|escape:'quotes':'UTF-8'}_small" alt="" width="50px" height="auto"/></li>
									{/foreach}
								</ul>
						{elseif $type == $CUSTOMIZE_TEXTFIELD}
							<ul>
								{foreach $custom_data as $textField}
									<li>
										{if $textField.name}
											{$textField.name|escape:'html':'UTF-8'}
										{else}
											{l s='Text #' mod='tacartreminder'}{$textField@index+1|escape:'html':'UTF-8'}
										{/if}
										: {$textField.value|escape:'html':'UTF-8'}
									</li>
								{/foreach}
							</ul>
						{/if}
					{/foreach}
				</td>
				<td>
					{if $quantityDisplayed == 0 AND isset($customizedDatas.$productId.$productAttributeId)}
						{$customizedDatas.$productId.$productAttributeId|@count|intval}
					{else}
						{$product.cart_quantity|intval - $quantityDisplayed|intval}
					{/if}
				</td>
			</tr>
			{assign var='quantityDisplayed' value=$quantityDisplayed+$customization.quantity}
		{/foreach}
		{* If it exists also some uncustomized products *}
		{if $product.quantity-$quantityDisplayed > 0}
			{include file="$tpl_mt_path/shopping-cart-product-line.tpl" }
		{/if}
	{/if}
{/foreach}
{foreach $gift_products as $product}
					{assign var='productId' value=$product.id_product}
					{assign var='productAttributeId' value=$product.id_product_attribute}
					{assign var='quantityDisplayed' value=0}
					{assign var='ignoreProductLast' value=isset($customizedDatas.$productId.$productAttributeId)}
					{assign var='cannotModify' value=1}
					{* Display the gift product line *}
					{include file="$tpl_mt_path/shopping-cart-product-line.tpl" productLast=$product@last productFirst=$product@first}
{/foreach}
</td>
</tr>
{if sizeof($discounts)}
<tr>
	<td colspan="5" style="border:1px solid #D6D4D4;color:#777;padding:7px 0">
	{foreach $discounts as $discount}
		<tr class="conf_body">
						<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
							<table class="table" style="width:100%;border-collapse:collapse">
								<tr>
									<td width="10" style="color:#333;padding:0"></td>
									<td align="right" style="color:#333;padding:0">
										<font size="2" face="Open-sans, sans-serif" color="#555454">
											<strong>{l s='Voucher name:' mod='tacartreminder'}&nbsp;{$discount.name|escape:'html':'UTF-8'}</strong>
										</font>
									</td>
									<td width="10" style="color:#333;padding:0"></td>
								</tr>
							</table>
						</td>
						<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
							<table class="table" style="width:100%;border-collapse:collapse">
								<tr>
									<td width="10" style="color:#333;padding:0"></td>
									<td align="right" style="color:#333;padding:0">
										<font size="2" face="Open-sans, sans-serif" color="#555454">
											{if !$priceDisplay}{displayPrice price=$discount.value_real*-1}{else}{displayPrice price=$discount.value_tax_exc*-1}{/if}
										</font>
									</td>
									<td width="10" style="color:#333;padding:0"></td>
								</tr>
							</table>
						</td>
		</tr>
	{/foreach}
	</td>
</tr>
{/if}
<tr class="conf_body">
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						<strong>{l s='Products' mod='tacartreminder'}</strong>
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td bgcolor="#f8f8f8" align="right" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{displayPrice price=$total_products}
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
<tr class="conf_body">
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						<strong>{l s='Discounts' mod='tacartreminder'}</strong>
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{if $use_taxes && $priceDisplay == 0}
							{assign var='total_discounts_negative' value=$total_discounts * -1}
						{else}
							{assign var='total_discounts_negative' value=$total_discounts_tax_exc * -1}
						{/if}
						{displayPrice price=$total_discounts_negative}
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
{if $total_wrapping >= 0}
<tr class="conf_body">
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						<strong>{l s='Gift-wrapping:' mod='tacartreminder'}</strong>
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{if $use_taxes}
							{if $priceDisplay}
								{displayPrice price=$total_wrapping_tax_exc}
							{else}
								{displayPrice price=$total_wrapping}
							{/if}
						{else}
							{displayPrice price=$total_wrapping_tax_exc}
						{/if}
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
{/if}
<tr class="conf_body">
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						<strong>{l s='Shipping' mod='tacartreminder'}</strong>
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{if $total_shipping_tax_exc <= 0 && !isset($virtualCart)}{l s='Free Shipping!' mod='tacartreminder'}
						{else}
							{if $use_taxes}
								{if $priceDisplay}
									{displayPrice price=$total_shipping_tax_exc}
								{else}
									{displayPrice price=$total_shipping}
								{/if}
							{else}
								{displayPrice price=$total_shipping_tax_exc}
							{/if}
						{/if}
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
{if $use_taxes && $show_taxes}
<tr class="conf_body">
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						<strong>{l s='Total (tax excl.)' mod='tacartreminder'}</strong>
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{displayPrice price=$total_price_without_tax}
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
<tr class="conf_body">
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						<strong>{l s='Total tax' mod='tacartreminder'}</strong>
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						{displayPrice price=$total_tax}
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
{/if}
<tr class="conf_body">
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="2" face="Open-sans, sans-serif" color="#555454">
						<strong>{l s='Total' mod='tacartreminder'}</strong>
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
	<td bgcolor="#f8f8f8" colspan="4" style="border:1px solid #D6D4D4;color:#333;padding:7px 0">
		<table class="table" style="width:100%;border-collapse:collapse">
			<tr>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
				<td align="right" style="color:#333;padding:0">
					<font size="4" face="Open-sans, sans-serif" color="#555454">
							{if $use_taxes}
								{displayPrice price=$total_price}
							{else}
								{displayPrice price=$total_price_without_tax}
							{/if}
					</font>
				</td>
				<td width="10" style="color:#333;padding:0">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
</tbody>
</table>