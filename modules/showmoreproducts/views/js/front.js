/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/

$(document).ready(function(){
    var displayProductCount = $("ul.product-list li").length;
    var totalProductCount = $("#total-pdoduct-count").val();
    $("#total-pdoduct-page").val(1)
    var numberPage;
    var category = $('#category-id').val();
    var productList = $("ul.product-list");

    if (totalProductCount > displayProductCount)
        $(".showmoreproduct-btn").show().css({
            display: 'block'
        })

    $(".showmoreproduct-btn").on("click", function(){
        numberPage = parseInt(parseInt($("#total-pdoduct-page").val()) + parseInt(1))
        $.ajax(
            {
                type: 'POST',
                url: baseDir + 'modules/showmoreproducts/ajax_more_products.php',
                dataType: 'json',
                data: {category: category, page: numberPage, qty: 12},
                cache: false,
                success: function(result){
                    $("#total-pdoduct-page").val(numberPage++)
                    for (var n in result) {
			productList.append(result[n]);
		    }
                    if($("ul.product-list li").length >= totalProductCount)
                        $(".showmoreproduct-btn").hide()
                }
            });
    })
})
