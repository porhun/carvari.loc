<?php
/**
 * Created by PhpStorm.
 * User: sergey
 * Date: 10.10.17
 * Time: 12:12
 */

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

$context = Context::getContext();

$page = Tools::getValue('page');
$qty = Tools::getValue('qty');
$id_category = Tools::getValue('category');
$orderBy = 'reference';
$orderWay = 'desc';

// Instantiate category
$category = new Category($id_category, $context->language->id);

$cat_products = $category->getProducts($context->language->id, (int)$page, (int)$qty, $orderBy, $orderWay);

if(isset($cat_products) && $cat_products) {
    foreach($cat_products as $key=>$value) {
        $html []= "<li><a href='".$value['link']."' class='item'>
        <img src='".$context->link->getImageLink($value['link_rewrite'], $value['id_image'], 'home_default')."'>
        <span title='".$value['name']." ".$value['reference']."' class='name'>".$value['name']." ".$value['reference']."</span>
        <span class='item_price'>
        <span class='price'>".$value['price']."</span>
        </span>
        </a></li>";
    }
}

echo Tools::jsonEncode($html);