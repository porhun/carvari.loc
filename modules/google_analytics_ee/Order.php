<?php
/**
 * If you wish to customize this module for your needs,
 * please contact the authors first for more information.
 *
 * It's not allowed selling or reselling this file or any other module files without author permission.
 *
 * @author Bl Modules <blmodules@gmail.com>
 * @copyright 2010-2014 Bl Modules
 * @homepage http://www.blmodules.com
 */

if (!defined('_PS_VERSION_'))
    exit;

/**
 * Class OrderBlMod
 */
class OrderBlMod
{
    public $orderId = false;
    public $orderSend = false;
    public $orderSendDate = false;
    public $orderRefund = false;
    public $orderRefundDate = false;
    public $langId = 0;
    public $currentPageNr = 1;

    public function __construct()
    {
        $this->setLangId();
    }

    public function setOrderId($orderId)
    {
        $this->orderId = (int)$orderId;
    }

    public function setCurrentPageNr($pageNr = 1)
    {
        $this->currentPageNr = (int)$pageNr;
    }

    public function setOrderGAStatus($order = array())
    {
        $this->orderSend = $order[0]['send_order'];
        $this->orderSendDate = $order[0]['send_order_date'];
        $this->orderRefund = $order[0]['send_refund'];
        $this->orderRefundDate = $order[0]['send_refund_date'];
    }

    public function setLangId($langId=false)
    {
        if (empty($langId)) {
            $langId = Configuration::get('PS_LANG_DEFAULT');
        }

        $this->langId = $langId;
    }

    public function resetOrderGAStatus()
    {
        $this->orderSend = false;
        $this->orderSendDate = false;
        $this->orderRefund = false;
        $this->orderRefundDate = false;
    }

    public function getTotalUnsentOrders($whereRefund = false)
    {
        //Can be send order to GA
        $where = '((go.send_order != "'.ECommerceBlMod::SEND_MANUAL.'" AND go.send_order != "'.ECommerceBlMod::SEND_AUTO.'") OR go.id IS NULL)';

        //Can be send order refund to GA
        if (!empty($whereRefund)) {
            $where = $whereRefund;
        }

        $orderTotal = Db::getInstance()->getValue('
            SELECT COUNT(o.id_order) AS total
            FROM '._DB_PREFIX_.'orders o
            LEFT JOIN '._DB_PREFIX_.'blmod_google_ee go ON
            o.id_order = go.id_order
            WHERE '.$where
        );

        if (empty($orderTotal)) {
            return 0;
        }

        return $orderTotal;
    }

    public function getUnsentOrders($whereRefund = false)
    {
        //Can be send order to GA
        $where = '((go.send_order != "'.ECommerceBlMod::SEND_MANUAL.'" AND go.send_order != "'.ECommerceBlMod::SEND_AUTO.'") OR go.id IS NULL)';

        //Can be send order refund to GA
        if (!empty($whereRefund)) {
            $where = $whereRefund;
        }

        if (!empty($this->orderId)) {
            $where .= ' AND o.id_order = "'.$this->orderId.'"';
            $this->currentPageNr = 0;
        }

        $limitFrom = 0;

        if ($this->currentPageNr > 1) {
            $limitFrom = ($this->currentPageNr - 1) * ToolsBlMod::ITEM_IN_PAGE;
        }

        $orders = Db::getInstance()->ExecuteS('
            SELECT o.id_order, o.payment, o.date_upd,
            go.send_order, go.send_order_date, go.send_order_date, go.send_order_error, go.send_refund,
            go.send_refund_date, go.send_refund_error, go.utmz, go.gclid, go.dclid,
            (
                SELECT osl.name
                FROM '._DB_PREFIX_.'order_history oh
                LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON
                (oh.id_order_state = osl.id_order_state AND osl.id_lang = "'.$this->langId .'")
                WHERE oh.id_order = o.id_order
                ORDER BY oh.id_order_history DESC
                LIMIT 1
            ) AS order_status_name
            FROM '._DB_PREFIX_.'orders o
            LEFT JOIN '._DB_PREFIX_.'blmod_google_ee go ON
            o.id_order = go.id_order
            WHERE '.$where.'
            ORDER BY o.id_order DESC
            LIMIT '.$limitFrom.', '.ToolsBlMod::ITEM_IN_PAGE
        );

        return $orders;
    }

    public function getUnsentRefundOrders()
    {
        return $this->getUnsentOrders($this->refundOrderWhereQuery());
    }

    public function getTotalUnsentRefundOrders()
    {
        return $this->getTotalUnsentOrders($this->refundOrderWhereQuery());
    }

    public function getRefundOrderById($orderId)
    {
        $this->resetOrderGAStatus();
        $this->setOrderId($orderId);

        $order = $this->getUnsentRefundOrders();

        if (empty($order)) {
            return false;
        }

        $this->setOrderGAStatus($order);

        return $order;
    }

    public function refundOrderWhereQuery()
    {
        return 'go.send_refund != "'.ECommerceBlMod::SEND_MANUAL.'" AND go.send_refund != "'.ECommerceBlMod::SEND_AUTO.'"
            AND (go.send_order = "'.ECommerceBlMod::SEND_MANUAL.'" OR go.send_order = "'.ECommerceBlMod::SEND_AUTO.'")';
    }

    public function getOrderById($orderId)
    {
        $this->resetOrderGAStatus();
        $this->setOrderId($orderId);

        $order = $this->getUnsentOrders();

        if (empty($order)) {
            return false;
        }

        $this->setOrderGAStatus($order);

        return $order;
    }
}