<?php
/**
 * If you wish to customize this module for your needs,
 * please contact the authors first for more information.
 *
 * It's not allowed selling or reselling this file or any other module files without author permission.
 *
 * @author Bl Modules <blmodules@gmail.com>
 * @copyright 2010-2014 Bl Modules
 * @homepage http://www.blmodules.com
 */

if (!defined('_PS_VERSION_'))
    exit;

/**
 * Class ECommerceBlMod
 */
class ECommerceBlMod
{
    /**
     * @var \OrderCore
     */
    public $order = false;
    public $orderId = 0;
    public $langId = 0;
    public $currentPageNr = 1;
    protected $userSettings = array();
    protected $transactionId = false;

    CONST SEND_MANUAL = 1;
    CONST SEND_AUTO = 2;
    CONST SEND_WITH_ERROR = 3;

    public function __construct()
    {
        $this->setLangId();
    }

    public function setLangId($langId=false)
    {
        if (empty($langId)) {
            $langId = Configuration::get('PS_LANG_DEFAULT');
        }

        $this->langId = $langId;
    }

    public function setCurrentPageNr($pageNr = 1)
    {
        $this->currentPageNr = (int)$pageNr;
    }

    public function setOrderId($orderId = false)
    {
        $this->orderId = (int)$orderId;
    }

    public function setOrder($orderId)
    {
        $this->setOrderId($orderId);
        $this->order = new Order($this->orderId);
    }

    public function sendOrderRefundToGA($transactionType = false)
    {
        if (empty($transactionType)) {
            $transactionType = self::SEND_MANUAL;
        }

        $orderTransactionInfo = $this->getOrderTransaction();
        $utmz = $this->getUtmzAndUrlData($orderTransactionInfo);
        $uuidv4 = $this->uuidv4($orderTransactionInfo);
        $shopHostUrl = _PS_BASE_URL_;
        $affiliate = false;
        $customerIpAddress = isset($orderTransactionInfo['user_ip_address']) ? $orderTransactionInfo['user_ip_address'] : false;

        if (!is_array($utmz)) {
            $utmz = array();
        }

        if (!empty($utmz['utmcid'])) {
            $uuidv4 = $utmz['utmcid'];
        }

        $transactionHit = array_diff(array_merge($utmz, array(
            'v' => '1', //Version
            'tid' => $this->getGoogleTrackingId(), //Tracking ID
            'cid' => $uuidv4, // Anonymous Client ID.
            'dh' => $shopHostUrl, //Document hostname
            't' => 'pageview', //Transaction hit type.
            'ti' => $this->orderId, //Transaction ID
            'ta' => $affiliate, //Transaction affiliation
            'pa' => 'refund', //Transaction action
            'uip' => $customerIpAddress,
            'dp' => $this->userSettings['analytics_dp'],
        )), array(''));

        $transactionLog = array(
            'send_refund' => $transactionType,
            'send_refund_date' => date('Y-m-d H:i:s'),
            'refund_transaction_data' => $transactionHit,
        );

        $this->saveOrderTransaction($transactionLog);

        $this->curlSendToGa($transactionHit);
    }

    public function getVoucherCode()
    {
        if (_PS_VERSION_ >= 1.5) {
            $orderCartRules = $this->order->getCartRules();
        } else {
            $orderCartRules = $this->order->getDiscounts();
        }

        if (!empty($orderCartRules)) {
            return $orderCartRules[0]['name'];
        }

        return false;
    }

    public function sendOrderToGa($transactionType = false)
    {
        if (empty($transactionType)) {
            $transactionType = self::SEND_MANUAL;
        }

        ToolsBlMod::saveError('start', 'ECommerce/sendOrderToGa', $this->orderId);

        $orderTransactionInfo = $this->getOrderTransaction();
        $utmz = $this->getUtmzAndUrlData($orderTransactionInfo);
        $uuidv4 = $this->uuidv4($orderTransactionInfo);
        $shopHostUrl = _PS_BASE_URL_;
        $shopUrl = _PS_BASE_URL_.__PS_BASE_URI__;
        $affiliate = false;
        $customerIpAddress = isset($orderTransactionInfo['user_ip_address']) ? $orderTransactionInfo['user_ip_address'] : false;
        $couponCode = $this->getVoucherCode();

        if (!is_array($utmz)) {
            $utmz = array();
        }

        if (!empty($utmz['utmcid'])) {
            $uuidv4 = $utmz['utmcid'];
        }

        $transactionHit = array_diff(array_merge($utmz, array(
            'v' => '1', //Version
            'tid' => $this->getGoogleTrackingId(), //Tracking ID
            'cid' => $uuidv4, //Anonymous Client ID
            'dh' => $shopHostUrl, //Document hostname
            't' => 'pageview', //Transaction hit type
            'ti' => $this->orderId, //Transaction ID
            'ta' => $affiliate,  //Transaction affiliation
            'tr' => $this->order->total_paid_real, //Transaction revenue
            'ts' => $this->order->total_shipping, //Transaction shipping
            'tt' => round(($this->order->total_products_wt - $this->order->total_products), 2), //Transaction tax
            'cu' => $this->getCurrencyCode($this->order->id_currency), //Currency code
            'pa' => 'purchase',
            'dl' => $shopUrl,
            'dp' => $this->userSettings['analytics_dp'],
            'uip' => $customerIpAddress,
            'tcc' => $couponCode,
        )), array(''));

        $products = $this->order->getProducts();

        if (empty($products)) {
            return false;
        }

        $i = 1;

        foreach ($products as $p) {
            $productClass = new Product($p['product_id'], $this->langId);

            $productCategory = false;
            $productManufacturer = false;

            if (!empty($productClass)) {
                $productCategory = Db::getInstance()->getValue('
                    SELECT cl.`name`
                    FROM  '._DB_PREFIX_.'category_lang cl
                    WHERE cl.id_category = "'.$productClass->id_category_default.'" AND cl.id_lang = "'.$this->langId.'"
                ');

                $productManufacturer = $productClass->manufacturer_name;
            }

            $transactionHit['pr'.$i.'id'] = $p['product_id']; //artnum
            $transactionHit['pr'.$i.'nm'] = $p['product_name']; //title
            $transactionHit['pr'.$i.'pr'] = $p['product_price_wt']; //price
            $transactionHit['pr'.$i.'ca'] = $productCategory;
            $transactionHit['pr'.$i.'br'] = $productManufacturer;

            if (!empty($p['product_attribute_id'])) {
                $transactionHit['pr'.$i.'va'] = $p['product_attribute_id']; //variant
            }

            $transactionHit['pr'.$i.'qt'] = $p['product_quantity']; //quantity
            $transactionHit['pr'.$i.'cc'] = $couponCode; //coupon
            $i++;
        }

        $transactionLog = array(
            'send_order' => $transactionType,
            'send_order_date' => date('Y-m-d H:i:s'),
            'order_transaction_data' => $transactionHit,
        );

        $this->saveOrderTransaction($transactionLog);
        $this->curlSendToGa($transactionHit);

        return true;
    }

    public function curlSendToGa($transactionData = array())
    {
        $transactionType = 'send_order';

        if (!empty($transactionData['pa']) and $transactionData['pa'] == 'refund') {
            $transactionType = 'send_refund';
        }

        if (empty($transactionData['tid'])) {
            $transactionLog = array(
                $transactionType => self::SEND_WITH_ERROR,
                $transactionType.'_error' => 'empty tid',
            );

            $this->saveOrderTransaction($transactionLog);
            return false;
        }

        $url = 'http://www.google-analytics.com/collect';

        $transactionDataString = http_build_query($transactionData);
        $transactionDataString = utf8_encode($transactionDataString);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $transactionDataString);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        $result = curl_exec($ch);

        if ($result === false) {
            $result = curl_error($ch);

            $transactionLog = array(
                $transactionType => self::SEND_WITH_ERROR,
                $transactionType.'_error' => $result,
            );

            $this->saveOrderTransaction($transactionLog);
        }

        $this->updateTransactionResponse($result);

        curl_close($ch);
    }

    public function uuidv4($trafficSource =array())
    {
        if (!empty($trafficSource['ga_cid'])) {
            return htmlspecialchars_decode($trafficSource['ga_cid'], ENT_QUOTES);
        }

        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function getCidFromCookie()
    {
        $gaCid = isset($_COOKIE['_ga']) ? $_COOKIE['_ga'] : false;

        if (empty($gaCid)) {
            return false;
        }

        $ga = explode('.', $gaCid);

        if (empty($ga[2]) or empty($ga[3])) {
            return false;
        }

        return htmlspecialchars($ga[2].'.'.$ga[3], ENT_QUOTES);
    }

    public function getCurrencyCode($currencyId)
    {
        $currency = new Currency($currencyId);

        return $currency->iso_code;
    }

    public function getGoogleTrackingId()
    {
        return $this->userSettings['analytics_id'];
    }

    public function saveOrderTransaction($orderDataNew = array())
    {
        $orderInfo = $this->getOrderTransaction();

        //Save full log
        if (!empty($orderDataNew)) {
            $this->saveTransactionToLog($orderDataNew);
        }

        $sendOrder = isset($orderDataNew['send_order']) ? $orderDataNew['send_order'] : 0;
        $sendOrderDate = isset($orderDataNew['send_order_date']) ? $orderDataNew['send_order_date'] : false;
        $sendRefund = isset($orderDataNew['send_refund']) ? $orderDataNew['send_refund'] : 0;
        $sendRefundDate = isset($orderDataNew['send_refund_date']) ? $orderDataNew['send_refund_date'] : false;
        $utmz = isset($orderDataNew['utmz']) ? htmlspecialchars(serialize($orderDataNew['utmz']), ENT_QUOTES)  : false;
        $orderTransactionData = isset($orderDataNew['order_transaction_data']) ? htmlspecialchars(serialize($orderDataNew['order_transaction_data']), ENT_QUOTES) : false;
        $refundTransactionData = isset($orderDataNew['refund_transaction_data']) ? htmlspecialchars(serialize($orderDataNew['refund_transaction_data']), ENT_QUOTES) : false;
        $userIpAddress = htmlspecialchars(Tools::getRemoteAddr(), ENT_QUOTES);
        $gclid = isset($orderDataNew['gclid']) ? htmlspecialchars($orderDataNew['gclid'], ENT_QUOTES) : false;
        $dclid = isset($orderDataNew['dclid']) ? htmlspecialchars($orderDataNew['dclid'], ENT_QUOTES) : false;

        $dateNow = date('Y-m-d H:i:s');

        if (empty($orderInfo)) {
            $gaCid = $this->getCidFromCookie();

            Db::getInstance()->Execute('
                INSERT INTO `'._DB_PREFIX_.'blmod_google_ee`
                (`id_order`, `send_order`, `send_order_date`, `send_refund`, `send_refund_date`,
                `utmz`, `order_transaction_data`, `refund_transaction_data`, `last_action_date`,
                 `user_ip_address`, `gclid`, `dclid`, `ga_cid`)
                VALUES
                ("'.$this->orderId.'", "'.$sendOrder.'", "'.$sendOrderDate.'", "'.$sendRefund.'", "'.$sendRefundDate.'",
                "'.$utmz.'", "'.$orderTransactionData.'", "'.$refundTransactionData.'", "'.$dateNow.'",
                "'.$userIpAddress.'", "'.$gclid.'", "'.$dclid.'", "'.$gaCid.'")
            ');

            return true;
        }

        if (!empty($sendOrder)) {
            if ($sendOrder == self::SEND_WITH_ERROR) {
                $orderTransactionData = $orderInfo['order_transaction_data'];
            }

            $error = isset($orderDataNew['send_order_error']) ? htmlspecialchars($orderDataNew['send_order_error'], ENT_QUOTES)  : false;

            Db::getInstance()->Execute('
                UPDATE `'._DB_PREFIX_.'blmod_google_ee` SET
                `send_order` = "'.$sendOrder.'", send_order_date = "'.$dateNow.'", order_transaction_data = "'.$orderTransactionData.'",
                `send_order_error` = "'.$error.'", `last_action_date` = "'.$dateNow.'"
                WHERE `id_order` = "'.$this->orderId.'"
            ');
        }

        if (!empty($sendRefund)) {
            if ($sendRefund == self::SEND_WITH_ERROR) {
                $refundTransactionData = $orderInfo['refund_transaction_data'];
            }

            $error = isset($orderDataNew['send_refund_error']) ? htmlspecialchars($orderDataNew['send_refund_error'], ENT_QUOTES)  : false;

            Db::getInstance()->Execute('
                UPDATE `'._DB_PREFIX_.'blmod_google_ee` SET
                `send_refund` = "'.$sendRefund.'", send_refund_date = "'.$dateNow.'", refund_transaction_data = "'.$refundTransactionData.'",
                `send_refund_error` = "'.$error.'", `last_action_date` = "'.$dateNow.'"
                WHERE `id_order` = "'.$this->orderId.'"
            ');
        }

        if (!empty($utmz)) {
            Db::getInstance()->Execute('
                UPDATE `'._DB_PREFIX_.'blmod_google_ee` SET
                `utmz` = "'.$utmz.'"
                WHERE `id_order` = "'.$this->orderId.'"
            ');
        }
    }

    public function getOrderTransaction()
    {
        $orderInfo = Db::getInstance()->getRow('SELECT * FROM '._DB_PREFIX_.'blmod_google_ee WHERE id_order = "'.$this->orderId.'"');

        return $orderInfo;
    }

    public function getTotalTransaction()
    {
        $total = Db::getInstance()->getValue('
            SELECT COUNT(`id`) AS total
            FROM '._DB_PREFIX_.'blmod_google_ee
            WHERE (send_order != "0" OR send_refund != "0")'
        );

        if (empty($total)) {
            return 0;
        }

        return $total;
    }

    public function getAllTransaction()
    {
        $where = 'WHERE (send_order != "0" OR send_refund != "0")';

        if (!empty($this->orderId)) {
            $where .= ' AND id_order = "'.$this->orderId.'"';
            $this->currentPageNr = 0;
        }

        $limitFrom = 0;

        if ($this->currentPageNr > 1) {
            $limitFrom = ($this->currentPageNr - 1) * ToolsBlMod::ITEM_IN_PAGE;
        }

        $transactions = Db::getInstance()->ExecuteS('
            SELECT *
            FROM `'._DB_PREFIX_.'blmod_google_ee`
            '.$where.'
            ORDER BY last_action_date DESC
            LIMIT '.$limitFrom.', '.ToolsBlMod::ITEM_IN_PAGE
        );

        return $transactions;
    }

    public function saveTransactionToLog($transaction  = array())
    {
        $transaction['send_order'] = isset($transaction['send_order']) ? $transaction['send_order'] : 0;
        $transactionString = htmlspecialchars(serialize($transaction), ENT_QUOTES);
        $ipAddress = htmlspecialchars($_SERVER['REMOTE_ADDR'], ENT_QUOTES);

        Db::getInstance()->Execute('
            INSERT INTO `'._DB_PREFIX_.'blmod_google_transactions`
            (`id_order`, `date_add`, `ip_address`, `full_post`, `send_type`)
            VALUES
            ("'.$this->orderId.'", "'.date('Y-m-d H:i:s').'", "'.$ipAddress.'", "'.$transactionString.'", "'.$transaction['send_order'].'")
        ');

        $this->transactionId = Db::getInstance()->Insert_ID();
    }

    public function updateTransactionResponse($response = false)
    {
        if (empty($this->transactionId)) {
            return false;
        }

        Db::getInstance()->Execute('
            UPDATE `'._DB_PREFIX_.'blmod_google_transactions` SET
            `response` = "'.htmlspecialchars($response, ENT_QUOTES).'"
            WHERE `id` = "'.$this->transactionId.'"
        ');

        return true;
    }

    public function setUserSettings($userSettings = array())
    {
        $this->userSettings = $userSettings;

        $this->userSettings['analytics_id'] = isset($this->userSettings['analytics_id']) ? $this->userSettings['analytics_id'] : false;
        $this->userSettings['analytics_dp'] = isset($this->userSettings['analytics_dp']) ? $this->userSettings['analytics_dp'] : false;

        if (empty($this->userSettings['analytics_id'])) {
            ToolsBlMod::saveError('empty analytics_dp', 'ECommerce/setUserSettings', $this->orderId);
        }
    }

    public function getUtmzAndUrlData($trafficSources = array())
    {
        $dataTransaction = array();
        $utmz = !empty($trafficSources['utmz']) ? unserialize(htmlspecialchars_decode($trafficSources['utmz'], ENT_QUOTES)) : array();

        foreach ($utmz as $key => $val) {
            $dataTransaction[$key] = $val;
        }

        if (!empty($trafficSources['gclid'])) {
            $dataTransaction['gclid'] = htmlspecialchars_decode($trafficSources['gclid'], ENT_QUOTES);
        }

        if (!empty($trafficSources['dclid'])) {
            $dataTransaction['dclid'] = htmlspecialchars_decode($trafficSources['dclid'], ENT_QUOTES);
        }

        return $dataTransaction;
    }

    public function getUserUtmz()
    {
        $utmzData = array();

        if (empty($_COOKIE['__utmz'])) {
            return $utmzData;
        }

        $cookieValue = $_COOKIE['__utmz'];

        $params = explode('|', $cookieValue);
        $parts  = explode('.', $params[0], 5);

        if (count($parts) != 5) {
            return $utmzData;
        }

        array_unshift($params, $parts[4]);

        $paramMap = array(
            'utmcid'   => 'ci',
            'utmcsr'   => 'cs',
            'utmgclid' => 'gclid',
            'utmdclid' => 'dclid',
            'utmccn'   => 'cn',
            'utmcmd'   => 'cm',
            'utmctr'   => 'ck',
            'utmcct'   => 'cc',
        );

        foreach ($params as $param) {
            list($key, $val) = explode('=', $param);

            if (isset($paramMap[$key]) and $val != '(not set)' and $val != '(not%20set)') {
                $utmzData[$paramMap[$key]] = urldecode($val);
            }
        }

        return $utmzData;
    }
}