<?php
/**
 * If you wish to customize this module for your needs,
 * please contact the authors first for more information.
 *
 * It's not allowed selling or reselling this file or any other module files without author permission.
 *
 * @author Bl Modules <blmodules@gmail.com>
 * @copyright 2010-2014 Bl Modules
 * @homepage http://www.blmodules.com
 */

if (!defined('_PS_VERSION_'))
    exit;

/**
 * Class Google_analytics_ee
 * @package Google_analytics_ee
 */
class Google_analytics_ee extends Module
{
    private $full_address_no_t = false;
    private $token = false;
    private $currentPage = false;
    private $moduleImgPath = false;
    public $mainSettings = array();
    public $hideSendButton = false;

    /**
     * @var ECommerceBlMod
     */
    private $eCommerceObject = false;
    CONST ORDER_AUTOSEND_STATUS = 1;
    CONST ORDER_AUTOSEND_ALWAYS = 2;

    /**
     * Class constructor
     */
    public function __construct()
	{
		$this->name = 'google_analytics_ee';
        $this->full_name = $this->name;
		$this->tab = 'analytics_stats';
		$this->author = 'Bl Modules';
		$this->version = 1.1; //E
		$this->module_key = 'd1ac00aee943ad4105fffaf3e2424b9a';
        $this->_html = false;

		parent::__construct();

		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Google Analytics EE Orders');
		$this->description = $this->l('Google Analytics Enhanced Ecommerce - collect orders');
		$this->confirmUninstall = $this->l('Are you sure you want to delete a module?');
	}

    /**
     * Install module
     *
     * @return bool
     */
    public function install()
	{
        if (!parent::install()) {
			return false;
        }
			
		if (!$this->registerHook('header') or !$this->registerHook('newOrder') or !$this->registerHook('updateOrderStatus') or !$this->registerHook('orderConfirmation')) {
			return false;
        }

        $googleEETableSql = '
            CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'blmod_google_ee
            (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `id_order` int(11) NOT NULL,
                `send_order` tinyint(3) NOT NULL DEFAULT "0",
                `send_order_date` datetime NOT NULL,
                `send_refund` tinyint(3) NOT NULL DEFAULT "0",
                `send_refund_date` datetime NOT NULL,
                `utmz` text CHARACTER SET utf8,
                `order_transaction_data` text CHARACTER SET utf8,
                `refund_transaction_data` text CHARACTER SET utf8,
                `send_order_error` text CHARACTER SET utf8,
                `send_refund_error` text CHARACTER SET utf8,
                `last_action_date` datetime NOT NULL,
                `user_ip_address` varchar(15) CHARACTER SET utf8 DEFAULT NULL,
                `gclid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
                `dclid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
                `ga_cid` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
              PRIMARY KEY (`id`)
            )';

        $googleEETableSqlRes = Db::getInstance()->Execute($googleEETableSql);

        $googleEETransactionSql = '
            CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'blmod_google_transactions
            (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `id_order` int(11) NOT NULL,
              `date_add` datetime NOT NULL,
              `ip_address` varchar(15) CHARACTER SET utf8 NOT NULL,
              `full_post` text CHARACTER SET utf8 NOT NULL,
              `response` text CHARACTER SET utf8 NOT NULL,
              `send_type` TINYINT( 3 ) NOT NULL DEFAULT  "0",
              PRIMARY KEY (`id`)
            )';

        $googleEETransactionSqlRes = Db::getInstance()->Execute($googleEETransactionSql);

        $googleEEErrorLogSql = '
            CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'blmod_google_error
            (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `error_text` text CHARACTER SET utf8,
                `error_location` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
                `error_order_id` int(11) DEFAULT NULL,
                `extra_data` text CHARACTER SET utf8,
                `date_add` datetime NOT NULL,
                PRIMARY KEY (`id`)
            )';

        $googleEEErrorLogSqlRes = Db::getInstance()->Execute($googleEEErrorLogSql);

        if (!$googleEETableSqlRes or !$googleEETransactionSqlRes or !$googleEEErrorLogSqlRes) {
            return false;
        }

        $getDefaultGAId = Tools::getValue('ganalytics_id', Configuration::get('GANALYTICS_ID'));
        $getDefaultGAId = isset($getDefaultGAId) ? $getDefaultGAId : false;

        $settingsDefault = array(
            'analytics_id' => $getDefaultGAId,
            'analytics_dp' => '/order-confirm-manual.html',
        );

        $this->updateSettings('SETTINGS', $settingsDefault);

        $payments = $this->getAllPaymentModules();
        $paymentsDefault = array();

        if (!empty($payments)) {
            foreach($payments as $p) {
                $paymentsDefault[] = $p['name'];
            }
        }

        $this->updateSettings('PAYMENTS', $paymentsDefault);

        $orderStatusDefault = array(
            'autosend' => 2,
            'status' => array(4),
        );

        $this->updateSettings('ORDER_STATUS', $orderStatusDefault);

        $refundStatusDefault = array(
            'autosend' => 1,
            'status' => array(7),
        );

        $this->updateSettings('REFUND_STATUS', $refundStatusDefault);

        return true;
	}

    /**
     * Uninstall module
     *
     * @return bool
     */
    public function uninstall()
	{		
		if (!parent::uninstall()) {
 	 		return false;
        }

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'blmod_google_ee');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'blmod_google_transactions');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'blmod_google_error');

        Configuration::deleteByName('BLMOD_GA_SETTINGS');
        Configuration::deleteByName('BLMOD_GA_PAYMENTS');
        Configuration::deleteByName('BLMOD_GA_ORDER_STATUS');
        Configuration::deleteByName('BLMOD_GA_REFUND_STATUS');
        Configuration::deleteByName('BLMOD_GA_ignore_gid_warn');

		return true;
	}

    /**
     * Initialize admin
     *
     * @return string
     */
    public function getContent()
	{
        $this->loadModuleData();

        $this->moduleImgPath = '../modules/'.$this->name.'/img/';

		$this->_html = '
		    <div class="blmod_module">
		        <div class="module_logo">
                    <img src="'.$this->moduleImgPath.'icon.png" />
		        </div>
		        <div class="module_title">
                    <h2>'.$this->displayName.'</h2>
                    <div class="module_version">'.$this->l('Version:').' '.$this->version.'</div>
		        </div>
		        <div class="clear_block"></div>';

        if (_PS_VERSION_ >= 1.6) {
            $this->_html .= '
            <style>
                .blmod_module .bootstrap input[type="checkbox"] {
                    margin-top: 2px!important;
                }
            </style>';
        }

        if (_PS_VERSION_ >= 1.5) {
            $this->_html .= '
            <style>
                .blmod_module .conf img, .blmod_module .warn img, .blmod_module .error img, .alert img{
                    display: none;
                }
                .blmod_module .warn, .blmod_module .error, .blmod_module .conf {
                    padding-left: 40px;
                    padding-right: 0px;
                }
            </style>';
        } else if (_PS_VERSION_ < 1.5) {
            $this->_html .= '
            <style>
            .blmod_module .row{
                background: #FFF;
            }
            .module_logo{
                margin-top: 0px!important;
                margin-bottom: 15px;
            }
            .blmod_module #content{
                border: 0px!important;
            }
            .blmod_module .order_table_order{
                width: 900px!important;
            }
            .blmod_module .order_table_date{
                font-size: 11px;
            }
            .order_table_order tr:hover, .order_table_logs tr:hover{
                background-color: #d9edf7!important;
            }
            .info_block_order_status .list_checkbox{
                margin-top: 4px;
            }
            .list_name img{
                margin-right: 4px;
            }
            .icon_menu_box{
                margin-right: 15px!important;
            }
            .blmod_module .pagination{
                margin-bottom: 10px;
            }
            </style>';
        }

        $this->_html .= '<link rel="stylesheet" href="../modules/google_analytics_ee/css/style_admin.css" type="text/css" />';
        $this->_html .= '<script type="text/javascript" src="../modules/google_analytics_ee/js/script_admin.js"></script>';

        if (_PS_VERSION_ < 1.6) {
            $this->_html .= '<link rel="stylesheet" href="../modules/google_analytics_ee/css/style_admin_ps_old.css" type="text/css" />';
            $this->_html .= '<link rel="stylesheet" href="../modules/google_analytics_ee/ps_16/css/admin-theme.css" type="text/css" />';
        }

        $this->full_address_no_t = 'http://' . $_SERVER['HTTP_HOST'] . __PS_BASE_URI__.Tools::substr($_SERVER['PHP_SELF'], Tools::strlen(__PS_BASE_URI__)).'?tab='.Tools::getValue('tab').'&configure='.Tools::getValue('configure');
        $this->token = '&token='.Tools::getValue('token');

        $this->_html .= '<div class="bootstrap">';
        $this->catchSaveAction();

        //Reload settings
        $this->mainSettings = $this->getSettings('SETTINGS', true);

        if (!ToolsBlMod::isCurl()) {
            $this->_html .= '
                <div class="'.$this->setMessageStyle('warning').' empty_message">
                    <img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Warning! Please install/enable CURL').'
                </div>
            ';
        }

        if (empty($this->mainSettings['analytics_id'])) {
            $this->_html .= '
                <div class="'.$this->setMessageStyle('warning').' empty_message">
                    <img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Warning! You must enter Google Analytics tracking ID (Settings > Tracking ID)').'
                </div>
            ';

            $this->hideSendButton = true;
        }

        $this->_html .= '</div>';

        $this->loadPage();

		return $this->_html.'</div>';
	}

    /**
     * Load module data/classes
     */
    public function loadModuleData()
    {
        include_once(dirname(__FILE__).'/ps_function.php');
        include_once(dirname(__FILE__).'/Tools.php');
        include_once(dirname(__FILE__).'/ECommerce.php');
        include_once(dirname(__FILE__).'/Order.php');

        $this->mainSettings = $this->getSettings('SETTINGS', true);

        $this->eCommerceObject = new ECommerceBlMod();
        $this->eCommerceObject->setUserSettings($this->mainSettings);
    }

    /**
     * Load page by get parameter
     */
    public function loadPage()
    {
        $currentPage = Tools::getValue('pageName');
        $this->currentPage = $currentPage;

        $pageList = array('settings', 'orderSend', 'refundSend', 'logs', 'about');

        if (!in_array($currentPage, $pageList)) {
            $currentPage = 'settings';
        }

        $pageName = $currentPage.'Page';

        $this->$pageName();
    }

    /**
     * Catch save action
     */
    public function catchSaveAction()
    {
        $sendOrderList = Tools::getValue('send_order');
        $sendOrderAction = Tools::getValue('send_order_action');
        $sendOrderRefundAction = Tools::getValue('send_refund_action');
        $updateSettingsPage = Tools::getValue('update_settings');

        if (!empty($sendOrderAction) or !empty($sendOrderRefundAction)) {
            $sendFunction = 'sendOrderToGa';
            $messageType = false;

            if (!empty($sendOrderRefundAction)) {
                $sendFunction = 'sendOrderRefundToGA';
                $messageType = 'refund ';
            }

            if (!empty($sendOrderList)) {
                foreach ($sendOrderList as $o) {
                    $this->eCommerceObject->setOrder($o);
                    $this->eCommerceObject->$sendFunction();
                }

                $this->_html .= '<div class="'.$this->setMessageStyle('confirm').'"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Order '.$messageType.'has been sent to Google Analytics').'</div>';
            } else {
                $this->_html .= '<div class="'.$this->setMessageStyle('warning').' empty_message"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Please choose at least one order').'</div>';
            }
        }

        if (!empty($updateSettingsPage)) {
            $this->updateSettingsPage();

            $this->_html .= '<div class="'.$this->setMessageStyle('confirm').'"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings have been updated successfully').'</div>';
        }
    }

    /**
     * Get module menu
     *
     * @return string
     */
    public function getMenu()
    {
        $activeClass = 'menu_active';

        $activeSettings = $activeClass; //first page
        $activeOrderSend = false;
        $activeRefundSend = false;
        $activeLogs = false;
        $activeAbout = false;

        switch ($this->currentPage) {
            case 'settings':
                $activeSettings = $activeClass;
                break;
            case 'orderSend':
                $activeOrderSend = $activeClass;
                $activeSettings = false;
                break;
            case 'refundSend':
                $activeRefundSend = $activeClass;
                $activeSettings = false;
                break;
            case 'logs':
                $activeLogs = $activeClass;
                $activeSettings = false;
                break;
            case 'about':
                $activeAbout = $activeClass;
                $activeSettings = false;
                break;
        }

        $html = '
        <div id="content" class="bootstrap">
            <div class="bootstrap">
                <div class="panel">
                    <div class="panel-heading">
                        <i class="icon-bars"></i> '.$this->l('Menu').'
                    </div>
                    <div class="row">
                        <a href="'.$this->full_address_no_t.'&pageName=settings'.$this->token.'">
                            <div class="icon_menu_box '.$activeSettings.'">
                                <div class="color_box_core color_box_blue">
                                    <i class="icon-cog"></i>
                                </div>
                                <div class="title_text">
                                    <span class="title_menu">'.$this->l('Settings').'</span>
                                    <div class="title_info">'.$this->l('Main module settings').'</div>
                                </div>
                            </div>
                        </a>
                        <a href="'.$this->full_address_no_t.'&pageName=orderSend'.$this->token.'">
                            <div class="icon_menu_box '.$activeOrderSend.'">
                                <div class="color_box_core color_box_green">
                                    <i class="icon-credit-card"></i>
                                </div>
                                <div class="title_text">
                                    <div class="title_menu">'.$this->l('Order').'</div>
                                    <div class="title_info">'.$this->l('Send order to GA').'</div>
                                </div>
                            </div>
                        </a>
                        <a href="'.$this->full_address_no_t.'&pageName=refundSend'.$this->token.'">
                            <div class="icon_menu_box '.$activeRefundSend.'">
                                <div class="color_box_core color_box_red">
                                    <i class="icon-undo"></i>
                                </div>
                                <div class="title_text">
                                    <div class="title_menu">'.$this->l('Refund').'</div>
                                    <div class="title_info">'.$this->l('Send refund to GA').'</div>
                                </div>
                            </div>
                        </a>
                        <a href="'.$this->full_address_no_t.'&pageName=logs'.$this->token.'">
                            <div class="icon_menu_box '.$activeLogs.'">
                                <div class="color_box_core color_box_purple">
                                    <i class="icon-retweet"></i>
                                </div>
                                <div class="title_text">
                                    <div class="title_menu">'.$this->l('Logs').'</div>
                                    <div class="title_info">'.$this->l('Transaction history').'</div>
                                </div>
                            </div>
                        </a>
                        <a href="'.$this->full_address_no_t.'&pageName=about'.$this->token.'">
                            <div class="icon_menu_box '.$activeAbout.'">
                                <div class="color_box_core color_box_grey">
                                    <i class="icon-info"></i>
                                </div>
                                <div class="title_text">
                                    <div class="title_menu">'.$this->l('About').'</div>
                                    <div class="title_info">'.$this->l('Module information').'</div>
                                </div>
                            </div>
                        </a>
                        <div class="clear_block"></div>
                    </div>
                </div>
            </div>
        </div>';

        return $html;
    }

    /**
     * Update settings page
     */
    public function updateSettingsPage()
    {
        $settings = Tools::getValue('settings');
        $payments = Tools::getValue('payments');
        $orderStatus = Tools::getValue('order_status');
        $refundStatus = Tools::getValue('refund_status');
        $ignoreGidWarning = Tools::getValue('ignore_gid_warn');

        if (!empty($ignoreGidWarning)) {
            $ignoreGidWarning = !empty($settings['analytics_id']) ? $settings['analytics_id'] : 1;
            Configuration::updateValue('BLMOD_GA_ignore_gid_warn', $ignoreGidWarning);
        }

        $this->updateSettings('SETTINGS', $settings);
        $this->updateSettings('PAYMENTS', $payments);
        $this->updateSettings('ORDER_STATUS', $orderStatus);
        $this->updateSettings('REFUND_STATUS', $refundStatus);

        if (!empty($orderStatus['status']) and is_array($orderStatus['status']) and !empty($refundStatus['status']) and is_array($refundStatus['status'])) {
            $statusDuplicate = array_intersect($orderStatus['status'], $refundStatus['status']);

            if (!empty($statusDuplicate)) {
                $this->_html .= '<div class="'.$this->setMessageStyle('warning').' empty_message"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Warning! Autosend order and refund have the same status').'</div>';
            }
        }
    }

    /**
     * Settings page, html
     */
    public function settingsPage()
    {
        $orderStatus = $this->getAllOrderStatus();
        $payments = $this->getAllPaymentModules();
        $settingsActive = $this->getSettings('SETTINGS', true);
        $paymentsActive = $this->getSettings('PAYMENTS', true);
        $orderStatusActive = $this->getSettings('ORDER_STATUS', true);
        $refundStatusActive = $this->getSettings('REFUND_STATUS', true);
        $newGATrackingId = $this->detectNewTrackingId();
        $newGATrackingIdHtml = false;
        $ignoreGidWarning = Configuration::get('BLMOD_GA_ignore_gid_warn');

        if (!empty($newGATrackingId)) {
            if ($ignoreGidWarning != 1 and $ignoreGidWarning != $settingsActive['analytics_id']) {
                $newGATrackingIdHtml = '<span class="blmod_comment_warn">'.$this->l('[We found new main GA tracking ID ('.$newGATrackingId.'), maybe you need change it and here?]').'</span>';
                $newGATrackingIdHtml .= '<span class="checkbox_inline"><label for="ignore_gid_warn"><input id="ignore_gid_warn" type="checkbox" name="ignore_gid_warn" value="1"/> Ignore</label></span>';
            }
        }

        $trackingIdEmpty = false;

        if (empty($settingsActive['analytics_id'])) {
            $trackingIdEmpty = 'input_empty';
        }

        $this->_html .= $this->getMenu().'
			<div id="content" class="bootstrap content_blmod">
            <div class="bootstrap">
                <div class="panel">
                    <div class="panel-heading">
                        <i class="icon-cog"></i> '.$this->l('Settings').'
                    </div>
                    <div class="row">
				<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
				    <hr/>
					<div class="name_block">'.$this->l('Main settings:').'</div>
					<div class="info_block">
                        <label for="settings_ga_id" class="list_row">
                            <div class="list_name"><img src="'.$this->moduleImgPath.'ga_logo.png" />'.$this->l('Tracking ID').'</div>
                            <div class="clear_block"></div>
                            <div class="list_input_text">
                                <input id="settings_ga_id" class="'.$trackingIdEmpty.'" placeholder="'.$this->l('You must enter tracking ID').'" type="text" value="'.$settingsActive['analytics_id'].'" name="settings[analytics_id]" />'.$newGATrackingIdHtml.'
                            </div>
                        </label>
                        <div class="clear_block"></div>
                        <div class="blmod_comment">'.$this->l('[The tracking ID / web property ID. The format is UA-XXXX-Y]').'</div>
                        <div class="clear_block"></div>
                        <label for="settings_dp" class="list_row">
                            <div class="list_name"><img src="../img/admin/localization.gif" />'.$this->l('DP (Document Path)').'</div>
                            <div class="clear_block"></div>
                            <div class="list_input_text"><input id="settings_dp" type="text" placeholder="'.$this->l('Recommended insert DP').'" value="'.$settingsActive['analytics_dp'].'" name="settings[analytics_dp]" /></div>
                        </label>
                        <div class="clear_block"></div>
                        <div class="blmod_comment">'.$this->l('[The path portion of the page URL. Should begin with "/"]').'</div>
					</div>
					<div class="clear_block"></div>
					<hr/>
					<div class="name_block">'.$this->l('Payments:').'</div>
					<div class="info_block">';

        if (empty($paymentsActive)) {
            $paymentsActive = array();
        }

        if (!empty($payments)) {
            foreach ($payments as $p) {
                $active = 'checked="checked"';

                if (!is_array($payments)) {
                    $active = false;
                }

                if (is_array($payments) and !in_array($p['name'], $paymentsActive)) {
                    $active = false;
                }

                $this->_html .= '
                    <label for="payment_'.$p['id_module'].'" class="list_row">
                        <div class="list_checkbox"><input id="payment_'.$p['id_module'].'" type="checkbox" name="payments[]" value="'.$p['name'].'" '.$active.'></div>
                        <div class="list_name" ><img src="../modules/'.$p['name'].'/logo.gif"/>'.$p['displayName'].'</div>
                    </label>
                    <div class="clear_block"></div>
                 ';
            }
        } else {
            $this->_html .= '<div class="'.$this->setMessageStyle('warning').'"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Empty payments list').'</div>';
        }

        $orderStatusActive['autosend'] = isset($orderStatusActive['autosend']) ? $orderStatusActive['autosend'] : false;
        $autoSendStatus1 = '';
        $autoSendStatus2 = '';
        $autoSendDisabled = 'disabled="disabled"';

        if ($orderStatusActive['autosend'] == self::ORDER_AUTOSEND_STATUS) {
            $autoSendStatus1 = 'checked="checked"';
            $autoSendDisabled = '';

        } elseif ($orderStatusActive['autosend'] == self::ORDER_AUTOSEND_ALWAYS) {
            $autoSendStatus2 = 'checked="checked"';
        }

        $this->_html .= '
                <div class="blmod_comment">'.$this->l('[Use autosend order (and refund) feature only with selected payments]').'</div>
            </div>
            <div class="clear_block"></div>
            <hr/>
            <div class="name_block name_block_main">'.$this->l('Autosend order:').'</div>
            <div class="info_block info_block_last">
                <label for="autosend_order_always" class="list_row">
                    <div class="list_checkbox"><input class="autosend_order_button" id="autosend_order_always" type="checkbox" name="order_status[autosend]" value="'.self::ORDER_AUTOSEND_ALWAYS.'" '.$autoSendStatus2.'></div>
                    <div class="list_name"><img src="../img/admin/tab.gif"/>'.$this->l('Automatic transmission when order was created').'</div>
                </label>
                <div class="clear_block"></div>
                <label for="autosend_order" class="list_row">
                    <div class="list_checkbox"><input class="autosend_order_button" id="autosend_order" type="checkbox" name="order_status[autosend]" value="'.self::ORDER_AUTOSEND_STATUS.'" '.$autoSendStatus1.'></div>
                    <div class="list_name"><img src="../img/admin/cart.gif"/>'.$this->l('Automatic transmission according to status').'</div>
                </label>
                <div class="clear_block"></div>
            </div>
            <div class="clear_block"></div>
            <div class="name_block">'.$this->l('Order status [order]:').'</div>
            <div class="info_block info_block_order_status">';

        if (empty($orderStatusActive['status'])) {
            $orderStatusActive['status'] = array();
        }

        if (!empty($orderStatus)) {
            foreach ($orderStatus as $s) {
                $active = 'checked="checked"';

                if (is_array($orderStatusActive) and !in_array($s['id_order_state'], $orderStatusActive['status'])) {
                    $active = false;
                }

                $this->_html .= '
                    <label for="status_'.$s['id_order_state'].'" class="list_row">
                        <div class="list_checkbox">
                            <input id="status_'.$s['id_order_state'].'" class="order_status_order" type="checkbox" name="order_status[status][]" value="'.$s['id_order_state'].'" '.$autoSendDisabled.' '.$active.' />
                        </div>
                        <div class="list_name order_status_color_box"><div class="order_status_color" style="background-color: '.$s['color'].'">&nbsp;</div>'.$s['name'].'</div>
                    </label>
                    <div class="clear_block"></div>
                 ';
            }
        } else {
            $this->_html .= '<div class="'.$this->setMessageStyle('warning').'"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Empty order status list').'</div>';
        }

        $autoRefundStatus = 'checked="checked"';
        $autoRefundDisabled = '';

        if (empty($refundStatusActive['autosend'])) {
            $autoRefundStatus = '';
            $autoRefundDisabled = 'disabled="disabled"';
        }

        $this->_html .= '
            </div>
            <div class="clear_block"></div>
            <hr/>
            <div class="name_block name_block_main">'.$this->l('Autosend refund:').'</div>
            <div class="info_block info_block_last">
                <label for="autosend_refund" class="list_row">
                    <div class="list_checkbox"><input id="autosend_refund" type="checkbox" name="refund_status[autosend]" value="1" '.$autoRefundStatus.'></div>
                    <div class="list_name"><img src="../img/admin/forbbiden.gif" />'.$this->l('Automatic transmission according to status').'</div>
                </label>
                <div class="clear_block"></div>
            </div>
            <div class="clear_block"></div>
            <div class="name_block">'.$this->l('Order status [refund]:').'</div>
            <div class="info_block info_block_order_status">';

        if (empty($refundStatusActive['status'])) {
            $refundStatusActive['status'] = array();
        }

        if (!empty($orderStatus)) {
            foreach ($orderStatus as $s) {
                $active = 'checked="checked"';

                if (is_array($refundStatusActive) and !in_array($s['id_order_state'], $refundStatusActive['status'])) {
                    $active = false;
                }

                $this->_html .= '
                    <label for="status_refund_'.$s['id_order_state'].'" class="list_row">
                        <div class="list_checkbox">
                            <input id="status_refund_'.$s['id_order_state'].'" class="order_status_refund" type="checkbox" name="refund_status[status][]" value="'.$s['id_order_state'].'" '.$autoRefundDisabled.' '.$active.' />
                        </div>
                        <div class="list_name order_status_color_box"><div class="order_status_color" style="background-color: '.$s['color'].'">&nbsp;</div>'.$s['name'].'</div>
                    </label>
                    <div class="clear_block"></div>
                 ';
            }
        } else {
            $this->_html .= '<div class="'.$this->setMessageStyle('warning').'"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Empty order status list').'</div>';
        }

        $this->_html .= '
					</div>
					<div class="clear_block"></div>
					<hr/><br/>
					<center><input type="submit" name="update_settings" value="'.$this->l('Update settings').'" class="button" /></center>
			    </form>
			</div></div></div>';
    }

    /**
     * Order send page, html
     */
    public function orderSendPage()
    {
        $isSearch = Tools::getValue('search_order_id');
        $page = Tools::getValue('page');

        $order = new OrderBlMod();
        $order->setCurrentPageNr($page);

        $ordersTotal = 0;

        if (empty($isSearch)) {
            //Default order list
            $orderList = $order->getUnsentOrders();
            $ordersTotal = $order->getTotalUnsentOrders();
        } else {
            //Search
            $orderList = $order->getOrderById($isSearch);
        }

        $this->_html .= $this->getMenu().'
            <div id="content" class="bootstrap content_blmod">
                <div class="bootstrap">
                    <div class="panel">
                        <div class="panel-heading">
                            <i class="icon-credit-card"></i> '.$this->l('Send order').'
                        </div>
                        <div class="row">
                            '.$this->searchForm($isSearch).'
                            <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
                                <div>
                                    '.$this->printOrderTable($orderList, $ordersTotal, $page).'
                                </div>';

                                if (!$this->hideSendButton) {
                                    $this->_html .= '<center><input type="submit" name="send_order_action" value="'.$this->l('Send Order').'" class="button" /></center>';
                                }

                            $this->_html .='
                            </form>
                        </div>
                    </div>
                </div>
		    </div>';
    }

    /**
     * Refund send page, html
     */
    public function refundSendPage()
    {
        $isSearch = Tools::getValue('search_order_id');
        $page = Tools::getValue('page');

        $order = new OrderBlMod();
        $order->setCurrentPageNr($page);

        $ordersTotal = 0;

        if (empty($isSearch)) {
            //Default order list
            $orderList = $order->getUnsentRefundOrders();
            $ordersTotal = $order->getTotalUnsentRefundOrders();
        } else {
            //Search
            $orderList = $order->getRefundOrderById($isSearch);
        }

        $this->_html .= $this->getMenu().'
			<div id="content" class="bootstrap content_blmod">
                <div class="bootstrap">
                    <div class="panel">
                        <div class="panel-heading">
                            <i class="icon-undo"></i> '.$this->l('Send refund').'
                        </div>
                        <div class="row">
                            '.$this->searchForm($isSearch).'
                            <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
                                <div>
                                    '.$this->printOrderTable($orderList, $ordersTotal, $page).'
                                </div>';

                                if (!$this->hideSendButton) {
                                    $this->_html .='<center><input type="submit" name="send_refund_action" value="'.$this->l('Send refund').'" class="button" /></center>';
                                }

                            $this->_html .='
                            </form>
                        </div>
                    </div>
                </div>
		    </div>';
    }

    /**
     * Logs page, html
     */
    public function logsPage()
    {
        $isSearch = Tools::getValue('search_order_id');
        $page = Tools::getValue('page');

        $this->eCommerceObject->setCurrentPageNr($page);

        $transactionsTotal = 0;

        if (empty($isSearch)) {
            //Default order list
            $transactionsList = $this->eCommerceObject->getAllTransaction($page);
            $transactionsTotal = $this->eCommerceObject->getTotalTransaction();
        } else {
            //Search
            $this->eCommerceObject->setOrderId($isSearch);
            $transactionsList = $this->eCommerceObject->getAllTransaction();
        }

        $this->_html .= $this->getMenu().'
			<div id="content" class="bootstrap content_blmod">
                <div class="bootstrap">
                    <div class="panel">
                        <div class="panel-heading">
                            <i class="icon-retweet"></i> '.$this->l('Send order').'
                        </div>
                        <div class="row">
                            '.$this->searchForm($isSearch).'
                            <form action="'.$_SERVER['REQUEST_URI'].'" method="post">';

                                if (empty($transactionsList)) {
                                    $this->_html .= '<div class="'.$this->setMessageStyle('warning').' empty_message"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('No orders found').'</div>';
                                    $this->_html .= '</form></fieldset>';
                                    return false;
                                }

                                $this->_html .= '
                                <div>
                                    <table class="table order_table_logs" cellspacing="0">
                                        <thead>
                                            <tr class="nodrag nodrop">
                                                <th class="fixed-width-xs">
                                                    <span class="title_box">'.$this->l('Order ID').'</span>
                                                </th>
                                                <th class="fixed-width-xs">
                                                    <span class="title_box">'.$this->l('Order send date').'</span>
                                                </th>
                                                <th class="fixed-width-xs">
                                                    <span class="title_box">'.$this->l('Order send').'</span>
                                                </th>
                                                <th class="fixed-width-xs">
                                                    <span class="title_box">'.$this->l('Refund send date').'</span>
                                                </th>
                                                <th class="fixed-width-xs">
                                                    <span class="title_box">'.$this->l('Refund send').'</span>
                                                </th>
                                                <th class="fixed-width-xs text-center center">
                                                    <span class="title_box">'.$this->l('UTMZ').'</span>
                                                </th>
                                                <th class="fixed-width-xs text-center center">
                                                    <span class="title_box">'.$this->l('gClid').'</span>
                                                </th>
                                                <th class="fixed-width-xs text-center center">
                                                    <span class="title_box">'.$this->l('dClid').'</span>
                                                </th>
                                                <th class="fixed-width-xs text-center center" style="width: 50px;">
                                                    <span class="title_box">'.$this->l('View').'</span>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>';

                                        $i = 0;

                                        foreach ($transactionsList as $t) {
                                            $styleRowOdd = 'alt_row';
                                            $styleRow = false;

                                            if ($i & 1) {
                                                $styleRow = $styleRowOdd;
                                            }

                                            $this->_html .= '
                                                <tr class="odd '.$styleRow.'">
                                                    <td class="">'.$t['id_order'].'</td>
                                                    <td class="">'.$this->logDateCell($t, 'order').'</td>
                                                    <td class="">'.$this->logSendStatusCell($t, 'order').'</td>
                                                    <td class="">'.$this->logDateCell($t, 'refund').'</td>
                                                    <td class="">'.$this->logSendStatusCell($t, 'refund').'</td>';

                                                    $this->_html .= $this->displayTrafficSource($t);
                                                    $this->_html .= $this->orderViewImageUrl($t['id_order']);

                                            $this->_html .= '
                                                </tr>
                                               ';

                                            $i++;
                                        }

                                        $this->_html .= '
                                        </tbody>
                                    </table>';

                                    $pagination = ToolsBlMod::pagination($page, ToolsBlMod::ITEM_IN_PAGE, $transactionsTotal, $this->full_address_no_t.'&pageName='.$this->currentPage.$this->token.'&');

                                    $this->_html .= $pagination[2];

                                $this->_html .= '
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
		    </div>';
    }

    /**
     * About module page
     */
    public function aboutPage()
    {
        $moduleVersionOk = '<span style="color: green; font-size: 11px; line-height: 11px;">[' . $this->l('Module\'s version is up to date') . ']</span>';

        $moduleVersion = $moduleVersionOk;

        if (!empty($this->full_name) and !empty($this->version)) {
            $currentVersion = BlModPsFunction::file_get_contents('http://www.blmodules.com/check_module_version.php?n=' . $this->full_name . '&v=' . $this->version);

            if (!empty($currentVersion)) {
                $moduleVersion = '<div class="'.$this->setMessageStyle('warning').' empty_message"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l($currentVersion . ' version is available! You can contact to us for update.').'</div>';
            }
        }

        $this->_html .= $this->getMenu().'
            <style type="text/css">
			.blmod_about a{
				color: #268CCD;
			}
			.blmod_about{
				line-height: 25px;
			}
			</style>
			<div id="content" class="bootstrap content_blmod">
                <div class="bootstrap">
                    <div class="panel">
                        <div class="panel-heading">
                            <i class="icon-info"></i> '.$this->l('About').'
                        </div>
                        <div class="row">
                            <div class="blmod_about">
                                <div style="float: left; margin-right: 30px;">
                                    ' . $this->l('Contact module\'s support team at') . ' <a href="mailto:blmodules@gmail.com">blmodules@gmail.com</a><br/>
                                    ' . $this->l('Find more information at') . '  <a href="http://www.blmodules.com" target="_blank">www.blmodules.com</a><br/>
                                    ' . $this->l('Module version:') . ' ' . $this->version . '<br/>' . $moduleVersion . '
                                </div>
                                <div style="float: left;">
                                    <a href="http://www.blmodules.com" target="_blank">
                                        <img style="border: 1px solid #CCCED7; padding: 0px;" alt="Bl Modules" title="Bl Modules home page" src="../modules/'.$this->name.'/img/blmod_logo.png" />
                                    </a>
                                </div>
                                <div class="clear_block"></div>
                            </div>
                        </div>
                    </div>
                </div>
		    </div>';
    }

    /**
     * @param $transaction
     * @param $type
     * @return string
     */
    public function logDateCell($transaction, $type)
    {
        if (ToolsBlMod::isEmptyDate($transaction['send_'.$type.'_date'])) {
            return $this->l('-');
        }

        $html = $transaction['send_'.$type.'_date'].' <span></span>';

        return $html;
    }

    /**
     * @param $transaction
     * @param $type
     * @param bool $isRefundPage
     * @return string
     */
    public function logSendStatusCell($transaction, $type, $isRefundPage = false)
    {
        if (empty($transaction['send_'.$type])) {
            return $this->l('-');
        }

        $html = '';

        $transaction['send_'.$type.'_error'] = isset($transaction['send_'.$type.'_error']) ? $transaction['send_'.$type.'_error'] : false;
        $errorImg = ' <img src="../img/admin/details.gif" class="showMoreInfoHover" alt="'.$this->l('View error').'" title="'.$transaction['send_'.$type.'_error'].'">';
        $styleBefore = '';
        $styleAfter = '';
        $errorTitle = '';

        if (!empty($isRefundPage)) {
            $errorImg = '';
            $styleBefore = '[';
            $styleAfter = ']';
            $errorTitle = ' title="'.$transaction['send_'.$type.'_error'].'" ';
        }

        switch ($transaction['send_'.$type]) {
            case ECommerceBlMod::SEND_MANUAL:
                $html = '<span class="log_send_manual">'.$styleBefore.$this->l('Manual').$styleAfter.'</span>';
                break;
            case ECommerceBlMod::SEND_AUTO:
                $html = '<span class="log_send_auto">'.$styleBefore.$this->l('Auto').$styleAfter.'</span>';
                break;
            case ECommerceBlMod::SEND_WITH_ERROR:
                $html = '<span class="log_send_with_error showMoreInfoHover" '.$errorTitle.'>'.$styleBefore.$this->l('Error').$styleAfter.$errorImg.'</span>';
                break;
        }

        return $html;
    }

    /**
     * @param bool $orderId
     * @return string
     */
    public function searchForm($orderId = false)
    {
        $orderIdDefault = $orderId;

        $orderId = (int)$orderId;

        if (empty($orderId)) {
            $orderId = false;
        }

        $html = '';

        if (!empty($orderIdDefault) and empty($orderId)) {
            $html .= '<div class="'.$this->setMessageStyle('warning').' empty_message"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Allowed only order ID').'</div>';
        }

        $html .= '
        <div>
            <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
                Order id: <input type="text" class="search_order_id" name="search_order_id" value="'.$orderId.'" />
                <input type="submit" name="search" value="'.$this->l('Search').'" class="button" />
            </form>
        </div>';

        return $html;
    }

    /**
     * Print order view icon
     *
     * @param $orderId
     * @return string
     */
    public function orderViewImageUrl($orderId)
    {
        global $cookie;

        $tab = 'AdminOrders';

        $orderPageToken =  Tools::getAdminToken($tab.(int)(Tab::getIdFromClassName($tab)).$cookie->id_employee);

        if (_PS_VERSION_ >= '1.6') {
            $view = '
                <div class="btn-group pull-right">
                    <a href="index.php?controller=AdminOrders&id_order='.$orderId.'&vieworder&token='.$orderPageToken.'" class="btn btn-default" target="_blank" title="'.$this->l('View order').'">
                        <i class="icon-eye-open"></i> '.$this->l('View').'
                    </a>
                </div>';
        } else {
            $view = '
                <a href="index.php?tab=AdminOrders&id_order='.$orderId.'&vieworder&token='.$orderPageToken.'" target="_blank">
                    <img src="../img/admin/details.gif" alt="'.$this->l('View order').'" title="'.$this->l('View order').'">
                </a>';
        }

        return '<td class="text-center center">'.$view.'</td>';
    }

    /**
     * @param array $orders
     * @param bool $ordersTotal
     * @param int $page
     * @return string
     */
    public function printOrderTable($orders = array(), $ordersTotal = false, $page = 1)
    {
        if (empty($orders)) {
            $html = '<div class="'.$this->setMessageStyle('warning').' empty_message"><img src="../img/admin/warning.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('No orders found').'</div>';
            return $html;
        }

        if ($this->currentPage == 'refundSend') {
            $isRefundPage = true;
        }

        if ($this->currentPage == 'orderSend') {
            $isOrderPage = true;
        }

        $html = '
            <table class="table order_table_order" cellspacing="0">
                <thead>
                    <tr class="nodrag nodrop">
                        <th class="fixed-width-xs text-center center" style="width: 25px;">
                            <span class="title_box"><input class="select_all_orders" type="checkbox" name="select_all_orders" value="0"></span>
                        </th>
                        <th class="fixed-width-xs">
                            <span class="title_box">'.$this->l('Order ID').'</span>
                        </th>
                        <th class="fixed-width-xs" style="width: 200px;">
                            <span class="title_box">'.$this->l('Payment').'</span>
                        </th>
                        <th class="fixed-width-xs" style="width: 200px;">
                            <span class="title_box">'.$this->l('Status').'</span>
                        </th>
                        <th class="fixed-width-xs">
                            <span class="title_box">'.$this->l('Order date').'</span>
                        </th>';

                        if (!empty($isRefundPage)) {
                            $html .= '
                            <th class="fixed-width-xs">
                                <span class="title_box">'.$this->l('Order send').'</span>
                            </th>';
                        }

                        if (!empty($isOrderPage)) {
                            $html .= '
                            <th class="fixed-width-xs text-center center">
                                <span class="title_box">'.$this->l('UTMZ').'</span>
                            </th>
                            <th class="fixed-width-xs text-center center">
                                <span class="title_box">'.$this->l('gClid').'</span>
                            </th>
                            <th class="fixed-width-xs text-center center">
                                <span class="title_box">'.$this->l('dClid').'</span>
                            </th>';
                        }

                        $html .= '
                        <th class="fixed-width-xs text-center center" style="width: 50px;">
                            <span class="title_box">'.$this->l('View').'</span>
                        </th>
                    </tr>
                </thead>
                <tbody>';

        $i = 0;

        foreach ($orders as $o) {
            $orderStatusName = $o['order_status_name'];

            if (empty($orderStatusName)) {
                $orderStatusName = '<span class="order_error_message">'.$this->l('Order error').'</span>';
            }

            $styleRowOdd = 'alt_row';
            $styleRow = false;

            if ($i & 1) {
                $styleRow = $styleRowOdd;
            }

            $html .= '
                <tr class="odd '.$styleRow.'">
                    <td class="text-center center">
                        <input class="send_order_id" type="checkbox" name="send_order[]" value="'.$o['id_order'].'">
                    </td>
                    <td class="">'.$o['id_order'].'</td>
                    <td class="">'.$o['payment'].'</td>
                    <td class="">'.$orderStatusName.'</td>
                    <td class="order_table_date">'.$o['date_upd'].'</td>';

                    if (!empty($isRefundPage)) {
                        $html .= '<td class="">'.$o['send_order_date'].' <span class="send_refund_send_status">'.$this->logSendStatusCell($o, 'order', $isRefundPage).'</span></td>';
                    }

                    if (!empty($isOrderPage)) {
                        $html .= $this->displayTrafficSource($o);
                    }

                    $html .= $this->orderViewImageUrl($o['id_order']).'
                </tr>';

            $i++;
        }

        $html .= '</tbody></table>';

        $pagination = ToolsBlMod::pagination($page, ToolsBlMod::ITEM_IN_PAGE, $ordersTotal, $this->full_address_no_t.'&pageName='.$this->currentPage.$this->token.'&');

        $html .= $pagination[2];

        return $html;
    }

    /**
     * Print traffic source
     *
     * @param array $orderInfo
     * @return string
     */
    public function displayTrafficSource($orderInfo = array())
    {
        $html = '<td class="text-center center">';

        if (!empty($orderInfo['utmz'])) {
            $html .= '<img src="'.$this->moduleImgPath.'information.png" class="showMoreInfoHover" alt="'.$this->l('View utmz').'" title="'.$this->printNiceUtmz($orderInfo['utmz']).'">';
        } else {
            $html .= $this->l('-');
        }

        $html .= '</td>';

        $html .= '<td class="text-center center">';

        if (!empty($orderInfo['gclid'])) {
            $html .= '<img src="'.$this->moduleImgPath.'information.png" class="showMoreInfoHover" alt="'.$this->l('View gclid').'" title="'.$orderInfo['gclid'].'">';
        } else {
            $html .= $this->l('-');
        }

        $html .= '</td>';

        $html .= '<td class="text-center center">';

        if (!empty($orderInfo['dclid'])) {
            $html .= '<img src="'.$this->moduleImgPath.'information.png" class="showMoreInfoHover" alt="'.$this->l('View dclid').'" title="'.$orderInfo['dclid'].'">';
        } else {
            $html .= $this->l('-');
        }

        $html .= '</td>';

        return $html;
    }

    /**
     * Print utmz content
     *
     * @param bool $utmz
     * @return string
     */
    public function printNiceUtmz($utmz = false)
    {
        if (empty($utmz)) {
            return '-';
        }

        $html = '';

        $utmzArray = unserialize(htmlspecialchars_decode($utmz, ENT_QUOTES));

        if (empty($utmzArray)) {
            return '-';
        }

        foreach ($utmzArray as $key => $u) {
            $html .= $key.': '.$u.'<br>';
        }

        return $html;
    }

    /**
     * @param $key
     * @param $value
     */
    public function updateSettings($key, $value)
    {
        Configuration::updateValue('BLMOD_GA_'.$key, htmlspecialchars(serialize($value), ENT_QUOTES));
    }

    /**
     * @param $key
     * @param bool $returnArray
     * @return array|bool|string
     */
    public function getSettings($key, $returnArray = false)
    {
        $value = Configuration::get('BLMOD_GA_'.$key);

        if (empty($value)) {
            if ($returnArray) {
                return array();
            }

            return false;
        }

        return unserialize(htmlspecialchars_decode($value, ENT_QUOTES));
    }

    /**
     * Get all Prestashop order status
     *
     * @return array
     */
    public function getAllOrderStatus()
    {
        global $cookie;

        $id_lang = (int)$cookie->id_lang;

        return Db::getInstance()->ExecuteS('
            SELECT os.`id_order_state`, osl.`name`, os.`color`
            FROM `'._DB_PREFIX_.'order_state` os
            LEFT JOIN `'._DB_PREFIX_.'order_state_lang` osl ON
            (os.`id_order_state` = osl.`id_order_state`)
            WHERE osl.`id_lang` = '.($id_lang).'
            ORDER BY os.`id_order_state` DESC
        ');
    }

    /**
     * Get all Prestashop payment modules
     *
     * @return array
     */
    public function getAllPaymentModules()
    {
        $paymentHookId = 1;

        $paymentModules = Db::getInstance()->ExecuteS('
		SELECT DISTINCT(m.`id_module`), m.`name`
		FROM `'._DB_PREFIX_.'hook_module` h
		LEFT JOIN `'._DB_PREFIX_.'module` m ON
		(h.`id_module` = m.`id_module`)
		WHERE h.`id_hook` = '.($paymentHookId).' AND m.`active` = "1"
		ORDER BY h.`position` ASC');

        if (empty($paymentModules)) {
            return array();
        }

        $modules = Module::getModulesOnDisk();

        foreach ($paymentModules as $id=>$p) {
            foreach ($modules as $m) {
                if ($m->name == $p['name']) {
                    $paymentModules[$id]['displayName'] = $m->displayName;
                    continue;
                }
            }
        }

        return $paymentModules;
    }

    /**
     * Set message box style by PS version
     *
     * @param string $type
     * @return string
     */
    public function setMessageStyle($type)
    {
        switch ($type) {
            case 'warning':
                if (_PS_VERSION_ >= '1.6') {
                    return 'alert alert-warning';
                }

                return 'warning warn';

                break;
            case 'confirm':
                if (_PS_VERSION_ >= '1.6') {
                    return 'alert alert-success';
                }

                return 'conf confirm';

                break;
        }

        return 'warning warn';
    }

    /**
     * Detect new tracking id from Prestashop main module
     *
     * @return bool
     */
    public function detectNewTrackingId()
    {
        $getDefaultGAId = Tools::getValue('ganalytics_id', Configuration::get('GANALYTICS_ID'));
        $settings = $this->getSettings('SETTINGS');

        if ($getDefaultGAId != $settings['analytics_id']) {
            return $getDefaultGAId;
        }

        return false;
    }

    /**
     * Save traffic info
     */
    public function saveTrafficInfo()
    {
        global $cookie;

        $gClid = Tools::getValue('gclid');
        $dClid = Tools::getValue('dclid');

        if (!empty($gClid)) {
            $cookie->blmod_gclid = $gClid;
        }

        if (!empty($dClid)) {
            $cookie->blmod_dclid = $dClid;
        }
    }

    /**
     * Send auto order
     *
     * @param $order
     * @param $orderStatus
     * @return bool
     */
    public function sendAutoOrder($order, $orderStatus)
    {
        global $cookie;

        $orderId = $order->id;

        if (empty($orderId)) {
            return false;
        }

        $paymentsActive = $this->getSettings('PAYMENTS', true);
        $orderStatusActive = $this->getSettings('ORDER_STATUS', true);

        $orderStatusActive['autosend'] = isset($orderStatusActive['autosend']) ? $orderStatusActive['autosend'] : false;

        if ($orderStatusActive['autosend'] != self::ORDER_AUTOSEND_ALWAYS) {
            if (empty($orderStatusActive['status']) or empty($orderStatusActive['autosend']) or empty($paymentsActive)) {
                return false;
            }
        }

        //Check order status
        if ($orderStatusActive['autosend'] != self::ORDER_AUTOSEND_ALWAYS) {
            if ($orderStatusActive['autosend'] == self::ORDER_AUTOSEND_STATUS and !in_array($orderStatus->id, $orderStatusActive['status'])) {
                return false;
            }
        }

        //Check payment type
        if (!in_array($order->module, $paymentsActive)) {
            return false;
        }

        //Check if not has already been sent and order exists
        $orderObject = new OrderBlMod();
        $orderFromDb = $orderObject->getOrderById($orderId);

        if (empty($orderFromDb)) {
            return false;
        }

        //Send order to GA
        $this->eCommerceObject->setOrder($orderId);
        $this->eCommerceObject->sendOrderToGa(ECommerceBlMod::SEND_AUTO);

        //Reset google ads data
        $cookie->blmod_gclid = false;
        $cookie->blmod_dclid = false;
    }

    /**
     * Send auto order when status changed
     *
     * @param bool $newStatus
     * @param bool $orderId
     * @param $order
     * @return bool
     */
    public function sendAutoOrderByStatus($newStatus = false, $orderId = false, $order)
    {
        if (empty($order)) {
            ToolsBlMod::saveError('empty order', 'Google_analytics_ee/sendAutoRefundByStatus', $orderId, $newStatus);
            return false;
        }

        if (empty($newStatus) or empty($orderId)) {
            ToolsBlMod::saveError('empty date', 'Google_analytics_ee/sendAutoOrderByStatus', $orderId, $newStatus);
            return false;
        }

        $orderStatusActive = $this->getSettings('ORDER_STATUS', true);
        $paymentsActive = $this->getSettings('PAYMENTS', true);

        if (empty($orderStatusActive['status']) or empty($orderStatusActive['autosend']) or empty($paymentsActive)) {
            return false;
        }

        if ($orderStatusActive['autosend'] != self::ORDER_AUTOSEND_STATUS) {
            return false;
        }

        //Check order new status
        if (!in_array($newStatus, $orderStatusActive['status'])) {
            return false;
        }

        //Check payment type
        if (!in_array($order->module, $paymentsActive)) {
            return false;
        }

        //Check if not has already been sent and order exists
        $orderObject = new OrderBlMod();
        $orderFromDb = $orderObject->getOrderById($orderId);

        if (empty($orderFromDb)) {
            return false;
        }

        //Send order to GA
        $this->eCommerceObject->setOrder($orderId);
        $this->eCommerceObject->sendOrderToGa(ECommerceBlMod::SEND_AUTO);

        return true;
    }

    /**
     * Send auto refund
     *
     * @param bool $newStatus
     * @param bool $orderId
     * @param object $order
     * @return bool
     */
    public function sendAutoRefundByStatus($newStatus = false, $orderId = false, $order)
    {
        if (empty($order)) {
            ToolsBlMod::saveError('empty order', 'Google_analytics_ee/sendAutoRefundByStatus', $orderId, $newStatus);
            return false;
        }

        if (empty($newStatus) or empty($orderId)) {
            ToolsBlMod::saveError('empty date', 'Google_analytics_ee/sendAutoRefundByStatus', $orderId, $newStatus);
            return false;
        }

        $refundStatusActive = $this->getSettings('REFUND_STATUS', true);
        $paymentsActive = $this->getSettings('PAYMENTS', true);

        if (empty($refundStatusActive['status']) or empty($refundStatusActive['autosend']) or empty($paymentsActive)) {
            return false;
        }

        //Check order new status
        if (!in_array($newStatus, $refundStatusActive['status'])) {
            return false;
        }

        //Check payment type
        if (!in_array($order->module, $paymentsActive)) {
            return false;
        }

        //Check if not has already been sent refund and order exists
        $orderObject = new OrderBlMod();
        $orderFromDb = $orderObject->getRefundOrderById($orderId);

        if (empty($orderFromDb)) {
            return false;
        }

        //Send order refund to GA
        $this->eCommerceObject->setOrder($orderId);
        $this->eCommerceObject->sendOrderRefundToGA(ECommerceBlMod::SEND_AUTO);

        return true;
    }

    /**
     * Header hook, collect traffic info
     */
    public function hookHeader()
    {
        $this->saveTrafficInfo();
    }

    /**
     * Hooke new order
     *
     * @param $params
     */
    public function hookNewOrder($params)
    {
        global $cookie;

        $this->loadModuleData();

        $gclid = !empty($cookie->blmod_gclid) ? $cookie->blmod_gclid : false;
        $dclid = !empty($cookie->blmod_dclid) ? $cookie->blmod_dclid : false;

        $orderInfo = array(
            'utmz' => $this->eCommerceObject->getUserUtmz(),
            'gclid' => $gclid,
            'dclid' => $dclid,
        );

        $orderId = $params['order']->id;

        $this->eCommerceObject->setOrderId($orderId);
        $this->eCommerceObject->saveOrderTransaction($orderInfo);

        $this->sendAutoOrder($params['order'], $params['orderStatus']);
    }

    /**
     * Hook order status
     *
     * @param $params
     * @return bool
     */
    public function hookUpdateOrderStatus($params)
    {
        $this->loadModuleData();

        if (empty($params['id_order'])) {
            ToolsBlMod::saveError('empty date', 'Google_analytics_ee/hookUpdateOrderStatus', 0, $params);
            return false;
        }

        $order = new Order($params['id_order']);

        $orderSend = $this->sendAutoOrderByStatus($params['newOrderStatus']->id, $params['id_order'], $order);

        //Maybe need send refund
        if (!$orderSend) {
            $this->sendAutoRefundByStatus($params['newOrderStatus']->id, $params['id_order'], $order);
        }

        return true;
    }
}