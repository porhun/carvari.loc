$(document).ready(function() {
    $('.showMoreInfoHover').hover(function(){
        var title = $(this).attr('title');
        $(this).data('tipText', title).removeAttr('title');

        if (title == '' || title.length == 0){
            return false;
        }

        $('<p class="showMoreInfo"></p>')
            .html(title)
            .appendTo('body')
            .fadeIn('slow');
    }, function() {
        $(this).attr('title', $(this).data('tipText'));
        $('.showMoreInfo').remove();
    }).mousemove(function(e) {
        var mousex = e.pageX + 15; //Position X
        var mousey = e.pageY - 27; //Position Y
        $('.showMoreInfo').css({ top: mousey, left: mousex });
    });

    $('#autosend_order_always').change(function(){
        if (this.checked) {
            $("#autosend_order").attr("checked", false);
            $(".order_status_order").attr("disabled", true);
        }
    });

    $('#autosend_order').change(function(){
        if (this.checked) {
            $(".order_status_order").removeAttr("disabled");
            $("#autosend_order_always").attr("checked", false);
        } else {
            $(".order_status_order").attr("disabled", true);
        }
    });

    $('#autosend_refund').change(function(){
        if (this.checked) {
            $(".order_status_refund").removeAttr("disabled");
        } else {
            $(".order_status_refund").attr("disabled", true);
        }
    });

    $('.select_all_orders').change(function(){
        if (this.checked) {
            $(".send_order_id").attr("checked", true);
        } else {
            $(".send_order_id").attr("checked", false);
        }
    });
});