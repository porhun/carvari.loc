<?php
/**
 * If you wish to customize this module for your needs,
 * please contact the authors first for more information.
 *
 * It's not allowed selling or reselling this file or any other module files without author permission.
 *
 * @author Bl Modules <blmodules@gmail.com>
 * @copyright 2010-2014 Bl Modules
 * @homepage http://www.blmodules.com
 */

if (!defined('_PS_VERSION_'))
    exit;

/**
 * Class ToolsBlMod
 */
class ToolsBlMod
{
    CONST ITEM_IN_PAGE = 30;

    public static function pagination($page=1, $maxInPage, $totalInvoice, $pageAddress = false)
    {
        $currentPage = $page;
        $html = '<div class = "pagination">';

        if (empty($page)) {
            $page = 1;
            $currentPage = 1;
        }

        if ($maxInPage >= $totalInvoice) {
            $html .= '</div>';
            return array (0, $maxInPage, $html);
        }

        $start = ($maxInPage * $page) - $maxInPage;

        if ($totalInvoice <= $maxInPage) {
            $num_of_pages = 1;
        } elseif (($totalInvoice % $maxInPage) == 0) {
            $num_of_pages = $totalInvoice / $maxInPage;
        } else {
            $num_of_pages = $totalInvoice / $maxInPage + 1;
        }

        if ($currentPage > 1) {
            $back = $currentPage - 1;
            $html .= '<a href = "'.$pageAddress.'page='.$back.'">«</a>' . ' ';
        }

        $html .= '|';
        $num_of_pages_f = (int)$num_of_pages;

        if ($currentPage - 4 > 1) {
            $html .= '<a href = "'.$pageAddress.'page=1">1</a>|';
        }

        if ($currentPage - 5 > 1) {
            $html .= ' ... |';
        }

        $firs_element = $currentPage - 4;

        if($firs_element < 1) {
            $firs_element = 1;
        }

        for ($i = $firs_element; $i < $currentPage; $i++) {
            $html .= '<a href = "'.$pageAddress.'page='.$i.'">'.$i.'</a>|';
        }

        $html .= ' '.$currentPage . ' |';

        for ($i = $currentPage + 1; $i < $currentPage + 5; $i++) {
            if($i > $num_of_pages_f) {
                break;
            }

            $html .= '<a href = "'.$pageAddress.'page='.$i.'">'.$i.'</a>|';
        }

        if ($currentPage + 5 < $num_of_pages_f) {
            $html .= ' ... |';
        }

        if ($currentPage + 4 < $num_of_pages_f) {
            $html .= '<a href = "'.$pageAddress.'page='.$num_of_pages_f.'">'.$num_of_pages_f.'</a>|';
        }

        if ($currentPage + 1 < $num_of_pages) {
            $next = $currentPage + 1;
            $html .= '<a href = "'.$pageAddress.'page='.$next.'">»</a>';
        }

        $html .= '</div>';

        return array ($start, $maxInPage, $html);
    }

    public static function isEmptyDate($date = false)
    {
        if (empty($date)) {
            return true;
        }

        if ($date == '0000-00-00' or $date == '0000-00-00 00:00:00') {
            return true;
        }

        return false;
    }

    public static function saveError($errorText = false, $errorLocation = false, $errorOrderId = 0, $extraData = array())
    {
        $extraDataArray = BlModPsFunction::jsonDecode(BlModPsFunction::jsonEncode($extraData), true);
        $extraDataS = serialize($extraDataArray);

        Db::getInstance()->Execute('
            INSERT INTO `'._DB_PREFIX_.'blmod_google_error`
            (`error_text`, `error_location`, `error_order_id`, `extra_data`, `date_add`)
            VALUES
            ("'.htmlspecialchars($errorText, ENT_QUOTES).'", "'.htmlspecialchars($errorLocation, ENT_QUOTES).'",
            "'.htmlspecialchars($errorOrderId, ENT_QUOTES).'", "'.htmlspecialchars($extraDataS, ENT_QUOTES).'", "'.date('Y-m-d H:i:s').'")
        ');
    }

    public static function isCurl()
    {
        return function_exists('curl_version');
    }
}