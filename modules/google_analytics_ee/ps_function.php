<?php
/**
 * If you wish to customize this module for your needs,
 * please contact the authors first for more information.
 *
 * It's not allowed selling or reselling this file or any other module files without author permission.
 *
 * @author Bl Modules <blmodules@gmail.com>
 * @copyright 2010-2014 Bl Modules
 * @homepage http://www.blmodules.com
 */

/**
 * Class BlModPsFunction
 *
 * Adapt to old Prestashop version
 */
class BlModPsFunction
{
    /**
     * Adapt to old Prestashop version - Tools::file_get_contents()
     *
     * @param bool $url
     * @return bool|mixed|string
     */
    public static function file_get_contents($url=false)
    {
        if (method_exists('Tools','file_get_contents')) {
            return Tools::file_get_contents($url);
        }

        $use_include_path = false;
        $stream_context = null;
        $curl_timeout = 5;

        if ($stream_context == null && preg_match('/^https?:\/\//', $url))
            $stream_context = @stream_context_create(array('http' => array('timeout' => $curl_timeout)));
        if (in_array(ini_get('allow_url_fopen'), array('On', 'on', '1')) || !preg_match('/^https?:\/\//', $url))
            return @file_get_contents($url, $use_include_path, $stream_context);
        elseif (function_exists('curl_init'))
        {
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($curl, CURLOPT_TIMEOUT, $curl_timeout);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            if ($stream_context != null) {
                $opts = stream_context_get_options($stream_context);
                if (isset($opts['http']['method']) && Tools::strtolower($opts['http']['method']) == 'post')
                {
                    curl_setopt($curl, CURLOPT_POST, true);
                    if (isset($opts['http']['content']))
                    {
                        parse_str($opts['http']['content'], $datas);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $datas);
                    }
                }
            }
            $content = curl_exec($curl);
            curl_close($curl);
            return $content;
        }
        else
            return false;
    }

    /**
     * Adapt to old Prestashop version - Tools::jsonDecode()
     *
     * @param $json
     * @return array|mixed
     */
    public static function jsonDecode($json)
    {
        if (method_exists('Tools','jsonDecode')) {
            return Tools::jsonDecode($json);
        }

        $assoc = false;

        if (function_exists('json_decode'))
            return json_decode($json, $assoc);
        else
        {
            include_once(_PS_TOOL_DIR_.'json/json.php');
            $pear_json = new Services_JSON(($assoc) ? SERVICES_JSON_LOOSE_TYPE : 0);
            return $pear_json->decode($json);
        }
    }

    /**
     * Adapt to old Prestashop version - Tools::jsonEncode()
     *
     * @param $data
     * @return mixed|string
     */
    public static function jsonEncode($data)
    {
        if (method_exists('Tools','jsonEncode')) {
            return Tools::jsonEncode($data);
        }

        if (function_exists('json_encode'))
            return json_encode($data);
        else
        {
            include_once(_PS_TOOL_DIR_.'json/json.php');
            $pear_json = new Services_JSON();
            return $pear_json->encode($data);
        }
    }
}