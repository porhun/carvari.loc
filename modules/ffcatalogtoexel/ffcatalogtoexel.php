<?php

if (!defined('_PS_VERSION_'))
    exit;

class Ffcatalogtoexel extends Module
{

    public function __construct()
    {
        $this->name = 'ffcatalogtoexel';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Catalog в exel');
        $this->description = $this->l('Экспорт каталога товаров в exel');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');

    }


    public function install()
    {
        if (!parent::install())
            return false;
        return true;
    }

    public function uninstall()
    {

        if (!parent::uninstall())
            return false;

        return true;
    }

    public function getContent()
    {
        if ( Tools::getValue('makeExport') ) {
            $this->makeExport();
        }

        return $this->context->smarty->fetch(__DIR__.'/views/templates/ffcatalogtoexel.tpl');
    }

    protected function makeExport()
    {
        require_once(__DIR__.'/lib/xlsxwriter.class.php');

        $currency_iso_code= $this->context->currency->iso_code;
        $id_lang = $this->context->language->id;
        // clean buffer
        if (ob_get_level() && ob_get_length() > 0)
            ob_clean();


        $id_feature = Db::getInstance()->getValue('SELECT id_feature FROM ps_feature_lang WHERE `name`="'.'Сезон'.'" AND id_lang='.$this->context->language->id);

        $result = Db::getInstance()->executeS('SELECT pp.id_product, pp.reference, pp.id_category_default, pps.active, ppl.name, ppl.description, ppl.description_short, ppl.link_rewrite, pcl.name as category_name, pfvl.value as season, i.id_image
						FROM ps_product pp
						LEFT JOIN ps_product_lang ppl ON ppl.id_product=pp.id_product AND ppl.id_lang='.$id_lang
            .' LEFT JOIN ps_product_shop pps ON pps.id_product=pp.id_product AND pps.id_shop=1'
            .' LEFT JOIN ps_category_lang pcl ON pcl.id_category=pp.id_category_default AND pcl.id_lang='.$id_lang
            .( $id_feature ? ' LEFT JOIN ps_feature_product pfp ON pfp.id_product=pp.id_product AND pfp.id_feature='.$id_feature : '')
            .' LEFT JOIN ps_feature_value_lang pfvl ON pfvl.id_feature_value=pfp.id_feature_value AND pfvl.id_lang='.$id_lang
            .' INNER JOIN ps_image i ON i.id_product=ppl.id_product AND i.cover=1'
        );



        $link = new LinkCore();

        $content = array();

        $feed_title = array(
            'ID' => 'integer',
            'Item title' => 'string',
            'Final URL' => 'string',
            'Image URL' => 'string',
            'Price' => 'string',
            'Item subtitle' => 'string',
            'Item description' => 'string',
            'Sale price' => 'string',
            'Action' => 'string',
        );


        foreach ( $result as $i => $product ) {
            $feed_line = array();
            $feed_line[] = $product['id_product'];
            $feed_line[] = $product['name'];
            $feed_line[] = $link->getProductLink($product['id_product']);
            $feed_line[] = 'http://'.$link->getImageLink($product['link_rewrite'], $product['id_image'], 'large_default');
            $feed_line[] = number_format( Product::getPriceStatic($product['id_product'], true, null, 6, null, false, false ), 2, '.', '').' '.$currency_iso_code;
            $feed_line[] = $product['reference'];
            $feed_line[] = $product['season'];
            $feed_line[] = number_format( Product::getPriceStatic($product['id_product']), 2, '.', '').' '.$currency_iso_code;
            $feed_line[] = ($product['active'] == 1) ? 'Set' : 'Remove';
            $content[] = $feed_line;
        }


        $filename = "carvari.xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
//        $header = array(
//            'year'=>'string',
//            'month'=>'string',
//            'amount'=>'money',
//            'first_event'=>'datetime',
//            'second_event'=>'date',
//        );

        $writer = new XLSXWriter();
        $writer->setAuthor('ForForce');
        $writer->writeSheet($content,'Sheet1',$feed_title);
        $writer->writeToStdOut();
        //$writer->writeToFile('example.xlsx');
        //echo $writer->writeToString();
        exit(0);


    }

}