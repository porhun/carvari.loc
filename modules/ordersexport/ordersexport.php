<?php

if (!defined('_PS_VERSION_'))
    exit;

class Ordersexport extends Module
{
    private $_html = '';

    public function __construct()
    {
        $this->name = 'ordersexport';
        $this->tab = 'advertising_marketing';
        $this->version = '2.0';
        $this->author = 'Arthur Povorin';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');

        parent::__construct();

        $this->displayName = $this->l('Экспорт заказов в CSV');
        $this->description = $this->l('Экспорт заказов в CSV');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('actionOrderHistoryAddAfter')
        )
            return false;
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;

        return true;
    }

    public function hookActionOrderHistoryAddAfter($params)
    {
        if($_SERVER['HTTP_HOST'] == 'carvari.com')
            $this->export((int)$params[order_history]->id_order);
    }

//    public function getContent()
//    {
//        $this->export(98843);
//        return '';
//    }

    public static function export($id)
    {
//        $sql = 'SELECT r.id_order, ROUND(r.total_paid, 2) total_price, r.date_add date, osl.name status,
//        oc.firstname, oc.lastname, addr.phone_mobile, ROUND(r.total_shipping_tax_incl, 2) delivery_price,
//        (SELECT GROUP_CONCAT(DISTINCT(CONCAT_WS(",",od.product_id, TRIM(p.reference), ROUND(od.product_price, 2),od.product_quantity)) SEPARATOR ";") FROM '._DB_PREFIX_.'order_detail od
//        JOIN '._DB_PREFIX_.'product p ON(od.product_id = p.id_product)
//        WHERE r.id_order = od.id_order) as products
//        FROM '._DB_PREFIX_.'orders r
//        LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON(r.current_state = osl.id_order_state)
//        LEFT JOIN '._DB_PREFIX_.'customer oc ON(r.id_customer = oc.id_customer)
//        LEFT JOIN '._DB_PREFIX_.'address addr ON(addr.id_customer = oc.id_customer)
//        WHERE r.id_order = '.$id;

         $sql = 'SELECT r.id_order, ROUND(r.total_paid, 0) total_price, r.date_add date,osl.name status,
         oc.firstname, oc.lastname, addr.phone_mobile,ROUND(r.total_shipping_tax_incl, 0) delivery_price, ttn.en,
         (SELECT GROUP_CONCAT(DISTINCT(CONCAT_WS(",", IF (pa.ean13 = 0, od.product_ean13, od.product_ean13), TRIM(p.reference), ROUND(od.total_price_tax_incl, 0), od.product_quantity)) SEPARATOR ";") FROM '._DB_PREFIX_.'order_detail od
         JOIN '._DB_PREFIX_.'product p ON(od.product_id = p.id_product)
         LEFT JOIN '._DB_PREFIX_.'product_attribute pa ON (pa.id_product_attribute = od.product_attribute_id)
         WHERE r.id_order = od.id_order) as products,
         em.`code`
         FROM '._DB_PREFIX_.'orders r
         LEFT JOIN '._DB_PREFIX_.'order_state_lang osl ON(r.current_state = osl.id_order_state)
         LEFT JOIN '._DB_PREFIX_.'customer oc ON(r.id_customer = oc.id_customer)
         LEFT JOIN '._DB_PREFIX_.'address addr ON(addr.id_customer = oc.id_customer)
         LEFT JOIN '._DB_PREFIX_.'ecm_newpost_orders ttn ON(r.id_order = ttn.id_order)
         LEFT JOIN '._DB_PREFIX_.'employee em ON(r.id_employee = em.id_employee)
         WHERE osl.id_lang = 1 AND osl.`id_order_state` <> 5 AND r.id_order = '.$id;

        $result = Db::getInstance()->executeS($sql);

        $csv = '';
        // foreach ($result as $key => $value) {
            $dop = '';
            foreach ($result[0] as $key2 => $value2) {

                if($key2 == 'products') {
                    $value2 = explode(';', $value2);

                    if(trim($value2[0])) {

                        foreach ($value2 as $key3 => $value3) {
                            $csv .= $dop;
                            $value3 = explode(',', $value3);

                            foreach ($value3 as $key4 => $value4) {
                                $csv .= $value4.'	';
                            }
                            $csv .= "\r\n";

                        }
                    }
                }
                $dop .= $value2.'	';
            }
        // }

        // convert from uft8 to win-1251
        $csv = iconv('utf-8', 'windows-1251', $csv);

        // создаём файл локально
        file_put_contents ( $_SERVER['DOCUMENT_ROOT'].'/upload/orders/orderexport_'.$id.'.csv' , $csv);

        // загружаем файл на ftp
        $ftp_server = 'ftp.carvari.com';
        $ftp_user_name = 'gianni_sites';
        $ftp_user_pass = '5IOOwXjavuqysxb';
        $remote_file = '/FTP/Gianni_Sites/Carvari/Carvari_rem/Carvari_Orders/orderexport_'.$id.'.csv';

        $file = $_SERVER['DOCUMENT_ROOT'].'/upload/orders/orderexport_'.$id.'.csv';

        // установка соединения
        $conn_id = ftp_connect($ftp_server, 21);

        // проверка имени пользователя и пароля
        $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
        ftp_pasv($conn_id, true);

        // загрузка файла
        if (ftp_put($conn_id, $remote_file, $file, FTP_ASCII)) {

        } else {
            echo "Не удалось загрузить $file на сервер\n";
        }

        // закрытие соединения
        ftp_close($conn_id);
        
    }
}