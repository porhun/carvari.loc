<?php

class AdvancedcheckoutConfirmModuleFrontController extends ModuleFrontController
{
	public $display_column_left = false;
	public $ssl = true;

	public $php_self = 'module-advancedcheckout-confirm';

public function initContent()
{
	parent::initContent();
        /**@var $cart Cart  */
		$cart = $this->context->cart;

		if ($cart->id == 0 || $cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
			Tools::redirect('index.php?controller=order&step=1');

		$customer = new Customer($cart->id_customer);
		if (!Validate::isLoadedObject($customer))
			Tools::redirect('index.php?controller=order&step=1');

		$currency = $this->context->currency;
		$total = (float)$cart->getOrderTotal(true, Cart::BOTH) + OrderCarrier::getCarrierDeliveryPrice($cart->id_carrier);

		require_once(dirname(__FILE__).'/../../classes/Unpay.php');
		$paysistem = new Unpay(1, $this->context->cookie->id_lang);
		if (!Validate::isLoadedObject($paysistem))
			return;

		$mail_vars =	array(
			'{paysistem_name}' => $paysistem->name
		);

		$or = new AdvOrderCreate();
		$or->name = $this->linkrewrite(Tools::str2url($paysistem->name));
		$or->validateOrder((int)$cart->id, $paysistem->id_order_state, $total, $paysistem->name, null,
							$mail_vars, (int)$currency->id, false, ($cart->secure_key ? $cart->secure_key : false));

        if ($paysistem->description_success || $or->currentOrder)
		{
			$order = new Order($or->currentOrder);
//			$description_success = str_replace(
//				array('%total%', '%order_number%'),
//				array(Tools::DisplayPrice($total), sprintf('#%06d', $order->id)),
//				$paysistem->description_success
//			);

			if ($this->context->customer->is_guest)
			{
				$this->context->smarty->assign(array(
					'id_order' => $order->id,
					'reference_order' => $order->reference,
					'id_order_formatted' => sprintf('#%06d', $order->id),
					'email' => $this->context->customer->email
				));
				$this->context->customer->logout();
			}
			$params = array();
			$currency = new Currency($order->id_currency);
			$params['total_to_pay'] = $order->getOrdersTotalPaid();
			$params['currency'] = $currency->sign;
			$params['objOrder'] = $order;
			$params['currencyObj'] = $currency;

//			$this->context->smarty->assign(array(
//				'is_guest' => $this->context->customer->is_guest,
//				'HOOK_ORDER_CONFIRMATION' => Hook::exec('displayOrderConfirmation', $params),
//				'HOOK_PAYMENT_RETURN' => $description_success
//			));

            $order->id_carrier = $cart->id_carrier;
            $order->save();
            $orderCarrier = new OrderCarrier(OrderCarrier::getCarrierByIdOrder($order->id));
            $orderCarrier->id_carrier =  $order->id_carrier;
            $orderCarrier->shipping_cost_tax_incl =  OrderCarrier::getCarrierDeliveryPrice($cart->id_carrier);
            $orderCarrier->save();
            if (Validate::isLoadedObject($order) && $order->id_customer == $this->context->customer->id)
            {
                $id_order_state = (int)$order->getCurrentState();
                $carrier = new Carrier((int)$order->id_carrier, (int)$order->id_lang);
                $addressInvoice = new Address((int)$order->id_address_invoice);
                $addressDelivery = new Address((int)$order->id_address_delivery);

//                $inv_adr_fields = AddressFormat::getOrderedAddressFields($addressInvoice->id_country);
//                $dlv_adr_fields = AddressFormat::getOrderedAddressFields($addressDelivery->id_country);

//                $invoiceAddressFormatedValues = AddressFormat::getFormattedAddressFieldsValues($addressInvoice, $inv_adr_fields);
//                $deliveryAddressFormatedValues = AddressFormat::getFormattedAddressFieldsValues($addressDelivery, $dlv_adr_fields);

                if ($order->total_discounts > 0)
                    $this->context->smarty->assign('total_old', (float)$order->total_paid - $order->total_discounts);
                $products = $order->getProducts();

                /* DEPRECATED: customizedDatas @since 1.5 */
                $customizedDatas = Product::getAllCustomizedDatas((int)$order->id_cart);
                Product::addCustomizationPrice($products, $customizedDatas);

                OrderReturn::addReturnedQuantity($products, $order->id);

                $customer = new Customer($order->id_customer);
				$link = new Link();

//				$image = new Image(14);
//				$image_url = _PS_BASE_URL_._THEME_PROD_DIR_.$image->getExistingImgPath().".jpg";
//
				if ( isset($this->context->cookie->id_store) && !empty( $this->context->cookie->id_store )){
					Db::getInstance()->insert('ffcarvaricarrier', array(
						'id_order' => $order->id,
						'id_store' => $this->context->cookie->id_store,
					));
				}
                if ( isset($this->context->cookie->address) && !empty( $this->context->cookie->address ) ){
                    Db::getInstance()->insert('ffcarvaricourier', array(
                        'id_order' => $order->id,
                        'address' => $this->context->cookie->address,
                    ));
                }
                $order->total_paid_tax_incl=  $order->total_paid_tax_incl + $orderCarrier->shipping_cost_tax_incl;
                $order->total_shipping =  $orderCarrier->shipping_cost_tax_incl;
                $order->total_shipping_tax_incl =  $orderCarrier->shipping_cost_tax_incl;
                $order->save();

                $this->context->smarty->assign(array(
					'order' => $order,
					'products' => $products,
					'discounts' => $order->getCartRules(),
					'use_tax' => Configuration::get('PS_TAX'),
					'group_use_tax' => (Group::getPriceDisplayMethod($customer->id_default_group) == PS_TAX_INC),
					'link' => $link,
					'total' => $total,
					'img_url' => _PS_BASE_URL_._THEME_PROD_DIR_,
                    'cart_qties' => 0,  // set cart products number to 0 because customer has already purchased it successfully
//					'shop_name' => strval(Configuration::get('PS_SHOP_NAME')),
//					'return_allowed' => (int)$order->isReturnable(),
//					'currency' => new Currency($order->id_currency),
//					'order_state' => (int)$id_order_state,
//					'invoiceAllowed' => (int)Configuration::get('PS_INVOICE'),
//					'invoice' => (OrderState::invoiceAvailable($id_order_state) && count($order->getInvoicesCollection())),
//					'order_history' => $order->getHistory($this->context->language->id, false, true),
//					'carrier' => $carrier,
//					'address_invoice' => $addressInvoice,
//					'invoiceState' => (Validate::isLoadedObject($addressInvoice) && $addressInvoice->id_state) ? new State($addressInvoice->id_state) : false,
//					'address_delivery' => $addressDelivery,
//					'inv_adr_fields' => $inv_adr_fields,
//					'dlv_adr_fields' => $dlv_adr_fields,
//					'invoiceAddressFormatedValues' => $invoiceAddressFormatedValues,
//					'deliveryAddressFormatedValues' => $deliveryAddressFormatedValues,
//					'deliveryState' => (Validate::isLoadedObject($addressDelivery) && $addressDelivery->id_state) ? new State($addressDelivery->id_state) : false,
//					'is_guest' => false,
//					'messages' => CustomerMessage::getMessagesByOrderId((int)$order->id, false),
//					'CUSTOMIZE_FILE' => Product::CUSTOMIZE_FILE,
//					'CUSTOMIZE_TEXTFIELD' => Product::CUSTOMIZE_TEXTFIELD,
//					'isRecyclable' => Configuration::get('PS_RECYCLABLE_PACK'),
//					/* DEPRECATED: customizedDatas @since 1.5 */
//					'customizedDatas' => $customizedDatas,
//					/* DEPRECATED: customizedDatas @since 1.5 */
//					'reorderingAllowed' => !(int)Configuration::get('PS_DISALLOW_HISTORY_REORDERING'),
                ));

//                if ($carrier->url && $order->shipping_number)
//                    $this->context->smarty->assign('followup', str_replace('@', $order->shipping_number, $carrier->url));
//                $this->context->smarty->assign('HOOK_ORDERDETAILDISPLAYED', Hook::exec('displayOrderDetail', array('order' => $order)));
//                Hook::exec('actionOrderDetail', array('carrier' => $carrier, 'order' => $order));

                unset($carrier, $addressInvoice, $addressDelivery);
            }
            else
                $this->errors[] = Tools::displayError('This order cannot be found.');
		}
		else
			Tools::redirect('index.php?controller=order-confirmation&id_cart='.(int)$cart->id.'&id_module='.
			(int)$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);


		$this->setTemplate('order-confirmation_f.tpl');
	}


//	public function setTemplate($default_template)
//	{
//		if ($this->context->getMobileDevice() != false)
//			$this->setMobileTemplate($default_template);
//		else
//		{
//			$template = $this->getOverrideTemplate();
//			if ($template)
//				$this->template = $template;
//			else
//				$this->template = $default_template;
//		}
//	}

	public function linkrewrite($insert)
	{
		$insert = mb_strtolower($insert);    // Если работаем с юникодными строками
		$insert = Tools::strtolower($insert); $replase = array(
		// Буквы
		'а'=>'a',
		'б'=>'b',
		'в'=>'v',
		'г'=>'g',
		'д'=>'d',
		'е'=>'e',
		'ё'=>'yo',
		'ж'=>'zh',
		'з'=>'z',
		'и'=>'i',
		'й'=>'j',
		'к'=>'k',
		'л'=>'l',
		'м'=>'m',
		'н'=>'n',
		'о'=>'o',
		'п'=>'p',
		'р'=>'r',
		'с'=>'s',
		'т'=>'t',
		'у'=>'u',
		'ф'=>'f',
		'х'=>'h',
		'ц'=>'c',
		'ч'=>'ch',
		'ш'=>'sh',
		'щ'=>'shh',
		'ъ'=>'j',
		'ы'=>'y',
		'ь'=>'',
		'э'=>'e',
		'ю'=>'yu',
		'я'=>'ya',
		' '=>'_',
		' - '=>'_',
		'-'=>'_',
		'.'=>'',
		':'=>'',
		';'=>'',
		','=>'',
		'!'=>'',
		'?'=>'',
		'>'=>'',
		'<'=>'',
		'&'=>'',
		'*'=>'',
		'%'=>'',
		'$'=>'',
		'"'=>'',
		'\''=>'',
		'('=>'',
		')'=>'',
		'`'=>'',
		'+'=>'',
		'/'=>'',
		'\\'=>'',
		);
		$insert = preg_replace('/  +/', ' ', $insert); // Удаляем лишние пробелы
		$insert = strtr($insert, $replase);
		return $insert;
	}
}

class AdvOrderCreate extends PaymentModule
{
	public $active = true;
	public $module = 'advcheckout';
	public $name = 'advcheckout';
}