<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 06.02.17
 * Time: 12:43
 */
class Tools extends ToolsCore
{
    public static function cleanNonDigitChars($string)
    {
        if (!is_string($string) || !self::strlen($string)) {
            return '';
        }
        return preg_replace('/\D+/', '', $string);
    }
}