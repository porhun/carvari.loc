<div class="form__group form__group--hidden" id="try-login-div">
    {*<div class="row">*}
        {*<div class="col-offset-3 col-9">*}
            {*<div class="input__message input__message--hello">*}
                {*{$customer_name}, {l s='доброго дня!' mod='advancedcheckout'}*}
                {*<br>*}
                {*<b>{l s='Вкажіть свій пароль' mod='advancedcheckout'}</b>*}
            {*</div>*}
        {*</div>*}
    {*</div>*}
    <div class="input_block">
        <label>{l s='Пароль:' mod='advancedcheckout'} {if $field.required}<sup>*</sup>{/if}</label>
        <input type="hidden" name="pssw" id="pssw" value="1"/>
        <input data-validate="isPasswd" autocomplete="off" type="password" class="form__input" id="passwd" name="passwd" />
        {*<input placeholder="{$field.tooltip}" data-validate="is{$field.name}" autocomplete="off" type="text" class="{if $field.required}is_required{/if} validate" id="{$field.name}"  name="{$field.name}" />*}
        <span id="opc_passwd_errors" class="error_msg"></span>
        <div class="order_login_holder clearfix">
            <div class="button btn__login btn--submit" id="try_login">{l s='Вхiд' mod='advancedcheckout'}</div>
            <a href="#" class="input_link" id="get-passwd-by-sms">{l s='Я забув пароль' mod='advancedcheckout'}</a>
        </div>
        {*<span id="opc_{$field.name}_errors" class="error_msg"></span>*}
    </div>
</div>
