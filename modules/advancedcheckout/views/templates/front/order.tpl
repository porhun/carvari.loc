{**
* Module is prohibited to sales! Violation of this condition leads to the deprivation of the license!
*
* @category  Front Office Features
* @package   Advanced Checkout Module
* @author    Maxim Bespechalnih <2343319@gmail.com>
* @copyright 2013-2015 Max
* @license   license.txt in the module folder.
*}

{include file="./css.tpl"}
<script type="text/javascript">
	// <![CDATA[
	var displayPrice = '{$priceDisplay|intval}';
	var logg = '{$logged|escape:"quotes"}';
	var txtProduct = "{l s='product' mod='advancedcheckout'}";
	var txtProducts = "{l s='products' mod='advancedcheckout'}";
	var errpym = "{l s='Select payment methods!' mod='advancedcheckout'}";
	var deliveryAddress = "{$cart->id_address_delivery|intval}";
	var msg_order_carrier = "{l s='You must agree to the terms of service before continuing.' mod='advancedcheckout'}";
	var trm = "{$conditions|intval}";
	var errorPayment = "{l s='Please select payment method!'  mod='advancedcheckout'}";
	var errc = "{l s='Please select delivery method!'  mod='advancedcheckout'}";
	var isseterr = "{l s='There are errors!'  mod='advancedcheckout'}";
	// ]]>
</script>
<div id="orderform">
    {hook h='newOrder' mod='google_analytics_ee'}
{if !$mobile_device}
<div class="clearfix">
	<div class="left_content">
		<h1 class="left_content__title">{if !$logged}{l s='Реєстрація та оформлення' mod='advancedcheckout'}{else}{l s='Оформлення заказу' mod='advancedcheckout'}{/if}</h1>
		<div class="tab_wrapper">
			{if !$logged}
				<div id="tab1" class="tab_content"  style="display: none;">
					<p class="color_red mb10" style="display: none;" id="sms-alert">{l s='На Ваш телефон був надіслан пароль. Введіть його нижче, будь ласка.' mod='advancedcheckout'}</p>
					<div id="login_form_content">
						<form action="{$link->getPageLink('authentication', true, NULL, "back=my-account")|escape:'html':'UTF-8'}" method="post" id="login_form_ff">
							<div class="input_block">
								<label>{l s='Телефон' mod='advancedcheckout'}</label>
								<input class="validate" type="tel" id="login_phone" name="login_phone_ff" placeholder="+380" data-validate="isPhoneNumber" />
								<span id="login_phone_errors" class="error_msg"></span>
							</div>
							<div class="input_block">
								<label>{l s='Пароль' mod='advancedcheckout'}</label>
								<input class="validate" type="password" id="login_passwd" name="login_passwd_ff" data-validate="isPasswd" />
								<span id="login_passwd_errors" class="error_msg"></span>
							</div>

							<div class="btn_holder login">
								<input type="submit" id="SubmitLoginFf" class="button big" value="{l s='Увійти' mod='advancedcheckout'}">
								<a href="{$link->getPageLink('password', true)|escape:'html':'UTF-8'}">{l s='Нагадати пароль' mod='advancedcheckout'}</a>
							</div>
						</form>

					</div>


				</div>
			{/if}
				<div id="tab2" class="tab_content">
					<form class="clearfix">
						<div id="opc-left-user-content" class="user_info">
							<div id="new-acc">
								<div class="opc-user-data"></div>
							</div>
						</div>

						<div class="user_info">
							<div class="profile_section_title">
								{l s='Доставка' mod='advancedcheckout'}
							</div>
							<div id="opc-right-carrier-content" {if $adv_show_carrier}style="display:none;"{/if}>
								<div id="opc-page-loader" class="on"><span class="opc-spinner"><img src="{$img_dir}/preloader.gif"></span></div>
								<div id="opc_delivery_methods" class="opc-main-block"></div>
							</div>

							<div class="profile_section_title">
                                {l s='Спосіб оплати' mod='advancedcheckout'}
							</div>
							<div id="opc-right-payment-content" {if $adv_show_payment}style="display:none;"{/if}>
								{*<div id="oplat" style="display:none;"></div>*}
								<div id="opc-page-loader" class="on"><span class="opc-spinner"><img src="{$img_dir}/preloader.gif"></span></div>
								<div id="opc_payment_methods" class="opc-main-block"></div>
							</div>
						</div>
					</form>
				</div>



		</div>
	</div>
	<div class="right_content">
		<h1 class="right_content__title">{l s='Замовлення' mod='advancedcheckout'}</h1>
		<div id="error-note" class="error_note"></div>
		<div id="opc-right-summary-content">
			<div id="opc-page-loader" class="on"><span class="opc-spinner"><img src="{$img_dir}/preloader.gif" /></span></div>
			<div class="cart-container"></div>
		</div>
		<div class="btn_holder">
			<button type="button" class="button big place_order_ff">
                {l s='Підтвердити' mod='advancedcheckout'}
				<span id="place_order_loader" class="opc-spinner" style="display: none;"><img src="{$img_dir}/preloader.gif"></span>
			</button>
		</div>
	</div>
</div>
{else}
	<div id="error-note" class="error_note"></div>

	<div class="padded_block user_info">

		<h3 class="title">{if !$logged}{l s='Реєстрація та оформлення' mod='advancedcheckout'}{else}{l s='Оформлення заказу' mod='advancedcheckout'}{/if}</h3>
		<div class="tab_wrapper">
            {if !$logged}
				<div id="tab1" class="tab_content" style="display: none;">
					<div id="login_form_content">
						<form action="{$link->getPageLink('authentication', true, NULL, "back=my-account")|escape:'html':'UTF-8'}"
							  method="post" id="login_form_ff">
							<div class="input_block">
								<label>{l s='Телефон' mod='advancedcheckout'}</label>
								<input class="validate" type="tel" id="login_phone" name="login_phone_ff"
									   placeholder="+380" data-validate="isPhoneNumber"/>
								<span id="login_phone_errors" class="error_msg"></span>
							</div>
							<div class="input_block">
								<label>{l s='Пароль' mod='advancedcheckout'}</label>
								<input class="validate" type="password" id="login_passwd" name="login_passwd_ff"
									   data-validate="isPasswd"/>
								<span id="login_passwd_errors" class="error_msg"></span>
							</div>

							<div class="btn_holder login">
								<input type="submit" id="SubmitLoginFf" class="button big"
									   value="{l s='Увійти' mod='advancedcheckout'}">
								<a class="remind_pass"
								   href="{$link->getPageLink('password', true)|escape:'html':'UTF-8'}">{l s='Нагадати пароль' mod='advancedcheckout'}</a>
							</div>
						</form>
					</div>
				</div>
            {/if}

			<div id="tab2" class="tab_content">

				<div id="opc-left-user-content">
					<div id="new-acc">
						<div class="opc-user-data"></div>
					</div>
				</div>
				<h2>
                    {l s='Доставка' mod='advancedcheckout'}
				</h2>

				<div id="opc-right-carrier-content" {if $adv_show_carrier}style="display:none;"{/if}>
					<div id="opc-page-loader" class="on"><span class="opc-spinner"></span></div>
					<div id="opc_delivery_methods" class="opc-main-block"></div>
				</div>
				<h2>
                    {l s='Способ оплаты' mod='advancedcheckout'}
				</h2>
				<div id="opc-right-payment-content" {if $adv_show_payment}style="display:none;"{/if}>
					<div id="oplat" style="display:none;"></div>
					<div id="opc-page-loader" class="on"><span class="opc-spinner"></span></div>
					<div id="opc_payment_methods" class="opc-main-block"></div>
				</div>
			</div>
		</div>
	</div>

	<h1>{l s='Замовлення' mod='advancedcheckout'}</h1>
	<div id="opc-right-summary-content">
		<div id="opc-page-loader" class="on"><span class="opc-spinner"></span></div>
		<div class="cart-container"></div>
	</div>
	<div class="btn_holder btn_holder__place_order_ff">
		<a href="#" class="button big place_order_ff">{l s='Підтвердити' mod='advancedcheckout'}</a>
	</div>
{/if}
	</div>
<div class="clearfix"></div>
