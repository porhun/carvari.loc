{**
* Module is prohibited to sales! Violation of this condition leads to the deprivation of the license!
*
* @category  Front Office Features
* @package   Advanced Checkout Module
* @author    Maxim Bespechalnih <2343319@gmail.com>
* @copyright 2013-2015 Max
* @license   license.txt in the module folder.
*}

<script type="text/javascript">
// <![CDATA[
var new_addr = "{$new_addr|escape:'quotes'}";
var idSelectedCountry = {if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}{if isset($address->id_state)}{$address->id_state|intval}{else}false{/if}{/if};
var idSelectedCountry_i = {if isset($smarty.post.id_state_invoice)}{$smarty.post.id_state_invoice|intval}{else}{if isset($address_i->id_state)}{$address_i->id_state|intval}{else}false{/if}{/if};
var countries = new Array();
var countriesNeedIDNumber = new Array();
var countriesNeedZipCode = new Array();
{foreach from=$countries item='country'}
	{if isset($country.states) && $country.contains_states}
		countries[{$country.id_country|intval}] = new Array();
		{foreach from=$country.states item='state' name='states'}
			countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|addslashes}'{rdelim});
		{/foreach}
	{/if}
	{if $country.need_identification_number}
		countriesNeedIDNumber.push({$country.id_country|intval});
	{/if}
	{if isset($country.zip_code_format)}
		countriesNeedZipCode[{$country.id_country|intval}] = "{$country.zip_code_format}";
	{/if}
{/foreach}
$(document).ready(function(){
	$('.hidepass').hide();
	$('#frsnconnect_form h3').hide();
	if(postcode_refresh){
		$('input[name="postcode"]').typeWatch({
			highlight: true, wait: 1500, captureLength: 0, callback: function(val){
				savepostcode(val, true, this);
			}
		});
	}

	if(city_refresh){
		$('input[name="city"]').typeWatch({
			highlight: true, wait: 1500, captureLength: 0, callback: function(val){
				savecity(val, true, this);
			}
		});
	}
});
//]]>
</script>

<div class="polya">
    {if !$mobile_device}
	<div class="profile_section_title">
		{l s='Контактні данні' mod='advancedcheckout'}
        {if $logged}
			<a class="logout_link" href="{if $active_url_lang}{$active_url_lang}{/if}/?mylogout=">{l s='Вийти'}</a>
        {/if}
	</div>
    {/if}

	<div id="delivery_div">
	{$HOOK_CREATE_ACCOUNT_TOP|escape:'quotes':'UTF-8'}
	<input autocomplete="off" type="hidden" value="1" name="ajx" />
	<input autocomplete="off" type="hidden" value="register" name="method" />
	{if $cn == 0}
		<input autocomplete="off" type="hidden" value="{$defcountry|intval}" name="id_country" id="id_country"/>
		<input autocomplete="off" type="hidden" value="{$defcountry|intval}" name="id_country_invoice" id="id_country_invoice"/>
	{/if}
	{*{if $logged}*}
		{*<br/>*}
		{*<div class="opc-form-group">*}
			{*<label for="id_address_delivery" class="w100p opc-control-label">{l s='Choose a delivery address:' mod='advancedcheckout'}</label>*}
			{*<div class="w100p opc-pr">*}
				{*<select onchange="updadvopcaddr($(this).val(), 'd');" class="address_select opc-form-control" name="id_address_delivery" id="id_address_delivery">*}
					{*{$list_addr|escape:'quotes':'UTF-8'}*}
				{*</select>*}
			{*</div>*}
		{*</div>*}
	{*{/if}*}
	{foreach from=$fields item=field}
		{if $field.type == 'input' and $field.name != 'passwd' and $field.name != 'phone_login'}

			<div class="input_block">
				<label>{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>
				<input placeholder="{$field.tooltip}" data-validate="{$field.validate}" autocomplete="off" type="text" class="{if $field.required}is_required{/if} validate" id="{$field.name}" {if $field.group != '' && count(${$field.group})}{if isset({${$field.group}->$field.name})}value="{${$field.group}->$field.name}"{/if}{/if} name="{$field.name}" {if $logged} readonly="readonly"{/if}/>
				<span id="opc_{$field.name}_errors" class="error_msg"></span>
			</div>

			{*<div class="opc-form-group">*}
				{*<label class="w100p opc-control-label" for="{$field.name}">{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>*}
				{*<div class="w100p">*}
					{*<input data-tooltip="{$field.tooltip}" data-validate="{$field.validate}" autocomplete="off" type="text" class="opc-input-sm opc-form-control {if $field.required}is_required{/if} validate" id="{$field.name}" {if $field.group != '' && count(${$field.group})}{if isset({${$field.group}->$field.name})}value="{${$field.group}->$field.name}"{/if}{/if} name="{$field.name}" placeholder="{$field.description}" />*}
				{*</div>*}
			{*</div>*}
		{elseif $field.type == 'select' and $field.name == 'birthday'}
			<div class="select opc-form-group mb0 date-select">
				<label>{$field.description|escape:'html':'UTF-8'}: {if $field.required}<sup>*</sup>{/if}</label>
				<div class="notinline">
					<div class="opc-form-group opc-inline">
						<div class="w100p opc-pr">
							<select id="days" name="days" class="opc-form-control {if $field.required}is_required{/if} form-control">
								<option value="">-</option>
								{foreach from=$days item=day}
									<option value="{$day|escape:'html':'UTF-8'}" {if count(${$field.group})}{if isset({${$field.group}->$field.name}) && $s_d == $day} selected="selected"{/if}{/if}>{$day|escape:'html':'UTF-8'}&nbsp;&nbsp;</option>
								{/foreach}
							</select>
						</div>
					</div>
					<div class="opc-form-group opc-inline">
						<div class="w100p opc-pr">
							<select id="months" name="months" class="opc-form-control {if $field.required}is_required{/if} form-control">
								<option value="">-</option>
								{foreach from=$months key=k item=month}
									<option value="{$k|escape:'html':'UTF-8'}" {if count(${$field.group})}{if isset({${$field.group}->$field.name}) && $s_m == $k} selected="selected"{/if}{/if}>{l s=$month mod='advancedcheckout'}&nbsp;</option>
								{/foreach}
							</select>
						</div>
					</div>
					<div class="opc-form-group opc-inline">
						<div class="w100p opc-pr">
							<select id="years" name="years" class="opc-form-control {if $field.required}is_required{/if} form-control">
								<option value="">-</option>
								{foreach from=$years item=year}
									<option value="{$year|escape:'html':'UTF-8'}" {if count(${$field.group})}{if isset({${$field.group}->$field.name}) && $s_y == $year} selected="selected"{/if}{/if}>{$year|escape:'html':'UTF-8'}&nbsp;&nbsp;</option>
								{/foreach}
							</select>
						</div>
					</div>
					{*
						{l s='January' mod='advancedcheckout'}
						{l s='February' mod='advancedcheckout'}
						{l s='March' mod='advancedcheckout'}
						{l s='April' mod='advancedcheckout'}
						{l s='May' mod='advancedcheckout'}
						{l s='June' mod='advancedcheckout'}
						{l s='July' mod='advancedcheckout'}
						{l s='August' mod='advancedcheckout'}
						{l s='September' mod='advancedcheckout'}
						{l s='October' mod='advancedcheckout'}
						{l s='November' mod='advancedcheckout'}
						{l s='December' mod='advancedcheckout'}
					*}
				</div>
			</div>
		{elseif $field.type == 'radio' and $field.name == 'gender'}
			<div class="required clearfix gender-line mb0 opc-form-group" data-tooltip="{$field.tooltip|escape:'html':'UTF-8'}">
				<label class="fl">{$field.description|escape:'html':'UTF-8'}: {if $field.required}<sup>*</sup>{/if}</label>
				{foreach from=$genders key=k item=gender}
					<div class="opc-radio opc-inline">
						<label for="id_gender{$gender->id_gender|intval}">
							<input class="opc-form-control" type="radio" name="id_gender" id="id_gender{$gender->id_gender|intval}" value="{$gender->id_gender}" {if count($customer)}{if isset({$customer->id_gender}) && $customer->id_gender == {$gender->id_gender}} checked="checked"{/if}{/if} />
							<span class="opc-text">{$gender->name|escape:'html':'UTF-8'}</span>
						</label>
					</div>
				{/foreach}
			</div>
        {elseif $field.type == 'input' && $field.name == 'phone_login'}
			<div id="js_toggle_hello">
				<div class="input_block">
					<label>{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>
					<input data-tooltip="{$field.tooltip}" data-validate="{$field.validate}"
						   autocomplete="off" type="tel"
						   class="form__input phone_required_text" id="{$field.name}"
						   id="phone_login"
						   placeholder="+38" {if !$logged}value="+38"
						   {else}{if $field.group != '' && count(${$field.group})}{if isset({${$field.group}->$field.name})}value="{${$field.group}->$field.name}"{/if}{/if}{/if}
						   name="{$field.name}" {if $logged && !empty(${$field.group}->$field.name)} readonly="readonly"{/if} {if $field.validate='isPhoneNumber'}maxlength="13{/if}"/>
					<span id="opc_{$field.name}_errors" class="error_msg"></span>
				</div>
			</div>
		{elseif $field.type == 'input' && $field.name == 'passwd'}
		{if !$logged}
			{if !$field.required}
				<div class="opc-form-group">
					<div class="w100p">
						<div class="opc-checkbox">
							<label for="pssw">
								<input data-tooltip="{$field.tooltip|escape:'html':'UTF-8'}" type="checkbox" name="pssw" id="pssw" value="1" autocomplete="off"/>
								<span class="opc-text">{l s='Registration?' mod='advancedcheckout'}</span>
							</label>
						</div>
					</div>
				</div>
				<div class="opc-form-group hidepass" style="display:none;">
					<label class="w100p opc-control-label" for="{$field.name}">{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>
					<div class="w100p">
						<input data-tooltip="{$field.tooltip}" data-validate="{$field.validate}" autocomplete="off" type="text" class="opc-form-control {if $field.required}is_required{/if} validate" id="{$field.name}"  name="{$field.name}" placeholder="{$field.description}" data-validate="is{$field.name}" />
					</div>
				</div>
			{else}
				{*<div class="input_block">*}
					{*<label>{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>*}
					{*<input placeholder="{$field.tooltip}" data-validate="{$field.validate}" autocomplete="off" type="text" class="{if $field.required}is_required{/if} validate" id="{$field.name}" {if $field.group != '' && count(${$field.group})}{if isset({${$field.group}->$field.name})}value="{${$field.group}->$field.name}"{/if}{/if} name="{$field.name}" />*}
					{*<span id="opc_{$field.name}_errors" class="error_msg"></span>*}
				{*</div>*}
				<div class="input_block">
					<label>{$field.description} {if $field.required}<sup>*</sup>{/if}</label>
					<input type="hidden" name="pssw" id="pssw" value="1"/>
					<input placeholder="{$field.tooltip}" data-validate="is{$field.name}" autocomplete="off" type="text" class="{if $field.required}is_required{/if} validate" id="{$field.name}"  name="{$field.name}" />
					<span id="opc_{$field.name}_errors" class="error_msg"></span>
				</div	>
			{*<div class="opc-form-group">*}
				{*<label class="w100p opc-control-label" for="{$field.name}">{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>*}
				{*<input type="hidden" name="pssw" id="pssw" value="1"/>*}
				{*<div class="w100p">*}
					{*<input data-tooltip="{$field.tooltip}" data-validate="{$field.validate}" autocomplete="off" type="text" class="opc-form-control {if $field.required}is_required{/if} validate" id="{$field.name}"  name="{$field.name}" placeholder="{$field.description}" data-validate="is{$field.name}" />*}
				{*</div>*}
			{*</div>*}
			{/if}
		{/if}
		{elseif $field.type == 'select' && $field.name == 'id_country'}
			<div class="opc-form-group" data-tooltip="{$field.tooltip|escape:'html':'UTF-8'}">
				<label class="w100p opc-control-label" for="{$field.name}">{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>
				<div class="w100p opc-pr">
					<select name="{$field.name}" id="{$field.name}" class="{if $field.required}is_required{/if} opc-form-control">
						{foreach from=$countries item=v}
							<option value="{$v.id_country}" {if ($sl_country == $v.id_country)} selected="selected"{/if}>{$v.name|escape:'htmlall':'UTF-8'}</option>
						{/foreach}
					</select>
				</div>
			</div>
		{elseif $field.type == 'select' && $field.name == 'id_state'}
			<div class="opc-form-group" data-tooltip="{$field.tooltip|escape:'html':'UTF-8'}">
				<label class="w100p opc-control-label" for="{$field.name}">{$field.description}: {if $field.required}<sup>*</sup>{/if}</label>
				<div class="w100p opc-pr">
					<select name="{$field.name}" id="{$field.name}" class="{if $field.required}is_required{/if} opc-form-control">
						<option value="">-</option>
					</select>
				</div>
			</div>
		{elseif $field.type == 'textarea'}
			{*<div class="opc-form-group" data-tooltip="{$field.tooltip|escape:'html':'UTF-8'}">*}
				{*<label for="{$field.name|escape:'html':'UTF-8'}" class="w100p opc-control-label">{$field.description|escape:'html':'UTF-8'}: {if $field.required}<sup>*</sup>{/if}</label>*}
				{*<div class="w100p opc-input-icon opc-icon-right">*}
					{*<textarea data-validate="{$field.validate}" class="{if $field.required}is_required{/if} opc-form-control opc-elastic validate" name="{$field.name}" placeholder="{$field.description}" id="{$field.name}" cols="26" rows="3">{if $field.group != '' && count(${$field.group})}{if isset({${$field.group}->$field.name})}{${$field.group}->$field.name}{/if}{/if}</textarea>*}
					{*{if $field.name == 'address1'}*}
						{*<i class="fa fa-envelope"></i>*}
					{*{elseif $field.name == 'address2'}*}
						{*<i class="fa fa-envelope-alt"></i>*}
					{*{else}*}
						{*<i class="fa fa-comment"></i>*}
					{*{/if}*}
				{*</div>*}
			{*</div>*}

			<div class="input_block js_hide_adress">
				<label>{$field.description} {if $field.required}<sup>*</sup>{/if}</label>
				<input type="hidden" name="{$field.name}" id="{$field.name}" value="1"/>
				<textarea placeholder="{$field.tooltip}" data-validate="{$field.validate}" autocomplete="off" type="text" class="{if $field.required}is_required{/if} validate" id="{$field.name}"  name="{$field.name}" cols="49" rows="3">{if $field.group != '' && count(${$field.group})}{if isset({${$field.group}->$field.name})}{${$field.group}->$field.name}{/if}{/if}</textarea>
				<span id="opc_{$field.name}_errors" class="error_msg"></span>
			</div>
		{elseif $field.type == 'checkbox'}
			<div class="opc-form-group">
				<div class="w100p">
					<div class="opc-checkbox">
						<label for="{$field.name|escape:'html':'UTF-8'}">
							<input data-tooltip="{$field.tooltip}" type="checkbox" name="{$field.name}" id="{$field.name}" value="1" {if count(${$field.group})}{if isset({${$field.group}->$field.name}) && ${$field.group}->$field.name == 1}checked="checked"{/if}{/if} autocomplete="off"/>
							<span class="opc-text">{$field.description}{if $field.required == '1'}<sup>*</sup>{/if}</span>
						</label>
					</div>
				</div>
			</div>
		{/if}
	{/foreach}
	</div>

	{*{$HOOK_CREATE_ACCOUNT_FORM}*}
</div>
