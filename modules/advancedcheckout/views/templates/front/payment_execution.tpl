{***}
{** Module is prohibited to sales! Violation of this condition leads to the deprivation of the license!*}
{***}
{** @category  Front Office Features*}
{** @package   Advanced Checkout Module*}
{** @author    Maxim Bespechalnih <2343319@gmail.com>*}
{** @copyright 2013-2015 Max*}
{** @license   license.txt in the module folder.*}


{*<style>*}
	{*#right_column }*}
		{*display:none !important;*}
	{*}*}
{*</style>*}
{*<div class="main_content checkout thx">*}
		{*{capture name=path}{$paysistem->name}{/capture}*}
		{*<div class="bread_crumbs">*}
			{*<a href="{$base_dir}" title="Главная">{l s='Главная' mod='advancedcheckout'}</a>*}
			{*<span class="bc_arrow">→</span>*}
			{*<span class="bc_text">{l s='Оформление заказа' mod='advancedcheckout'}</span>*}
			{*<span class="bc_arrow">→</span>*}
			{*<span class="bc_text">{l s='Подтверждение заказа' mod='advancedcheckout'}</span>*}
		{*</div>*}
		{*<h1 class="title_24">{l s='Order summary' mod='advancedcheckout'}</h1>*}
		{*{if isset($nbProducts) && $nbProducts <= 0}*}
			{*<p class="warning">{l s='Your shopping cart is empty.' mod='advancedcheckout'}</p>*}
		{*{else}*}
		{*<form action="{$link->getModuleLink('advancedcheckout', 'validation', [], true)|escape:'html':'UTF-8'}" method="post">*}
			{*<div class="box">*}
				{*<h3>{$paysistem->name|escape:'html':'UTF-8'}</h3>*}
				{*{$paysistem->description|escape:'quote':'UTF-8'}*}
				{*<div class="fs16 cl_gray mb25">*}
					{*<b>{l s='Please confirm your order by clicking \'I confirm my order\'' mod='advancedcheckout'}.</b>*}
				{*</div>*}
			{*</div>*}
			{*<p class="cart_navigation clearfix" id="cart_navigation">*}
					{*<input type="hidden" name="id_unpay" value="{$paysistem->id|intval}" />*}
				{*<div class="fleft mr20">*}
					{*<button class="tdn grey_button btn_green inline-block btn btn-default js_user_log_out" type="submit">*}
						{*<span>{l s='I confirm my order' mod='advancedcheckout'}<i class="icon-chevron-right right"></i></span>*}
					{*</button>*}
				{*</div>*}
				{*<div class="fleft pt8">*}
				{*<a class="cl_gray fs15 button-exclusive btn btn-default" href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'quote':'UTF-8'}">*}
					{*<i class="icon-chevron-left"></i>{l s='Other payment methods' mod='advancedcheckout'}*}
				{*</a>*}
				{*</div>*}
				{*</p>*}
		{*</form>*}
		{*{/if}*}


	{*<div class="left_content">*}
		{*<h1>Вдячні за покупку!</h1>*}
		{*<h1>Замовлення успішно оформлено №2235423 на суму 7 500 грн.</h1>*}
		{*<div class="thx_text">Протягом години наш менеджер вам зателефонує.</div>*}
		{*<div class="btn_holder">*}
			{*<input type="button" class="button big mt50" value="Продовжити покупки">*}
		{*</div>*}
	{*</div>*}
	{*<div class="right_content">*}
		{*<h1>Замовили</h1>*}
		{*<div class="order_list">*}
			{*<ul class="order_items">*}
				{*<li>*}
					{*<div class="item">*}
						{*<div class="item_img">*}
							{*<a href="#">*}
								{*<img src="../img/main_page_content/order_item.png" alt="">*}
							{*</a>*}
						{*</div>*}
						{*<div class="item_descr">*}
							{*<div class="title"><a href="#">Мокасини жЫночЫ</a></div>*}
							{*<div class="code">JB007</div>*}
							{*<div class="size">Розмір:45.5</div>*}
							{*<div class="price">1 482 грн</div>*}
						{*</div>*}
						{*<div class="delete_item">*}
							{*<i class="icon">*}
								{*<svg>*}
									{*<use xlink:href="#close_icon">*}
								{*</svg>*}
							{*</i>*}
						{*</div>*}
					{*</div>*}
				{*</li>*}
				{*<li>*}
					{*<div class="item">*}
						{*<div class="item_img">*}
							{*<a href="#">*}
								{*<img src="../img/main_page_content/order_item.png" alt="">*}
							{*</a>*}
						{*</div>*}
						{*<div class="item_descr">*}
							{*<div class="title"><a href="#">Мокасини жЫночЫ</a></div>*}
							{*<div class="code">JB007</div>*}
							{*<div class="size">Розмір:45.5</div>*}
							{*<div class="price">1 482 грн</div>*}
						{*</div>*}
						{*<div class="delete_item">*}
							{*<i class="icon">*}
								{*<svg>*}
									{*<use xlink:href="#close_icon">*}
								{*</svg>*}
							{*</i>*}
						{*</div>*}
					{*</div>*}
				{*</li>*}
			{*</ul>*}
		{*</div>*}
		{*<div class="history_link">*}
			{*<a href="#">Історія і статус замовлень</a>*}
		{*</div>*}
	{*</div>*}
{*</div>*}


{capture name=path}{l s='Order confirmation'}{/capture}
{hook h="displayorderconfirmation" mod='ffgoogletagmanager'}
{if !$mobile_device}
	<div class="main_content checkout thx">
		<div class="left_content">
			<h1>{l s='Вдячні за покупку!' mod='advancedcheckout'}</h1>
			<h1>{l s='Замовлення успішно оформлено' mod='advancedcheckout'} <br/>№{$order->id} {l s='на суму' mod='advancedcheckout'} {convertPriceWithCurrency price=$total currency=$currency}</h1>
			<div class="thx_text">{l s='Протягом години наш менеджер вам зателефонує.' mod='advancedcheckout'}</div>
			<div class="btn_holder">
				<input type="button" class="button big mt50" value="{l s='Продовжити покупки' mod='advancedcheckout'}" onclick="location.href='{$base_dir}'">
			</div>
		</div>
		<div class="right_content">
			<h1>{l s='Замовили' mod='advancedcheckout'}</h1>

			<div class="order_list">
				<ul class="order_items">
                    {foreach $products as $product}
						<li>
							<div class="item">
								<div class="item_img">
									<a href="{$link->getProductLink($product.product_id)|escape:'html':'UTF-8'}">
                                        {if $product.image}
											<img src="{$img_url}{$product.image->getExistingImgPath()}-small_default.jpg" alt="">
                                        {/if}
									</a>
								</div>
								<div class="item_descr">
									<div class="title"><a href="{$link->getProductLink($product.product_id)|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></div>
									<div class="code">{$product.reference}</div>
									<div class="size">
                                        {$product.attributes|escape:'html':'UTF-8'|@replace: ',':'<br/>'}
									</div>
									<div class="price">
                                        {if $group_use_tax}
                                            {convertPriceWithCurrency price=$product.total_price_tax_incl currency=$currency}
                                        {else}
                                            {convertPriceWithCurrency price=$product.total_price_tax_excl currency=$currency}
                                        {/if}
									</div>
								</div>
							</div>
						</li>
                    {/foreach}
				</ul>
                {if $discounts}
					<div class="total">

						<div class="price_text clearfix">
							<span class="price_title">{$discounts[0].name}</span>
							<span class="price">
                                {if $group_use_tax}
                                    {convertPriceWithCurrency price=$discounts[0].value currency=$currency}
                                {else}
                                    {convertPriceWithCurrency price=$discounts[0].value_tax_excl currency=$currency}
                                {/if}
                            </span>
						</div>

					</div>
                {/if}

			</div>
			<div class="history_link">
				<a href="{if $active_url_lang}{$active_url_lang}{/if}/order-history">{l s='Історія і статус замовлень' mod='advancedcheckout'}</a>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
{else}
	<div class="main_content proof-page padded_block">
		<h1>{l s='Вдячні за покупку' mod='advancedcheckout'}</h1>
		<h1 class="scss_text">{l s='Замовлення успішно оформлено' mod='advancedcheckout'}<br>
			№ {$order->id}<br>
            {l s='на суму' mod='advancedcheckout'} {convertPriceWithCurrency price=$total currency=$currency}</h1>
		<div class="text">
            {l s='Протягом години наш менеджер вам зателефонує' mod='advancedcheckout'}
		</div>
		<div class="btn_holder">
			<a class="button" href="/">{l s='Продовжити покупки' mod='advancedcheckout'}</a>
		</div>
		<a class="history_link" href="{if $active_url_lang}{$active_url_lang}{/if}/order-history">{l s='Історія і статус замовлень' mod='advancedcheckout'}</a>
	</div>
{/if}
