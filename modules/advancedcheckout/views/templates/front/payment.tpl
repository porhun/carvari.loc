{**
* Module is prohibited to sales! Violation of this condition leads to the deprivation of the license!
*
* @category  Front Office Features
* @package   Advanced Checkout Module
* @author    Maxim Bespechalnih <2343319@gmail.com>
* @copyright 2013-2015 Max
* @license   license.txt in the module folder.
*}

{if $universalpay}
{foreach from=$universalpay item=ps}
	<div class="payment_module">
		<a href="{$link->getModuleLink('advancedcheckout', 'payment', ['id_unpay'=>$ps.id_unpay], true)|escape:'quote':'UTF-8'}" title="{$ps.name|escape:'html':'UTF-8'}" class="universalpay">
			<img src=""/>
			{$ps.description_short|escape:'html':'UTF-8'}
		</a>
	</div>
{/foreach}
{/if}