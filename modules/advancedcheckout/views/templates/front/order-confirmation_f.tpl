{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}{l s='Order confirmation'}{/capture}
{hook h="displayorderconfirmation" mod='ffgoogletagmanager'}
{if !$mobile_device}
    <div class="main_content checkout thx">
        <div class="left_content">
            <h1>{l s='Вдячні за покупку!' mod='advancedcheckout'}</h1>
            <h1>{l s='Замовлення успішно оформлено' mod='advancedcheckout'} <br/>№{$order->id} {l s='на суму' mod='advancedcheckout'} {convertPriceWithCurrency price=$total currency=$currency}</h1>
            <div class="thx_text">{l s='Протягом години наш менеджер вам зателефонує.' mod='advancedcheckout'}</div>
            <div class="btn_holder">
                <input type="button" class="button big mt50" value="{l s='Продовжити покупки' mod='advancedcheckout'}" onclick="location.href='{$base_dir}'">
            </div>
        </div>
        <div class="right_content">
            <h1>{l s='Замовили' mod='advancedcheckout'}</h1>
            <div class="order_list">
                <ul class="order_items">
                    {foreach $products as $product}
                        <li>
                            <div class="item">
                                <div class="item_img">
                                    <a href="{$link->getProductLink($product.product_id)|escape:'html':'UTF-8'}">
                                        {if $product.image}
                                            <img src="{$img_url}{$product.image->getExistingImgPath()}-small_default.jpg" alt="">
                                        {/if}
                                    </a>
                                </div>
                                <div class="item_descr">
                                    <div class="title"><a href="{$link->getProductLink($product.product_id)|escape:'html':'UTF-8'}">{$product.name|escape:'html':'UTF-8'}</a></div>
                                    <div class="code">{$product.reference}</div>
                                    <div class="size">
                                        {$product.attributes|escape:'html':'UTF-8'|@replace: ',':'<br/>'}
                                    </div>
                                    <div class="price">
                                        {if $group_use_tax}
                                            {convertPriceWithCurrency price=$product.total_price_tax_incl currency=$currency}
                                        {else}
                                            {convertPriceWithCurrency price=$product.total_price_tax_excl currency=$currency}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </li>
                    {/foreach}
                </ul>
                {if $discounts}
                    <div class="total">

                        <div class="price_text clearfix">
                            <span class="price_title">{$discounts[0].name}</span>
                            <span class="price">
                                {if $group_use_tax}
                                    {convertPriceWithCurrency price=$discounts[0].value currency=$currency}
                                {else}
                                    {convertPriceWithCurrency price=$discounts[0].value_tax_excl currency=$currency}
                                {/if}
                            </span>
                        </div>

                    </div>
                {/if}

            </div>
            <div class="history_link">
                <a href="{if $active_url_lang}{$active_url_lang}{/if}/order-history">{l s='Історія і статус замовлень' mod='advancedcheckout'}</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
{else}
    <div class="main_content proof-page padded_block">
        <h1>{l s='Вдячні за покупку' mod='advancedcheckout'}</h1>
        <h1 class="scss_text">{l s='Замовлення успішно оформлено' mod='advancedcheckout'}<br>
            № {$order->id}<br>
            {l s='на суму' mod='advancedcheckout'} {convertPriceWithCurrency price=$total currency=$currency}</h1>
        <div class="text">
            {l s='Протягом години наш менеджер вам зателефонує' mod='advancedcheckout'}
        </div>
        <div class="btn_holder">
            <a class="button" href="/">{l s='Продовжити покупки' mod='advancedcheckout'}</a>
        </div>
        <a class="history_link" href="{if $active_url_lang}{$active_url_lang}{/if}/order-history">{l s='Історія і статус замовлень' mod='advancedcheckout'}</a>
    </div>
{/if}