<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

if ( Tools::getValue('key') && Tools::getValue('key') == 'superasdf123') {
	$search_str = trim(Tools::getValue('q'));

	$result = Db::getInstance()
		->executeS('SELECT p.`id_product`, p.`reference`, pl.`name`
                    FROM `'._DB_PREFIX_.'product` p
                    LEFT JOIN  `'._DB_PREFIX_.'product_lang` pl ON pl.id_product=p.id_product
                    LEFT JOIN  `'._DB_PREFIX_.'product_shop` ps ON ps.id_product=p.id_product
                    WHERE (pl.`name` LIKE "%'.pSQL($search_str).'%"
                    OR p.`reference` LIKE "%'.pSQL($search_str).'%")'.
			' AND ps.`active`=1');

	die(Tools::jsonEncode($result));
}

$status = 'success';
$message = '';

include_once(dirname(__FILE__).'/ph_simpleblog.php');
include_once(dirname(__FILE__).'/models/SimpleBlogPost.php');

$action = Tools::getValue('action');

switch ($action){

	case 'addRating':
		$simpleblog_post_id = Tools::getValue('simpleblog_post_id');
		$reply = SimpleBlogPost::changeRating('up', (int)$simpleblog_post_id);		
		$message = $reply[0]["likes"];
	break;

	case 'removeRating':
		$simpleblog_post_id = Tools::getValue('simpleblog_post_id');
		$reply = SimpleBlogPost::changeRating('down', (int)$simpleblog_post_id);		
		$message = $reply[0]["likes"];
	break;

	default:
		$status = 'error';
		$message = 'Unknown parameters!';
	break;
}
$response = new stdClass();
$response->status = $status;
$response->message = $message;
$response->action = $action;
echo json_encode($response);