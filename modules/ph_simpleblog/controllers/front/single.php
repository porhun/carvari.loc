<?php
require_once _PS_MODULE_DIR_ . 'ph_simpleblog/ph_simpleblog.php';

class PH_SimpleBlogSingleModuleFrontController extends ModuleFrontController
{
	protected static $cache_products;

	public $simpleblog_post_rewrite;

	public function init()
	{
		parent::init();

		$simpleblog_post_rewrite = Tools::getValue('rewrite');

		if($simpleblog_post_rewrite)
			$this->simpleblog_post_rewrite = $simpleblog_post_rewrite;
	}

	public function initContent()
	{

        $this->context->controller->addJqueryPlugin('cooki-plugin');

		$sidebar = Configuration::get('PH_BLOG_LAYOUT');

  		if($sidebar == 'left_sidebar')
  		{
  			$this->display_column_left = true;
  			$this->display_column_right = false;
  		}
  		elseif($sidebar == 'right_sidebar') {
  			$this->display_column_left = false;
  			$this->display_column_right = true;
  		}
  		elseif($sidebar == 'full_width')
  		{
  			$this->display_column_left = false;
  			$this->display_column_right = false;
  		}
  		else
  		{
  			$this->display_column_left = true;
  			$this->display_column_right = true;
  		}

  		
    	parent::initContent();

    	$id_lang = Context::getContext()->language->id;

        if((int)Configuration::get('PH_BLOG_ENABLE_LANG'))
        {
            $SimpleBlogPost = SimpleBlogPost::getByRewrite($this->simpleblog_post_rewrite, $id_lang);
        }
        else
        {
            $SimpleBlogPost = SimpleBlogPost::getByRewrite($this->simpleblog_post_rewrite, (int)Configuration::get('PS_LANG_DEFAULT'));
        }

//        $logged = (isset(Context::getContext()->customer) && Context::getContext()->customer->isLogged() ? true : false);
        $logged = false;

        if(Validate::isLoadedObject($SimpleBlogPost) && $SimpleBlogPost->logged && !$logged || Validate::isLoadedObject($SimpleBlogPost) && !$SimpleBlogPost->active)
        {
            Tools::redirect('index.php?controller=404');
        }

      	$this->context->smarty->assign('meta_title', $SimpleBlogPost->meta_title);

        if(!empty($SimpleBlogPost->meta_description))
        {
            $this->context->smarty->assign('meta_description', $SimpleBlogPost->meta_description);
        }

        if(!empty($SimpleBlogPost->meta_keywords))
        {
            $this->context->smarty->assign('meta_keywords', $SimpleBlogPost->meta_keywords);
        }
          	
      	if(!Validate::isLoadedObject($SimpleBlogPost))
        {
            $SimpleBlogPost = SimpleBlogPost::getByRewrite($this->simpleblog_post_rewrite, false);

            if(Validate::isLoadedObject($SimpleBlogPost))
            {
                $SimpleBlogPost = new SimpleBlogPost($SimpleBlogPost->id, $id_lang);
                header('HTTP/1.1 301 Moved Permanently');
                header('Location: '.SimpleBlogPost::getLink($SimpleBlogPost->link_rewrite, $SimpleBlogPost->category_rewrite));
            }
            else
            {
                Tools::redirect('index.php?controller=404');
            }
        }

        $SimpleBlogPost->increaseViewsNb();
//		d(Hook::exec('blogProducts', array('id_category' => $SimpleBlogPost->id_category)));
        $this->context->smarty->assign('recomended_products', $this->renderRecomended( $SimpleBlogPost->id_category, $SimpleBlogPost->id_color, $SimpleBlogPost->id_simpleblog_post ));
        $this->context->smarty->assign('post', $SimpleBlogPost);
        $this->context->smarty->assign('postNext', $SimpleBlogPost->getNext());
        $this->context->smarty->assign('postPrev', $SimpleBlogPost->getPrevious());
		$this->context->smarty->assign('is_16', (bool)(version_compare(_PS_VERSION_, '1.6.0', '>=') === true));
		$this->setTemplate('single.tpl');
		$this->context->smarty->assign('bp', (int)Tools::getValue('bp'));
	}

	public function renderRecomended($id_category = null, $id_color = null, $id_blog = null)
	{

		if ($id_blog && $result = DB::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'simpleblog_post_product` WHERE id_post='.$id_blog) ){
			$ids = [];
			foreach ($result as $row){
				$ids[] =   $row['id_product'];
			}
			$result = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'product` WHERE id_product IN ('.implode(',', $ids ).')');
		} else {
			$nb = 8;

			if (Configuration::get('HOME_FEATURED_RANDOMIZE')) {
				$result = Product::getProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 8), 'RAND()', 'asc', $id_category ? $id_category : null , true, null, $id_color, true );
			}
			else {
				$result = Product::getProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 8), 'id_product', 'desc', $id_category ? $id_category : null, true, null, $id_color, true );
			}

			if (!$result) {
				$result = Product::getProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 8), 'RAND()', 'desc', $id_category ? $id_category : false, true, null, null, true);
			}
		}

		if(!$result)
			return false;

		foreach ($result as &$row) {
			$row['id_image'] = Product::getCover($row['id_product'])['id_image'];
		}

		return Product::getProductsProperties((int)Context::getContext()->language->id, $result);
	}
}