<?php
require_once _PS_MODULE_DIR_ . 'ph_simpleblog/ph_simpleblog.php';

class AdminSimpleBlogPostsController extends ModuleAdminController
{
    protected $max_file_size = null;
    protected $max_image_size = null;
    public $is_16;

    public function __construct()
    {

        $this->table = 'simpleblog_post';
        $this->className = 'SimpleBlogPost';
        $this->lang = true;

        $this->bootstrap = true;

        $this->addRowAction('edit');
        $this->addRowAction('view');
        $this->addRowAction('delete');

        $this->is_16 = (bool)(version_compare(_PS_VERSION_, '1.6.0', '>=') === true);
        
        $this->bulk_actions = array(
                'delete' => array(
                    'text' => $this->l('Delete selected'), 
                    'confirm' => $this->l('Delete selected items?'
                    )
                ),
                'enableSelection' => array('text' => $this->l('Enable selection')),
                'disableSelection' => array('text' => $this->l('Disable selection'))
            );

        $this->_select = 'sbcl.name AS `category`';

        $this->_join = 'LEFT JOIN `'._DB_PREFIX_.'simpleblog_category_lang` sbcl ON (sbcl.`id_simpleblog_category` = a.`id_simpleblog_category` AND sbcl.`id_lang` = '.(int)Context::getContext()->language->id.')';

        $this->fields_list = array(
            'id_simpleblog_post' => array(
                'title' => $this->l('ID'), 
                'align' => 'center', 
                'width' => 30),
            'cover' => array(
                'title' => $this->l('Post thumbnail'), 
                'width' => 150,
                'orderby' => false, 
                'search' => false,
                'callback' => 'getPostThumbnail'
            ),
            'category' => array(
                'title' => $this->l('Category'), 
                'width' => 'auto',
                'filter_key' => 'sbcl!name',
            ),
            'meta_title' => array(
                'title' => $this->l('Name'), 
                'width' => 'auto',
                'filter_key' => 'b!meta_title',
            ),
            'short_content' => array(
                'title' => $this->l('Description'), 
                'width' => 500, 
                'orderby' => false, 
                'callback' => 'getDescriptionClean'
            ),
            'views' => array(
                'title' => $this->l('Views'), 
                'width' => 30,
                'align' => 'center',
                'search' => false,
            ),
            'likes' => array(
                'title' => $this->l('Likes'), 
                'width' => 30,
                'align' => 'center',
                'search' => false,
            ),
            'is_featured' => array(
                'title' => $this->l('Featured?'), 
                'orderby' => false, 
                'align' => 'center', 
                'type' => 'bool', 
                'active' => 'is_featured'
            ),
            'active' => array(
                'title' => $this->l('Displayed'), 'width' => 25, 'active' => 'status',
                'align' => 'center','type' => 'bool', 'orderby' => false
        ));


        parent::__construct();

        $this->max_file_size = (int)(Configuration::get('PS_LIMIT_UPLOAD_FILE_VALUE') * 1000000);
        $this->max_image_size = (int)Configuration::get('PS_PRODUCT_PICTURE_MAX_SIZE');
    }

    public function init()
    {
        parent::init();

        Shop::addTableAssociation($this->table, array('type' => 'shop'));

        if (Shop::getContext() == Shop::CONTEXT_SHOP)
            $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'simpleblog_post_shop` sa ON (a.`id_simpleblog_post` = sa.`id_simpleblog_post` AND sa.id_shop = '.(int)$this->context->shop->id.') ';
        // else
        //     $this->_join .= ' LEFT JOIN `'._DB_PREFIX_.'simpleblog_post_shop` sa ON (a.`simpleblog_post` = sa.`simpleblog_post` AND sa.id_shop = a.id_shop_default) ';

        if (Shop::getContext() == Shop::CONTEXT_SHOP && Shop::isFeatureActive())
            $this->_where = ' AND sa.`id_shop` = '.(int)Context::getContext()->shop->id;

        if (Shop::isFeatureActive() && Shop::getContext() != Shop::CONTEXT_SHOP)
            unset($this->fields_list['position']);
    }

    public function setMedia()
    {
        parent::setMedia();
        $this->addJqueryUi('ui.widget');
        $this->addJqueryPlugin('tagify');
    }

    public static function getDescriptionClean($description)
    {
        return substr(strip_tags(stripslashes($description)), 0, 80).'...';
    }

    public static function getPostThumbnail($cover, $row)
    {
        return ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$row['id_simpleblog_post'].'.'.$cover, 'ph_simpleblog_'.$row['id_simpleblog_post'].'-list.'.$cover, 75, $cover, true);
    }

    public function renderList()
    {
        $this->initToolbar();
        return parent::renderList();
    }

    public function initFormToolBar()
    {
        unset($this->toolbar_btn['back']);    
        $this->toolbar_btn['save-and-stay'] = array(
                        'short' => 'SaveAndStay',
                        'href' => '#',
                        'desc' => $this->l('Save and stay'),
                    );
        $this->toolbar_btn['back'] = array(
                        'href' => self::$currentIndex.'&token='.Tools::getValue('token'),
                        'desc' => $this->l('Back to list'),
                    );
    }

    public function renderForm()
    {

        $this->initFormToolbar();
        if (!$this->loadObject(true))
            return;

        $obj = $this->loadObject(true);

        $cover = false;
        $featured = false;
        $postImg = false;

        if(isset($obj->id))
        {
            $this->display = 'edit';

            $cover = ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$obj->id.'.'.$obj->cover, 'ph_simpleblog_'.$obj->id.'.'.$obj->cover, 350, $obj->cover, false);
            $featured = ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/featured/'.$obj->id.'.'.$obj->featured, 'ph_simpleblog_featured_'.$obj->id.'.'.$obj->featured, 350, $obj->featured, false);
            $postImg = ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$obj->id.'.'.$obj->postImg, 'ph_simpleblog_postimg_'.$obj->id.'.'.$obj->postImg, 350, $obj->postImg, false);
        }
        else
        {
            $this->display = 'add';
        }
        
        $obj->tags = SimpleBlogTag::getPostTags($obj->id);

        $this->tpl_form_vars['PS_ALLOW_ACCENTED_CHARS_URL'] = (int)Configuration::get('PS_ALLOW_ACCENTED_CHARS_URL');
        $this->tpl_form_vars['languages'] = $this->_languages;
        $this->tpl_form_vars['simpleblogpost'] = $obj;
        $this->tpl_form_vars['is_16'] = $this->is_16;
        $this->fields_value = array(
            'cover' => $cover ? $cover : false,
            'cover_size' => $cover ? filesize(_PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$obj->id.'.'.$obj->cover) / 1000 : false,
            'featured' => $featured ? $featured : false,
            'featured_size' => $featured ? filesize(_PS_MODULE_DIR_ . 'ph_simpleblog/featured/'.$obj->id.'.'.$obj->featured) / 1000 : false,
            'postImg' => $postImg ? $postImg : false,
            'postImg_size' => $postImg ? filesize(_PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$obj->id.'.'.$obj->postImg) / 1000 : false,
        );

        $root = Category::getRootCategory();

        //Generating the tree for the first column
        $tree = new HelperTreeCategoriesCore('id_category'); //The string in param is the ID used by the generated tree
        $tree->setUseCheckBox(true)
            ->setFullTree(true)
            ->setAttribute('is_category_filter', $root->id)
            ->setRootCategory($root->id)
            ->setSelectedCategories(array( $obj ? $obj->id_category : null ))
            ->setInputName('id_category'); //Set the name of input. The option "name" of $fields_form doesn't seem to work with "categories_select" type
        $categoryTree = $tree->render();

//        d(array_merge(array(array('id_feature_value'=> '', 'value' => '')),FeatureValue::getFeatureValuesWithLangActive(Configuration::get('PS_LANG_DEFAULT'),7)));

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('SimpleBlog Post'),
                'image' => '../img/admin/tab-categories.gif'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title:'),
                    'name' => 'meta_title',
                    'required' => true,
                    'lang' => true,
                    'id' => 'name',
                    'class' => 'copyNiceUrl',
                ),
                array(
                    'type'  => 'categories_select',
                    'label' => $this->l('Category for recomends'),
                    'desc'    => $this->l('Category for recomended goods'),
                    'name'  => 'id_category',
                    'category_tree'  => $categoryTree //This is the category_tree called in form.tpl
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Color:'),
                    'name' => 'id_color',
                    'options' => array(
                        'id' => 'id_feature_value',
                        'query' => array_merge(array(array('id_feature_value'=> '', 'value' => '')),FeatureValue::getFeatureValuesWithLangActive(Configuration::get('PS_LANG_DEFAULT'),7)),
                        'name' => 'value'
                    )
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description:'),
                    'name' => 'short_content',
                    'lang' => true,
                    'rows' => 5,
                    'cols' => 40,
                    'autoload_rte' => true,
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Content:'),
                    'name' => 'content',
                    'lang' => true,
                    'rows' => 15,
                    'cols' => 40,
                    'autoload_rte' => true,
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Category:'),
                    'name' => 'id_simpleblog_category',
                    'required' => true,
                    'options' => array(
                        'id' => 'id',
                        'query' => SimpleBlogCategory::getCategories($this->context->language->id),
                        'name' => 'name'
                        )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Tags:'),
                    'desc' => $this->l('separate by comma for eg. ipod, apple, something'),
                    'name' => 'tags',
                    'display_tags' => true,
                    'required' => false,
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Author:'),
                    'name' => 'author',
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Featured?'),
                    'name' => 'is_featured',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'is_featured_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'is_featured_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('Displayed:'),
                    'name' => 'active',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),

                array(
                    'type' => 'radio',
                    'label' => $this->l('For logged customers only?'),
                    'name' => 'logged',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'logged_on',
                            'value' => 1,
                            'label' => $this->l('Yes')
                        ),
                        array(
                            'id' => 'logged_off',
                            'value' => 0,
                            'label' => $this->l('No')
                        )
                    ),
                ),
            ),
            'buttons' => array(
                array(
                    'type' => 'submit',
                    'name' => 'submitAddsimpleblog',
                    'icon' => 'process-icon-save',
                    'title' => $this->l('Save'),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save and stay'),
                'class' => 'btn btn-default pull-right',
                'stay' => true,
            ),
        );

        $this->fields_form[1]['form'] = array(
            'legend' => array(
                'title' => $this->l('Post Images'),
                'image' => $this->is_16 ? null : '../img/t/AdminImages.gif',
                'icon'  => 'icon-picture',
            ),
            'input' => array(
               array(
                    'type' => 'file',
                    'label' => $this->l('Post cover:'),
                    'display_image' => true,
                    'name' => 'cover',
                    'desc' => $this->l('Upload a image from your computer.')
                ),
               array(
                    'type' => 'file',
                    'label' => $this->l('Post featured image:'),
                    'display_image' => true,
                    'name' => 'featured',
                    'desc' => $this->l('Upload a image from your computer. Featured image will be displayed only if you want on the single post page.')
                ),
//               array(
//                    'type' => 'file',
//                    'label' => $this->l('Post image:'),
//                    'display_image' => true,
//                    'name' => 'post',
//                    'desc' => $this->l('Upload a image from your computer.')
//               ),

            ),
            'buttons' => array(
                array(
                    'type' => 'submit',
                    'name' => 'submitAddsimpleblog',
                    'icon' => 'process-icon-save',
                    'title' => $this->l('Save'),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save and stay'),
                'class' => 'btn btn-default pull-right',
                'stay' => true,
            ),
        );

        $this->fields_form[2]['form'] = array(
            'legend' => array(
                'title' => $this->l('SimpleBlog SEO'),
                'image' => '../img/admin/tab-categories.gif'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta description:'),
                    'name' => 'meta_description',
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Meta keywords:'),
                    'name' => 'meta_keywords',
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Friendly URL:'),
                    'name' => 'link_rewrite',
                    'required' => true,
                    'lang' => true,
                ),
            ),
            'buttons' => array(
                array(
                    'type' => 'submit',
                    'name' => 'submitAddsimpleblog',
                    'icon' => 'process-icon-save',
                    'title' => $this->l('Save'),
                ),
            ),
            'submit' => array(
                'title' => $this->l('Save and stay'),
                'class' => 'btn btn-default pull-right',
                'stay' => true,
            ),
        );

        if (Shop::isFeatureActive())
            $this->fields_form[3]['form'] = array(
            'legend' => array(
                'title' => $this->l('Shop association:')
            ),      
            'input' => array(   
                array(
                    'type' => 'shop',
                    'label' => $this->l('Shop association:'),
                    'name' => 'checkBoxShopAsso',
                ),
            
            )
        );

        $this->multiple_fieldsets = true;

//        return $this->renderProductAddForm().$this->renderProductList().parent::renderForm().$this->renderPostImg();
        return $this->renderProductAddForm().$this->renderProductList().parent::renderForm();
    }

    public function renderPostImg()
    {
        $this->context->controller->addJqueryPlugin('autocomplete');
        $obj = $this->loadObject(true);

        $id_post = (int)Tools::getvalue('id_simpleblog_post');
        $page = (int)Tools::getValue('page');

        $dir = 'img/SimpleBlog/'.$obj->id.'/';
        $files = scandir(_PS_IMG_DIR_.'SimpleBlog/'.$obj->id.'/');
        $count_images = count($files)-2;
        $images = [];
        $i=0;
        foreach ($files as $img){
            if($img=="." || $img == "..") continue;
            $images[] = ['id' => $i++, 'url' => $dir.$img];
        }

        $languages = Language::getLanguages(true);
        $image_uploader = new HelperImageUploader('file');
        $image_uploader->setAcceptTypes(array('jpeg', 'gif', 'png', 'jpg'))->setMaxSize($this->max_image_size);
        $image_uploader->setMultiple(!(Tools::getUserBrowser() == 'Apple Safari' && Tools::getUserPlatform() == 'Windows'))
            ->setUseAjax(true)->setUrl(
                Context::getContext()->link->getAdminLink('AdminSimpleBlogPosts').'&ajax=1&id_simpleblog_post='.(int)$id_post
                .'&action=addBlogpostImage');
        $protocol_link = (Configuration::get('PS_SSL_ENABLED')) ? 'https://' : 'http://';
        $this->context->smarty->assign(array(
            'countImages' => $count_images,
            'images' => $images,
            'iso_lang' => $languages[0]['iso_code'],
            'token' =>  $this->token,
            'table' => $this->table,
            'id_post' => $id_post,
            'base_dir' => _PS_BASE_URL_.__PS_BASE_URI__,
            'base_dir_ssl' => $protocol_link.Tools::getShopDomainSsl().__PS_BASE_URI__,
            'force_ssl' => Configuration::get('PS_SSL_ENABLED') && Configuration::get('PS_SSL_ENABLED_EVERYWHERE'),
            'max_image_size' => $this->max_image_size / 1024 / 1024,
            'default_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
            'form_action' => $this->context->link->getAdminLink('AdminSimpleBlogPosts').'&'.($id_post ? 'updatesimpleblog_post&id_simpleblog_post='.(int)$id_post : 'addproduct').($page > 1 ? '&page='.(int)$page : ''),
            'image_uploader' => $image_uploader->render()
        ));

        return $this->context->smarty->fetch(_PS_MODULE_DIR_.'ph_simpleblog/views/templates/loadfile.tpl');
    }

    public function ajaxProcessaddBlogpostImage()
    {
        self::$currentIndex = 'index.php?tab=AdminSimpleBlogPosts';

        $obj = $this->loadObject(true);

        $files = array();
        $files[0]['error'] = Tools::displayError('Cannot add image because product creation failed.');

        $image_uploader = new HelperImageUploader('file');
        $image_uploader->setAcceptTypes(array('jpeg', 'gif', 'png', 'jpg'))->setMaxSize($this->max_image_size);
        $files = $image_uploader->process();

        $thumbXhor = Configuration::get('PH_BLOG_POST_HOR_THUMB_X');
        $thumbYhor = Configuration::get('PH_BLOG_POST_HOR_THUMB_Y');

        $thumbXvert = Configuration::get('PH_BLOG_POST_VERT_THUMB_X');
        $thumbYvert = Configuration::get('PH_BLOG_POST_VERT_THUMB_Y');

        $thumbMethod = Configuration::get('PH_BLOG_THUMB_METHOD');

        $total_errors = array();

        foreach ($files as $key => &$file) {
            $errors = array();

            $image = new Image();
//            $link = new Link;
//            $image->image_format

            list($picWidth, $picHeight) = getimagesize($file['save_path']);

            if (isset($file['error']) && (!is_numeric($file['error']) || $file['error'] != 0))
                continue;

            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (isset($file['save_path']) && !ImageManager::checkImageMemoryLimit($file['save_path']))
                $errors[] = Tools::displayError('Due to memory limit restrictions, this image cannot be loaded. Please increase your memory_limit value via your server\'s configuration settings. ');

            $toDir = _PS_IMG_DIR_.'SimpleBlog/'.$obj->id.'/';
            if (!file_exists($toDir))
                mkdir($toDir, 0777);

            $pathAndNameHor = $toDir . $obj->id . '_post_' . $this->translit($file['name']);
            $pathAndNameVert = $toDir . $obj->id . '_post_vert_' . $this->translit($file['name']);

            try {
                if($picWidth >= $picHeight)
                    $thumbHor = PhpThumbFactory::create($file['save_path']);
                else
                    $thumbVert = PhpThumbFactory::create($file['save_path']);
            }
            catch (Exception $e) {
                echo $e;
            }

            if(empty($thumbXhor) || empty($thumbYhor) || empty($thumbXvert) || empty($thumbYvert))
                die(Tools::displayError('Необходимо заполнить размеры картинок в настройках'));

            if($thumbMethod == '1') {
                if($picWidth >= $picHeight)
                    $thumbHor->adaptiveResize($thumbXhor,$thumbYhor);
                else
                    $thumbVert->adaptiveResize($thumbXvert,$thumbYvert);
            }
            elseif($thumbMethod == '2') {
                if($picWidth >= $picHeight)
                    $thumbHor->cropFromCenter($thumbXhor,$thumbYhor);
                else
                    $thumbVert->cropFromCenter($thumbXvert,$thumbYvert);
            }

            if($picWidth >= $picHeight)
                $thumbHor->save($pathAndNameHor) &&
                ImageManager::thumbnail($file['save_path'], $toDir.(int)$obj->id, 350);
            else
                $thumbVert->save($pathAndNameVert) &&
                ImageManager::thumbnail($file['save_path'], $toDir.(int)$obj->id, 350);

            // Copy new image
//            if (!isset($file['save_path']) || empty($errors) && !ImageManager::resize($file['save_path'], $toDir.(int)$obj->id . '-' . $file['name'] . '_thumb.jpg')) {
//                $errors[] = Tools::displayError('An error occurred while uploading the image.');
//            }

            if (count($errors))
                $total_errors = array_merge($total_errors, $errors);

            if (isset($file['save_path']) && is_file($file['save_path']))
                unlink($file['save_path']);

            //Necesary to prevent hacking
            if (isset($file['save_path']))
                unset($file['save_path']);

            if (isset($file['tmp_name']))
                unset($file['tmp_name']);

            $file['status']   = 'ok';
            $file['id']       = $obj->id;
            if($picWidth >= $picHeight)
                $file['path'] = 'img/SimpleBlog/'.$obj->id.'/' . $obj->id . '_post_' . $this->translit($file['name']);
            else
                $file['path'] = 'img/SimpleBlog/'.$obj->id.'/' . $obj->id . '_post_vert_' . $this->translit($file['name']);
            $file['position'] = $image->position;

            //Add image preview and delete url  $toDir . $obj->id . '_post_vert' . $file['name']
//            $file['image'] = ImageManager::thumbnail($file['save_path'], $toDir.(int)$obj->id, 100, 'jpg', true, true);
//            $file['delete_url'] = Context::getContext()->link->getAdminLink('AdminSimpleBlogPosts') . '&deletePostImage=' . (int)$obj->id;
        }

        if (count($total_errors))
            $this->context->controller->errors = array_merge($this->context->controller->errors, $total_errors);
        else
            Tools::clearSmartyCache();

        die(Tools::jsonEncode(array($image_uploader->getName() => $files)));
    }

    public function renderProductAddForm()
    {
        $this->context->controller->addJqueryPlugin('autocomplete');
        

        return $this->context->smarty->fetch(_PS_MODULE_DIR_.'ph_simpleblog/views/templates/product_add_form.tpl');
    }

    public function renderProductList()
    {
        $id_lang = $this->context->language->id;

        $result = Db::getInstance()
            ->executeS('SELECT spb.`active` as product_status, pl.`name`, p.`id_product`, p.`reference` FROM `' . _DB_PREFIX_ . 'simpleblog_post_product` spb
									LEFT JOIN `' . _DB_PREFIX_ . 'product` p ON p.`id_product`=spb.`id_product`
									LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON pl.`id_product`=spb.`id_product` AND pl.`id_lang`='.$id_lang.'
									WHERE spb.id_post='.(int)Tools::getValue('id_simpleblog_post'));


        $this->context->smarty->assign(array(
            'products' => $result,
            'module_link' => AdminController::$currentIndex.'&id_simpleblog_post='.(int)Tools::getValue('id_simpleblog_post').'&updatesimpleblog_post&token='.Tools::getAdminTokenLite('AdminSimpleBlogPosts')
        ));

        return $this->context->smarty->fetch(_PS_MODULE_DIR_.'ph_simpleblog/views/templates/product_list.tpl');
    }

    public function postProcess()
    {
        if( Tools::getValue('id_product_post_status')) {

            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'simpleblog_post_product` SET active=ABS(active-1) WHERE id_post='.(int)Tools::getValue('id_simpleblog_post').' AND id_product='.(int)Tools::getValue('id_product_post_status'));


            Tools::redirectAdmin(AdminController::$currentIndex.'&id_simpleblog_post='.(int)Tools::getValue('id_simpleblog_post').'&updatesimpleblog_post&token='.Tools::getAdminTokenLite('AdminSimpleBlogPosts'));
        }

        if( Tools::getValue('id_product_post_delete')) {
            Db::getInstance()->delete('simpleblog_post_product', 'id_post='.(int)Tools::getValue('id_simpleblog_post').' AND id_product='.(int)Tools::getValue('id_product_post_delete'));
            Tools::redirectAdmin(AdminController::$currentIndex.'&id_simpleblog_post='.(int)Tools::getValue('id_simpleblog_post').'&updatesimpleblog_post&token='.Tools::getAdminTokenLite('AdminSimpleBlogPosts'));
        }

        if (Tools::getValue('id_product_post_add')) {
            DB::getInstance()->insert('simpleblog_post_product',
                [
                    'id_post' => (int)Tools::getValue('id_simpleblog_post'),
                    'id_product' => (int)Tools::getValue('id_product_post_add'),
                    'active' => 1,
                ]);

            Tools::redirectAdmin(AdminController::$currentIndex.'&id_simpleblog_post='.(int)Tools::getValue('id_simpleblog_post').'&updatesimpleblog_post&token='.Tools::getAdminTokenLite('AdminSimpleBlogPosts'));
        }
        
        if (Tools::isSubmit('viewsimpleblog_post') && ($id_simpleblog_post = (int)Tools::getValue('id_simpleblog_post')) && ($SimpleBlogPost = new SimpleBlogPost($id_simpleblog_post, $this->context->language->id)) && Validate::isLoadedObject($SimpleBlogPost))
        {
            $redir = $SimpleBlogPost->getObjectLink($id_lang);
            Tools::redirectAdmin($redir);
        }

        if(Tools::isSubmit('deleteCover'))
        {
            $this->deleteCover((int)Tools::getValue('id_simpleblog_post'));
        }

        if(Tools::isSubmit('deleteFeatured'))
        {
            $this->deleteFeatured((int)Tools::getValue('id_simpleblog_post'));
        }

        return parent::postProcess();
    }

    public function processAdd()
    {
        if (is_array($_POST['id_category']))
            $_POST['id_category'] = $_POST['id_category']['0'];
        else
            $_POST['id_category'] = 0;

        $object = parent::processAdd();

        // Cover

        if (isset($_FILES['cover']) && $_FILES['cover']['size'] > 0)
        {
            $object->cover = pathinfo($_FILES['cover']['name'], PATHINFO_EXTENSION);
            $object->update();
        }

        if(!empty($object->cover))
        {
            $this->createCover($_FILES['cover']['tmp_name'], $object);
        }

        // Post images

        if (isset($_FILES['post']) && $_FILES['post']['size'] > 0)
        {
            $object->postImg = pathinfo($_FILES['post']['name'], PATHINFO_EXTENSION);
            $object->update();
        }

        if(!empty($object->postImg))
        {
            $this->createPostImg($_FILES['post']['tmp_name'], $object);
        }

        // Featured

        if (isset($_FILES['featured']) && $_FILES['featured']['size'] > 0)
        {
            $object->featured = pathinfo($_FILES['featured']['name'], PATHINFO_EXTENSION);
            $object->update();
        }

        if(!empty($object->featured))
        {
            $this->createFeatured($_FILES['featured']['tmp_name'], $object);
        }

        $languages = Language::getLanguages(false);

        $this->updateTags($languages, $object);
        
        $this->updateAssoShop($object->id);
        
        return $object;
    }

    public function processUpdate()
    {

        if (is_array($_POST['id_category']))
            $_POST['id_category'] = $_POST['id_category']['0'];
        else
            $_POST['id_category'] = 0;

        $object = parent::processUpdate();


        // Cover

        if (isset($_FILES['cover']) && $_FILES['cover']['size'] > 0)
        {
            $object->cover = pathinfo($_FILES['cover']['name'], PATHINFO_EXTENSION);
            $object->update();
        }

        if(!empty($object->cover) && isset($_FILES['cover']) && $_FILES['cover']['size'] > 0)
        {
            $this->createCover($_FILES['cover']['tmp_name'], $object);
        }

        // Post images

        if (isset($_FILES['post']) && $_FILES['post']['size'] > 0)
        {
            $object->postImg = pathinfo($_FILES['post']['name'], PATHINFO_EXTENSION);
            $object->update();
        }

        if(!empty($object->postImg) && isset($_FILES['post']) && $_FILES['post']['size'] > 0)
        {
            $this->createPostImg($_FILES['post']['tmp_name'], $object);
        }

        // Featured

        if (isset($_FILES['featured']) && $_FILES['featured']['size'] > 0)
        {
            $object->featured = pathinfo($_FILES['featured']['name'], PATHINFO_EXTENSION);
            $object->update();
        }

        if(!empty($object->featured) && isset($_FILES['featured']) && $_FILES['featured']['size'] > 0)
        {
            $this->createFeatured($_FILES['featured']['tmp_name'], $object);
        }

        $languages = Language::getLanguages(false);

        $this->updateTags($languages, $object);

        $this->updateAssoShop($object->id);

        return $object;
    }

    public function createPostImg($img = null, $object = null)
    {
        if(!isset($img))
            die('AdminSimpleBlogPostsController@createCover: No image to process');

        $thumbXhor = Configuration::get('PH_BLOG_POST_HOR_THUMB_X');
        $thumbYhor = Configuration::get('PH_BLOG_POST_HOR_THUMB_Y');

        $thumbXvert = Configuration::get('PH_BLOG_POST_VERT_THUMB_X');
        $thumbYvert = Configuration::get('PH_BLOG_POST_VERT_THUMB_Y');

        $thumbMethod = Configuration::get('PH_BLOG_THUMB_METHOD');

        list($picWidth, $picHeight) = getimagesize($img);

        if(isset($object) && Validate::isLoadedObject($object))
        {
            $fileTmpLoc = $img;
            $pathAndNameHor = _PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$object->id.'-post.'.$object->postImg;
            $pathAndNameVert = _PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$object->id.'-post_vert.'.$object->postImg;

            $tmp_location = _PS_TMP_IMG_DIR_.'ph_simpleblog_'.$object->id.'.'.$object->postImg;
            if(file_exists($tmp_location))
                @unlink($tmp_location);

            try
            {
                if($picWidth >= $picHeight)
                    $thumbHor = PhpThumbFactory::create($fileTmpLoc);
                else
                    $thumbVert= PhpThumbFactory::create($fileTmpLoc);
            }
            catch (Exception $e)
            {
                echo $e;
            }
            if($thumbMethod == '1')
            {
                if($picWidth >= $picHeight)
                    $thumbHor->adaptiveResize($thumbXhor,$thumbYhor);
                else
                    $thumbVert->adaptiveResize($thumbXvert,$thumbYvert);
            }
            elseif($thumbMethod == '2')
            {
                if($picWidth >= $picHeight)
                    $thumbHor->cropFromCenter($thumbXhor,$thumbYhor);
                else
                    $thumbVert->cropFromCenter($thumbXvert,$thumbYvert);
            }

            if($picWidth >= $picHeight){
                return
                    $thumbHor->save($pathAndNameHor) &&
                    ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$object->id.'.'.$object->postImg, 'ph_simpleblog_postimg_'.$object->id.'.'.$object->postImg, 350, $object->postImg);
            }else{
                return
                    $thumbVert->save($pathAndNameVert) &&
                    ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$object->id.'.'.$object->postImg, 'ph_simpleblog_postimg_'.$object->id.'.'.$object->postImg, 350, $object->postImg);
            }

        }
    }

    public function createCover($img = null, $object = null)
    {
        if(!isset($img))
            die('AdminSimpleBlogPostsController@createCover: No image to process');

        $thumbX = Configuration::get('PH_BLOG_THUMB_X');
        $thumbY = Configuration::get('PH_BLOG_THUMB_Y');

        $thumb_wide_X = Configuration::get('PH_BLOG_THUMB_X_WIDE');
        $thumb_wide_Y = Configuration::get('PH_BLOG_THUMB_Y_WIDE');

        $thumbMethod = Configuration::get('PH_BLOG_THUMB_METHOD');

        if(isset($object) && Validate::isLoadedObject($object))
        {
            $fileTmpLoc = $img;
            $origPath = _PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$object->id.'.'.$object->cover;
            $pathAndName = _PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$object->id.'-thumb.'.$object->cover;
            $pathAndNameWide = _PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$object->id.'-wide.'.$object->cover;

            $tmp_location = _PS_TMP_IMG_DIR_.'ph_simpleblog_'.$object->id.'.'.$object->cover;
            if(file_exists($tmp_location))
                @unlink($tmp_location);

            $tmp_location_list = _PS_TMP_IMG_DIR_.'ph_simpleblog_'.$object->id.'-list.'.$object->cover;
            if(file_exists($tmp_location_list))
                @unlink($tmp_location_list);

            try
            {
                $orig = PhpThumbFactory::create($fileTmpLoc);
                $thumb = PhpThumbFactory::create($fileTmpLoc);
                $thumbWide = PhpThumbFactory::create($fileTmpLoc);
            }
            catch (Exception $e)
            {
                echo $e;
            }

            if($thumbMethod == '1')
            {
                $thumb->adaptiveResize($thumbX,$thumbY);
                $thumbWide->adaptiveResize($thumb_wide_X,$thumb_wide_Y);
            }
            elseif($thumbMethod == '2')
            {
                $thumb->cropFromCenter($thumbX,$thumbY);
                $thumbWide->cropFromCenter($thumb_wide_X,$thumb_wide_Y);
            }

            return $orig->save($origPath) &&
                   $thumb->save($pathAndName) &&
                   $thumbWide->save($pathAndNameWide) &&
                   ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$object->id.'.'.$object->cover, 'ph_simpleblog_'.$object->id.'.'.$object->cover, 350, $object->cover);
        }

    }

    public function createFeatured($img = null, $object = null)
    {
        if(!isset($img))
            die('AdminSimpleBlogPostsController@createFeatured: No image to process');

        if(isset($object) && Validate::isLoadedObject($object))
        {
            $fileTmpLoc = $img;
            $origPath = _PS_MODULE_DIR_ . 'ph_simpleblog/featured/'.$object->id.'.'.$object->featured;

            $this->deleteFeatured($object->id, true);

            try
            {
                $orig = PhpThumbFactory::create($fileTmpLoc);
            }
            catch (Exception $e)
            {
                echo $e;
            }
           
            return $orig->save($origPath) && $tmp_featured_location && ImageManager::thumbnail(_PS_MODULE_DIR_ . 'ph_simpleblog/featured/'.$object->id.'.'.$object->featured, 'ph_simpleblog_featured_'.$object->id.'.'.$object->featured, 350, $object->featured);
        }

    }

    public function deleteFeatured($id, $only_delete = false)
    {
        $object = new SimpleBlogPost($id, Context::getContext()->language->id);

        $tmp_location = _PS_TMP_IMG_DIR_.'ph_simpleblog_featured_'.$object->id.'.'.$object->featured;
        if(file_exists($tmp_location))
            @unlink($tmp_location);

        $orig_location = _PS_MODULE_DIR_ . 'ph_simpleblog/featured/'.$object->id.'.'.$object->featured;

        if(file_exists($orig_location))
            @unlink($orig_location);

        if($only_delete)
            return;

        $object->featured = NULL;
//        $object->update();

        Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminSimpleBlogPosts').'&id_simpleblog_post='.$id.'&updatesimpleblog_post');
    }

    public function ajaxProcessDeletePostImage()
    {
        $this->display = 'content';
        $res = true;
        $this->content['id'] = $_POST['id_image'];

        if(isset($_POST['id_image']) && $_POST['id_image'] != '')
        {
            $filename = $_POST['image_name'];
            $id_post = $_POST['id_post'];

            $toDir = _PS_IMG_DIR_.'SimpleBlog/'.$id_post.'/';

            if (file_exists($toDir . $filename))
                $res &= unlink($toDir . $filename);
        }

        if ($res)
            $this->jsonConfirmation($this->_conf[7]);
        else
            $this->jsonError(Tools::displayError('An error occurred while attempting to delete the product image.'));
    }

    public function deletePostImg($id)
    {
        $object = new SimpleBlogPost($id, Context::getContext()->language->id);

        $tmp_location = _PS_TMP_IMG_DIR_.'ph_simpleblog_postimg_'.$object->id.'.'.$object->postImg;
        if(file_exists($tmp_location))
            @unlink($tmp_location);

        $orig_location = _PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$object->id.'.'.$object->postImg;
        $pathAndNameHor = _PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$object->id.'-post.'.$object->postImg;
        $pathAndNameVert = _PS_MODULE_DIR_ . 'ph_simpleblog/postimg/'.$object->id.'-post_vert.'.$object->postImg;

        if(file_exists($orig_location))
            @unlink($orig_location);

        if(file_exists($pathAndNameHor))
            @unlink($pathAndNameHor);

        if(file_exists($pathAndNameVert))
            @unlink($pathAndNameVert);

        $object->featured = NULL;
        $object->update();

        Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminSimpleBlogPosts').'&id_simpleblog_post='.$id.'&updatesimpleblog_post');
    }

    public function deleteCover($id)
    {
        $object = new SimpleBlogPost($id, Context::getContext()->language->id);

        $tmp_location = _PS_TMP_IMG_DIR_.'ph_simpleblog_'.$object->id.'.'.$object->cover;
        if(file_exists($tmp_location))
            @unlink($tmp_location);

        $orig_location = _PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$object->id.'.'.$object->cover;
        $thumb = _PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$object->id.'-thumb.'.$object->cover;
        $thumbWide = _PS_MODULE_DIR_ . 'ph_simpleblog/covers/'.$object->id.'-wide.'.$object->cover;

        if(file_exists($orig_location))
            @unlink($orig_location);

        if(file_exists($thumb))
            @unlink($thumb);

        if(file_exists($thumbWide))
            @unlink($thumbWide);

        $object->cover = NULL;
//        $object->update();

        Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminSimpleBlogPosts').'&id_simpleblog_post='.$id.'&updatesimpleblog_post');
    }

    public function updateTags($languages, $post)
    {
        $tag_success = true;
        foreach ($languages as $language)
        if ($value = Tools::getValue('tags_'.$language['id_lang']))
            if (!Validate::isTagsList($value))
                $this->errors[] = sprintf(
                    Tools::displayError('The tags list (%s) is invalid.'),
                    $language['name']
                );

        if (!SimpleBlogTag::deleteTagsForPost((int)$post->id))
            $this->errors[] = Tools::displayError('An error occurred while attempting to delete previous tags.');

        foreach ($languages as $language)
            if ($value = Tools::getValue('tags_'.$language['id_lang']))
                $tag_success &= SimpleBlogTag::addTags($language['id_lang'], (int)$post->id, $value);

        if (!$tag_success)
            $this->errors[] = Tools::displayError('An error occurred while adding tags.');
    }

    public function translit($insert)
    {
        $insert = mb_strtolower($insert);    // Если работаем с юникодными строками
        $insert = Tools::strtolower($insert); $replase = array(
        // Буквы
        'Ъ'=>'J',
        'Ы'=>'Y',
        'Ь'=>'Y',
        'Э'=>'E',
        'Ю'=>'Yu',
        'Я'=>'Ya',
        'І'=>'I',
        'Ї'=>'Y',
        'I'=> 'I',
        'а'=>'a',
        'б'=>'b',
        'в'=>'v',
        'г'=>'g',
        'д'=>'d',
        'е'=>'e',
        'ё'=>'yo',
        'ж'=>'zh',
        'з'=>'z',
        'и'=>'i',
        'й'=>'j',
        'к'=>'k',
        'л'=>'l',
        'м'=>'m',
        'н'=>'n',
        'о'=>'o',
        'п'=>'p',
        'р'=>'r',
        'с'=>'s',
        'т'=>'t',
        'у'=>'u',
        'ф'=>'f',
        'х'=>'h',
        'ц'=>'c',
        'ч'=>'ch',
        'ш'=>'sh',
        'щ'=>'shh',
        'ъ'=>'j',
        'ы'=>'y',
        'ь'=>'y',
        'э'=>'e',
        'ю'=>'yu',
        'я'=>'ya',
        'і'=>'i',
        'ї'=>'y',
        'i'=>'i',
    );
        $insert = preg_replace('/  +/', ' ', $insert); // Удаляем лишние пробелы
        $insert = strtr($insert, $replase);
        return $insert;
    }
}
