<div class="panel">
    <div class="panel-heading">
        {l s='Добавление товаров в блок рекомендуемых' mod='ph_simpleblog'}
    </div>
    <div class="panel-body">
        <div class="form-horizontal">

            <div class="form-group">

                <label class="control-label col-lg-3">
                    {l s='Поиск товара' mod='ph_simpleblog'}
                </label>



                <div class="col-lg-5">

                    <input type="text" name="search_str" id="blocknewproducts_search_str" value="">



                    <p class="help-block">
                        {l s='Введите название либо артикул товара. Минимум 3 символов.' mod='ph_simpleblog'}
                        <br/> <span id="blocknewproducts_warning" class="label label-warning" style="display: none;">{l s='Введите больше символов для поиска' mod='ph_simpleblog'}</span>

                    </p>

                </div>



            </div>


        </div>


    </div>
</div>

{literal}
    <script type="text/javascript">

        var search_term = '';
        $('document').ready( function() {
            $("#blocknewproducts_search_str")
                    .autocomplete(
                    '/modules/ph_simpleblog/ajax.php', {
                        minChars: 3,
                        max: 15,
                        width: $("#blocknewproducts_search_str").width(),
                        selectFirst: false,
                        scroll: false,
                        dataType: "json",
                        formatItem: function(data, i, max, value, term) {
                            search_term = term;
                            // adding the little
//                            if ($('.ac_results').find('.separation').length == 0)
//                                $('.ac_results').prepend('<div style="color:#585A69; padding:2px 5px">Use a product from the list<div class="separation"></div></div>');
                            return value;
                        },
                        parse: function(data) {
                            var mytab = new Array();
                            for (var i = 0; i < data.length; i++)
                                mytab[mytab.length] = { data: data[i], value: data[i].name + ' - ' + data[i].reference };
                            return mytab;
                        },
                        extraParams: {
                            ajax: 1,
                            key: 'superasdf123',
                        }
                    }
            )
                    .result(function(event, data, formatted) {
                        // keep the searched term in the input
                        $('#name_1').val(search_term);
                        jConfirm('Добавить этот продукт для отображения в модуле?\n&nbsp;<strong>'+data.name+'</strong>', 'Confirmation', function(confirm){
                            if (confirm == true){
                                document.location.href = window.location.href+'&id_product_post_add='+data.id_product;
                            }
                            else
                                return false;
                        });
                    });
        });

    </script>
{/literal}