{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div id="product-images" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="Images" />
	<div class="panel-heading tab" >
		{l s='Images for post'}
		<span class="badge" id="countImage">{if $countImages > 0}{$countImages} {else}0{/if} </span>
	</div>
	<div class="row">
		<div class="form-group">
			<label class="control-label col-lg-3 file_upload_label">
			<span class="label-tooltip" data-toggle="tooltip"
				  title="{l s='Format:'} JPG, GIF, PNG. {l s='Filesize:'} {$max_image_size|string_format:"%.2f"} {l s='MB max.'}">
				{if isset($id_image)}{l s='Edit this product\'s image:'}{else}{l s='Add a new image to this product'}{/if}
			</span>
			</label>
			<div class="col-lg-9">
				<form id="product_form" class="form-horizontal col-lg-10 col-md-9" action="{$form_action|escape:'html':'UTF-8'}" method="post" enctype="multipart/form-data" name="product" novalidate>

					{$image_uploader}

				</form>
			</div>
		</div>
	</div>

	<div class="table tableDnD postWrapper" id="imageTable" >
		<div id="imageList">
		</div>
	</div>
	<div id="lineType" style="display:none;">
		<div id="image_id" class="blockImgRow">
			<div class="postImg">
				{if isset($force_ssl) && $force_ssl}
					<a href="{$base_dir_ssl}image_path.jpg" class="fancybox">
						<img
								src="/{$base_dir_ssl}-default.jpg"
								alt="legend"
								title="legend"
								class="img_post"
                                style="width: 150px"
                        />
					</a>
                {else}
					<a href="{$base_dir}image_path.jpg" class="fancybox">
						<img
								src="{$base_dir}-default.jpg"
								alt="legend"
								title="legend"
								class="img_post"
                                style="width: 150px"
                        />
					</a>
                {/if}

			</div>
			<div id="imgUrl" class="postImgUrl">/img_url</div>
			<div class="postDel">
				<a href="#" class="delete_product_image pull-right btn btn-default" >
					<i class="icon-trash"></i> {l s='Delete this image'}
				</a>
			</div>
		</div>
	</div>

	<script type="text/javascript">
        var upbutton = '{l s='Upload an image'}';
        var come_from = '{$table}';
        var success_add =  '{l s='The image has been successfully added.'}';
        var id_tmp = 0;
        var id_post = '{$id_post}';

        {literal}
        //Ready Function
        function imageLine(id, path, img_url)
        {
            line = $("#lineType").html();
            line = line.replace(/image_id/g, id);
            line = line.replace(/(\/)?[a-z]{0,2}-default/g, function($0, $1){
                return $1 ? $1 + path : $0;
            });
            line = line.replace(/image_path/g, path);
            line = line.replace(/\.jpg"\s/g, '?time=' + new Date().getTime() + '" ');
            line = line.replace(/img_url/g, img_url);
            $("#imageList").append(line);
        }

        $(document).ready(function(){
            {/literal}

            {foreach from=$images item=image}

            assoc = {literal}"{"{/literal};

            if (assoc != {literal}"{"{/literal})
            {
                assoc = assoc.slice(0, -1);
                assoc += {literal}"}"{/literal};
                assoc = jQuery.parseJSON(assoc);
            }
            else
                assoc = false;

            imageLine({$image.id}, "{$image.url}", "{$image.url}");
            {/foreach}
            {literal}

            /**
             * on success function
             */
            function afterDeletePostImage(data)
            {
                data = $.parseJSON(data);

                if (data)
                {
                    id = data.content.id;

                    if (data.status == 'ok')
                        $("#" + id).remove();

                    $("#imageTable").eq(1).find(".covered").addClass('icon-check-sign');
                    $("#countImage").html(parseInt($("#countImage").html()) - 1);

                    if (parseInt($("#countImage").html()) <= 1)
                        $('#caption_selection').addClass('hidden');
                }
            }

            $('.delete_product_image').die().live('click', function(e)
            {
                e.preventDefault();
                id = $(this).parent().parent().attr('id');
                var imgElement_src = $(this).parent().parent().find('#imgUrl').text().split('/');
                if (confirm("{/literal}{l s='Are you sure?' js=1}{literal}"))
                    doAdminAjax({
                        "action":"deletePostImage",
                        "id_image":id,
                        "id_post":id_post,
                        "image_name":imgElement_src[4],
                        "token" : "{/literal}{$token|escape:'html':'UTF-8'}{literal}",
                        "tab" : "AdminSimpleBlogPosts",
                        "ajax" : 1 }, afterDeletePostImage
                    );
            });

            $('.fancybox').fancybox();
        });
        {/literal}
	</script>
</div>