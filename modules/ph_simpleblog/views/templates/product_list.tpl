<div class="panel">
    <div class="panel-heading">
        {l s='Список товаров в блоке рекомендуемых' mod='ph_simpleblog'}
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>{l s='Название' mod='ph_simpleblog'}</th>
                <th>{l s='Артикул' mod='ph_simpleblog'}</th>
                <th>{l s='Статус' mod='ph_simpleblog'}</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            {foreach from=$products item=product}
                <tr>
                    <td>{$product.name}</td>
                    <td>{$product.reference}</td>
                    <td>
                        {if $product.product_status}
                            <a class="list-action-enable action-enabled" href="{$module_link}&id_product_post_status={$product.id_product}" title="{l s='Включено' mod='ph_simpleblog'}">
                                <i class="icon-check"></i>
                                <i class="icon-remove hidden"></i>
                            </a>
                        {else}
                            <a class="list-action-enable action-disabled" href="{$module_link}&id_product_post_status={$product.id_product}" title="{l s='Отключено' mod='ph_simpleblog'}">
                                <i class="icon-check hidden"></i>
                                <i class="icon-remove"></i>
                            </a>
                        {/if}
                    </td>
                    <td>
                        <a href="{$module_link}&id_product_post_delete={$product.id_product}" class="btn btn-danger confirm" data-confirm="{l s='Вы уверены, что хотите удалить товар из блока?' mod='ph_simpleblog'}" role="button">{l s='Удалить' mod='ph_simpleblog'}</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>

{literal}
    <script type="text/javascript">
        $(document).ready(function(){
            $(".confirm").bind('click', function(e){
                e.preventDefault();

                jConfirm( e.srcElement.getAttribute('data-confirm'), 'Confirmation', function(confirm){
                    if (confirm == true){
                        document.location.href = e.srcElement.getAttribute('href');
                    }
                    else
                        return false;
                });
//
//                if (confirm(e.srcElement.getAttribute('data-confirm'))) {
//                    location.href = e.srcElement.getAttribute('href');
//                }
            })
        });

    </script>
{/literal}