<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 24.01.17
 * Time: 16:13
 */
if ($_GET['secure_key'] !== '3hh8ge89hg08fy$dgviuh$dfog0') {
    die();
}
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

$module_name = 'ph_simpleblog';
if (!Module::isInstalled($module_name)) {
    die('Module is not installed');
}

$module = new Module('ph_simpleblog');
$module->registerHook('displayHomePageList');