<?php

if (!defined('_PS_VERSION_'))
    exit;

class FfPokupon extends Module
{
    private $output;

    public function __construct()
    {
        $this->name = 'ffpokupon';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->secure_key = Tools::encrypt($this->name);
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Remarketing Pokupon');
        $this->description = $this->l('Add pixel pokupon for site pages');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('displayHeader') ||
            !$this->registerHook('displayPokupon')
        )
            return false;
        $this->cleanClassCache();
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache))
            unlink($class_cache);
    }

    public function hookDisplayHeader($params)
    {
        return $this->display(__FILE__, 'ffpokupon.tpl');
    }

    public function hookDisplayPokupon($params)
    {
        $page_name = Tools::getValue('controller');
        $context = Context::getContext();
        $cartID = (int)$context->cart->id;
        $orderID = (int)Order::getOrderByCartId($cartID);
        $OrderAmount = sprintf('%01.2f',(float)$context->cart->getOrderTotal(true, Cart::BOTH));

        switch ($page_name) {
            case 'orderopc':
                if (!empty($_COOKIE['pdid'])){
                    $this->output = sprintf( '<img src="https://pokupon.ua/pixel/v2/%d/new.png?uid=%d'.'&ord_id=%d&amount=%d" />',
                        $_COOKIE['pdid'],
                        !empty($_COOKIE['puid']) ? $_COOKIE['puid'] : 0,
                        $cartID,
                        $OrderAmount
                    );
                }
                break;

            case 'confirm':
                if ( !empty($_COOKIE['pdid']) ) {
                    $this->output = sprintf( '<img src="https://pokupon.ua/pixel/v2/%d/complete.png?uid=%d'.'&ord_id=%d&amount=%d" />',
                        $_COOKIE['pdid'],
                        !empty($_COOKIE['puid']) ? $_COOKIE['puid'] : 0,
                        $orderID,
                        $OrderAmount
                    );
                }
                break;
        }

        return $this->output;
    }
}