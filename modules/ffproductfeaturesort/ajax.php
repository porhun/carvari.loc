<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 27.02.17
 * Time: 10:40
 */
require_once(__DIR__.'/../../config/config.inc.php');
require_once(__DIR__.'/../../init.php');

$moduleName = 'ffproductfeaturesort';

$res = Hook::exec('actionLoadFeatureValues', ['id_feature' => Tools::getValue('id_feature', null), 'ajax' => Tools::getValue('ajax', false)], Module::getModuleIdByName($moduleName), true);

die(Tools::jsonEncode($res[$moduleName]));