<?php
if (!defined('_PS_VERSION_'))
    exit;

require_once __DIR__.'/models/ProductFeatureSort.php';

/**
 * Class ffproductfeaturesort
 *
 * This module is used to sort products by some features first of all.
 * If product has this feature, it will be moved at the beginning of result product set. Otherwise, default sorting will be applied to it.
 * Logic should be injected in the sql query that grabs the category products
 *
 * @TODO Sorting by categories is off at the moment and doesn't work!
 * @TODO If it is required then additional queries (hookActionAddSortQuery) MUST be configured for such logic. Search other TODO to enable categories
 */
class ffproductfeaturesort extends Module
{
    private   $secure_key    = 'f$b$39b23s7$e7a92vh42V$D29215f4xc1$9Db';
    protected $_extra_html   = '';
    const INSTALL_SQL_FILE   = 'sql/install.php';
    const UNINSTALL_SQL_FILE = 'sql/uninstall.php';

    public function __construct()
    {
        $this->name = 'ffproductfeaturesort';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'shandur';
        $this->need_instance = 0;

        parent::__construct();
        $this->bootstrap = true;

        $this->displayName = $this->l('Sort products in category');
        $this->description = $this->l('Sorting of products by specified features to display on frontend');
        $this->confirmUninstall       = $this->l('Are you sure you want to delete this module ?');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install($delete_params = true)
    {
        if (!parent::install() || !$this->registerHook('moduleRoutes')
            || !$this->registerHook('actionLoadFeatureValues')
            || !$this->registerHook('actionAddSortQuery')
        )
            return false;
        if ($delete_params && !$this->createTables()) {
            return false;
        }
        return true;
    }

    public function uninstall($delete_params = true)
    {
        if (!parent::uninstall())
            return false;
        if ($delete_params && !$this->deleteTables()) {
            return false;
        }
        return true;
    }

    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }
        if (!$this->install(false)) {
            return false;
        }
        return true;
    }

    protected function createTables()
    {
        Configuration::updateValue(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE_VALUE, '');
        Configuration::updateValue(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE, '');
        $sql = $this->getTableSql(self::INSTALL_SQL_FILE);
        return $this->performSql($sql);
    }

    protected function deleteTables()
    {
        Configuration::deleteByName(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE_VALUE);
        Configuration::deleteByName(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE);
        $sql = $this->getTableSql(self::UNINSTALL_SQL_FILE);
        return $this->performSql($sql);
    }

    protected function getTableSql($sqlFile)
    {
        $sql = require (dirname(__FILE__).'/'.$sqlFile);
        if (!$sql) {
            return [];
        }
        return $sql;
    }

    protected function performSql(array $sqlQueries)
    {
        $dbInstance = Db::getInstance();
        foreach ($sqlQueries as $query) {
            if (!$dbInstance->execute($query)) {
                return false;
            }
        }
        return true;
    }

    public function getContent()
    {
        if (Tools::isSubmit('submit'.$this->name))
        {
            $this->submitCategories();
        }

        if (Tools::isSubmit('delete'.ProductFeatureSort::$definition['table']) && ($idCategoryShare = (int)Tools::getValue(ProductFeatureSort::$definition['primary']))) {
            $deleteResult = (new ProductFeatureSort($idCategoryShare))->delete();
            $this->_extra_html = ($deleteResult) ? $this->displayConfirmation($this->l('Item has been deleted')) : $this->displayError($this->l('Item has not been deleted'));
        }

        return $this->_extra_html
            /** @TODO At the moment, we don't need sorting by features for particular category. Just one sorting for all the categories. To use it, uncomment code below AND comment other renders */
            . $this->renderFormWithoutCategories()

            /** Uncomment */
//            . $this->renderForm()
//            . $this->renderList()
            /** Uncomment */
            ;
    }

    protected function submitCategories()
    {
        if (!count($submittedValues = $this->getSubmittedValues())) {
            $this->_extra_html .= $this->displayError($this->l('All fields are required'));
            return;
        }
        $categoryShare = $this->saveFeatureInfo($submittedValues['id_category'], $submittedValues['id_feature'], $submittedValues['id_feature_value']);
        $this->_extra_html = ($categoryShare) ? $this->displayConfirmation($this->l('Item have been added')) : $this->displayError($this->l('Item have not been added'));
    }

    protected function saveFeatureInfo($idCategory, $idFeature, $idFeatureValue)
    {
        /** @TODO At the moment, we don't need sorting by features for particular category. Just one sorting for all the categories. To use it, uncomment code below */

        /** Uncomment */
//        $categoryShare = ProductFeatureSort::create((int)$idCategory, (int)$idFeatureValue, (int)$idFeature);

        /**  * * * * * * * * *  */
        $categoryShare =  Configuration::updateValue(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE_VALUE, (int)$idFeatureValue);
        $categoryShare &= Configuration::updateValue(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE, (int)$idFeature);
        /**  * * * * * * * * *  */

        return $categoryShare;
    }

    /** @TODO At the moment, we don't need sorting by features for particular category. Just one sorting for all the categories. To use it, uncomment code below */
    protected function getSubmittedValues()
    {
        /** Uncomment */
        // if (!($idCategory = (int)Tools::getValue('id_category', 0)) || !($idFeature = (int)Tools::getValue('id_feature', 0)) || !($idFeatureValue = Tools::getValue('id_feature_value', 0))) {
//            return [];
//        }
        /** Uncomment */
        if (!($idFeatureValue = Tools::getValue('id_feature_value', 0)) || !($idFeature = (int)Tools::getValue('id_feature', 0)) || !($idCategory = 1)) {
            return [];
        }

        return [
            'id_category' => $idCategory,
            'id_feature'  => $idFeature,
            'id_feature_value' => $idFeatureValue
        ];
    }

    public function renderForm()
    {
        $this->addMedia();

        $features = FeatureCore::getFeatures($this->context->language->id);
        $fieldsForm = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type'  => 'select',
                        'name'  => 'id_feature',
                        'class' => 'js_feature_block',
                        'label' => $this->l('Feature'),
                        'options' => [
                            'query' => $features,
                            'id'    => 'id_feature',
                            'name'  => 'name'
                        ]
                    ],
                    [
                        'type'  => 'select',
                        'name'  => 'id_feature_value',
                        'class' => 'js_feature_value_block',
                        'label' => $this->l('Feature value'),
                        'desc'  => $this->l('This value will be used for product sorting on "all category\'s products" page'),
                        'options' => [
                            'query' => [
                                ['id' => 0, 'name' => $this->l('Choose the feature...')]
                            ],
                            'id' => 'id',
                            'name' => 'name'
                        ],
                    ],
                    [
                        'type'  => 'free',
                        'name'  => 'id_category',
                        'label' => $this->l('Categories')
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                ]
            ],
        ];

        $treeCategory = new HelperTreeCategories($this->name.'_categories');

        $treeCategory->setInputName('id_category');

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table        = $this->table;
        $lang                 = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->module                   = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'submit'.$this->name;
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => [
                'id_category' => $treeCategory->render(),
            ],
        );

        return $helper->generateForm([$fieldsForm]);
    }

    public function renderFormWithoutCategories()
    {
        $this->addMedia();
        $features = Feature::getFeatures($this->context->language->id);
        $savedFeature  = (int)ConfigurationCore::get(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE, 0);
        $featureValues = ($savedFeature) ? FeatureValue::getFeatureValuesWithLang($this->context->language->id, $savedFeature): [];
        $fieldsForm = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type'  => 'select',
                        'name'  => 'id_feature',
                        'class' => 'js_feature_block',
                        'label' => $this->l('Feature'),
                        'options' => [
                            'query' => $features,
                            'id'    => 'id_feature',
                            'name'  => 'name'
                        ]
                    ],
                    [
                        'type'  => 'select',
                        'name'  => 'id_feature_value',
                        'class' => 'js_feature_value_block',
                        'label' => $this->l('Feature value'),
                        'desc'  => $this->l('This value will be used for product sorting on "all category\'s products" page'),
                        'options' => [
                            'query' => $featureValues,
                            'id' => 'id_feature_value',
                            'name' => 'value'
                        ],
                    ],
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                ]
            ],
        ];

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table        = $this->table;
        $lang                 = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language    = $lang->id;
        $helper->module                   = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier               = $this->identifier;
        $helper->submit_action            = 'submit'.$this->name;
        $helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token                    = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => [
                'id_feature'       => ConfigurationCore::get(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE, ''),
                'id_feature_value' => ConfigurationCore::get(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE_VALUE, ''),
            ],
        );

        return $helper->generateForm([$fieldsForm]);
    }

    public function renderList()
    {
        $fields_list = [
            ProductFeatureSort::$definition['primary'] => [
                'title' => $this->l('ID'),
                'align' => 'left',
                'width' => 'auto'
            ],
            'id_category' => [
                'title'   => $this->l('Product category'),
                'width'   => 150,
                'orderby' => false,
            ],
            'id_feature'  => [
                'title'   => $this->l('Feature'),
                'width'   => 150,
                'orderby' => false,
            ],
            'id_feature_value' => [
                'title'   => $this->l('Feature value'),
                'width'   => 150,
                'orderby' => false,
            ],
        ];

        $helper = new HelperListCore();
        $helper->simple_header = true;
        $helper->identifier    = ProductFeatureSort::$definition['primary'];
        $helper->table         = ProductFeatureSort::$definition['table'];
        $helper->actions       = ['delete'];
        $helper->show_toolbar  = false;
        $helper->module        = $this;
        $helper->title         = $this->l('Share category list');
        $helper->token         = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex  = AdminController::$currentIndex.'&configure='.$this->name;
        $categories            = (new ProductFeatureSort)->getAll();

        return $helper->generateList(array_merge([], $categories), $fields_list);
    }

    protected function addMedia()
    {
        $this->context->controller->addJS($this->getLocalPath().'js/'.$this->name.'.js');
    }

    /** @TODO At the moment, we don't need sorting by features for particular category. Just one sorting for all the categories. To use it, uncomment code below AND comment other */
    protected function getSortItems()
    {
        return [
            ['id_feature_value' => Configuration::get(ProductFeatureSort::FFPRODUCTFEATURESORT_FEATURE_VALUE)]
        ];
//        return (new ProductFeatureSort)->getAll();
    }

    public function hookActionLoadFeatureValues($params)
    {
        if (!isset($params['ajax']) || !(bool)$params['ajax'] || !isset($params['id_feature'])) {
            return ['status' => 'error'];
        }
        $idFeature = (int)$params['id_feature'];
        $featureValues = FeatureValue::getFeatureValuesWithLang($this->context->language->id, $idFeature);
        return ['status' => 'ok', 'feature_values' => $featureValues];
    }

    public function hookActionAddSortQuery($params)
    {
        $res = ['sort_left_join' => '', 'sort_order_by' => ''];
        $sortItems = $this->getSortItems();
        if ($sortItems && ($featureValuesIds = array_map('intval', array_column($sortItems, 'id_feature_value')))) {
            $res['sort_left_join'] = ' LEFT JOIN `'._DB_PREFIX_.'feature_product` AS fp ON fp.id_product = cp.id_product AND fp.id_feature_value IN ('.implode(',', $featureValuesIds).')';
            $res['sort_order_by']  = ' FIELD(fp.id_feature_value, '.implode(',', $featureValuesIds).') DESC, ';
        }

        return $res;
    }

    public function hookModuleRoutes()
    {
        return [
//            'module-'.$this->name.'-category_share' => [
//                'controller' => 'category_share',
//                'rule' => '/module/'.$this->name.'/category_share/',
//                'keywords' => [],
//                'params' => [
//                    'fc' => 'module',
//                    'module' => $this->name,
//                ],
//            ]
        ];
    }
}
