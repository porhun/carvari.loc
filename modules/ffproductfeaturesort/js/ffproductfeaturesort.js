/**
 * Created by shandur on 27.02.17.
 */

$(document).ready(function () {
    initFeatureValueUpdating();
});

function initFeatureValueUpdating() {
    var $featureSelectBlock = $('.js_feature_block');
    if ($featureSelectBlock && $featureSelectBlock.length) {
        $featureSelectBlock.on('change', function (e) {
            var featureId = e.target.value;
            $.ajax({
                url: '/modules/ffproductfeaturesort/ajax.php' + '?ajax=1&rand=' + new Date().getTime(),
                dataType: 'JSON',
                type: 'POST',
                data: {id_feature: featureId},
                success: function (rsp) {
                    if (rsp.status != 'ok' || !rsp.feature_values) {
                        alert('Oops! There is no any values for current feature.');
                        return;
                    }
                    updateFeatureValues(rsp.feature_values);
                },
                error: function () {
                    alert('Oops! Something goes wrong.');
                }
            });
        });
    }
}

function updateFeatureValues(featureValues) {
    var $featureValueBlock = $('.js_feature_value_block');
    if (!$featureValueBlock || !$featureValueBlock.length || !featureValues || !featureValues.length) {
        return;
    }
    $featureValueBlock.empty();
    featureValues.forEach(function (featureValue, index) {
        var selectOption = document.createElement('option');
        selectOption.value     = featureValue.id_feature_value;
        selectOption.innerHTML = featureValue.value;
        $featureValueBlock.append(selectOption);
    });
}
