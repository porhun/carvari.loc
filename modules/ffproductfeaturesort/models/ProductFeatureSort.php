<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 23.02.17
 * Time: 11:37
 */
require_once __DIR__.'/ProductFeatureSortInterface.php';

class ProductFeatureSort extends ObjectModel implements ProductFeatureSortInterface
{
    const FFPRODUCTFEATURESORT_FEATURE        = 'FFPRODUCTFEATURESORT_FEATURE';
    const FFPRODUCTFEATURESORT_FEATURE_VALUE  = 'FFPRODUCTFEATURESORT_FEATURE_VALUE';

    public $id_product_feature_sort;
    public $id_category;
    public $id_feature;
    public $id_feature_value;

    public static $definition = [
        'table'   => 'product_feature_sort',
        'primary' => 'id_product_feature_sort',
        'fields'  => [
            'id_category'        => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_feature'         => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true],
            'id_feature_value'   => ['type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true]
        ]
    ];

    public static function create($idCategory, $idFeature, $idFeatureValue)
    {
        $object = new static;
        $object->id_category      = (int)$idCategory;
        $object->id_feature       = (int)$idFeature;
        $object->id_feature_value = (int)$idFeatureValue;
        return $object->save();
    }

    public function getAll()
    {
        $qb = new DbQuery();
        $qb->select('*')->from(self::$definition['table']);
        $res = Db::getInstance()->executeS($qb);
        return $res ?: [];
    }

}