<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 23.02.17
 * Time: 12:43
 */
interface ProductFeatureSortInterface
{
    /**
     * @return array
     */
    public function getAll();
}