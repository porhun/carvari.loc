<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 23.02.17
 * Time: 11:20
 */
$sql = [];

$sql[] = 'CREATE TABLE IF NOT EXISTS '.bqSQL(_DB_PREFIX_).'product_feature_sort(
    `id_product_feature_sort` int(11) unsigned not null auto_increment,
    `id_category`             int(11) unsigned not null,
    `id_feature`              int(11) unsigned not null,
    `id_feature_value`        int(11) unsigned not null,
    UNIQUE KEY (`id_category`, `id_feature`, `id_feature_value`),
    PRIMARY KEY(`id_product_feature_sort`)
) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

return $sql;