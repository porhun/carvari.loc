<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 23.02.17
 * Time: 12:40
 */
require_once _PS_MODULE_DIR_.'ffcategoryshare/models/CategoryShareInterface.php';
require_once __DIR__.'/CategoryShareableInterface.php';

class CategoryShareService implements CategoryShareableInterface
{
    protected $categoryShareModel = null;

    private function __construct(CategoryShareInterface $categoryShare)
    {
        $this->categoryShareModel = $categoryShare;
    }

    public static function createService(CategoryShareInterface $categoryShare)
    {
        return new self($categoryShare);
    }

    public function shareCategories()
    {
        $categories = $this->categoryShareModel->getAllCategories();
        foreach ($categories as $key => $categoriesShareInfo) {
            $categoryFromObject = new Category((int)$categoriesShareInfo['id_category_from']);
            $idCategoryTo = $categoriesShareInfo['id_category_to'];
            if (!$idCategoryTo || !$categoryFromObject) continue;
            $this->addCategoriesToProducts($categoryFromObject, $idCategoryTo);
        }
    }

    /**
     * Retrieve all $categoryFrom children and their products' IDs
     *
     * @param CategoryCore $categoryFrom        Category from where products wil be taken
     * @param int $categoryTo                   Category ID that the products will have
     */
    protected function addCategoriesToProducts(CategoryCore $categoryFrom, $categoryTo) {
        $categoryFromChildren = $categoryFrom->getAllChildren()->getAll();

        if (!$categoryFromChildren->count()) {
            $categoryFromChildren->offsetSet(null, $categoryFrom);
        }
        /** @var Category $childCategory */
        foreach ($categoryFromChildren as $key => $childCategory) {
            $productsIds = array_column($childCategory->getProductsWs(), 'id');
            $this->addCategoryToProducts($productsIds, $categoryTo);
        }
    }

    /**
     * Add category to products
     *
     * @param array $productsIds    Products' IDs
     * @param int $categoryTo       Category ID that each product will have
     */
    protected function addCategoryToProducts(array $productsIds, $categoryTo)
    {
        if (!count($productsIds)) {
            return;
        }
        foreach ($productsIds as $productId) {
            $product = new Product($productId);
            $product->addToCategories([(int)$categoryTo]);
            unset($product);
        }
    }

}