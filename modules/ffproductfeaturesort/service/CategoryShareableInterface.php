<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 23.02.17
 * Time: 12:48
 */
interface CategoryShareableInterface
{
    /**
     * @return void
     */
    public function shareCategories();
}