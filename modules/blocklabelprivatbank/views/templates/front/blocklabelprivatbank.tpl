<div class="product_label__pb">
	<a href="{if $link}{$link}{else}#{/if}">
		<div class="product_label__pb--img">
			{if $label_privatbank}
				<img src="{$module_dir}img/{$label_privatbank}"/>
			{/if}
		</div>
		<div class="product_label__pb--name" onclick="myFunction()">{l s='Рассрочка 10 платежей' mod='blocklabelprivatbank'}<div class="popupimg" id="myPopup"><img class="pop" src="{$module_dir}img/popup.png"/></div></div>
	</a>
</div>
<script>
// When the user clicks on div, open the popup
function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}
</script>