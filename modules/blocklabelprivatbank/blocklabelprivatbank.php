<?php

if (!defined('_PS_VERSION_'))
    exit;

class BlockLabelPrivatBank extends Module
{
	public function __construct()
	{
		$this->name = 'blocklabelprivatbank';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

		$this->displayName = $this->l('Block label PrivatBank');
		$this->description = $this->l('Add display the label "Credit" on the product\'s page');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
	}

    public function install()
    {
        if(!parent::install() ||
            !$this->registerHook('header') ||
            !$this->registerHook('displayLabelBlock')
        )
            return false;

        $this->cleanClassCache();
        if (file_exists(dirname(__FILE__).'/img/privat24.png'))
            Configuration::updateValue('BLOCKIMG','privat24.png');

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;

        Configuration::deleteByName('BLOCKIMG');
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache))
            unlink($class_cache);
    }

    public function postProcess()
    {
        if (Tools::isSubmit('submitOptions'))
        {
            if (!($languages = Language::getLanguages(true)))
                return false;

            $text = array();
            foreach ($languages as $lang)
                $text[$lang['id_lang']] = Tools::getValue('BLOCKURI_'.$lang['id_lang']);

            Configuration::updateValue('BLOCKURI', $text);

            if (isset($_FILES['BLOCKIMG']) && isset($_FILES['BLOCKIMG']['tmp_name']) && !empty($_FILES['BLOCKIMG']['tmp_name']))
            {
                if ($error = ImageManager::validateUpload($_FILES['BLOCKIMG'], 4000000))
                    return $this->displayError($this->l('Invalid image.'));
                else
                {
                    $ext = substr($_FILES['BLOCKIMG']['name'], strrpos($_FILES['BLOCKIMG']['name'], '.') + 1);
//                    $file_name = md5($_FILES['BLOCKIMG']['name']).'.'.$ext;
                    $file_name = 'privat24.'.$ext;
                    if (!move_uploaded_file($_FILES['BLOCKIMG']['tmp_name'], dirname(__FILE__).'/img/'.$file_name))
                        return $this->displayError($this->l('An error occurred while attempting to upload the file.'));
                    else
                    {
                        if (Configuration::hasContext('BLOCKIMG', null, Shop::getContext()) && Configuration::get('BLOCKIMG') != $file_name)
                            @unlink(dirname(__FILE__).'/'.Configuration::get('BLOCKIMG'));
                        Configuration::updateValue('BLOCKIMG', $file_name);
                        $this->_clearCache('blocklabelprivatbank.tpl');
                        return $this->displayConfirmation($this->l('The settings have been updated.'));
                    }
                }
            }
            $this->_clearCache('blocklabelprivatbank.tpl');
        }
        return '';
    }

    public function getContent()
    {
        return $this->postProcess().$this->renderForm();
    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'file',
                        'label' => $this->l('Add a picture from your PC'),
                        'name' => 'BLOCKIMG',
                        'thumb' => '../modules/'.$this->name.'/img/'.Configuration::get('BLOCKIMG'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Url for label block'),
                        'name' => 'BLOCKURI',
                        'lang' => true,
                        'col'  => 6,
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = true;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitOptions';
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        if (!($languages = Language::getLanguages(true)))
            return false;

        $data = array(
            'BLOCKIMG' => Tools::getValue('BLOCKIMG', Configuration::get('BLOCKIMG')),
        );

        foreach ($languages as $lang)
            $data['BLOCKURI'][$lang['id_lang']] = Configuration::get('BLOCKURI', $lang['id_lang']);

        return $data;
    }

    public function hookHeader($params)
    {
        $this->context->controller->addCSS(($this->_path) . 'views/css/blocklabelprivatbank.css', 'all');
    }

	public function hookDisplayLabelBlock($params)
	{
		if (!$this->isCached('blocklabelprivatbank.tpl', $this->getCacheId()))
		{
            $id_lang = $this->context->cart->id_lang;
            $this->smarty->assign(array(
                'label_privatbank' => Configuration::get('BLOCKIMG'),
                'link' => Configuration::get('BLOCKURI', $id_lang),
            ));
		}
		return $this->display(__FILE__, 'blocklabelprivatbank.tpl', $this->getCacheId());
	}

}
