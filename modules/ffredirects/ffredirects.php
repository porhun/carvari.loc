<?php
if (!defined('_PS_VERSION_'))
    exit;

require_once __DIR__.'/models/FfRedirect.php';

class Ffredirects extends Module
{
    protected $extra_html = '';

    public function __construct()
    {
        $this->name = 'ffredirects';
        $this->tab = 'social_networks';
        $this->version = '1.0.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('FF redirects - верхнее меню магазина');
        $this->description = $this->l('FF redirects - верхнее меню магазина.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    }

    public function install($keep = false)
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);


        if (!parent::install() || !$this->registerHook('actionControllerInited') ||
            !( $keep ? true : $this->installSql()) )
            return false;
        return true;
    }

    public function uninstall($keep = false)
    {
        if (!parent::uninstall() ||
            !( $keep ? true : $this->uninstallSql())
        )
            return false;
        return true;
    }

    public function reset()
    {
        if (!$this->uninstall(true))
            return false;
        if (!$this->install(true))
            return false;
        return true;
    }

    protected function installSql()
    {
        return Db::getInstance()->execute("CREATE TABLE `" . _DB_PREFIX_ . "ffredirects`(
            `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
            `from` varchar(400),
            `to` varchar(400),
            `status` tinyint(1),
            PRIMARY KEY(`id`),
            UNIQUE KEY `uc_from_key` (`from`(255))
            ) ENGINE=" . _MYSQL_ENGINE_ . " default CHARSET=utf8");
    }

    protected function uninstallSql()
    {
        return Db::getInstance()->execute("DROP TABLE `" . _DB_PREFIX_ . "ffredirects`");
    }



    public function getContent()
    {


        $this->postProcess();

        if (Tools::getIsset('updateffredirects') || Tools::getIsset('addredirect'))
            return $this->displayForm();


        return $this->generateList();
    }

    protected function displayForm()
    {
        $menu_item = new FfRedirect((int)Tools::getValue('id'));

        $fields_form = array('');
        // Get default Language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Edit menu item'),
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'redirect[id]',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Redirect From'),
                    'name' => 'redirect[from]',
                    'required' => false,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Redirect To'),
                    'name' => 'redirect[to]',
                    'required' => false,
                    'desc' => ''
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable'),
                    'name' => 'redirect[status]',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
            ),

            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'import'
            ),

        );

        $helper = new HelperForm();

        // // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

        // Language

        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int)($language['id_lang'] == $default_lang);
        }
        $helper->languages = $languages;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title = $this->l('Edit Menu item');
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->show_cancel_button = true;

        $helper->submit_action = 'submitMenu';
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        $helper->fields_value['redirect[id]'] = $menu_item->id;
        $helper->fields_value['redirect[from]'] = $menu_item->from;
        $helper->fields_value['redirect[to]'] = $menu_item->to;
        $helper->fields_value['redirect[status]'] = $menu_item->status;

        return strval($this->extra_html).$helper->generateForm($fields_form);

    }

    private function generateList()
    {
        $fields_list = array(
            'id' => array(
                'title' => $this->l('Id'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
            'from' => array(
                'title' => $this->l('Redirect From'),
                'type' => 'text',
                'search' => false,
            ),
            'to' => array(
                'title' => $this->l('Redirect To'),
                'type' => 'text',
                'search' => false,
            ),
            'status' => array(
                'title' => $this->l('Status'),
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-sm',
                'orderby' => false,
                'search' => false,
                'ajax' => true,

            ),
        );



        $total_count = Db::getInstance()->getValue('SELECT count(*) FROM `'._DB_PREFIX_.'ffredirects`');
        $fields_values = FfRedirect::loadList();

        $helper = new HelperList();

        $helper->shopLinkType = '';

        $helper->simple_header = false;

        // Actions to be displayed in the "Actions" column
        $helper->actions = array('edit', 'delete');

        $helper->identifier = 'id';
        $helper->show_toolbar = true;
//        $helper->bulk_actions = array(
//            'delete' => array(
//                'text' => $this->l('Delete selected'),
//                'icon' => 'icon-trash',
//                'confirm' => $this->l('Delete selected items?')
//            )
//        );
//        $helper->force_show_bulk_actions = true;

        $helper->toolbar_btn['new'] = array(
            'href' => $this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
                $this->name . '&addredirect&token=' . Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add new'),
        );

        $helper->listTotal = $total_count;
        $helper->title = $this->l('Top menu items');
        $helper->table = 'ffredirects';

        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return strval($this->extra_html).$helper->generateList( $fields_values , $fields_list);
    }



    protected function postProcess()
    {
        $redirect = 0;
        if (Tools::getIsset('deleteffredirects') && ($id = (int)Tools::getValue('id', 0))) {
            $redirect_item = new FfRedirect($id);
            if ($redirect_item) {
                $this->extra_html = (!$redirect_item->delete()) ? $this->displayError($this->l('Item was not deleted')) : $this->displayConfirmation($this->l('Item was deleted'));
            }
        }
        if (Tools::isSubmit('submitMenu')){
            $redirect = Tools::getValue('redirect');
            $redirect_item = ((int)$redirect['id']) ? new FfRedirect((int)$redirect['id']) : new FfRedirect();
            $redirect_item->from = pSQL($this->formatRedirectValue($redirect['from']));
            $redirect_item->to = pSQL($this->formatRedirectValue($redirect['to']));
            $redirect_item->status = (int)$redirect['status'];
            if (!$redirect_item->save()) {
                $this->extra_html = $this->displayError($this->l('Item was not saved'));
                $_GET['addredirect'] = true;
                return;
            } else {
                $redirect = 1;
            }
        }

        if (Tools::getValue('action') == 'statusffredirects') {
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffredirects` SET `status` = abs(`status` - 1) WHERE `id`='.(int)Tools::getValue('id'));
            die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully'))));
        }

        if($redirect)
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
                $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }

    protected function isUniqueUrl($url, array $redirect_urls, $strict = false)
    {
        return in_array($url, $redirect_urls, $strict);
    }

    protected function formatRedirectValue($redirect_url)
    {
        return sprintf('/%s', trim($redirect_url, ' \/'));
    }


    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;

        return $cache_id;
    }

    public function hookActionControllerInited($params)
    {
        /**
         * @var FrontController $controller
         */
        $controller = $params['controller'];
        if (!($controller instanceof FrontController) || !($save_redirect_urls = FfRedirect::loadList(FfRedirect::ACTIVE))) {
            return;
        }
        $request_uri = sprintf('/%s', trim($_SERVER['REQUEST_URI'], ' \/'));
        $redirect_from = array_column($save_redirect_urls, 'to', 'from');
        if (isset($redirect_from[$request_uri])) {
            Tools::redirect(Tools::getShopDomain(true).$redirect_from[$request_uri], __PS_BASE_URI__, $this->context->link, ['HTTP/1.1 301 Moved Permanently']);
        }
    }

}