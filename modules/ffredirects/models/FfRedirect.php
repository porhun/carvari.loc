<?php

class FfRedirect extends ObjectModel {
    const INACTIVE = 0;
    const ACTIVE = 1;

    public $id;
    public $from;
    public $to;
    public $status;

    public static $definition = array(
        'table'                         => 'ffredirects',
        'primary'                       => 'id',
        'fields'                        => array(
            'id'                        => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'from'                       => array('type' => self::TYPE_STRING),
            'to'                       => array('type' => self::TYPE_STRING),
            'status'                    => array('type' => self::TYPE_BOOL),
        ),
    );

    /**
     * Return list of saved redirect urls
     *
     * @param null|bool $status     Status of redirect urls
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    public static function loadList($status = null)
    {
        $qb = new DbQuery();
        $qb->select('*')->from(self::$definition['table']);
        if (!is_null($status)) {
            $qb->where('status = '.(bool)$status);
        }
        return Db::getInstance()->executeS($qb);
    }
}