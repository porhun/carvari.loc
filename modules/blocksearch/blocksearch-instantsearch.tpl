{if $ajaxsearch}
<script type="text/javascript">
// <![CDATA[
$('document').ready(function() {

	var config = {
        "minLength" : 3,
        "autoFocus": false,
        "method" : "GET",
        "dataType": "JSON"
    };
    var smallPointTemplate = function( ul, item ) {
        return $("<li></li>") //создаем новый элемент
            .data("item.autocomplete", item)// добавляем данные из объекта
            .append('<a href="' + item.product_link + '"><div class="autocompleter__name">' + '<span>' + item.pname + ' ' + (item.reference ? item.reference : '') + '</span></div></a>') // аппендим в созданный элемент информацию из объекта item
            .appendTo(ul); // аппендим элемент в выбранный ul
    };

    function autocompleter ($input, $appendTo, focusDo, selectDo) {
        var isAjaxSearchAllowed = window.ajaxsearch, //глобальная переменная из админки, включающая поиск аяксом
            searchUrl = window.search_url, //глобальный адрес экшена для поиска
            idLang = window.id_lang; //Айдишник текущего языка

        //Без этого мы не сможем запуститься
        if (!isAjaxSearchAllowed && !searchUrl) return;
        if (typeof $input.length === 'undefined') {
            throw new Error ('Identifier $input is required and must be a jQuery object');
        }

        if (typeof $.fn.autocomplete !== 'function') {
            throw new Error ('There are no jQuery plugin for autocomplete');
        }

        //Установка дефолтных значений, если не установлено иное
        $appendTo && ($appendTo == $('body'));

        focusDo = focusDo || function (event, ui) {
                event.preventDefault();
                if (typeof  event.keyCode !== 'undefined') {
                    $input.val( ui.item.pname );
                }
            };

        selectDo = selectDo || function( event, ui ) {
                event.preventDefault();
                $input.val( ui.item.pname );
                window.location.href = ui.item.product_link;
            };

        //Обертки, которые будут выполнены непосредственно в плагине
        var focusDoWrapper = function(event, ui) {
            focusDo(event, ui);
        };

        var selectDoWrapper = function(event, ui){
            selectDo(event, ui);
        };

        var openEvent = function () {
	        if (window.isMobile) {
                $('body').css('overflow', 'hidden');
            }
        };

        var closeEvent = function () {
	        if (window.isMobile) {
                $('body').css('overflow', '');
            }
        };

        $input.autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: searchUrl,
                    type: config.method,
                    dataType: config.dataType,
                    data: {
                        q: request.term,
                        limit: 10,
                        timestamp: Math.floor( (new Date().getTime()) / 1000  ),
                        ajaxSearch: 1,
                        id_lang: idLang
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            appendTo: $appendTo,
            minLength: config.minLength,
            autoFocus: config.autoFocus,
            focus: focusDoWrapper,
            select: selectDoWrapper,
            open: openEvent,
            close: closeEvent
        }).data("ui-autocomplete")._renderItem = smallPointTemplate;
    }

    //инициализируем автокомплитер
    var $autocompleter = $('#search_query_top'),
        //$autocompleteTextRemove = $('.js-click--clean_autocompleter'),
        $searchBlock = $autocompleter.closest('.header__search');

    var classes = {
        hasText: 'header__search--has_text'
    };

    if ($autocompleter.length){
        var autocompleteHolder = window.isMobile ? '.header' : '#search_block_top';
        autocompleter($autocompleter, $(autocompleteHolder));

//        $autocompleteTextRemove.click(function () {
//            $autocompleter
//                .val('')
//                .autocomplete( "close" );
//            $searchBlock.removeClass(classes.hasText);
//        });

//        $autocompleter.on('textchange', function () {
//            console.log($autocompleter.val().length);
//            if ($autocompleter.val().length) {
//                $searchBlock.addClass(classes.hasText);
//            } else {
//                $searchBlock.removeClass(classes.hasText);
//            }
//        });
    }
});
// ]]>
</script>
{/if}

{if $instantsearch}
<script type="text/javascript">
// <![CDATA[
function tryToCloseInstantSearch()
{
	var $oldCenterColumn = $('#old_center_column');
	if ($oldCenterColumn.length > 0)
	{
		$('#center_column').remove();
		$oldCenterColumn.attr('id', 'center_column').show();
		return false;
	}
}

instantSearchQueries = [];
function stopInstantSearchQueries()
{
	for(var i=0; i<instantSearchQueries.length; i++) {
		instantSearchQueries[i].abort();
	}
	instantSearchQueries = [];
}

$('document').ready(function() {

	var $input = $("#search_query_{$blocksearch_type}");

	$input.on('keyup', function() {
		if ($(this).val().length > 4)
		{
			stopInstantSearchQueries();
			instantSearchQuery = $.ajax({
				url: '{if $search_ssl == 1}{$link->getPageLink('search', true)|addslashes}{else}{$link->getPageLink('search')|addslashes}{/if}',
				data: {
					instantSearch: 1,
					id_lang: {$cookie->id_lang},
					q: $(this).val()
				},
				dataType: 'html',
				type: 'POST',
				headers: { "cache-control": "no-cache" },
				async: true,
				cache: false,
				success: function(data){
					if($input.val().length > 0)
					{
						tryToCloseInstantSearch();
						$('#center_column').attr('id', 'old_center_column');
						$('#old_center_column').after('<div id="center_column" class="' + $('#old_center_column').attr('class') + '">'+data+'</div>').hide();
						// Button override
						ajaxCart.overrideButtonsInThePage();
						$("#instant_search_results a.close").on('click', function() {
							$input.val('');
							return tryToCloseInstantSearch();
						});
						return false;
					}
					else
						tryToCloseInstantSearch();
				}
			});
			instantSearchQueries.push(instantSearchQuery);
		}
		else
			tryToCloseInstantSearch();
	});
});
// ]]>
</script>
{/if}