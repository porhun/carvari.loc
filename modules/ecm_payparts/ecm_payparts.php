<?php
if (!defined('_PS_VERSION_'))   exit;

class ecm_payparts extends PaymentModule
{
    private $_html = '';
    public function __construct()
    {
        $this->name = 'ecm_payparts';
        $this->tab = 'payments_gateways';
        $this->version = '0.2';
        $this->author = 'Elcommerce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array(
            'min'=> '1.5',
            'max'=> '1.6'
        );
        $this->need_instance = 1;
        $this->bootstrap = true;
        $this->currencies = true;
        $this->displayName = $this->l('PayParts');
        $this->description = $this->l('Accept payments with PayParts');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->bootstrap = true;

        $params = array('storeId','password','merchantType','merchantTypePP','merchantTypeII','partsCount','qr_order');
        $config = Configuration::getMultiple($params);

        if (isset($config['storeId']) && $config['storeId']) {
            $this->storeId = $config['storeId'];
        }
        if (isset($config['password']) && $config['password']) {
            $this->password = $config['password'];
        }
        if (isset($config['merchantType']) && $config['merchantType']) {
            $this->merchantType = $config['merchantType'];
        }
        if (isset($config['merchantTypePP']) && $config['merchantTypePP']) {
            $this->merchantTypePP = $config['merchantTypePP'];
        }
        if (isset($config['merchantTypeII']) && $config['merchantTypeII']) {
            $this->merchantTypeII = $config['merchantTypeII'];
        }
        if (isset($config['qr_order']) && $config['qr_order']) {
            $this->qr_order = $config['qr_order'];
        }

        if (isset($config['partsCount']) && $config['partsCount']) {
            $this->partsCount = $config['partsCount'];
        }
        else  $this->partsCount = 5;

        parent::__construct();
        $this->page = basename(__FILE__, '.php');
        $correctly = !isset($this->storeId) OR !isset($this->password);
        if ($correctly) {
            $this->warning = $this->l('Your PayParts account must be set correctly');
        }
    }


    public function getPath()
    {
        return $this->_path;
    }

    public function install()
    {
        return parent::install() && $this->registerHook('payment') && $this->registerHook('paymentReturn') && $this->registerHook('displayBackOfficeHeader');
    }

    public function uninstall()
    {
        return
        parent::uninstall() &&
        Configuration::deleteByName('storeId') &&
        Configuration::deleteByName('password') &&
        Configuration::deleteByName('merchantType') &&
        Configuration::deleteByName('merchantTypePP') &&
        Configuration::deleteByName('merchantTypeII') &&
        Configuration::deleteByName('partsCount') &&
        Configuration::deleteByName('qr_order');
    }


    public function hookPayment($params)
    {

        require_once(dirname(__FILE__).'/controllers/PayParts.php');

        $context = Context::getContext();
        $cart    = $context->cart;


        $total    = $cart->getOrderTotal(true, Cart::ONLY_PRODUCTS);
        $total_shipping    = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);

        $prods   = $cart->getProducts();
        $products = [];

        foreach ($prods as $prod) {
            array_push($products,
                [
                    'name' =>  $prod['name'],
                    'count' => $prod['cart_quantity'],
                    'price' => round($prod['price_wt'], 2)
                ]
            );
        }

        if($total_shipping>0) {
            array_push($products,
                [
                    'name' =>  $this->l('Shipment'),
                    'count' => 1,
                    'price' => round($total_shipping, 2)
                ]
            );
        }



        $storeId     = Configuration::get('storeId');
        $password    = Configuration::get('password');
        $ssl_enable = Configuration::get('PS_SSL_ENABLED');
        $base = (($ssl_enable) ? 'https://' : 'http://');
        $responseUrl = $base.htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').$this->getPath(). 'validation.php';
        $pp          = new \ecm_payparts\PayParts_git($storeId, $password);
        $this->context->cookie->ppOrderID = $context->cart->id.'-'.uniqid();
        $options = array(
//            'amount' => $total,
            'responseUrl' => $responseUrl,
            'redirectUrl' => $responseUrl,
            'OrderID'     => $this->context->cookie->ppOrderID,
            'ProductsList'=> $products,
            'recipientId' => ''
        );



        $partsCount   = Configuration::get('partsCount');
        $qr_order     = Configuration::get('qr_order');
        $merchantType = Tools::getValue('merchantType', 'II');
        $this_path    = $this->getPath();
        $options['merchantType'] = $merchantType;
        $options['PartsCount'] = $partsCount;
        $pp->setOptions($options);
        $send = $pp->create('pay');


        if ($qr_order && $qr_order == 1) {
            $url = "https://payparts2.privatbank.ua/ipp/qr/generate?token=".$send['token']."&size=200&type=".$merchantType."&amount=".$total;
            $res = json_decode($pp->sendGeTQR($url), true);
            if ($res['state'] == 'SUCCESS')$qr = $res['qr'];
        }

        $this->context->smarty->assign(compact('server_url', 'id_cart', 'partsCount','merchantType','this_path','qr','qr_order','total'));


        $this->smarty->assign(array(
                'this_path'          => $this->_path,
                'this_path_ssl'      => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/',
                'id'                 => (int)$params['cart']->id,
                'module_id'                 => $this->id,
                'displayMerchantType'=> Configuration::get('merchantType'),
                'MerchantTypePP'    => Configuration::get('merchantTypePP'),
                'MerchantTypeII'    => Configuration::get('merchantTypeII'),
            ));

        return $this->display(__FILE__, 'payment.tpl');
    }
     public function hookDisplayBackOfficeHeader()
    {
        if (method_exists($this->context->controller, 'addJS'))
            $this->context->controller->addJS(($this->_path).'views/js/ecm_payparts.js', 'all');
    }
    public function hookpaymentReturn($params)
    {
        if (!$this->active)
        return;

        $state = $params['objOrder']->getCurrentState();
        if ($state == Configuration::get('PS_OS_PAYMENT')) {
            $this->smarty->assign(array(
                    'status'  => 'success',
                    'id_order'=> $params['objOrder']->id
                ));
            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
            $this->smarty->assign('reference', $params['objOrder']->reference);
        }

        elseif ($state == Configuration::get('PS_OS_BANKWIRE')) {
            $this->smarty->assign(array(
                    'status'  => 'wait_secure',
                    'id_order'=> $params['objOrder']->id
                ));
            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
            $this->smarty->assign('reference', $params['objOrder']->reference);
        }

        elseif ($state == Configuration::get('PS_OS_ERROR')) {
            $this->smarty->assign(array(
                    'status'  => 'failure',
                    'id_order'=> $params['objOrder']->id
                ));
            if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
            $this->smarty->assign('reference', $params['objOrder']->reference);
        }

        else
        $this->smarty->assign('status', 'other');
        $this->smarty->assign('this_path',$this->_path);
        return $this->display(__FILE__, 'payment_return.tpl');
    }

    public function getContent()
    {
        if (Tools::isSubmit('submit'.$this->name)) {
            $storeId      = strval(Tools::getValue('storeId'));
            $password     = strval(Tools::getValue('password'));
            $merchantType = strval(Tools::getValue('merchantType'));
            $merchantTypePP = strval(Tools::getValue('merchantTypePP'));
            $merchantTypeII = strval(Tools::getValue('merchantTypeII'));
            $partsCount   = strval(Tools::getValue('partsCount'));
            $qr_order     = (int)Tools::getValue('qr_order');
            $err = !$storeId || empty($storeId) || !Validate::isGenericName($storeId) ||
            !$password || empty($password) || !Validate::isGenericName($password);

            if ($err) {
                $this->_html .= $this->displayError( $this->l('Invalid Configuration value') );
            } else {
                Configuration::updateValue('storeId', $storeId);
                Configuration::updateValue('password', $password);
                Configuration::updateValue('merchantType', $merchantType);
                Configuration::updateValue('merchantTypePP', $merchantTypePP);
                Configuration::updateValue('merchantTypeII', $merchantTypeII);
                Configuration::updateValue('partsCount', $partsCount);
                Configuration::updateValue('qr_order', $qr_order);
                $this->qr_order = $qr_order;
                $this->storeId = $storeId;
                $this->password = $password;
                $this->merchantType = $merchantType;
                $this->merchantTypePP = $merchantTypePP;
                $this->merchantTypeII = $merchantTypeII;
                $this->partsCount = $partsCount;

                $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
        $this->_html .= $this->displayForm();
        $this->_displayabout();
        return $this->_html;
    }

    public function displayForm()
    {
        $options    = [

            [   'id_option' => '1',
                'name' => 'Оплата частями'],
            [   'id_option' => '2',
                'name' => 'Мгновенная рассрочка'],
            [   'id_option' => '3',
                'name' => 'Мгновенная рассрочка и Оплата частями'],
        ];
         $options_pp    = [

            [   'id_option' => 'PP',
                'name' => 'Обычная'],
            [   'id_option' => 'PB',
                'name' => 'Деньги в периоде'],
    
        ];
         $options_ii    = [

             [  'id_option' => 'II',
                'name' => 'Обычная'],
            [   'id_option' => 'IA',
                'name' => 'Акционная'],
        ];

        $partsCount = [];
        for ($i = 2;$i <= 24;$i++)
        array_push($partsCount, ['id'=>$i]);



        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $fields_form[0]['form'] = [
            'legend' => [
                'title' => $this->l('Настройки'),
                'icon' => 'icon-cog'
            ],
            'input' => [
                [
                    'type' => 'text',
                    'label' => $this->l('Идентификатор магазина'),
                    'desc' => $this->l('storeId'),
                    'name' => 'storeId',
                    'size' => 30,
                    'required' => true
                ],
                [
                    'type' => 'text',
                    'label' => $this->l('Пароль магазина'),
                    'desc' => $this->l('password'),
                    'name' => 'password',
                    'size' => 30,
                    'required' => true
                ],

                [
                    'type' => 'select',
                    'id' => 'merchantType',
                    'label' => $this->l('Тип кредита'),
                    'desc' => $this->l('merchantType'),  // A help text, displayed right next to the < select > tag.
                    'name' => 'merchantType',
                    'required' => true,
                    'options' => [
                        'query' => $options,
                        'id' => 'id_option',
                        'name' => 'name'
                    ]
                ],

                [
                    'id' => 'merchantTypePP',
                    'type' => 'select',
                    'label' => $this->l('Вид кредита оплата частями'),
                    'desc' => $this->l('Вид кредита оплата частями "обычная" или "деньги в периоде"'),  // A help text, displayed right next to the < select > tag.
                    'name' => 'merchantTypePP',
                   
                    'required' => true,
                    'options' => [
                        'query' => $options_pp,
                        'id' => 'id_option',
                        'name' => 'name'
                    ]
                ],
                [
                    'type' => 'select',
                    'label' => $this->l('Вид кредита мгновенная рассрочка'),
                    'desc' => $this->l('Вид кредита мгновенная рассрочка "обычная" или "акционная"'),  // A help text, displayed right next to the < select > tag.
                    'name' => 'merchantTypeII',
                    'id' => 'merchantTypeII',
                    'required' => true,
                    'options' => [
                        'query' => $options_ii,
                        'id' => 'id_option',
                        'name' => 'name'
                    ]
                ],
                [
                    'type' => 'select',
                    'label' => $this->l('partsCount'),
                    'desc' => $this->l('password'),
                    'name' => 'partsCount',
                    'required' => true,
                    'options' => [
                        'query' => $partsCount,
                        'id' => 'id',
                        'name' => 'id'
                    ]
                ],
                [
                    'type' => 'switch',
                    'label' => $this->l('Показывать QR Код'),
                    'name' => 'qr_order',
                    'desc' => $this->l('Показывать QR Код на странице оплаты заказа'),
                    'values' => [
                        [
                            'id' => 'qr_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ],
                        [
                            'id' => 'qr_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        ]
                    ]
                ],
            ],
            'submit' => [
                'title' => $this->l('Save'),

            ]
        ];

        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc'=> $this->l('Save'),
                'href'=> AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href'=> AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc'=> $this->l('Back to list')
            )
        );
        @$helper->fields_value['storeId'] = $this->storeId;
        @$helper->fields_value['password'] = $this->password;
        @$helper->fields_value['merchantType'] = $this->merchantType;
        @$helper->fields_value['merchantTypePP'] = $this->merchantTypePP;
        @$helper->fields_value['merchantTypeII'] = $this->merchantTypeII;
        @$helper->fields_value['partsCount'] = $this->partsCount;
        @$helper->fields_value['qr_order'] = $this->qr_order;


        return $helper->generateForm($fields_form);
    }
    private
    function _displayabout()
    {

        $this->_html .= '
        <div class="panel">
        <div class="panel-heading">
        <i class="icon-envelope"></i> ' . $this->l('Информация') . '
        </div>
        <div id="dev_div">
        <span><b>' . $this->l('Версия') . ':</b> ' . $this->version . '</span><br>
        <span><b>' . $this->l('Разработчик') . ':</b> <a class="link" href="mailto:support@elcommerce.com.ua" target="_blank">Savvato</a>

        <span><b>' . $this->l('Описание') . ':</b> <a class="link" href="http://elcommerce.com.ua" target="_blank">http://elcommerce.com.ua</a><br><br>
        <p style="text-align:center"><a href="http://elcommerce.com.ua/"><img src="http://elcommerce.com.ua/img/m/logo.png" alt="Электронный учет коммерческой деятельности" /></a>

        </div>
        </div>

        ';
    }

}
