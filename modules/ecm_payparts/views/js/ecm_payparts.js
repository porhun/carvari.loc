function show_hide(val){
    if (val ==1){
        $("#merchantTypeII").parent().parent().hide();
        $("#merchantTypePP").parent().parent().show();
    }else if(val == 2){
        $("#merchantTypePP").parent().parent().hide();
        $("#merchantTypeII").parent().parent().show();
    }else{
        $("#merchantTypePP").parent().parent().show();
        $("#merchantTypeII").parent().parent().show();
    }
}
$(document).ready(function () {
        show_hide($("select[name=merchantType] option:selected").val());
        $("#merchantType" ).change(function() {
                val        = $(this).val();
                show_hide(val);
            });
    });

