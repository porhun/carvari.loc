{*
*Template for Payment Return Hook
*}
{if $status == 'success'}
<p>
    <img src="{$this_path}img/ok.png" align="left">
    <strong>
        {l s='Your order on %s is complete.' sprintf=$shop_name mod='ecm_payparts'}
    </strong>
    <br /><br />
    <strong>
        {l s='Your order will be sent soon.' mod='ecm_payparts'}
    </strong>
    <br /><br />{l s='You can view your order history by following this link:' mod='ecm_payparts'}
    <a href="{$link->getPageLink('history', true)}">
        {l s='Order History' mod='ecm_payparts'}
    </a>
    <br /><br />{l s='For any questions or for further information, please contact our' mod='ecm_payparts'}
    <a href="{$link->getPageLink('contact', true)}">
        {l s='customer support' mod='ecm_payparts'}
    </a>.
    <br /><br />
    <strong>
        {l s='Thank you!' mod='ecm_payparts'}
    </strong>
</p>

{elseif $status == 'wait_secure'}
<p>    <img src="{$this_path}img/ok.png" align="left">
<strong>
    {l s='P24 system is validating your payment.' mod='ecm_payparts'}
</strong>
<br /><br />
<strong>
    {l s='Your order will be sent as soon as we receive your settlement.' mod='ecm_payparts'}
</strong>
<br /><br />{l s='For any questions or for further information, please contact our' mod='ecm_payparts'}
<a href="{$link->getPageLink('contact', true)}">
    {l s='customer support' mod='ecm_payparts'}
</a>.
<br /><br />
<strong>
    {l s='Thank you!' mod='ecm_payparts'}
</strong>
</p>
{elseif $status == 'failure'}
<p class="warning">
    <img src="{$this_path}img/not_ok.png" align="left">{l s='К сожалению Вам пришел отказ' mod='ecm_payparts'}.
    <br /><br />
    {l s='Your order will not be shipped until the issue is addressed' mod='ecm_payparts'}
    <br /><br />
</p>
{else}
<p class="warning">
    <img src="{$this_path}img/not_ok.png" align="left">{l s='We noticed a problem with your order.' mod='ecm_payparts'}.
    <br /><br />
    {l s='Please choose P24 payment method from' mod='ecm_payparts'}
    <a href="{$link->getPageLink('order', true)}">
        {l s='Order Page' mod='ecm_payparts'}
    </a>
    <br /><br />
</p>
{/if}