{if $displayMerchantType == 3 || $displayMerchantType !=2}

<div class="row">
    <div class="col-xs-12">
        <p class="payment_module">
            <a class="parts" href="{$link->getModuleLink('ecm_payparts', 'redirect', ['id_cart' => {$id}, merchantType => {$MerchantTypePP}])}" title="{l s='Pay PayParts' mod='ecm_payparts'}">
                <img src="{$this_path}logo_opc_ps.png" width="50px" />{l s='Оплата частями' mod='ecm_payparts'}
                <span>
                    (оплата на сайте)
                </span>

            </a>
        </p>
    </div>
</div>

{/if}
{if $displayMerchantType == 3 || $displayMerchantType !=1}
<div class="row">
    <div class="col-xs-12">
        <p class="payment_module">
            <a class="rassrochka" href="{$link->getModuleLink('ecm_payparts', 'redirect', ['id_cart' => {$id}, merchantType => {$MerchantTypeII}])}" title="{l s='Pay PayParts' mod='ecm_payparts'}">
                <img src="{$this_path}clock_opc_ps.png" width="50px" />{l s='Мгновенная рассрочка' mod='ecm_payparts'}
                <span>
                    (оплата на сайте)
                </span>

            </a>
        </p>
    </div>
</div>
{/if}

<div class="add_description">
    <div class="ecm_payparts-add_description" style="display: none;">
        <link rel="stylesheet" href="{$this_path}views/css/ecm_payparts.css" />

        <div class="checkout ff_payparts">
            <div class="ff_payparts__left">
                {if $merchantType == 'PP'}
                    <img src="{$this_path}logo.png" width="40px" class="ff_payparts__logo" />
                {else}
                    <img src="{$this_path}clock.png" width="40px" class="ff_payparts__logo" />
                {/if}
                <h3 class="ff_payparts__type_title">
                    {*{l s='Оформление покупки в кредит' mod='ecm_payparts'} *}
                    {if $merchantType == 'PP'}{l s='ОПЛАТА ЧАСТЯМИ'}
                    {else}{l s='Мгновенная рассрочка' mod='ecm_payparts'}
                    {/if}
                </h3>
            </div>
            <div class="ff_payparts__right">
                <div class="ff_payparts__sum_title">{l s='Сумма платежа' mod='ecm_payparts'}</div>
                <div class="ff_payparts__sum_value js_ff_payparts_sum_month"></div>
                <div class="ff_payparts__sum_currency_sign">{$currencySign} / {l s='мес' mod='ecm_payparts'}</div>
            </div>
            <div class="clearfix"></div>
            <div class="ff_payparts__info">
                <div class="ff_payparts__info_title">{l s='Срок кредитования' mod='ecm_payparts'}</div>
                <ul class="ff_payparts__info_list">
                    <li class="ff_payparts__info_list_item">
                        <span class="ff_payparts__info_list_dt">{l s='месяцев' mod='ecm_payparts'}</span>
                        <span class="ff_payparts__info_list_dd js_ff_payparts_amount_months">1</span>
                    </li>
                    <li class="ff_payparts__info_list_item">
                        <span class="ff_payparts__info_list_dt">{l s='платежей' mod='ecm_payparts'}</span>
                        {*<span>{$partsCount}</span>*}
                        <select name="payparts" class="ff_payparts__info_list_dd js_ff_payparts_amount_parts">
                            {for $payPartsItem=2 to $partsCount}
                            <option value="{$payPartsItem}" {if $payPartsItem == 1}checked{/if}>
                                {$payPartsItem}
                                </option>{/for}
                        </select>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <div id="ecm_payparts_redirect" class="ff_payparts__form">

                <input type="hidden" name="id_cart"   value="{$id}" />
                <input type="hidden" name="merchantType"   value="{$merchantType}" />


                <input class="range_form js_ff_payparts_slide" type="range" name="PartsCountInput" id="PartsCountInputId"
                       value="2"
                       min="2" max="{$partsCount}" style="position: relative;" />

                <button id="ecm_payparts_submit" class="button btn btn-default button-medium" type="button" style="display: none;">
                    <span>
                        {l s='I confirm my order' mod='ecm_payparts'}
                        <i class="icon-chevron-right right">
                        </i>
                    </span>
                </button>

            </div>
            {if $qr_order && $qr && $qr_order == 1}
                <img src="data:image/png;base64,{$qr}"  />
                <br>{l s='Смартфон > Приват24 > Бесконтакт > Сканировать' mod='ecm_payparts'}
            {/if}
        </div>
    </div>
    <script>
        var js_ff_payparts_total_price = {if isset($total) && !empty($total)}{$total}{else}0{/if};
        var ecm_payparts_redirect_url= "{$link->getModuleLink('ecm_payparts', 'redirect', ['id_cart' => {$id}, merchantType => {$MerchantTypePP}])}";
        var ecm_payparts_module_id = {$module_id};
        {literal}
        (function () {
            var $slide = $('.js_ff_payparts_slide');
            if ($slide && $slide.length) {
                var $parts = $('.js_ff_payparts_amount_parts');
                var $months = $('.js_ff_payparts_amount_months');
                var $sumMonth = $('.js_ff_payparts_sum_month');

                $slide.on('input', function () {
                    var slideVal = $(this).val();
                    $parts.find('option[value=' + slideVal + ']').prop('selected', true);
                    $parts.trigger('refresh');
                    $months.text(slideVal - 1);
                    $sumMonth.text(countMonthSum(js_ff_payparts_total_price, slideVal));
                });

                $parts.on('change', function () {
                    var selectVal = $(this).val();
                    $slide.val(selectVal);
                    $months.text(selectVal - 1);
                    $sumMonth.text(countMonthSum(js_ff_payparts_total_price, selectVal));
                });

                $('input[name=id_payment_method]').on('change', function (e) {
                    $('.ecm_payparts-add_description').slideUp();
                    if($(this).attr('idorig') == ecm_payparts_module_id) {
                       $('.ecm_payparts-add_description').slideDown();
                    }
                });

                $('#ecm_payparts_submit').on('click', function (e) {
                    location.href = ecm_payparts_redirect_url + '&PartsCountInput='+$('#PartsCountInputId').val();
                });

                function countMonthSum(total, countMonth) {
                    return Math.round((((total * 2.9 / 100) * countMonth) + total) / countMonth);
                }

                $sumMonth.text(countMonthSum(js_ff_payparts_total_price, $slide.val()));
            }
        })();
        {/literal}
    </script>
</div><div>not_touch</div><div>not_touch</div>

