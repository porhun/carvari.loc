<?php

require_once(dirname(__FILE__).'../../../ecm_payparts.php');
require_once(dirname(__FILE__).'../../../controllers/PayParts.php');
use ecm_payparts\PayParts_git;
class ecm_paypartsredirectModuleFrontController extends ModuleFrontController
{
    public $display_header = true;
    public $display_column_left = false;
    public $display_column_right = true;
    public $display_footer = true;
    public $ssl = true;

    public function initContent()
    {

        parent::initContent();
        global $cookie;

        if ($id_cart = Tools::getValue('id_cart')) {
            $cart = new Cart($id_cart);
            if (!Validate::isLoadedObject($cart)) {
                $cart = $this->context->cart;
            }
        } else {
            $cart     = $this->context->cart;
        }

        $total    = $cart->getOrderTotal(true, Cart::BOTH);
        $total_shipping    = $cart->getOrderTotal(true, Cart::ONLY_SHIPPING);
        $id_cart = $cart->id;
        $prods   = $cart->getProducts();
        $products= [];
        foreach ($prods as $prod) {
            array_push($products,
                [
                    'name' =>  $prod['name'],
                    'count' => $prod['cart_quantity'],
                    'price' => round($prod['price_wt'], 2)
                ]
            );
        }

        if($total_shipping>0) {
            array_push($products,
                [
                    'name' =>  'Delivery',
                    'count' => 1,
                    'price' => round($total_shipping, 2)
                ]
            );
        }


        $payparts = new ecm_payparts();
        $currency = new Currency((int)($cart->id_currency));
        $storeId     = Configuration::get('storeId');
        $password    = Configuration::get('password');
        $language    = Configuration::get('PS_LOCALE_LANGUAGE') == 'en' ? 'en' : 'ru';
        $ssl_enable = Configuration::get('PS_SSL_ENABLED');
		$base = (($ssl_enable) ? 'https://' : 'http://');
        $responseUrl = $base.htmlspecialchars($_SERVER['HTTP_HOST'], ENT_COMPAT, 'UTF-8').$payparts->getPath(). 'validation.php';
        $pp          = new PayParts_git($storeId, $password);
        $this->context->cookie->ppOrderID = $id_cart.'-'.uniqid();
        $options = array(
            'responseUrl' => $responseUrl,
            'redirectUrl' => $responseUrl,
            'OrderID'     => $this->context->cookie->ppOrderID,
            'ProductsList'=> $products,
            'recipientId' => ''
        );
        if (!isset($_GET['PartsCountInput'])) {

            $partsCount   = Configuration::get('partsCount');
            $qr_order     = Configuration::get('qr_order');
            $merchantType = Tools::GetValue('merchantType');
            $this_path    = $payparts->getPath();
            $options['merchantType'] = $merchantType;
            $options['PartsCount'] = $partsCount;
            $pp->setOptions($options);
            $send = $pp->create('pay');
            if ($qr_order && $qr_order == 1) {
                $url = "https://payparts2.privatbank.ua/ipp/qr/generate?token=".$send['token']."&size=200&type=".$merchantType."&amount=".$total;
                $res = json_decode($pp->sendGeTQR($url), true);
                if ($res['state'] == 'SUCCESS')$qr = $res['qr'];
            }

            $this->context->smarty->assign(compact('server_url', 'id_cart', 'partsCount','merchantType','this_path','qr','qr_order','total'));
            $this->setTemplate('redirect.tpl');
        }

        if (isset($_GET['PartsCountInput'])) {

            $partsCount   = Tools::GetValue('PartsCountInput');
            $merchantType = $_GET['merchantType'];
            //$payparts->validateOrder(intval($cart->id), _PS_OS_PREPARATION_, $total, $payparts->displayName);
            $options['merchantType'] = $merchantType;
            $options['PartsCount'] = $partsCount;
            $pp->setOptions($options);
            $send = $pp->create('pay');//hold //pay

            if(!empty($send['token'])) {
                header('Location: https://payparts2.privatbank.ua/ipp/v2/payment?token=' . $send['token']);
            } else {
                throw new ErrorException($send['message']);
            }


        }

    }
}
