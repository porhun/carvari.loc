<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ecm_payparts}prestashop>ecm_payparts_34d7958b8ca99f4bf09eef92ea6c1d14'] = 'PayParts';
$_MODULE['<{ecm_payparts}prestashop>ecm_payparts_0f5133812d963569d288380abe2ce5b8'] = 'настройка оплаты PayParts';
$_MODULE['<{ecm_payparts}prestashop>ecm_payparts_876f23178c29dc2552c0b48bf23cd9bd'] = 'Уверены?';
$_MODULE['<{ecm_payparts}prestashop>ecm_payparts_41713f208913e85d7711c121ba55bee2'] = 'Ваш аккаунт не корректен';
$_MODULE['<{ecm_payparts}prestashop>redirect_569fd05bdafa1712c4f6be5b153b8418'] = 'Другие способы оплаты';
$_MODULE['<{ecm_payparts}prestashop>redirect_46b9e3665f187c739c55983f757ccda0'] = 'Я подтверждаю свой заказ';
$_MODULE['<{ecm_payparts}prestashop>payment_return_88526efe38fd18179a127024aba8c1d7'] = 'Заказ  %s сформирован.';
$_MODULE['<{ecm_payparts}prestashop>payment_return_006b89a5932a8899d70b1840d07d89df'] = 'и готовится к отправке';
$_MODULE['<{ecm_payparts}prestashop>payment_return_8e3eeca040c62b9e2a1d4610164552d5'] = 'Для просмотра истории заказов перейдите по ссылке';
$_MODULE['<{ecm_payparts}prestashop>payment_return_42027dc8724f95b3f0a6bef9630cad3d'] = 'История заказов';
$_MODULE['<{ecm_payparts}prestashop>payment_return_0db71da7150c27142eef9d22b843b4a9'] = 'По любым вопросам пишите или звоните';
$_MODULE['<{ecm_payparts}prestashop>payment_return_64430ad2835be8ad60c59e7d44e4b0b1'] = 'нам';
$_MODULE['<{ecm_payparts}prestashop>payment_return_498cd895eb5a102c5aeb977e2b928dee'] = 'Спасибо!';
$_MODULE['<{ecm_payparts}prestashop>payment_return_c4fb248006077c6f80279328037666d7'] = 'Обработка заказа';
