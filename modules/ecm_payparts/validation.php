<?php

include(dirname(__FILE__) . '/../../config/config.inc.php');
include(dirname(__FILE__) . '/ecm_payparts.php');
include(dirname(__FILE__) . '/controllers/PayParts.php');

use ecm_payparts\PayParts_git;

$paypartsOrderID = $cookie->ppOrderID;

$errors         = '';
$id_cart        = explode("-", $paypartsOrderID);
$payparts       = new ecm_payparts();
$storeId        = Configuration::get('storeId');
$password       = Configuration::get('password');
$cart           = new Cart($id_cart[0]);
$total          = $cart->getOrderTotal(true, Cart::BOTH);
$id_order       = Order::getOrderByCartId($cart->id);

$pp             = new PayParts_git($storeId, $password);
$OrderID        = $pp->setOrderID($paypartsOrderID);
$getState       = $pp->getState($OrderID, false);

$order          = new Order((int)$id_order);
$customer       = new Customer((int)$order->id_customer);
$history        = new OrderHistory();

$history->id_order = $id_order;

if ($getState['paymentState'] === 'SUCCESS') {

    $history->changeIdOrderState(_PS_OS_PAYMENT_, $id_order);

    if (!$id_order) {
        $payparts->validateOrder((int)$cart->id, _PS_OS_PREPARATION_, $total, $payparts->displayName, NULL, array(), NULL, false, $cart->secure_key);
        $id_order = $payparts->currentOrder;
    }

    if (!$id_order) {
        die();
    }
    $order = new Order((int)$id_order);

    if (Configuration::get('CARVARI_CARRIER_ID') == $order->id_carrier && isset($context->cookie->id_store) && !empty($context->cookie->id_store)){
        Db::getInstance()->insert('ffcarvaricarrier', array(
            'id_order' => $id_order,
            'id_store' => $context->cookie->id_store,
        ));
    }

    Tools::redirectLink(__PS_BASE_URI__.'order-confirmation.php?key='.$customer->secure_key.'&id_cart='.$order->id_cart.
        '&id_module='.$payparts->id.'&id_order='.$order->id);
    $history->addWithemail(true);
} else {
//    $history->changeIdOrderState(_PS_OS_ERROR_, $id_order);
    Tools::redirectLink('order');
}



