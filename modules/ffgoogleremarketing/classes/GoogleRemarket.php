<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 15.12.16
 * Time: 10:22
 */
class GoogleRemarket
{
    const REMARKET_PRODUCTS_NAME = 'remarket_product_ids';
    const REMARKET_PRODUCT_TOTAL_VALUE_NAME = 'remarket_products_total_value';

    protected $method_format = 'get%sPageTypeInfo';
    protected $default_page_type = 'other';
    /**
     * List of page types name by given page names
     * Page names might be changed for your purpose
     * @var array $pages
     */
    protected $pages = [
        'index'     => 'home',
        'product'   => 'offerdetail',     // one product page
        'search'    => 'searchresults',    // products search page
        'orderopc'  => 'conversionintent',   // cart page
        'moduleadvancedcheckoutpayment'     => 'conversionintent',   // submit order page
        'moduleadvancedcheckoutconfirm'  => 'conversion', // confirmed order's page
    ];

    protected $page_name = null;
    protected $page_type = null;
    protected $context = null;
    protected $products = [];

    /**
     * GoogleRemarket constructor.
     * @param string $page_name     Current page name. If it doesn't exist in $pages, default page type will be set
     * @param Context $context      Context to get additional info
     * @param array $products       Cart\search products
     */
    public function __construct($page_name, Context $context, array $products = [])
    {
        $this->page_name = trim(preg_replace('/[^a-zA-Z]+/', '', $page_name));
        $this->setPageType($this->definePageType($this->page_name));
        $this->context = $context;
        $this->products = $products;
    }

    /**
     * Assign special info for current page type
     */
    public function getAssignPageTypeInfo()
    {
        $method = sprintf($this->method_format, ucfirst(strtolower($this->page_type)));
        if (!method_exists($this, $method)) {
            throw new BadMethodCallException("No such method for assigning the page type info: {$method}");
        }
        return call_user_func([$this, $method]);
    }

    /**
     * Find and return page type based on page name
     * If no page type found, return default page type
     *
     * @param string $page_name
     * @return string
     */
    public function definePageType($page_name)
    {
        if (isset($this->pages[$page_name])) {
            return $this->pages[$page_name];
        }
        return $this->getDefaultPageType();
    }

    /**
     * Return default page type name
     *
     * @return string
     */
    public function getDefaultPageType()
    {
        return $this->default_page_type;
    }

    public function getPageType()
    {
        return $this->page_type;
    }

    protected function setPageType($page_type)
    {
        $this->page_type = $page_type;
    }

    protected function getHomePageTypeInfo()
    {
        return [];
    }

    /**
     * One product page
     * @return array
     */
    protected function getOfferdetailPageTypeInfo()
    {
        $product = $this->context->controller->getProduct();
        return [
            self::REMARKET_PRODUCTS_NAME => $product->id,
            self::REMARKET_PRODUCT_TOTAL_VALUE_NAME => $product->price
        ];
    }

    /**
     * Search page
     *
     * @return array
     */
    protected function getSearchresultsPageTypeInfo()
    {
        $products_info = $this->getProductFormattedInfo();
        return [
            self::REMARKET_PRODUCTS_NAME => $products_info[self::REMARKET_PRODUCTS_NAME],
            self::REMARKET_PRODUCT_TOTAL_VALUE_NAME => $products_info[self::REMARKET_PRODUCT_TOTAL_VALUE_NAME]
        ];
    }

    /**
     * Cart page
     * @return array
     */
    protected function getConversionintentPageTypeInfo()
    {
        $this->products = ($this->products && count($this->products)) ? $this->products : $this->context->cart->getProducts();
        $products_info = $this->getProductFormattedInfo();
        return [
            self::REMARKET_PRODUCTS_NAME => $products_info[self::REMARKET_PRODUCTS_NAME],
            self::REMARKET_PRODUCT_TOTAL_VALUE_NAME => $products_info[self::REMARKET_PRODUCT_TOTAL_VALUE_NAME]
        ];
    }

    /**
     * Order confirmation page
     * @return array
     */
    protected function getConversionPageTypeInfo()
    {
        $this->products = ($this->products && count($this->products)) ? $this->products : $this->context->cart->getProducts();
        $products_info = $this->getProductFormattedInfo();
        return [
            self::REMARKET_PRODUCTS_NAME => $products_info[self::REMARKET_PRODUCTS_NAME],
            self::REMARKET_PRODUCT_TOTAL_VALUE_NAME => $products_info[self::REMARKET_PRODUCT_TOTAL_VALUE_NAME]
        ];
    }

    protected function getOtherPageTypeInfo()
    {
        return [];
    }

    /**
     * Return formatted products info
     * If amount of prices or ids are equal to 1, we must return only this value, not an array
     *
     * @return array
     */
    protected function getProductFormattedInfo()
    {
        $products_ids    = array_column($this->products, 'id_product');
        $products_prices = array_column($this->products, 'price');
        $double_quotes_to_single = $this->changeDoubleQuotesToSingleFunc();
        $products_ids_formatted = '';
        $products_total_value   = '';
        if ($products_ids && count($products_ids)) {
            $products_ids = $double_quotes_to_single($products_ids);
            $products_ids_formatted = (count($products_ids) === 1) ? array_pop($products_ids) : $products_ids;
        }
        if ($products_prices && count($products_prices)) {
            $products_prices = $double_quotes_to_single($products_prices);
            $products_total_value = (count($products_prices) === 1) ? array_pop($products_prices) : $products_prices;
        }
        return [self::REMARKET_PRODUCTS_NAME => $products_ids_formatted, self::REMARKET_PRODUCT_TOTAL_VALUE_NAME => $products_total_value];
    }

    /**
     * Return function to change double quotes to single for strings/integers and arrays of strings/integers
     *
     * @return Closure
     */
    protected function changeDoubleQuotesToSingleFunc()
    {
        return function ($items) {
            if (is_string($items) || is_int($items) || is_float($items)) {
                return str_replace(chr(34), chr(39), (string)$items);
            }
            if (is_array($items)) {
                return array_map(function ($item) { return str_replace(chr(34), chr(39), strval($item)); }, $items);
            }
            return '';
        };
    }

}