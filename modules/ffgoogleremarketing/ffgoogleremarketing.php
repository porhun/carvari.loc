<?php
set_time_limit(0);

if (!defined('_PS_VERSION_'))
    exit;

/**
 * Class FfGoogleRemarketing
 * Cron link: /module/ffgoogleremarketing/export_products/?secure_key={$secure_key}
 * $secure_key is place below in the class
 */
class FfGoogleRemarketing extends Module
{

    const FF_GOOGLE_REMARKET_CID_NAME = 'ff_google_remarket_cid';
    const FF_GOOGLE_REMARKET_EXPORT_FILE_LINK = 'ff_google_remarket_csv_file';
    const FF_GOOGLE_REMARKET_EXPORT_FILE_PATH = 'ff_google_remarket_csv_file_path';


    const FF_GOOGLE_REMARKET_EXPORT_FILE_LINK_SKIDOCHNIK = 'ff_google_remarket_csv_file2';
    const FF_GOOGLE_REMARKET_EXPORT_FILE_PATH_SKIDOCHNIK = 'ff_google_remarket_csv_file_path2';

    protected $export_file_skeleton = ['id_product' => 'ID', 'product_link' => 'Final url', 'image_url' => 'Image URL', 'price' => 'Price', 'name' => 'Item title'];
    protected $export_file_skeletonsk = ['id_product' => 'ID', 'product_link' => 'Final url', 'image_url' => 'Image URL', 'price' => 'Price', 'oldprice' => 'Old Price', 'name' => 'Item title'];

    protected $product_catalog_file = 'remarket_catalog.csv';
    protected $product_catalog_file_skidochnik = 'remarket_catalog_skidochnik.xml';

    private $secure_key = 'f8bP33b23a7Fe7a9g5e92G5f42c019Db';

    public function __construct()
    {
        $this->name = 'ffgoogleremarketing';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'shandur';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Google Dynamic Remarketing');
        $this->description = $this->l('Dynamic Remarketing for site pages');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');
    }


    public function install()
    {
        if (!parent::install() || !$this->registerHook('displayGoogleRemarketing')
            || !$this->registerHook('actionGenerateExportCatalogProducts') || !$this->registerHook('moduleRoutes'))
            return false;

        Configuration::updateValue(self::FF_GOOGLE_REMARKET_CID_NAME, '');
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK, '');
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH, '');
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK_SKIDOCHNIK , '');
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH_SKIDOCHNIK, '');
        $this->cleanClassCache();

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;

        Configuration::deleteByName(self::FF_GOOGLE_REMARKET_CID_NAME);
        Configuration::deleteByName(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK);
        Configuration::deleteByName(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH);
        Configuration::deleteByName(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK_SKIDOCHNIK);
        Configuration::deleteByName(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH_SKIDOCHNIK);
        $this->deleteOldExportFile();
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache)) {
            unlink($class_cache);
        }
    }

    public function exportProducts()
    {
        $new_file_path = _PS_UPLOAD_DIR_.$this->product_catalog_file;
        $new_file_path_skidochnik = _PS_UPLOAD_DIR_.$this->product_catalog_file_skidochnik;
        $this->deleteOldExportFile();
        $file_csv = fopen($new_file_path, 'w');
        if (!$file_csv) {
            $this->adminDisplayWarning($this->l('Cannot create new product catalog file'));
            return;
        }
        $this->exportProductsToFile($file_csv, false);
        fclose($file_csv);

        $this->createXmlFile();
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH, $new_file_path);
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH_SKIDOCHNIK, $new_file_path_skidochnik);
        $this->updateExportFileDownloadLink();
        $this->adminDisplayInformation($this->l('Product catalog export file was created'));
    }

    protected function exportProductsToFile($file_handle, $skidochnik = false)
    {
        $skeleton = '';
        $skeletonsk = '';
        $link = $this->context->link;
        $currency_iso_code = $this->context->currency->iso_code;
        $products = Product::getProductForGoogleMarketExport();

        if($skidochnik) {
            $skeletonsk = $this->export_file_skeletonsk;
            $this->addFileExportHeader($file_handle, true);
        } else {
            $skeleton = $this->export_file_skeleton;
            $this->addFileExportHeader($file_handle, false);
        }
        foreach ($products as &$product_info) {
            $p = new Product($product_info['id_product']);
            $price = $p->getPrice(false);
            $product_info['product_link'] = $link->getProductLink($p);
            $product_info['image_url'] = $link->getImageLink((is_array($p->link_rewrite)) ? array_pop($p->link_rewrite) : $p->link_rewrite, (int)$product_info['id_image'], 'home_default');
            $product_info['price'] = ($price) ? sprintf('%01.2f %s', $price, $currency_iso_code) : '';

            if ($skidochnik) {
                $oldprice = (float)$p->getPriceWithoutReduct(false, null, 2);
                $product_info['oldprice'] = ($oldprice) ? sprintf('%01.2f %s', $oldprice, $currency_iso_code) : '';
            }
            $product_info['name'] = (is_array($p->name)) ? array_pop($p->name) : $p->name;
            // clean extra fields comparing with skeleton
            if ($skidochnik) {
                $product_info = array_intersect_key($product_info, $skeletonsk);
                // merge by keys in the right way (using skeleton)
                $product_info = array_merge($skeletonsk, $product_info);
            } else {
                $product_info = array_intersect_key($product_info, $skeleton);
                // merge by keys in the right way (using skeleton)
                $product_info = array_merge($skeleton, $product_info);
            }
            fputcsv($file_handle, $product_info);
            unset($p);
        }
    }

    protected function createXmlFile()
    {
        $lang = (int)Context::getContext()->language->id;
        $time = date("Y-m-d H:i:s");
        $link = $this->context->link;
        $shopName = Configuration::get('PS_SHOP_NAME');
        $currency_iso_code = $this->context->currency->iso_code;
        $catsHome = Category::getChildren(Configuration::get('PS_HOME_CATEGORY'), $lang, true);
        $products = Product::getProductForGoogleMarketExport();

        $new_file_path_skidochnik = _PS_UPLOAD_DIR_.$this->product_catalog_file_skidochnik;

        $dom = new DOMDocument('1.0', 'utf-8');
        $domEl = $dom->createElement('price');
        $domEl->setAttribute('date', $time);
        $dom->appendChild($domEl);

        $name = $dom->createElement('name', $shopName);
        $domEl->appendChild($name);

        $currencies = $dom->createElement('currencies');
        $currency = $dom->createElement('currency');
        $currency->setAttribute('id', $currency_iso_code);
        $currency->setAttribute('rate', '1');
        $currencies->appendChild($currency);
        $domEl->appendChild($currencies);

        $categories = $dom->createElement('categories');

        foreach ($catsHome as $cat) {
            $category = $dom->createElement('category', htmlspecialchars($cat['name']));
            $category->setAttribute('id', $cat['id_category']);
            $category->setAttribute('parentID', Configuration::get('PS_HOME_CATEGORY'));
            $categories->appendChild($category);

            $cats = new Category($cat['id_category'], $lang);
            $subCatFalse = Category::getChildren($cat['id_category'], $lang, 0);
            foreach ($subCatFalse as $subCatTrue){
                $subCatRoot = new Category($subCatTrue['id_category'],$lang,$this->context->shop->id);
                $subCat = $subCatRoot->getSubCategories($lang, true);
//                $subCat = Category::getChildren($subCatTrue['id_category'], $lang, 1);

                foreach ($subCat as $cates){
                    $category = $dom->createElement('category', htmlspecialchars($cates['name']));
                    $category->setAttribute('id', $cates['id_category']);
                    $category->setAttribute('parentID', $cates['id_parent']);
                    $categories->appendChild($category);
                }
            }

            $subCategories = $cats->getSubCategories($lang);;
            if(!empty($subCategories)){
                foreach ($subCategories as $sub){
                    $category = $dom->createElement('category', htmlspecialchars($sub['name']));
                    $category->setAttribute('id', $sub['id_category']);
                    $category->setAttribute('parentID', $sub['id_parent']);
                    $categories->appendChild($category);
                }
            }
        }
        $domEl->appendChild($categories);
        $offers = $dom->createElement('offers');

        foreach ($products as &$product_info) {
            $p = new Product($product_info['id_product']);
            $q = StockAvailableCore::getQuantityAvailableByProduct($p->id);
            $price = $p->getPrice(false);
            $oldprice = (float)$p->getPriceWithoutReduct(false, null, 2);
            $product_link = $link->getProductLink($p);
            $image_url = $link->getImageLink((is_array($p->link_rewrite)) ? array_pop($p->link_rewrite) : $p->link_rewrite, (int)$product_info['id_image'], 'home_default');
            $price = ($price) ? sprintf('%01.2f %s', $price, $currency_iso_code) : '';
            $oldprice = ($oldprice) ? sprintf('%01.2f %s', $oldprice, $currency_iso_code) : '';
            $features = $p->getFrontFeatures($this->context->language->id);

            if ($p->available_for_order != 0 && $q > 0 && $oldprice != $price){
                $offer = $dom->createElement('offer');
                $offer->setAttribute('id', $p->id);
                $offer->setAttribute('available', 'true');

                $vendor = $dom->createElement('vendor', htmlspecialchars($p->getWsManufacturerName()));
                $reference = $dom->createElement('reference', ($p->reference));
                $categoryId = $dom->createElement('categoryId', $p->id_category_default);
                $description = $dom->createElement('description', htmlspecialchars($p->description[4]));
                $name = $dom->createElement('name', htmlspecialchars($p->name[1]));
                $picture = $dom->createElement('picture', $image_url);
                $url = $dom->createElement('url', $product_link);
                $price = $dom->createElement('price', $price);
                $oldPrice = $dom->createElement('discountprice', $oldprice);

                $offer->appendChild($url);
                $offer->appendChild($name);
                $offer->appendChild($description);
                $offer->appendChild($categoryId);
                $offer->appendChild($price);
                $offer->appendChild($oldPrice);
                $offer->appendChild($picture);
                $offer->appendChild($vendor);
                $offer->appendChild($reference);

                foreach ($features as $feature){
                    if(isset($feature['value']) && !empty($feature['value']) && $feature['name'] != 'Стиль'){
                        $param  = $dom->createElement('param', htmlspecialchars($feature['value']));
                        $param->setAttribute('name', htmlspecialchars($feature['name']));
                        $offer->appendChild($param);
                    }
                }
                $offers->appendChild($offer);
            }
        }
        $domEl->appendChild($offers);

        $dom->save($new_file_path_skidochnik);
    }

    protected function addFileExportHeader($file_handler, $skidochnik = false)
    {
        if (!$file_handler) {
            return;
        }
        // enable UTF-8 for CSV file
        if($skidochnik){
            fprintf($file_handler, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($file_handler, $this->export_file_skeletonsk);
        }else {
            fprintf($file_handler, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($file_handler, $this->export_file_skeleton);
        }

    }

    protected function deleteOldExportFile()
    {
        foreach ($this->getExportFilePath() as $file_path){
            if ($file_path && file_exists($file_path)) {
                @unlink($file_path);
            }
        }
    }

    protected function getExportFilePath()
    {
        if(Configuration::get(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH_SKIDOCHNIK))
            return Configuration::get(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH_SKIDOCHNIK, '');

        return Configuration::get(self::FF_GOOGLE_REMARKET_EXPORT_FILE_PATH, '');
    }

    protected function getExportFileName()
    {
        if (!$file_path = $this->getExportFilePath()) {
            return '';
        }
        return pathinfo($file_path, PATHINFO_BASENAME);
    }

    protected function updateExportFileDownloadLink()
    {

        $file_link = (new ShopUrl(1))->getURL().'upload/'.$this->product_catalog_file;
        $file_link_skidochnik = (new ShopUrl(1))->getURL().'upload/'.$this->product_catalog_file_skidochnik;
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK, $file_link);
        Configuration::updateValue(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK_SKIDOCHNIK , $file_link_skidochnik);
    }

    protected function getExportFileDownloadLink($skidochnik = false)
    {
        if($skidochnik)
            $link = Configuration::get(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK_SKIDOCHNIK , '');
        else
            $link = Configuration::get(self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK, '');
        return $link;
    }

    public function getContent()
    {
        $html = $this->displayConfigForm();
        if (Tools::getIsset('exportProducts')) {
            $this->exportProducts();
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
        if (Tools::isSubmit('submitOptions'))
        {
            $g_cid = trim(Tools::getValue(self::FF_GOOGLE_REMARKET_CID_NAME));
            if (!$g_cid) {
                $this->adminDisplayWarning($this->l('Google Remarketing will not work without Conversion ID'));
                return $html;
            }
//            if (!$g_cid_skidochnik) {
//                $this->adminDisplayWarning($this->l('Google Remarketing will not work without Conversion ID'));
//                return $html;
//            }
            Configuration::updateValue(self::FF_GOOGLE_REMARKET_CID_NAME , $g_cid);
        }

        $cron_html = '<div class="alert alert-info">Ссылка для генерации по крону: '._PS_BASE_URL_.'/modules/'.$this->name.'/ajax.php?secret=supersecret&action=exportProducts</div>';
//        $cron_html = '<div class="alert alert-info">Ссылка для генерации файла Scidochnik по крону: '._PS_BASE_URL_.'/modules/'.$this->name.'/ajax.php?secret=supersecret&action=exportProducts</div>';

        return $html.$cron_html;
    }

    public function displayConfigForm()
    {
        $this->fields_options = array(
            'general' => array(
                'title' => $this->l('Настройка модуля'),
                'fields' =>
                    array(
                        self::FF_GOOGLE_REMARKET_CID_NAME => array(
                            'title' => $this->l('Google Conversion ID'),
                            'type' => 'text',
                            'cast' => 'strval',
                        ),
                        self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK => [
                            'title' => $this->l('Export file download link'),
                            'type' => 'text',
                            'auto_value' => false,
                            'value' => $this->getExportFileDownloadLink(false)
                        ],
                        self::FF_GOOGLE_REMARKET_EXPORT_FILE_LINK_SKIDOCHNIK =>[
                            'title' => $this->l('Export file skidochnik download link'),
                            'type' => 'text',
                            'auto_value' => false,
                            'value' => $this->getExportFileDownloadLink(true)
                        ]
                    ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'button',
                    'name' => 'submitOptions'
                ),
                'buttons' => [
                    [
                        'title' => $this->l('Export products in file'),
                        'class' => 'btn btn-info',
                        'type' => 'submit',
                        'name' => 'exportProducts'
                    ]
                ]
            )
        );

        $helper = new HelperOptions();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ),
        );
        return $helper->generateOptions($this->fields_options);
    }

    protected function getRemarketCID()
    {
        return ConfigurationCore::get(self::FF_GOOGLE_REMARKET_CID_NAME);
    }

    public function hookDisplayGoogleRemarketing($params)
    {
        require_once __DIR__.'/classes/GoogleRemarket.php';
        $products = (isset($params['products']) && is_array($params['products'])) ? $params['products'] : [];
        $page_name = $params['page_name'];

        $google_remarket_service = new GoogleRemarket($page_name, $this->context, $products);
        $remarket_info = $google_remarket_service->getAssignPageTypeInfo();

        $this->context->smarty->assign('remarket_page_type', $google_remarket_service->getPageType());
        $this->context->smarty->assign(GoogleRemarket::REMARKET_PRODUCTS_NAME, $remarket_info[GoogleRemarket::REMARKET_PRODUCTS_NAME]);
        $this->context->smarty->assign(GoogleRemarket::REMARKET_PRODUCT_TOTAL_VALUE_NAME, $remarket_info[GoogleRemarket::REMARKET_PRODUCT_TOTAL_VALUE_NAME]);
        $this->context->smarty->assign('google_conversion_id', $this->getRemarketCID());

        return $this->display(__FILE__, 'google_remarketing.tpl');
    }

    public function hookActionGenerateExportCatalogProducts($params)
    {
        if (!isset($params['secure_key']) || !$secure_key = (string)$params['secure_key']) {
            return false;
        }
        $mod_secure_key = $this->getSecureKey();
        if (strlen($secure_key) !== strlen($mod_secure_key) || $secure_key !== $mod_secure_key) {
            return false;
        }
        if (isset($params['download_file']) && $params['download_file']) {
            $this->sendExportFile();
        }
        $this->exportProducts();
    }

    protected function sendExportFile()
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public");
        header('Content-type:text/csv');
        header('Content-Disposition: attachment; filename='.$this->getExportFileName());
        readfile($this->getExportFilePath());
        return true;
    }

    protected function getSecureKey()
    {
        return $this->secure_key;
    }

    public function hookModuleRoutes()
    {
        return [
            "module-{$this->name}-export_products" => [
                'controller' => 'export_products',
                'rule' => "/modules/{$this->name}/export_products/",
                'keywords' => [],
                'params' => [
                    'fc' => 'module',
                    'module' => $this->name,
                ],
            ]
        ];
    }

}