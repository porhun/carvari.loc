<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 16.12.16
 * Time: 10:52
 */
class ffgoogleremarketingexport_productsModuleFrontController extends ModuleFrontControllerCore
{

    public function checkAccess()
    {
        if (!$secure_key = Tools::getValue('secure_key', false)) {
            return false;
        }
        return true;
    }

    public function postProcess()
    {
        Hook::exec('actionGenerateExportCatalogProducts', ['secure_key' => Tools::getValue('secure_key', false), 'download_file' => Tools::getValue('download_file', false)], ModuleCore::getModuleIdByName('ffgoogleremarketing'));
        die();
    }
}