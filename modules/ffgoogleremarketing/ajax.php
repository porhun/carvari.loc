<?php

require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');


$module_instance = new FfGoogleRemarketing();

if (Tools::getValue('secret') != 'supersecret') {
    exit();
}

if (Tools::getValue('action') == 'exportProducts') {
    $module_instance->exportProducts();
}


