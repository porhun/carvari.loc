{if $search_products}
    {assign var='marketing_products' value=$search_products}
{elseif $products}
    {assign var='marketing_products' value=$products}
{else}
    {assign var='marketing_products' value=[]}
{/if}
<script type="text/javascript">
    {literal} var google_tag_params = {}; {/literal}
    google_tag_params.dynx_pagetype = '{$remarket_page_type}';

    {if $remarket_product_ids}
        {if is_array($remarket_product_ids)}
                google_tag_params.dynx_itemid = {$remarket_product_ids|json_encode}
        {else}
            google_tag_params.dynx_itemid = '{$remarket_product_ids}';
        {/if}
    {/if}

     {if $remarket_products_total_value}
        {if is_array($remarket_products_total_value)}
            google_tag_params.dynx_totalvalue = {$remarket_products_total_value|json_encode};
        {else}
            google_tag_params.dynx_totalvalue = '{$remarket_products_total_value}';
        {/if}
     {/if}
</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = {if $google_conversion_id}{$google_conversion_id}{else}''{/if};
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/{$google_conversion_id}/?value=0&guid=ON&script=0"/>
    </div>
</noscript>