<span class="js_menu_item menu_burger">
    <div></div>
    <div></div>
    <div></div>
    <ul style="display: none;">
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/carvaricard/">{l s='Carvari card' mod='ffcarvarimenu'}</a></li>*}
        {if isset($accessories_category)}
            <li class="accessories_menu_item">
                <a href="{$accessories_category.link}">{$accessories_category.name}</a>
                <ul class="accessories_dop_menu">
                    {foreach from=$menuCategTree.children[$accessories_category.id_category]['children'] item=acCategory}
                        <li class="accessories_dop_menu__point">
                            <a href="{$acCategory.link}">{$acCategory.name}</a>
                        </li>
                    {/foreach}
                </ul>
            </li>
        {/if}
        {*<li class="menu_burger__item--hidden"><a href="{if $active_url_lang}{$active_url_lang}{/if}/novinki-3042/">{l s='Novelty' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/novinki-3042/">{l s='Novelty' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/pro-magazin-6/">{l s='About shop' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/club-lcarvari-15/">{l s='Club LCARVARI' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/stores/">{l s='Contacts' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/oplata-7/">{l s='Payment' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/credit-18/">{l s='Кредит' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/delivery-1/">{l s='Delivery' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/garantiya-8/">{l s='Guaranty' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/obmin-ta-povernennya-9/">{l s='Exchange and return' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/blog/">{l s='Блог' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/akcii-3041">{l s='Shares' mod='ffcarvarimenu'}</a></li>*}
        {*<li class="menu_burger__item--hidden"><a href="{if $active_url_lang}{$active_url_lang}{/if}/akcii-3041">{l s='Shares' mod='ffcarvarimenu'}</a></li>*}
        {*<li><a href="/content/opt-12">{l s='Opt' mod='ffcarvarimenu'}</a></li>
    	<li><a href="/content/franchising-13">{l s='Franchising' mod='ffcarvarimenu'}</a></li>*}

        {foreach from=$contextmenu item=menu}
            <li><a href="{if $active_url_lang}{$active_url_lang}{/if}{$menu.link}">{$menu.title}</a></li>
        {/foreach}
    </ul>
</span>
<div class="menu">
    {foreach from=$menuCategTree.children item=menuCategory}
        <div class="menu_item {if $menuCategory.children|count}js_menu_item{/if}{if $menuCategory.name=='Новинки' || $menuCategory.name=='Аксесуари' || $menuCategory.name=='Акцii' || $menuCategory.name=='Аксессуары'} menu_item--hidden{/if}">
            {if $menuCategory.name=='Аксесуари' ||  $menuCategory.name=='Аксессуары'}
                <div class="accessories_menu_item">{$menuCategory.name}</div>
            {elseif !$menuCategory.children|count}
                <a href="{$menuCategory.link}">{$menuCategory.name}</a>
            {else}
                {$menuCategory.name}
            {/if}
            <ul style="display: none">
                {if $menuCategory.name|in_array:['Жiнкам', 'Жінкам', 'Женщинам', 'Чоловікам', 'Чоловiкам', 'Мужчинам']}<li><a href="{$menuCategory.link}">{l s='Всі товари' mod='ffcarvarimenu'}</a></li>{/if}
                {foreach from=$menuCategory.children item=menuSub}
                    {if $menuSub.active}
                        <li><a {if $menuSub.name == 'Спеціальна пропозиція'} class="outline" {/if} href="{$menuSub.link}">{$menuSub.name}</a></li>
                    {/if}
                {/foreach}
                {foreach from=$menuCategory.children item=menuSub}
                    {foreach from=$menuSub.children item=menuSubcategory name=menuSubForeach}
                        <li><a href="{$menuSubcategory.link}">{$menuSubcategory.name}</a></li>
                    {/foreach}
                {/foreach}

                {*{if $menuCategory.thumb_link}
                    <div class="dd_menu__image_holder">
                        <a href="{$menuCategory.thumb_url}" class="dropdown_menu__image_link">
                            <img src="{$menuCategory.thumb_link}" class="dropdown_menu__image">
                        </a>
                    </div>
                {/if}*}
            </ul>
        </div>
    {/foreach}
</div>


{if $mobile_device}
    <a href="{if $active_url_lang}{$active_url_lang}{/if}/order/" class="cart">
		<span class="ajax_cart_quantity">
			{if $page_name=='module-advancedcheckout-confirm'}0{else}{$cart_qties}{/if}
		</span>
    </a>
    <div class="mobile_search_holder js_mobile_search_holder">
        <div class="mobile_search js_mobile_search_open"></div>
        <div class="mobile_search_form">
		    {hook h='displayMobileSearch' mod='blocksearch'}
        </div>
        <div class="menu_burger active js_mobile_search_close">
            <div></div>
            <div></div>
            <div></div>
        </div>
    </div>

{/if}
