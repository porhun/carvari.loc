<li class="burger">
    <span class="js_menu_item menu_burger">
        <div></div>
        <div></div>
        <div></div>
    </span>
    <div class="js_menu_cont" style="display: none;">
        {*<ul class="dd_menu_list">*}
            {*{if $logged}*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/pro-magazin-6/">{l s='About shop' mod='ffcarvarimenu'}</a></li>*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/club-lcarvari-15/">{l s='Club LCARVARI' mod='ffcarvarimenu'}</a></li>*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/carvaricard/">{l s='Carvari card' mod='ffcarvarimenu'}</a></li>*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/stores/">{l s='Contacts' mod='ffcarvarimenu'}</a></li>*}
            {*{else}*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/pro-magazin-6/">{l s='About shop' mod='ffcarvarimenu'}</a></li>*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/club-lcarvari-15/">{l s='Club LCARVARI' mod='ffcarvarimenu'}</a></li>*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/stores/">{l s='Contacts' mod='ffcarvarimenu'}</a></li>*}
            {*{/if}*}
        {*</ul>*}
        {*<ul class="dd_menu_list">*}
            {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/oplata-7/">{l s='Payment' mod='ffcarvarimenu'}</a></li>*}
            {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/credit-18/">{l s='Кредит' mod='ffcarvarimenu'}</a></li>*}
            {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/garantiya-8/">{l s='Guaranty' mod='ffcarvarimenu'}</a></li>*}
            {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/delivery-1/">{l s='Delivery' mod='ffcarvarimenu'}</a></li>*}
            {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/obmin-ta-povernennya-9/">{l s='Exchange and return' mod='ffcarvarimenu'}</a></li>*}
        {*</ul>*}
        {*<ul class="dd_menu_list">*}
            {*{if !$logged}*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/obmin-ta-povernennya-9/">{l s='Exchange and return' mod='ffcarvarimenu'}</a></li>*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/akciya-2-plyus-1-16/">{l s='Акции' mod='ffcarvarimenu'}</a></li>*}
            {*{else}*}
                {*<li><a href="{if $active_url_lang}{$active_url_lang}{/if}/content/akciya-2-plyus-1-16/">{l s='Акцii' mod='ffcarvarimenu'}</a></li>*}
            {*{/if}*}
            {*<li><a href="/content/franchising-13">{l s='Franchising' mod='ffcarvarimenu'}</a></li>*}
            {*<li><a href="/content/opt-12">{l s='Opt' mod='ffcarvarimenu'}</a></li>*}
        {*</ul>*}

        {$iter = 0}
        <ul class="dd_menu_list">
        {foreach from=$contextmenu item=contextMenu}
            {$iter = $iter + 1}
            <li><a href="{if $active_url_lang}{$active_url_lang}{/if}{$contextMenu.link}">{$contextMenu.title}</a></li>
            {if $iter % 4 == 0}
        </ul>
        <ul class="dd_menu_list">
            {/if}
        {/foreach}

    </div>
</li>
{foreach from=$menuCategTree.children item=menuCategory}
    <li>
        {*
        @TODO Объяснение этого мега-костыля:
        -- Под-меню "Все взуття" сейчас должно выводиться только для двух этих категорий, остальные остаются прямыми ссылками на категории кроме аксессуаров.
        -- дублирование названий в массиве по той причине, что "менеджерам" лень было
         поставить укр. расскладку и они вместо украинской "і" писали английскую, поэтому проверка в коде не проходила.
         ¯\_(ツ)_/¯
        *}
        {if !$menuCategory.name|in_array:['Жiнкам', 'Жінкам', 'Женщинам', 'Чоловікам', 'Чоловiкам', 'Мужчинам', 'Аксесуари', 'Аксессуары']}
            <a href="{$menuCategory.link}">{$menuCategory.name}</a>
        {else}
            <span class="js_menu_item">{$menuCategory.name}</span>
        {/if}
            <div class="js_menu_cont" style="display: none;">
                <div class="menu_inner_wrap">
                    <ul class="dd_menu_list">
                        {if $menuCategory.name|in_array:['Жiнкам', 'Жінкам', 'Женщинам', 'Чоловікам', 'Чоловiкам', 'Мужчинам']}
                            <li><a href="{$menuCategory.link}">{l s='Всі товари' mod='ffcarvarimenu'}</a></li>
                        {/if}
                        {$iter = 0}
                        {foreach from=$menuCategory.children item=menuSub}
                        {if $menuSub.active}
                        {$iter = $iter + 1}
                        <li><a {if $menuSub.name == 'Спеціальна пропозиція'} class="outline" {/if} href="{$menuSub.link}">{$menuSub.name}</a></li>
                        {if $iter % 5 == 0}
                    </ul>
                    <ul class="dd_menu_list">
                        {/if}
                        {/if}
                        {/foreach}
                        {*</ul>*}
                        {*<ul class="dd_menu_list">*}
                        {foreach from=$menuCategory.children item=menuSub}

                        {if count($menuSub.children)}
                        {*{$iter = 0}*}
                        {foreach from=$menuSub.children item=menuSubcategory name=menuSubForeach}
                        {$iter = $iter + 1}
                        <li><a href="{$menuSubcategory.link}">{$menuSubcategory.name}</a></li>
                        {if $iter % 5 == 0}
                    </ul>
                    <ul class="dd_menu_list">
                        {/if}
                        {/foreach}
                        {*</ul>*}
                        {*<ul class="dd_menu_list">*}
                        {/if}
                        {/foreach}

                    </ul>
                </div>
                {if $menuCategory.thumb_link}
                    <div class="dd_menu__image_holder">
                        <a href="{if $active_url_lang}{$active_url_lang}{/if}{$menuCategory.thumb_url}" class="dropdown_menu__image_link">
                            <img src="{$menuCategory.thumb_link}" class="dropdown_menu__image">
                        </a>
                    </div>
                {/if}
            </div>
    </li>
{/foreach}
<li>
    <span><a href="{if $active_url_lang}{$active_url_lang}{/if}/{Configuration::get('PH_BLOG_SLUG')}/">{l s='Trends' mod='ffcarvarimenu'}</a></span>
</li>
{*<li>
    <span><a href="/carvaricard">{l s='My profile' mod='ffcarvarimenu'}</a></span>
</li>*}
