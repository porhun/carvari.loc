<?php

class TopMenu extends ObjectModel
{
    const INACTIVE = 0;
    const ACTIVE = 1;

    public $id;
    public $id_menu;
    public $title;
    public $link;
    public $status;
    public $is_logged;
    public $is_footer;
    public $position = 0;

    public static $definition = array(
        'table'              => 'ffcarvarimenu',
        'primary'            => 'id_menu',
        'multilang'          => true,
        'fields'             => array(
            'link'           => array('type' => self::TYPE_STRING),
            'status'         => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'is_logged'      => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'is_footer'      => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
            'position'       => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),

            // Lang fields
            'title'          => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 255),
        ),
    );

    public static function loadList($status = false)
    {
        $order_by = 'position';
        $order_way = 'ASC';
        $id_lang = (int)Context::getContext()->language->id;

        $qb = new DbQuery();
        $qb->select('*');
        $qb->from('ffcarvarimenu', 'm');
        $qb->leftJoin('ffcarvarimenu_lang', 'ml', 'ml.'.self::$definition['primary'].' = m.'.self::$definition['primary'].' AND ml.id_lang = '.$id_lang);
        $qb->groupBy( 'm.'.self::$definition['primary'].'');
        $qb->orderBy($order_by.' '.$order_way);
        if ($status) {
            $qb->where('m.status = 1');
        }

        $cache_id = 'TopMenu::loadList_'.md5($qb);
        if (!Cache::isStored($cache_id)) {
            $rules = Db::getInstance()->executeS($qb);
            Cache::store($cache_id, $rules);
        }
        $rules = Cache::retrieve($cache_id);

        return $rules;
    }

    public static function getMenu($logged = false, $footer = false)
    {
        $order_by = 'position';
        $order_way = 'ASC';
        $id_lang = (int)Context::getContext()->language->id;

        $sql = '
		SELECT *
		FROM `'._DB_PREFIX_.self::$definition['table'].'` m
		LEFT JOIN `'._DB_PREFIX_.self::$definition['table'].'_lang` ml
		ON ml.'.self::$definition['primary'].' = m.'.self::$definition['primary'].'
		WHERE m.`status` = 1
        '.($logged ? '' : ' NOT IN (m.`is_logged` = 1)').'
        '.($footer ? ' AND m.`is_footer` = 1' : '').'
        AND `id_lang` = '.(int)$id_lang.'
        ORDER BY '.$order_by.' '.$order_way;

        return Db::getInstance()->executeS($sql);
    }

}