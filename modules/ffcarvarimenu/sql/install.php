<?php

$sql = [];

$sql[] = 'CREATE TABLE IF NOT EXISTS '.bqSQL(_DB_PREFIX_).'ffcarvarimenu(
    `id_menu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    `link`    VARCHAR(255) NOT NULL,
    `status`  tinyint(1) NOT NULL DEFAULT 0,
    `is_logged`  tinyint(1) NOT NULL DEFAULT 0,
    `is_footer`  tinyint(1) NOT NULL DEFAULT 0,
    `position` int(10) UNSIGNED NOT NULL DEFAULT 0,
    PRIMARY KEY(`id_menu`)
) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS '.bqSQL(_DB_PREFIX_).'ffcarvarimenu_lang(
    `id_menu`  int(11) UNSIGNED NOT NULL,
    `id_lang`  int(11) UNSIGNED NOT NULL,
    `title`    VARCHAR(255) NOT NULL,
    PRIMARY KEY(`id_menu`, `id_lang`)
) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';


return $sql;