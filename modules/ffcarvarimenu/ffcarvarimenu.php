<?php
if (!defined('_PS_VERSION_'))
    exit;

require_once __DIR__.'/models/TopMenu.php';

class Ffcarvarimenu extends Module
{
    const INSTALL_SQL_FILE   = 'sql/install.php';
    const UNINSTALL_SQL_FILE = 'sql/uninstall.php';
    private $html_return = '';

    public function __construct()
    {
        $this->name = 'ffcarvarimenu';
        $this->tab = 'social_networks';
        $this->version = '1.0.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Carvari Menu - верхнее и нижнее меню магазина Carvari');
        $this->description = $this->l('Carvari Menu - верхнее и нижнее меню магазина Carvari.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->menu_tpl_name = 'category_menu.tpl';
        $this->menu_tpl_name_mobile = 'category_menu_mobile.tpl';

    }

    public function install($keep = false)
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !( $keep ? true : $this->createTables()) ||
            !$this->registerHook('actionCategoryUpdate') ||
            !$this->registerHook('displayTop') ||
            !$this->registerHook('displayMobileTop') ||
            !$this->registerHook('displayFooter')
            )
            return false;

        $this->cleanClassCache();

        return true;
    }

    public function uninstall($keep = false)
    {
        if (!parent::uninstall() ||
            !( $keep ? true : $this->deleteTables()))
            return false;
        return true;
    }

    public function reset()
    {
        if (!$this->uninstall(true))
            return false;
        if (!$this->install(true))
            return false;
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache)) {
            unlink($class_cache);
        }
    }

    protected function createTables()
    {
        $sql = $this->getTableSql(self::INSTALL_SQL_FILE);
        return $this->performSql($sql);
    }

    protected function deleteTables()
    {
        $sql = $this->getTableSql(self::UNINSTALL_SQL_FILE);
        return $this->performSql($sql);
    }

    protected function getTableSql($sqlFile)
    {
        $sql = require (dirname(__FILE__).'/'.$sqlFile);
        if (!$sql) {
            return [];
        }
        return $sql;
    }

    protected function performSql(array $sqlQueries)
    {
        $dbInstance = Db::getInstance();
        foreach ($sqlQueries as $query) {
            if (!$dbInstance->execute($query)) {
                return false;
            }
        }
        return true;
    }

    public function getContent()
    {
        $this->postProcess();

        if (Tools::getIsset('updateffcarvarimenu') || Tools::getIsset('addffcarvarimenu'))
            return $this->displayForm();

        $this->html_return = '<script type="text/javascript">currentIndex=\''.
            $this->context->link->getAdminLink(
                'AdminModules',
                false
            ).'&configure='.$this->name.'&tab_module='.$this->tab.
            '&module_name='.$this->name.'\'</script>';

        $this->html_return .= $this->generateList();

        return $this->html_return;
    }

    protected function displayForm()
    {
        $menu_item = new TopMenu((int)Tools::getValue('id_menu'));

        $fields_form = array('');

        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Edit menu item'),
            ),
            'input' => array(
                array(
                    'type' => 'hidden',
                    'name' => 'menu[id_menu]',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Title'),
                    'name' => 'title',
                    'col' => 4,
                    'required' => false,
                    'lang' => true,
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Link'),
                    'name' => 'menu[link]',
                    'required' => false,
                    'col' => 4,
                    'desc' => 'Впереди ссылки обязательно ставтиь слэш - "/"'
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable'),
                    'name' => 'menu[status]',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show link'),
                    'desc' => $this->l('The show was only logged user'),
                    'name' => 'menu[is_logged]',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show link in footer'),
                    'desc' => $this->l('The show link only footer'),
                    'name' => 'menu[is_footer]',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
            ),

            'submit' => array(
                'title' => $this->l('Save'),
                'name' => 'import'
            ),

        );

        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;

        // Language
        $languages = Language::getLanguages(false);
        foreach ($languages as $k => $language) {
            $languages[$k]['is_default'] = (int)($language['id_lang'] == $default_lang);
        }
        $helper->languages = $languages;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title = $this->l('Edit Menu item');
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->show_cancel_button = true;

        $helper->submit_action = 'submitMenu';
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name .
                        '&token=' . Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        $helper->fields_value['menu[id_menu]'] = $menu_item->id_menu;
        $helper->fields_value['title'] = $menu_item->title;
        $helper->fields_value['menu[link]'] = $menu_item->link;
        $helper->fields_value['menu[status]'] = $menu_item->status;
        $helper->fields_value['menu[is_logged]'] = $menu_item->is_logged;
        $helper->fields_value['menu[is_footer]'] = $menu_item->is_footer;

        return $helper->generateForm($fields_form);

    }

    private function generateList()
    {
        $fields_list = array(
            'id_menu' => array(
                'title' => $this->l('Id'),
                'class' => 'fixed-width-xs',
                'type' => 'text',
                'search' => false,
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'type' => 'text',
                'search' => false,
            ),
            'link' => array(
                'title' => $this->l('Link'),
                'width' => 140,
                'type' => 'text',
                'search' => false,
            ),
            'position' => array(
                'title' => $this->l('Position'),
                'position' => 'position',
                'align' => 'center',
                'class' => 'fixed-width-xs',
                'search' => false,
            ),
            'status' => array(
                'title' => $this->l('Status'),
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-sm',
                'search' => false,
                'ajax' => true,
            ),
            'is_logged' => array(
                'title' => $this->l('Only logged user'),
                'align' => 'center',
                'active' => 'is_logged',
                'type' => 'bool',
                'class' => 'fixed-width-sm',
                'search' => false,
                'ajax' => true,
            ),
            'is_footer' => array(
                'title' => $this->l('Only footer'),
                'align' => 'center',
                'active' => 'is_footer',
                'type' => 'bool',
                'class' => 'fixed-width-sm',
                'search' => false,
                'ajax' => true,
            ),
        );

        $fields_values = TopMenu::loadList();

        $helper = new HelperList();

        $helper->module = $this;
        $helper->identifier = 'id_menu';
        $helper->title = $this->l('Top menu items');
        $helper->table = 'ffcarvarimenu';
        $helper->shopLinkType = '';
        $helper->listTotal = count($fields_values);
        $helper->simple_header = false;
        $helper->actions = array(
            'edit',
            'delete',
        );

        $helper->position_identifier = 'id_menu';
        $helper->orderBy =  'position';

        $helper->show_toolbar = true;
        $helper->toolbar_btn['new'] = array(
            'href' => $this->context->link->getAdminLink('AdminModules').
                '&configure='.$this->name.'&module_name='.$this->name.'&addffcarvarimenu&token=' . Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Add New Link', null, null, false),
        );

        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        $helper->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );
        $helper->force_show_bulk_actions = true;

        return $helper->generateList($fields_values, $fields_list);
    }

    protected function postProcess()
    {
        $redirect = 0;

        if (Tools::getValue('action') == 'updatePositions'){
            $this->ajaxProcessLinkPositions();
        }

        if (Tools::getIsset('deleteffcarvarimenu') && ($id_menu = (int)Tools::getValue('id_menu', 0))) {
            $redirect_item = new TopMenu($id_menu);
            if ($redirect_item) {
                $this->html_return = (!$redirect_item->delete()) ? $this->displayError($this->l('Item was not deleted')) : $this->displayConfirmation($this->l('Item was deleted'));
            }
        }

        if (Tools::isSubmit('submitMenu')){

            $menu = Tools::getValue('menu');
            $menu_item = new TopMenu((int)$menu['id_menu']);

            /* Multilingual fields */
            $class_vars = get_class_vars(get_class($menu_item));
            $fields = array();
            if (isset($class_vars['definition']['fields'])) {
                $fields = $class_vars['definition']['fields'];
            }

            foreach ($fields as $field => $params) {
                if (array_key_exists('lang', $params) && $params['lang']) {
                    foreach (Language::getIDs(false) as $id_lang) {
                        if (Tools::isSubmit($field.'_'.(int)$id_lang)) {
                            $menu_item->{$field}[(int)$id_lang] = Tools::getValue($field.'_'.(int)$id_lang);
                        }
                    }
                }
            }

            $menu_item->link = pSQL($menu['link']);
            $menu_item->status = pSQL($menu['status']);
            $menu_item->is_logged = pSQL($menu['is_logged']);
            $menu_item->is_footer = pSQL($menu['is_footer']);

            $menu_item->save();

            $redirect = 1;
            $this->hookActionCategoryUpdate();
        }

        if (Tools::getValue('action') == 'statusffcarvarimenu') {
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffcarvarimenu` SET `status` = abs(`status` - 1) WHERE `id_menu`='.(int)Tools::getValue('id_menu'));
            $this->hookActionCategoryUpdate();
            die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully'))));
        }
        if (Tools::getValue('action') == 'is_loggedffcarvarimenu') {
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffcarvarimenu` SET `is_logged` = abs(`is_logged` - 1) WHERE `id_menu`='.(int)Tools::getValue('id_menu'));
            $this->hookActionCategoryUpdate();
            die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully'))));
        }
        if (Tools::getValue('action') == 'is_footerffcarvarimenu') {
            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffcarvarimenu` SET `is_footer` = abs(`is_footer` - 1) WHERE `id_menu`='.(int)Tools::getValue('id_menu'));
            $this->hookActionCategoryUpdate();
            die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully'))));
        }

        if($redirect)
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false) . '&configure=' .
                $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'));
    }

    public function ajaxProcessLinkPositions()
    {
        $positions = Tools::getValue('menu');

        $new_positions = array();

        if (is_array($positions)) {
            foreach ($positions as $position => $value) {
                $pos = explode('_', $value);
                if (isset($pos[2])) {
                    Db::getInstance()->update('ffcarvarimenu', array('position' => $position), 'id_menu='.(int)$pos[2]);
                    $new_positions[$pos[2]] = $position;
                }
            }
        }

        $this->hookActionCategoryUpdate();
        die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The positions has been updated successfully'))));
    }

    public function hookActionCategoryUpdate()
    {
        $this->_clearCache($this->menu_tpl_name);
        $this->_clearCache($this->menu_tpl_name_mobile);
    }

    public function hookDisplayTop()
    {
        $menu = '<ul class="menu_list mobile_flex_menu">';
        $menu .= $this->showCatMenu();
        $menu .= '</ul>';
        return $menu;
    }

    public function hookDisplayMobileTop()
    {
//        d($this->context->getMobileDevice());
        return $this->showCatMenu();

    }

    public  function hookDisplayFooter()
    {
        $this->smarty->assign('footermenu', TopMenu::getMenu(false, true));

        $display = $this->display(__FILE__, 'footer_menu.tpl', $this->getCacheId('footer'));

        return $display;
    }

    private function showCatMenu() {

        $menu_tpl_name = $this->menu_tpl_name;
        $maxdepth = 4;

        if ($this->context->getMobileDevice())
            $menu_tpl_name = $this->menu_tpl_name_mobile;


        if (!$this->isCached($menu_tpl_name, $this->getCacheId( 'top|'.$maxdepth ))) {

            $groups = implode(', ', Customer::getGroupsStatic((int)$this->context->customer->id));
            if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT DISTINCT c.id_parent, c.id_category, cl.name, cl.description, cl.link_rewrite , c.level_depth, c.`active`, c.`thumb_url`
				FROM `' . _DB_PREFIX_ . 'category` c
				' . Shop::addSqlAssociation('category', 'c') . '
				LEFT JOIN `' . _DB_PREFIX_ . 'category_lang` cl ON (c.`id_category` = cl.`id_category` AND cl.`id_lang` = ' . (int)$this->context->language->id . Shop::addSqlRestrictionOnLang('cl') . ')
				LEFT JOIN `' . _DB_PREFIX_ . 'category_group` cg ON (cg.`id_category` = c.`id_category`)
				WHERE (c.`active` = 1 OR c.`level_depth` = 3 OR c.`id_category` = ' . (int)Configuration::get('PS_ROOT_CATEGORY') . ')
				' . ((int)($maxdepth) != 0 ? ' AND `level_depth` <= ' . (int)($maxdepth) : '') . '
				AND cg.`id_group` IN (' . pSQL($groups) . ')
				ORDER BY `level_depth` ASC, category_shop.`position` ASC')
            )
                return;


//        d($result);
            $resultParents = array();
            $resultIds = array();

            foreach ($result as &$row) {
                $resultParents[$row['id_parent']][] = &$row;
                $resultIds[$row['id_category']] = &$row;
            }

            $menuCategTree = $this->getTree($resultParents, $resultIds, Configuration::get('BLOCK_CATEG_MAX_DEPTH'));
            unset($resultParents, $resultIds);
            $this->smarty->assign('menuCategTree', $menuCategTree);
//            d($menuCategTree);

            $default_language_id = (int)Configuration::get('PS_LANG_DEFAULT');
            $id_lang = Language::getIdByIso('uk');
            if (!Validate::isUnsignedId($id_lang))
                $id_lang = $default_language_id;
            $accessories_category = Category::searchByNameAndParentCategoryId($id_lang, 'Аксесуари', Configuration::get('PS_HOME_CATEGORY'));
            if($accessories_category) {
                $accessories_category['link'] = $this->context->link->getCategoryLink($accessories_category['id_category'], $accessories_category['link_rewrite']);
                $this->smarty->assign('accessories_category', $accessories_category);
            }

        }

        if ($this->context->customer->isLogged(true)){
            $contextmenu = TopMenu::getMenu(true, false);
        } else{
            $contextmenu = TopMenu::getMenu(false, false);
        }

        $this->smarty->assign('contextmenu', $contextmenu);

//        $display = $this->display(__FILE__, $menu_tpl_name);
        $display = $this->display(__FILE__, $menu_tpl_name, $this->getCacheId( 'top|'.$maxdepth ));

        return $display;

//        return $this->context->smarty->fetch(__DIR__.'/views/templates/category_menu.tpl');
    }

    public function getTree($resultParents, $resultIds, $maxDepth, $id_category = null, $currentDepth = 0)
    {
        $limits_array = array('vzuttya' => 0, 'aksesuari' => 0, 'sumki' => 0);

        if (is_null($id_category))
            $id_category = $this->context->shop->getCategory();
        $children = array();
        if (isset($resultParents[$id_category]) && count($resultParents[$id_category]) && ($maxDepth == 0 || $currentDepth < $maxDepth)){
            foreach ($resultParents[$id_category] as $subcat){
                $children[$subcat['id_category']] = $this->getTree($resultParents, $resultIds, $maxDepth, $subcat['id_category'], $currentDepth + 1);
            }
        }

        if (isset($resultIds[$id_category]))
        {
            $link = $this->context->link->getCategoryLink($id_category, $resultIds[$id_category]['link_rewrite']);
            $name = $resultIds[$id_category]['name'];
            $desc = $resultIds[$id_category]['description'];
            $level_depth = $resultIds[$id_category]['level_depth'];
            $thumb_url = $resultIds[$id_category]['thumb_url'];
            $active = $resultIds[$id_category]['active'];
        }
        else
            $link = $name = $desc = $level_depth = '';

        if ( isset($resultIds[$id_category]['link_rewrite']) && isset($limits_array[$resultIds[$id_category]['link_rewrite']]) && $limit = $limits_array[$resultIds[$id_category]['link_rewrite']] ) {
            $children = array_slice($children, 0, $limit);
        } else {
//            if ($currentDepth==2){
//                $children = array();
//                $link = $name = $desc = '';
//            }
        }

        if ((int)$level_depth == 2) {
            $files = scandir(_PS_CAT_IMG_DIR_);

//            if ($id_category==3013) {
//                d(preg_grep('/^'.$id_category.'(-[0-9])?_thumb.jpg/i', $files));
//            }

            if (count(preg_grep('/^'.$id_category.'(-[0-9])?_thumb.(jpg|png)/i', $files)) > 0) {

                foreach ($files as $file) {
                    if (preg_match('/^'.$id_category.'(-[0-9])?_thumb.(jpg|png)/i', $file) === 1) {
                        $thumb_link = $this->context->link->getMediaLink(_THEME_CAT_DIR_.$file);

                    }
                }

            }
        }

        $return = array(
            'id' => $id_category,
            'link' => $link,
            'thumb_link' => isset($thumb_link) ? $thumb_link : false,
            'thumb_url' => isset($thumb_url) ? $thumb_url : false,
            'name' => $name,
//            'desc'=> $desc,
            'active'=> isset($active) ? $active : false,
            'level_depth'=> $level_depth,
            'children' => $children
        );
        return $return;
    }

    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;

        $categories_cnt = Db::getInstance()->getValue('SELECT COUNT(*) FROM `' . _DB_PREFIX_ . 'category` WHERE active=1');

        return $cache_id.'|'.$categories_cnt;
    }
}