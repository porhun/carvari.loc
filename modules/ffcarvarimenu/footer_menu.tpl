<ul class="footer_menu__list">
    {foreach from=$footermenu item=menu}
    <li><a href="{if $active_url_lang}{$active_url_lang}{/if}{$menu.link}" class="footer_menu__item">{$menu.title}</a></li>
    {/foreach}
</ul>
