<?php
/**
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

if (!defined('_PS_VERSION_'))
    exit;
include_once(dirname(__FILE__) . '/PtTab.php');

class PtTabs extends Module
{
    protected $categories = array();
    protected $error = false;
	private $html;
    private $defaultHook = array('displayStatics5', 'displayStatics6', 'displayStatics14');

    public function __construct()
    {
        $this->name = 'pttabs';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'PathThemes';
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('PT Tabs');
        $this->description = $this->l('Display products on tabs');
        $this->confirmUninstall = $this->l('Are you sure?');
    }

	/**
	 * @see Module::install()
	 */
    public function install()
    {
        if (parent::install() == false OR !$this->registerHook('displayHeader') OR !$this->registerHook('actionShopDataDuplication'))
            return false;
        foreach ($this->defaultHook AS $hook) {
            if (!$this->registerHook($hook))
                return false;
        }
		$res = $this->createTables();
        if (!$res)
            return false;
		$this->installSamples();
        return true;
    }
	
	/**
	 * @see Module::uninstall()
	 */
    public function uninstall()
    {
        if (parent::uninstall() == false)
            return false;
        if (!Db::getInstance()->Execute('
			DROP TABLE ' . _DB_PREFIX_ . 'pttabs') OR !Db::getInstance()->Execute('
			DROP TABLE ' . _DB_PREFIX_ . 'pttabs_shop') OR !Db::getInstance()->Execute('
			DROP TABLE ' . _DB_PREFIX_ . 'pttabs_lang'))
            return false;
        foreach ($this->defaultHook as $hook) {
            $this->clearCache();
        }
        return true;
    }
	
	/**
	 * Creates tables
	 */
	protected function createTables()
	{
        $res = Db::getInstance()->Execute('
			CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'pttabs (
			`id_module` int(10) unsigned NOT NULL AUTO_INCREMENT, 
			`hook` int(10) unsigned, 
			`params` text NOT NULL DEFAULT \'\' ,
			`active` tinyint(1) NOT NULL DEFAULT \'1\', 
			`ordering` int(10) unsigned NOT NULL, 
			PRIMARY KEY (`id_module`)) ENGINE=InnoDB default CHARSET=utf8');
			
        $res = Db::getInstance()->Execute('
			CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'pttabs_shop (
			`id_module` int(10) unsigned NOT NULL,
			`id_shop` int(10) unsigned NOT NULL,
			`active` tinyint(1) NOT NULL DEFAULT \'1\', 
			PRIMARY KEY (`id_module`,`id_shop`)) ENGINE=InnoDB default CHARSET=utf8'
		);
        $res = Db::getInstance()->Execute('
			CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'pttabs_lang (
			`id_module` int(10) unsigned NOT NULL, 
			`id_lang` int(10) unsigned NOT NULL,
			`title_module` varchar(255) NOT NULL DEFAULT \'\', 
			PRIMARY KEY (`id_module`,`id_lang`)) ENGINE=InnoDB default CHARSET=utf8'
		);
		return $res;
	}	
	
	public function installSamples()
	{
		$image_pro_types = ImageType::getImagesTypes ('products');
		$product_type = array_shift($image_pro_types);
		$datas = array(
			array(	
				'active' => 1,			
				'title_module' => 'New Arrivals',
				'hook' => Hook::getIdByName('displayStatics5'),
				'target' => '_self',
				'col_lg' => 6,
				'col_md' => 4,
				'col_sm' => 3,
				'col_xs' => 2,
				'tabs_type' =>'categories',	
				'catids' => 'all',
				'product_limit' => 20,
				'category_preload' =>'',
				'sort_by' => 'name',
				'sort_direction' => 'ASC',
				'field_select' =>'date_add',
				'field_preload' =>'date_add',
				'field_direction' => 'ASC',
				'show_tab_all' => 0,
				'tab_name_maxlength' => 25,
				'cat_field_sort_by' => 'name',
				'cat_field_direction' => 'ASC',
				'image_size' => isset($product_type['name']) ? $product_type['name'] : 'none' ,
				'show_name' => 1,
				'name_maxlength' => 50,
				'show_description' => 0,
				'description_maxlength' => 50,
				'show_price' => 1,
				'show_addtocart' => 1,
				'show_wishlist' => 1,
				'show_compare' => 1,
				'show_reviews' => 1,
				'show_new' => 1,
				'show_sale' => 1,
				'step' => 1,
				'auto' => 0,
				'pause' => 1,
				'duration' => 250,
				'interval' => 5000,
				'show_page' => 0,
				'show_nav' => 1
			),
			array(	
				'active' => 1,			
				'title_module' => 'Best Sellers',
				'hook' => Hook::getIdByName('displayStatics6'),
				'target' => '_self',
				'col_lg' => 6,
				'col_md' => 4,
				'col_sm' => 3,
				'col_xs' => 2,
				'tabs_type' =>'categories',	
				'catids' => 'all',
				'product_limit' => 20,
				'category_preload' =>'',
				'sort_by' => 'name',
				'sort_direction' => 'ASC',
				'field_select' =>'date_add',
				'field_preload' =>'date_add',
				'field_direction' => 'ASC',
				'show_tab_all' => 0,
				'tab_name_maxlength' => 25,
				'cat_field_sort_by' => 'name',
				'cat_field_direction' => 'ASC',
				'image_size' => isset($product_type['name']) ? $product_type['name'] : 'none' ,
				'show_name' => 1,
				'name_maxlength' => 50,
				'show_description' => 0,
				'description_maxlength' => 50,
				'show_price' => 1,
				'show_addtocart' => 1,
				'show_wishlist' => 1,
				'show_compare' => 1,
				'show_reviews' => 1,
				'show_new' => 1,
				'show_sale' => 1,
				'step' => 1,
				'auto' => 0,
				'pause' => 1,
				'duration' => 250,
				'interval' => 5000,
				'show_page' => 0,
				'show_nav' => 1
			),
			array(	
				'active' => 1,			
				'title_module' => 'New Arrivals',
				'hook' => Hook::getIdByName('displayStatics14'),
				'target' => '_self',
				'col_lg' => 4,
				'col_md' => 4,
				'col_sm' => 2,
				'col_xs' => 2,
				'tabs_type' =>'categories',	
				'catids' => 'all',
				'product_limit' => 20,
				'category_preload' =>'',
				'sort_by' => 'name',
				'sort_direction' => 'ASC',
				'field_select' =>'date_add',
				'field_preload' =>'date_add',
				'field_direction' => 'ASC',
				'show_tab_all' => 1,
				'tab_name_maxlength' => 25,
				'cat_field_sort_by' => 'name',
				'cat_field_direction' => 'ASC',
				'image_size' => isset($product_type['name']) ? $product_type['name'] : 'none' ,
				'show_name' => 1,
				'name_maxlength' => 50,
				'show_description' => 0,
				'description_maxlength' => 50,
				'show_price' => 1,
				'show_addtocart' => 1,
				'show_wishlist' => 1,
				'show_compare' => 1,
				'show_reviews' => 1,
				'show_new' => 1,
				'show_sale' => 1,
				'step' => 1,
				'auto' => 0,
				'pause' => 1,
				'duration' => 250,
				'interval' => 5000,
				'show_page' => 0,
				'show_nav' => 1
			)
		);

		$return = true;
		foreach ($datas as $i => $data)
		{
			$pttabs = new PtTab();
			$pttabs->hook = $data['hook'];
			$pttabs->active = $data['active'];
			$pttabs->ordering = $i;
			$pttabs->params = serialize($data);
			foreach (Language::getLanguages(false) as $lang)
				$pttabs->title_module[$lang['id_lang']] = $data['title_module'];

			$return &= $pttabs->add();
		}
		return $return;
	}	

    public function getContent()
    {
		if (Tools::isSubmit ('saveItem') || Tools::isSubmit ('saveAndStay') || Tools::isSubmit ('updateItemConfirmation'))
		{
			if ($this->_postValidation())
			{
				if (Tools::isSubmit ('updateItemConfirmation') || Tools::isSubmit ('saveItem'))
					$this->html .= $this->displayConfirmation ($this->l('Module successfully updated'));
				$this->html .= $this->_postProcess();
				$this->html .= $this->renderForm();
			}
			else
				$this->html .= $this->renderForm();
		}
		elseif (Tools::isSubmit ('addItem') || Tools::isSubmit('editItem') || Tools::isSubmit ('saveItem'))
		{			
			if (Tools::isSubmit('addItem'))
				$mode = 'add';
			else
				$mode = 'edit';
			if ($mode == 'add')
			{				
				if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL){
					$this->html .= $this->renderForm ();
				}else{
					$this->html .= $this->getShopContextError(null, $mode);
				}
			}
			else
			{
				$associated_shop_ids = PtTab::getAssociatedIdsShop((int)Tools::getValue('id_module'));
				$context_shop_id = (int)Shop::getContextShopID();

				if ($associated_shop_ids === false)
					$this->html .= $this->getShopAssociationError((int)Tools::getValue('id_module'));
				else if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL
					&& in_array($context_shop_id, $associated_shop_ids))
				{
					if (count($associated_shop_ids) > 1)
						$this->html = $this->getSharedSlideWarning();
					$this->html .= $this->renderForm();
				}
				else
				{
					$shops_name_list = array();
					foreach ($associated_shop_ids as $shop_id)
					{
						$associated_shop = new Shop((int)$shop_id);
						$shops_name_list[] = $associated_shop->name;
					}
					$this->html .= $this->getShopContextError($shops_name_list, $mode);
				}
			}
		}
		else
		{
			if ($this->_postValidation())
			{				
				$this->html .= $this->_postProcess();				
				$this->html .= $this->renderList();
			}
			else
				$this->html .= $this->renderList();
		}
		return $this->html;
    }
	
	private function _postValidation()
	{	
		$errors = array();
		if (Tools::isSubmit ('saveItem') || Tools::isSubmit ('saveAndStay'))
		{
			if (!Validate::isInt(Tools::getValue('active')) || (Tools::getValue('active') != 0
					&& Tools::getValue('active') != 1))
				$errors[] = $this->l('Invalid slide state.');
				
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				if (Tools::strlen(Tools::getValue('title_module_'.$language['id_lang'])) > 255)
					$errors[] = $this->l('The title is too long.');
			}
			$id_lang_default = (int)Configuration::get('PS_LANG_DEFAULT');
			if (Tools::strlen(Tools::getValue('title_module_'.$id_lang_default)) == 0)
				$errors[] = $this->l('The title module is not set');
			if (!is_numeric (Tools::getValue('product_limit')) || floor (Tools::getValue('product_limit')) < 0)
				$errors[] = $this->l('Invalid Count Number');			
			if (!is_numeric (Tools::getValue('name_maxlength')) || floor (Tools::getValue('name_maxlength')) < 0)
				$errors[] = $this->l('Invalid Name Maxlength');
			if (!is_numeric (Tools::getValue('description_maxlength')) || floor (Tools::getValue('description_maxlength')) < 0)
				$errors[] = $this->l('Invalid Description Maxlength');
			if (!is_numeric (Tools::getValue('tab_name_maxlength')) || floor (Tools::getValue('tab_name_maxlength')) < 0)
				$errors[] = $this->l('Invalid Tab Name Maxlength');				
		}
		if (count($errors))
		{
			$this->html .= $this->displayError(implode('<br />', $errors));
			return false;
		}
		return true;
	}
	

    private function _postProcess()
    {
		$currentIndex = AdminController::$currentIndex;
        $errors = array();
        if (Tools::isSubmit('saveItem') || Tools::isSubmit('saveAndStay')) {
			if (Tools::getValue('id_module'))
			{
				$block = new PtTab((int)Tools::getValue ('id_module'));
				if (!Validate::isLoadedObject($block))
				{
					$this->html .= $this->displayError($this->l('Invalid slide ID'));
					return false;
				}
			}
			else				
            $block = new PtTab();		
			$default_lang = (int)Configuration::get ('PS_LANG_DEFAULT');
			$block = new PtTab(Tools::getValue ('id_module'));
			$next_ps = $this->getMaxOdering();
			$block->ordering = (!empty($block->ordering)) ? (int)$block->ordering : $next_ps;
			$block->active = (Tools::getValue('active')) ? (int)Tools::getValue('active') : 0;
			$block->hook	= (int)Tools::getValue('hook');	
			
            $cf_data = array();
			$id_module = (int)Tools::getValue ('id_module');
			$id_module = $id_module ? $id_module : (int)$block->getModuleId();
			$cf_data['id_module'] = (int)$id_module;
            $cf_data['active'] = Tools::getValue('active');
            $cf_data['target'] = Tools::getValue('target');
			$cf_data['col_lg'] = Tools::getValue('col_lg');
			$cf_data['col_md'] = Tools::getValue('col_md');
			$cf_data['col_sm'] = Tools::getValue('col_sm');
			$cf_data['col_xs'] = Tools::getValue('col_xs');
			$cf_data['tabs_type'] = Tools::getValue('tabs_type');
            $catids = Tools::getValue('catids');
            $catids = (is_array($catids) && !empty($catids)) ? implode(',', $catids) : false;
            $cf_data['catids'] = $catids;
			$cf_data['product_limit'] = Tools::getValue('product_limit');
			$cf_data['category_preload'] = Tools::getValue('category_preload');
			$cf_data['sort_by'] = Tools::getValue('sort_by');
            $cf_data['sort_direction'] = Tools::getValue('sort_direction');	
			$field_select = Tools::getValue ('field_select');
			$field_select = ( is_array ($field_select) && !empty( $field_select ) )?implode (',', $field_select):false;
			$cf_data['field_select'] = $field_select;			
            $cf_data['field_preload'] = Tools::getValue('field_preload');
			$cf_data['field_direction'] = Tools::getValue('field_direction');
			$cf_data['show_tab_all'] = Tools::getValue('show_tab_all');
            $cf_data['tab_name_maxlength'] = Tools::getValue('tab_name_maxlength');
			$cf_data['cat_field_sort_by'] = Tools::getValue('cat_field_sort_by');
			$cf_data['cat_field_direction'] = Tools::getValue('cat_field_direction');
            $cf_data['image_size'] = Tools::getValue('image_size');
            $cf_data['show_name'] = Tools::getValue('show_name');
            $cf_data['name_maxlength'] = Tools::getValue('name_maxlength');
            $cf_data['show_description'] = Tools::getValue('show_description');
            $cf_data['description_maxlength'] = Tools::getValue('description_maxlength');
            $cf_data['show_price'] = Tools::getValue('show_price');
			$cf_data['show_addtocart'] = Tools::getValue('show_addtocart');
            $cf_data['show_wishlist'] = Tools::getValue('show_wishlist');
            $cf_data['show_compare'] = Tools::getValue('show_compare');            
			$cf_data['show_reviews'] = Tools::getValue('show_reviews');
            $cf_data['show_new'] = Tools::getValue('show_new',1);
            $cf_data['show_sale'] = Tools::getValue('show_sale',1);				
            $cf_data['step'] = Tools::getValue('step');
            $cf_data['auto'] = Tools::getValue('auto');
			$cf_data['pause'] = Tools::getValue('pause');
            $cf_data['duration'] = Tools::getValue('duration');
            $cf_data['interval'] = Tools::getValue('interval');            
            $cf_data['show_page'] = Tools::getValue('show_page',1);
            $cf_data['show_nav'] = Tools::getValue('show_nav',1);	
			
			foreach (Language::getLanguages (false) as $lang)
			{
				$title_value = Tools::getValue ('title_module_'.(int)$lang['id_lang']) ? Tools::getValue ('title_module_'.(int)$lang['id_lang'])
					: Tools::getValue ('title_module_'.$default_lang);
				$block->title_module[(int)$lang['id_lang']] = $title_value;
			}			
			
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				$block->title_module[$language['id_lang']] = Tools::getValue('title_module_'.$language['id_lang']);
			}
			$block->params = serialize($cf_data);
			$get_id = Tools::getValue ('id_module');	
			($get_id) ? $block->update() : $block->add();
			$this->clearCache();
			if (Tools::isSubmit('saveAndStay')) {
				$tool_id_module = Tools::getValue ('id_module');
				$higher_module = $block->getModuleId();
				$id_module = $tool_id_module?(int)$tool_id_module:(int)$higher_module;
				Tools::redirectAdmin ($currentIndex.'&configure='
				.$this->name.'&token='.Tools::getAdminTokenLite ('AdminModules').'&editItem&id_module='
					.$id_module.'&updateItemConfirmation');
			} else {
				Tools::redirectAdmin($currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&saveItemConfirmation');
			}
        } elseif (Tools::isSubmit('changeStatusItem') AND Tools::getValue('id_module')) {
			$stblock = new PtTab((int)Tools::getValue ('id_module'));
			if ($stblock->active == 0)
				$stblock->active = 1;
			else
				$stblock->active = 0;
			$stblock->update();
			$this->clearCache ();
			Tools::redirectAdmin ($currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite ('AdminModules'));			
        } elseif (Tools::isSubmit('deleteItem') AND Tools::getValue('id_module')) {
            $block = new PtTab(Tools::getValue('id_module'));
            $block->delete();
            $this->clearCache();
            Tools::redirectAdmin($currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&deleteItemConfirmation');
        } elseif (Tools::isSubmit('duplicateItem') AND Tools::getValue('id_module')) {
            $block = new PtTab(Tools::getValue('id_module'));
            foreach (Language::getLanguages(false) as $lang) {
                $block->title_module[(int)$lang['id_lang']] = $block->title_module[(int)$lang['id_lang']] . $this->l(' (Copy)');
            }
            $block->duplicate();
            $this->clearCache();
            Tools::redirectAdmin($currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&duplicateItemConfirmation');
        } elseif (Tools::isSubmit('saveItemConfirmation'))
            $this->html = $this->displayConfirmation($this->l('Module successfully updated'));
        elseif (Tools::isSubmit('deleteItemConfirmation'))
            $this->html = $this->displayConfirmation($this->l('Module successfully deleted'));
        elseif (Tools::isSubmit('duplicateItemConfirmation'))
            $this->html = $this->displayConfirmation($this->l('Module successfully duplicated'));
        elseif (Tools::isSubmit('updateItemConfirmation'))
            $this->html = $this->displayConfirmation($this->l('Module successfully updated'));
    }

    public function renderForm()
    {
        $imageTypes = ImageType::getImagesTypes('products');
        array_push($imageTypes, array('name' => 'none'));
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
		$shops_to_get = Shop::getContextListShopID();
		foreach ($shops_to_get as $shop_id)
			$this->generateCatOption($this->customGetNestedCat($shop_id, null, (int)$this->context->language->id, true));
			
		$catopt = $this->getCatSelect();
        $hooks = $this->getHookList();
        $opt_column = array(
            array(
                'id_option' => 1,
                'name' => 1
            ),
            array(
                'id_option' => 2,
                'name' => 2
            ),
            array(
                'id_option' => 3,
                'name' => 3
            ),
            array(
                'id_option' => 4,
                'name' => 4
            ),
            array(
                'id_option' => 5,
                'name' => 5
            ),
            array(
                'id_option' => 6,
                'name' => 6
            )
        );
        $opt_target = array(
            array(
                'id_option' => '_blank',
                'name' => $this->l('New Window')
            ),
            array(
                'id_option' => '_self',
                'name' => $this->l('Same Window')
            ),
            array(
                'id_option' => '_windowopen',
                'name' => $this->l('Popup window')
            )
        );
		
        $this->fields_form[0]['form'] = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('General Settings'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enabled Module'),
                    'name' => 'active',
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),			
                array(
                    'type' => 'text',
                    'label' => $this->l('Module Title'),
                    'lang' => true,
                    'name' => 'title_module',
                    'required' => true,
                    'class' => 'fixed-width-xl'
                ),			
                array(
                    'type' => 'select',
                    'label' => $this->l('Hook Select'),
                    'name' => 'hook',
                    'options' => array(
                        'query' => $hooks,
                        'id' => 'key',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'lang' => true,
                    'label' => $this->l('Open Link Type'),
                    'name' => 'target',
                    'class' => 'fixed-width-xl',
                    'options' => array(
                        'query' => $opt_target,
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),				
                array(
                    'type' => 'select',
                    'lang' => true,
                    'label' => $this->l('# Column [on Large Screens]'),
                    'name' => 'col_lg',
                    'options' => array(
                        'query' => $opt_column,
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'lang' => true,
                    'label' => $this->l('# Column [on Medium Screens]'),
                    'name' => 'col_md',
                    'options' => array(
                        'query' => $opt_column,
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'lang' => true,
                    'label' => $this->l('# Column [on Small Screens]'),
                    'name' => 'col_sm',
                    'class' => 'fixed-width-xl',
                    'options' => array(
                        'query' => $opt_column,
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'select',
                    'lang' => true,
                    'label' => $this->l('# Column [on Extra Small Screens]'),
                    'name' => 'col_xs',
                    'class' => 'fixed-width-xl',
                    'options' => array(
                        'query' => $opt_column,
                        'id' => 'id_option',
                        'name' => 'name'
                    )
                )
            ),
            'submit' => array(
                'title' => $this->l('Save')
            ),
            'buttons' => array(
                array(
                    'title' => $this->l('Save and stay'),
                    'name' => 'saveAndStay',
                    'type' => 'submit',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save'
                )
            )
        );

		$this->fields_form[1]['form'] = array(
			'legend'  => array(
				'title' => $this->l('Source Settings '),
				'icon'  => 'icon-cogs'
			),
			'input'   => array(
				$this->descComment ('For General'),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Tabs Type'),
					'name'    => 'tabs_type',
					'class'   => 'fixed-width-xl',
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'categories',
								'name'      => $this->l('Categories')
							),
							array(
								'id_option' => 'field',
								'name'      => $this->l('Field Products')
							)
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),				
				array(
					'type' => 'categories',
					'label' => 'Select Categories',
					'name' => 'catids',
					'tree' => array(
						'id' => 'id_category',
						'use_checkbox' => true,
						'use_search'  => true,
						'name' => 'catids',
						'selected_categories' => $this->getValuesCat(),
						'root_category'       => Context::getContext()->shop->getCategory(),
					)
				),
				array(
					'type'  => 'text',
					'label' => $this->l('Product Limitation'),
					'name'  => 'product_limit',
					'class' => 'fixed-width-xl'
				),
				$this->descComment ('For Categories'),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Category Preload'),
					'name'    => 'category_preload',
					'class'   => 'fixed-width-xxl',
					'height'  => '300px',
					'options' => array(
						'query' => $catopt,
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Sort by'),
					'name'    => 'sort_by',
					'class'   => 'fixed-width-xl',
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'name',
								'name'      => $this->l('Name')
							),
							array(
								'id_option' => 'id_product',
								'name'      => $this->l('ID')
							),
							array(
								'id_option' => 'date_add',
								'name'      => $this->l('Date Add')
							),
							array(
								'id_option' => 'price',
								'name'      => $this->l('Price')
							),
							array(
								'id_option' => 'sales',
								'name'      => $this->l('Sales')
							),

						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Sort Direction'),
					'name'    => 'sort_direction',
					'class'   => 'fixed-width-xl',
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'DESC',
								'name'      => $this->l('Descending')
							),
							array(
								'id_option' => 'ASC',
								'name'      => $this->l('Ascending')
							),
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
				$this->descComment ('For Field Products'),
				array(
					'type'     => 'select',
					'lang'     => true,
					'label'    => $this->l('Select Product Field'),
					'name'     => 'field_select[]',
					'class'    => 'fixed-width-xxl',
					'height'   => '400px',
					'multiple' => 'multiple',
					'options'  => array(
						'query' => array(
							array(
								'id_option' => 'name',
								'name'      => $this->l('Name')
							),
							array(
								'id_option' => 'id_product',
								'name'      => $this->l('ID')
							),
							array(
								'id_option' => 'date_add',
								'name'      => $this->l('Date Add')
							),
							array(
								'id_option' => 'price',
								'name'      => $this->l('Price')
							),
							array(
								'id_option' => 'sales',
								'name'      => $this->l('Sales')
							)
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Field Preload'),
					'name'    => 'field_preload',
					'class'   => 'fixed-width-xl',
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'name',
								'name'      => $this->l('Name')
							),
							array(
								'id_option' => 'id_product',
								'name'      => $this->l('ID')
							),
							array(
								'id_option' => 'date_add',
								'name'      => $this->l('Date Add')
							),
							array(
								'id_option' => 'price',
								'name'      => $this->l('Price')
							),
							array(
								'id_option' => 'sales',
								'name'      => $this->l('Sales')
							),
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Sort Direction'),
					'name'    => 'field_direction',
					'class'   => 'fixed-width-xl',
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'DESC',
								'name'      => $this->l('Descending')
							),
							array(
								'id_option' => 'ASC',
								'name'      => $this->l('Ascending')
							),
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),

			),
			'submit'  => array(
				'title' => $this->l('Save')
			),
			'buttons' => array(
				array(
					'title' => $this->l('Save and stay'),
					'name'  => 'saveAndStay',
					'type'  => 'submit',
					'class' => 'btn btn-default pull-right',
					'icon'  => 'process-icon-save'
				)
			)
		);

		$this->fields_form[2]['form'] = array(
			'legend'  => array(
				'title' => $this->l('Tabs Settings '),
				'icon'  => 'icon-cogs'
			),
			'input'   => array(
				array(
					'type'    => 'switch',
					'label'   => $this->l('Show Tab All'),
					'name'    => 'show_tab_all',
					'is_bool' => true,
					'values'  => array(
						array(
							'id'    => 'avatar_on',
							'value' => 1,
							'label' => $this->l('Enabled')
						),
						array(
							'id'    => 'avatar_off',
							'value' => 0,
							'label' => $this->l('Disabled')
						)
					)
				),
				array(
					'type'  => 'text',
					'label' => $this->l('Tab Name Max Length'),
					'name'  => 'tab_name_maxlength',
					'class' => 'fixed-width-xl',
				),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Sort by'),
					'name'    => 'cat_field_sort_by',
					'class'   => 'fixed-width-xl',
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'name',
								'name'      => $this->l('Name')
							),
							array(
								'id_option' => 'id_category',
								'name'      => $this->l('ID')
							),
							array(
								'id_option' => 'rand',
								'name'      => $this->l('Random')
							),
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
				array(
					'type'    => 'select',
					'lang'    => true,
					'label'   => $this->l('Sort Direction'),
					'name'    => 'cat_field_direction',
					'class'   => 'fixed-width-xl',
					'options' => array(
						'query' => array(
							array(
								'id_option' => 'ASC',
								'name'      => $this->l('Ascending')
							),
							array(
								'id_option' => 'DESC',
								'name'      => $this->l('Descending')
							),
						),
						'id'    => 'id_option',
						'name'  => 'name'
					)
				),
			),
			'submit'  => array(
				'title' => $this->l('Save')
			),
			'buttons' => array(
				array(
					'title' => $this->l('Save and stay'),
					'name'  => 'saveAndStay',
					'type'  => 'submit',
					'class' => 'btn btn-default pull-right',
					'icon'  => 'process-icon-save'
				)
			)
		);
		
        $this->fields_form[3]['form'] = array(
            'legend' => array(
                'title' => $this->l('Product Settings'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Size image (W x H)'),
                    'name' => 'image_size',
                    'options' => array(
                        'query' => $imageTypes,
                        'id' => 'name',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Title'),
                    'name' => 'show_name',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'avatar_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'avatar_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Title Max Length'),
                    'name' => 'name_maxlength',
                    'class' => 'fixed-width-xl'
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Description'),
                    'name' => 'show_description',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'avatar_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'avatar_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Description Max Length'),
                    'name' => 'description_maxlength',
                    'class' => 'fixed-width-xl'
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Price'),
                    'name' => 'show_price',
                    'values' => array(
                        array(
                            'id' => 'price_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'price_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Add to Cart'),
                    'name' => 'show_addtocart',
                    'values' => array(
                        array(
                            'id' => 'addcart_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'addcart_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Add to Wishlist'),
                    'name' => 'show_wishlist',
                    'values' => array(
                        array(
                            'id' => 'on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Add to Compare'),
                    'name' => 'show_compare',
                    'values' => array(
                        array(
                            'id' => 'on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
				array(
                    'type' => 'switch',
                    'label' => $this->l('Show Reviews'),
                    'name' => 'show_reviews',
                    'values' => array(
                        array(
                            'id' => 'reviews_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'reviews_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show label New'),
                    'name' => 'show_new',
                    'values' => array(
                        array(
                            'id' => 'new_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'new_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('show label Sale'),
                    'name' => 'show_sale',
                    'values' => array(
                        array(
                            'id' => 'sale_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'sale_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                )
            ),
            'submit' => array(
                'title' => $this->l('Save')
            ),
            'buttons' => array(
                array(
                    'title' => $this->l('Save and stay'),
                    'name' => 'saveAndStay',
                    'type' => 'submit',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save'
                )
            )
        );

        $this->fields_form[4]['form'] = array(
            'legend' => array(
                'title' => $this->l('Effect Sttings'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Step'),
                    'name' => 'step',
                    'class' => 'fixed-width-xl'
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Auto Play'),
                    'name' => 'auto',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'auto_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'auto_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Pause on Hover'),
                    'name' => 'pause',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'pause_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'pause_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),				
                array(
                    'type' => 'text',
                    'label' => $this->l('Speed'),
                    'name' => 'duration',
                    'class' => 'fixed-width-xl',
                    'suffix' => 'ms',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Interval'),
                    'name' => 'interval',
                    'class' => 'fixed-width-xl',
                    'suffix' => 'ms',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Button Page'),
                    'name' => 'show_page',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'page_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'page_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Show Button Next/Prev'),
                    'name' => 'show_nav',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'nav_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'nav_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                )				
            ),
            'submit' => array(
                'title' => $this->l('Save')
            ),
            'buttons' => array(
                array(
                    'title' => $this->l('Save and stay'),
                    'name' => 'saveAndStay',
                    'type' => 'submit',
                    'class' => 'btn btn-default pull-right',
                    'icon' => 'process-icon-save'
                )
            )
        );
		
        $helper = new HelperForm();
        $helper->module = $this;
        $helper->name_controller = 'pttabs';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->show_cancel_button = true;
        $helper->back_url = AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules');
        foreach (Language::getLanguages(false) as $lang)
            $helper->languages[] = array(
                'id_lang' => $lang['id_lang'],
                'iso_code' => $lang['iso_code'],
                'name' => $lang['name'],
                'is_default' => ($default_lang == $lang['id_lang'] ? 1 : 0)
            );
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
        $helper->toolbar_scroll = true;
        $helper->title = $this->displayName;
        $helper->submit_action = 'saveItem';
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&save' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules')
            ),
            'back' => array(
                'href' => AdminController::$currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ),
            'save-and-stay' => array(
                'title' => $this->l('Save then add another value'),
                'name' => 'submitAdd' . $this->table . 'AndStay',
                'type' => 'submit',
                'class' => 'btn btn-default pull-right',
                'icon' => 'process-icon-save'
            )
        );

        $id_module = Tools::getValue('id_module');		
        if (Tools::isSubmit('id_module') && $id_module) {
            $block = new PtTab((int)$id_module);
            $this->fields_form[0]['form']['input'][] = array(
                'type' => 'hidden',
                'name' => 'id_module'
            );
			$params = unserialize($block->params);	
            $helper->fields_value['id_module'] = (int)Tools::getValue('id_module', $block->id_module);			
        } else{
            $block = new PtTab();
			$params = array();
		}
        foreach (Language::getLanguages(false) as $lang) {
            $helper->fields_value['title_module'][(int)$lang['id_lang']] = Tools::getValue('title_module_' . (int)$lang['id_lang'], $block->title_module[(int)$lang['id_lang']]);
        }

        $helper->fields_value['active'] = Tools::getValue('active', $block->active);
		$helper->fields_value['hook'] = Tools::getValue('hook', $block->hook);
		$helper->fields_value['target'] = Tools::getValue('target', isset($params['target']) ? $params['target'] : '_self');
        $helper->fields_value['col_lg'] = Tools::getValue('col_lg', isset($params['col_lg']) ? $params['col_lg'] : 4);
        $helper->fields_value['col_md'] = Tools::getValue('col_md', isset($params['col_md']) ? $params['col_md'] : 3);
        $helper->fields_value['col_sm'] = Tools::getValue('col_sm', isset($params['col_sm']) ? $params['col_sm'] : 2);
        $helper->fields_value['col_xs'] = Tools::getValue('col_xs', isset($params['col_xs']) ? $params['col_xs'] : 1);
		$helper->fields_value['tabs_type'] = (string)Tools::getValue ('tabs_type', isset( $params['tabs_type'] )?$params['tabs_type']:'categories');		
		if ($this->getCatSelect(true) != null && isset($params['catids']))
		{
			if ($params['catids'] == 'all')
				$catids = array_slice($this->getCatSelect(true), 0, 5);
			else
				$catids = $params['catids'];
		}
		else
			$catids = false;
		$helper->fields_value['catids[]'] = Tools::getValue ('catids', $catids);
		$helper->fields_value['product_limit'] = Tools::getValue('product_limit', isset($params['product_limit']) ? $params['product_limit'] : 6);
		$helper->fields_value['category_preload'] = Tools::getValue ('category_preload', isset( $params['category_preload'] )?$params['category_preload']:false);
        $helper->fields_value['sort_by'] = Tools::getValue('sort_by', (isset($params['sort_by'])) ? $params['sort_by'] : 'name');
        $helper->fields_value['sort_direction'] = Tools::getValue('sort_direction', (isset($params['sort_direction'])) ? $params['sort_direction'] : 'DESC');
		$helper->fields_value['field_select[]'] = Tools::getValue ('field_select', ( isset( $params['field_select'] ) && $params['field_select'] !== false )?explode (',', $params['field_select']):false);
		$helper->fields_value['field_preload'] = (string)Tools::getValue ('field_preload', isset( $params['field_preload'] )?$params['field_preload']:'name');
		$helper->fields_value['field_direction'] = (string)Tools::getValue ('field_direction', isset( $params['field_direction'] )?$params['field_direction']:'ASC');		
		
		$helper->fields_value['show_tab_all'] = (int)Tools::getValue ('show_tab_all', isset( $params['show_tab_all'] )?$params['show_tab_all']:1);
		$helper->fields_value['tab_name_maxlength'] = (int)Tools::getValue ('tab_name_maxlength', isset( $params['tab_name_maxlength'] )?$params['tab_name_maxlength']:25);
		$helper->fields_value['cat_field_sort_by'] = (string)Tools::getValue ('cat_field_sort_by', isset( $params['cat_field_sort_by'] )?$params['cat_field_sort_by']:'name');
		$helper->fields_value['cat_field_direction'] = (string)Tools::getValue ('cat_field_direction', isset( $params['cat_field_direction'] )?$params['cat_field_direction']:'ASC');
		
        $helper->fields_value['image_size'] = Tools::getValue('image_size', (isset($params['image_size'])) ? $params['image_size'] : 'home_default');
        $helper->fields_value['show_name'] = Tools::getValue('show_name', isset($params['show_name']) ? $params['show_name'] : 1);
        $helper->fields_value['name_maxlength'] = Tools::getValue('name_maxlength', isset($params['name_maxlength']) ? $params['name_maxlength'] : 25);
        $helper->fields_value['show_description'] = Tools::getValue('show_description', isset($params['show_description']) ? $params['show_description'] : 0);
        $helper->fields_value['description_maxlength'] = Tools::getValue('description_maxlength', isset($params['description_maxlength']) ? $params['description_maxlength'] : 100);
        $helper->fields_value['show_price'] = Tools::getValue('show_price', isset($params['show_price']) ? $params['show_price'] : 1);
        $helper->fields_value['show_addtocart'] = Tools::getValue('show_addtocart', isset($params['show_addtocart']) ? $params['show_addtocart'] : 1);
        $helper->fields_value['show_wishlist'] = Tools::getValue('show_wishlist', isset($params['show_wishlist']) ? $params['show_wishlist'] : 1);
        $helper->fields_value['show_compare'] = Tools::getValue('show_compare', isset($params['show_compare']) ? $params['show_compare'] : 1);
		$helper->fields_value['show_reviews'] = Tools::getValue('show_reviews', isset($params['show_reviews']) ? $params['show_reviews'] : 1);
        $helper->fields_value['show_new'] = Tools::getValue('show_new', (isset($params['show_new'])) ? $params['show_new'] : 1);
        $helper->fields_value['show_sale'] = Tools::getValue('show_sale', (isset($params['show_sale'])) ? $params['show_sale'] : 1);
		
        $helper->fields_value['step'] = Tools::getValue('step', isset($params['step']) ? $params['step'] : 1);
        $helper->fields_value['auto'] = Tools::getValue('auto', isset($params['auto']) ? $params['auto'] : 0);
        $helper->fields_value['pause'] = Tools::getValue('pause', isset($params['pause']) ? $params['pause'] : 1);
        $helper->fields_value['duration'] = Tools::getValue('duration', isset($params['duration']) ? $params['duration'] : 250);
		$helper->fields_value['interval'] = Tools::getValue('interval', isset($params['interval']) ? $params['interval'] : 5000);
        $helper->fields_value['show_page'] = Tools::getValue('show_page', (isset($params['show_page'])) ? $params['show_page'] : 1);
        $helper->fields_value['show_nav'] = Tools::getValue('show_nav', (isset($params['show_nav'])) ? $params['show_nav'] : 1);		
			
        $this->html .= $helper->generateForm($this->fields_form);		
    }
	
    private function renderList()
    {
		$currentIndex = AdminController::$currentIndex;
		$modules = array();
		$this->html .= $this->headerHTML ();
		if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL)
			$this->html .= $this->getWarningMultishopHtml();
		else if (Shop::getContext() != Shop::CONTEXT_GROUP && Shop::getContext() != Shop::CONTEXT_ALL)
		{
			$modules = $this->getListConfig();
			if (!empty($modules))
			{
				foreach ($modules as $key => $mod)
				{
					$associated_shop_ids = PtTab::getAssociatedIdsShop((int)$mod['id_module']);
					if ($associated_shop_ids && count($associated_shop_ids) > 1)
						$modules[$key]['is_shared'] = true;
					else
						$modules[$key]['is_shared'] = false;
				}
			}
		}
        $this->html .= '
	 	<div class="panel">
			<div class="panel-heading">
				' . $this->l('Module Manager') . '
				<span class="panel-heading-action">
						<a class="list-toolbar-btn" href="' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&addItem"><span data-toggle="tooltip" class="label-tooltip" data-original-title="' . $this->l('Add new module') . '" data-html="true"><i class="process-icon-new "></i></span></a>
				</span>
			</div>
			<table width="100%" class="table" cellspacing="0" cellpadding="0">
			<thead>
			<tr class="nodrag nodrop">
				<th>' . $this->l('ID') . '</th>				
				<th class=" left">' . $this->l('Title') . '</th>
				<th class=" left">' . $this->l('Hook') . '</th>
				<th class=" left">' . $this->l('Status') . '</th>
				<th>' . $this->l('Ordering') . '</th>
				<th class=" right"><span class="title_box text-right">' . $this->l('Actions') . '</span></th>
			</tr>
			</thead>
			<tbody id="list_config">';
        if (is_array($modules)) {
            static $irow;
            foreach ($modules as $block) {
                $this->html .= '
				<tr id="item_' . $block['id_module'] . '" class=" ' . ($irow++ % 2 ? ' ' : '') . '">
					<td class=" 	" onclick="document.location = \'' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&editItem&id_module=' . $block['id_module'] . '\'">' . $block['id_module'] . '</td>					
					<td class="  " onclick="document.location = \'' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&editItem&id_module=' . $block['id_module'] . '\'">' . $block['title_module'] . '</td>
					<td class="  " onclick="document.location = \'' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&editItem&id_module=' . $block['id_module'] . '\'">' . (Validate::isInt($block['hook']) ? $this->getHookName($block['hook']) : '') . '</td>
					<td class="  "> <a href="' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&changeStatusItem&id_module=' . $block['id_module'] . '&status=' . $block['active'] . '&hook=' . $block['hook'] . '">' . ($block['active'] ? '<i class="icon-check"></i>' : '<i class="icon-remove"></i>') . '</a> </td>
					<td class=" dragHandle"><div class="dragGroup"><div class="positions">' . $block['ordering'] . '</div></div></td>
					<td class="text-right">
						<div class="btn-group-action">
							<div class="btn-group pull-right">
								<a class="btn btn-default" href="' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&editItem&id_module=' . $block['id_module'] . '">
									<i class="icon-pencil"></i> Edit
								</a> 
								<button data-toggle="dropdown" class="btn btn-default dropdown-toggle">
									<span class="caret"></span>&nbsp;
								</button>
								<ul class="dropdown-menu">
									<li>
										<a onclick="return confirm(\'' . $this->l('Are you sure want duplicate this item?', __CLASS__, TRUE, FALSE) . '\');"  title="' . $this->l('Duplicate') . '" href="' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&duplicateItem&id_module=' . $block['id_module'] . '">
											<i class="icon-copy"></i> ' . $this->l('Duplicate') . '
										</a>								
									</li>
									<li class="divider"></li>
									<li>
										<a title ="' . $this->l('Delete') . '" onclick="return confirm(\'' . $this->l('Are you sure?', __CLASS__, TRUE, FALSE) . '\');" href="' . $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getAdminTokenLite('AdminModules') . '&deleteItem&id_module=' . $block['id_module'] . '">
											<i class="icon-trash"></i> ' . $this->l('Delete') . '
										</a>
									</li>
								</ul>
							</div>
						</div>
					</td>
				</tr>';
            }
        }
        $this->html .= '
			</tbody>
			</table>
		</div>';
    }
	
    private function getListConfig()
    {
        $this->context = Context::getContext();
        $id_lang = $this->context->language->id;
        $id_shop = $this->context->shop->id;
        if (!$result = Db::getInstance()->ExecuteS('SELECT b.id_module,  b.hook, b.ordering, bs.active, bl.`title_module`
			FROM `' . _DB_PREFIX_ . 'pttabs` b 
			LEFT JOIN `' . _DB_PREFIX_ . 'pttabs_shop` bs ON (b.`id_module` = bs.`id_module` )
			LEFT JOIN `' . _DB_PREFIX_ . 'pttabs_lang` bl ON (b.`id_module` = bl.`id_module`' . ($id_shop ? 'AND bs.`id_shop` = ' . $id_shop : ' ') . ') 
			WHERE bl.id_lang = ' . (int)$id_lang . ($id_shop ? ' AND bs.`id_shop` = ' . $id_shop : ' ') . '
			 ORDER BY b.ordering')
        )
            return false;
        return $result;	
    }
	
    public function headerHTML()
    {
        if (Tools::getValue('controller') != 'AdminModules' && Tools::getValue('configure') != $this->name)
            return;
        $this->context->controller->addJqueryUI('ui.sortable');
        $html = '<script type="text/javascript">
			$(function() {
				var $list_config = $("#list_config");
				$list_config.sortable({
					opacity: 0.7,
					cursor: "move",
					handle: ".dragGroup",
					update: function() {
						var order = $(this).sortable("serialize") + "&action=updateSlidesPosition";
							$.ajax({
								type: "POST",
								dataType: "json",
								data:order,
								url:"' . $this->context->shop->physical_uri . $this->context->shop->virtual_uri . 'modules/' . $this->name . '/ajax_' . $this->name . '.php?secure_key=' . $this->secure_key . '",
								success: function (msg){
									if (msg.error)
									{
										showErrorMessage(msg.error);
										return;
									}
									$(".positions", $list_config).each(function(i){
										$(this).text(i);
									});
									showSuccessMessage(msg.success);
								}
							});
						
						}
					});
					$(".dragGroup",$list_config).hover(function() {
						$(this).css("cursor","move");
					},
					function() {
						$(this).css("cursor","auto");
				    });
			});
		</script>
		';
        $html .= '<style type="text/css">#list_config .ui-sortable-helper{display:table!important;}
		#list_config .ui-sortable-helper td{ background-color:#00aff0!important;color:#FFF;}</style>';
        return $html;
    }	
	
    private function clearCache()
    {
        $this->_clearCache('default.tpl');
    }

	public function getMaxOdering()
	{
		$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow('
			SELECT MAX(cs.`ordering`) AS `next_position`
			FROM `'._DB_PREFIX_.'pttabs` cs, `'._DB_PREFIX_.'pttabs_shop` css
			WHERE css.`id_module` = cs.`id_module` AND css.`id_shop` = '.(int)$this->context->shop->id
		);

		return (++$row['next_position']);
	}
	
    private function getHookName($id_hook, $name = false)
    {
        if (!$result = Db::getInstance()->getRow('
			SELECT `name`,`title`
			FROM `' . _DB_PREFIX_ . 'hook` 
			WHERE `id_hook` = ' . (int)($id_hook))
        )
            return false;
        return (($result['title'] != "" && $name) ? $result['title'] : $result['name']);
    }

    public function getHookList()
    {
        $hooks = array();
        foreach ($this->defaultHook as $key => $hook) {
            $id_hook = Hook::getIdByName($hook);
            $name_hook = $this->getHookName($id_hook);
            $hooks[$key]['key'] = $id_hook;
            $hooks[$key]['name'] = $name_hook;
        }
        return $hooks;
    }

    public function hookdisplayHeader($params)
    {
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'index')
			$this->context->controller->addCSS (_THEME_CSS_DIR_.'product_list.css');
		
        $this->context->controller->addCSS($this->_path . 'css/styles.css', 'all');
		$this->context->controller->addCSS($this->_path . 'css/owl.carousel.css', 'all');
        /*$hook_list = $this->getHookList();
        if(empty($hook_list)) return;
        $contents = array();
        foreach($hook_list as $hook_name){
            $check = $this->getItems($hook_name['name']);
            if(!empty($check)){
                $contents[] = $this->getItems($hook_name['name']);
            }
        }
        if(empty($contents)) return;
        foreach($contents as $modules){
            foreach($modules as $module) {
                if (!defined('PATHTHEMES_JQUERY')) {
                    $this->context->controller->addJS($this->_path . 'js/jquery-2.1.1.min.js');
                    $this->context->controller->addJS($this->_path . 'js/jquery-noconflict.js');
                    define('PATHTHEMES_JQUERY', 1);
                    break;
                }
            }
        }*/
        $this->context->controller->addJS($this->_path . 'js/owl.carousel.min.js');
		return $this->display(__FILE__, 'header.tpl');
    }
    
	public function hookdisplayStatics6(){
        global $smarty;
        $smarty_cache_id = $this->getCacheId('pttabs_hookStatics6');
        if (!$this->isCached('default.tpl', $smarty_cache_id)) {
            $list = $this->getItems('displayStatics6');
            $smarty->assign(array(
                'list' => $list
            ));
        }
        return $this->display(__FILE__, 'default.tpl', $smarty_cache_id);		
	}
	
    public function hookdisplayStatics5(){
        global $smarty;
        $smarty_cache_id = $this->getCacheId('pttabs_hookStatics5');
        if (!$this->isCached('default.tpl', $smarty_cache_id)) {
            $list = $this->getItems('displayStatics5');
            $smarty->assign(array(
                'list' => $list
            ));
        }
        return $this->display(__FILE__, 'default.tpl', $smarty_cache_id);		
	}
	
	public function hookdisplayStatics14(){
        global $smarty;
        $smarty_cache_id = $this->getCacheId('pttabs_hookStatics14');
        if (!$this->isCached('default2.tpl', $smarty_cache_id)) {
            $list = $this->getItems('displayStatics14');
            $smarty->assign(array(
                'list' => $list
            ));
        }
        return $this->display(__FILE__, 'default2.tpl', $smarty_cache_id);		
	}

    public function hookActionShopDataDuplication($params)
    {
        Db::getInstance()->execute('
		INSERT IGNORE INTO ' . _DB_PREFIX_ . 'pttabs_shop (id_module, id_shop)
		SELECT id_module, ' . (int)$params['new_id_shop'] . '
		FROM ' . _DB_PREFIX_ . 'pttabs_shop
		WHERE id_shop = ' . (int)$params['old_id_shop']);
    }
		
	public function callAjax()
	{
		if (Tools::getValue ('pttabs_is_ajax') == 1)
		{
			$smarty = $this->context->smarty;
			$id_pttabs = Tools::getValue ('pttabs_moduleid');
			$pttabs = new PtTab($id_pttabs);
			$params = unserialize($pttabs->params);
			if ($id_pttabs == $pttabs->id_module)
			{
				$id_category = Tools::getValue ('categoryid');
				if ($id_category == "*"){
					if ($params['catids'] == 'all')
						$id_category = array_slice($this->getCatSelect(true), 0, 5);
					else
						$id_category = $this->getCatIds($params);
				}
				if ($params['tabs_type'] == 'categories')
					$child_items = $this->getProductInfo ($params, $id_category, false, null);
				else
					$child_items = $this->getProductInfo ($params, '*', false, $id_category);
				$result = new stdClass();
				$smarty->assign (array(
					'child_items'  => $child_items,
					'items_params' => $params,
					'id_module' => $id_pttabs
				));

				$buffer = $this->display (__FILE__, 'product.tpl');
				$result->items_markup = preg_replace (
					array(
						'/ {2,}/',
						'/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s'
					),
					array(
						' ',
						''
					),
					$buffer
				);

				die( Tools::jsonEncode ($result) );
			}
		}
	}

	private function getCatInfo($catids, $params)
	{
		!is_array ($catids) && settype ($catids, 'array');
		if (empty( $catids ))
			return;
		$context = Context::getContext ();
		$id_lang = (int)$context->language->id;
		$cat_root = '';
		foreach ($catids as $cat)
		{
			$category = new Category($cat);
			if ($category->isRootCategoryForAShop())
			{
				$cat_root .= $cat;
				break;
			}
		}		
		$categories = Category::getCategoryInformations ($catids, $id_lang);		
		if (empty( $categories ))
			return;
		$list = array();
		
		foreach ($categories as $category)
		{			
			$category_image_url = $this->context->link->getCatImageLink ($category['link_rewrite'], $category['id_category']);
			$category['image'] = $category_image_url;		
			$category['count'] = $this->countProduct($category['id_category'], $params);
			$category['link'] = $this->context->link->getCategoryLink ($category['id_category'], $category['link_rewrite']);
			$category['_target'] = $this->parseTarget ($params['target']);
			$category['name'] = $this->truncate ($category['name'], $params['tab_name_maxlength']);
			$list[] = $category;
		}
	
		$cat_order_by = $params['cat_field_sort_by'];
		$cat_ordering = $params['cat_field_direction'];
		if ($cat_order_by != null)
		{
			switch ($cat_order_by)
			{
				default:
				case 'name':
					if ($cat_ordering == 'ASC')
						usort ($list, create_function ('$a, $b', 'return strnatcasecmp( $a["name"], $b["name"]);'));
					else
						usort ($list, create_function ('$a, $b', 'return strnatcasecmp( $b["name"], $a["name"]);'));
					break;
				case 'id_category':
					if ($cat_ordering == 'ASC')
						usort ($list, create_function ('$a, $b', 'return $a["id_category"] > $b["id_category"];'));
					else
						usort ($list, create_function ('$a, $b', 'return $a["id_category"] < $b["id_category"];'));
					break;
				case 'rand':
					shuffle ($list);
					break;
			}
		}
		return $list;
	}	
	
	public function getProductInfo($params, $catids, $count_product = false, $product_filter = null)
	{
		if ($catids == '*')
			$catids = $this->getCatIds ($params);
		!is_array ($catids) && settype ($catids, 'array');
		$products = $this->_getProducts ($catids, $params, $count_product, $product_filter);
		if (empty( $products ))
			return;
		$list = array();
		foreach ($products as $product)
		{	
			$obj = new Product(( $product['id_product'] ), false, $this->context->language->id);
			$images = $obj->getImages ($this->context->language->id);
			$images_pro = array();
			if (!empty( $images ))
			{
				foreach ($images as $image)
					$images_pro[] = $obj->id.'-'.$image['id_image'];
			}
			$product['title'] = $this->truncate ($product['name'], $params['name_maxlength']);				
			$product['desc'] = $this->_cleanText ($product['description']);
			$product['desc'] = $this->_trimEncode ($product['desc']) != ''?$product['desc']:$this->_cleanText ($product['description_short']);
			$product['desc'] = $this->truncate ($product['desc'], $params['description_maxlength']);
			$product['_images'] = $images_pro;
			$product['_target'] = $this->parseTarget ($params['target']);
			$list[] = $product;			
		}
		return $list;
	}

	public function getCatIds($params)
	{
		$catids = ( isset( $params['catids'] ) && $params['catids'] != '' )?explode (',', $params['catids']):'';

		if ($catids == '')
			return;
		return $catids;

	}
	
	public function getList($params)
	{
		$type_filter = $params['tabs_type'];
		if ($this->getCatSelect(true) != null && isset($params['catids']))
		{
			if ($params['catids'] == 'all')
				$catids = array_slice($this->getCatSelect(true), 0, 5);
			else
				$catids = $this->getCatIds($params);
		}
		$list = array();

		switch ($type_filter)
		{
			case 'categories':
				if ($catids == '*')
					$catids = $this->getCatIds ($params);
				!is_array ($catids) && settype ($catids, 'array');
				if (empty( $catids ))
					return;
				$cats = $this->getCatInfo ($catids, $params);

				if (empty( $cats ))
					return;
				if ($params['show_tab_all'])
				{
					$all = array();
					$all['id_category'] = '*';
					$all['count'] = $this->countProduct ($catids, $params);
					$all['name'] = $this->l('All');
					array_unshift ($cats, $all);
				}
				$catidpreload = $params['category_preload'];
				if (!in_array ($catidpreload, $catids))
					$catidpreload = array_shift ($catids);

				$selected = false;

				foreach ($cats as $cat)
				{
					if (isset( $cat['sel'] ))
						unset( $cat['sel'] );
					if ($cat['count'] > 0)
					{
						if ($cat['id_category'] == $catidpreload)
						{
							$cat['sel'] = 'sel';
							$cat['child'] = $this->getProductInfo ($params, $catidpreload);
							$selected = true;
						}
						$list[$cat['id_category']] = $cat;
					}
				}

				// first tab is active
				if (!$selected)
				{
					foreach ($cats as $cat)
					{
						if ($cat['count'] > 0)
						{
							$cat['sel'] = 'sel';
							$cat['child'] = $this->getProductInfo ($params, $cat->id);
							$list[$cat['id_category']] = $cat;
							break;
						}
					}
				}
				break;

			case 'field':
				$filters = explode (',', $params['field_select']);
				$products = array();
				$filter_preload = $params['field_preload'];
				if (empty( $filters ))
					return;
				if (!in_array ($filter_preload, $filters))
					$filter_preload = $filters[0];
				foreach ($filters as $filter)
				{
					$product = array();
					$product['count'] = $this->countProduct ($catids, $params);
					$product['id_category'] = $filter;
					$product['name'] = $this->getLabel ($filter);
					array_unshift ($products, $product);
				}

				foreach ($products as $product)
				{
					if ($product['count'] > 0)
					{
						if ($product['id_category'] == $filter_preload)
						{
							$product['sel'] = 'sel';
							$product['child'] = $this->getProductInfo ($params, $catids, false, $filter_preload);
						}
						$list[$product['id_category']] = $product;
					}
				}
				break;
		}
		ini_set ('xdebug.var_display_max_depth', 10);
		if (empty( $list ))
			return;
		return $list;
	}

	private function countProduct($catids, $params)
	{
		!is_array ($catids) && settype ($catids, 'array');
		$count_product = $this->_getProducts ($catids, $params, true, null);
		return $count_product;
	}

	protected function generateCatOption($categories, $current = null, $id_selected = 1)
	{
		foreach ($categories as $category)
		{
			$this->categories[$category['id_category']] = str_repeat(' -|- ', $category['level_depth'] * 1)
				.Tools::stripslashes($category['name']);
			if (isset($category['children']) && !empty($category['children']))
			{
				$current = $category['id_category'];
				$this->generateCatOption($category['children'], $current, $id_selected);
			}
		}
	}
	
	public function customGetNestedCat($shop_id, $root_category = null, $id_lang = false, $active = true,
	$groups = null, $use_shop_restriction = true, $sql_filter = '', $sql_sort = '', $sql_limit = '')
	{
		if (isset($root_category) && !Validate::isInt($root_category))
			die(Tools::displayError());
		if (!Validate::isBool($active))
			die(Tools::displayError());
		if (isset($groups) && Group::isFeatureActive() && !is_array($groups))
			$groups = (array)$groups;
		$cache_id = 'Category::getNestedCategories_'.md5((int)$shop_id
				.(int)$root_category.(int)$id_lang.(int)$active.(int)$active
				.(isset($groups) && Group::isFeatureActive() ? implode('', $groups) : ''));
		if (!Cache::isStored($cache_id))
		{
			$result = Db::getInstance()->executeS('
				SELECT c.*, cl.*
				FROM `'._DB_PREFIX_.'category` c
				INNER JOIN `'._DB_PREFIX_.'category_shop` category_shop
				ON (category_shop.`id_category` = c.`id_category` AND category_shop.`id_shop` = "'.(int)$shop_id.'")
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
				ON (c.`id_category` = cl.`id_category` AND cl.`id_shop` = "'.(int)$shop_id.'")
				WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND cl.`id_lang` = '.(int)$id_lang : '').'
				'.($active ? ' AND (c.`active` = 1 OR c.`is_root_category` = 1)' : '').'
			'.(isset($groups) && Group::isFeatureActive() ? ' AND cg.`id_group` IN ('.implode(',', $groups).')' : '').'
				'.(!$id_lang || (isset($groups) && Group::isFeatureActive()) ? ' GROUP BY c.`id_category`' : '').'
				'.($sql_sort != '' ? $sql_sort : ' ORDER BY c.`level_depth` ASC , c.`nleft` ASC').'
				'.($sql_sort == '' && $use_shop_restriction ? ', category_shop.`position` ASC' : '').'
				'.($sql_limit != '' ? $sql_limit : '')
			);

			$categories = array();
			$buff = array();
			foreach ($result as $row)
			{
				$current = &$buff[$row['id_category']];
				$current = $row;
				if ($row['id_parent'] == 0)
					$categories[$row['id_category']] = &$current;
				else
					$buff[$row['id_parent']]['children'][$row['id_category']] = &$current;
			}
			Cache::store($cache_id, $categories);
		}
		return Cache::retrieve($cache_id);
	}

	public function getCatSelect($default = false)
	{
		$shops_to_get = Shop::getContextListShopID();
		foreach ($shops_to_get as $shop_id)
			$this->generateCatOption($this->customGetNestedCat($shop_id, null, (int)$this->context->language->id, true));
		$catopt = array();
		if (!empty( $this->categories ))
		{
			foreach ($this->categories as $key => $cat)
			{
				if ($cat !== 'Root')
				{
					if ($default)
						$catopt[] = $key;
					else
					{
						$tmp = array();
						if ($cat != '')
						{
							$tmp['id_option'] = $key;
							$tmp['name'] = $cat;
							$catopt[] = $tmp;
						}
					}
				}
			}
		}
		return $catopt;
	}
	
	private function getValuesCat()
	{
		$id_module = Tools::getValue ('id_module');
		if (Tools::isSubmit ('id_module') && $id_module)
		{
			$block = new PtTab((int)$id_module);
			$params = unserialize($block->params);
		}
		else
		{
			$block = new PtTab();
			$params = array();
		}
		if ($this->getCatSelect(true) != null && isset($params['catids']) && $params['catids'])
		{
			if ($params['catids'] == 'all')
				$catids = array_slice($this->getCatSelect(true), 0, 5);
			else
				$catids = $params['catids'];
			$catids = (!empty($catids) && is_string($catids)) ? explode(',', $catids) : $catids;
			$catids = Tools::getValue ('catids', $catids);
			$catids1 = array();
			foreach ($catids as $cat)
				$catids1[] = (int)$cat;
		}
		else
			$catids1 = array();
		return $catids1;
	}

	public function getLabel($order)
	{
		if (empty( $order ))
			return;
		switch ($order)
		{
			case 'name':
				return $this->l('Name');
			case 'id_product':
				return $this->l('ID Product');
			case 'date_add':
				return $this->l('Date Add');
			case 'price':
				return $this->l('Price');
			case 'sales':
				return $this->l('Sales');
		}
	}	
	
	private function _getProductsSpecialPrice(){
		$sql = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT DISTINCT  s.`id_product`
			FROM `'._DB_PREFIX_.'specific_price` s');
		$list_product = array();
		if(!empty($sql)) {
			foreach($sql as $res){
				$list_product[]= $res['id_product'];
			}
		}	
		return $list_product;
	}	
	
    public function _getProducts($id_category = false, $params, $count_product = false, $product_filter = null)
    {
		$context = Context::getContext ();
		$id_lang = (int)$context->language->id;
		if (!$product_filter)
		{
			$order_by = $params['sort_by'];
			$order_way = $params['sort_direction'];
		}
		else
		{
			$order_by = $product_filter;
			$order_way = $params['field_direction'];
		}

		if (empty( $id_category ))
			return;
		if ($id_category == 'all')
			$id_category = $this->getCatIds ($params);
		else
		{
			$child_category_products = 'exclude';
			$level_depth = 9999;
			$id_category = ( $child_category_products == 'include' )?$this->getChildenCategories ($id_category,
				$level_depth, true):$id_category;
		}
		$start = 0;
		$limit = (int)$params['product_limit'];
		$only_active = true;
		$number_days_new_product = 9999;
		if ($number_days_new_product == 0)
			$number_days_new_product = -1;
		$front = true;
		if (!in_array ($context->controller->controller_type, array( 'front', 'modulefront' )))
			$front = false;

		if (!Validate::isOrderBy ($order_by) || !Validate::isOrderWay ($order_way))
			die ( Tools::displayError () );
		if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add' || $order_by == 'date_upd')
			$order_by_prefix = 'p';
		else if ($order_by == 'name')
			$order_by_prefix = 'pl';

		if (strpos ($order_by, '.') > 0)
		{
			$order_by = explode ('.', $order_by);
			$order_by_prefix = $order_by[0];
			$order_by = $order_by[1];
		}

		if ($order_by == 'sales' || $order_by == 'rand')
			$order_by_prefix = '';
		$sql = 'SELECT DISTINCT  p.`id_product`, p.*, product_shop.*, pl.* , m.`name` AS manufacturer_name, s.`name` AS supplier_name,
				MAX(product_attribute_shop.id_product_attribute) id_product_attribute,
				  MAX(image_shop.`id_image`) id_image,  il.`legend`,
				   ps.`quantity` AS sales, cl.`link_rewrite` AS category,
				    IFNULL(stock.quantity,0) as quantity,
				     IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity,
				      stock.out_of_stock, product_shop.`date_add` > "'
			.date ('Y-m-d', strtotime ('-'.( $number_days_new_product?(int)$number_days_new_product:20 ).' DAY')).'" as new, product_shop.`on_sale`
				FROM `'._DB_PREFIX_.'product` p
				'.Shop::addSqlAssociation ('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang ('pl').')
				LEFT JOIN `'._DB_PREFIX_.'product_sale` ps ON (p.`id_product` = ps.`id_product` '.Shop::addSqlAssociation ('product_sale', 'ps').')
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation ('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock ('p', 'product_attribute_shop', false, $context->shop).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
				ON cl.`id_category` = product_shop.`id_category_default`
				AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang ('cl').'
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
			Shop::addSqlAssociation ('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
				LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (s.`id_supplier` = p.`id_supplier`)'.
			( $id_category?' LEFT JOIN `'._DB_PREFIX_.'category_product` c ON (c.`id_product` = p.`id_product`)':'' ).'
				WHERE pl.`id_lang` = '.(int)$id_lang.
			( $id_category?' AND c.`id_category` IN ('.implode (',', $id_category).')':'' ).
			( $front?' AND product_shop.`visibility` IN ("both", "catalog")':'' ).
			( $only_active?' AND product_shop.`active` = 1':'' ).'
				GROUP BY  p.`id_product`
				ORDER BY '.( isset( $order_by_prefix )?( ( $order_by_prefix != '' )?pSQL ($order_by_prefix).'.':'' ):'' )
			.( $order_by == 'rand'?' rand() ':'`'.pSQL ($order_by).'`' ).pSQL ($order_way);
		if (!$count_product)
			$sql .= ( $limit > 0?' LIMIT '.(int)$start.','.(int)$limit:'' );
		$rq = Db::getInstance (_PS_USE_SQL_SLAVE_)->executeS ($sql);
		if ($count_product)
			return count ($rq);
		if ($order_by == 'price')
			Tools::orderbyPrice ($rq, $order_way);
		$products_ids = array();
		foreach ($rq as $row)
			$products_ids[] = $row['id_product'];

		Product::cacheFrontFeatures ($products_ids, $id_lang);
		return Product::getProductsProperties ((int)$id_lang, $rq);
    }
	
    private function _getData($params)
    {		
		$type_filter = $params['tabs_type'];
		if ($this->getCatSelect(true) != null && isset($params['catids']))
		{
			if ($params['catids'] == 'all')
				$catids = array_slice($this->getCatSelect(true), 0, 5);
			else
				$catids = $this->getCatIds($params);
		}
		$list = array();

		switch ($type_filter)
		{
			case 'categories':
				if ($catids == '*')
					$catids = $this->getCatIds ($params);
				!is_array ($catids) && settype ($catids, 'array');
				if (empty( $catids ))
					return;				
				$cats = $this->getCatInfo ($catids, $params);
				if (empty( $cats ))
					return;
				if ($params['show_tab_all'])
				{
					$all = array();
					$all['id_category'] = '*';
					$all['count'] = $this->countProduct ($catids, $params);
					$all['name'] = $this->l('ALL Products');
					array_unshift ($cats, $all);
				}
				$catidpreload = $params['category_preload'];
				if (!in_array ($catidpreload, $catids))
					$catidpreload = array_shift ($catids);
				
				$selected = false;

				foreach ($cats as $cat)
				{
					if (isset( $cat['sel'] ))
						unset( $cat['sel'] );
					if ($cat['count'] > 0)
					{
						if ($cat['id_category'] == $catidpreload)
						{
							$cat['sel'] = 'sel';
							$cat['child'] = $this->getProductInfo ($params, $catidpreload);
							$selected = true;
						}
						$list[$cat['id_category']] = $cat;
					}
				}
				// first tab is active
				if (!$selected)
				{
					foreach ($cats as $cat)
					{
						if ($cat['count'] > 0)
						{
							$cat['sel'] = 'sel';
							$cat['child'] = $this->getProductInfo ($params, $cat->id);
							$list[$cat['id_category']] = $cat;
							break;
						}
					}
				}	
				break;

			case 'field':			
				$filters = explode (',', $params['field_select']);
				$products = array();
				$filter_preload = $params['field_preload'];
				if (empty( $filters ))
					return;
				if (!in_array ($filter_preload, $filters))
					$filter_preload = $filters[0];				
				foreach ($filters as $filter)
				{
					$product = array();
					$product['count'] = $this->countProduct($catids, $params);
					$product['id_category'] = $filter;
					$product['name'] = $this->getLabel ($filter);
					array_unshift ($products, $product);
				}				
				foreach ($products as $product)
				{
					if ($product['count'] > 0)
					{
						if ($product['id_category'] == $filter_preload)
						{
							$product['sel'] = 'sel';
							$product['child'] = $this->getProductInfo ($params, $catids, false, $filter_preload);
						}
						$list[$product['id_category']] = $product;
					}
				}
				break;
		}		
		ini_set ('xdebug.var_display_max_depth', 10);
		if (empty( $list ))
			return;	
		return $list;
    }

    private function getItems($hook_name)
    {
        $list = array();
        $this->context = Context::getContext();
        $id_shop = $this->context->shop->id;
        $id_hook = Hook::getIdByName($hook_name);
        if ($id_hook) {
            $results = Db::getInstance()->ExecuteS('SELECT b.`id_module` FROM `' . _DB_PREFIX_ . 'pttabs` b
			LEFT JOIN `' . _DB_PREFIX_ . 'pttabs_shop` bs ON (b.id_module = bs.id_module)
			WHERE bs.active = 1 AND (bs.id_shop = ' . (int)$id_shop . ') AND b.`hook` = ' . (int)($id_hook) . ' ORDER BY b.ordering');

            foreach ($results as $row) {
                $temp = new PtTab($row['id_module']);
				$temp->params = unserialize($temp->params); 
                $temp->products = $this->_getData($temp->params);
                $list[] = $temp;
            }
        }
        if (empty($list)) return;
        return $list;
    }

	private function descComment($text = null)
	{
		return array(
			'type'         => 'html',
			'name'         => 'description',
			'html_content' => '<div style="text-transform:uppercase;width:300px;color:#555; padding:3px 0; border-bottom: 1px solid #dfdfdf; font-style: italic; font-weight: bold;">
        '.$text.'</div>',
		);
	}	
	
	private function getWarningMultishopHtml()
	{
		if (Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_ALL)
			return '<p class="alert alert-warning">'.
						$this->l('You cannot manage modules items from a "All Shops" or a "Group Shop" context, select directly the shop you want to edit').
					'</p>';
		else
			return '';
	}

	private function getShopContextError($shop_contextualized_name, $mode)
	{
		if (is_array($shop_contextualized_name))
			$shop_contextualized_name = implode('<br/>', $shop_contextualized_name);

		if ($mode == 'edit')
			return '<p class="alert alert-danger">'.
							sprintf($this->l('You can only edit this module from the shop(s) context: %s'),
								$shop_contextualized_name).
					'</p>';
		else
			return '<p class="alert alert-danger">'.
							sprintf($this->l('You cannot add modules from a "All Shops" or a "Group Shop" context')).
					'</p>';
	}

	private function getShopAssociationError($id_module)
	{
		return '<p class="alert alert-danger">'.
			sprintf($this->l('Unable to get module shop association information (id_module: %d)'), (int)$id_module).
				'</p>';
	}

	private function getCurrentShopInfoMsg()
	{
		$shop_info = null;

		if (Shop::isFeatureActive())
		{
			if (Shop::getContext() == Shop::CONTEXT_SHOP)
			$shop_info = sprintf($this->l('The modifications will be applied to shop: %s'), $this->context->shop->name);
			else if (Shop::getContext() == Shop::CONTEXT_GROUP)
				$shop_info = sprintf($this->l('The modifications will be applied to this group: %s'),
					Shop::getContextShopGroup()->name);
			else
				$shop_info = $this->l('The modifications will be applied to all shops and shop groups');

			return '<div class="alert alert-info">'.
						$shop_info.
					'</div>';
		}
		else
			return '';
	}
	
	private function getSharedSlideWarning()
	{
		return '<p class="alert alert-warning">'.
					$this->l('This module is shared with other shops! All shops associated to this module will apply modifications made here').
				'</p>';
	}	
			
    public function _cleanText($text)
    {
        $text = strip_tags($text, '<a><b><blockquote><code><del><dd><dl><dt><em><h1><h2><h3><i><kbd><p><pre><s><sup><strong><strike><br><hr>');
        $text = trim($text);
        return $text;
    }

    public function _trimEncode($text)
    {
        $str = strip_tags($text);
        $str = preg_replace('/\s(?=\s)/', '', $str);
        $str = preg_replace('/[\n\r\t]/', '', $str);
        $str = str_replace(' ', '', $str);
        $str = trim($str, "\xC2\xA0\n");
        return $str;
    }

    public function parseTarget($type = '_self')
    {
        $target = '';
        switch ($type) {
            default:
            case '_self':
                break;
            case '_blank':
            case '_parent':
            case '_top':
                $target = 'target="' . $type . '"';
                break;
            case '_windowopen':
                $target = "onclick=\"window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,false');return false;\"";
                break;
            case '_modal':
                break;
        }
        return $target;
    }

    /**
     * Truncate string by $length
     * @param string $string
     * @param int $length
     * @param string $etc
     * @return string
     */
    public function truncate($string, $length, $etc = '...')
    {
        return defined('MB_OVERLOAD_STRING') ? $this->_mb_truncate($string, $length, $etc) : $this->_truncate($string, $length, $etc);
    }

    /**
     * Truncate string if it's size over $length
     * @param string $string
     * @param int $length
     * @param string $etc
     * @return string
     */
    private static function _truncate($string, $length, $etc = '...')
    {
        if ($length > 0 && $length < strlen($string)) {
            $buffer = '';
            $buffer_length = 0;
            $parts = preg_split('/(<[^>]*>)/', $string, -1, PREG_SPLIT_DELIM_CAPTURE);
            $self_closing_tag = split(',', 'area,base,basefont,br,col,frame,hr,img,input,isindex,link,meta,param,embed');
            $open = array();
            foreach ($parts as $i => $s) {
                if (false === strpos($s, '<')) {
                    $s_length = strlen($s);
                    if ($buffer_length + $s_length < $length) {
                        $buffer .= $s;
                        $buffer_length += $s_length;
                    } else if ($buffer_length + $s_length == $length) {
                        if (!empty($etc)) {
                            $buffer .= ($s[$s_length - 1] == ' ') ? $etc : " $etc";
                        }
                        break;
                    } else {
                        $words = preg_split('/([^\s]*)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);
                        $space_end = false;
                        foreach ($words as $w) {
                            if ($w_length = strlen($w)) {
                                if ($buffer_length + $w_length < $length) {
                                    $buffer .= $w;
                                    $buffer_length += $w_length;
                                    $space_end = (trim($w) == '');
                                } else {
                                    if (!empty($etc)) {
                                        $more = $space_end ? $etc : " $etc";
                                        $buffer .= $more;
                                        $buffer_length += strlen($more);
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                } else {
                    preg_match('/^<([\/]?\s?)([a-zA-Z0-9]+)\s?[^>]*>$/', $s, $m);
                    if (empty($m[1]) && isset($m[2]) && !in_array($m[2], $self_closing_tag)) {
                        array_push($open, $m[2]);
                    } else if (trim($m[1]) == '/') {
                        $tag = array_pop($open);
                        if ($tag != $m[2]) {
                        }
                    }
                    $buffer .= $s;
                }
            }
            // close tag openned.
            while (count($open) > 0) {
                $tag = array_pop($open);
                $buffer .= "</$tag>";
            }
            return $buffer;
        }
        return $string;
    }

    /**
     * Truncate mutibyte string if it's size over $length
     * @param string $string
     * @param int $length
     * @param string $etc
     * @return string
     */
    private function _mb_truncate($string, $length, $etc = '...')
    {
        $encoding = mb_detect_encoding($string);
        if ($length > 0 && $length < mb_strlen($string, $encoding)) {
            $buffer = '';
            $buffer_length = 0;
            $parts = preg_split('/(<[^>]*>)/', $string, -1, PREG_SPLIT_DELIM_CAPTURE);
            $self_closing_tag = explode(',', 'area,base,basefont,br,col,frame,hr,img,input,isindex,link,meta,param,embed');
            $open = array();
            foreach ($parts as $i => $s) {
                if (false === mb_strpos($s, '<')) {
                    $s_length = mb_strlen($s, $encoding);
                    if ($buffer_length + $s_length < $length) {
                        $buffer .= $s;
                        $buffer_length += $s_length;
                    } else if ($buffer_length + $s_length == $length) {
                        if (!empty($etc)) {
                            $buffer .= ($s[$s_length - 1] == ' ') ? $etc : " $etc";
                        }
                        break;
                    } else {
                        $words = preg_split('/([^\s]*)/', $s, -1, PREG_SPLIT_DELIM_CAPTURE);
                        $space_end = false;
                        foreach ($words as $w) {
                            if ($w_length = mb_strlen($w, $encoding)) {
                                if ($buffer_length + $w_length < $length) {
                                    $buffer .= $w;
                                    $buffer_length += $w_length;
                                    $space_end = (trim($w) == '');
                                } else {
                                    if (!empty($etc)) {
                                        $more = $space_end ? $etc : " $etc";
                                        $buffer .= $more;
                                        $buffer_length += mb_strlen($more);
                                    }
                                    break;
                                }
                            }
                        }
                        break;
                    }
                } else {
                    preg_match('/^<([\/]?\s?)([a-zA-Z0-9]+)\s?[^>]*>$/', $s, $m);
                    if (empty($m[1]) && isset($m[2]) && !in_array($m[2], $self_closing_tag)) {
                        array_push($open, $m[2]);
                    } else if (trim($m[1]) == '/') {
                        $tag = array_pop($open);
                        if ($tag != $m[2]) {
                        }
                    }
                    $buffer .= $s;
                }
            }
            // close tag openned.
            while (count($open) > 0) {
                $tag = array_pop($open);
                $buffer .= "</$tag>";
            }
            return $buffer;
        }
        return $string;
    }
}
