<?php
/**
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

include_once ( '../../config/config.inc.php' );
include_once ( '../../init.php' );
include_once ( 'pttabs.php' );
$context = Context::getContext ();
$pttabs = new PtTabs();
echo $pttabs->callAjax();