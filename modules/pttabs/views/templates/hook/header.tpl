{*
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *}

{strip}
    {addJsDefL name=min_item|escape:'html':'UTF-8'}{l s='Please select at least one product' js=1 mod='pttabs'}{/addJsDefL}
    {addJsDefL name=max_item|escape:'html':'UTF-8'}{l s='You cannot add more than %d product(s) to the product comparison' sprintf=$comparator_max_item js=1 mod='pttabs'}{/addJsDefL}
    {addJsDef comparator_max_item=$comparator_max_item}
    {addJsDef comparedProductsIds=$compared_products}
{/strip}