{*
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *}


{if isset($list) && !empty($list)}
    {if isset($list) && !empty($list)}
        {foreach from=$list item=items}
            {$_list = $items->products}
			
			{math equation='rand()' assign='rand'}
			{assign var='randid' value="now"|strtotime|cat:$rand}
			{assign var="tag_id" value="pt_tabs_{$items->id_module}_{$randid}"}

            <div id="{$tag_id|escape:'html':'UTF-8'}" class="pt-tabs-container">
				<span class="module-title">
					<h3>
						{$items->title_module[$cookie->id_lang]}
					</h3>
				</span>

                <div class="pt-tabs-container-inner">
				
					<!--Begin Tabs-->
					<div class="list-tabs-wrap"
						data-ajaxurl="{$base_dir|escape:'html':'UTF-8'}"
						data-moduleid="{$items->id_module|escape:'html':'UTF-8'}">
						<ul class="list-tabs-inner">
							{foreach $_list as $tab}
								{assign var="tab_active" value=(isset($tab['sel']) && $tab['sel'] == 'sel')?' active' : ''}
								{assign var="tab_all" value=($tab['id_category'] == '*')?' tab-all':''}
								{assign var="active_content" value=($tab['id_category'] == '*')?'all':$tab['id_category'] }
								{if $items->params.filter_type == 'categories'}
									<li class="tab-item{$tab_active|escape:'html':'UTF-8'} {$tab_all|escape:'html':'UTF-8'}"
										data-category="{$tab['id_category']|escape:'html':'UTF-8'}"
										data-content=".items-category-{$active_content|escape:'html':'UTF-8'}">
										<span>{$tab['name']|escape:'html':'UTF-8'}</span>
									</li>
								{else}
									<li class="tab-item{$tab_active|escape:'html':'UTF-8'}"
										data-category="{$tab['id_category']|escape:'html':'UTF-8'}"
										data-content=".items-category-{$active_content|escape:'html':'UTF-8'}">
										<span>{$tab['name']|escape:'html':'UTF-8'}</span>
									</li>
								{/if}
							{/foreach}
						</ul>						
					</div>
					<!-- End Tabs-->

					<!--Begin Content-->
					<div class="list-items-wrap">
						{foreach $_list as $item}
							{assign var="child_items" value=(isset($item['child']))?$item['child']:''}
							{assign var="active" value=(isset($item['sel']) && $item['sel'] == 'sel')?' active':''}
							{assign var="tab_all" value=($item['id_category'] == "*")?' items-category-all':' items-category-'|cat:$item['id_category']}
							<div class="list-items {$active|escape:'html':'UTF-8'} {$tab_all|escape:'html':'UTF-8'}">
								{if !empty($child_items)}
									{include file="./product.tpl"}
								{else}
									<div class="image-loading"></div>
								{/if}
							</div>
						{/foreach}
					</div>
					<!-- End Tabs-->
					
                </div>
                {include file="./default_js.tpl"}
            </div>
        {/foreach}
    {else}
        {l s='Has no content to show.' mod='pttabs'}
    {/if}
{/if}





