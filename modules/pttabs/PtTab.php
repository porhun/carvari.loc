<?php
/**
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */


class PtTab extends ObjectModel
{
    public $id_module;
    public $title_module;
    public $short_desc;
    public $identifier_block;
    public $active;
    public $hook;
    public $params;
    public $ordering;
	
	/**
	 * @see ObjectModel::$definition
	 */	
    public static $definition = array(
		'table' => 'pttabs', 
		'primary' => 'id_module', 
		'multilang' => true, 
		'fields' => array(
			'hook' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
			'title_module' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 255), 
			'active' => array('type' => self::TYPE_INT, 'shop' => true, 'validate' => 'isunsignedInt'), 
			'params' => array( 'type' => self::TYPE_HTML, 'validate' => 'isString' ),
			'ordering' => array('type' => self::TYPE_INT, 'validate' => 'isInt')
		)
	);

	public function __construct($id_module = null, $id_lang = null, $id_shop = null)
	{
		Shop::addTableAssociation ('pttabs', array('type' => 'shop'));
		parent::__construct ($id_module, $id_lang, $id_shop);
	}

	public function add($autodate = true, $null_values = false)
	{
		$this->ordering = $this->getOrdering () + 1;
		$res = parent::add($autodate, $null_values);
		return $res;
	}
	
	public function delete()
	{
		$res = true;
		$res &= $this->reOrderPositions();
		$res &= parent::delete();
		return $res;
	}
	
	public function duplicate($autodate = true)
	{
		$this->ordering = $this->getOrdering () + 1;
		$return = parent::add ($autodate, true);
		return $return;
	}
	
	public function getModuleId()
	{
		$sql = 'SELECT MAX(`id_module`)
				FROM `'._DB_PREFIX_.'pttabs`';
		$id_module = DB::getInstance ()->getValue($sql);
		return ( is_numeric ($id_module) )?$id_module:1;
	}

	public function getOrdering()
	{
		$sql = 'SELECT MAX(`ordering`)
				FROM `'._DB_PREFIX_.'pttabs`';
		$ordering = DB::getInstance ()->getValue ($sql);
		return ( is_numeric ($ordering) )? $ordering : 0;
	}

	public function reOrderPositions()
	{
		$id_module = $this->id;
		$context = Context::getContext();
		$id_shop = $context->shop->id;

		$max = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT MAX(cs.`ordering`) as ordering
			FROM `'._DB_PREFIX_.'pttabs` cs, `'._DB_PREFIX_.'pttabs_shop` css
			WHERE css.`id_module` = cs.`id_module` AND css.`id_shop` = '.(int)$id_shop
		);

		if ((int)$max == (int)$id_module)
			return true;

		$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT cs.`ordering` as ordering, cs.`id_module` as id_module
			FROM `'._DB_PREFIX_.'pttabs` cs
			LEFT JOIN `'._DB_PREFIX_.'pttabs_shop` css ON (css.`id_module` = cs.`id_module`)
			WHERE css.`id_shop` = '.(int)$id_shop.' AND cs.`ordering` > '.(int)$this->ordering
		);

		foreach ($rows as $row)
		{
			$customs = new PtTab($row['id_module']);
			--$customs->ordering;
			$customs->update();
			unset($customs);
		}

		return true;
	}
	
	public static function getAssociatedIdsShop($id_module)
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT css.`id_shop`
			FROM `'._DB_PREFIX_.'pttabs` cs
			LEFT JOIN `'._DB_PREFIX_.'pttabs_shop` css ON (css.`id_module` = cs.`id_module`)
			WHERE cs.`id_module` = '.(int)$id_module
		);

		if (!is_array($result))
			return false;

		$return = array();

		foreach ($result as $id_shop)
			$return[] = (int)$id_shop['id_shop'];
		return $return;
	}
	
}
