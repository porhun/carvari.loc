<?php
/**
 * @package PT Tabs
 * @version 1.0.0
 * @copyright (c) 2015 PathThemes. (http://www.paththemes.com)
 * @license http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

include_once('../../config/config.inc.php');
include_once('../../init.php');
include_once('pttabs.php');

$context = Context::getContext();
$pttabs = new PtTabs();
$items = array();

if (!Tools::isSubmit('secure_key') || Tools::getValue('secure_key') != $pttabs->secure_key || !Tools::getValue('action'))
    die(1);

if (Tools::getValue('action') == 'updateSlidesPosition' && Tools::getValue('item')) {
    $items = Tools::getValue('item');
    $pos = array();
    $success = true;
    foreach ($items as $position => $item) {
        $success = Db::getInstance()->execute('
			UPDATE `' . _DB_PREFIX_ . 'pttabs` SET `ordering` = ' . (int)$position . '
			WHERE `id_module` = ' . (int)$item);
        $pos[] = $position;
    }

    if (!$success)
        die(Tools::jsonEncode(array(
            'error' => 'Update Fail'
        )));
    die(Tools::jsonEncode(array(
        'success' => 'Update Success',
        'error' => false
    )));
}