<div class="panel">
    <h3>Product label</h3>
    <div class="alert alert-info">
        Здесь вы можете задать дополнительную наклейку на продукт. Приоритет выше чем в настройках модуля.
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3" for="uploadable_files">
            {*<span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="">*}
            Выбрать картинку:
            {*</span>*}
        </label>
        <div class="col-lg-3">
            <select name="label[id_label]">
                <option value="0">Не выбрано</option>
                {foreach from=$labels item=label}
                    <option value="{$label.id_label}"{if $label.id_label==$current_label.id_label} selected{/if}>{$label.title}</option>
                {/foreach}
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3" for="uploadable_files">
            <span class="label-tooltip" data-toggle="tooltip" title="" data-original-title="Расположение наклейки на картинке товара.">
            Выбрать расположение:
            </span>
        </label>
        <div class="col-lg-3">
            <select name="label[label_position]">
                <option value="1"{if $current_label.label_position==1} selected{/if}>Вверху слева</option>
                <option value="2"{if $current_label.label_position==2} selected{/if}>Вверху справа</option>
                <option value="3"{if $current_label.label_position==3} selected{/if}>Внизу слева</option>
                <option value="4"{if $current_label.label_position==4} selected{/if}>Внизу справа</option>
            </select>
        </div>
    </div>

    </div>
    <div class="panel-footer">
        <a href="index.php?controller=AdminProducts&token={$token}" class="btn btn-default"><i class="process-icon-cancel"></i> Cancel</a>
        <button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> Save</button>
        <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> Save and stay</button>
    </div>
</div>