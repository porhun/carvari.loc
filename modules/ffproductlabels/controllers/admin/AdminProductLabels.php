<?php

class AdminProductLabelsController extends AdminController
{

    public function __construct()
    {
        $this->module_name = 'ffproductlabels';

        $this->bootstrap = true;
        $this->table = 'ffproductlabels';
        $this->lang = false;
        $this->addRowAction('view');
        $this->explicitSelect = true;
        $this->deleted = false;
        $this->context = Context::getContext();

        $this->img_dir = _PS_IMG_DIR_.'labels/';
        $this->img_dir_name = 'labels';
        $this->imageType = 'png';

        $this->image_sizes = array(
            array(
                'name' => 'big',
                'width' => 120,
                'height' => 120,
            ),
            array(
                'name' => 'small',
                'width' => 80,
                'height' => 80,
            ),
        );

        $this->label_positions = array(
            array('label_position' => 1, 'name' => 'Вверху слева'),
            array('label_position' => 2, 'name' => 'Вверху справа'),
            array('label_position' => 3, 'name' => 'Внизу слева'),
            array('label_position' => 4, 'name' => 'Внизу справа'),
        );

//        $this->filter = false;
        $this->_select = '`id_label`, CONCAT(`id_label`, "-imgbig") as image';
        $this->_orderBy = 'id_label';
        $this->identifier = 'id_label';
        $this->actions= array('edit', 'delete');
        $this->show_form_cancel_button = true;


        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'icon' => 'icon-trash',
                'confirm' => $this->l('Delete selected items?')
            )
        );

        $this->fields_list = array(
            'id_label' => array(
                'title' => $this->l('Id'),
                'width' => 140,
                'type' => 'text',
                'orderby' => false,
                'search' => false,
            ),
            'title' => array(
                'title' => $this->l('Name'),
                'width' => 140,
                'type' => 'text',
                'orderby' => false,
                'search' => false,
            ),
            'image' => array(
                'title' => $this->l('Image'),
                'width' => 140,
                'image' => $this->img_dir_name,
                'image_id' => 'image',
                'orderby' => false,
                'search' => false,
            ),
        );

        $this->fields_form =  array(
            'legend' => array(
                'title' => 'Добавить новую иконку для товаров',
                'enctype' => "multipart/form-data"
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'name' => 'label[title]',
                    'label' => 'Название',
                ),
                array(
                    'type' => 'file',
                    'id' => 'imgbig',
                    'label' => 'Большая иконка',
                    'desc' => 'Минимальный размер - 45px*25рх',
                    'name' => 'imgbig',
                    'required' => true,
                    'hint' => $this->l('Иконка для страницы товара.'),
                    'files' => array(),
                ),
                array(
                    'type' => 'file',
                    'id' => 'imgsmall',
                    'label' => 'Маленькая иконка',
                    'desc' => 'Минимальный размер - 45px*25рх',
                    'name' => 'imgsmall',
                    'required' => true,
                    'hint' => $this->l('Иконка для списка товаров.'),
                    'files' => array(),
                ),

            ),

            'submit' => array(
                'title' => 'Cохранить',
                'name' => isset($_GET['update'.$this->table]) ? 'editLabel' : 'addLabel'
            ),
        );


        parent::__construct();
    }
    public function setMedia()
    {
        parent::setMedia();
        $this->addJS(_PS_MODULE_DIR_.$this->module_name.'/views/js/ffproductlabels.js');
    }

    protected function setCriteriaFieldsForm(){

        $this->table = 'ffproductlabels_criteria';
        $this->identifier = 'id_criteria';

        $this->fields_form =  array(
            'legend' => array(
                'title' => 'Изменить критерий',
                'enctype' => "multipart/form-data"
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'name' => 'criteria[title]',
                    'label' => 'Название',
                    'readonly' => true,
                ),
                array(
                    'type' => 'text',
                    'name' => 'criteria[description]',
                    'label' => 'Описание критерия',
                    'readonly' => true,
                ),
                array(
                    'type' => 'select',
                    'name' => 'criteria[id_label]',
                    'label' => 'Используемая иконка',
                    'onchange' => 'change_criteria_img(this.value);',
                    'options' => array(
                        'query' => Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'ffproductlabels'),
                        'id' => 'id_label',
                        'name' => 'title',
                        'default' => array(
                            'label' => 'Не выбрано',
                            'value' => 0,
                        ),
                    )
                ),
                array(),
                array(
                    'type' => 'select',
                    'name' => 'criteria[label_position]',
                    'label' => 'Место расположение иконки',
                    'options' => array(
                        'query' => $this->label_positions,
                        'id' => 'label_position',
                        'name' => 'name'
                    )
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Enable'),
                    'name' => 'criteria[active]',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),
                array(
                    'type' => 'hidden',
                    'name' => 'criteria[id_criteria]',
                ),

            ),

            'submit' => array(
                'title' => 'Cохранить',
                'name' => 'editCriteria'
            ),
        );

    }

    protected function getCriteriaFieldsList(){
        $result = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'ffproductlabels_criteria');
        $label_array = array();
        foreach ($result as $row)
            $label_array[$row['id_label']] = $row['title'];

        return array(
            'id_criteria' => array(
                'title' => $this->l('Id'),
                'type' => 'text',
                'orderby' => false,
                'search' => false,

            ),
//            'label' => array(
//                'title' => 'Используемая иконка',
//                'type' => 'select',
//                'list' => $label_array,
//                'filter_key' => 'id_label',
//            ),
            'title' => array(
                'title' => $this->l('Name'),
                'type' => 'text',
                'orderby' => false,
                'search' => false,
            ),
            'description' => array(
                'title' => $this->l('Description'),
                'type' => 'text',
                'orderby' => false,
                'search' => false,
            ),
            'position' => array(
                'title' => $this->l('Position'),
//                'filter_key' => 'sa!position',
                'position' => 'position',
                'orderby' => false,
                'search' => false,
            ),
            'active' => array(
                'title' => $this->l('Status'),
                'align' => 'center',
                'active' => 'status',
                'type' => 'bool',
                'class' => 'fixed-width-sm',
                'orderby' => false,
                'search' => false,
                'ajax' => true,
            ),
//            'image' => array(
//                'title' => $this->l('Image'),
//                'image' => $this->img_dir_name,
//                'image_id' => 'id_label',
//                'orderby' => false,
//                'search' => false,
//            ),
        );
    }

    public function initProcess()
    {
        $redirect = false;

        if (Tools::isSubmit('addLabel')) {
            $data = Tools::getValue('label');
            $connector = Db::getInstance();
            $connector->insert($this->table, array('title' => pSQL($data['title'])));
            $id_label = $connector->Insert_ID();
            $this->postImage($id_label);
            $redirect = true;
        } elseif (Tools::isSubmit('editLabel')) {
            $data = Tools::getValue('label');
            $id_label = (int)$data['id_label'];
            Db::getInstance()->update('ffproductlabels', array(
                'title' => pSQL($data['title']),
            ), 'id_label=' . $id_label);
            $this->postImage($id_label);
            $redirect = true;
        }elseif(Tools::isSubmit('editCriteria')) {
            $data = Tools::getValue('criteria');
            $id_criteria = (int)$data['id_criteria'];
            Db::getInstance()->update('ffproductlabels_criteria', array(
                'title' => pSQL($data['title']),
                'description' => pSQL($data['description']),
                'id_label' => pSQL($data['id_label']),
                'label_position' => pSQL($data['label_position']),
                'active' => pSQL($data['active']),
            ), 'id_criteria=' . $id_criteria);
            $this->cleanCache();
            $redirect = true;
        } elseif (Tools::getIsset('deleteffproductlabels')) {
            $id_label = (int)Tools::getValue('id_label');
            $this->deleteLabel($id_label);
            $this->cleanCache();
            $redirect = true;
        } elseif (Tools::getIsset('updateffproductlabels_criteria')) {
            $this->renderCriteriaForm();
        } elseif (Tools::getValue('deleteImg')) {
            $id_label = (int)Tools::getValue('id_label');
            $this->deleteImages($id_label, Tools::getValue('deleteImg'));
        }

        if ($redirect)
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminProductLabels', false).'&token='.Tools::getAdminTokenLite('AdminProductLabels'));

        parent::initProcess();
    }

    public function renderLabelsList(){
        $this->informations = array(
            $this->l('При удаление ярлычка, он удалится из все товаров и критериев.'),
        );
        $this->content .= parent::renderList();
    }

    public function renderCriteriaForm(){
        $this->setCriteriaFieldsForm();
//        return parent::renderForm();
    }

    public function renderCriteriaList(){
        $this->toolbar_title = 'Список стандартных критериев';
        // reset actions and query vars
        $this->actions = array();
        unset($this->fields_list, $this->_select, $this->_join, $this->_group, $this->_filterHaving, $this->_filter);

        $this->table = 'ffproductlabels_criteria';
        $this->list_id = 'address';
        $this->identifier = 'id_criteria';

        $this->position_identifier = 'position';
        $this->_orderBy = 'position';
        $this->actions = array('edit');
//        $this->list_simple_header = true;
        $this->bulk_actions = false;


        $this->fields_list = $this->getCriteriaFieldsList();

        $this->context->smarty->assign('title_list', $this->l('Manufacturers addresses'));


        $this->_select = 'id_label';

        // call postProcess() for take care about actions and filters
        $this->postProcess();

        $this->initToolbar();

        $this->content .= parent::renderList();
    }

    public function renderList()
    {
        $this->renderLabelsList();
        $this->renderCriteriaList();
    }

    public function renderForm()
    {
        $this->informations = array(
            $this->l('Ярлычки нужно загружать уже необходимых размеров.'),
        );
        return parent::renderForm();
    }

    public function getFieldsValue($obj)
    {
        if (Tools::getIsset('updateffproductlabels')) {
            $label = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ffproductlabels` WHERE id_label='.(int)Tools::getValue('id_label'));
            if ($this->display == 'edit') {
                $image = $this->img_dir.$label['id_label'].'-imgbig.'.$this->imageType;
                $image_url = ImageManager::thumbnail($image, $this->table.'_big_'.$label['id_label'].'.'.$this->imageType, 200,
                    $this->imageType, true, true);
                $image_size = file_exists($image) ? filesize($image) / 1000 : false;

                if ($image_size) {
                    $this->fields_form[0]['form']['input'][1]['files'] = array(
                        array(
                            'type' => 'image',
                            'image' => $image_url ? $image_url : false,
                            'size' => $image_size,
                            'delete_url' => AdminController::$currentIndex.'&deleteImg=imgbig&id_label='.(int)Tools::getValue('id_label').'&updateffproductlabels&token='.Tools::getAdminTokenLite($this->controller_name)
                        )
                    );
                }

                $image = $this->img_dir.$label['id_label'].'-imgsmall.'.$this->imageType;
                $image_url = ImageManager::thumbnail($image, $this->table.'_small_'.$label['id_label'].'.'.$this->imageType, 200,
                    $this->imageType, true, true);
                $image_size = file_exists($image) ? filesize($image) / 1000 : false;

                if ($image_size) {
                    $this->fields_form[0]['form']['input'][2]['files'] = array(
                        array(
                            'type' => 'image',
                            'image' => $image_url ? $image_url : false,
                            'size' => $image_size,
                            'delete_url' => AdminController::$currentIndex.'&deleteImg=imgsmall&id_label='.(int)Tools::getValue('id_label').'&updateffproductlabels&token='.Tools::getAdminTokenLite($this->controller_name)
                        )
                    );
                }


                $this->fields_form[0]['form']['input'][] = array(
                    'type' => 'hidden',
                    'name' => 'label[id_label]',
                );

            }

            return array(
                'label[title]' => $label['title'],
                'label[id_label]' => $label['id_label'],
            );
        } elseif(Tools::getIsset('updateffproductlabels_criteria')){
            $criteria = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ffproductlabels_criteria` WHERE id_criteria='.(int)Tools::getValue('id_criteria'));

            $image = $this->img_dir.$criteria['id_label'].'-imgbig.'.$this->imageType;
            $image_url = ImageManager::thumbnail($image, $this->table.'_big_'.$criteria['id_label'].'.'.$this->imageType, 200,
                $this->imageType, true, true);
            $image_size = file_exists($image) ? filesize($image) / 1000 : false;


//            d(FeatureValue::getFeatureValuesWithLang( $this->context->language->id, 1));
            if ($criteria['criteria_method'] == 'criteriaNewCollection') {

                $this->fields_form[0]['form']['input'][] =
                    array(
                        'label' => 'Год коллекции',
                        'type' => 'select',
                        'name' => 'criteria_year',
                        'options' => array(
                            'id' => 'id_feature_value',
                            'name' => 'value',
                            'query' => FeatureValue::getFeatureValuesWithLang( $this->context->language->id, 2),
                        )

                    );

                $this->fields_form[0]['form']['input'][] =
                    array(
                        'label' => 'Сезон коллекции',
                        'type' => 'select',
                        'name' => 'criteria_season',
                        'options' => array(
                            'id' => 'id_feature_value',
                            'name' => 'value',
                            'query' => FeatureValue::getFeatureValuesWithLang( $this->context->language->id, 1),
                        )

                    );

            }

            if ($image_size) {

                $this->fields_form[0]['form']['input'][3] =
                    array(
                        'label' => 'Картинка используемой иконки',
                        'type' => 'html',
                        'html_content' => $image_url ? '<div id="label-img">'.$image_url.'</div>' : false,
                        'name' => 'criteria[none]',

                );
            } else {
                unset($this->fields_form[0]['form']['input'][3]);
            }

            return array(
                'criteria_year' => Configuration::get($this->module_name.'_criteria_year'),
                'criteria_season' => Configuration::get($this->module_name.'_criteria_season'),
                'criteria[id_criteria]' => $criteria['id_criteria'],
                'criteria[id_label]' => $criteria['id_label'],
                'criteria[title]' => $criteria['title'],
                'criteria[description]' => $criteria['description'],
                'criteria[label_position]' => $criteria['label_position'],
                'criteria[active]' => $criteria['active'],
            );
        }

    }

    public function postProcess()
    {
        if (Tools::isSubmit('editCriteria')) {
            if (Tools::getValue('criteria_year')) {
                Configuration::updateValue($this->module_name.'_criteria_year', Tools::getValue('criteria_year'));
            }
            if (Tools::getValue('criteria_season')) {
                Configuration::updateValue($this->module_name.'_criteria_season', Tools::getValue('criteria_season'));
            }
        }

        parent::postProcess();
    }

    /**
     * Delete multiple items
     *
     * @return boolean true if succcess
     */
    protected function processBulkDelete()
    {
        if( $delete_array = Tools::getValue('ffproductlabelsBox')){
            foreach ($delete_array as $id_label){
                $this->deleteLabel($id_label);
            }
        }
        $this->cleanCache();
        Tools::redirectAdmin($this->context->link->getAdminLink('AdminProductLabels', false).'&token='.Tools::getAdminTokenLite('AdminProductLabels'));
    }

    protected function deleteImages($id_label, $size)
    {
        unlink($this->img_dir.$id_label.'-'.$size.'.'.$this->imageType);
    }

    protected function deleteLabel($id_label)
    {
        unlink($this->img_dir.$id_label.'-imgbig.'.$this->imageType);
        unlink($this->img_dir.$id_label.'-imgsmall.'.$this->imageType);

        Db::getInstance()->delete('ffproductlabels','id_label='.$id_label);
        Db::getInstance()->delete('ffproductlabels_link','id_label='.$id_label);
        Db::getInstance()->update('ffproductlabels_criteria', array('id_label' => 0),'id_label='.$id_label);
    }

    protected function postImage($id)
    {
        $ret = true;

//        d($_FILES);
        foreach ($_FILES as $filed_name=>$file) {
            if(!$file['error']){

                if ($filed_name == 'imgbig') {
                    $cache_image = $this->table.'_mini_'.$id.'-imgbig_'.$this->context->shop->id.'.'.$this->imageType;
                    if (file_exists(_PS_TMP_IMG_DIR_.$cache_image))
                        @unlink(_PS_TMP_IMG_DIR_.$cache_image);
                }

                $ret &= $this->uploadImage($id, $filed_name, $this->img_dir);

            }
        }


        return $ret;
    }

    protected function uploadImage($id, $name, $dir, $ext = false, $width = null, $height = null)
    {


        if (isset($_FILES[$name]['tmp_name']) && !empty($_FILES[$name]['tmp_name']))
        {

            // Check image validity
            $max_size = isset($this->max_image_size) ? $this->max_image_size : 0;
            if ($error = ImageManager::validateUpload($_FILES[$name], Tools::getMaxUploadSize($max_size)))
                $this->errors[] = $error;

            $tmp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
            if (!$tmp_name)
                return false;

            if (!move_uploaded_file($_FILES[$name]['tmp_name'], $dir.$id.'-'.$name.'.'.$this->imageType))
                return false;

            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
//            if (!ImageManager::checkImageMemoryLimit($tmp_name))
//                $this->errors[] = Tools::displayError('Due to memory limit restrictions, this image cannot be loaded. Please increase your memory_limit value via your server\'s configuration settings. ');

            // Copy new image
//            if (empty($this->errors) && !ImageManager::resize($tmp_name, _PS_IMG_DIR_.$dir.$id.'.'.$this->imageType, (int)$width, (int)$height, ($ext ? $ext : $this->imageType)))
//                $this->errors[] = Tools::displayError('An error occurred while uploading the image.');

            if (count($this->errors))
                return false;

            unlink($tmp_name);
            return true;
        }
        return true;
    }

    public function ajaxProcessUpdatePositions()
    {
        $positions = Tools::getValue('criteria');

        $position_array = array();
        if (is_array($positions))
            foreach ($positions as $key => $value)
            {
                $pos = explode('_', $value);
                if (isset($pos[2]))
                {
                    Db::getInstance()->update('ffproductlabels_criteria', array('position' => $key), 'id_criteria='.(int)$pos[2]);
                    $position_array[$pos[2]] = $key;
                }
            }

        $this->cleanCache();
        die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The positions has been updated successfully'))));
    }

    public function ajaxProcessStatusffproductlabelsCriteria()
    {
        Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffproductlabels_criteria` SET `active` = abs(`active` - 1) WHERE `id_criteria`='.(int)Tools::getValue('id_criteria'));
        $this->cleanCache();
        die(Tools::jsonEncode(array('success' => true, 'text' => $this->l('The status has been updated successfully'))));
    }

    protected function cleanCache(){
        Cache::clean('label_criteriaList');
    }
}