<?php
if (!defined('_PS_VERSION_'))
    exit;

class Ffproductlabels extends Module
{
    private static $module_tab = array(
        'class_name' => 'AdminProductLabels',
        'name' => array(
            'default' => 'Product Labels',
            'en' => 'Product Labels',
            'ru' => 'Ярлычки товаров',
            'uk' => 'Ярлички товарів',
        ),
        'module' => false,
        'id_parent' => false
    );

    private static $module_languages;

    public function __construct()
    {
        $this->name = 'ffproductlabels';
        $this->tab = 'content_management';
        $this->version = '1.0.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Product Labels');
        $this->description = $this->l('Product Labels - additional product labels.');

        self::$module_languages = Language::getLanguages();

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        $this->errors = array();
        $this->img_dir = _PS_IMG_DIR_.'labels/';
        $this->img_dir_name = 'labels';
        $this->imageType = 'png';

        $this->label_positions = array(
            1 => 'left_up',
            2 => 'right_up',
            3 => 'left_bottom',
            4 => 'right_bottom',
        );
    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!is_dir($this->img_dir))
            mkdir($this->img_dir);

        if (!parent::install()
            || !$this->createTab()
            || !$this->registerHook('displayHeader')
            || !$this->registerHook('displayAdminProductsExtra')
            || !$this->registerHook('actionProductUpdate')
            || !$this->registerHook('displayProductLabel')
            || !$this->installSql())
            return false;
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() || !$this->deleteTab() || !$this->uninstallSql())
            return false;
        return true;
    }

    private function installSql(){

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffproductlabels` (
			`id_label` INT NOT NULL AUTO_INCREMENT,
			`title` VARCHAR(255),
			`label_position` tinyint(4),
		PRIMARY KEY (`id_label`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffproductlabels_link` (
			`id_product` INT,
			`id_label` INT,
			`label_position` tinyint(1),
		PRIMARY KEY (`id_product`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffproductlabels_criteria` (
			`id_criteria` INT NOT NULL AUTO_INCREMENT,
			`id_label` INT,
			`title` varchar(40),
			`description` tinytext,
			`criteria_method` varchar(40),
			`position` tinyint(4),
			`label_position` tinyint(1),
			`active` tinyint(1),
		PRIMARY KEY (`id_criteria`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');


        $criteria_count = Db::getInstance()->getValue('SELECT COUNT(*) FROM `'._DB_PREFIX_.'ffproductlabels_criteria`');
        if (!$criteria_count) {
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("1", "",
                                        "' . $this->l('Последний размер (атрибут)') . '",
                                        "' . $this->l('Показывает ярлычек у товаров, у которых есть атрибут размер и остался только один размер.') . '",
                                        "criteriaLastSize",
                                        0,
                                        1,
                                        0
                                        )');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("2", "",
                                        "'.$this->l('Топ продаж').'",
                                        "'.$this->l('Ярлычек показывает у товаров c наибольшими продажами. Берем все товары за последние 60 дней (как мы делали для вывода на главную).').'",
                                        "criteriaTopSells",
                                        1,
                                        1,
                                        0
                                        )');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("3", "",
                                        "' . $this->l('Суперскидка') . '",
                                        "' . $this->l('Товары, где скидка 50% и больше.') . '",
                                        "criteriaSuperDiscount",
                                        2,
                                        1,
                                        0
                                        )');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("4", "",
                                        "' . $this->l('Скидка') . '",
                                        "' . $this->l('Ярлычек показывает у товаров, у которых есть скидка. Нужно ставить после Суперскидки.') . '",
                                        "criteriaDiscount",
                                        3,
                                        1,
                                        0
                                        )');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("5", "",
                                        "'.$this->l('Новый товар').'",
                                        "'.$this->l('Ярлычек показывает у товаров, которые были недавно добавленные. Период, берется из настроек магазина.').'",
                                        "criteriaIsNew",
                                        4,
                                        1,
                                        0
                                        )');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("6", "",
                                        "'.$this->l('Распродажа').'",
                                        "'.$this->l('Ярлычек показывает у товаров, у которых стоит отметка как распродажа.').'",
                                        "criteriaOnSale",
                                        5,
                                        1,
                                        0
                                        )');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("", "",
                                        "'.$this->l('Новая коллекция').'",
                                        "'.$this->l('Ярлычек показывает у товаров, которые находятся в последней коллекции.').'",
                                        "criteriaNewCollection",
                                        6,
                                        1,
                                        0
                                        )');
        }


        return true;
    }

    private function uninstallSql(){
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'ffproductlabels`');
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'ffproductlabels_link`');
        Db::getInstance()->execute('DROP TABLE `'._DB_PREFIX_.'ffproductlabels_criteria`');
        return true;
    }

    public function createTab()
    {
        if ($this->tabExists())
            return true;

        $parent_id = Tab::getIdFromClassName('AdminCatalog');

        if (!$parent_id)
        {
            $this->_errors[] = $this->l('Invalid parent tab selected');

            return false;
        }

        $tab = new Tab();

        foreach (self::$module_tab as $k => $v)
        {
            if (property_exists($tab, $k))
            {
                switch ($k)
                {
                    case 'name':
                        $v = self::getMultilangField($v);
                        break;
                    case 'module':
                        $v = $this->name;
                        break;
                    case 'id_parent':
                        $v = $parent_id;
                        break;
                }

                $tab->{$k} = $v;
            }
        }

        if (!$tab->add())
        {
            $this->_errors[] = $this->l('Unable to create Product label tab');
            return false;
        }

        return true;
    }

    private static function getTabId()
    {
        return Tab::getIdFromClassName(self::$module_tab['class_name']);
    }

    private function deleteTab()
    {
        $tab = self::getTabId();

        if ($tab && Validate::isLoadedObject($tab_obj = new Tab((int)$tab)))
        {
            if ($tab_obj->delete())
                return true;

            $this->_errors[] = $this->l('Unable to remove Product label tab');

            return false;
        }

        return false;
    }

    private static function tabExists()
    {
        return self::getTabId();
    }

    public function getMultilangField($field_array)
    {
        $languages = self::getLanguages();
        $prepared = array();

        foreach ($languages as $language) {
            if( isset($field_array[$language['iso_code']]))
                $prepared[$language['id_lang']] = $field_array[$language['iso_code']];
            else
                $prepared[$language['id_lang']] = $field_array['default'];
        }


        return $prepared;
    }

    public static function getLanguages()
    {
        if (!is_array(self::$module_languages))
            self::$module_languages = Language::getLanguages();

        return self::$module_languages;
    }


    public function hookDisplayHeader($params){
        $this->context->controller->addCSS(($this->_path).'views/css/ffproductlabels.css', 'all');
    }

    public function hookDisplayProductLabel($params){
        if(!isset($params['size']))
            $params['size'] = 'imgsmall';

        if (!$params['product_id'])
            return '';

        $current_label = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ffproductlabels_link` WHERE id_product='.(int)$params['product_id']);
        if ($current_label && $current_label['id_label']) {
            return '<img class="product_label '.$params['size'].' '.$this->label_positions[$current_label['label_position']].'" src="'._PS_IMG_.$this->img_dir_name.'/'.$current_label['id_label'].'-'.$params['size'].'.'.$this->imageType.'">';
        }

        if (!Cache::isStored('label_criteriaList')) {
            $criterias = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'ffproductlabels_criteria` WHERE `active`=1 AND `id_label`!=0 ORDER BY `position` ASC');

            Cache::store('label_criteriaList', $criterias);
        } else
            $criterias = Cache::retrieve('label_criteriaList');

        if ($criterias)
            foreach ($criterias as $criteria){
                if (method_exists($this, $criteria['criteria_method']) && $this->$criteria['criteria_method']($params) == true) {
                    return '<img class="product_label '.$params['size'].' '.$this->label_positions[$criteria['label_position']].'" src="'._PS_IMG_.$this->img_dir_name.'/'.$criteria['id_label'].'-'.$params['size'].'.'.$this->imageType.'">';
                }
            }

        return '';
    }

    protected function criteriaTopSells($params){
        $id_product = (int)$params['product_id'];
        $product_ids= array();

        if (!Cache::isStored('label_criteria_topSells')) {
            $result = self::getBestSalesLightPeriod((int)$params['cookie']->id_lang, 0, 60, null, 60);
            if ($result)
                foreach ($result as $id){
                    $product_ids[] = $id['id_product'];
                }

            Cache::store('label_criteria_topSells', $product_ids);
        } else
            $product_ids = Cache::retrieve('label_criteria_topSells');

        if (in_array($id_product,$product_ids))
            return true;

        return false;
    }

    protected function criteriaLastSize($params){
        $id_product = (int)$params['product_id'];
        $product_ids= array();
        if (!Cache::isStored('label_criteria_lastSize')) {
            $result = Db::getInstance()->executeS('SELECT `id_product` FROM (
                SELECT id_product, id_product_attribute, COUNT(`id_product_attribute`) AS cnt
                FROM
                    (SELECT * FROM `'._DB_PREFIX_.'stock_available` WHERE `quantity`!=0 AND `id_product_attribute`!=0) tmp
                GROUP BY `id_product`
                ) tmp2 WHERE `cnt`=1 AND `id_product_attribute` IN (
                    SELECT `id_product_attribute` FROM `'._DB_PREFIX_.'product_attribute_combination` WHERE `id_attribute` IN (
                        SELECT DISTINCT a.`id_attribute` FROM `ps_attribute_group_lang` agl
                        LEFT JOIN `'._DB_PREFIX_.'attribute` a ON a.`id_attribute_group`=agl.`id_attribute_group`
                        WHERE agl.`name`="Size" OR agl.`name`="Размер"
                    )
                )');
            if ($result)
                foreach ($result as $id){
                    $product_ids[] = $id['id_product'];
                }

            Cache::store('label_criteria_lastSize', $product_ids);
        } else
            $product_ids = Cache::retrieve('label_criteria_lastSize');

        if (in_array($id_product,$product_ids))
            return true;

        return false;
    }

    protected function criteriaIsNew($params){
        $id_product = (int)$params['product_id'];
            $product_ids= array();
            if (!Cache::isStored('label_criteria_isNew')) {
                $result = Db::getInstance()->executeS('
                    SELECT p.id_product
                    FROM `'._DB_PREFIX_.'product` p
                    '.Shop::addSqlAssociation('product', 'p').'
                    WHERE DATEDIFF(
                        product_shop.`date_add`,
                        DATE_SUB(
                            NOW(),
                            INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
                        )
                    ) > 0');
                if ($result)
                    foreach ($result as $id){
                        $product_ids[] = $id['id_product'];
                    }

                Cache::store('label_criteria_isNew', $product_ids);
            } else
                $product_ids = Cache::retrieve('label_criteria_isNew');

            if (in_array($id_product,$product_ids))
                return true;

        return false;
    }

    protected function criteriaDiscount($params){

//        Todo : make work without blocklayered module
        $id_product = (int)$params['product_id'];
        if (Module::isInstalled('blocklayered') && Module::isEnabled('blocklayered')) {
            $product_ids= array();

            if (!Cache::isStored('label_criteria_discount')) {
                $result = Db::getInstance()->executeS('SELECT lpi.`id_product` FROM `'._DB_PREFIX_.'layered_price_index` lpi
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON ( pa.`default_on`=1 AND pa.`id_product`=lpi.`id_product`)
                    WHERE ROUND(((1-lpi.`price_min`/lpi.`price_max`)*100))>1');
                if ($result)
                    foreach ($result as $id){
                        $product_ids[] = $id['id_product'];
                    }

                Cache::store('label_criteria_discount', $product_ids);
            } else
                $product_ids = Cache::retrieve('label_criteria_discount');

            if (in_array($id_product,$product_ids))
                return true;

        }

        return false;
    }

    protected function criteriaOnSale($params){
//        Todo : make work without blocklayered module
        $id_product = (int)$params['product_id'];
        $product_ids= array();

        if (!Cache::isStored('label_criteria_onSale')) {
            $result = Db::getInstance()->executeS('SELECT `id_product` FROM `'._DB_PREFIX_.'product` WHERE `on_sale`=1');
            if ($result)
                foreach ($result as $id){
                    $product_ids[] = $id['id_product'];
                }

            Cache::store('label_criteria_onSale', $product_ids);
        } else
            $product_ids = Cache::retrieve('label_criteria_onSale');

        if (in_array($id_product,$product_ids))
            return true;

        return false;
    }

    protected function criteriaSuperDiscount($params){
//        Todo : make work without blocklayered module
        $id_product = (int)$params['product_id'];
        if (Module::isInstalled('blocklayered') && Module::isEnabled('blocklayered')) {
            $product_ids= array();

            if (!Cache::isStored('label_criteria_superDiscount')) {
                $result = Db::getInstance()->executeS('SELECT lpi.`id_product` FROM `'._DB_PREFIX_.'layered_price_index` lpi
                    LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON ( pa.`default_on`=1 AND pa.`id_product`=lpi.`id_product`)
                    WHERE ROUND(((1-lpi.`price_min`/lpi.`price_max`)*100))>=50');
                if ($result)
                    foreach ($result as $id){
                        $product_ids[] = $id['id_product'];
                    }

                Cache::store('label_criteria_superDiscount', $product_ids);
            } else
                $product_ids = Cache::retrieve('label_criteria_superDiscount');

            if (in_array($id_product,$product_ids))
                return true;
        }

        return false;
    }

    protected function criteriaNewCollection($params){

        $id_product = (int)$params['product_id'];

        $criteria_year = Configuration::get($this->name.'_criteria_year');
        $criteria_season = Configuration::get($this->name.'_criteria_season');

        $product_ids= array();

        if (!Cache::isStored('label_criteria_NewCollection')) {
            $result = Db::getInstance()->executeS('SELECT t1.id_product FROM
                (SELECT * FROM `'._DB_PREFIX_.'feature_product` WHERE`id_feature_value`='.(int)$criteria_year.' GROUP BY `id_product`) t1
                INNER JOIN `'._DB_PREFIX_.'feature_product` t2 ON t2.`id_product`=t1.`id_product` AND t2.`id_feature_value`='.(int)$criteria_season);
            if ($result)
                foreach ($result as $id){
                    $product_ids[] = $id['id_product'];
                }

            Cache::store('label_criteria_NewCollection', $product_ids);
        } else
            $product_ids = Cache::retrieve('label_criteria_NewCollection');

        if (in_array($id_product,$product_ids))
            return true;

        return false;
    }

    public function hookDisplayAdminProductsExtra($params){
        $current_label = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ffproductlabels_link` WHERE id_product='.Tools::getValue('id_product'));
        $labels = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'ffproductlabels`');

        $this->context->smarty->assign(array(
            'token' => Tools::getAdminTokenLite('AdminProducts'),
            'labels' => $labels,
            'current_label' => $current_label,
        ));

        return $this->display(__FILE__, 'form.tpl');
    }

    public function hookActionProductUpdate($params) {
        $this->cleanCache();

        $id_product = (int)Tools::getValue('id_product');

        if ($data = Tools::getValue('label')){
            Db::getInstance()->execute('
              INSERT INTO `'._DB_PREFIX_.'ffproductlabels_link`
              VALUES('.$id_product.', '.(int)$data['id_label'].', '.(int)$data['label_position'].')
              ON DUPLICATE KEY UPDATE id_label='.(int)$data['id_label'].', label_position='.(int)$data['label_position']
            );
        }

    }

    public function getContent()
    {

        if (Tools::getValue('updateff')){
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffproductlabels_criteria`
                                        VALUES("", "",
                                        "'.$this->l('Новая коллекция').'",
                                        "'.$this->l('Ярлычек показывает у товаров, которые находятся в последней коллекции.').'",
                                        "criteriaNewCollection",
                                        6,
                                        1,
                                        0
                                        )');

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
//        $this->postProcess();
//
//        if (Tools::getIsset('updateffproductlabels')) {
//            $label = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ffproductlabels` WHERE id_label='.(int)Tools::getValue('id_label'));
//            return $this->renderForm($label);
//        }
//        if (Tools::getIsset('addffproductlabels')) {
//            return $this->renderForm();
//        }
//        if (Tools::getIsset('statusffproductlabels_criteria')) {
//            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffproductlabels_criteria` SET `active` = abs(`active` - 1) WHERE `id_criteria`='.(int)Tools::getValue('id_criteria'));
//            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
//        }

//        return $this->renderCriteriaList().$this->renderList();
    }

    public function renderForm(array $label = null){
        $fields_form = array('');
        // Get default Language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        if ($label){
            $image = $this->img_dir.$label['id_label'].'.'.$this->imageType;
            $image_url = ImageManager::thumbnail($image, $this->name.'_'.$label['id_label'].'.'.$this->imageType, 350,
                $this->imageType, true, true);
            $image_size = file_exists($image) ? filesize($image) / 1000 : false;
        }

        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $label ? 'Изменить иконку' : 'Добавить новую иконку для товаров',
                'enctype' => "multipart/form-data"
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'name' => 'label[title]',
                    'label' => 'Название',
                ),
                array(
                    'type' => 'file',
                    'id' => 'image1',
                    'label' => 'Картинка',
                    'name' => 'image',
                    'required' => true,
                    'hint' => $this->l('Storefront picture.'),
                    'files' => $label ?  array(
                        array(
                            'type' => 'image',
                            'image' => $image_url ? $image_url : false,
                            'size' => $image_size,
                            'delete_url' => AdminController::$currentIndex.'&configure='.$this->name.'&id_label='.$label['id_label'].'&updateffproductlabels&token='.Tools::getAdminTokenLite('AdminModules').'&deleteImg=image'
                        )
                    ) : array(),
                ),

            ),

            'submit' => array(
                'title' => 'Cохранить',
                'name' => $label ? 'editLabel' : 'addLabel'
            ),
        );



        $helper = new HelperForm();

        // // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar

        if ($label) {
            $fields_form[0]['form']['input'][] = array(
                'type' => 'hidden',
                'name' => 'label[id_label]',
            );

            $helper->fields_value = array(
                'label[title]' => $label['title'],
                'label[id_label]' => $label['id_label'],
            );
            $helper->currentIndex .= '&updateffproductlabels&id_label='.$label['id_label'];
        } else {
            $helper->fields_value = array(
                'label[title]' => '',
            );
        }
        $helper->show_cancel_button = true;
        $helper->back_url = AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules');


        return $helper->generateForm($fields_form);

    }




    public function postProcess(){
        $redirect = false;

        if ($redirect)
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
    }


    public static function getBestSalesLightPeriod($id_lang, $page_number = 0, $nb_products = 10, Context $context = null, $days_period = 0)
    {
        if (!$context)
            $context = Context::getContext();
        if ($page_number < 0) $page_number = 0;
        if ($nb_products < 1) $nb_products = 10;

        $sql = '
		SELECT
			p.id_product,  MAX(product_attribute_shop.id_product_attribute) id_product_attribute, pl.`link_rewrite`, pl.`name`, pl.`description_short`, product_shop.`id_category_default`,
			MAX(image_shop.`id_image`) id_image, il.`legend`,
			ps.`quantity` AS sales,  ps.`date_upd`, p.`ean13`, p.`upc`, cl.`link_rewrite` AS category, p.show_price, p.available_for_order, IFNULL(stock.quantity, 0) as quantity, p.customizable,
			IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity, stock.out_of_stock,
			product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'" as new,
			product_shop.`on_sale`, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity
		FROM `'._DB_PREFIX_.'product_sale` ps
		LEFT JOIN `'._DB_PREFIX_.'product` p ON ps.`id_product` = p.`id_product`
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
			ON (p.`id_product` = pa.`id_product`)
		'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
		'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
			ON p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
            Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
			ON cl.`id_category` = product_shop.`id_category_default`
			AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl');

        if (Group::isFeatureActive())
        {
            $groups = FrontController::getCurrentCustomerGroups();
            $sql .= '
				JOIN `'._DB_PREFIX_.'category_product` cp ON (cp.`id_product` = p.`id_product`)
				JOIN `'._DB_PREFIX_.'category_group` cg ON (cp.id_category = cg.id_category AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '= 1').')';
        }

        $sql.= '
		WHERE product_shop.`active` = 1 AND DATEDIFF(now(), ps.`date_upd`) < '.(int)$days_period .'
		AND p.`visibility` != \'none\'
		GROUP BY product_shop.id_product
		ORDER BY sales DESC
		LIMIT '.(int)($page_number * $nb_products).', '.(int)$nb_products;

        if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql))
            return false;

        return $result;
    }

    protected function cleanCache(){
        Cache::clean('label_criteriaList');
        Cache::clean('label_criteria_topSells');
        Cache::clean('label_criteria_lastSize');
        Cache::clean('label_criteria_isNew');
        Cache::clean('label_criteria_discount');
        Cache::clean('label_criteria_onSale');
        Cache::clean('label_criteria_superDiscount');
        Cache::clean('label_criteria_NewCollection');
    }

}