<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{emscarrier}prestashop>emscarrier_2ee8c6485d48ac509d2c919553f9e880'] = 'EMS почта России';
$_MODULE['<{emscarrier}prestashop>emscarrier_57ed7fafc0ab8157b20d302d57e50959'] = 'Расчет стоимости доставки EMS почтой России';
$_MODULE['<{emscarrier}prestashop>emscarrier_ba0b84f69e49cde5def71630ca52ef04'] = 'Города обновлены.';
$_MODULE['<{emscarrier}prestashop>emscarrier_8bb6f2cddac739d8db2872e59e379ef1'] = 'Ошибка EMS api.';
$_MODULE['<{emscarrier}prestashop>emscarrier_df6a7e8a98e6f5e41ab093cf9ae00534'] = 'Кэш очищен.';
$_MODULE['<{emscarrier}prestashop>emscarrier_55ec20ba0d6a1c0a7b035a0c84ebfd8a'] = 'Ошибка базы данных.';
$_MODULE['<{emscarrier}prestashop>emscarrier_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{emscarrier}prestashop>emscarrier_574c335dc4f9a0bccbd5e6ee61ea6b9a'] = 'Обновить города';
$_MODULE['<{emscarrier}prestashop>emscarrier_79c0d6cba080dc90b01c887064c9fc2f'] = 'Очистить кэш';
$_MODULE['<{emscarrier}prestashop>emscarrier_f4f70727dc34561dfde1a3c529b6205c'] = 'Настроки';
$_MODULE['<{emscarrier}prestashop>emscarrier_8fa8206978f9fda520607751aa6c3f0e'] = 'Город отправки';
$_MODULE['<{emscarrier}prestashop>emscarrier_c88cb8eda38db1924c4cded4c9b4141b'] = 'Выберите город из которого отправляются заказы';
$_MODULE['<{emscarrier}prestashop>emscarrier_27c7e97634a954be066bd89e4f3e7162'] = 'С объявленной стоимостью';
$_MODULE['<{emscarrier}prestashop>emscarrier_5671844cb9f8b1979599bda1ec626f6e'] = 'Отметьте, если в стоимости доставки нужно включить объявленную ценность';
$_MODULE['<{emscarrier}prestashop>emscarrier_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Включен';
$_MODULE['<{emscarrier}prestashop>emscarrier_b9f5c797ebbf55adccdd8539a65a0241'] = 'Отключен';
$_MODULE['<{emscarrier}prestashop>emscarrier_4bbb8f967da6d1a610596d7257179c2b'] = 'Неверный';
$_MODULE['<{emscarrier}prestashop>emscarrier_57d056ed0984166336b7879c2af3657f'] = 'Город';
$_MODULE['<{emscarrier}prestashop>emscarrier_f38f5974cdc23279ffe6d203641a8bdf'] = 'Настроки обновлены.';
$_MODULE['<{emscarrier}prestashop>form_a82be0f551b8708bc08eb33cd9ded0cf'] = 'Информация';
$_MODULE['<{emscarrier}prestashop>form_34b6cd75171affba6957e308dcbd92be'] = 'Версия';
$_MODULE['<{emscarrier}prestashop>form_794df3791a8c800841516007427a2aa3'] = 'Лицензия';
$_MODULE['<{emscarrier}prestashop>form_672caf27f5363dc833bda5099775e891'] = 'Разработчик';
$_MODULE['<{emscarrier}prestashop>form_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Описание';
$_MODULE['<{emscarrier}prestashop>form_764a18c1d364a1eb563785d57137a945'] = 'Модули и шаблоны для PrestaShop';
