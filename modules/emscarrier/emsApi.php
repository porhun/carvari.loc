<?php

/**
 * API UniSender
 *
 * @see http://www.unisender.com/ru/help/api/
 * @version 1.0
 */
class EmsApi {

	/**
	 * @var string
	 */
	protected $Encoding = 'UTF8';

	/**
	 * @var int
	 */
	protected $RetryCount = 0;

	/**
	 * @param string $ApiKey
	 * @param string $Encoding
	 * @param int $RetryCount
	 */
	function __construct($Encoding = 'UTF8', $RetryCount = 4) {
		if (!empty($Encoding)) {
			$this->Encoding = $Encoding;
		}

		if (!empty($RetryCount)) {
			$this->RetryCount = $RetryCount;
		}
	}

	/**
	 * @param string $Name
	 * @param array $Arguments
	 * @return string
	 */
	function __call($Name, $Arguments) {
		if (!is_array($Arguments) || empty($Arguments)) {
			$Params = array();
		} else {
			$Params = $Arguments[0];
		}

		return $this->callMethod($Name, $Params);
	}

	/**
	 * @param string $Value
	 * @param string $Key
	 */
	protected function iconv(&$Value, $Key) {
		$Value = iconv($this->Encoding, 'UTF8//IGNORE', $Value);
	}

	/**
	 * @param string $Value
	 * @param string $Key
	 */
	protected function mb_convert_encoding(&$Value, $Key) {
		$Value = mb_convert_encoding($Value, 'UTF8', $this->Encoding);
	}

	/**
	 * @param string $MethodName
	 * @param array $Params
	 * @return string
	 */
	public function callMethod($MethodName, $Params = array()) {
		if ($this->Encoding != 'UTF8') {
			if (function_exists('iconv')) {
				array_walk_recursive($Params, array($this, 'iconv'));
			}

			if (function_exists('mb_convert_encoding')) {
				array_walk_recursive($Params, array($this, 'mb_convert_encoding'));
			}
		}

		$ContextOptions = array(
			'http' => array(
				'method'  => 'GET',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				//'content' => http_build_query($Params)
			)
		);

		$RetryCount = 0;
		$Context = stream_context_create($ContextOptions);
		do {
			$Host = $this->getApiHost($RetryCount);
			$Result = file_get_contents($Host . '?method='. $MethodName .'&'.http_build_query($Params), FALSE, $Context);
			$RetryCount++;
		} while ($Result === false && $RetryCount < $this->RetryCount);

		return json_decode($Result);
	}

	/**
	 * @param int $RetryCount
	 * @return string
	 */
	protected function getApiHost($RetryCount = 0) {
		return 'http://emspost.ru/api/rest/';
	}

    public static function getCities()
    {
        $api=new EmsApi();
        $response=$api->callMethod('ems.get.locations',array('type'=>'cities', 'plain'=>'true'));
        if($response->rsp->stat!='ok')
            return false;
        foreach ($response->rsp->locations as $location)
        {
            $city['id_emscarrier_city']=$location->value;
            $city['name']=$location->name;
            $cities[]=$city;
        }
        return $cities;
    }

    public static function getRegions()
    {
        $api=new EmsApi();
        $response=$api->callMethod('ems.get.locations',array('type'=>'regions', 'plain'=>'true'));
        if($response->rsp->stat!='ok')
            return false;
        foreach ($response->rsp->locations as $location)
        {
            $city['id_emscarrier_region']=$location->value;
            $city['name']=$location->name;
            $cities[]=$city;
        }
        return $cities;
    }

    public static function getCost($from, $to, $weight)
    {
        $api=new EmsApi();
        $response=$api->callMethod('ems.calculate',array('type'=>'att', 'from'=>$from, 'to'=>$to, 'weight'=>$weight));
        if($response->rsp->stat!='ok')
            return false;
        return (float)$response->rsp->price;
    }
}