<?php

/**
 * emscarrier module main file.
 *
 * @author 0RS <admin@prestalab.ru>
 * @link http://prestalab.ru/
 * @copyright Copyright &copy; 2009-2012 PrestaLab.Ru
 * @license    http://www.opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 * @version 0.2
 */

if (!defined('_PS_VERSION_'))
    exit;

class emscarrier extends CarrierModule
{
    private $_html = '';
    private $_postErrors = array();

    function __construct()
    {
        $this->name = 'emscarrier';
        $this->tab = 'shipping_logistics';
        $this->version = '0.3';
        $this->author = 'PrestaLab.Ru';
        $this->need_instance = 0;
        //Ключик из addons.prestashop.com
        $this->module_key = '';

        parent::__construct();

        $this->displayName = $this->l('EMS Russian Post');
        $this->description = $this->l('EMS Russian Post shipping cost calculation');
        require_once('emsApi.php');
    }

    public function install()
    {
        $carrierConfig = array(
            'name' => $this->l('EMS Russian Post'),
            'id_tax_rules_group' => 0,
            'active' => true,
            'deleted' => 0,
            'shipping_handling' => false,
            'range_behavior' => 0,
            'delay' => array('default' => 'EMS express shipping', 'ru' => 'Экспресс доставка до двери'),
            'id_zone' => 7,
            'is_module' => true,
            'shipping_external' => true,
            'external_module_name' => $this->name,
            'need_range' => true
        );
        if ($id_carrier = self::installExternalCarrier($carrierConfig))
            Configuration::updateValue('emscarrier_id', (int)$id_carrier);

        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'emscarrier_city` (
            `id_emscarrier_city` varchar(255)  NOT NULL,
            `name` varchar(255) NOT NULL,
            INDEX (`id_emscarrier_city`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'emscarrier_region` (
            `id_emscarrier_region` varchar(255)  NOT NULL,
            `name` varchar(255) NOT NULL,
            INDEX (`id_emscarrier_region`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );
        Db::getInstance()->Execute('CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'emscarrier_cache` (
            `location` varchar(255) NOT NULL,
            `weight` varchar(5) default NULL,
            `price` varchar(5) default NULL,
            UNIQUE KEY `location` (`location`,`weight`)
            ) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8'
        );
        self::updateCities();
        return (parent::install()
            && $this->registerHook('updateCarrier')
        );
    }

    public function uninstall()
    {
        $carrier = new Carrier((int)(Configuration::get('emscarrier_id')));

        if (Configuration::get('PS_CARRIER_DEFAULT') == (int)$carrier->id) {
            $carriersD = Carrier::getCarriers($this->context->cookie->id_lang, true, false, false, NULL, PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
            foreach ($carriersD as $carrierD)
                if ($carrierD['active'] AND !$carrierD['deleted'] AND ($carrierD['name'] != $carrier->name))
                    Configuration::updateValue('PS_CARRIER_DEFAULT', $carrierD['id_carrier']);
        }

        $carrier->deleted = 1;
        if (!$carrier->update())
            return false;
        return (parent::uninstall()
            && Configuration::deleteByName('emscarrier_city')
            && Configuration::deleteByName('emscarrier_cost')
            && Configuration::deleteByName('emscarrier_id')
            && Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . 'emscarrier_city`')
            && Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . 'emscarrier_region`')
            && Db::getInstance()->Execute('DROP TABLE `' . _DB_PREFIX_ . 'emscarrier_cache`')
        );
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitemscarrier')) {
            $this->_postValidation();
            if (!sizeof($this->_postErrors))
                $this->_postProcess();
            else
                foreach ($this->_postErrors AS $err)
                    $this->_html .= $this->displayError($err);
        } elseif (Tools::isSubmit('updatecities')) {
            if(self::updateCities())
                $this->_html .= $this->displayConfirmation($this->l('Cities updated.'));
            else
                $this->_html .= $this->displayError($this->l('EMS api error.'));
        }
        elseif (Tools::isSubmit('clearcache'))
        {
            if(Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'emscarrier_cache`'))
                $this->_html .= $this->displayConfirmation($this->l('Cache has been cleaned.'));
            else
                $this->_html .= $this->displayError($this->l('DB error.'));
        }
        $this->_displayForm();
        return $this->_html;
    }

    protected function updateCities()
    {
        if(!($cities=emsApi::getCities()))
            return false;
        Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'emscarrier_city`');
        foreach($cities as $city)
            Db::getInstance()->insert('emscarrier_city', $city, false, true, Db::INSERT_IGNORE);

        if(!($regions=emsApi::getRegions()))
            return false;
        Db::getInstance()->execute('TRUNCATE TABLE `' . _DB_PREFIX_ . 'emscarrier_region`');
        foreach($regions as $region)
            Db::getInstance()->insert('emscarrier_region', $region, false, true, Db::INSERT_IGNORE);
        return true;
    }

    protected function getCities()
    {
        return Db::getInstance()->ExecuteS('SELECT * FROM `' . _DB_PREFIX_ . 'emscarrier_city`');
    }

    private function initToolbar()
    {
        $this->toolbar_btn['save'] = array(
            'href' => '#',
            'desc' => '<br>' . $this->l('Save')
        );
        $this->toolbar_btn['refresh-index'] = array(
            'href' => 'index.php?controller=AdminModules&updatecities&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($this->context->cookie->id_employee)),
            'desc' => $this->l('Update cities')
        );
        $this->toolbar_btn['refresh-cache'] = array(
            'href' => 'index.php?controller=AdminModules&clearcache&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($this->context->cookie->id_employee)),
            'desc' => $this->l('Clear cache')
        );
        return $this->toolbar_btn;
    }

    protected function _displayForm()
    {
        $this->_display = 'index';

        $cities = self::getCities();

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
                'image' => _PS_ADMIN_IMG_ . 'information.png'
            ),
            'input' => array(
                array(
                    'type' => 'select',
                    'label' => $this->l('Shop city'),
                    'desc' => $this->l('Select city, where located your shop'),
                    'name' => 'emscarrier_city',
                    'required' => true,
                    'options' => array(
                        'query' => $cities,
                        'id' => 'id_emscarrier_city',
                        'name' => 'name'
                    ),
                ),
                array(
                    'type' => 'radio',
                    'label' => $this->l('With declared cost'),
                    'desc' => $this->l('Check if package with declared cost'),
                    'name' => 'emscarrier_cost',
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'emscarrier_cost_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'emscarrier_cost_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    )
                ),

            ),

            'submit' => array(
                'name' => 'submitemscarrier',
                'title' => $this->l('Save'),
                'class' => 'button'
            )
        );
        $this->fields_value['emscarrier_city'] = Configuration::get('emscarrier_city');
        $this->fields_value['emscarrier_cost'] = Configuration::get('emscarrier_cost');

        $helper = $this->initForm();
        $helper->submit_action = '';

        $helper->title = $this->displayName;

        $helper->fields_value = $this->fields_value;
        $this->_html .= $helper->generateForm($this->fields_form);
        return;
    }

    private function initForm()
    {
        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = 'emscarrier';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name;
        $helper->toolbar_scroll = true;
        $helper->tpl_vars['version'] = $this->version;
        $helper->tpl_vars['author'] = $this->author;
        $helper->tpl_vars['this_path'] = $this->_path;
        $helper->toolbar_btn = $this->initToolbar();

        return $helper;
    }

    private function _postValidation()
    {
        if (Tools::getValue('emscarrier_city') && (!Validate::isString(Tools::getValue('emscarrier_city'))))
            $this->_postErrors[] = $this->l('Invalid') . ' ' . $this->l('City');
        if (Tools::getValue('emscarrier_cost') && (!Validate::isBool(Tools::getValue('emscarrier_cost'))))
            $this->_postErrors[] = $this->l('Invalid') . ' ' . $this->l('With declared cost');
    }

    private function _postProcess()
    {
        Configuration::updateValue('emscarrier_city', Tools::getValue('emscarrier_city'));
        Configuration::updateValue('emscarrier_cost', Tools::getValue('emscarrier_cost'));
        $this->_html .= $this->displayConfirmation($this->l('Settings updated.'));
    }

    public function hookupdateCarrier($params)
    {
        if ((int)($params['id_carrier']) == (int)(Configuration::get('emscarrier_id')))
            Configuration::updateValue('emscarrier_id', (int)($params['carrier']->id));

    }


    /** Добавление способа доставки
     * @param $config - настроки доставки
     * @return integer - идентификатор способа доставки
     */
    private static function installExternalCarrier($config)
    {
        $carrier = new Carrier();
        $carrier->name = $config['name'];
        $carrier->id_tax_rules_group = $config['id_tax_rules_group'];
        $carrier->id_zone = $config['id_zone'];
        $carrier->active = $config['active'];
        $carrier->deleted = $config['deleted'];
        $carrier->delay = $config['delay'];
        $carrier->shipping_handling = $config['shipping_handling'];
        $carrier->range_behavior = $config['range_behavior'];
        $carrier->is_module = $config['is_module'];
        $carrier->shipping_external = $config['shipping_external'];
        $carrier->external_module_name = $config['external_module_name'];
        $carrier->need_range = $config['need_range'];

        $languages = Language::getLanguages(true);
        foreach ($languages as $language) {
            if (!isset($config['delay'][$language['iso_code']]))
                $carrier->delay[$language['id_lang']] = $config['delay']['default'];
            else
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
        }

        if ($carrier->add()) {
            $groups = Group::getGroups(true);
            foreach ($groups as $group)
                Db::getInstance()->autoExecute(_DB_PREFIX_ . 'carrier_group', array('id_carrier' => (int)($carrier->id), 'id_group' => (int)($group['id_group'])), 'INSERT');

            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '1000000';
            $rangePrice->add();

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '1000000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $zone) {
                Db::getInstance()->autoExecute(_DB_PREFIX_ . 'carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])), 'INSERT');
                Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_ . 'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => (int)($rangePrice->id), 'id_range_weight' => NULL, 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), 'INSERT');
                Db::getInstance()->autoExecuteWithNullValues(_DB_PREFIX_ . 'delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => NULL, 'id_range_weight' => (int)($rangeWeight->id), 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), 'INSERT');
            }

            if (!copy(dirname(__FILE__) . '/carrier.jpg', _PS_SHIP_IMG_DIR_ . '/' . (int)$carrier->id . '.jpg'))
                return false;

            return (int)($carrier->id);
        }

        return false;
    }

    public function getOrderShippingCostExternal($params)
    {
        return $this->getOrderShippingCost($params, 0);
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        // Init var
        $weight=$params->getTotalWeight();
        if(($weight==0)&&($weight>31))
            return false;
        if (!($code=self::getCodeByAddressId($params->id_address_delivery)))
            return false;

        if(!($ru_currency_iso=Currency::getIdByIsoCode('RUB')))
            return false;
        $ru_currency=new Currency($ru_currency_iso);
        $cost=self::getCost($code, $weight);
        if($params->id_currency!=$ru_currency->id)
        {
            $currency=new Currency($params->id_currency);
            $cost=$cost*$currency->conversion_rate/$ru_currency->conversion_rate;
        }
        if (Configuration::get('emscarrier_cost'))
            $cost+=$params->getOrderTotal(true, Cart::BOTH_WITHOUT_SHIPPING)*0.01;

        return $cost;
    }

    private static function getCost($to, $weight)
    {
        $query = new DbQuery();
        $query->select('price');
        $query->from('emscarrier_cache');
        $query->where('location = \''.pSQL($to).'\'');
        $query->where('weight = \''.pSQL($weight).'\'');

        if ($emscost=(float)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query->build()))
            return $emscost;

        if(!$emscost = emsApi::getCost(Configuration::get('emscarrier_city'), $to, $weight))
            return false;

        $insert = array(
            'location' => pSQL($to),
            'weight' => (float)$weight,
            'price' => (float)$emscost
        );
        Db::getInstance()->insert('emscarrier_cache', $insert, false, true, Db::INSERT_IGNORE);
        return $emscost;
    }

    private static function getCodeByAddressId($id_address)
    {
        $query = new DbQuery();
        $query->select('c.`id_emscarrier_city`');
        $query->select('r.`id_emscarrier_region`');
        $query->select('co.`iso_code`');
        $query->from('address', 'a');
        $query->leftJoin('emscarrier_city', 'c', 'c.`name` = a.`city`');
        $query->leftJoin('state', 's', 's.`id_state` = a.`id_state`');
        $query->leftJoin('country', 'co', 'co.`id_country` = a.`id_country`');
        $query->leftJoin('emscarrier_region', 'r', 'r.`name` = s.`name`');
        $query->where('a.`id_address` = '.(int)$id_address);
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($query->build());

        if(!empty($result['id_emscarrier_city']))
            $return = $result['id_emscarrier_city'];
        elseif(!empty($result['id_emscarrier_region']))
            $return = $result['id_emscarrier_region'];
        elseif(!empty($result['iso_code']))
            $return = $result['iso_code'];
        else
            $return = false;

        return $return;
    }
}