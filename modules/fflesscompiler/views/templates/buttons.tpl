<div class="panel">

    <div class="panel-heading">
        Скомпилировать LESS
    </div>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <div class="form-group col-sm-3">
                <input type="hidden" name="compileLess" value="1">
                <input type="submit" class="btn btn-success" value="Компилировать">
            </div>
        </div>
    </form>

    <span>Для работы модуля нужно положить в папку {$tpl_dir}less/ , файл стилей main.less</span>


</div>

<div class="panel">

    <div class="panel-heading">
        Скомпилировать LESS Мобильной темы
    </div>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <div class="form-group col-sm-3">
                <input type="hidden" name="compileLessMobile" value="1">
                <input type="submit" class="btn btn-success" value="Компилировать для мобильной темы">
            </div>
        </div>
    </form>

    <span>Для работы модуля для мобильной версии, нужно положить в папку {$tpl_dir_mobile}less/ , файл стилей main.less</span>

</div>