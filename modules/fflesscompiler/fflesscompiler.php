<?php
if (!defined('_PS_VERSION_'))
    exit;

class Fflesscompiler extends Module
{
    public function __construct()
    {
        $this->name = 'fflesscompiler';
        $this->tab = 'others';
        $this->version = '1.0.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Less Compiler');
        $this->description = $this->l('Less Compiler - компилятор Less.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    }

    public function install()
    {
        if (!is_dir(_PS_THEME_DIR_.'less'))
            mkdir(_PS_THEME_DIR_.'less');
        if (!is_dir(_PS_THEME_MOBILE_DIR_.'less'))
            mkdir(_PS_THEME_MOBILE_DIR_.'less');

        if (!parent::install() ||
            !$this->registerHook('displayHeader'))
            return false;
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }

    public function hookDisplayHeader(){
        if ( _PS_MODE_DEV_ ) {
            $this->compileLess();
            $this->compileLessMobile();
        }
        return;
    }

    public function getContent()
    {
        if ( Tools::getValue('compileLess') ) {
           $this->compileLess();
        }

        if ( Tools::getValue('compileLessMobile') ) {
            $this->compileLessMobile();
        }

        return $this->showButtons();
    }

    public function showButtons()
    {
        $this->context->smarty->assign(array(
            'tpl_dir'             => _PS_THEME_DIR_,
            'tpl_dir_mobile'             => _PS_THEME_MOBILE_DIR_,
        ));
        return $this->context->smarty->fetch(__DIR__.'/views/templates/buttons.tpl');
    }

    public function compileLess()
    {
        require_once __DIR__.'/less/Less.php';

        $options = array( 'compress'=>true );
        $parser = new Less_Parser( $options );
        $parser->parseFile( _PS_THEME_DIR_.'less/main.less', _PS_BASE_URL_._THEME_DIR_.'less/' );
        $css = $parser->getCss();

        file_put_contents( _PS_THEME_DIR_.'css/autoload/main.css', $css);
    }

    public function compileLessMobile()
    {
        require_once __DIR__.'/less/Less.php';

        $options = array( 'compress'=>true );
        $parser = new Less_Parser( $options );
        $parser->parseFile( _PS_THEME_MOBILE_DIR_.'less/main.less', _PS_BASE_URL_._THEME_MOBILE_DIR_.'less/' );
        $css = $parser->getCss();

        file_put_contents( _PS_THEME_MOBILE_DIR_.'css/autoload/main.css', $css);
    }

}