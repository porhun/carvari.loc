<?php
if (!defined('_PS_VERSION_'))
    exit;

class Ffdisqus extends Module
{
    public function __construct()
    {
        $this->name = 'ffdisqus';
        $this->tab = 'social_networks';
        $this->version = '1.0.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Disqus Comments - комментарии для продуктов');
        $this->description = $this->l('Disqus Comments - комментарии для продуктов.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !$this->registerHook('displayDisqus')
            )
            return false;
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }


    public function hookDisplayDisqus()
    {
        return $this->context->smarty->fetch(__DIR__.'/views/templates/disqus.tpl');
    }
}