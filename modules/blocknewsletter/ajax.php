<?php
require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');

require_once(dirname(__FILE__).'/blocknewsletter.php');
require_once(dirname(__FILE__).'../../../override/modules/blocknewsletter/blocknewsletter.php');

//d('here');
//$new_email =  Tools::getValue('email');
$newsletter = new BlocknewsletterOverride();

if (Tools::getValue('action') == 'popup_close'){
    $context = Context::getContext();
    $context->cookie->popup_close = true;
    die(json_encode( array('status' => 'ok'), JSON_UNESCAPED_UNICODE));
}

$newsletter->publicRegistration();
if ($newsletter->valid)
    die(json_encode( array('status' => 'ok', 'msg'=>$newsletter->valid), JSON_UNESCAPED_UNICODE));
else
    die(json_encode( array('status' => 'error', 'msg'=>$newsletter->error), JSON_UNESCAPED_UNICODE));