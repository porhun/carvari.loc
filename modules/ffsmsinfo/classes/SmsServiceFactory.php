<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 07.02.17
 * Time: 15:18
 */
require_once __DIR__.'/SmsServiceFactoryInterface.php';

class SmsServiceFactory implements SmsServiceFactoryInterface
{
    public function create()
    {
        try {
            $provider_name = Configuration::get('smsinfo_provider');
            $login = Configuration::get('smsinfo_login');
            $password = Configuration::get('smsinfo_password');
            $sender = Configuration::get('smsinfo_sender');

            require_once(_PS_MODULE_DIR_.'ffsmsinfo/providers/'.strtolower($provider_name).'.php');
            return new $provider_name( $login, $password, $sender );

        } catch (Exception $e) {
            Tools::error_log(['message' => $e->getMessage()], 3, _PS_MODULE_DIR_.'/ffsmsinfo/log.txt');
            return null;
        }
    }
}