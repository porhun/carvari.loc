<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 07.02.17
 * Time: 15:08
 */
class SmsService
{
    /** @var bool|null|SmsServiceInterface  */
    private $_provider = null;

    public function __construct() {
        /** @var Ffsmsinfo $smsInfoModule */
        $smsInfoModule = Module::getInstanceByName('ffsmsinfo');
        $this->_provider = $smsInfoModule->getSmsServiceFactory()->create();
    }

    public function sendSMS( $message, $phone, $return_error = false ) {
        if (is_null($this->_provider)) {
            throw new Exception('No sms providers given');
        }
        return $this->_provider->sendSMS( $message, $phone);
    }
}