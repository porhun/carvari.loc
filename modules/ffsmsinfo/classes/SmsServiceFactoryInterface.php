<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 07.02.17
 * Time: 15:16
 */
interface SmsServiceFactoryInterface
{
    /**
     * Create SMS service
     *
     * @return SmsServiceInterface
     */
    public function create();
}