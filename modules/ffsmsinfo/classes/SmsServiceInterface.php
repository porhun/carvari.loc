<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 07.02.17
 * Time: 15:17
 */
interface SmsServiceInterface
{
    /**
     * Send SMS
     *
     * @param string $message   Text message
     * @param string $phone     Phone number where message will be send to
     * @return mixed
     */
    public function sendSMS($message, $phone);
}