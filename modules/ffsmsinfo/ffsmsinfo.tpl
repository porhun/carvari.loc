<form action="" method="post" class="form-horizontal">

    <div class="panel panel-default">
        <div class="panel-heading">Протестировать СМС Сервис</div>
        <div class="panel-body">
            <div class="form-wrapper">


                <div class="form-group">

                    <label class="control-label col-lg-3">
                        Телефон
                    </label>

                    <div class="col-lg-3">
                        <input class="form-control " type="text" size="10" name="test_phone" value="{if isset($test_phone)}{$test_phone}{/if}">
                        {if $result}
                            Результат: {if $result==true}Успешная отправка{else}{$result}{/if}
                        {/if}
                    </div>

                </div>
                <div class="form-group">

                    <label class="control-label col-lg-3">

                    </label>

                    <div class="col-lg-3">
                        <input class="btn btn-success" type="submit" value="Отправить">
                    </div>

                </div>
            </div>
        </div>
    </div>
</form>