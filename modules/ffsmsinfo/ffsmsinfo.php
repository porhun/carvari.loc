<?php

if (!defined('_PS_VERSION_'))
    exit;

class Ffsmsinfo extends Module
{

    public function __construct()
    {
        $this->name = 'ffsmsinfo';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('SMS информирование');
        $this->description = $this->l('SMS информирование при оформлении заказа');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');

//        $this->sms_gate_url = 'http://turbosms.in.ua/api/wsdl.html';
    }


    public function install()
    {
        copy(__DIR__.'/classes/SmsService.php', _PS_ROOT_DIR_.'/override/classes/SmsService.php');
        unlink(_PS_ROOT_DIR_.'/cache/class_index.php');

        Configuration::updateValue('smsinfo_provider', 'TurboSms');
        Configuration::updateValue('smsinfo_order_confirmation', 1);
        Configuration::updateValue('smsinfo_order_status_posted', 0);
        Configuration::updateValue('smsinfo_order_ttn_send', 0);
        Configuration::updateValue('smsinfo_provider', 'turbosms');
        Configuration::updateValue('smsinfo_login', 'test');
        Configuration::updateValue('smsinfo_password', 'test');
        Configuration::updateValue('smsinfo_sender', 'test');
        Configuration::updateValue('smsinfo_order_tpl', 'Ваш заказ № {id}. Мы свяжемся с Вами в ближайшее время. 0800210087');
        Configuration::updateValue('smsinfo_order_status_tpl', 'Ваш заказ № {id} был отправлен! Ждите уведомления SMS.');
        Configuration::updateValue('smsinfo_order_ttn_tpl', 'Ваш заказ № {id} был отправлен, ТТН: {ttn}.');

        if (!parent::install()
            || !$this->registerHook('actionValidateOrder')
            || !$this->registerHook('actionOrderStatusUpdate')
            || !$this->registerHook('actionNpTtnCreate')
        )
            return false;
        return true;
    }

    public function uninstall()
    {
        unlink(_PS_ROOT_DIR_.'/override/classes/SmsService.php');
        unlink(_PS_ROOT_DIR_.'/cache/class_index.php');

        Configuration::deleteByName('smsinfo_order_confirmation');
        Configuration::deleteByName('smsinfo_order_status_posted');
        Configuration::deleteByName('smsinfo_order_ttn_send');
        Configuration::deleteByName('smsinfo_login');
        Configuration::deleteByName('smsinfo_password');
        Configuration::deleteByName('smsinfo_sender');
        Configuration::deleteByName('smsinfo_order_tpl');
        Configuration::deleteByName('smsinfo_order_status_tpl');
        Configuration::deleteByName('smsinfo_order_ttn_tpl');

        if (!parent::uninstall())
            return false;

        return true;
    }

    public function getContent()
    {

        if(Tools::getValue('test'))
            $this->test();

        if (Tools::isSubmit('submitOptions'))
        {
            Configuration::updateValue('smsinfo_provider' , Tools::getValue('smsinfo_provider'));
            Configuration::updateValue('smsinfo_login' , Tools::getValue('smsinfo_login'));
            if ( Tools::getValue('smsinfo_password') != '' )
                Configuration::updateValue('smsinfo_password' , Tools::getValue('smsinfo_password'));
            Configuration::updateValue('smsinfo_sender' , Tools::getValue('smsinfo_sender'));
            Configuration::updateValue('smsinfo_order_confirmation' , Tools::getValue('smsinfo_order_confirmation'));
            Configuration::updateValue('smsinfo_order_status_posted' , Tools::getValue('smsinfo_order_status_posted'));
            Configuration::updateValue('smsinfo_order_tpl' , Tools::getValue('smsinfo_order_tpl'));
            Configuration::updateValue('smsinfo_order_status_tpl' , Tools::getValue('smsinfo_order_status_tpl'));
            Configuration::updateValue('smsinfo_order_ttn_send' , Tools::getValue('smsinfo_order_ttn_send'));
            Configuration::updateValue('smsinfo_order_ttn_tpl' , Tools::getValue('smsinfo_order_ttn_tpl'));
        }

        return $this->displayConfigForm().$this->displayTestForm();
    }

    public function displayTestForm()
    {
        $result = '';
        if ($test_phone = Tools::getValue('test_phone')) {
            $test_phone = preg_replace('/[. ()-+]*/', '', $test_phone);
            $test_phone = '+' . $test_phone;
            if ( Validate::isPhoneNumber($test_phone) ) {
                $sms = new SmsService();
                $result = $sms->sendSMS('Тестовое СМС!', $test_phone, true);
            } else {
                $result = 'Неверный формат телефона';
            }
            $this->context->smarty->assign(array(
                'result' => $result,
                'test_phone' => $test_phone,
            ));
        }

        return $this->display(__FILE__, 'ffsmsinfo.tpl');
    }

    public function displayConfigForm()
    {
        $this->fields_options = array(
            'general' => array(
                'title' => $this->l('Настройка модуля'),
                'fields' => array(
                    'smsinfo_provider' => array(
                        'title' => $this->l('SMS Provider'),
                        'type' => 'select',
                        'identifier' => 'value',
                        'list' => array(
                            array(
                                'value' => 'TurboSms',
                                'name' => $this->l('TurboSms')
                            ),
                            array(
                                'value' => 'ESputnik',
                                'name' => $this->l('ESputnik')
                            ),
                            array(
                                'value' => 'SMSC',
                                'name'  => $this->l('SMSC')
                            ),
                        )

                    ),
                    'smsinfo_login' => array(
                        'title' => $this->l('SMS Provider Login'),
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '10'
                    ),
                    'smsinfo_password' => array(
                        'title' => $this->l('SMS Provider Password'),
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '10'
                    ),
                    'smsinfo_sender' => array(
                        'title' => $this->l('Sender'),
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '10'
                    ),
                    'smsinfo_order_confirmation' => array(
                        'title' => $this->l('Включить SMS информирование при новом заказе'),
                        'cast' => 'intval',
                        'type' => 'bool',
                        'size' => '10'
                    ),
                    'smsinfo_order_tpl' => array(
                        'title' => $this->l('Шаблон SMS при новом заказе'),
                        'desc' => $this->l('Шаблон смс уведомления при заказе. {id} заменяется на номер заказа'),
                        'cast' => 'strval',
                        'type' => 'textarea'
                    ),
                    'smsinfo_order_status_posted' => array(
                        'title' => $this->l('Включить SMS информирование при отправке заказа'),
                        'cast' => 'intval',
                        'type' => 'bool',
                        'size' => '10'
                    ),
                    'smsinfo_order_status_tpl' => array(
                        'title' => $this->l('Шаблон SMS при отправке заказа'),
                        'desc' => $this->l('Шаблон смс уведомления при смене статуса заказа на "Отправлено". {id} заменяется на номер заказа'),
                        'cast' => 'strval',
                        'type' => 'textarea'
                    ),
                    'smsinfo_order_ttn_send' => array(
                        'title' => 'Включить SMS информирование при создании ТТН Новой Почты',
                        'cast' => 'intval',
                        'type' => 'bool',
                        'size' => '10'
                    ),
                    'smsinfo_order_ttn_tpl' => array(
                        'title' => 'Шаблон SMS при создании ТТН Новой Почты',
                        'desc' => 'Шаблон смс уведомления при создании ТТН номера Новой Почты. {id} - заменяется на номер заказа, {ttn} - номер ТТН',
                        'cast' => 'strval',
                        'type' => 'textarea'
                    ),


                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'button',
                    'name' => 'submitOptions'
                )
            )
        );

        $helper = new HelperOptions();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        return $helper->generateOptions($this->fields_options);
    }

    public function hookActionValidateOrder($params)
    {
        if( Configuration::get('smsinfo_order_confirmation') )
        {
            $customer = new Customer($params['customer']->id);
//            $addresses = $customer->getAddresses($this->context->language->id);
//            if (count($addresses)){
//                $address = $addresses[0];
                $this->sendOrderConfirmSms( $params['order']->id, $customer->phone_login, Configuration::get('smsinfo_order_tpl') );
//            }
        }
    }

    public function hookActionNpTtnCreate($params)
    {
        // Пример вызова хука из модуля НП
        // Hook::exec('actionNpTtnCreate', ['ttn' => 1212212121, 'id_order' => 1221, 'phone_login' => '+380111111111']);

        if( Configuration::get('smsinfo_order_ttn_send') && isset($params['phone_login']) ) {
            $this->sendOrderTtnSms( $params['ttn'], $params['id_order'], $params['phone_login'], Configuration::get('smsinfo_order_ttn_tpl') );
        }
    }

    public function hookActionOrderStatusUpdate($params)
    {
        if( Configuration::get('smsinfo_order_status_posted') )
            if ( mb_strtolower($params['newOrderStatus']->template) == 'shipped') {
                $customer = new Customer($params['cart']->id_customer);
//                $addresses = $customer->getAddresses($this->context->language->id);
//                if (count($addresses)){
//                    $address = $addresses[0];
                    $this->sendOrderConfirmSms( $params['id_order'], $customer->phone_login, Configuration::get('smsinfo_order_status_tpl') );
//                }
            }
        return true;
    }

    public static function sendOrderConfirmSms($id = 0, $phone = null, $tpl)
    {
        if (!$phone)
            return false;

        $phone = preg_replace('/[. ()-+]*/', '', $phone);
        $phone = '+'.$phone;

        $sms_service = new SmsService();
        $sms_text = str_replace('{id}', $id, $tpl);
        return $sms_service->sendSMS($sms_text, $phone);
    }

    public static function sendOrderTtnSms($ttn = null, $id = null, $phone = null, $tpl = null)
    {
        if (!$ttn || !$id || !$phone || !$tpl)
            return false;

        $phone = preg_replace('/[. ()-+]*/', '', $phone);
        $phone = '+'.$phone;

        $sms_service = new SmsService();
        $sms_text = str_replace('{id}', $id, $tpl);
        $sms_text = str_replace('{ttn}', $ttn, $sms_text);
        return $sms_service->sendSMS($sms_text, $phone);
    }

    public function getPhoneLoginById($id_customer)
    {
        if (property_exists('Customer', 'phone_login')) {
            $customer = new Customer($id_customer);
            if ($customer && $customer->phone_login)
                return $customer->phone_login;
        }
        return false;
    }

    /**
     * Return SMS service factory
     *
     * @return SmsServiceFactory
     */
    public function getSmsServiceFactory()
    {
        require_once __DIR__.'/classes/SmsServiceFactory.php';
        return new SmsServiceFactory();
    }

}