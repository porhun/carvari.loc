<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 07.02.17
 * Time: 16:33
 */
require_once _PS_MODULE_DIR_.'/ffsmsinfo/classes/SmsServiceInterface.php';

class SMSC implements SmsServiceInterface
{
    protected $logPath;
    protected $login;
    protected $password;
    protected $sender;

    protected $apiSendUrl = 'https://api.life.com.ua/ip2sms/';

    public function __construct($login, $password, $sender)
    {
        $this->login    = $login;
        $this->password = $password;
        $this->sender   = $sender;
        $this->logPath  = _PS_MODULE_DIR_.'/ffsmsinfo/log.txt';
    }

    public function sendSMS($message, $phone)
    {
        $phone = '+'.preg_replace('/\D+/', '', $phone);
        $xmlData = "<message>
                    <service id='single' type='OTTSMS' ott='VIBER' source='{$this->sender}'/>
                    <to>{$phone}</to>
                    <body content-type='text/plain'>{$message}</body>
                </message>";

        if ($xmlResponse = $this->apiRequest($xmlData)) {
            return $this->handleApiResponse($xmlResponse);
        }
        return false;
    }

    protected function handleApiResponse(SimpleXMLElement $xmlResponse)
    {
        $this->log('Handle API response start', (string)$xmlResponse);
        if (isset($xmlResponse->state['error']) && strlen($xmlResponse->state['error'])) {
            $this->log('Handle API response ERROR', (string)$xmlResponse);
            return false;
        }
        return true;
    }

    protected function apiRequest($xmlData) {
        $credentials = sprintf('Authorization: Basic %s', base64_encode($this->login.":".$this->password) );
        $params = ['http' => ['method' => 'POST', 'content' => $xmlData, 'header' => [$credentials, 'Content-Type: text/xml']]];
        $ctx = stream_context_create($params);
        $fp  = @fopen($this->apiSendUrl, 'rb', false, $ctx);
        if ($fp) {
            $response = @stream_get_contents($fp);
            return new SimpleXMLElement($response);
        }
        $this->log('Response not open', compact('params'));
        return false;
    }

    protected function log($status, $data)
    {
        Tools::error_log(['status' => $status, 'data' => $data], 3, $this->logPath);
    }
}