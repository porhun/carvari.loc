<?php

require_once _PS_MODULE_DIR_.'/ffsmsinfo/classes/SmsServiceInterface.php';

class TurboSms implements SmsServiceInterface {

    private $_login;

    private $_password;

    private $_sender;

    private $_server_url = 'http://turbosms.in.ua/api/wsdl.html';

    private $_client = null;

    public function __construct($login, $password, $sender) {

        $this->_login    = $login;
        $this->_password = $password;
        $this->_sender   = $sender;

        $this->_client = new SoapClient ($this->_server_url);
        $this->_connect();
    }

    private function _connect() {
        header ('Content-type: text/html; charset=utf-8');

        $this->_client->Auth(
            Array (
                'login' => $this->_login,
                'password' => $this->_password
            )
        );
    }

    public function sendSMS( $message, $phone , $return_error = false) {
        $charset = mb_detect_encoding( $message, array('UTF-8', 'Windows-1251') );
        if( $charset=='Windows-1251' ) {
            $message = iconv('windows-1251', 'UTF-8', $message);
        }
        $sms = Array (
            'sender' => $this->_sender,
            'destination' => $phone,
            'text' => $message
        );

        $result = $this->_client->SendSMS( $sms );

        if (trim($result->SendSMSResult->ResultArray[0]) == 'Сообщения успешно отправлены')
            return true;
        else
            return false;
    }
}
