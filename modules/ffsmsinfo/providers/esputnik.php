<?php
require_once _PS_MODULE_DIR_.'/ffsmsinfo/classes/SmsServiceInterface.php';

class ESputnik implements SmsServiceInterface {

    private $_login;

    private $_password;

    private $_sender;

    private $_client = null;

    public function __construct($login, $password, $sender) {

        $this->api_group_name = 'fromsite'; // Название группы для контактов. Нужно создать заранее в админке eSputnik !!!
        $this->api_dedupe_on = 'email'; // По какому полю смотрится уникальность контакта
        $this->api_mail_from = ''; // Обратный адресс для писем
        // eSpitnik API URLs
        $this->api_get_version_url = 'https://esputnik.com.ua/api/v1/version';
        $this->api_get_balance_url = 'https://esputnik.com.ua/api/v1/balance';
        $this->api_users_add_url = 'https://esputnik.com.ua/api/v1/contacts';
        $this->api_users_email_url = 'https://esputnik.com.ua/api/v1/contacts/email';
        $this->api_email_send_url = 'https://esputnik.com.ua/api/v1/message';
        $this->api_email_url = 'https://esputnik.com.ua/api/v1/message/email';
        $this->api_emails_url = 'https://esputnik.com.ua/api/v1/messages/email';
        $this->api_group_url = 'https://esputnik.com.ua/api/v1/group';
        $this->api_groups_url = 'https://esputnik.com.ua/api/v1/groups';
        $this->api_send_sms_url = 'https://esputnik.com.ua/api/v1/message/sms';


        $this->_login    = $login;
        $this->_password = $password;
        $this->_sender   = $sender;

    }

    public function sendSMS( $message, $phone, $return_error = false ) {

        $json_value = new stdClass();
        $json_value->text = $message;
        $json_value->from = $this->_sender;
        $json_value->phoneNumbers = array($phone);

        $return = $this->apiSendRequest($this->api_send_sms_url, $json_value, null);
//        $return = '{"id":"0","results":{"id":"0","locator":"380676122926","status":"OK","requestId":"84f7d2b8-f933-458d-8382-e426664099d3"}}';
        $return = json_decode($return);

        if ($return->results->status == 'OK')
            return true;
        else {
            if ($return_error){
                return $return->results->status;
            }
            else
                return false;
        }
    }

    public function apiSendRequest($url, $json_value = null, $custom_request = null) {
        $ch = curl_init();
        if ($json_value) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($json_value));
        }
        if ($custom_request)
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom_request);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_USERPWD, $this->_login.':'.$this->_password);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        if ($output = curl_exec($ch))
        {
//            d($output);

            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($output, 0, $header_size);
            $body = substr($output, $header_size);

            curl_close($ch);

//            $return = json_decode($body,true);
            if (preg_match('/HTTP\/1.1 200 OK\b/', $header)) {
                return $body;
            } else
            {
                $this->warning = $body;
                return false;
            }
        } else {
            return $this->warning = 'Превышено время ожидания ответа от eSputnik.';
        }

    }
}
