<?php

$sql = [];

$sql[] = 'CREATE TABLE IF NOT EXISTS '.bqSQL(_DB_PREFIX_).'ffcategory_seo(
            `id_category_seo` int(11) unsigned NOT NULL auto_increment,
            `active` int(10) unsigned NOT NULL,
            PRIMARY KEY(`id_category_seo`)
        ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'ffcategory_seo_lang` (
            `id_category_seo` int(11) unsigned NOT NULL,
            `id_lang` int(11) unsigned NOT NULL,
			`title` varchar(128) NOT NULL,
            `description` longtext,
            PRIMARY KEY (`id_category_seo`,`id_lang`)
        ) ENGINE = ' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8';

return $sql;