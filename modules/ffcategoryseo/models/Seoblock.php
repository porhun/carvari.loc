<?php
class Seoblock extends ObjectModel
{
    /** @var string Name */
    public $description;
    public $title;
    public $active;

    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'ffcategory_seo',
        'primary' => 'id_category_seo',
		'multilang' => true,
        'fields' => array(
            'active' =>           array('type' => self::TYPE_INT,'lang' => false),

            // Lang fields
            'title' =>          array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128),
			'description' =>            array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999),
        ),
    );

    public static function getAll() {
        $id_lang = (int)Context::getContext()->language->id;
        $res =  Db::getInstance()->executeS('
                    SELECT * FROM '._DB_PREFIX_.'ffcategory_seo AS cs 
					LEFT JOIN '._DB_PREFIX_.'ffcategory_seo_lang AS csl ON cs.id_category_seo = csl.id_category_seo
                    WHERE id_lang ='.$id_lang.'
						AND active = 1					
		');
        $result = [];
        foreach ($res as $item){
            $category = new Category((int)Tools::getValue('id_category'), $id_lang);
            $result[] = [
                'category_name' => $category->h1tag,
                'active' => $item['active'],
                'description' => $item['description'],
                'title' => $item['title']
            ];
        }
        return $result ?: [];
    }

}