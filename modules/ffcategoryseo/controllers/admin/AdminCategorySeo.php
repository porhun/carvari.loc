<?php


class AdminCategorySeoController extends ModuleAdminController
{
    public function __construct() {
        $this->bootstrap = true;
        $this->table = 'ffcategory_seo';
        $this->className = 'Seoblock';
        $this->identifier = 'id_category_seo';
        $this->lang = true;
        $this->deleted = false;
        $this->colorOnBackground = false;
        $this->bulk_actions = array('delete' => array('text' => $this->l('Delete selected'), 'confirm' => $this->l('Delete selected items?')));
//        Shop::addTableAssociation($this->table, array('type' => 'shop'));
        $this->context = Context::getContext();

//        $this->fieldImageSettings = array(
//            'name' => 'image',
//            'dir' => 'cms'
//        );
//        $this->imageType = "jpg";

        parent::__construct();
    }

    public function renderList() {

        $this->addRowAction('edit');
        $this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected items?')
            )
        );

        $this->fields_list = array(
            'id_category_seo' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'width' => 25,
                'lang' => false
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'width' => 90,
                'lang' => false
            ),
            'description' => array(
                'title' => $this->l('Description'),
                'width' => '300',
                'lang' => false
            )
        );

//        $this->fields_list['image'] = array(
//            'title' => $this->l('Image'),
//            'width' => 70,
//            "image" => $this->fieldImageSettings["dir"]
//        );
//
//        $listSlideshows = Seoblock::getSlideshowLists($this->context->language->id);
//        echo "<pre>"; print_r($listSlideshows); die;
        $lists = parent::renderList();
        parent::initToolbar();

        return $lists;
    }



    public function renderForm()
    {
        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('Settings'),
                'image' => '../img/admin/cog.gif'
            ),

            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Title:'),
                    'name' => 'title',
                    'size' => 40,
                    'lang' => true
                ),
                array(
                    'type'      => 'radio',
                    'label'     => $this->l('Show/hide'),
                    'name'      => 'active',
                    'required'  => true,
                    'class'     => 't',
                    'is_bool'   => true,
                    'values'    => array(
                        array(
                            'id'    => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id'    => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Description'),
                    'name' => 'description',
                    'autoload_rte' => true,
                    'lang' => true,
                    'rows' => 5,
                    'cols' => 60,
                    'desc' => $this->l('Use variable: "{category_name}"')
                ),

            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );

        if (!($obj = $this->loadObject(true)))
            return;


        return parent::renderForm();
    }

}
