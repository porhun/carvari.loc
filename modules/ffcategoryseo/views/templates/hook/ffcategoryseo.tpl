{if $active}
<div class="container">
    <div class="content_scene_cat">
        <div class="content_scene is_post">
            <div class="cat_desc rte">
                {if $title}
                <h2>{$title}</h2>
                {/if}
                <p>{$description}</p>
            </div>
        </div>
    </div>
</div>
{/if}

