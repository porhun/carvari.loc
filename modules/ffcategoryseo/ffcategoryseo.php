<?php

if (!defined('_PS_VERSION_'))
    exit;

require_once __DIR__.'/models/Seoblock.php';

class FfCategorySeo extends Module
{
    private static $module_tab = array(
        'class_name' => 'AdminCategorySeo',
        'name' => array(
            'default' => 'Category SEO Text',
            'en' => 'Category SEO Text',
            'ru' => 'SEO для категорий',
            'uk' => 'SEO для категорiй',
        ),
        'module' => false,
        'id_parent' => false
    );

    private static $module_languages;

    const INSTALL_SQL_FILE   = 'sql/install.php';
    const UNINSTALL_SQL_FILE = 'sql/uninstall.php';

    public function __construct()
    {
        $this->name = 'ffcategoryseo';
        $this->tab = 'smart_shopping';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->secure_key = Tools::encrypt($this->name);
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Category SEO Text');
        $this->description = $this->l('Add the template text into category pages ');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');
        self::$module_languages = Language::getLanguages();
        $this->errors = array();
    }

    public function install($delete_params = true)
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !$this->registerHook('displaySeoText') ||
            !$this->registerHook('displayAdminProductsExtra') ||
            !$this->createTab()
        )
            return false;
        if ($delete_params && !$this->createTables()) {
            return false;
        }
        $this->cleanClassCache();
        return true;
    }

    public function uninstall($delete_params = true)
    {
        if (!parent::uninstall() || !$this->deleteTab())
            return false;
        if ($delete_params && !$this->deleteTables()) {
            return false;
        }
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache))
            unlink($class_cache);
    }

    public function reset()
    {
        if (!$this->uninstall(false)) {
            return false;
        }
        if (!$this->install(false)) {
            return false;
        }
        return true;
    }

    protected function createTables()
    {
        $sql = $this->getTableSql(self::INSTALL_SQL_FILE);
        return $this->performSql($sql);
    }

    protected function deleteTables()
    {
        $sql = $this->getTableSql(self::UNINSTALL_SQL_FILE);
        return $this->performSql($sql);
    }

    protected function getTableSql($sqlFile)
    {
        $sql = require (dirname(__FILE__).'/'.$sqlFile);
        if (!$sql) {
            return [];
        }
        return $sql;
    }

    protected function performSql(array $sqlQueries)
    {
        $dbInstance = Db::getInstance();
        foreach ($sqlQueries as $query) {
            if (!$dbInstance->execute($query)) {
                return false;
            }
        }
        return true;
    }

    public function createTab()
    {
        if ($this->tabExists())
            return true;

        $parent_id = Tab::getIdFromClassName('AdminCatalog');

        if (!$parent_id)
        {
            $this->_errors[] = $this->l('Invalid parent tab selected');

            return false;
        }

        $tab = new Tab();

        foreach (self::$module_tab as $k => $v)
        {
            if (property_exists($tab, $k))
            {
                switch ($k)
                {
                    case 'name':
                        $v = self::getMultilangField($v);
                        break;
                    case 'module':
                        $v = $this->name;
                        break;
                    case 'id_parent':
                        $v = $parent_id;
                        break;
                }

                $tab->{$k} = $v;
            }
        }

        if (!$tab->add())
        {
            $this->_errors[] = $this->l('Unable to create Product label tab');
            return false;
        }

        return true;
    }

    private static function getTabId()
    {
        return Tab::getIdFromClassName(self::$module_tab['class_name']);
    }

    private function deleteTab()
    {
        $tab = self::getTabId();

        if ($tab && Validate::isLoadedObject($tab_obj = new Tab((int)$tab)))
        {
            if ($tab_obj->delete())
                return true;

            $this->_errors[] = $this->l('Unable to remove Product label tab');

            return false;
        }

        return false;
    }

    private static function tabExists()
    {
        return self::getTabId();
    }

    public function getMultilangField($field_array)
    {
        $languages = self::getLanguages();
        $prepared = array();

        foreach ($languages as $language) {
            if( isset($field_array[$language['iso_code']]))
                $prepared[$language['id_lang']] = $field_array[$language['iso_code']];
            else
                $prepared[$language['id_lang']] = $field_array['default'];
        }

        return $prepared;
    }

    public static function getLanguages()
    {
        if (!is_array(self::$module_languages))
            self::$module_languages = Language::getLanguages();

        return self::$module_languages;
    }

    public function hookDisplaySeoText($params)
    {
        $values = Seoblock::getAll();

        foreach ($values as $item) {
            $category_name = mb_strtolower($item['category_name']);
            $description = str_replace("{category_name}", $category_name, $item['description']);

            $this->smarty->assign(array(
                'active' => $item['active'],
                'description' => $description,
                'title' => $item['title']
            ));
        }

        return $this->display(__FILE__, 'ffcategoryseo.tpl');
    }

}