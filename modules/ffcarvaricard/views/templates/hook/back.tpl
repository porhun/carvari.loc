<div class="col-lg-12">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-credit-card"></i>
            {l s='Карта карвари' mod='ffcarvaricard'}
        </div>

        <form action="" method="post" class="form-horizontal">
            <div class="form-group">
                <label>{l s='№ карты:' mod='ffcarvaricard'}</label>
                <input type="text" class="form-control" name="card[number]" value="{$card.number}">
            </div>
            <div class="form-group">
                <label>html:</label>
                <textarea type="text" class="form-control rte" name="card[html]">{$card.html}</textarea>
            </div>
            <button type="submit" class="btn btn-success">{l s='Изменить' mod='ffcarvaricard'}</button>
        </form>
    </div>
</div>

{addJsDef iso=$iso|addslashes}
{addJsDef ad=$ad|addslashes}