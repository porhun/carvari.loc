<div class="panel " id="configuration_fieldset_general">
    <div class="panel-heading">
        <i class="icon-cloud-upload"></i>
        Экспорт
    </div>
    <div class="panel-body">
        <div class="form-group">
            <div id="conf_id_ffcarvaricard_enable_send">
                <label class="control-label col-lg-3">
                    Запусить экспорт клиентов
                </label>
                <div class="col-lg-9">
				    <a href="{$export_link}" class="btn btn-success">Экспорт в файл XLS</a>
                </div>
            </div>
        </div>
    </div>
</div>