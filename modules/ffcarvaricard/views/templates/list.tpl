<div class="panel">
    <div class="panel-heading">
        {l s='Список созданных статей' mod='ffcarvaricard'}
        <span class="panel-heading-action">
            <a id="desc-product-new" class="list-toolbar-btn" href="{$current_url}&add=1">
                <span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="{l s='Добавить' mod='ffcarvaricard'}"
                      data-html="true" data-placement="top">
                    <i class="process-icon-new" style="color: green;"></i>
                </span>
            </a>

        </span>
    </div>

    <table class="table table-hover">
        <thead>
        <tr>
            <td>ID</td>
            <td>{l s='Дата' mod='ffcarvaricard'}</td>
            <td>{l s='Название' mod='ffcarvaricard'}</td>
            <td>{l s='Группа пользователей' mod='ffcarvaricard'}</td>
            <td>{l s='Статус' mod='ffcarvaricard'}</td>
            <td></td>
        </tr>
        </thead>
        <tbody>
            {foreach from=$htmls item=html}
                <tr>
                    <td>{$html.id_html}</td>
                    <td>{$html.date_add}</td>
                    <td>{$html.title}</td>
                    <td>{$html.group_name}</td>
                    <td>{if $html.active==1}{l s='Активна' mod='ffcarvaricard'}{else}<span class="label label-danger">{l s='Скрыта' mod='ffcarvaricard'}</span>{/if}</td>
                    <td>
                        <a href="{$current_url}&change={$html.id_html}" class="btn btn-success">{l s='Изменить' mod='ffcarvaricard'}</a>
                        <a href="{$current_url}&del={$html.id_html}" data-confirm='Удалить статью с темой "{$html.title}"?' class="btn btn-danger confirm">{l s='Удалить' mod='ffcarvaricard'}</a>
                    </td>
                </tr>
            {/foreach}
        </tbody>
    </table>
</div>

<div class="modal fade bs-example-modal-sm" id="alert-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">{l s='Подтверждение' mod='ffcarvaricard'}</h4>
            </div>
            <div class="modal-body">
                <p id="confirm-text"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{l s='Отменить' mod='ffcarvaricard'}</button>
                <button type="button" class="btn btn-success" id="confirm-confirm">{l s='Да, уверен!' mod='ffcarvaricard'}</button>
            </div>
        </div>
    </div>
</div>

{literal}
<script type="text/javascript">
    $(document).ready(function () {
        $('a.confirm').on( 'click', function (event) {
            event.preventDefault();
            var element = $(this);
            $('#alert-modal').modal();
            $('#alert-modal #confirm-text').html(element.attr('data-confirm'));
            $('#alert-modal #confirm-confirm').on('click', function(){
                document.location.href = element.attr('href');
            })
        });
    });
</script>
{/literal}