{if !$mobile_device}
    <div class="main_content profile clearfix">
        <div class="min_width">

            <div class="clearfix">
                <h1>{l s='Мій профіль' mod='ffcarvaricard'}</h1>
                <a class="logout_link" href="{if $active_url_lang}{$active_url_lang}{/if}/?mylogout=">{l s='Вийти' mod='ffcarvaricard'}</a>
            </div>

            <div class="profile_nav">
                {$HOOK_CUSTOMER_ACCOUNT}

                <span class="profile_nav_link" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/my-account'">{l s='Персональні дані' mod='ffcarvaricard'}
                    <span class="underline"></span>
                </span>
                <span class="profile_nav_link" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/order-history'">{l s='Статуси замовлень' mod='ffcarvaricard'}
                    <span class="underline"></span>
                </span>


            </div>

            <div class="tab_wrapper">
                <div class="tab_content">
                    <div class="carvari_card">
                        <div class="card_number">{$card.number}</div>
                        <div class="holder_name"><span class="js-holder_name">{$customer->firstname}</span> {$customer->lastname}</div>
                        <div class="holder_phone">{$customer_address.phone_mobile}</div>
                        <div class="holder_bonuses">{l s='у вас' mod='ffcarvaricard'} {$customer->getBonus()} {l s='грн. на бонусному рахунку' mod='ffcarvaricard'}<br />
                            <a href="{if $active_url_lang}{$active_url_lang}{/if}/module/loyalty/default?process=summary">{l s='подивитися докладніше' mod='ffcarvaricard'}</a>
                        </div>
                    </div>
                    <div class="blogs_list">
                        {foreach from=$articles item=article}
                            <span class="blog_link">
                                <div class="img_holder">
                                    {if $active_url_lang and $article.link_ru != ''}
                                        <a href="{$article.link_ru}">
                                            <img src="{$article.img_url}" alt="">
                                        </a>
                                    {elseif $active_url_lang == false and $article.link != ''}
                                        <a href="{$article.link}">
                                            <img src="{$article.img_url}" alt="">
                                        </a>
                                    {else}
                                        <img src="{$article.img_url}" alt="">
                                    {/if}
                                    {*{if $article.link}*}
                                        {*<a href="{$article.link}">*}
                                            {*<img src="{$article.img_url}" alt="">*}
                                        {*</a>*}
                                    {*{else}*}
                                        {*<img src="{$article.img_url}" alt="">*}
                                    {*{/if}*}
                                </div>
                                <div class="blog_title">{$article.title}</div>
                                <div class="blog_text">{$article.html}</div>
                                <div class="blog_date">{$article.date_add}</div>
                            </span>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
{else}
    <div class="main_content body trends-page">
        <div class="profile_nav">
            {$HOOK_CUSTOMER_ACCOUNT}
                <span class="profile_nav_link" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/my-account'">{l s='Персональні дані' mod='ffcarvaricard'}
                    <span class="underline"></span>
                </span>
                <span class="profile_nav_link" onclick="location.href='{if $active_url_lang}{$active_url_lang}{/if}/order-history'">{l s='Статуси замовлень' mod='ffcarvaricard'}
                    <span class="underline"></span>
                </span>
        </div>
        <div class="padded_block">
            <h1>{l s='Карта L’CARVARI' mod='ffcarvaricard'}</h1>
        </div>
        <div class="carvari_card">
            <div class="card_number">{$card.number}</div>
            <div class="holder_name">{$customer->firstname} {$customer->lastname}</div>
            <div class="holder_phone">{$customer->phone_login}</div>
        </div>


        <div class="slider_container slider">

            {foreach from=$articles item=article}
                <div class="one_entry toggled in">

                    {if $active_url_lang and $article.link_ru != ''}
                        <a href="{$article.link_ru}">
                            <img src="{$article.img_url}" alt="{$article.title}" class="full-screen-img cover">
                            <h2>{$article.title}</h2>
                        </a>
                    {elseif $active_url_lang == false and $article.link != ''}
                        <a href="{$article.link}">
                            <img src="{$article.img_url}" alt="{$article.title}" class="full-screen-img cover">
                            <h2>{$article.title}</h2>
                        </a>
                    {else}
                        <img src="{$article.img_url}" alt="{$article.title}" class="full-screen-img cover">
                        <h2>{$article.title}</h2>
                    {/if}

                    {*<a href="{$article.link}">*}
                        {*<img src="{$article.img_url}" alt="{$article.title}" class="full-screen-img cover">*}
                        {*<h2>{$article.title}</h2>*}
                    {*</a>*}

                    <div class="padded_block">
                        <div class="post is_post">
                            <p>
                                {$article.html}
                            </p>
                        </div>

                        <div class="attrs mb20">
                            {$article.date_add}
                        </div>
                    </div>
                </div>
            {/foreach}

        </div>
    </div>
{/if}


