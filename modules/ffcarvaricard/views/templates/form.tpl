<div class="panel">
    <form action="" method="post" enctype="multipart/form-data">
        <div class="panel-heading">
            {l s='Добавить сатью' mod='ffcarvaricard'}
        </div>
        <div class="panel-body">
            <div class="form-group col-sm-3">
                <label for="userGroup">{l s='Выберите группу пользователей:' mod='ffcarvaricard'}</label>
                {*<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">*}
                <select class="form-control" name="html[id_group]" id="userGroup" required>
                    <option></option>
                    {foreach from=$user_groups item=user_group}
                        <option value="{$user_group.id_group}" {if $html.id_group==$user_group.id_group} selected{/if}>{$user_group.name}</option>
                    {/foreach}
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-sm-3">
                <label for="htmlStatus">{l s='Статус:' mod='ffcarvaricard'}</label>
                {*<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Email">*}
                <select class="form-control" name="html[active]" id="htmlStatus">
                    <option value="1" {if $html.active===1} selected{/if}>{l s='Опубликовать' mod='ffcarvaricard'}</option>
                    <option value="0" {if $html.active===0} selected{/if}>{l s='Скрыть' mod='ffcarvaricard'}</option>
                </select>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-sm-3">
                <label for="htmlImage">{l s='Картинка:' mod='ffcarvaricard'}</label>
                {if $img_url}
                    {$img_url}
                {/if}
                <input type="file"  name="image" id="htmlImage">
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-sm-3">
                <label for="htmlLink">{l s='Ссылка для картинки:' mod='ffcarvaricard'}</label>
                <input type="text" class="form-control"  name="html[link]" id="htmlLink" value="{$html.link}">
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-sm-3">
                <label for="htmlLink_ru">{l s='Ссылка для картинки русская версия:' mod='ffcarvaricard'}</label>
                <input type="text" class="form-control"  name="html[link_ru]" id="htmlLink_ru" value="{$html.link_ru}">
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-sm-3">
                <label for="htmlTitle">{l s='Название статьи:' mod='ffcarvaricard'}</label>
                <input type="text" class="form-control"  name="html[title]" id="htmlTitle" value="{$html.title}">
            </div>

            <div class="clearfix"></div>
            <div class="form-group col-sm-12">
                <label for="htmlText">{l s='Статья:' mod='ffcarvaricard'}</label>
                <textarea class="rte"  name="html[html]" rows="10" id="htmlText">{$html.html}</textarea>
            </div>
        </div>
        <div class="panel-footer">
            <a href="{$back_url}" class="btn btn-default"><i class="process-icon-back"></i>{l s='К списку' mod='ffcarvaricard'}</a>
            {if $html}
                <input type="hidden" name="html[id_html]" value="{$html.id_html}">
                <button type="submit" name="submitChhtml" class="btn btn-default pull-right"><i class="process-icon-save"></i>{l s='Изменить' mod='ffcarvaricard'}</button>
                <button type="submit" name="submitChhtmlStay" class="btn btn-default pull-right"><i class="process-icon-save"></i>{l s='Изменить и остаться' mod='ffcarvaricard'}</button>
            {else}
                <button type="submit" name="submitAddhtml" class="btn btn-default pull-right"><i class="process-icon-save"></i>{l s='Сохранить' mod='ffcarvaricard'}</button>
                <button type="submit" name="submitAddhtmlStay" class="btn btn-default pull-right"><i class="process-icon-save"></i>{l s='Сохранить и остаться' mod='ffcarvaricard'}</button>
            {/if}
        </div>
    </form>
</div>

{addJsDef iso=$iso|addslashes}
{addJsDef ad=$ad|addslashes}
