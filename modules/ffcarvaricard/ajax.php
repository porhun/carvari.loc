<?php
require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');

if ( Tools::getValue('secret') != '7carvarisecret7' )
    exit();

require_once(__DIR__.'/ffcarvaricard.php');

$module = new Ffcarvaricard();

if ( Tools::getValue('command') == 'get_card_bonus_list' )
    $module->getCardBonusList();
elseif ( Tools::getValue('command') == 'cron_bonuses_compare' )
    $module->cronBonusesCompare();

exit();