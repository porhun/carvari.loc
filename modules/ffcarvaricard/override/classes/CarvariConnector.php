<?php


class CarvariConnector
{

    private $api_url;
    private $api_key;
    private $test_mode = true;

    private $api_pathes = array();

    public function __construct(){
        $this->api_key = array('API-key:'.Configuration::get('ffcarvaricard_api'));
//        $this->api_key = array('API-key:9b7ne4056e1209djsd42f2beb75ae6d8a458');
        $this->api_url = Configuration::get('ffcarvaricard_url');

        if (Configuration::get('ffcarvaricard_enable_send'))
            $this->test_mode = false;
    }


    public function getCardInfo($card_number)
    {
        $info = $this->apiSendRequest($this->api_url.'/Cards/'.$card_number);
        if ( $info ) {
            $info = preg_replace("/[^a-zA-ZА-Яа-я0-9:,\s]/ui",'',$info);
            $info = explode(',',$info);
            return array(
                'card_number' => explode(':',$info[0])[1],
                'first_name' => explode(':',$info[2])[1],
                'last_name' => explode(':',$info[1])[1],
                'phone' => explode(':',$info[3])[1],
                'email' => explode(':',$info[4])[1],
                'city' => explode(':',$info[5])[1],
            );

        }

        return false;
    }

    public function getClientBonusByCardId( $card_number )
    {
        $info = $this->apiSendRequest($this->api_url.'/Bonuses/'.$card_number);
        return $info;
    }

    public function getClientSumBonusByCardId( $card_number )
    {
        $info = $this->apiSendRequest($this->api_url.'/SumBonuses/'.$card_number);
        return $info;
    }

    public function getCarvariBonuses()
    {
        $info = $this->apiSendRequest($this->api_url.'/SumBonuses');
        return json_decode($info);
    }

    public function addClientCard($card_number, $last_name, $first_name, $phone, $email, $city, $date_add = null )
    {
        if (!$date_add) $date_add = date('Y-m-d H:i');
        $cr_arr = array(
            $card_number,
            $last_name,
            $first_name,
            $phone,
            $email,
            $city,
            $date_add,
        );

        $cr_arr = array_map('trim', $cr_arr);
        $url_str = implode('/', $cr_arr);

        $handle = fopen(__DIR__.'/../../log/log_api.txt', 'a');
        fwrite($handle, date('Y-m-d H:i').' - '.$this->api_url.'/Cards/'.$url_str."\n");
        fclose($handle);

        return $this->apiSendRequest($this->api_url.'/Cards/'.$url_str, null, 'POST');
    }

    public function updateClientCard($card_number, $last_name, $first_name, $phone, $email, $city, $date_add = null )
    {
        if (!$date_add) $date_add = date('Y-m-d H:i');
        $upd_arr = array(
            'GianniFIO' => $last_name,
            'GianniStreet' => $first_name,
            'GianniPhone1' => $phone,
            'GianniEmail1' => $email,
            'GianniCity' => $city,
            'GianniDateEdit' => $date_add,
        );

        $upd_arr = array_map('trim', $upd_arr);

        foreach ($upd_arr as $key=>$value) {
            if ( !isset($value) || !$value ) {
                unset($upd_arr[$key]);
                continue;
            }
            $upd_arr[$key] = '['.$key.']='.'"'.$value.'"';
        }


        return $this->apiSendRequest($this->api_url.'/Cards/'.$card_number, $upd_arr, 'PUT');
    }

    public function postBonusUsed($card_number, $id_order, $sum, $date = null)
    {
        if ($sum == 0) return true;
        if (!$date) $date = date('Y-m-d H:i');
        $url_str = implode('/', array( $card_number, $id_order, $sum, $date));

        $handle = fopen(__DIR__.'/../../log/log_bonuses.txt', 'a');
        fwrite($handle, date('Y-m-d H:i').' - '.$this->api_url.'/Bonuses/'.$url_str."\n");
        fclose($handle);

        return $this->apiSendRequest($this->api_url.'/Bonuses/'.$url_str, null, 'POST');
    }

    public function apiSendRequest($url, $json_value = null, $custom_request = null)
    {
        if ( $this->test_mode )
            return true;

        $ch = curl_init();

        if ($json_value) {
//            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, implode(',', $json_value));
        }

        if ($custom_request)
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom_request);

        curl_setopt($ch, CURLOPT_HEADER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->api_key);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        if ($output = curl_exec($ch)) {
//            d($output);
            $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
            $header = substr($output, 0, $header_size);
            $body = substr($output, $header_size);

            curl_close($ch);

//            $return = json_decode($body,true);
            if (preg_match('/HTTP\/1.1 200 OK\b/', $header)) {
                return $body;
            } else {
                $this->warning = $body;
                return false;
            }
        } else {
            $this->warning = 'Превышено время ожидания ответа от сервера: ' . $this->api_url;
            return false;
        }

    }
}