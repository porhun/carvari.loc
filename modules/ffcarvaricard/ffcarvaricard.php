<?php
if (!defined('_PS_VERSION_'))
    exit;

class Ffcarvaricard extends Module
{
    public function __construct()
    {
        $this->name = 'ffcarvaricard';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0.1';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Carvari Discount Cards - карта скидки клиента');
        $this->description = $this->l('Carvari Discount Cards - карта скидки клиента.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');


        $this->img_dir = _PS_IMG_DIR_.'carvari';
        $this->img_height = 300;
        $this->img_width = 490;
        $this->max_image_size = 4000000;
        $this->imageType = 'jpg';
    }

    public function install()
    {
        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        @mkdir($this->img_dir);

        if (!parent::install() ||
            !$this->installSql() ||
            !$this->updateSql() ||
            !$this->updateSqlOrder() ||
//            !$this->registerHook('actionCustomerAccountAdd') ||
            !$this->registerHook('actionObjectCustomerAddAfter') ||
            !$this->registerHook('actionObjectOrderAddAfter') ||
            !$this->registerHook('displayCustomerAccount') ||
            !$this->registerHook('displayAdminCustomers') ||
            !Configuration::updateValue('ffcarvaricard_enable_send' , 0) ||
            !Configuration::updateValue('ffcarvaricard_api' , '9b7ne4056e1209djsd42f2beb75ae6d8a458') ||
            !Configuration::updateValue('ffcarvaricard_url' , 'https://ftp.carvari.com:8888')
        )
            return false;
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->uninstallSql())
            return false;
        return true;
    }

    private function installSql(){
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards`');

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffusercards` (
			`id_card` INT NOT NULL AUTO_INCREMENT,
			`id_customer` INT NOT NULL,
			`number` BIGINT,
			`html` TEXT,
		PRIMARY KEY (`id_card`),
		UNIQUE INDEX (`number`),
		UNIQUE INDEX (`id_customer`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffusercards_html` (
			`id_html` INT NOT NULL AUTO_INCREMENT,
			`id_group` INT NOT NULL,
			`id_customer` INT NOT NULL,
			`date_add` DATETIME,
			`active` tinyint,
			`title` varchar(200),
			`link` varchar(200),
			`link_ru` varchar(200),
			`html` TEXT,
		PRIMARY KEY (`id_html`),
		INDEX (`id_group`),
		INDEX (`id_customer`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        return true;
    }

    private function updateSql(){
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'ffusercards_html` ADD `link` varchar(200) default \'\' AFTER `title`');
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'ffusercards_html` ADD `link_ru` varchar(200) default \'\' AFTER `link`');


        return true;
    }

    private function fixSql(){
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'customer` ADD INDEX phone_login (`phone_login`)');
        Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'customer` SET `is_offline_only`=1 WHERE `company` IS NULL');
    }

    private function updateSqlOrder(){
        Db::getInstance()->execute('ALTER TABLE `'._DB_PREFIX_.'orders` ADD `carvari_order_id` varchar(20) default \'\' AFTER `reference`');


        return true;
    }

    private function uninstallSql(){
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards`');
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards_html`');

        return true;
    }

    protected function get_image_url($id){
        $image = $this->img_dir.'/'.$id.'.'.$this->imageType;
        return ImageManager::thumbnail($image, 'carvari_'.(int)$id.'.'.$this->imageType, 350,
            $this->imageType, false, true);
    }

    protected function postImage($id)
    {
        $ret = true;


        foreach ($_FILES as $filed_name=>$file) {
            if(!$file['error']){
                // Check image validity
                $max_size = isset($this->max_image_size) ? $this->max_image_size : 0;
                if ($error = ImageManager::validateUpload($_FILES[$filed_name], Tools::getMaxUploadSize($max_size))) {
                    $this->errors[] = $error;
                }
                $tmp_name = tempnam(_PS_TMP_IMG_DIR_, 'PS');
                if (!$tmp_name) {
                    return false;
                }

                if (!move_uploaded_file($_FILES[$filed_name]['tmp_name'], $tmp_name)) {
                    return false;
                }

                // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
                if (!ImageManager::checkImageMemoryLimit($tmp_name)) {
                    $this->errors[] = Tools::displayError('Due to memory limit restrictions, this image cannot be loaded. Please increase your memory_limit value via your server\'s configuration settings. ');
                }

                // Copy new image
                if (empty($this->errors) && !ImageManager::resize($tmp_name, $this->img_dir.'/'.$id.'.'.$this->imageType, (int)$this->img_width, (int)$this->img_height, $this->imageType)) {
                    $this->errors[] = Tools::displayError('An error occurred while uploading the image.');
                }

                unlink($tmp_name);

            }
        }
        return $ret;
    }

    public function getContent()
    {
//        $this->compareCards();

//        $this->postBonusesToCarvari();
//        require_once(__DIR__.'/override/classes/CarvariConnector.php');
//        $connector = new CarvariConnector();
//        d($connector->getClientBonusByCardId(500004));
//        d($connector->addClientCard(699999, 'Testovfff', 'Test', '380671234567', 'test@test.com', 'NoCity'));

//        d($connector->postBonusUsed(500004, 12345, +130));
//        die('dd');

        if (Tools::getIsset('compareFromGMS'))
        {
            $this->compareFromGMS();
        }
        if (Tools::getIsset('cardnumsFromGMS'))
        {
            $this->cardnumsFromGMS();
        }

        if (Tools::isSubmit('submitOptions'))
        {
            Configuration::updateValue('ffcarvaricard_enable_send' , Tools::getValue('ffcarvaricard_enable_send'));
            Configuration::updateValue('ffcarvaricard_api' , Tools::getValue('ffcarvaricard_api'));
            Configuration::updateValue('ffcarvaricard_url' , Tools::getValue('ffcarvaricard_url'));

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&conf=4&token='.Tools::getAdminTokenLite('AdminModules'));

        }
        if (Tools::getIsset('fixOrders'))
        {
            $this->fixOrders();
        }

        if (Tools::getIsset('clearOrdersFromXML'))
        {
            $this->clearOrdersFromXML();
        }

        if (Tools::getValue('clearOrdersByCarvariCard'))
        {
            $this->clearOrdersByCarvariCard(Tools::getValue('clearOrdersByCarvariCard'));
        }



        if (Tools::getIsset('makeExport'))
        {
            $from_card = null;
            if (Tools::getValue('from_card'))
                $from_card = Tools::getValue('from_card');

            $this->makeExport($from_card);
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&conf=4&token='.Tools::getAdminTokenLite('AdminModules'));

        }
        if (Tools::getIsset('exportUsedBonuses'))
        {
            $this->postUsedBonusesToCarvari();
        }

        if (Tools::getIsset('exportAllBonusesFromXml'))
        {
            $this->postBonusesToCarvariFromXml();
        }



        if (Tools::getIsset('exportAllBonuses'))
        {
            $id_card = null;
            if(Tools::getValue('id_card'))
                $id_card = (int)Tools::getValue('id_card');

            $this->postBonusesToCarvari($id_card);
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&conf=4&token='.Tools::getAdminTokenLite('AdminModules'));

        }


        if (Tools::getValue('fixSql'))
        {
            $this->fixSql();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&conf=4&token='.Tools::getAdminTokenLite('AdminModules'));

        }

        if (Tools::getValue('updateSQL'))
        {
            $this->updateSql();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&conf=4&token='.Tools::getAdminTokenLite('AdminModules'));

        }

        if (Tools::getValue('updateSqlOrder'))
        {
            $this->updateSqlOrder();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&conf=4&token='.Tools::getAdminTokenLite('AdminModules'));

        }

        if (Tools::getValue('import'))
        {
            $this->importCards();
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&conf=4&token='.Tools::getAdminTokenLite('AdminModules'));

        }

        if (Tools::getValue('del'))
        {
            $html_id = (int)Tools::getValue('del');
            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'ffusercards_html` WHERE id_html='.$html_id);
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }

        if( Tools::isSubmit('submitAddhtml') || Tools::isSubmit('submitAddhtmlStay')) {
            $data = Tools::getValue('html');
            Db::getInstance()->insert('ffusercards_html',array(
                'id_group' => pSQL($data['id_group']),
                'active' => pSQL($data['active']),
                'title' => pSQL($data['title']),
                'link' => pSQL($data['link']),
                'link_ru' => pSQL($data['link_ru']),
                'date_add' => date('Y-m-d H:i:s'),
                'html' => pSQL($data['html'], true),
            ));

            $id = Db::getInstance()->getValue('SELECT max(id_html) FROM '._DB_PREFIX_.'ffusercards_html');
            $this->postImage($id);

            if (Tools::isSubmit('submitAddhtml'))
                Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
            else
                Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&change='.$id);
        }

        if( Tools::isSubmit('submitChhtml') || Tools::isSubmit('submitChhtmlStay') ) {
            $data = Tools::getValue('html');
            Db::getInstance()->update('ffusercards_html',array(
                'id_group' => pSQL($data['id_group']),
                'active' => pSQL($data['active']),
                'title' => pSQL($data['title']),
                'link' => pSQL($data['link']),
                'link_ru' => pSQL($data['link_ru']),
                'html' => pSql($data['html'],true),
            ), 'id_html='.(int)$data['id_html']);

            $this->postImage((int)$data['id_html']);

            if(Tools::isSubmit('submitChhtml'))
               Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }

        if( Tools::getValue('add') || Tools::getValue('change')) {
            return $this->getForm();
        }

        if(Tools::getIsset('makeExportXLS')) {
            return $this->makeExportXLS();
        }

        $this->context->smarty->assign([
            'export_link' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules').'&makeExportXLS'
        ]);

        return $this->displayConfigForm().$this->context->smarty->fetch(__DIR__.'/views/templates/admin/export.tpl').$this->getList();
    }

    public function displayConfigForm()
    {


        $this->fields_options = array(
            'general' => array(
                'title' => 'Настройка модуля',
                'fields' => array(

                    'ffcarvaricard_enable_send' => array(
                        'title' => 'Включить отправку данных к Carvari',
                        'cast' => 'strval',
                        'type' => 'bool',
                        'size' => '10'
                    ),

                    'ffcarvaricard_api' => array(
                        'title' => 'API Key',
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '30',
                    ),

                    'ffcarvaricard_url' => array(
                        'title' => 'URL Carvari сервера',
                        'cast' => 'strval',
                        'type' => 'text',
                        'size' => '30'
                    ),

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'button',
                    'name' => 'submitOptions'
                )
            )
        );

        $helper = new HelperOptionsCore();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
        return $helper->generateOptions($this->fields_options);
    }

    public function importCards() {
        set_time_limit(2400);
        die('this method is off');

        $separator = ';';
        $tmp_array = array();

        $filename = __DIR__.'/oldcards_db/db.csv';


        $handle = fopen($filename,'r');
        for ($current_line = 0; $line = fgetcsv($handle, MAX_LINE_SIZE, $separator); $current_line++) {

//            if ( $current_line > 50)
//                continue;

            $line = array_map('mb_convert_encoding' , $line, array_fill(0, count($line), 'utf-8'), array_fill(0, count($line), 'cp1251'));
            $line = array_map('trim',$line);
            $line = array_map('str_replace', array_fill(0, count($line), '#'),array_fill(0, count($line), ''), $line);


            $customer = new Customer();

            if ( ( Customer::isCustomerExistsByPhone($line[4]) ) )
            {
                $customer = $customer->getByPhone2($line[4]);
                $tmp_array[] = $line[4];
            } /*else {
                $customer->setPhoneLogin( $line[4] );
                $customer->email = $line[4].'@carvari.com';
                $customer->setWsPasswd( Customer::generatePin() );
                $is_new_customer = true;
            }*/

            $customer->firstname = $line[3];
            $customer->lastname = $line[2];

            if ( $customer->validateFields( false ) )
            {

                if ( $is_new_customer )
                    $customer->add();
                else
                    $customer->save();

                if (!Address::getFirstCustomerAddressId($customer->id)) {
                    $address = new Address();
                    $address->id_customer = $customer->id;
                    $address->id_country = (int)Configuration::get('PS_COUNTRY_DEFAULT');
                    $address->alias = 'My Address';
                    $address->lastname = $line[2];
                    $address->firstname = $line[3];
                    $address->address1 = 'Address Sample';
                    $address->phone_mobile = $line[4];
                    $address->city = Validate::isCityName($line[5]) ? $line[5] : 'Some';
                    $address->add();
                }

                Db::getInstance()->insert('ffusercards', array(
                    'id_customer' => $customer->id,
                    'number' => $line[0],
                ));

            }
        }
        fclose($handle);
        $handle = fopen(__DIR__.'/oldcards_db/repeted_list_'.date('Y-m-d_H-i').'.txt','w+');
        fwrite($handle, implode("\n", $tmp_array));
        fclose($handle);
    }

    public function getList()
    {
        $htmls = Db::getInstance()->executeS('SELECT fh.*, gl.name as group_name FROM `'._DB_PREFIX_.'ffusercards_html` fh LEFT JOIN `'._DB_PREFIX_.'group_lang` gl ON gl.`id_group`=fh.`id_group` AND gl.`id_lang`='.$this->context->language->id.' ORDER BY `date_add` DESC');

        $this->context->smarty->assign(
            array(
                'current_url' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'htmls' => $htmls
            ));

        return $this->context->smarty->fetch(__DIR__.'/views/templates/list.tpl');
    }



    public function getForm()
    {
        $this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
        $this->context->controller->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
        $this->context->controller->addJS($this->_path.'/ffcarvaricard.js');

        $user_groups = Group::getGroups($this->context->language->id);
//        d($user_groups);
        if ( Tools::getValue('change') )
        {
            $id_html = (int)Tools::getValue('change');
            $row = Db::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'ffusercards_html` WHERE id_html='.$id_html);
            $this->context->smarty->assign(array(
                    'html' => $row,
                    'img_url' => $this->get_image_url($id_html)
                ));
        }

        $iso = $this->context->language->iso_code;
        $this->context->smarty->assign(
            array(
                'back_url' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'user_groups' => $user_groups,
                'iso' =>  file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en',
                'ad' =>  __PS_BASE_URI__.basename(_PS_ADMIN_DIR_)
            ));

        return $this->context->smarty->fetch(__DIR__.'/views/templates/form.tpl');
    }

    public function hookDisplayCustomerAccount()
    {
//        d($this->context);
        $card = Db::getInstance()->getRow('SELECT `number`, `html` FROM `'._DB_PREFIX_.'ffusercards` WHERE id_customer='.(int)$this->context->cart->id_customer);
        $this->context->smarty->assign(array(
            'card' => $card,
        ));
        return $this->context->smarty->fetch(__DIR__.'/views/templates/hook/front.tpl');
    }

    public function hookDisplayAdminCustomers()
    {
        if (Tools::getValue('card')) {
            $card = Tools::getValue('card');
            Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffusercards`
            (`id_customer`,`number`,`html`) VALUES ('.(int)$this->context->customer->id.',"'.pSQL($card['number']).'","'.pSQL($card['html'], true).'")
            ON DUPLICATE KEY UPDATE number=values(`number`),html=values(`html`)');
            Tools::redirectAdmin($this->context->link->getAdminLink('AdminCustomers', true).'&id_customer='.(int)$this->context->customer->id.'&viewcustomer');
        }
        $card = Db::getInstance()->getRow('SELECT `number`, `html` FROM `'._DB_PREFIX_.'ffusercards` WHERE id_customer='.(int)$this->context->customer->id);
        $iso = $this->context->language->iso_code;
        $this->context->smarty->assign(array(
            'card' => $card,
            'iso' =>  file_exists(_PS_CORE_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en',
            'ad' =>  __PS_BASE_URI__.basename(_PS_ADMIN_DIR_)
        ));

        $this->context->controller->addJS(_PS_JS_DIR_.'tiny_mce/tiny_mce.js');
        $this->context->controller->addJS(_PS_JS_DIR_.'admin/tinymce.inc.js');
        $this->context->controller->addJS($this->_path.'/ffcarvaricard.js');

        return $this->context->smarty->fetch(__DIR__.'/views/templates/hook/back.tpl');
    }

    public function hookActionObjectCustomerAddAfter ($customer)
    {
        $carvari_card = 0;
        if ( isset($customer['object']->carvari_card_tmp) && $customer['object']->carvari_card_tmp ) {

            $carvari_card = $customer['object']->carvari_card_tmp;

            Db::getInstance()->insert('ffusercards', array(
                'id_customer' => $customer['object']->id,
                'number' => $carvari_card,
            ));
        } else {

            $last_card = Db::getInstance()->getValue('SELECT MAX(`number`) AS number  FROM `'._DB_PREFIX_.'ffusercards` WHERE `number`>=500000 AND `number`<700000');
            $card_number = $last_card ? ($last_card+1) : 500000;

            if (Db::getInstance()->insert('ffusercards', array(
                'id_customer' => $customer['object']->id,
                'number' => $card_number,
            ))) {
                require_once(__DIR__.'/override/classes/CarvariConnector.php');
                $connector = new CarvariConnector();
                $connector->addClientCard($card_number, $customer['object']->lastname, $customer['object']->firstname, $customer['object']->phone_login, $customer['object']->email, 'NoCity');
            }
        }
        return true;
    }

    public function hookActionObjectOrderAddAfter ($order)
    {
        if ( isset($order['object']->carvari_order_id) && $order['object']->carvari_order_id ) {
            $order['object']->setCurrentStateFF(2);
            $order['object']->addOrderPayment($order['object']->total_paid);
        }
        return true;
    }

    public function makeExport($from_cards = null)
    {
        //USAGE  &makeExport[&from_card=CARD_NUMBER]


	    set_time_limit(3600);
        require_once(__DIR__.'/override/classes/CarvariConnector.php');
        $connector = new CarvariConnector();

//        $usercards = Db::getInstance()->executeS('SELECT fu.*, c.lastname, c.firstname,c .phone_login, c.email FROM `'._DB_PREFIX_.'ffusercards` fu LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer=fu.id_customer WHERE fu.`number`>10000000');
//
//        $last_card = Db::getInstance()->getValue('SELECT MAX(`number`) AS number FROM ( SELECT * FROM `'._DB_PREFIX_.'ffusercards` WHERE `number`>=500000 AND `number`<700000) t');
//        $card_number = $last_card ? ($last_card+1) : 500000;
//        foreach ($usercards as $ucard) {
//            Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffusercards` SET `number`='.$card_number.' WHERE `id_card`='.$ucard['id_card']);
//            $card_number++;
//        }

        $customers_export = Db::getInstance()->executeS('SELECT fu.*, c.lastname, c.firstname, c.phone_login, c.email FROM `'._DB_PREFIX_.'ffusercards` fu LEFT JOIN '._DB_PREFIX_.'customer c ON c.id_customer=fu.id_customer'.
            (($from_cards) ? ' WHERE fu.`number`>='.(int)$from_cards : '' )
        );

        foreach ($customers_export as $customer){
            // Export user to Carvari server
            $result = $connector->addClientCard($customer['number'], $customer['lastname'], $customer['firstname'], $customer['phone_login'], $customer['email'], 'NoCity');
            if ($result){
                // Export user Bonuses to Carvari server
                $loyalty_plus = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'loyalty WHERE id_loyalty_state IN (2, 5)  AND id_customer='.$customer['id_customer']);
                foreach ($loyalty_plus as $l){
                    $connector->postBonusUsed($customer['number'],$l['id_order'],$l['points'], date("Y-m-d H:i", strtotime($l['date_add'])) );
                }

                $loyalty_minus = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'loyalty WHERE id_loyalty_state=4  AND id_customer='.$customer['id_customer']);
                foreach ($loyalty_minus as $l){
                    $connector->postBonusUsed($customer['number'],$l['id_order'],(-1)*$l['points'], date("Y-m-d H:i", strtotime($l['date_add'])) );
                }
            }

        }
    }

    public function getCardBonusList()
    {
        $validity_period = Configuration::get('PS_LOYALTY_VALIDITY_PERIOD');
        $sql_period = '';
        if ((int)$validity_period > 0)
            $sql_period = ' AND datediff(NOW(),l.date_add) <= '.$validity_period;

        $result = Db::getInstance()->executeS('
        SELECT c.`number` as carvari_card, t3.`points` as bonus_points FROM (SELECT  t.id_customer, IFNULL(t.points+t2.points, 0) points FROM (SELECT id_customer, SUM(points) points FROM `'._DB_PREFIX_.'loyalty` l WHERE l.`id_loyalty_state` IN(2,5) '.$sql_period.' GROUP BY `id_customer`) t
        LEFT JOIN (SELECT id_customer, SUM(points) points FROM (SELECT id_customer, points FROM `'._DB_PREFIX_.'loyalty` l WHERE l.`id_loyalty_state`=3 AND l.`points`<0'.$sql_period.') t GROUP BY `id_customer`) t2
        ON t.id_customer=t2.id_customer) t3
        LEFT JOIN `'._DB_PREFIX_.'ffusercards` c ON c.`id_customer`=t3.id_customer
        WHERE points!=0');

        die(json_encode($result));
    }

    public function cronBonusesCompare()
    {
        set_time_limit(2400);
        $log_wrong = '';
        $log_no_data = '';
        require_once(__DIR__.'/override/classes/CarvariConnector.php');
        $connector = new CarvariConnector();

        $validity_period = Configuration::get('PS_LOYALTY_VALIDITY_PERIOD');
        $sql_period = '';
        if ((int)$validity_period > 0)
            $sql_period = ' AND datediff(NOW(),l.date_add) <= '.$validity_period;

        $result = Db::getInstance()->executeS('
        SELECT cus.`id_customer`, c.`number` as carvari_card, cus.`phone_login`, CONCAT_WS(\' \', cus.`lastname`, cus.`firstname`) as fio, t3.`points` as bonus_points FROM (SELECT  t.id_customer, IFNULL(IFNULL(t.points,0)+IFNULL(t2.points,0), 0) points FROM (SELECT id_customer, SUM(points) points FROM `'._DB_PREFIX_.'loyalty` l WHERE l.`id_loyalty_state` IN(2,5)'.$sql_period.' GROUP BY `id_customer`) t
        LEFT JOIN (SELECT id_customer, SUM(points) points FROM (SELECT id_customer, points FROM `'._DB_PREFIX_.'loyalty` l WHERE l.`id_loyalty_state`=3 AND l.`points`<0'.$sql_period.') t GROUP BY `id_customer`) t2
        ON t.id_customer=t2.id_customer) t3
        LEFT JOIN `'._DB_PREFIX_.'ffusercards` c ON c.`id_customer`=t3.id_customer
        LEFT JOIN `'._DB_PREFIX_.'customer` cus ON cus.`id_customer`=t3.id_customer
        WHERE cus.`id_customer` IS NOT NULL');

//        d($result);

        $carvari_bonuses = $connector->getCarvariBonuses();

//        d($carvari_bonuses);

        $carvari_bonuses_array = array();
        foreach ($carvari_bonuses->Bonuses as $bonus){
            $carvari_bonuses_array[$bonus->CardID] = $bonus->Bonus;
        }

        $cards_exist = array();
        $log_wrong = 'id|card num|mob tel|fio|inet bonuses|gms bonuses'."\n";
        foreach($result as $client){
//            $carvari_card_info = $connector->getCardInfo($client['carvari_card']);
            if (isset($carvari_bonuses_array[$client['carvari_card']])) {
                $raznica = floor($carvari_bonuses_array[$client['carvari_card']]) - $client['bonus_points'];
                if( -3 > $raznica || $raznica > 3) {
//                    $log_wrong .= implode('|', $client).'|'.$carvari_bonuses_array[$client['carvari_card']]."|-- on carvari server --|".implode('|', $carvari_card_info)."\n";
                    $log_wrong .= implode('|', $client).'|'.$carvari_bonuses_array[$client['carvari_card']]."\n";
                }
            } else {
//                $log_wrong .= implode('|', $client).'|0'."|-- on carvari server --|".implode('|', $carvari_card_info)."\n";
                $log_wrong .= implode('|', $client).'|0'."|-- no such card on carvari server --|"."\n";
            }
            $cards_exist[] = $client['carvari_card'];
        }

//        $cards_not_exist = array_diff(array_keys($carvari_bonuses_array),$cards_exist);
//
//        $result_not_exist = Db::getInstance()->executeS('SELECT c.`id_customer`, uc.`number` as carvari_card, c.`phone_login`, CONCAT_WS(\' \', c.`lastname`, c.`firstname`) AS fio, 0 AS bonus_points FROM `ps_ffusercards` uc
//                LEFT JOIN `ps_customer` c ON c.`id_customer`=uc.`id_customer`
//                WHERE uc.`number` IN ('.implode(',', $cards_not_exist).')');
//
//        foreach($result_not_exist as $client){
//            $log_wrong .= implode('|', $client).'|'.$carvari_bonuses_array[$client['carvari_card']]."\n";
//        }

        $handle = fopen(__DIR__.'/log/log.txt', 'w');
        fwrite($handle, $log_wrong);
        fclose($handle);
        d($log_wrong);



    }
    public function postBonusesToCarvariFromXml()
    {
//              Запуск из УРЛ :
//              &exportAllBonusesFromXml - всех
        $file_name = 'sverka_1910.xlsx';
        $file_dir = __DIR__ . '/tmp/';

        $filename = $file_dir.$file_name;

        if (!file_exists($filename))
            return true;

        require_once(__DIR__.'/lib/simplexlsx/simplexlsx.class.php');
        $Reader = new SimpleXLSX($filename);

        $current_line = 0;
        foreach ($Reader->rows() as $line) {
            $current_line++;

            if ($current_line<2) continue;
            $line = array_map('trim', $line);


            $this->postBonusesToCarvari($line[0]);

        }

        return true;
    }

    public function postBonusesToCarvari($carvari_card_id = null)
    {
        set_time_limit(2400);

//              Запуск из УРЛ :
//              &exportAllBonuses - всех
//              &exportAllBonuses&id_card=XXXXXX  - только по клиенту с картой XXXXXX

        require_once _PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php';
        require_once(__DIR__.'/override/classes/CarvariConnector.php');
        $connector = new CarvariConnector();

        if($carvari_card_id) {
            $customer = Customer::getByCarvariCardNumber($carvari_card_id);

            if(!$customer)
                return false;
        }

        $bonus_plus = Db::getInstance()->executeS('
		SELECT f.id_customer, f.points, f.`id_order`, f.`date_add`, c.number AS carvari_card
		FROM `'._DB_PREFIX_.'loyalty` f'.
            ' INNER JOIN `ps_orders` o ON o.`id_order`=f.`id_order` AND o.`carvari_order_id`=\'\''.
            ' INNER JOIN `ps_ffusercards` c ON c.`id_customer`=f.`id_customer`'.
            ' WHERE f.id_loyalty_state IN ('.(int)(LoyaltyStateModule::getValidationId()).', '.(int)(LoyaltyStateModule::getConvertId()).', '.(int)(LoyaltyStateModule::getNoneAwardId()).')'.
            ( $carvari_card_id ? ' AND f.id_customer = '.(int)$customer->id : ' '));

        $bonus_minus = Db::getInstance()->executeS('
		SELECT f.id_customer, f.points, f.`id_order`, f.`date_add`, c.number AS carvari_card
		FROM `'._DB_PREFIX_.'loyalty` f'.
            ' INNER JOIN `ps_orders` o ON o.`id_order`=f.`id_order`'.
            ' INNER JOIN `ps_ffusercards` c ON c.`id_customer`=f.`id_customer`'.
            ' WHERE f.id_loyalty_state = '.(int)LoyaltyStateModule::getConvertId().
            ( $carvari_card_id ? ' AND f.id_customer = '.(int)$customer->id : ' '));

/*        $bonus_plus = Db::getInstance()->executeS('
		SELECT f.id_customer, f.points, f.`id_order`, f.`date_add`, c.number AS carvari_card
		FROM `'._DB_PREFIX_.'loyalty` f'.
            ' INNER JOIN `ps_orders` o ON o.`id_order`=f.`id_order` AND o.`carvari_order_id`=\'\''.
            ' INNER JOIN `ps_ffusercards` c ON c.`id_customer`=f.`id_customer`'.
            ' WHERE f.id_loyalty_state IN ('.(int)(LoyaltyStateModule::getValidationId()).', '.(int)(LoyaltyStateModule::getConvertId()).', '.(int)(LoyaltyStateModule::getNoneAwardId()).')'.
            ( $carvari_card_id ? ' AND f.id_customer = '.(int)$customer->id : ' AND c.`number`>=500000 AND c.`number`<600000'));

        $bonus_minus = Db::getInstance()->executeS('
		SELECT f.id_customer, f.points, f.`id_order`, f.`date_add`, c.number AS carvari_card
		FROM `'._DB_PREFIX_.'loyalty` f'.
            ' INNER JOIN `ps_orders` o ON o.`id_order`=f.`id_order`'.
            ' INNER JOIN `ps_ffusercards` c ON c.`id_customer`=f.`id_customer`'.
            ' WHERE f.id_loyalty_state = '.(int)LoyaltyStateModule::getConvertId().
            ( $carvari_card_id ? ' AND f.id_customer = '.(int)$customer->id : ' AND c.`number`>=500000 AND c.`number`<600000'));*/

        foreach ($bonus_plus as $l){
            $connector->postBonusUsed($l['carvari_card'],$l['id_order'],$l['points'], date("Y-m-d H:i", strtotime($l['date_add'])) );
        }

        foreach ($bonus_minus as $l){
            $connector->postBonusUsed($l['carvari_card'],$l['id_order'],(-1)*$l['points'], date("Y-m-d H:i", strtotime($l['date_add'])) );
        }

        return true;

    }

    public function postUsedBonusesToCarvari() {

        set_time_limit(2400);

//              Запуск из УРЛ :
//              &exportUsedBonuses - всех

        require_once _PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php';
        require_once(__DIR__.'/override/classes/CarvariConnector.php');
        $connector = new CarvariConnector();

        $bonus_minus = Db::getInstance()->executeS('
		SELECT f.id_customer, f.points, f.`id_order`, f.`date_add`, c.number AS carvari_card
		FROM `'._DB_PREFIX_.'loyalty` f'.
            ' INNER JOIN `ps_orders` o ON o.`id_order`=f.`id_order` AND o.`carvari_order_id`=\'\''.
            ' INNER JOIN `ps_ffusercards` c ON c.`id_customer`=f.`id_customer`'.
            ' WHERE f.id_loyalty_state = '.(int)LoyaltyStateModule::getConvertId().

            ' AND c.`number`>=500000 AND c.`number`<600000');

        foreach ($bonus_minus as $l){
            $connector->postBonusUsed($l['carvari_card'],$l['id_order'],(-1)*$l['points'], date("Y-m-d H:i", strtotime($l['date_add'])) );
        }

        return true;
    }



    public function fixOrders($all = false)
    {
//              Запуск из УРЛ :
//              &fixOrders - из файла
//              &fixOrders&all=1  - только по клиенту с картой XXXXXX

        set_time_limit(2400);
        $carvari_array = array();
        $orders = [];

        if ($all) {
            $orders = DB::getInstance()->executeS("SELECT * FROM ps_orders WHERE `carvari_order_id`>0");
        } else {
            $orders = array();

            $file_name = 'sverka_1910.xlsx';
            $file_dir = __DIR__ . '/tmp/';

            $filename = $file_dir.$file_name;

            if (!file_exists($filename))
                return true;

            require_once(__DIR__.'/lib/simplexlsx/simplexlsx.class.php');
            $Reader = new SimpleXLSX($filename);

            $current_line = 0;
            foreach ($Reader->rows() as $line) {
                $current_line++;

                if ($current_line<2) continue;
                $line = array_map('trim', $line);



                if ($customer = Customer::getByCarvariCardNumber($line[0])){
                    $customer_orders = DB::getInstance()->executeS("SELECT * FROM ps_orders WHERE `carvari_order_id`>0 AND `id_customer`=".$customer->id);
                    $orders =  array_merge($orders, $customer_orders);

                }

            }


            require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
            require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

            foreach ($orders as $order) {
                $order_obj = new Order($order['id_order']);
                $order_cart = new Cart($order_obj->id_cart);
                $loyalty = new LoyaltyModule(LoyaltyModule::getByOrderId($order_obj->id));


                $order_obj->delete();
                $order_cart->delete();
                $loyalty->delete();
            }

            return true;
        }



        require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        foreach ($orders as $order) {
            if (in_array($order['carvari_order_id'], $carvari_array)) {
                $order_obj = new Order($order['id_order']);
                $order_cart = new Cart($order_obj->id_cart);
                $loyalty = new LoyaltyModule(LoyaltyModule::getByOrderId($order_obj->id));


                $order_obj->delete();
                $order_cart->delete();
                $loyalty->delete();
            } else {
                $carvari_array[] = $order['carvari_order_id'];
            }
        }
    }

    public function clearOrdersFromXML(){
        $orders = array();

        $file_name = 'sverka_1910.xlsx';
        $file_dir = __DIR__ . '/tmp/';

        $filename = $file_dir.$file_name;

        if (!file_exists($filename))
            return true;

        require_once(__DIR__.'/lib/simplexlsx/simplexlsx.class.php');
        $Reader = new SimpleXLSX($filename);

        $current_line = 0;
        foreach ($Reader->rows() as $line) {
            $current_line++;

            if ($current_line<2) continue;
            $line = array_map('trim', $line);

            if ($customer = Customer::getByCarvariCardNumber($line[0])){
                $customer_orders = DB::getInstance()->executeS("SELECT * FROM ps_orders WHERE `carvari_order_id`>0 AND `id_customer`=".$customer->id);

                $orders =  array_merge($orders, $customer_orders);

            }

        }

        require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        foreach ($orders as $order) {
                $order_obj = new Order($order['id_order']);
                $order_cart = new Cart($order_obj->id_cart);
                $loyalty = new LoyaltyModule(LoyaltyModule::getByOrderId($order_obj->id));


                $order_obj->delete();
                $order_cart->delete();
                $loyalty->delete();

        }

    }

    public function clearOrdersByCarvariCard($carvari_card)
    {
        require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        require_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');
        // usage: &clearOrdersByCarvariCard=carvari_card
        if ($customer = Customer::getByCarvariCardNumber($carvari_card)){
            $orders = DB::getInstance()->executeS("SELECT * FROM ps_orders WHERE `carvari_order_id`>0 AND `id_customer`=".$customer->id);
            foreach ($orders as $order) {
                $order_obj = new Order($order['id_order']);
                $order_cart = new Cart($order_obj->id_cart);
                $loyalty = new LoyaltyModule(LoyaltyModule::getByOrderId($order_obj->id));


                $order_obj->delete();
                $order_cart->delete();
                $loyalty->delete();

            }

        }
    }

    public function compareFromGMS()
    {
//              Запуск из УРЛ :
//              &compareFromGMS


        require_once(__DIR__.'/lib/simplexlsx/simplexlsx.class.php');

        $file_name = 'presta_0.xlsx';
        $file_dir = __DIR__ . '/tmp/';

        $filename = $file_dir.$file_name;


        $Reader = new SimpleXLSX($filename);

        $log_mistmach = '';
        $log_orders = '';
        $current_line = 0;
        foreach ($Reader->rows() as $line) {
            $current_line++;

            if ($current_line<2) continue;

            $line = array_map('trim', $line);
            $line = array_map('str_replace', array_fill(0, count($line), '*'), array_fill(0, count($line), ''), $line);


            if ( Customer::isCustomerExistsByPhone($line[1]) ){
                $customer = new Customer();
                if (Customer::isCustomerExistsByPhone($line[1]))
                    $customer->getByPhone2($line['1']);

                $carvari_card = Customer::getCarvariCardNumberById($customer->id);

                if ($line[0] != $carvari_card) {
                    $log_mistmach .= 'Mistmach: '.$line[0].','.$line[1].','.$line[2].','.$line[3].','.$carvari_card.','.$customer->firstname.','.$customer->lastname."\n";

                    $orders = Order::getCustomerOrders($customer->id);
                    foreach ($orders as $order){
                        $log_orders .= $carvari_card.','.$order['carvari_order_id'].','.$order['total_products'].','.$order['date_add'].','.$customer->phone_login.','.$customer->firstname.','.$customer->lastname."\n";
                    }
                }


            } else {
                if (Customer::isExistByCarvariCardNumber($line[0])) {
                    $log_mistmach .= 'No phone in DB, carvari card exist :'.$line['1'].' - '.$line['0']."\n";
                    $customer = Customer::getByCarvariCardNumber($line[0]);
                    if ($customer){
                        $orders = Order::getCustomerOrders($customer->id);
                        foreach ($orders as $order){
                            $log_orders .= $line[0].','.$order['carvari_order_id'].','.$order['total_products'].','.$order['date_add'].','.$customer->phone_login.','.$customer->firstname.','.$customer->lastname."\n";
                        }

                        $customer->setPhoneLogin($line[1]);
                        $customer->save();
                    }
                    
                }
                else {

                    $line[2] = preg_replace('/[^a-zA-Zа-яА-Я]/ui', '',mb_substr($line[2], 0, 32) );
                    $line[3] = preg_replace('/[^a-zA-Zа-яА-Я]/ui', '',mb_substr($line[3], 0, 32) );

                    $customer = new Customer();
                    $customer->setWsCarvariCard((int)$line[0]);
                    $customer->setWsOfflineOnly(1);
                    $customer->email = $line[0] . '@carvari.com';
                    $customer->setPhoneLogin($line[1]);
                    $customer->setWsPasswd(Customer::generatePin());
                    $customer->firstname = $line[2] ? $line[2] :'Ім\'я';
                    $customer->lastname = $line[3] ? $line[3] : 'Фамілія';
                    $customer->add();

                    if (!($id_address = Address::getFirstCustomerAddressId($customer->id))) {
                        $address = new Address();
                        $address->id_customer = $customer->id;
                        $address->id_country = (int)Configuration::get('PS_COUNTRY_DEFAULT');
                        $address->alias = 'Моя Адреса';
                        $address->lastname = $line[2] ? $line[2] :'Ім\'я';
                        $address->firstname = $line[3] ? $line[3] : 'Фамілія';
                        $address->address1 = 'Адреса';
                        $address->phone_mobile = $line[1];
                        $address->city = 'Місто';
                        $address->add();
                    }

                    $log_mistmach .= 'No phone and carvari_card in DB :'.$line['1'].' - '.$line['0']."\n";
                }
            }

        }

        $handle = fopen(__DIR__.'/log/log_mistmach.txt', 'w');
        fwrite($handle, $log_mistmach);
        fclose($handle);

        $handle = fopen(__DIR__.'/log/log_orders.txt', 'w');
        fwrite($handle, $log_orders);
        fclose($handle);
        d($log_orders);


    }

    public function cardnumsFromGMS()
    {
//              Запуск из УРЛ :
//              &cardnumsFromGMS

        require_once(__DIR__.'/lib/simplexlsx/simplexlsx.class.php');

        $file_name = 'presta_0.xlsx';
        $file_dir = __DIR__ . '/tmp/';

        $filename = $file_dir.$file_name;

        $Reader = new SimpleXLSX($filename);

        $log_mistmach = '';
        $log_orders = '';
        $current_line = 0;
        foreach ($Reader->rows() as $line) {
            $current_line++;

            if ($current_line<2) continue;

            $line = array_map('trim', $line);
            $line = array_map('str_replace', array_fill(0, count($line), '*'), array_fill(0, count($line), ''), $line);


            if (Customer::isCustomerExistsByPhone($line[1])){
                $customer = new Customer();
                if (Customer::isCustomerExistsByPhone($line[1]))
                    $customer->getByPhone2($line['1']);

                $carvari_card = Customer::getCarvariCardNumberById($customer->id);

                if ($line[0] != $carvari_card) {
                    $res = $customer->changeCarvariCard($line[0], $customer->id);
                    $log_changed .= 'change, '.$carvari_card.', to, '.$line[0].','.$line[1].','.$line[2].','.$line[3].','.$res."\n";
                }


            } else {
                $log_changed .= 'no found phone:'.$line[1].', '.$line[0]."\n";
            }

        }

        $handle = fopen(__DIR__.'/log/log_changed.txt', 'w');
        fwrite($handle, $log_changed);
        fclose($handle);
        d($log_changed);


    }
    
    public function makeExportXLS()
    {
        require_once(__DIR__.'/lib/xlsxwriter.class.php');

        // clean buffer
        if (ob_get_level() && ob_get_length() > 0)
            ob_clean();

        $result = Db::getInstance()->executeS('
        SELECT cus.`id_customer`, cus.`email`, c.`number` as carvari_card, cus.`phone_login`, CONCAT_WS(\' \', cus.`lastname`, cus.`firstname`) as fio, t3.`points` as bonus_points FROM (SELECT  t.id_customer, IFNULL(IFNULL(t.points,0)+IFNULL(t2.points,0), 0) points FROM (SELECT id_customer, SUM(points) points FROM `'._DB_PREFIX_.'loyalty` l WHERE l.`id_loyalty_state` IN(2,5)'.$sql_period.' GROUP BY `id_customer`) t
        LEFT JOIN (SELECT id_customer, SUM(points) points FROM (SELECT id_customer, points FROM `'._DB_PREFIX_.'loyalty` l WHERE l.`id_loyalty_state`=3 AND l.`points`<0'.$sql_period.') t GROUP BY `id_customer`) t2
        ON t.id_customer=t2.id_customer) t3
        LEFT JOIN `'._DB_PREFIX_.'ffusercards` c ON c.`id_customer`=t3.id_customer
        LEFT JOIN `'._DB_PREFIX_.'customer` cus ON cus.`id_customer`=t3.id_customer
        WHERE cus.`id_customer` IS NOT NULL');


        $content = array();

        $feed_title = array(
            'FIO' => 'string',
            'Phone' => 'string',
            'Email' => 'string',
            'Bonuses' => 'integer',
        );

        foreach ( $result as $i => $customer ) {
            $feed_line = array();
            $feed_line[] = $customer['fio'];
            $feed_line[] = $customer['phone_login'];
            $feed_line[] = $customer['email'];
            $feed_line[] = $customer['bonus_points'];
            $content[] = $feed_line;
        }


        $filename = "carvari_bonuses.xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');


        $writer = new XLSXWriter();
        $writer->setAuthor('ForForce');
        $writer->writeSheet($content,'Sheet1',$feed_title);
        $writer->writeToStdOut();

        exit(0);
    }
}