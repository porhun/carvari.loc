<?php
class FfcarvaricardCarvaricardModuleFrontController extends ModuleFrontController
{

	public function __construct()
	{
		parent::__construct();
		$this->img_dir = 'carvari';
		$this->imageType = 'jpg';
	}

	public function setMobileMedia()
	{
		parent::setMobileMedia();

		$this->addJqueryPlugin('slickslider');
	}

	public function initContent()
	{
		parent::initContent();

		if ( !$this->context->customer->isLogged() ){
			Tools::redirect('/authentication');
		}

		$addresses = $this->context->customer->getAddresses($this->context->language->id);
		$card = Db::getInstance()->getRow('SELECT `number`, `html` FROM `'._DB_PREFIX_.'ffusercards` WHERE id_customer='.(int)$this->context->cart->id_customer);
		$articles = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'ffusercards_html` WHERE `active`=1 AND id_group='.(int)$this->context->customer->id_default_group.' ORDER BY date_add desc LIMIT 10');

		foreach ( $articles as &$article) {
			$article['img_url'] = _PS_BASE_URL_.'/img/'.$this->img_dir.'/'.$article['id_html'].'.'.$this->imageType;
		}

		$this->context->smarty->assign(array(
			'card' => $card,
			'articles' => $articles,
			'customer' => $this->context->customer,
			'customer_address' => $addresses[0],
		));

		$this->context->smarty->assign('HOOK_CUSTOMER_ACCOUNT', Hook::exec('displayCustomerAccount'));

		$this->setTemplate('carvaricard.tpl');
	}

}
