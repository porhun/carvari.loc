<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 26.08.16
 * Time: 12:05
 */

/**
 * Rebuilder classes should be specified in $rebuilders and included in module file
 */
class ProductCategoryRebuilderManager
{
    private static $rebuilders = [
        CategoryRebuilderType::DISCOUNT => 'DiscountRebuilder',
        CategoryRebuilderType::LATEST   => 'LatestRebuilder'
    ];

    /**
     * @var string $type    Type used for rebuilding
     */
    private $type;

    private function __construct($type)
    {
        if (!array_key_exists(static::getAvailableTypes(),(string)$type)) {
            throw new Exception('There is no such category type');
        }
        $this->type = $type;
//        AND product_shop.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'"
    }

    public function create($type)
    {
        return new self($type);
    }

    /**
     * Add resolved category to products
     */
    public function rebuild()
    {
        $this->resolveRebuilder();
    }

    public static function getAvailableTypes()
    {
        return self::$types;
    }

    /**
     * Resolve what category should be added/deleted
     *
     * @throws Exception
     */
    private function resolveRebuilder()
    {
        $method_add = 'addCategory'.ucfirst($this->getTypeMethod());
        $method_clean = 'cleanCategory'.ucfirst($this->getTypeMethod());
        if (!method_exists($this, $method_add) || !method_exists($this, $method_clean)) {
            throw new Exception('There is no such method to perform product category rebuilding');
        }
        $this->$method_clean();
        $this->$method_add();
    }

    private function addCategoryDiscount()
    {

    }

    private function cleanCategoryDiscount()
    {

    }

    private function addCategoryLatest()
    {
        $products_ids = Product::getNewProductsId();
        $category_id = $this->getCategoryTypeId();
        foreach ($categories as $new_id_categ)
            if (!in_array($new_id_categ, $current_categories))
                $product_cats[] = array(
                    'id_category' => (int)$new_id_categ,
                    'id_product' => (int)$this->id,
                    'position' => (int)$new_categ_pos[$new_id_categ],
                );
    }

    private function cleanCategoryLatest()
    {

    }

    /**
     * Return category ID for current type
     *
     * @return int
     */
    private function getCategoryTypeId()
    {
        $category_id = (int)ConfigurationCore::get($this->type);
        $this->checkCategoryExisting($category_id);
        return $category_id;
    }

    /**
     * Check whether given category exists or not
     *
     * @param int $category_id  Category ID to check
     * @throws Exception
     */
    private function checkCategoryExisting($category_id)
    {
        if (!Category::categoryExists((int)$category_id)) {
            throw new Exception('There is no existed category for type '.$this->getTypeMethod());
        }
    }

    /**
     * Get specified method for current type
     *
     * @return string
     */
    private function getTypeMethod()
    {
        $types = self::getAvailableTypes();
        return (string)$types[$this->type];
    }

}