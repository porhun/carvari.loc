<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 26.08.16
 * Time: 10:14
 */
class ProductCategoryRebuilder
{
    /**
     * @var array   $types  Configuration name of category type and its method for resolving
     */
    private static $types = [
        CategoryRebuilderType::DISCOUNT => 'Discount',
        CategoryRebuilderType::LATEST   => 'Latest'
    ];

    /**
     * @var string $type    Type used for rebuilding
     */
    private $type;

    private function __construct($type)
    {
        if (!array_key_exists((string)$type, static::getAvailableTypes())) {
            throw new Exception('There is no such category type');
        }

        $this->type = $type;
    }

    /**
     * Rebuild all the existed categories
     *
     * @return bool
     */
    public static function rebuildAll()
    {
        $types = array_keys(self::getAvailableTypes());
        return array_walk($types, function ($type) {
            self::create($type)->rebuild();
        });
    }

    public static function create($type)
    {
        return new self($type);
    }

    /**
     * Add resolved category to products
     */
    public function rebuild()
    {
        $this->resolveRebuilder();
    }

    /**
     * Retrieve existed category types
     *
     * @return array
     */
    public static function getAvailableTypes()
    {
        return self::$types;
    }

    /**
     * Resolve what category should be added/deleted
     *
     * @throws Exception
     */
    private function resolveRebuilder()
    {
        $method_add = 'addCategory'.ucfirst($this->getTypeMethod());
        $method_clean = 'cleanCategory'.ucfirst($this->getTypeMethod());
        if (!method_exists($this, $method_add) || !method_exists($this, $method_clean)) {
            throw new Exception('There is no such method to perform product category rebuilding');
        }
        $this->$method_clean();
        $this->$method_add();
    }

    private function addCategoryDiscount()
    {
        $this->addProductCategories(SpecificPrice::getAvailableProductsIds());
        return $this;
    }

    private function addCategoryLatest()
    {
        $products_ids = array_column(Product::getNewProductsId(), 'id_product');
        $this->addProductCategories($products_ids);
        return $this;
    }

    /**
     * @see Product::addProductsToCategory()
     * @return $this
     */
    private function addProductCategories(array $product_ids)
    {
        $category_id = $this->getCategoryTypeId();
        Product::addProductsToCategory($category_id, $product_ids);
        return $this;
    }

    private function cleanCategoryDiscount()
    {
        $category_id = $this->getCategoryTypeId();
        $this->cleanCategories($category_id);
        return $this;
    }

    private function cleanCategoryLatest()
    {
        $category_id = $this->getCategoryTypeId();
        $this->cleanCategories($category_id);
        return $this;
    }

    /**
     * Remove rows with given category ID
     *
     * @param int $category_id  Category ID
     * @return $this
     */
    private function cleanCategories($category_id)
    {
        Db::getInstance()->delete('category_product', 'id_category = '. (int)$category_id);
        return $this;
    }

    /**
     * Return category ID for current type
     *
     * @return int
     */
    private function getCategoryTypeId()
    {
        $category_id = (int)ConfigurationCore::get($this->type);
        $this->checkCategoryExisting($category_id);
        return $category_id;
    }

    /**
     * Check whether given category exists or not
     *
     * @param int $category_id  Category ID to check
     * @throws Exception
     */
    private function checkCategoryExisting($category_id)
    {
        if (!Category::categoryExists((int)$category_id)) {
            throw new Exception('There is no existed category for type '.$this->getTypeMethod());
        }
    }

    /**
     * Get specified method for current type
     *
     * @return string
     */
    private function getTypeMethod()
    {
        $types = self::getAvailableTypes();
        return (string)$types[$this->type];
    }

}