<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 26.08.16
 * Time: 10:38
 */
class CategoryRebuilderType
{
    const DISCOUNT = 'EXPAND_CATEGORY_DISCOUNT_TYPE';
    const LATEST   = 'EXPAND_CATEGORY_LATEST_TYPE';
}