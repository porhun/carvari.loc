<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 26.08.16
 * Time: 9:53
 */
class expand_categorycategory_builderModuleFrontController extends ModuleFrontControllerCore
{

    public function checkAccess()
    {
        if (!$secure_key = Tools::getValue('secure_key', false)) {
            return false;
        }
    }

    public function init()
    {
        Hook::exec('actionRebuildProductCategories', ['secure_key' => Tools::getValue('secure_key', false)], ModuleCore::getModuleIdByName('expand_category'));
        parent::init();
        die('Ok');
    }
}