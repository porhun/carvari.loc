<?php
if (!defined('_PS_VERSION_'))
    exit;

require_once dirname(__FILE__).'/models/ProductCategoryRebuilder.php';
require_once dirname(__FILE__).'/models/CategoryRebuilderType.php';

class expand_category extends Module
{
    private $secure_key = 'f8b833b23a7Ee7a911e9215f42c119Db';

    public function __construct()
    {
        $this->name = 'expand_category';
        $this->tab = 'front_office_features';
        $this->version = '0.1';
        $this->author = 'shandur';
        $this->need_instance = 0;

        parent::__construct();
        $this->bootstrap = true;

        $this->displayName = $this->l('Expand Categories');
        $this->description = $this->l('Automatically add categories when products is for sale or new');
        $this->confirmUninstall = $this->l('Are you sure you want to delete this module ?');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        if (!parent::install() || !$this->registerHook('actionRebuildProductCategories') || !$this->registerHook('moduleRoutes'))
            return false;
        Configuration::updateValue(CategoryRebuilderType::DISCOUNT, '');
        Configuration::updateValue(CategoryRebuilderType::LATEST, '');
        if (file_exists(_PS_ROOT_DIR_.'/cache/class_index.php'))
        {
            unlink(_PS_ROOT_DIR_.'/cache/class_index.php');
        }
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        Configuration::deleteByName(CategoryRebuilderType::DISCOUNT);
        Configuration::deleteByName(CategoryRebuilderType::LATEST);
        return true;
    }

    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submit'.$this->name))
        {
            Configuration::updateValue(CategoryRebuilderType::DISCOUNT, (int)Tools::getValue(CategoryRebuilderType::DISCOUNT));
            Configuration::updateValue(CategoryRebuilderType::LATEST,   (int)Tools::getValue(CategoryRebuilderType::LATEST));
            $output .= $this->displayConfirmation($this->l('Settings was successfully saved'));
        }
        if (Tools::isSubmit('submitRebuildCategories')) {
            $this->rebuildProductCategories();
            $output .= $this->displayConfirmation($this->l('Categories was successfully rebuilt'));
        }
        return $output.$this->renderForm();
    }

    public function hookModuleRoutes()
    {
        return [
            'module-expand_category-rebuild_category' => [
                'controller' => 'category_builder',
                'rule' => '/module/expand_category/category_builder/',
                'keywords' => [],
                'params' => [
                    'fc' => 'module',
                    'module' => 'expand_category',
                ],
            ]
        ];
    }

    public function renderForm()
    {
        $categories = $this->getCategories();
        $fields_form = [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ],
                'input' => [
                    [
                        'type' => 'select',
                        'label' => $this->l('`Discount` category: '),
                        'name' => CategoryRebuilderType::DISCOUNT,
                        'desc' => $this->l('Category with discount/sale products'),
                        'options' => [
                            'id' => 'id_category',
                            'name' => 'name',
                            'query' => $categories
                        ]
                    ],
                    [
                        'type' => 'select',
                        'label' => $this->l('`Latest` category: '),
                        'name' => CategoryRebuilderType::LATEST,
                        'desc' => $this->l('Category with the latest(new) products'),
                        'options' => [
                            'id' => 'id_category',
                            'name' => 'name',
                            'query' => $categories
                        ]
                    ],
                    [
                        'type' => 'free',
                        'name' => 'rebuild_link',
                    ]
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                ]
            ],
        ];
        $fields_form_btn = [
            'form' => [
                'submit' => [
                    'title' => $this->l('Rebuild categories'),
                    'name' => 'submitRebuildCategories'
                ]
            ]
        ];
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submit'.$this->name;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => [
                CategoryRebuilderType::DISCOUNT => Configuration::get(CategoryRebuilderType::DISCOUNT),
                CategoryRebuilderType::LATEST   => Configuration::get(CategoryRebuilderType::LATEST),
                'rebuild_link' => $this->getRebuildLink()
            ],
        );

        return $helper->generateForm([$fields_form, $fields_form_btn]);
    }

    public function hookActionRebuildProductCategories($params)
    {
        if (!isset($params['secure_key']) || !$secure_key = (string)$params['secure_key']) {
            return false;
        }
        $mod_secure_key = $this->getSecureKey();
        if (strlen($secure_key) !== strlen($mod_secure_key) || $secure_key !== $mod_secure_key) {
            return false;
        }
        $this->rebuildProductCategories();
    }

    /**
     * Get link to rebuild the categories
     *
     * @return string
     */
    private function getRebuildLink()
    {
        return "<a href=/module/expand_category/category_builder?secure_key={$this->getSecureKey()}>".$this->l('Rebuild categories')."</a>";
    }

    private function getSecureKey()
    {
        return $this->secure_key;
    }

    /**
     * Retrieve the main product categories
     *
     * @return array
     */
    private function getCategories()
    {
        $categories = CategoryCore::getCategories($this->context->language->id, true, true, 'and id_parent = 2');
        return array_column(call_user_func_array('array_merge', $categories), 'infos');
    }


    /**
     * Run the rebuilding product categories
     */
    private function rebuildProductCategories()
    {
        ProductCategoryRebuilder::rebuildAll();
    }
}
