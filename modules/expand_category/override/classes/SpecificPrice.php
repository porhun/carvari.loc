<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 25.01.17
 * Time: 12:58
 */
class SpecificPrice extends SpecificPriceCore
{
    /**
     * Retrieve discount products ID
     *
     * @return array
     */
    public static function getAvailableProductsIds()
    {
        $context = Context::getContext();
        return SpecificPrice::getProductIdByDate(
            $context->shop->id,
            $context->currency->id,
            $context->country->id,
            $context->customer->id_default_group,
            $begin = date('Y-m-d H:i:s', mktime(0,0,0,1,1,2010)),
            $current_date = date('Y-m-d H:i:s'),
            0,
            false
        );
    }
}