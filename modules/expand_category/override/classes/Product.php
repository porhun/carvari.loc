<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 25.01.17
 * Time: 13:03
 */
class Product extends ProductCore
{
    public static function addProductsToCategory($category_id, array $products_ids)
    {
        $position = 0;
        $product_cats = array_reduce($products_ids, function ($total, $prod_id) use ($category_id, &$position) {
            $position++;
            $total[] =  [
                'id_category' => (int)$category_id,
                'id_product' => (int)$prod_id,
                'position' => (int)$position,
            ];
            return $total;
        }, []);
        return Db::getInstance()->insert('category_product', $product_cats);
    }
    public static function getNewProductsId()
    {
        $sql = new DbQuery();
        $sql->select('p.`id_product`');
        $sql->from('product', 'p');
        $sql->where('p.`date_add` > "'.date('Y-m-d', strtotime('-'.(Configuration::get('PS_NB_DAYS_NEW_PRODUCT') ? (int)Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY')).'"');
        $sql->where('p.`active` = 1');
        $sql->groupBy('p.`id_product`');
        return Db::getInstance()->executeS($sql);
    }
}