<?php
set_time_limit(0);

if (!defined('_PS_VERSION_'))
    exit;

class FfFacebookRemarketing extends Module
{
    const FF_FACEBOOK_REMARKET_EXPORT_FILE_LINK = 'ff_facebook_remarket_csv_file';
    const FF_FACEBOOK_REMARKET_EXPORT_FILE_PATH = 'ff_facebook_remarket_csv_file_path';

    protected $export_file_skeleton = ['id_product' => 'ID',
                                       'title' => 'Title',
                                       'description' => 'Description',
                                       'product_link' => 'Link',
                                       'image_url' => 'Image_link',
                                       'availability' => 'Availability',
                                       'condition' => 'Condition',
                                       'price' => 'Price',
                                       'mpn' => 'Mpn',
                                       'brand' => 'Brand ',
                                       'google_product_category' => 'Google_product_category',
    ];
    protected $product_catalog_file = 'facebook_remarket_catalog.csv';

    private $secure_key = 'f8bP33b23a7Fe7a9g5e92G5f42c019Db';

    public function __construct()
    {
        $this->name = 'fffacebookremarketing';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Facebook Dynamic Remarketing');
        $this->description = $this->l('Dynamic Remarketing for site pages');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('actionGenerateExportCatalogProducts') ||
            !$this->registerHook('moduleRoutes'))
            return false;

        Configuration::updateValue(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_LINK, '');
        Configuration::updateValue(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_PATH, '');
        $this->cleanClassCache();

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;

        Configuration::deleteByName(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_LINK);
        Configuration::deleteByName(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_PATH);
        $this->deleteOldExportFile();
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache)) {
            unlink($class_cache);
        }
    }

    public function exportProducts()
    {
        $new_file_path = _PS_UPLOAD_DIR_.$this->product_catalog_file;
        $this->deleteOldExportFile();
        $file_csv = fopen($new_file_path, 'w');
        if (!$file_csv) {
            $this->adminDisplayWarning($this->l('Cannot create new product catalog file'));
            return;
        }
        $this->exportProductsToFile($file_csv);
        fclose($file_csv);

        Configuration::updateValue(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_PATH, $new_file_path);
        $this->updateExportFileDownloadLink();
        $this->adminDisplayInformation($this->l('Product catalog export file was created'));
    }

    protected function exportProductsToFile($file_handle)
    {
        $cat_aso = Category::getChildren(3022,$this->context->cookie->id_lang);
        $id_category = [];
        foreach ($cat_aso as $item){
            $id_category[] =  $item['id_category'];
        }
        $link = $this->context->link;
        $currency_iso_code = $this->context->currency->iso_code;
        $products = Product::getProductForFacebookMarketExport();
        $skeleton = $this->export_file_skeleton;
        $this->addFileExportHeader($file_handle);
        foreach ($products as &$product_info) {
            $p = new Product($product_info['id_product']);
            $price = $p->getPrice(false);
            $manufacturer = new Manufacturer($p->id_manufacturer, (int)Context::getContext()->language->id);
            $product_info['title'] = (is_array($p->name)) ? array_pop($p->name).' '.$p->reference : $p->name.' '.$p->reference;
//            $product_info['description'] = (is_array($p->description)) ? array_pop($p->description) : $p->description;
            $product_info['description'] = 'Взуття L CARVARI - сміливе поєднання модних акцентів з модельним, класичним і спортивним напрямами';
            $product_info['product_link'] = $link->getProductLink($p);
            $product_info['image_url'] = $link->getImageLink((is_array($p->link_rewrite)) ? array_pop($p->link_rewrite) : $p->link_rewrite, (int)$product_info['id_image'], 'large_default');
            $product_info['availability'] = ($p->available_for_order) ? 'in stock' : 'out of stock';
            $product_info['condition'] = $p->condition;
            $product_info['price'] = ($price) ? sprintf('%01.2f %s', $price, $currency_iso_code) : '';
            $product_info['mpn'] = (is_array($p->ean13)) ? array_pop($p->ean13) : $p->ean13;
            $product_info['brand'] = $manufacturer->name;
            if(in_array($p->id_category_default, $id_category)) {
                $product_info['google_product_category'] = 'Apparel & Accessories > Clothing [Предметы одежды и принадлежности > Одежда]';
            } else  {
                $product_info['google_product_category'] = 'Apparel & Accessories > Shoes [Предметы одежды и принадлежности > Обувь]';
            }

            // clean extra fields comparing with skeleton
            $product_info = array_intersect_key($product_info, $skeleton);

            // merge by keys in the right way (using skeleton)
            $product_info = array_merge($skeleton, $product_info);
            fputcsv($file_handle, $product_info);
            unset($p);
        }
    }

    protected function addFileExportHeader($file_handler)
    {
        if (!$file_handler) {
            return;
        }
        // enable UTF-8 for CSV file
            fprintf($file_handler, chr(0xEF).chr(0xBB).chr(0xBF));
            fputcsv($file_handler, $this->export_file_skeleton);
    }

    protected function deleteOldExportFile()
    {
        $file_path = $this->getExportFilePath();
        if ($file_path && file_exists($file_path)) {
            unlink($file_path);
        }
    }

    protected function getExportFilePath()
    {
        return Configuration::get(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_PATH, '');
    }

    protected function getExportFileName()
    {
        if (!$file_path = $this->getExportFilePath()) {
            return '';
        }
        return pathinfo($file_path, PATHINFO_BASENAME);
    }

    protected function updateExportFileDownloadLink()
    {

        $file_link = (new ShopUrl(1))->getURL().'upload/'.$this->product_catalog_file;
        Configuration::updateValue(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_LINK, $file_link);
    }

    protected function getExportFileDownloadLink()
    {
        $link = Configuration::get(self::FF_FACEBOOK_REMARKET_EXPORT_FILE_LINK, '');
        return $link;
    }

    public function getContent()
    {
        $html = $this->displayConfigForm();
        if (Tools::getIsset('exportProducts')) {
            $this->exportProducts();
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }

        $cron_html = '<div class="alert alert-info">Ссылка для генерации по крону: '._PS_BASE_URL_.'/modules/'.$this->name.'/ajax.php?secret=supersecret&action=exportProducts</div>';

        return $html.$cron_html;
    }

    public function displayConfigForm()
    {
        $this->fields_options = array(
            'general' => array(
                'title' => $this->l('Настройка модуля'),
                'fields' =>
                    array(
                        self::FF_FACEBOOK_REMARKET_EXPORT_FILE_LINK => [
                            'title' => $this->l('Export file download link'),
                            'type' => 'text',
                            'auto_value' => false,
                            'value' => $this->getExportFileDownloadLink()
                        ],
                    ),
                'buttons' => [
                    [
                        'title' => $this->l('Export products in file'),
                        'class' => 'btn btn-info',
                        'type' => 'submit',
                        'name' => 'exportProducts'
                    ]
                ]
            )
        );

        $helper = new HelperOptions();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ),
        );
        return $helper->generateOptions($this->fields_options);
    }

    public function hookActionGenerateExportCatalogProducts($params)
    {
        if (!isset($params['secure_key']) || !$secure_key = (string)$params['secure_key']) {
            return false;
        }
        $mod_secure_key = $this->getSecureKey();
        if (strlen($secure_key) !== strlen($mod_secure_key) || $secure_key !== $mod_secure_key) {
            return false;
        }
        if (isset($params['download_file']) && $params['download_file']) {
            $this->sendExportFile();
        }
        $this->exportProducts();
    }

    protected function sendExportFile()
    {
        header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
        header("Cache-Control: public");
        header('Content-type:text/csv');
        header('Content-Disposition: attachment; filename='.$this->getExportFileName());
        readfile($this->getExportFilePath());
        return true;
    }

    protected function getSecureKey()
    {
        return $this->secure_key;
    }

    public function hookModuleRoutes()
    {
        return [
            "module-{$this->name}-export_products" => [
                'controller' => 'export_products',
                'rule' => "/modules/{$this->name}/export_products/",
                'keywords' => [],
                'params' => [
                    'fc' => 'module',
                    'module' => $this->name,
                ],
            ]
        ];
    }
}