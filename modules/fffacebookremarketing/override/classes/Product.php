<?php

/**
 * Methods from this class
 */
class Product extends ProductCore
{
    public static function getProductForFacebookMarketExport()
    {
        $id_lang = (int)Context::getContext()->language->id;
        $qb = new DbQuery();
        $qb->select('p.id_product, i.id_image');
        $qb->from('product', 'p');
        $qb->leftJoin('image', 'i', 'i.id_product = p.id_product AND i.cover = 1');
        $qb->leftJoin('image_lang', 'il', 'i.id_image = il.id_image AND i.cover = 1 AND il.id_lang = '.$id_lang);
        $qb->where('p.active = 1');
        $qb->where('p.visibility IN ("both", "catalog")');
        return Db::getInstance()->executeS($qb);
    }
}