<?php

require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');

include_once(dirname(__FILE__).'/ffproductremarketing.php');

$module_instance = new FfProductRemarketing();

if (Tools::getValue('method') == 'displayAjax') {
    $data['data'] = Tools::getValue('data');

//    if (!$id_product) {
//        die(Tools::jsonEncode(['status' => 'error']));
//    }

    die(Tools::jsonEncode(['status' => 'ok', 'product_ids' => $module_instance->hookActionCatalogProductIds($data)]));
}