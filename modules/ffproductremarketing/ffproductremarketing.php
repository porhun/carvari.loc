<?php

if (!defined('_PS_VERSION_'))
    exit;

class FfProductRemarketing extends Module
{
    const ADV_ID = '18612';
    const PRICE_ID = '2272';
    const AUDIENCE_ID = '18612_df46fef9-dbe6-427d-8a15-9c63b01fe0f3';
    private $output;

    public function __construct()
    {
        $this->name = 'ffproductremarketing';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'dkostrub';
        $this->secure_key = Tools::encrypt($this->name);
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Product Remarketing');
        $this->description = $this->l('Product Remarketing for site pages');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');
    }

    public function install()
    {
        if (!parent::install() ||
            !$this->registerHook('displayProductRemarketing') ||
            !$this->registerHook('actionCatalogProductIds') ||
            !$this->registerHook('header') ||
            !$this->registerHook('displayFooterRemarketing')
        )
            return false;
        $this->cleanClassCache();
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache))
            unlink($class_cache);
    }

//    public function hookHeader($params)
//    {
//        $this->context->controller->addJS($this->_path . 'views/js/ffproductremarketing.js');
//    }

    public function hookDisplayFooterRemarketing($params)
    {
        return $this->display(__FILE__, 'ffproductremarketingfooter.tpl');
    }

    public function hookActionCatalogProductIds($params)
    {
        $id_product = (int)$params['data'];

        return (int)$id_product;
    }

    public function hookDisplayProductRemarketing($params)
    {
        $page_name = Tools::getValue('controller');

        switch ($page_name) {
            case 'index':
                $this->output .= "rnt('add_event', {advId: '".self::ADV_ID."', pageType:'home'});\r\n";
                $this->output .= "rnt('add_audience', {audienceId: '".self::AUDIENCE_ID."'});";
                break;

//ToDo Необходимо доделать получение ids товаров в зависимости от текущей страницы каталога

//            case 'category':
////                $productIds = $this->hookActionCatalogProductIds($params);
//
//                $controller = new FrontController();
//                $controller->pagination();
//
//                $order_by = $this->context->controller->orderBy;
//                $order_way = $this->context->controller->orderWay;
//                $nb = (int)(Configuration::get('PS_PRODUCTS_PER_PAGE'));
//                $categoryId = Tools::getValue('id_category');
//                $category = new Category($categoryId);
//
//                $currentPage = Tools::getValue('p');
//                $startFrom =  ($currentPage <= 1) ? 1 : ($currentPage - 1) * $nb;
//                $products = $category->getProducts($this->context->language->id, $startFrom , $nb, $order_by, $order_way);
//
//                $productIds = $this->getProductsIds($products);
//
//
//                $this->output .= "rnt('add_category_event', {advId: '".self::ADV_ID."', priceId: '".self::PRICE_ID."', categoryId: '".$categoryId."', productIds: '".$productIds."'});\r\n";
//                $this->output .= "rnt('add_audience', {audienceId: '".self::AUDIENCE_ID."'});";
//                break;

            case 'product':
                $productId = Tools::getValue('id_product');
                $this->output .= "rnt('add_product_event', {advId: '".self::ADV_ID."', priceId: '".self::PRICE_ID."', productId: '".$productId."'});\r\n";
                $this->output .= "rnt('add_audience', {audienceId: '".self::AUDIENCE_ID."', priceId: '".self::PRICE_ID."', productId: '".$productId."'});";
                break;

            case 'orderopc':
                $cart_products = Context::getContext()->cart->getProducts();
                $productIds = $this->getProductsIds($cart_products);
                $this->output .= "rnt('add_shopping_cart_event', {advId: '".self::ADV_ID."', priceId: '".self::PRICE_ID."', productIds: '".$productIds."'});";
                break;

            case 'confirm':
                $cart_products = Context::getContext()->cart->getProducts();
                $productIds = $this->getProductsIds($cart_products);
                $this->output .= " rnt('add_order_event', {advId: '".self::ADV_ID."', priceId: '".self::PRICE_ID."', productIds: '".$productIds."'});";
                break;

            default:
                $this->output .= "rnt('add_audience', {audienceId: '".self::AUDIENCE_ID."'});";
                break;
        }

        $this->context->smarty->assign('is_goods_remark', $this->output);
        return $this->display(__FILE__, 'ffproductremarketingheader.tpl');
    }

    /**
     * @return string
     */
    private function getProductsIds($data)
    {
        $productIds = [];
        foreach ($data as $product){
            $productIds[] = $product['id_product'];
        }
        $productIds = implode(',', $productIds);

        return $productIds;
    }
}