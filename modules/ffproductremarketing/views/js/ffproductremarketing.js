$(document).ready(function(){
    if (page_name = 'category') {
        var data;
        var product_id_array = new Array();
        var click = true;

        $(document).ajaxSuccess(function () {
            $('.product_item').each(function (e) {
                product_id_array.push($(this).data('id_product'));
            });
            data = JSON.stringify(product_id_array);

            click = true;
        });

        $(document).on('click', '.pagination', function (e){
            e.preventDefault();
            if (click) {
                click = false;

                $.ajax({
                    type: 'POST',
                    headers: {"cache-control": "no-cache"},
                    url: baseDir + 'modules/ffproductremarketing/ajax.php',
                    async: true,
                    cache: false,
                    dataType: 'json',
                    data: {method: 'displayAjax', data: 1234, ajax: 1, controller: 'category'},

                    success: function (rsp) {
                        console.log(rsp);
                        if (!rsp || !rsp.status || rsp.status !== 'ok') {
                            console.error('Cant get info');
                            return false;
                        }
                    },
                    error: function () {
                        alert('Oops! Something goes wrong.');
                    }
                });
            }
        });
    }
});

