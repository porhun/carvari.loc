<?php

if (!defined('_PS_VERSION_'))
    exit;

class Ffbranding extends Module
{

    public function __construct()
    {
        $this->name = 'ffbranding';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Branding Module');
        $this->description = $this->l('Adding body backgroun branding img');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');


        $this->img_dir = _PS_IMG_DIR_;
        $this->img_height = 300;
        $this->img_width = 490;
        $this->max_image_size = 4000000;
        $this->imageType = 'jpg';

    }


    public function install()
    {
        if (!parent::install() || !$this->registerHook('displayHeader') || !Configuration::updateValue('BRANDING_MODULE_URL', _PS_BASE_URL_))
            return false;
        return true;
    }

    public function uninstall()
    {

        if (!parent::uninstall())
            return false;

        return true;
    }

    public function hookDisplayHeader()
    {
        if ($this->context->controller->php_self == 'index'){


            $this->context->smarty->assign(array(
                'branding_url' => Configuration::get('BRANDING_MODULE_URL'),
                'branding_img' => '/img/branding.jpg',
                'branding_enabled' => true,
            ));

//            Media::addJsDef(array('branding_url' => Configuration::get('BRANDING_MODULE_URL')));
//            $this->context->controller->addJs(_MODULE_DIR_.$this->name.'/views/js/front.js');
        }

    }

    public function getContent()
    {
        $this->postProcess();

        $this->context->smarty->assign('branding_url', Configuration::get('BRANDING_MODULE_URL'));
        $this->context->smarty->assign('img_url', $this->get_image_url('branding'));

        return $this->context->smarty->fetch(__DIR__.'/views/templates/ffbranding.tpl');
    }

    protected function postProcess()
    {
        if (Tools::isSubmit('submitBranding')) {
            $data = Tools::getValue('branding');
            Configuration::updateValue('BRANDING_MODULE_URL', $data['url']);


            $this->postImage('branding');

            Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        }
    }

    protected function get_image_url($id){
        $image = $this->img_dir.$id.'.'.$this->imageType;
        return ImageManager::thumbnail($image, $id.'.'.$this->imageType, 350,
            $this->imageType, false, true);
    }

    protected function postImage($id)
    {
        $ret = true;

        foreach ($_FILES as $filed_name=>$file) {
            if(!$file['error']){
                // Check image validity

                $max_size = isset($this->max_image_size) ? $this->max_image_size : 0;
                if ($error = ImageManager::validateUpload($_FILES[$filed_name], Tools::getMaxUploadSize($max_size))) {
                    $this->errors[] = $error;
                }


                if (!move_uploaded_file($_FILES[$filed_name]['tmp_name'], $this->img_dir.$id.'.'.$this->imageType)) {
                    return false;
                }


            }
        }
        return $ret;
    }

}