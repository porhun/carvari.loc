<div class="panel">

    <div class="panel-heading">
        Настройки модуля
    </div>

        <div class="panel-body">
            <form action="" method="post" enctype="multipart/form-data">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="url">URL</label>
                        <input class="form-control" type="url" id="url" name="branding[url]" value="{$branding_url}">
                    </div>
                    <div class="form-group">
                        {if $img_url}{$img_url} <br/><br/>{/if}
                        <label for="img">Загрузить картинку</label>
                        <input type="file" id="img" name="img">
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submitBranding" class="btn btn-success" value="Изменить">
                    </div>
                </div>
            </form>

        </div>

</div>
