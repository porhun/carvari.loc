<div class="panel col-lg-12">

    <div class="panel-heading"><i class="icon-truck "></i>&nbsp;{l s='Магазин для доставки заказа - Самовывоз Carvari' mod='ffcarvaricarrier'}</div>
    <div class="panel-body">
        {*<h1>Доставка в магазин: {if $store->id}{$store->city}, {$store->name}, {$store->address1}{else}Магазин отсутсвует либо удален{/if}</h1>*}
        <form action="" method="post">
        <select name="id_store">
            <option>{l s='Магазин доставки не выбран' mod='ffcarvaricarrier'}</option>
            {foreach from=$stores item=store}
                <option value="{$store.id_store}"{if $store.id_store==$id_store} selected{/if}>{$store.city}, {$store.name}, {$store.address1}</option>
            {/foreach}
        </select>
        <br/>
        <input class="btn btn-primary" type="submit" value="{l s='Изменить' mod='ffcarvaricarrier'}">
        <form>
    </div>


</div>