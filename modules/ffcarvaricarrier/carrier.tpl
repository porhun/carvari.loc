<div class="carrier">
{*<h4>Детали доставки Самовывоз Карвари</h4>*}
<input type="hidden" name="carvaricarrier" value="1">
<input type="hidden" name="alias" value="Самовывоз Carvari">
{*<input type="hidden" name="clean_state" value="true">*}
{*<input id="carvaricarrier-city" type="hidden" name="city" value="{$store.city}">*}
{*<input id="carvaricarrier-address1" type="hidden" name="address1" value="{$store.address1}">*}
{*<input type="hidden" name="" value="">*}
{if $mobile_device}
    <div class="input_block">
        <label>{l s='З точки видачі' mod='ffcarvaricarrier'}</label>
        <span id="opc_carvaricarrier_errors" class="error_msg"></span>
        <div class="select_wrap">
            <select name="id_store" required>
                <option value=""></option>
                {foreach from=$stores item=store}
                    <option value="{$store.id_store}" data-city="{$store.city}" data-address1="{$store.address1}">{$store.city}, {$store.name}, {$store.address1}</option>
                {/foreach}
            </select>
        </div>
    </div>

{else}
    <div class="input_block carrier_block">
        <label class="carrier_block__name">{l s='Магазин' mod='ffcarvaricarrier'}</label>
        <select name="id_store" class="carrier_block__select" style="opacity: 0" required>
            <option value=""></option>
            {foreach from=$stores item=store}
                <option value="{$store.id_store}" data-city="{$store.city}" data-address1="{$store.address1}">{$store.city}, {$store.name}, {$store.address1}</option>
            {/foreach}
        </select>
        <span id="opc_carvaricarrier_errors" class="error_msg"></span>
    </div>

    <div class="clearfix"></div>
{/if}
</div>
