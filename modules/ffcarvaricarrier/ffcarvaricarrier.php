<?php
if (!defined('_PS_VERSION_'))
    exit;

class Ffcarvaricarrier extends CarrierModule
{
    public function __construct()
    {
        $this->name = 'ffcarvaricarrier';
        $this->tab = 'shipping_logistics';
        $this->version = '1.0.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Carvari Carrier - доставка самовывоз Carvari');
        $this->description = $this->l('Carvari Carrier - доставка самовывоз Carvari.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    }

    public function install()
    {

        if ( Configuration::get('CARVARI_CARRIER_ID') ) {
            $id_carrier = Configuration::get('CARVARI_CARRIER_ID');
        } else {
            $carrierConfig = array(
                0 => array('name' => 'Самовывоз Карвари',
                    'id_tax_rules_group' => 0, // We do not apply thecarriers tax
                    'active' => true,
                    'is_free' => true,
                    'deleted' => 0,
                    'shipping_handling' => false,
                    'range_behavior' => 0,
                    'delay' => array(
                        'fr' => 'Description Carvari carrier',
                        'en' => 'Description Carvari carrier',
                        Language::getIsoById(Configuration::get
                        ('PS_LANG_DEFAULT')) => 'Описание самовывоз карвари'),
                    'id_zone' => 1, // Area where the carrier operates
                    'is_module' => true, // We specify that it is a module
                    'shipping_external' => true,
                    'external_module_name' => $this->name, // We specify the name of the module
                    'need_range' => true // We specify that we want the calculations for the ranges
                    // that are configured in the back office
                )
            );

            $id_carrier = $this->installExternalCarrier($carrierConfig[0]);
        }

        copy(dirname(__FILE__) . '/img/carvari.jpg', _PS_SHIP_IMG_DIR_ . '/' . (int) $id_carrier . '.jpg');


        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);

        if (!parent::install() ||
            !$this->installSql() ||
            !$this->registerHook('displayAdminOrder') ||
            !$this->registerHook('displayCarrierList') ||
            !$this->registerHook('actionCarrierUpdate') ||
            !Configuration::updateValue('CARVARI_CARRIER_ID', (int)$id_carrier)
            )
            return false;



        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !$this->uninstallSql())
            return false;

//        // Delete External Carrier
//        $carrier = new Carrier((int)(Configuration::get('CARVARI_CARRIER_ID')));
//
//        // If external carrier is default set other one as default
//        if (Configuration::get('PS_CARRIER_DEFAULT') == (int)($carrier->id))
//        {
//            global $cookie;
//            $carriersD = Carrier::getCarriers($cookie->id_lang, true, false, false, NULL, PS_CARRIERS_AND_CARRIER_MODULES_NEED_RANGE);
//            foreach($carriersD as $carrierD)
//                if ($carrierD['active'] AND !$carrierD['deleted'] AND ($carrierD['name'] != $this->_config['name']))
//                    Configuration::updateValue('PS_CARRIER_DEFAULT', $carrierD['id_carrier']);
//        }
//
//        // Then delete Carrier
//        if (!$carrier->delete())
//            return false;

        return true;
    }

    private function installSql(){
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards`');

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffcarvaricarrier` (
			`id_order` INT NOT NULL,
			`id_store` INT NOT NULL,
		PRIMARY KEY (`id_order`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');


        return true;
    }

    private function uninstallSql(){
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards`');
//        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffusercards_html`');

        return true;
    }

    private function fixSql(){
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffcarvaricarrier2`');

        Db::getInstance()->execute('
		CREATE TABLE IF NOT EXISTS  `'._DB_PREFIX_.'ffcarvaricarrier2` (
			`id_order` INT NOT NULL,
			`id_store` INT NOT NULL,
		PRIMARY KEY (`id_order`)
		)  ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;');

        if (Db::getInstance()->execute('INSERT INTO `'._DB_PREFIX_.'ffcarvaricarrier2` (SELECT * FROM (SELECT id_order, id_store FROM `'._DB_PREFIX_.'ffcarvaricarrier` order by id desc) asss GROUP BY id_order)')){
            Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffcarvaricarrier`');
            Db::getInstance()->execute('RENAME TABLE `'._DB_PREFIX_.'ffcarvaricarrier2` TO `'._DB_PREFIX_.'ffcarvaricarrier`');
        }

        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'ffcarvaricarrier2`');

        return true;
    }

    public function getContent()
    {
        if (Tools::getValue('fixSql')) {
            $this->fixSql();
        }
    }
    public function hookDisplayAdminOrder($params)
    {
        $order = new Order($params['id_order']);
        if ( Configuration::get('CARVARI_CARRIER_ID') == $order->id_carrier) {
            if (Tools::getValue('id_store')) {
                Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'ffcarvaricarrier (`id_order`,`id_store`) VALUES('.(int)Tools::getValue('id_order').','.(int)Tools::getValue('id_store').') ON DUPLICATE KEY UPDATE id_store='.(int)Tools::getValue('id_store'));
//                Tools::redirectAdmin('index.php?controller=AdminOrders&id_order='.(int)Tools::getValue('id_order').'&vieworder&token='.;
            }
            $id_store = Db::getInstance()->getValue('SELECT id_store FROM ' . _DB_PREFIX_ . 'ffcarvaricarrier  WHERE id_order='.$order->id);
            $stores = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'store');
            $this->smarty->assign(array(
                'id_store' => $id_store,
                'stores' => $stores
            ));

            $display = $this->display(__FILE__, 'admin_tab.tpl');

            return $display;
        }
    }

    public function hookDisplayCarrierList($arg)
    {
        $is_mobile = 'not_mobile';

        if ($arg['cart']->id_carrier != Configuration::get('CARVARI_CARRIER_ID'))
            return false;

        if ( $this->context->getMobileDevice()){
            $is_mobile = 'mobile';
        }

        if (!$this->isCached('carrier.tpl', $this->getCacheId($is_mobile))) {

            $stores = Db::getInstance()->executeS('
			SELECT s.*, cl.name country, st.iso_code state
			FROM ' . _DB_PREFIX_ . 'store s
			' . Shop::addSqlAssociation('store', 's') . '
			LEFT JOIN ' . _DB_PREFIX_ . 'country_lang cl ON (cl.id_country = s.id_country)
			LEFT JOIN ' . _DB_PREFIX_ . 'state st ON (st.id_state = s.id_state)
			WHERE s.active = 1 AND cl.id_lang = ' . (int)$this->context->language->id);

            $this->smarty->assign('stores', $stores);
        }

        $display = $this->display(__FILE__, 'carrier.tpl', $this->getCacheId($is_mobile));

        return $display;
    }

    public function hookActionCarrierUpdate($params)
    {
        $id_carrier_old = (int)($params['id_carrier']);
        $id_carrier_new = (int)($params['carrier']->id);
        if ($id_carrier_old == (int)(Configuration::get('CARVARI_CARRIER_ID')))
            Configuration::updateValue('CARVARI_CARRIER_ID', $id_carrier_new);
    }

    public static function installExternalCarrier($config)
    {
        $carrier = new Carrier();
        $carrier->name = $config['name'];
        $carrier->id_tax_rules_group = $config['id_tax_rules_group'];
        $carrier->id_zone = $config['id_zone'];
        $carrier->active = $config['active'];
        $carrier->is_free = $config['is_free'];
        $carrier->deleted = $config['deleted'];
        $carrier->delay = $config['delay'];
        $carrier->shipping_handling = $config['shipping_handling'];
        $carrier->range_behavior = $config['range_behavior'];
        $carrier->is_module = $config['is_module'];
        $carrier->shipping_external = $config['shipping_external'];
        $carrier->external_module_name = $config['external_module_name'];
        $carrier->need_range = $config['need_range'];

        $languages = Language::getLanguages(true);
        foreach ($languages as $language)
        {
            if ($language['iso_code'] == 'fr')
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == 'en')
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
            if ($language['iso_code'] == Language::getIsoById(Configuration::get('PS_LANG_DEFAULT')))
                $carrier->delay[(int)$language['id_lang']] = $config['delay'][$language['iso_code']];
        }

        if ($carrier->add())
        {
            $groups = Group::getGroups(true);
            foreach ($groups as $group)
                Db::getInstance()->insert('carrier_group', array('id_carrier' => (int)($carrier->id), 'id_group' => (int)($group['id_group'])));

            $rangePrice = new RangePrice();
            $rangePrice->id_carrier = $carrier->id;
            $rangePrice->delimiter1 = '0';
            $rangePrice->delimiter2 = '100000';
            $rangePrice->add();

            $rangeWeight = new RangeWeight();
            $rangeWeight->id_carrier = $carrier->id;
            $rangeWeight->delimiter1 = '0';
            $rangeWeight->delimiter2 = '100000';
            $rangeWeight->add();

            $zones = Zone::getZones(true);
            foreach ($zones as $zone)
            {
                Db::getInstance()->insert('carrier_zone', array('id_carrier' => (int)($carrier->id), 'id_zone' => (int)($zone['id_zone'])));
                Db::getInstance()->insert('delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => (int)($rangePrice->id), 'id_range_weight' => NULL, 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), true);
                Db::getInstance()->insert('delivery', array('id_carrier' => (int)($carrier->id), 'id_range_price' => NULL, 'id_range_weight' => (int)($rangeWeight->id), 'id_zone' => (int)($zone['id_zone']), 'price' => '0'), true);
            }

            // Copy Logo
//            if (!copy(dirname(__FILE__).'/carrier.jpg', _PS_SHIP_IMG_DIR_.'/'.(int)$carrier->id.'.jpg'))
//                return false;

            // Return ID Carrier
            return (int)($carrier->id);
        }

        return false;
    }

    public function getOrderShippingCost($params, $shipping_cost)
    {
        // This example returns shipping cost with overcost set in the back-office, but you can call a webservice or calculate what you want before returning the final value to the Cart
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER1_CARRIER_ID')) && Configuration::get('MYCARRIER1_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER1_OVERCOST'));
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER2_CARRIER_ID')) && Configuration::get('MYCARRIER2_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER2_OVERCOST'));

        // If the carrier is not known, you can return false, the carrier won't appear in the order process
        return false;
    }

    public function getOrderShippingCostExternal($params)
    {
        // This example returns the overcost directly, but you can call a webservice or calculate what you want before returning the final value to the Cart
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER1_CARRIER_ID')) && Configuration::get('MYCARRIER1_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER1_OVERCOST'));
//        if ($this->id_carrier == (int)(Configuration::get('MYCARRIER2_CARRIER_ID')) && Configuration::get('MYCARRIER2_OVERCOST') > 1)
//            return (float)(Configuration::get('MYCARRIER2_OVERCOST'));

        // If the carrier is not known, you can return false, the carrier won't appear in the order process
        return false;
    }




    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;

        return $cache_id.'|'.Db::getInstance()->getValue('SELECT MAX(id_store) FROM ' . _DB_PREFIX_ . 'store');
    }
}