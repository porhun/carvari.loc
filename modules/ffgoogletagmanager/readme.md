# Instalacion

Adds custom hooks GoogleTagManagerAfterHeader and GoogleTagManagerAfterBody header.tpl of your theme

    <head>
    {hook h='googletagmanagerafterheader' mod='ffgoogletagmanager'}
    <title>{$meta_title|escape:'htmlall':'UTF-8'}</title>
    ...    
    <body id="{$page_name|escape:'htmlall':'UTF-8'}">
    {hook h='googletagmanagerafterbody' mod='ffgoogletagmanager'}

If use at the project module advancedcheckout, then add custom hook in the order-confirmation_f.tpl

    {hook h="displayorderconfirmation" mod='ffgoogletagmanager'}
    
Put it in a folder ffgoogletagmanager in the directory modules/ Prestashop and active with back-office.

Define your ID in Google Tag Manager in the settings of the module.

To set up Google Tag Manager to correctly read the variables in the dataLayer

# DataLayer variables
This version of the module sends dataLayer following values
        
        transactionId
        transactionTotal
        transactionShipping
        products: array con { id_product, name, category, price, quantity }
        PageType: [Homepage|ProductPage|ListingPage|BasketPage|TransactionPage]
        ProductID: id del producto
        ProductIDList: array id a products 
        'typeof_c': [new_customer|returning_customer]
