<script data-keepinline="true">
window.dataLayer = window.dataLayer || [];

{if $transactionID}
    dataLayer.push({
        'ecommerce' : {
            'currencyCode' : '{$currencyCode}',
            'PageType': '{$PageType}',
            'purchase' : {
                'actionField' : {
                    'id'      : '{$transactionID}',
                    'revenue' : '{$revenue}',
                    'shipping': '{$shipping}',
                    'coupon'  : '{$coupon}'
                },
                'products' : [
                    {foreach from=$cartProducts item=product}
                    {literal}
                    {
                        {/literal}
                        'id'         : '{$product[0]['id']}',
                        'sku'        : '{$product[0]['sku']}',
                        'name'       : '{$product[0]['name']}',
                        'price'      : '{$product[0]['price']}',
                        'old_price'  : '{$product[0]['old_price']}',
                        'quantity'   : '{$product[0]['quantity']}',
                        'brand'      : '{$product[0]['brand']}',
                        'category'   : '{$product[0]['category']}',
                        'attributes' : '{$product['attributes']}',
                        {literal}
                    }
                    {/literal}{if not $product@last}, {/if}
                    {/foreach}
                ]
            }
        },
        'event' : '{$detailView}'
    });
{/if}

{if isset($PageType) &&  $PageType|escape:'html':'UTF-8' == 'ProductPage'}
    dataLayer.push({
        'ecommerce' : {
            'currencyCode' : '{$currencyCode}',
            'PageType': '{$PageType}',
            'detail' : {
                'products' : [
                    {foreach from=$productInfo item=product}
                        {literal}
                    {
                        {/literal}
                        'id'         : '{$product['id']}',
                        'sku'        : '{$product['sku']}',
                        'name'       : '{$product['name']}',
                        'price'      : '{$product['price']}',
                        'old_price'  : '{$product['old_price']}',
                        'quantity'   : '{$product['quantity']}',
                        'brand'      : '{$product['brand']}',
                        'category'   : '{$product['category']}',
                        'coupon'     : '{$product['coupon']}',
                        'variant'    : '{$product['variant']}'
                        {literal}
                    }
                        {/literal}
                        {if not $product@last}, {/if}
                    {/foreach}
                ]
            }
        },
        'event' : '{$detailView}'
    });
{/if}

{if $threeProducts}
    dataLayer.push({
        'ecommerce' : {
            'currencyCode' : '{$currencyCode}',
            'PageType': '{$PageType}',
            'impressions' : [
                {foreach from=$threeProducts item=product}
                    {literal}
                {
                    {/literal}
                    'id'       : '{$product['id_product']}',
                    'name'     : '{$product['name']}',
                    'price'    : '{$product['price']}',
                    'brand'    : '{$product['manufacturer_name']}',
                    'category' : '{$product['category_default']}',
                    'quantity' : '{$product['quantity']}',
                    'position' : '{$product@iteration}'
                    {literal}
                }
                    {/literal}
                    {if not $product@last}, {/if}
                {/foreach}
            ]
        },
        'event' : '{$detailView}'
    });
{/if}

{if $transactionProducts}
    dataLayer.push({
        'ecommerce' : {
            'currencyCode' : '{$currencyCode}',
            'PageType': '{$PageType}',
            'checkout' : {
                'products' : [
                    {foreach from=$transactionProducts item=product}
                    {literal}
                    {
                        {/literal}
                        'id'         : '{$product[0]['id']}',
                        'sku'        : '{$product[0]['sku']}',
                        'name'       : '{$product[0]['name']}',
                        'price'      : '{$product[0]['price']}',
                        'old_price'  : '{$product[0]['old_price']}',
                        'quantity'   : '{$product[0]['quantity']}',
                        'brand'      : '{$product[0]['brand']}',
                        'category'   : '{$product[0]['category']}',
                        'attributes' : '{$product['attributes']}',
                        'coupon'     : '{$product[0]['coupon']}',
                        'variant'    : '{$product[0]['variant']}',
                        {literal}
                    }
                    {/literal}
                    {if not $product@last}, {/if}
                    {/foreach}
                ]
            }
        },
        'event' : '{$detailView}'
    });
{/if}

{if $userID}
    dataLayer.push({
        'userID' : '{$userID}'
    });
{/if}
</script>
<!-- End Google Tag Manager Data Layer -->