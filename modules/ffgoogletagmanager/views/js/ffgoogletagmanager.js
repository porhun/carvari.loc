(function (obj) {
    window[obj.windowVariableName] = 0;
    function isElementInViewport(el) {
        if (typeof jQuery === "function" && el instanceof jQuery) {
            el = el[0];
        }
        var rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }
    function checkAllElements() {
        $(obj.targetSelector).each(function (i, e) {
            if (isElementInViewport(e)) {
                if ($(e).attr('data-viewed') === undefined) $(e).attr('data-viewed', true);
            }
        });
        return (function () {
            var elements = $('[data-viewed]').length;
            return elements;
        })();
    }
    function logResult(newValue) {
        if (window[obj.windowVariableName] !== newValue) window[obj.windowVariableName] = newValue;
    }
    $(document).on('scroll', function () { logResult(checkAllElements()) });
    logResult(checkAllElements());
})({
    targetSelector: '.product_item',
    windowVariableName: 'amoutOfViewedProduct'
});

$(document).ready(function (e) {
    eeGlobal.addListener('cart-product-delete', function (args) {
        var info = args[0];
        var data = {};
        if (info) {
        data = {
            update_way: 'delete',
            id_product: info.id_product,
            id_attribute: info.id_attribute
            };
        }

        $.ajax({
            type: 'POST',
            headers: {"cache-control": "no-cache"},
            url: baseDir + 'modules/ffgoogletagmanager/ajax.php',
            async: true,
            cache: false,
            dataType: 'json',
            data: {method: 'displayAjax', data: data, ajax: 1},

            success: function (data) {
                if (!data || !data.status || data.status !== 'ok' || !data.product_info) {
                    console.error('Cant get e-commerse info');
                    return false;
                }

                dataLayer.push({
                    'ecommerce': data.product_info,
                    'event': data.event_info
                });

                // console.log(data);
            }
        });
    });
});