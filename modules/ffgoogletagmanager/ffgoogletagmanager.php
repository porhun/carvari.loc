<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class FfGoogleTagManager extends Module
{
    protected $errors = array();
    protected $event_info_skeleton = ['id' => 0, 'sku' => '', 'name' => '', 'price' => 0, 'brand' => '', 'category' => '', 'variant' => '', 'quantity' => 0, 'coupon' => ''];

    const FF_GTA_ID = 'ff_gta_id';
    const CART_WAY_REMOVE = 'delete';
    const CART_WAY_ADD = 'add';
    const ORDER_CONFIRMED = 'transaction';
    const CART_PRODUCT_ADD = 'addToCart';
    const CART_PRODUCT_REMOVE = 'removeFromCart';
    const PRODUCT_ITEM_DETAIL = 'detailView';

    public function __construct()
    {
        $this->name = 'ffgoogletagmanager';
        $this->tab = 'analytics_stats';
        $this->version = '1.0';
        $this->author = 'dkostrub';
        $this->secure_key = Tools::encrypt($this->name);
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Google Ecommerce Analytics for Tag Manager');
        $this->description = $this->l('Google Ecommerce Analytics for site pages via Tag Manager');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if(!parent::install() ||

            !$this->registerHook('GoogleTagManagerAfterHeader') ||
            !$this->registerHook('GoogleTagManagerAfterBody') ||
            !$this->registerHook('header') ||
            !$this->registerHook('displayHome') ||
            !$this->registerHook('displayFooterProduct') ||

            // Use to set product listings (categories and search) page dataLayer vars
            !$this->registerHook('listingPage') ||

            !$this->registerHook('actionProductListOverride') ||
            !$this->registerHook('actionSearch') ||
            !$this->registerHook('displayShoppingCart') ||
            !$this->registerHook('displayOrderConfirmation') ||
            !$this->registerHook('actionCartUpdate')
        )
            return false;

        Configuration::updateValue(self::FF_GTA_ID, '');
        $this->cleanClassCache();
        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;

        Configuration::deleteByName(self::FF_GTA_ID);
        return true;
    }

    protected function cleanClassCache()
    {
        $class_cache = _PS_CACHE_DIR_.'class_index.php';
        if (file_exists($class_cache))
            unlink($class_cache);
    }

    public function displayConfigForm()
    {
        $this->fields_options = array(
            'general' => array(
                'title' => $this->l('Настройка модуля'),
                'fields' =>
                    array(
                        self::FF_GTA_ID => array(
                            'title' => $this->l('Tag Manager ID'),
                            'type' => 'text',
                            'cast' => 'strval',
                        ),
                    ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'button',
                    'required' => true,
                    'name' => 'submitOptions'
                ),
            )
        );

        $helper = new HelperOptions();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ),
        );
        return $helper->generateOptions($this->fields_options);
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitOptions'))
            ConfigurationCore::updateValue(self::FF_GTA_ID, Tools::getValue(self::FF_GTA_ID, ''));

        return $this->displayConfigForm();
    }

    public function hookGoogleTagManagerAfterHeader($params)
    {
        $this->context->smarty->assign('GTM_ID', Configuration::get(self::FF_GTA_ID, ''));
        $page_name = Tools::getValue('controller');
        $current_url = tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']);

        if($current_url != '/order/'){
            return $this->display(__FILE__, 'views/templates/hooks/googletagmanagerontop.tpl');
        }
    }

    public function hookGoogleTagManagerAfterBody()
    {
        $this->context->smarty->assign('GTM_ID', Configuration::get(self::FF_GTA_ID, ''));
        return $this->display(__FILE__, 'views/templates/hooks/googletagmanagerafterbody.tpl');
    }

    public function hookHeader($params)
    {
        $this->context->controller->addjs($this->getLocalPath().'/views/js/ffgoogletagmanager.js');
    }

    /**
     * Index page
     * @param $params
     */
    public function hookDisplayHome($params)
    {
        $customer = Context::getContext()->customer->id;
        if(count($customer) == 1 )
            $this->context->smarty->assign('userID', $customer);

        $this->context->smarty->assign('PageType', 'HomePage');
    }

    /**
     * Product page
     * @param $params
     */
    public function hookDisplayFooterProduct($params)
    {
        $customer = Context::getContext()->customer->id;
        if(count($customer) == 1 )
            $this->context->smarty->assign('userID', $customer);

        $currency = $this->formatCurrency();
        $product = $this->context->controller->getProduct();
        $eventInfo = $this->formatProducts([$product]);

        $this->context->smarty->assign(array(
            'PageType'     => 'ProductPage',
            'detailView'   => 'detailView',
            'currencyCode' => $currency,
            'productInfo'    => $eventInfo,
        ));
    }

    /**
     * Catalog
     * @param $params
     */
    public function hookActionProductListOverride($params)
    {
        //TODO to do get the values id products after the transition to the next page of pagination
        //Get first three products in category page
//        $order_by = $this->context->controller->orderBy;
//        $order_way = $this->context->controller->orderWay;
//        $currency = $this->formatCurrency();
//        $id_category = Tools::getValue('id_category');
//        $category = new Category($id_category);
//        $three_products = $category->getProducts($this->context->language->id, 1, 3, $order_by, $order_way);
//
////        var_dump($three_products);
//
//        $this->context->smarty->assign(array(
//            'PageType' => 'ListingPage',
//            'detailView'   => 'viewCatalog',
//            'currencyCode' => $currency,
//            'threeProducts' => $three_products,
//        ));
    }

    /**
     * Search result
     * @param $params
     */
    public function hookActionSearch($params)
    {
        //TODO: similar to previous function, but using search results
    }

    /**
     * Cart page
     * @param $params
     */
    public function hookDisplayShoppingCart($params)
    {
        $customer = Context::getContext()->customer->id;
        if(count($customer) == 1 )
            $this->context->smarty->assign('userID', $customer);

        $currency = $this->formatCurrency();
        $step_in_checkout_process= $this->context->controller->step;

        $eventInfo = [];
        foreach ($params['products'] as $product)
        {
            $products = new Product((int)$product['id_product']);
            $attributes = ['attributes'=>$product['attributes']];
            $event = $this->formatProducts([$products]);
            $eventInfo[] = array_merge($event, $attributes);
        }

        if( $step_in_checkout_process == 0) {
            $this->context->smarty->assign(array(
                'PageType' => 'BasketPage',
                'detailView' => 'addToCart',
                'currencyCode' => $currency,
                'transactionProducts' => $eventInfo,
            ));
        }

    }

    /**
     * Deleted product in Cart page
     * @param $params
     * @return array
     */
    public function hookActionCartUpdate($params)
    {
        if (!($update_way = $params['update_way']) || !($id_product = (int)$params['id_product'])) {
            return [];
        }

        $currency = $this->formatCurrency();
        $product = new Product($id_product);
        $eventInfo = ['products' => $this->formatProducts([$product])];

        return [
            'currencyCode' => $currency,
            'remove' => $eventInfo,
            ];
    }

    /**
     * Confirmation page
     * @param $params
     */
    public function hookDisplayOrderConfirmation($params)
    {
        $context = Context::getContext();
        $orderID = Order::getOrderByCartId((int)($context->cart->id));
        $order = new Order($orderID);

        $customer = Context::getContext()->customer->id;
        if(count($customer) == 1 )
            $this->context->smarty->assign('userID', $customer);

//        $order_details = $order->getOrderDetailList();

//        $customer = new Customer($context->cart->id_customer);
//        if( count(Order::getCustomerOrders($customer->id)) == 1 )
//            $this->smarty->assign('type_of_customer', 'new_customer');
//        else
//            $this->smarty->assign('type_of_customer', 'returning_customer');

        $currency = $this->context->currency->iso_code;
        $total = (float)$context->cart->getOrderTotal(true, Cart::BOTH);

        if (Validate::isLoadedObject($order) && $order->id_customer == $this->context->customer->id) {

            if ($order->total_discounts > 0)
                $this->context->smarty->assign('total_old', (float)$order->total_paid - $order->total_discounts);

            $cart_products = $this->context->cart->getProducts();
            $eventInfo = [];
            foreach ($cart_products as $product)
            {
                $products = new Product((int)$product['id_product']);
                $attributes = ['attributes'=>$product['attributes']];
                $event = $this->formatProducts([$products]);
                $eventInfo[] = array_merge($event, $attributes);
            }

            $cart_rules_coupon = array_pop($order->getCartRules());
            $manufacturer = new Manufacturer($cart_rules_coupon['id_manufacturer'], $cart_rules_coupon->id_lang);

            $this->context->smarty->assign(array(
                'PageType'           => 'TransactionPage',
                'detailView'         => 'transaction',
                'currencyCode'       => $currency,
                'transactionID'      => $order->id,
                'cartProducts'       => $eventInfo,
                'brand'              => $manufacturer,
                'affiliation'        => '',
                'revenue'            => sprintf('%01.2f', (float)$total),
                'tax'                => Configuration::get('PS_TAX'),
                'shipping'           => sprintf('%01.2f', (float)$order->total_shipping),
                'coupon'             => ($cart_rules_coupon) ? $cart_rules_coupon['name'] : ''
            ));
        }
    }

    protected function formatCurrency ()
    {
        $currency = Currency::getCurrencyInstance((int)Context::getContext()->currency);
        return $currency->iso_code;
    }

    protected function formatProducts(array $products)
    {
        $formatted_products = [];
        array_walk($products, function (Product $product) use (&$formatted_products) {
            $formatted_product = [];
            $cat_names = $this->getCategoriesNames($product->getParentCategories());
            $reference = str_replace('_',' ',$product->reference);
            $product_price = (float)$product->getPrice(false, null, 2);
            $product_old_price = (float)$product->getPriceWithoutReduct(false, null, 2);
            $product_quantity = ((int)$product->ecomm_quantity) ? (int)$product->ecomm_quantity : 1;
            $formatted_product['id'] = $product->id;
            $formatted_product['sku'] = $reference;
            $formatted_product['name'] = (is_array($product->name)) ? array_pop($product->name).' '.$reference : $product->name.' '.$reference;
            $formatted_product['price'] = $product_price;
            $formatted_product['old_price'] = $product_old_price;
            $formatted_product['brand'] = $product->getWsManufacturerName();
            $formatted_product['category'] = implode('/', $cat_names);
            $formatted_product['variant'] = ($product->ecomm_attribute) ?: '';
            $formatted_product['quantity'] = $product_quantity;
            $formatted_product['coupon'] = '';  // Coupon are being used for order only
            $formatted_product = array_merge($this->event_info_skeleton, $formatted_product);
            $formatted_products[] = $formatted_product;
        });
        return $formatted_products;
    }

    protected function getCategoriesNames(array $categories)
    {
        $categories = array_filter($categories, function ($category) { return !in_array($category['id_category'], [1,2]); });
        return array_column($categories, 'name');
    }

    public function hookModuleRoutes($params)
    {
        return [
            "module-{$this->name}-cart_update" => [
                'controller' => 'cart_update',
                'rule' => "module/{$this->name}/cart_update/",
                'keywords' => [],
                'params' => [
                    'fc' => 'module',
                    'module' => $this->name,
                ],
            ]
        ];
    }
}