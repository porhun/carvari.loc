<?php

require_once(dirname(__FILE__).'../../../config/config.inc.php');
require_once(dirname(__FILE__).'../../../init.php');

include_once(dirname(__FILE__).'/ffgoogletagmanager.php');

$module_instance = new FfGoogleTagManager();

if (Tools::getValue('method') == 'displayAjax') {
    $data = Tools::getValue('data');
    $id_product = (int)$data['id_product'];
    $id_attribute = ($data['id_attribute']) ? : 0;
    $update_way = $data['update_way'];

    if (!$id_product || !$update_way) {
        die(Tools::jsonEncode(['status' => 'error']));
    }
    $update_cart_event = ($update_way == FfGoogleTagManager::CART_WAY_REMOVE) ? FfGoogleTagManager::CART_PRODUCT_REMOVE : FfGoogleTagManager::CART_PRODUCT_ADD;

//    $event_info = Hook::exec('actionCartUpdate', array('id_product' => $id_product, 'update_way' => $update_way, 'id_attribute' => $id_attribute));
    die(Tools::jsonEncode(['status' => 'ok', 'event_info' => $update_cart_event, 'product_info' => $module_instance->hookActionCartUpdate($data)]));
}