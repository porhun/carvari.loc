<?php

if (!defined('_PS_VERSION_'))
    exit;

class Ffspecpricesxls extends Module
{

    public function __construct()
    {
        $this->name = 'ffspecpricesxls';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Special Prices From XSL');
        $this->description = $this->l('Импорт спец цен из XSL');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');

    }


    public function install()
    {
        if (!parent::install())
            return false;
        return true;
    }

    public function uninstall()
    {

        if (!parent::uninstall())
            return false;

        return true;
    }

    public function getContent()
    {

        if ( Tools::isSubmit('makeImport') ) {
            if ($this->makeImport()){
                $this->context->smarty->assign('import_success', true);
            }
        }

        if ( Tools::isSubmit('makeExport') ) {
            $this->makeExport();
        }

        if ( Tools::isSubmit('submitSpecPriceRule') ) {
            $this->processSpecPriceRule();
        }

        if( Tools::getIsset('updatespecific_price')) {
            return $this->renderForm();
        }

        if( Tools::getIsset('viewspecific_price')) {
            return $this->renderProductsList();
        }


        $this->context->smarty->assign('errors', $this->_errors);

        return $this->renderList().$this->context->smarty->fetch(__DIR__.'/views/templates/ffspecpricesxls.tpl');
    }

    protected function makeExport()
    {
        require_once(__DIR__.'/lib/xlsxwriter.class.php');

        $currency_iso_code= $this->context->currency->iso_code;
        $id_lang = $this->context->language->id;
        // clean buffer
        if (ob_get_level() && ob_get_length() > 0)
            ob_clean();


        $id_feature = Db::getInstance()->getValue('SELECT id_feature FROM ps_feature_lang WHERE `name`="'.'Сезон'.'" AND id_lang='.$this->context->language->id);

        $result = Db::getInstance()->executeS('SELECT pp.id_product, pp.reference, pp.ean13, pp.id_category_default, pps.active, ppl.name, ppl.description, ppl.description_short, ppl.link_rewrite, pcl.name as category_name, i.id_image
						FROM ps_product pp
						LEFT JOIN ps_product_lang ppl ON ppl.id_product=pp.id_product AND ppl.id_lang='.$id_lang
            .' LEFT JOIN ps_product_shop pps ON pps.id_product=pp.id_product AND pps.id_shop=1'
            .' LEFT JOIN ps_category_lang pcl ON pcl.id_category=pp.id_category_default AND pcl.id_lang='.$id_lang
            .' INNER JOIN ps_image i ON i.id_product=ppl.id_product AND i.cover=1'
        );



        $link = new LinkCore();

        $content = array();

        $feed_title = array(
            'название скидки' => 'integer',
            'код товара' => 'string',
            'дата начала скидки' => 'date',
            'дата окончания скидки' => 'date',
            'процент скидки' => 'integer',
        );


        foreach ( $result as $i => $product ) {
            $feed_line = array();
            $feed_line[] = '';
            $feed_line[] = $product['ean13'];
            $feed_line[] = '';
            $feed_line[] = '';
            $feed_line[] = '';
            $content[] = $feed_line;
        }


        $filename = "carvari.xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $writer = new XLSXWriter();
        $writer->setAuthor('ForForce');
        $writer->writeSheet($content,'Sheet1',$feed_title);
        $writer->writeToStdOut();
        exit(0);


    }

    protected function makeImport()
    {

        require_once(__DIR__.'/lib/simplexlsx/simplexlsx.class.php');

        $Reader = new SimpleXLSX($_FILES['file']['tmp_name']);

        $id_shop = $this->context->shop->id;
        $id_shop_group = $this->context->shop->id_shop_group;
        $id_currency = $this->context->currency->id;

        $current_line = 0;
        foreach ($Reader->rows() as $line) {
            $spec_price_new = null;
            $spec_name = $line[0];
            $ean13 = $line[1];
            $spec_from = $line[2];
            $spec_to = $line[3];
            $spec_percent = round($line[4]);

            $current_line++;
            if ($current_line < 2 || empty($ean13)) continue;

            $id_product = $this->getIdByEan13Carvari($ean13);
            if($id_product && is_numeric($spec_percent)) {
                $product = new Product($id_product);
                $spec_prices = SpecificPrice::getByProductId($id_product, 0);

                foreach ($spec_prices as $spec_price) {
                    if ($spec_price['name'] == $spec_name) {
                        $spec_price_new = new SpecificPrice($spec_price['id_specific_price']);
                        break;
                    }
                }

                if(empty($spec_price_new)) {
                    $spec_price_new = new SpecificPrice();
                }
                $spec_price_new->name = $spec_name;
                $spec_price_new->id_product = $product->id;
                $spec_price_new->id_product_attribute = 0;
                $spec_price_new->id_shop = 0;
                $spec_price_new->id_shop_group = 0;
                $spec_price_new->id_currency = 0;
                $spec_price_new->id_country = 0;
                $spec_price_new->id_group = 0;
                $spec_price_new->id_customer  = 0;
                $spec_price_new->price  = -1;
                $spec_price_new->from_quantity = 1;
                $spec_price_new->reduction = $spec_percent/100;
                $spec_price_new->reduction_type = 'percentage';
                $spec_price_new->from = $spec_from == 0 ? '0000-00-00 00:00:00' : date("Y-m-d H:i:s",strtotime($spec_from));
                $spec_price_new->to = $spec_to == 0 ? '0000-00-00 00:00:00' : date("Y-m-d H:i:s",strtotime($spec_to));
                $spec_price_new->save();
            } else {
                $this->_errors[] = 'Отсутствует продукт с таким ean13 - '.$ean13.', либо неверно введен параметр скидки - '.$spec_percent;
            }
        }

        return true;

    }

    public function processSpecPriceRule()
    {

        $post_reduction['name'] = Tools::getValue('name');
        $post_reduction['reduction'] = Tools::getValue('reduction')/100;
        $post_reduction['from'] = Tools::getValue('from') == '' ? '0000-00-00 00:00:00' : date("Y-m-d H:i:s",strtotime(Tools::getValue('from')));
        $post_reduction['to'] = Tools::getValue('to') == '' ? '0000-00-00 00:00:00' : date("Y-m-d H:i:s",strtotime(Tools::getValue('to')));

        $id_hash = Tools::getValue('id_hash');
        $current_reduction = DB::getInstance()->getRow('SELECT MD5(`name`) as id_hash, `name`, `from`, `to`, ROUND(`reduction`*100) as reduction FROM `'._DB_PREFIX_.'specific_price` WHERE MD5(`name`)="'.$id_hash.'"');

        if( $post_reduction['name'] != $current_reduction['name'] ||
            $post_reduction['reduction'] != $current_reduction['reduction'] ||
            $post_reduction['from'] != $current_reduction['from'] ||
            $post_reduction['to'] != $current_reduction['to']
        ) {
            DB::getInstance()->update('specific_price',
                [
                    'name' => $post_reduction['name'],
                    'reduction' => $post_reduction['reduction'],
                    'from' => $post_reduction['from'],
                    'to' => $post_reduction['to'],
                ],
                'MD5(`name`)="'.$id_hash.'"');
        }

        Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
    }

    public function getIdByEan13Carvari($ean13)
    {

        if (empty($ean13)) {
            return 0;
        }

        if (!Validate::isEan13($ean13)) {
            return 0;
        }

        $query = new DbQuery();
        $query->select('p.id_product');
        $query->from('product', 'p');
        $query->where('p.ean13 = \''.pSQL($ean13).'\'');
        $id_product = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);

        if (empty($id_product)) {
            $query = new DbQuery();
            $query->select('pa.id_product');
            $query->from('product_attribute', 'pa');
            $query->where('pa.ean13 = \''.pSQL($ean13).'\'');
            $id_product = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
        }

        return $id_product;
    }

    public function renderList()
    {
        $fields_list = [
//            'id_hash' => [
//                'title'   => $this->l('Id'),
//                'width'   => 150,
//                'orderby' => false,
//                'hidden' => true
//            ],
            'name' => [
                'title'   => $this->l('Name'),
                'width'   => 150,
                'orderby' => false,
            ],
            'from'  => [
                'title'   => $this->l('From'),
                'width'   => 150,
                'orderby' => false,
            ],
            'to' => [
                'title'   => $this->l('To'),
                'width'   => 150,
                'orderby' => false,
            ],
            'reduction' => [
                'title'   => $this->l('Reduction'),
                'width'   => 150,
                'orderby' => false,
            ],
        ];

        $helper = new HelperList();
        $helper->simple_header = true;
        $helper->identifier    = 'id_hash';
        $helper->table         = SpecificPrice::$definition['table'];
        $helper->actions       = ['view', 'edit', 'delete'];
        $helper->show_toolbar  = false;
        $helper->module        = $this;
        $helper->title         = $this->l('SpecificPrice list');
        $helper->token         = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex  = AdminController::$currentIndex.'&configure='.$this->name;
        $list_data            = $this->getListData();

        return $helper->generateList( $list_data, $fields_list);
    }

    public function renderForm()
    {

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Specific price edit'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_hash',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Name'),
                        'name' => 'name',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Reduction'),
                        'name' => 'reduction',
                        'desc' => $this->l('Процент скидки, %'),
                        'class' => 'fixed-width-lg'
                    ),
                    array(
                        'type' => 'datetime',
                        'label' => $this->l('From'),
                        'name' => 'from',
                    ),
                    array(
                        'type' => 'datetime',
                        'label' => $this->l('To'),
                        'name' => 'to',
                    )
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => [
                    [
                        'icon' => 'process-icon-back icon',
                        'title' => $this->l('Back'),
                        'href' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
                    ]
                ]
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitSpecPriceRule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');


        $helper->tpl_vars = array(
            'fields_value' => $this->getFormData(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function renderProductsList()
    {

        $id_hash = Tools::getValue('id_hash');

        $fields_list = [
            'id_product' => [
                'title'   => $this->l('Id'),
                'width'   => 150,
                'orderby' => false,
                'hidden' => true
            ],
            'name' => [
                'title'   => $this->l('Name'),
                'width'   => 150,
                'orderby' => false,
            ],
            'ean13'  => [
                'title'   => $this->l('Ean13'),
                'width'   => 150,
                'orderby' => false,
            ],
            'reference'  => [
                'title'   => $this->l('Reference'),
                'width'   => 150,
                'orderby' => false,
            ],
            'price_without_reduction' => [
                'title'   => $this->l('Price Without Reduction'),
                'width'   => 150,
                'orderby' => false,
            ],
            'price' => [
                'title'   => $this->l('Price'),
                'width'   => 150,
                'orderby' => false,
            ],
        ];

        $helper = new HelperList();
        $helper->simple_header = true;
        $helper->identifier    = 'id_product';
        $helper->table         = Product::$definition['table'];
//        $helper->actions       = ['delete'];
        $helper->show_toolbar  = false;
        $helper->module        = $this;
        $helper->title         = $this->l('SpecificPrice Product list') . ' <span class="badge">'. $this->getProductsListData($id_hash, 0, 0, true) .'</span>';
        $helper->token         = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex  = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->listTotal      = $this->getProductsListData($id_hash, 0, 0, true);

        $helper->toolbar_btn = [
            'back' => [
                'desc' => $this->l('Back'),
                'class' => 'icon',
                'href' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ]
        ];

        $list_data            = $this->getProductsListData($id_hash, Tools::getValue('page'));
//        d($list_data);

        return $helper->generateList( $list_data, $fields_list);
    }

    public function getFormData()
    {
        $id_hash = Tools::getValue('id_hash');

        return DB::getInstance()->getRow('SELECT MD5(`name`) as id_hash, `name`, `from`, `to`, ROUND(`reduction`*100) as reduction FROM `'._DB_PREFIX_.'specific_price` WHERE MD5(`name`)="'.$id_hash.'"');
    }

    public function getListData()
    {
        return DB::getInstance()->executeS('SELECT MD5(`name`) as id_hash, `name`, `from`, `to`, `reduction`  FROM `'._DB_PREFIX_.'specific_price` WHERE `name`!="" GROUP BY `name`');
    }

    public function getProductsListData($id_hash, $page_number = 0, $nb_products = 100000, $count = false)
    {
        $ids = [];
        if ($page_number < 0) {
            $page_number = 0;
        }
        if ($nb_products < 1) {
            $nb_products = 10;
        }
        $order_by = 'id_product';
        $order_way = 'ASC';

        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add' || $order_by == 'date_upd') {
            $order_by_prefix = 'product_shop';
        } elseif ($order_by == 'name') {
            $order_by_prefix = 'pl';
        }

        if ($count) {
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT COUNT(DISTINCT sp.`id_product`)  
                                            FROM `'._DB_PREFIX_.'specific_price` sp 
                                            WHERE MD5(`name`)="'.$id_hash.'"');
        }

        $id_lang = $this->context->language->id;
        if ($result = DB::getInstance()->executeS('SELECT id_product  
                                            FROM `'._DB_PREFIX_.'specific_price` sp 
                                            WHERE MD5(`name`)="'.$id_hash.'"
                                            GROUP BY `id_product`')
        ) {
            foreach ($result as $row) {
                $ids[] = $row['id_product'];
            }
        } else {
            return [];
        }


        $sql = '
		SELECT
			p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`,
			IFNULL(product_attribute_shop.id_product_attribute, 0) id_product_attribute,
			pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`,
			pl.`name`, image_shop.`id_image` id_image, il.`legend`, m.`name` AS manufacturer_name,
			DATEDIFF(
				p.`date_add`,
				DATE_SUB(
					"'.date('Y-m-d').' 00:00:00",
					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
				)
			) > 0 AS new
		FROM `'._DB_PREFIX_.'product` p
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_attribute_shop` product_attribute_shop
			ON (p.`id_product` = product_attribute_shop.`id_product` AND product_attribute_shop.`default_on` = 1 AND product_attribute_shop.id_shop='.(int)$this->context->shop->id.')
		'.Product::sqlStock('p', 0, false, $this->context->shop).'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
			p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
		)
		LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
			ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$this->context->shop->id.')
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
		WHERE product_shop.`active` = 1
		AND product_shop.`show_price` = 1
		AND p.`id_product` IN ('.((is_array($ids) && count($ids)) ? implode(', ', $ids) : 0).')'.'
		ORDER BY '.(isset($order_by_prefix) ? pSQL($order_by_prefix).'.' : '').pSQL($order_by).' '.pSQL($order_way).'
		LIMIT '.(int)($page_number * $nb_products).', '.(int)$nb_products;

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (!$result) {
            return [];
        }

        return Product::getProductsProperties($id_lang, $result);

    }

}