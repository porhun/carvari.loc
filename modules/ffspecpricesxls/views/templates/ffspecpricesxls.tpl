{if isset($import_success)}
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert">×</button>
        Данные успешно импортированны
    </div>
{/if}

{foreach from=$errors item=error}
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
        {$error}
    </div>
{/foreach}

{*<div class="panel">*}

    {*<div class="panel-heading">*}
        {*Экспортировать в EXEL*}
    {*</div>*}
    {*<form action="" method="post" enctype="multipart/form-data">*}
        {*<div class="panel-body">*}
            {*<div class="form-group col-sm-3">*}
                {*<input type="submit" name="makeExport" class="btn btn-success" value="Экспортировать">*}
            {*</div>*}
        {*</div>*}
    {*</form>*}

{*</div>*}


<div class="panel">

    <div class="panel-heading">
        Импортировать из EXEL
    </div>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <input type="file" name="file">
            </div>
            <div class="form-group col-sm-12">
                Формат EXEL файла: название скидки, код товара(EAN-13), дата начала скидки, дата окончания скидки, процент скидки. Первая строка - загловок. Если в датах ставить нули, то скидка неограниченна по времени.
            </div>
            <div class="form-group col-sm-3">
                <input type="submit" name="makeImport" class="btn btn-success" value="Импортировать">
            </div>
        </div>
    </form>

</div>
