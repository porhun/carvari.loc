<?php

if (!defined('_PS_VERSION_'))
    exit;

class Ffprestamodification extends Module
{

    public function __construct()
    {
        $this->name = 'ffprestamodification';
        $this->tab = 'advertising_marketing';
        $this->version = '1.0';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Модификации ядра');
        $this->description = $this->l('Изменение стандартных классов и шаблонов');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');

    }


    public function install()
    {

        if (!parent::install()
            || !$this->updateCustomerTable()
        )
            return false;
        return true;
    }

    public function uninstall()
    {

        if (!parent::uninstall()

        )
            return false;

        return true;
    }

    protected function updateCustomerTable()
    {
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'category_lang` ADD COLUMN `seoname` varchar(255) NULL AFTER `name`');
//        }
//        catch (Exception $e){}
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer` ADD COLUMN `code_1c` varchar(128) NULL AFTER `email`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer` ADD INDEX (`code_1c`)');
//        }
//        catch(Exception $e) {}
//
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'store` ADD COLUMN `phone_opt` varchar(128) NULL AFTER `phone`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'store` ADD COLUMN `email_opt` varchar(128) NULL AFTER `email`');
//        }
//        catch(Exception $e) {}


//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'category` ADD COLUMN `promo_url` varchar(128) NULL AFTER `thumb_url`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'category` ADD COLUMN `promo_id_label` int NULL AFTER `thumb_url`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'category_lang` ADD COLUMN `promo_text` varchar(200) NULL AFTER `name`');
//        }
//        catch(Exception $e) {}
//
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'attribute` ADD COLUMN `color_hover` varchar(32) NULL AFTER `color`');
//        }
//        catch(Exception $e) {}
//
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_comment` ADD COLUMN `email` varchar(128) NOT NULL AFTER `id_guest`');
//        }
//        catch(Exception $e) {}
//
//
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video4` varchar(128) NULL AFTER `name`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video4_title` varchar(128) NULL AFTER `name`');
//
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video3` varchar(128) NULL AFTER `name`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video3_title` varchar(128) NULL AFTER `name`');
//
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video2` varchar(128) NULL AFTER `name`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video2_title` varchar(128) NULL AFTER `name`');
//
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video1` varchar(128) NULL AFTER `name`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'product_lang` ADD COLUMN `video1_title` varchar(128) NULL AFTER `name`');
//        }
//        catch(Exception $e) {}
//
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'specific_price_rule` ADD COLUMN `description` text NULL AFTER `name`');
//        }
//        catch(Exception $e) {}
//
//
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'attribute` ADD COLUMN `color_border` varchar(32) NULL AFTER `color`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'attribute` ADD COLUMN `color_text` varchar(32) NULL AFTER `color`');
//        }
//        catch(Exception $e) {}

//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer` ADD COLUMN `phone_login` varchar(13) NULL AFTER `email`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'customer` ADD INDEX (`phone_login`)');
//        }
//        catch(Exception $e) {}

//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'specific_price` ADD COLUMN `name` varchar(255) NULL AFTER `id_specific_price`');
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'specific_price` ADD INDEX (`name`)');
//        }
//        catch(Exception $e) {}

//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'category` ADD COLUMN `thumb_url` varchar(100) NULL');
//        }
//        catch(Exception $e) {}
//
//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'store` MODIFY COLUMN `phone` varchar(128) NULL');
//        }
//        catch(Exception $e) {}


//        try {
//            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'carrier_lang` ADD COLUMN `name` varchar(255) NULL AFTER `delay`');
//            Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'carrier_lang` cl LEFT JOIN `' . _DB_PREFIX_ . 'carrier` c ON c.`id_carrier`=cl.`id_carrier` SET cl.`name`=c.`name`');
//        }
//        catch(Exception $e) {}

        try {
            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'orders` ADD COLUMN `id_employee` int(11) NULL AFTER `id_customer`');

            Db::getInstance()->execute('UPDATE `' . _DB_PREFIX_ . 'orders` o LEFT JOIN `'._DB_PREFIX_.'order_history` oh ON (oh.`id_order_state` = o.`current_state`) AND oh.`id_order_history`=(SELECT MAX(id_order_history) FROM `'._DB_PREFIX_.'order_history` ohj WHERE ohj.id_order=o.`id_order`) SET o.`id_employee`= oh.`id_employee`');
        }
        catch(Exception $e) {}

	try {
            Db::getInstance()->execute('ALTER TABLE `' . _DB_PREFIX_ . 'employee` ADD COLUMN `code` int(11) NULL AFTER `firstname`');
        }
        catch(Exception $e) {}

        return true;
    }

    public function getContent()
    {
        return $this->context->smarty->fetch($this->local_path.'content.tpl');
    }


}