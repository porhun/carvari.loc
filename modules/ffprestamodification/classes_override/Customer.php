<?php

//class Customer extends CustomerCore
//{
//    public $phone_login;
//
//    public static $definition = array(
//        'table' => 'customer',
//        'primary' => 'id_customer',
//        'fields' => array(
//            'secure_key' =>                array('type' => self::TYPE_STRING, 'validate' => 'isMd5', 'copy_post' => false),
//            'lastname' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
//            'firstname' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
//            'email' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 128),
//            'phone_login' =>                  array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'required' => true, 'size' => 13),
//            'passwd' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => true, 'size' => 32),
//            'last_passwd_gen' =>            array('type' => self::TYPE_STRING, 'copy_post' => false),
//            'id_gender' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
//            'birthday' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
//            'newsletter' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
//            'newsletter_date_add' =>        array('type' => self::TYPE_DATE,'copy_post' => false),
//            'ip_registration_newsletter' =>    array('type' => self::TYPE_STRING, 'copy_post' => false),
//            'optin' =>                        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
//            'website' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
//            'company' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
//            'siret' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isSiret'),
//            'ape' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isApe'),
//            'outstanding_allow_amount' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'copy_post' => false),
//            'show_public_prices' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
//            'id_risk' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
//            'max_payment_days' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
//            'active' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
//            'deleted' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
//            'note' =>                        array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'size' => 65000, 'copy_post' => false),
//            'is_guest' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
//            'id_shop' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
//            'id_shop_group' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
//            'id_default_group' =>            array('type' => self::TYPE_INT, 'copy_post' => false),
//            'id_lang' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
//            'date_add' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
//            'date_upd' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
//        ),
//    );
//
//
//    public static function generatePin()
//    {
//        return rand(0,9).rand(0,9).rand(0,9).rand(0,9);
//    }
//    public static function upadtePasswordByPhone( $phone , $password )
//    {
//        $phone = preg_replace('/[. ()-+]*/','',$phone);
//        $phone = '+'.$phone;
//        DB::getInstance()->update('customer',
//            array( 'passwd' => Tools::encrypt($password), 'last_passwd_gen' => date("Y-m-d G:i:s") ),
//            'phone_login="'.pSQL($phone).'"');
//    }
//    public static function isCustomerExistsByPhone( $phone , $return_name = false)
//    {
//        $phone = preg_replace('/[. ()-+]*/','',$phone);
//        $phone = '+'.$phone;
//        $result = Db::getInstance()->getRow('
//		SELECT *
//		FROM `'._DB_PREFIX_.'customer`
//		WHERE `phone_login` = \''.pSQL($phone).'\'
//		AND `deleted` = 0
//		');
//        if (!$result)
//            return false;
//
//        if ($return_name)
//            return $result['firstname'];
//
//        return true;
//    }
//    public function getByEmailOrPhone( $email, $phone_login, $ignore_guest = true )
//    {
//        if (!Validate::isEmail($email) || ($phone_login && !Validate::isPhoneNumber($phone_login))) {
//            die(Tools::displayError());
//        }
//
//        $result = Db::getInstance()->getRow('
//		SELECT *
//		FROM `'._DB_PREFIX_.'customer`
//		WHERE ( `email` = \''.pSQL($email).'\' OR `phone_login` = \''.pSQL($phone_login).'\' )
//		'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
//		'.(isset($passwd) ? 'AND `passwd` = \''.pSQL(Tools::encrypt($passwd)).'\'' : '').'
//		AND `deleted` = 0
//		'.($ignore_guest ? ' AND `is_guest` = 0' : ''));
//
////		d($result);
//
//        if (!$result) {
//            return false;
//        }
//        $this->id = $result['id_customer'];
//        foreach ($result as $key => $value) {
//            if (property_exists($this, $key)) {
//                $this->{$key} = $value;
//            }
//        }
//        return $this;
//    }
//    public function getByPhone2($phone, $passwd = null, $ignore_guest = true)
//    {
//        $phone = preg_replace('/[. ()-+]*/','',$phone);
//        $phone = '+'.$phone;
//        $result = Db::getInstance()->getRow('
//		SELECT *
//		FROM `'._DB_PREFIX_.'customer`
//		WHERE `phone_login` = \''.pSQL($phone).'\'
//		'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
//		'.(isset($passwd) ? 'AND `passwd` = \''.pSQL(Tools::encrypt($passwd)).'\'' : '').'
//		AND `deleted` = 0
//		'.($ignore_guest ? ' AND `is_guest` = 0' : ''));
//
//        if (!$result)
//            return false;
//        $this->id = $result['id_customer'];
//        foreach ($result as $key => $value)
//            if (property_exists($this, $key))
//                $this->{$key} = $value;
//        return $this;
//    }
//
//}