<?php

class SmsserviceCore extends ObjectModel {

   // static private $_login = 'sms_gate_connect';
   // static private $_password = 'qwerty12345';
   // static private $_sender = 'Aldaris';
    static private $_login = 'parpara';
    static private $_password = 'Parpara@Gianni.ua';
    static private $_sender = 'parpara.com';
    static private $_server_url = 'http://turbosms.in.ua/api/wsdl.html';

    /**
     * @var SoapClient
     */
    private $_client = null;
    public function __construct() {
        try{
            $this->_client = new SoapClient (self::$_server_url);
        } catch ( Exception $e ) {
            throw new Exception('Soap Client Extension Error');
        }
        try {
            $this->_connect();
        } catch (Exception $e) {
            throw new Exception($e);
        }
    }

    private function _connect() {
        header ('Content-type: text/html; charset=utf-8');
        $auth = Array (
            'login' => self::$_login,
            'password' => self::$_password
        );

        $result = $this->_client->Auth($auth);
        if( $result->AuthResult!='Вы успешно авторизировались' ) {
            throw new Exception( $result->AuthResult );
        }
    }

    public function sendSMS( $message, $phone ) {
        $charset = mb_detect_encoding( $message, array('UTF-8', 'Windows-1251') );
        if( $charset=='Windows-1251' ) {
            $message = iconv('windows-1251', 'UTF-8', $message);
        }
        $sms = Array (
            'sender' => self::$_sender,
            'destination' => $phone,
            'text' => $message
        );
        $result = $this->_client->SendSMS( $sms );
    }
}
