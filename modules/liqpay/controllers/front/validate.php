<?php

require_once(dirname(__FILE__).'../../../liqpay.php');
class liqpayvalidateModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $display_column_left = false;
    public $display_column_right = false;

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {

//        if (Tools::getValue('fread')){
//            $handle = fopen(dirname(__FILE__).'../../../log.txt', 'r');
//            $arr = unserialize(fread($handle, 10000));
//            d($arr);
//            $data = base64_decode($arr['data']);
//            $data = json_decode($data , true);
//            d($data);
//        }
//        if ($_POST) {
//            $handle = fopen(dirname(__FILE__).'../../../log.txt', 'w');
//            $post_var = serialize($_POST);
//            fwrite($handle, $post_var);
//        }


        if (Tools::getValue('data') && Tools::getValue('signature')) {
            $liqpay_module = Module::getInstanceByName('liqpay');

            $data = Tools::getValue('data');
            $signature = Tools::getValue('signature');
            if ($signature == base64_encode( sha1( $liqpay_module->liqpay_private_key . $data . $liqpay_module->liqpay_private_key, 1 ) )) {
                $data = base64_decode($data);
                $data = json_decode($data , true);

                if ($data['status'] == 'success'){
                    $id_order = (int)$data['order_id'];
                    $order = new Order($id_order);
                    if ($order->getCurrentState() != Configuration::get('PS_OS_PAYMENT')) {
                        $order->addOrderPayment($data['amount'], 'Оплата Liqpay.com', $data['liqpay_order_id']);
                        $order->setCurrentState(Configuration::get('PS_OS_PAYMENT'));
                        die('ok');
                    }
                }

            }
        }

        die('something wrong');

        parent::initContent();

        $this->setTemplate('validate.tpl');
    }

}
// POST DATA SAMPLE
//Array
//(
//    [action] => pay
//    [payment_id] => 130562208
//    [status] => wait_accept
//    [version] => 3
//    [type] => buy
//    [public_key] => i94093102520
//    [acq_id] => 414963
//    [order_id] => 00014
//    [liqpay_order_id] => ISN4B59Q1455198145560225
//    [description] => Order #00014
//    [sender_phone] => 380676122926
//    [sender_card_mask2] => 440588*70
//    [sender_card_bank] => pb
//    [sender_card_country] => 804
//    [ip] => 91.200.57.200
//    [amount] => 1
//    [currency] => UAH
//    [sender_commission] => 0
//    [receiver_commission] => 0.03
//    [agent_commission] => 0
//    [amount_debit] => 1
//    [amount_credit] => 1
//    [commission_debit] => 0
//    [commission_credit] => 0.03
//    [currency_debit] => UAH
//    [currency_credit] => UAH
//    [sender_bonus] => 0
//    [amount_bonus] => 0
//    [authcode_debit] => 400556
//    [rrn_debit] => 000323780119
//    [mpi_eci] => 7
//    [is_3ds] =>
//    [transaction_id] => 130562208
//)