<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{liqpay}prestashop>liqpay_7a12e10a2e4e5f76fbb19d5bf75bece3'] = 'Прием оплат через Liqpay';
$_MODULE['<{liqpay}prestashop>liqpay_fa214007826415a21a8456e3e09f999d'] = 'Внимание! Все настройки будут удалены. Продолжить?';
$_MODULE['<{liqpay}prestashop>liqpay_ee1facaa45ec35bb902dcbceb5272c2e'] = 'Ваш Liqpay аккаунт должен быть настроен правильно';
$_MODULE['<{liqpay}prestashop>liqpay_fe5d926454b6a8144efce13a44d019ba'] = 'Неправильная конфигурационная переменная';
$_MODULE['<{liqpay}prestashop>liqpay_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки обновлены';
$_MODULE['<{liqpay}prestashop>liqpay_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{liqpay}prestashop>liqpay_ed6445336472aef39084720adcf903b9'] = 'Публичный ключ';
$_MODULE['<{liqpay}prestashop>liqpay_6eb9040e470e8018db394832a528f56a'] = 'Приватный ключ';
$_MODULE['<{liqpay}prestashop>liqpay_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{liqpay}prestashop>liqpay_630f6dc397fe74e52d5189e2c80f282b'] = 'Назад к списку';
$_MODULE['<{liqpay}prestashop>redirect_c5f9ef2bd5a170624ee6531de649b1f8'] = 'Вы будете перенаправлены на сайт оплаты Liqpay';
$_MODULE['<{liqpay}prestashop>payment_e854382f12c76176536d2dbd0b47df19'] = 'Оплата через Liqpay';
