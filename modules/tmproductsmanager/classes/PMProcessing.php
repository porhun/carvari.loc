<?php
/**
 * 2002-2016 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2016 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMProcessing extends AdminController
{
    private $ids = [];
    protected $context;

    public function __construct($context, $ids)
    {
        $this->context = $context;
        $this->ids = Tools::jsonDecode($ids, true);
    }

    /************************************************* Information tab ************************************************/
    public function enableProducts(array $errors = [])
    {
        $status = (int)Tools::getValue('status');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->active = $status;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function availableOrder(array $errors = [])
    {
        $available_for_order = (int)Tools::getValue('available_for_order');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->available_for_order = $available_for_order;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function showPrice(array $errors = [])
    {
        $show_price = (int)Tools::getValue('show_price');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->show_price = $show_price;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function onlineOnly(array $errors = [])
    {
        $online_only = (int)Tools::getValue('online_only');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->online_only = $online_only;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function condition(array $errors = [])
    {
        $condition = Tools::getValue('condition');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->condition = $condition;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function description(array $errors = [])
    {
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                if (Tools::getValue('choose_place') == 'replace_description') {
                    foreach (Language::getLanguages(false) as $lang) {
                        $product->description[$lang['id_lang']] = Tools::getValue('description_'.$lang['id_lang']);
                    }
                } elseif (Tools::getValue('choose_place') == 'before_description') {
                    foreach (Language::getLanguages(false) as $lang) {
                        $product->description[$lang['id_lang']] = Tools::getValue(
                            'description_'.$lang['id_lang']
                        ).$product->description[$lang['id_lang']];
                    }
                } elseif (Tools::getValue('choose_place') == 'after_description') {
                    foreach (Language::getLanguages(false) as $lang) {
                        $product->description[$lang['id_lang']] = $product->description[$lang['id_lang']].Tools::getValue(
                            'description_'.$lang['id_lang']
                        );
                    }
                }
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function descriptionShort(array $errors = [])
    {
        if ($this->ids) {
            $limit = (int)Configuration::get('PS_PRODUCT_SHORT_DESC_LIMIT');
            if ($limit <= 0) {
                $limit = 400;
            }
            foreach (Language::getLanguages(false) as $lang) {
                if (Tools::strlen(strip_tags(Tools::getValue('description_short_'.$lang['id_lang']))) > $limit) {
                    $errors[] = sprintf($this->l('Description is too long in %s'), $lang['iso_code']);
                }
            }

            if (count($errors)) {
                return $errors;
            }

            foreach ($this->ids as $id) {
                $product = new Product($id);

                foreach (Language::getLanguages(false) as $lang) {
                    $product->description_short[$lang['id_lang']] = Tools::getValue(
                        'description_short_'.$lang['id_lang']
                    );
                }

                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function tags(array $errors = [])
    {
        if ($this->ids) {
            foreach ($this->ids as $id) {
                foreach (Language::getLanguages(false) as $language) {
                    if ($value = Tools::getValue('tags_'.$language['id_lang'])) {
                        if (!Tag::addTags($language['id_lang'], (int)$id, $value)) {
                            $errors[] = $this->l('Can\'t delete associations');
                        }
                    }
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }
    /********************************************* end of Information tab *********************************************/
    /********************************************* Price tab **********************************************************/
    public function onSale(array $errors = [])
    {
        $on_sale = Tools::getValue('on_sale');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->on_sale = $on_sale;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function taxRules(array $errors = [])
    {
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->id_tax_rules_group = (int)Tools::getValue('tax_rules');
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function specificPrice(array $errors = [])
    {
        $discount_price = Tools::getValue('discount_price');
        $reduction_type = Tools::getValue('reduction_type');
        $reduction_tax = Tools::getValue('reduction_tax');
        $from = Tools::getValue('data_start');
        $to = Tools::getValue('data_end');

        if (!$from) {
            $from = '0000-00-00 00:00:00';
        }

        if (!$to) {
            $to = '0000-00-00 00:00:00';
        }

        if (!Validate::isPrice($discount_price)) {
            $errors[] = $this->l('Invalid price field');
        } elseif (strtotime($to) < strtotime($from)) {
            $errors[] = $this->l('Invalid date range');
        } elseif ($from && $to && (!Validate::isDateFormat($from) || !Validate::isDateFormat($to))) {
            $errors[] = $this->l('The from/to date is invalid.');
        } elseif ($reduction_type == 'percentage' && ((float)$discount_price <= 0 || (float)$discount_price > 100)) {
            $errors[] = $this->l('Submitted reduction value (0-100) is out-of-range');
        } else {
            if ($this->ids) {
                foreach ($this->ids as $id) {
                    if (SpecificPrice::exists($id, 0, $this->context->shop->id, 0, 0, 0, 0, 1, $from, $to, false)) {
                        $errors[] = $this->l('A specific price already exists for these parameters.');
                    }
                    if (count($errors)) {
                        return $errors;
                    } else {
                        $specificPrice = new SpecificPrice();
                        $specificPrice->id_product = (int)$id;
                        $specificPrice->id_shop = $this->context->shop->id;
                        $specificPrice->id_currency = 0;
                        $specificPrice->id_country = 0;
                        $specificPrice->id_group = 0;
                        $specificPrice->id_customer = 0;
                        $specificPrice->from_quantity = 1;
                        $specificPrice->price = -1;
                        $specificPrice->reduction_type = $reduction_type;
                        $specificPrice->reduction_tax = $reduction_tax;
                        $specificPrice->from = $from;
                        $specificPrice->to = $to;
                        if ($reduction_type != 'amount') {
                            $specificPrice->reduction = $discount_price / 100;
                        } else {
                            $specificPrice->reduction = $discount_price;
                        }

                        if (!$specificPrice->add()) {
                            return $this->displayError($this->l('The specific price could not be added.'));
                        }
                    }
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }
    /********************************************* end of Price tab ***************************************************/
    /********************************************* Association tab ****************************************************/
    public function brand(array $errors = [])
    {
        $brand = (int)Tools::getValue('manufacturer');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->id_manufacturer = $brand;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function defaultCategory(array $errors = [])
    {
        $id_category_default = (int)Tools::getValue('id_parent');
        if (!$id_category_default) {
            return $this->l('No one category was selected');
        }
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->id_category_default = $id_category_default;
                if (!$product->update()) {
                    $errors[] = sprintf($this->l('Product %s wasn\'t saved'), $id);
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function categories(array $errors = [])
    {
        $method = Tools::getValue('method');
        $categories = Tools::getValue('id_parent');
        if ($this->ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                if ($method == 'replace') {
                    if (!$product->deleteCategories(true)) {
                        $errors[] = $this->l('Can\'t delete associations');
                    }
                }
                if (!$product->addToCategories($categories)) {
                    $errors[] = $this->l('Can\'t delete associations');
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }

    public function accessories()
    {
        $ids = array_filter(explode('-', Tools::getValue('inputAccessories')));
        if ($this->ids && $ids) {
            foreach ($this->ids as $id) {
                $product = new Product($id);
                $product->changeAccessories($ids);
            }
        }

        return false;
    }
    /************************************************* end of Association tab *****************************************/
    /*************************************************** Quantity tab *************************************************/
    public function quantity(array $errors = [])
    {
        $quantity = Tools::getValue('quantity');
        $action = Tools::getValue('method');
        if (Tools::isEmpty($quantity) || !Validate::isInt($quantity)) {
            $errors[] = $this->l('Bad quantity value');
        } else {
            if ($this->ids) {
                foreach ($this->ids as $id) {
                    $instocknow = StockAvailable::getQuantityAvailableByProduct($id, null, $this->context->shop->id);
                    if ($action == 'increase') {
                        $newquantity = $instocknow + $quantity;
                    } elseif ($action == 'decrease') {
                        if ($instocknow - $quantity < 0) {
                            $newquantity = 0;
                        } else {
                            $newquantity = $instocknow - $quantity;
                        }
                    } else {
                        $newquantity = $quantity;
                    }
                    if (StockAvailable::setQuantity($id, 0, $newquantity, $this->context->shop->id) === false) {
                        $errors[] = sprintf($this->l('Error occurred during product(%s) saving'), $id);
                    }
                }
            }
        }
        if (count($errors)) {
            return $errors;
        } else {
            return false;
        }
    }
    /**********************************************and of Quantity tab ************************************************/
}
