<?php
/**
 * 2002-2016 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2016 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

class PMForms extends AdminController
{
    /*
     * Main class that builds a forms on the last filter page
     */
    private $ids = [];
    protected $context;
    protected $helper;
    protected $tabInfo;

    public function __construct($context, $ids, $action, $info)
    {
        $this->context = $context;
        $this->ids = Tools::jsonDecode($ids, true);
        $this->helper = $this->buildHelper($action);
        $this->tabInfo = $info;
    }

    protected function buildHelper($action)
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = 'tmproductsmanager';
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitTMPM';
        $helper->currentIndex = $this->context->link->getAdminLink(
            'AdminTmProductsManager',
            false
        ).'&action='.$action.'&process';
        $helper->token = Tools::getAdminTokenLite('AdminTmProductsManager');
        $helper->tpl_vars = array(
            'fields_value' => false,
            'languages'    => $this->context->controller->getLanguages(),
            'id_language'  => $this->context->language->id
        );
        $helper->override_folder = '/';

        return $helper;
    }

    /********************************************* Information tab ***************************************************/
    public function enableProducts()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) status'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'status',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    )
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=enableProducts&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function availableOrder()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) available for order'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'available_for_order',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=availableOrder&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function showPrice()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->l('Show price selected products'),
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Show product(s) price'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'show_price',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=showPrice&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function onlineOnly()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) online only'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'online_only',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=onlineOnly&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function condition()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'name'    => 'condition',
                        'label'   => $this->l('Set product(s) condition'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id'   => 'new',
                                    'name' => $this->l('New')),
                                array(
                                    'id'   => 'used',
                                    'name' => $this->l('Used')),
                                array(
                                    'id'   => 'refurbished',
                                    'name' => $this->l('Refurbished'))
                            ),
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=condition&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function description()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'name'    => 'choose_place',
                        'label'   => $this->l('Choose the place for adding description'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id'   => 'before_description',
                                    'name' => $this->l('Before the main description')),
                                array(
                                    'id'   => 'after_description',
                                    'name' => $this->l('After the main description')),
                                array(
                                    'id'   => 'replace_description',
                                    'name' => $this->l('Replace the whole description'))
                            ),
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                    array(
                        'type'         => 'textarea',
                        'name'         => 'description',
                        'autoload_rte' => true,
                        'lang'         => true,
                        'label'        => $this->l('Enter description'),
                        'desc'         => $this->l('Description: "Changes will be applied to all listed products".')
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=description&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function descriptionShort()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'         => 'textarea',
                        'name'         => 'description_short',
                        'autoload_rte' => true,
                        'lang'         => true,
                        'label'        => $this->l('Set product(s) short description'),
                        'desc'         => $this->l('Selected short description will changed for all listed products.')
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=descriptionShort&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function tags()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'  => 'tags',
                        'name'  => 'tags',
                        'lang'  => true,
                        'label' => $this->l('Set product(s) tags'),
                        'desc'  => $this->l('Each tag has to be followed by a comma. The following characters are forbidden: !<;>;?=+#"°{}_$%.')
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=tags&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }
    /************************************************* end of Information tab *****************************************/
    /************************************************** Price tab *****************************************************/
    public function onSale()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'switch',
                        'label'   => $this->l('Set product(s) on sale'),
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'name'    => 'on_sale',
                        'is_bool' => true,
                        'values'  => array(
                            array(
                                'id'    => 'active_on',
                                'value' => true,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id'    => 'active_off',
                                'value' => false,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=onSale&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function taxRules(array $options = [])
    {
        $tax_rules_groups = TaxRulesGroup::getTaxRulesGroups(true);
        $options[] = array('id' => 0, 'name' => $this->l('No Tax'));
        if ($tax_rules_groups) {
            foreach ($tax_rules_groups as $tax_rules_group) {
                $options[] = array('id' => $tax_rules_group['id_tax_rules_group'], 'name' => $tax_rules_group['name']);
            }
        }
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Select tax rules'),
                        'name'    => 'tax_rules',
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => $options,
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=taxRules&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function specificPrice()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'  => 'datetime',
                        'label' => $this->l('Start Date'),
                        'name'  => 'data_start',
                        'col'   => 6,
                    ),
                    array(
                        'type'  => 'datetime',
                        'label' => $this->l('End Date'),
                        'name'  => 'data_end',
                        'col'   => 6,
                    ),
                    array(
                        'type'     => 'text',
                        'label'    => $this->l('Discount price'),
                        'col'      => 2,
                        'required' => true,
                        'name'     => 'discount_price',
                    ),
                    array(
                        'type'    => 'select',
                        'name'    => 'reduction_type',
                        'options' => array(
                            'id'    => 'id_option',
                            'name'  => 'name',
                            'query' => array(
                                array(
                                    'id_option' => 'amount',
                                    'name'      => 'amount'
                                ),
                                array(
                                    'id_option' => 'percentage',
                                    'name'      => '%'
                                ),
                            )
                        )
                    ),
                    array(
                        'type'    => 'select',
                        'name'    => 'reduction_tax',
                        'options' => array(
                            'id'    => 'id_option',
                            'name'  => 'name',
                            'query' => array(
                                array(
                                    'id_option' => 0,
                                    'name'      => 'Tax excluded'
                                ),
                                array(
                                    'id_option' => 1,
                                    'name'      => 'Tax included'
                                ),
                            )
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=specificPrice&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    /******************************************************* end of Price tab ****************************************/
    /***************************************************** Association tab *******************************************/
    public function brand(array $options = [])
    {
        $options[] = array('id' => '', 'name' => '');
        $manufacturers = Manufacturer::getManufacturers(false, 0, false);
        if ($manufacturers) {
            foreach ($manufacturers as $manufacturer) {
                $options[] = array('id' => $manufacturer['id_manufacturer'], 'name' => $manufacturer['name']);
            }
        }
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Select brand'),
                        'name'    => 'manufacturer',
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => $options,
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=brand&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function categories($multi = true)
    {
        $root = Category::getRootCategory();
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=categories&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        if ($multi) {
            $fields_form['form']['input'][] = array(
                'type'    => 'select',
                'label'   => $this->l('Select method'),
                'name'    => 'method',
                'options' => array(
                    'query' => array(
                        array(
                            'id' => 'add', 'name' => $this->l('Add to categories and save old relations')
                        ),
                        array(
                            'id' => 'replace', 'name' => $this->l('Add to categories and remove old relations')
                        ),
                    ),
                    'id'    => 'id',
                    'name'  => 'name'
                )
            );
        }
        $fields_form['form']['input'][] = array(
            'type'  => 'categories',
            'label' => $this->l('Choose categories'),
            'name'  => 'id_parent',
            'tree'  => array(
                'id'                  => 'categories-tree',
                'selected_categories' => array(),
                'disabled_categories' => null,
                'root_category'       => $root->id,
                'use_checkbox'        => $multi,
                'use_search'          => true,
            )
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }

    public function defaultCategory()
    {
        return $this->categories(false);
    }

    public function accessories()
    {
        $module = new Tmproductsmanager();
        $this->context->smarty->assign(
            'action',
            AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                'AdminTmProductsManager'
            ).'&action=accessories&process'
        );
        $this->context->smarty->assign(
            'back',
            AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                'AdminTmProductsManager'
            ).'&action=accessories&filter'
        );

        return $module->display($module->path, 'views/templates/admin/_forms/accessories.tpl');
    }
    /*************************************** end of Association tab ***************************************************/
    /**************************************** Quantity tab ************************************************************/
    public function quantity()
    {
        $fields_form = array(
            'form' => array(
                'legend'  => array(
                    'title' => $this->tabInfo['name'],
                    'icon'  => 'icon-cogs',
                ),
                'warning' => $this->l('Please note, that you won\'t be able to undo these changes the same way.'),
                'input'   => array(
                    array(
                        'type'    => 'select',
                        'label'   => $this->l('Select method'),
                        'name'    => 'method',
                        'desc'    => $this->l('Changes will be applied to all listed products.'),
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 'increase', 'name' => $this->l('Increase current quantity on')
                                ),
                                array(
                                    'id' => 'decrease', 'name' => $this->l('Decrease current quantity on')
                                ),
                                array(
                                    'id' => 'set', 'name' => $this->l('Set current quantity')
                                ),
                            ),
                            'id'    => 'id',
                            'name'  => 'name'
                        )
                    ),
                    array(
                        'type'  => 'text',
                        'label' => $this->l('Quantity'),
                        'name'  => 'quantity',
                        'desc'  => $this->l('Type a quantity value(only integer)'),
                        'col'   => 3
                    )
                ),
                'submit'  => array(
                    'title' => $this->l('Save'),
                ),
                'buttons' => array(
                    array(
                        'href'  => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite(
                            'AdminTmProductsManager'
                        ).'&action=quantity&filter',
                        'title' => $this->l('Back to filter'),
                        'icon'  => 'process-icon-back'
                    )
                )
            ),
        );
        $helper = $this->helper;

        return $helper->generateForm(array($fields_form));
    }
    /**************************************** end of Quantity tab *****************************************************/
}
