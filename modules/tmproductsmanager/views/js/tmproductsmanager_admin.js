/**
 * 2002-2016 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 *  @author    TemplateMonster
 *  @copyright 2002-2016 TemplateMonster
 *  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

$(document).ready(function() {
    tmproductsmanager.multiselectInit();
    $(document).on('change', 'input[name="categoryBox[]"]', function(e) {
      tmproductsmanager.updateCategoriesSelected();
    });
    $(document).on('click', '#check-all-categories_col, #uncheck-all-categories_col', function(e) {
      tmproductsmanager.updateCategoriesSelected();
    });
    if ($('#product_autocomplete_input').length) {
      tmproductsmanager.autocompleteInit($('#product_autocomplete_input'));
      $('#divAccessories').delegate('.delAccessory', 'click', function() {
        tmproductsmanager.delAccessory($(this).attr('name'));
      });
    }
    $('select#reduction_type').bind('change', function(e) {
      if ($('select#reduction_type').val() == 'percentage') {
        $('select#reduction_tax').hide();
      }
      else {
        $('select#reduction_tax').show();
      }
    }).trigger('change');
    $('.filter-tab li a').mouseenter(function(){
      var content = $(this).parents('li').find('div.tab-info').html();
      $(this).parents('.tab-pane').find('.tab-info-holder').html(content);
    }).mouseleave(function(){
      $(this).parents('.tab-pane').find('.tab-info-holder').html('');
    });
  }
);
tmproductsmanager = {
  updateCategoriesSelected : function() {
    var btn        = $('a#tmproductsmanagerSelectedCategories');
    var categories = $('input[name="categoryBox[]"]:checked');
    if (!categories.length) {
      btn.html('<span>' + tmproductsmanagerSelectedCategoriesEmptyText + '</span><b class="caret"></b>');
      return;
    } else {
      var categoriesCount = 0;
      categories.each(function() {
        categoriesCount++;
      });
    }
    btn.html('<span>' + categoriesCount + ' ' + tmproductsmanagerSelectedCategories + '</span><b class="caret"></b>');
  },
  multiselectInit          : function(element, name) {
    if (!$('.tmproductsmanager-form').length) {
      return;
    }
    var name = $(element).parents('.multiselect-box').find('.empty-text').attr('data-empty-text');
    $(element).multiselect({
      enableFiltering : false,
      includeSelectAllOption: true,
      nonSelectedText : name,
      selectAllText: tmproductsmanagerSelectAllText,
    });
  },
  autocompleteInit         : function(block) {
    block.autocomplete('ajax_products_list.php?exclude_packs=0&excludeVirtuals=0', {
      minChars      : 3,
      autoFill      : true,
      max           : 20,
      matchContains : true,
      mustMatch     : false,
      scroll        : false,
      cacheLength   : 0,
      formatItem    : function(item) {
        return item[1] + ' - ' + item[0];
      }
    }).result(this.addAccessory);
    block.setOptions({
      extraParams : {
        excludeIds : this.getAccessoriesIds()
      }
    });
  },
  getAccessoriesIds        : function() {
    return $('#inputAccessories').val().replace(/\-/g, ',');
  },
  addAccessory             : function(event, data, formatted) {
    if (data == null) {
      return false;
    }
    var productId         = data[1];
    var productName       = data[0];
    var $divAccessories   = $('#divAccessories');
    var $inputAccessories = $('#inputAccessories');
    var $nameAccessories  = $('#nameAccessories');
    /* delete product from select + add product line to the div, input_name, input_ids elements */
    $divAccessories.html($divAccessories.html() + '<div class="form-control-static"><button type="button" class="delAccessory btn btn-default" name="' + productId + '"><i class="icon-remove text-danger"></i></button>&nbsp;' + productName + '</div>');
    $nameAccessories.val($nameAccessories.val() + productName + '¤');
    $inputAccessories.val($inputAccessories.val() + productId + '-');
    $('#product_autocomplete_input').val('');
    $('#product_autocomplete_input').setOptions({
      extraParams : {excludeIds : tmproductsmanager.getAccessoriesIds()}
    });
  },
  delAccessory             : function(id) {
    var div      = getE('divAccessories');
    var input    = getE('inputAccessories');
    var name     = getE('nameAccessories');
    // Cut hidden fields in array
    var inputCut = input.value.split('-');
    var nameCut  = name.value.split('¤');
    if (inputCut.length != nameCut.length) {
      return jAlert('Bad size');
    }
    // Reset all hidden fields
    input.value   = '';
    name.value    = '';
    div.innerHTML = '';
    for (i in inputCut) {
      // If empty, error, next
      if (!inputCut[i] || !nameCut[i]) {
        continue;
      }
      // Add to hidden fields no selected products OR add to select field selected product
      if (inputCut[i] != id) {
        input.value += inputCut[i] + '-';
        name.value += nameCut[i] + '¤';
        div.innerHTML += '<div class="form-control-static"><button type="button" class="delAccessory btn btn-default" name="' + inputCut[i] + '"><i class="icon-remove text-danger"></i></button>&nbsp;' + nameCut[i] + '</div>';
      }
      else {
        $('#selectAccessories').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
      }
    }
    $('#product_autocomplete_input').setOptions({
      extraParams : {excludeIds : tmproductsmanager.getAccessoriesIds()}
    });
  }
}