{**
* 2002-2016 TemplateMonster
*
* TM Products Manager
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div class="botstrap">
  <div class="row">
    <div class="col-sm-12">
      <div class="filter-tab">
        <div class="panel">
          <h3>{l s='Choose section:' mod='tmproductsmanager'}</h3>
          <p>
            <small>{l s='Choose the general product property' mod='tmproductsmanager'}</small>
          </p>
          <ul class="nav">
            {foreach from=$tmpmtabs key=k item='tab'}
              <li {if $k == 0}class="active"{/if}>
                <a href="#tab{$k|escape:'htmlall':'UTF-8'}" data-toggle="tab">{$tab.name|escape:'htmlall':'UTF-8'}</a>
              </li>
            {/foreach}
          </ul>
        </div>
        <div class="panel">
          <h3>{l s='Choose option:' mod='tmproductsmanager'}</h3>
          <p>
            <small>{l s='Choose the option you want to change in current section' mod='tmproductsmanager'}</small>
          </p>
          <div class="tab-content">
            {foreach from=$tmpmtabs key=k item='tab'}
              <div class="row tab-pane{if $k == 0} active{/if}" id="tab{$k|escape:'htmlall':'UTF-8'}">
                <div class="col-sm-12 col-md-8">
                  <ul class="row">
                    {foreach from=$tab.tabs  key=event item='action'}
                      <li class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
                        <a class="{$action.icon|escape:'htmlall':'UTF-8'}" href="{$formUrl|escape:'html':'UTF-8'}&action={$event|escape:'htmlall':'UTF-8'}&filter"><span>{$action.name|escape:'quotes':'UTF-8'}</span></a>
                        {if (isset($action.desc) && $action.desc) || (isset($action.warning) && $action.warning)}
                          <div class="hidden tab-info">
                            {if isset($action.desc) && $action.desc}
                              <div class="alert alert-info">{$action.desc|escape:'javascript':'UTF-8'}</div>
                            {/if}
                            {if isset($action.warning) && $action.warning}
                              <div class="alert alert-warning">{$action.warning|escape:'javascript':'UTF-8'}</div>
                            {/if}
                          </div>
                        {/if}
                      </li>
                    {/foreach}
                  </ul>
                </div>
                <div class="col-sm-12 col-md-4 tab-info-holder"></div>
              </div>
            {/foreach}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

{addJsDefL name='tmproductsmanagerSelectedCategoriesEmptyText'}{l s='empty' mod='tmproductsmanager'}{/addJsDefL}
{addJsDefL name='tmproductsmanagerSelectedCategories'}{l s='selected' mod='tmproductsmanager'}{/addJsDefL}
{addJsDefL name='tmproductsmanagerSelectAllText'}{l s='Select all' mod='tmproductsmanager'}{/addJsDefL}