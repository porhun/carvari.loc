{**
* 2002-2016 TemplateMonster
*
* TM Products Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<ul class="items-list">
{foreach from=$items key=type item='item'}
  <li>
    {if isset($item['values']) && $item['values']}
      {include file='./_partials/list-active-item.tpl' inner=$type items=$item['values']}
    {else}
      <span class="type">
        {$item.name|escape:'htmlall':'UTF-8'} :
      </span>
      {foreach from=$item.value key=id name='value' item='value'}
        {if $smarty.foreach.value.iteration > 1}, {/if}
        <a href="{$formUrl|escape:'htmlall':'UTF-8'}&rmv={if isset($inner) && $inner}{$inner|escape:'htmlall':'UTF-8'}{else}{$type|escape:'htmlall':'UTF-8'}{/if}&itm={$id|escape:'htmlall':'UTF-8'}">{$value|escape:'htmlall':'UTF-8'} <i class="icon icon-remove"></i></a>
      {/foreach}
    {/if}
  </li>
{/foreach}
</ul>
