{**
* 2002-2016 TemplateMonster
*
* TM Products Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div class="multiselect-box col-xs-5 indent-bottom">
  <select id="multiselect-{$name|escape:'html':'UTF-8'}" class="multiselect-{$name|escape:'html':'UTF-8'}" multiple="multiple" name="tmpm_{$name|escape:'html':'UTF-8'}[]">
    {if $filter}
      {assign var="selected_name" value="tmpm_`$name`"}
      {foreach from=$filter item=item}
        <option {if isset($selected[$selected_name]) && $item.id|in_array:$selected[$selected_name]}selected="selected"{/if} value="{$item.id|escape:'htmlall':'UTF-8'}">{$item.name|escape:'htmlall':'UTF-8'}</option>
      {/foreach}
    {else}
      <optgroup label="{l s='nothing to select' mod='tmproductsmanager'}"></optgroup>
    {/if}
  </select>
  <div class="hidden empty-text" data-empty-text="{l s='empty' mod='tmproductsmanager'}"></div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    tmproductsmanager.multiselectInit($('#multiselect-{$name|escape:"html":"UTF-8"}'), '{$name|escape:"html":"UTF-8"}');
  });
</script>
