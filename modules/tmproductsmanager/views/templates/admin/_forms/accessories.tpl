{**
* 2002-2016 TemplateMonster
*
* TM Products Filter
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    TemplateMonster
*  @copyright 2002-2016 TemplateMonster
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*}

<div class="row">
  <form id="tmproductsmanager_form" class="defaultForm form-horizontal" action="{$action|escape:'html':'UTF-8'}" method="post" enctype="multipart/form-data" novalidate="">
    <input name="submitTMPM" value="1" type="hidden">
    <div id="fieldset_0" class="panel">
      <div class="panel-heading">
        <i class="icon-cogs"></i>
        {l s='Select accessories' mod='tmproductsmanager'}
      </div>
      <div class="form-wrapper">
        <div class="form-group">
          <label class="control-label col-lg-3" for="product_autocomplete_input">
            <span class="label-tooltip" data-toggle="tooltip"
                  title="{l s='You can indicate existing products as accessories for this product.'  mod='tmproductsmanager'}{l s='Start by typing the first letters of the product\'s name, then select the product from the drop-down list.' mod='tmproductsmanager'}{l s='Do not forget to save the product afterwards!' mod='tmproductsmanager'}">
			{l s='Accessories' mod='tmproductsmanager'}
			      </span>
          </label>
          <div class="col-lg-5">
            <input id="inputAccessories" name="inputAccessories" value="-" type="hidden">
            <input id="nameAccessories" name="nameAccessories" value="¤" type="hidden">
            <div id="ajax_choose_product">
              <div class="input-group">
                <input type="text" id="product_autocomplete_input" name="product_autocomplete_input"/>
                <span class="input-group-addon"><i class="icon-search"></i></span>
              </div>
            </div>
            <p class="help-block">{l s='Start typing the product name and then select it it from the dropdown list.' mod='tmproductmanager'}</p>
            <div id="divAccessories">
            </div>
          </div>
        </div>
      </div>
      <div class="panel-footer">
        <button id="tmproductsmanager_form_submit_btn" class="btn btn-default pull-right" type="submit" value="1" name="submitTMPM">
          <i class="process-icon-save"></i>
          {l s='Save' mod='tmproductsmanager'}
        </button>
        <a class="btn btn-default" href="{$back|escape:'html':'UTF-8'}">
          <i class="process-icon-back"></i>
          {l s='Back to filter' mod='tmproductsmanager'}
        </a>
      </div>
    </div>
  </form>
</div>