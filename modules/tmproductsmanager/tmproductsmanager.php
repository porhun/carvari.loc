<?php
/**
 * 2002-2016 TemplateMonster
 *
 * TM Products Manager
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the General Public License (GPL 2.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/GPL-2.0
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the module to newer
 * versions in the future.
 *
 * @author    TemplateMonster
 * @copyright 2002-2016 TemplateMonster
 * @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
 */

if (!defined('_PS_VERSION_')) {
    exit;
}
include_once(dirname(__FILE__).'/classes/PMFilters.php');
include_once(dirname(__FILE__).'/classes/PMFilterProcess.php');
include_once(dirname(__FILE__).'/classes/PMProcessing.php');
include_once(dirname(__FILE__).'/classes/PMForms.php');
include_once(dirname(__FILE__).'/classes/PMHistory.php');

class Tmproductsmanager extends Module
{
    public $path;

    public function __construct()
    {
        $this->name = 'tmproductsmanager';
        $this->tab = 'smart_shopping';
        $this->version = '0.0.1';
        $this->author = 'TemplateMonster';
        $this->need_instance = 1;
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('TM Products Manager');
        $this->description = $this->l('Manage your products easy, by using needed parameters');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->languages = Language::getLanguages(false);
        $this->path = $this->local_path;
    }

    public function install()
    {
        include(dirname(__FILE__).'/sql/install.php');

        return parent::install() &&
        $this->addTab();
    }

    public function uninstall()
    {
        include(dirname(__FILE__).'/sql/uninstall.php');

        return $this->removeTab() &&
        parent::uninstall();
    }

    protected function addTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminTmProductsManager';
        $tab->id_parent = (int)Tab::getIdFromClassName('AdminCatalog');
        $tab->module = $this->name;
        foreach ($this->languages as $lang) {
            $tab->name[$lang['id_lang']] = 'TM Products Manager';
        }
        if (!$tab->save()) {
            return false;
        }

        return true;
    }

    protected function removeTab()
    {
        if ($id_tab = (int)Tab::getIdFromClassName('AdminTmProductsManager')) {
            $tab = new Tab($id_tab);
            if (!$tab->delete()) {
                return false;
            }
        }

        return true;
    }

    public function displayRemoveLink($token, $id)
    {
        $this->context->smarty->assign(
            array(
                'href'   => AdminController::$currentIndex.'&id_product='.$id.'&removetmpm&token='.($token != null ? $token : $this->token),
                'action' => $this->l('Remove from list'),
                'id'     => $id
            )
        );

        return $this->display($this->_path, 'views/templates/admin/_configure/helpers/list/list_action_remove.tpl');
    }
}
