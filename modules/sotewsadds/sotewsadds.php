<?php


class sotewsadds extends Module {
	
	private $_html = '';
	function __construct() {
		$this->name = 'sotewsadds';
		$this->tab = 'Advertisement';
		$this->version = '0.4';

		parent::__construct();

		$this->page = basename(__FILE__, '.php');
        $this->displayName = $this->l('SotEW\'s Adds');
        $this->description = $this->l('Displays advertising files');

//        Configuration::updateValue('PS_SA_LinkPromoBanner7', '');
//        Configuration::updateValue('PS_SA_NewTabPromoBanner7', 'checked');
//        Configuration::updateValue('PS_SA_TypePromoBanner7', 0);
//        Configuration::updateValue('PS_SA_WidthPromoBanner7', 800);
//        Configuration::updateValue('PS_SA_HeightPromoBanner7', 190);
//        Configuration::updateValue('PS_SA_DisplayPromoBanner7', 'checked');
//
//        Configuration::updateValue('PS_SA_LinkPromoBanner8', '');
//        Configuration::updateValue('PS_SA_NewTabPromoBanner8', 'checked');
//        Configuration::updateValue('PS_SA_TypePromoBanner8', 0);
//        Configuration::updateValue('PS_SA_WidthPromoBanner8', 800);
//        Configuration::updateValue('PS_SA_HeightPromoBanner8', 190);
//        Configuration::updateValue('PS_SA_DisplayPromoBanner8', 'checked');
	}

	function install() {
        if (parent::install() == false 
				OR $this->registerHook('displayAdvantage1') == false
				OR $this->registerHook('displayAdvantage2') == false
				OR $this->registerHook('displayAdvantage3') == false
				OR $this->registerHook('displayAdvantage4') == false
				OR $this->registerHook('displayPromoBanner1') == false
				OR $this->registerHook('displayPromoBanner2') == false
				OR $this->registerHook('displayPromoBanner3') == false
				OR $this->registerHook('displayPromoBanner4') == false
				OR $this->registerHook('displayPromoBanner5') == false
				OR $this->registerHook('displayPromoBanner6') == false
				OR $this->registerHook('displayPromoBanner7') == false
				OR $this->registerHook('displayPromoBanner8') == false

				OR Configuration::updateValue('PS_SA_LinkLeft', '') == false
				OR Configuration::updateValue('PS_SA_NewTabLeft', 'checked') == false
				OR Configuration::updateValue('PS_SA_TypeLeft', 0) == false
				OR Configuration::updateValue('PS_SA_WidthLeft', 190) == false
				OR Configuration::updateValue('PS_SA_HeightLeft', 190) == false
				OR Configuration::updateValue('PS_SA_DisplayLeft', 'checked') == false
				OR Configuration::updateValue('PS_SA_DisplayBlockLeft', 'uncheked') == false
				
				OR Configuration::updateValue('PS_SA_LinkRight', '') == false
				OR Configuration::updateValue('PS_SA_NewTabRight', 'checked') == false
				OR Configuration::updateValue('PS_SA_TypeRight', 0) == false
				OR Configuration::updateValue('PS_SA_WidthRight', 190) == false
				OR Configuration::updateValue('PS_SA_HeightRight', 190) == false
				OR Configuration::updateValue('PS_SA_DisplayRight', 'checked') == false
				OR Configuration::updateValue('PS_SA_DisplayBlockRight', 'uncheked') == false
				
				OR Configuration::updateValue('PS_SA_LinkHome', '') == false
				OR Configuration::updateValue('PS_SA_NewTabHome', 'checked') == false
				OR Configuration::updateValue('PS_SA_TypeHome', 0) == false
				OR Configuration::updateValue('PS_SA_WidthHome', 500) == false
				OR Configuration::updateValue('PS_SA_HeightHome', 190) == false
				OR Configuration::updateValue('PS_SA_DisplayHome', 'checked') == false
				
				OR Configuration::updateValue('PS_SA_LinkHeader', '') == false
				OR Configuration::updateValue('PS_SA_NewTabHeader', 'checked') == false
				OR Configuration::updateValue('PS_SA_TypeHeader', 0) == false
				OR Configuration::updateValue('PS_SA_WidthHeader', 800) == false
				OR Configuration::updateValue('PS_SA_HeightHeader', 190) == false
				OR Configuration::updateValue('PS_SA_DisplayHeader', 'checked') == false
				
				OR Configuration::updateValue('PS_SA_LinkFooter', '') == false
				OR Configuration::updateValue('PS_SA_NewTabFooter', 'checked') == false
				OR Configuration::updateValue('PS_SA_TypeFooter', 0) == false
				OR Configuration::updateValue('PS_SA_WidthFooter', 800) == false
				OR Configuration::updateValue('PS_SA_HeightFooter', 190) == false
				OR Configuration::updateValue('PS_SA_DisplayFooter', 'checked') == false
				
				OR Configuration::updateValue('PS_SA_LinkTop', '') == false
				OR Configuration::updateValue('PS_SA_NewTabTop', 'checked') == false
				OR Configuration::updateValue('PS_SA_TypeTop', 0) == false
				OR Configuration::updateValue('PS_SA_WidthTop', 600) == false
				OR Configuration::updateValue('PS_SA_HeightTop', 190) == false
				OR Configuration::updateValue('PS_SA_DisplayTop', 'checked') == false

                OR Configuration::updateValue('PS_SA_LinkPromoBanner3', '') == false
                OR Configuration::updateValue('PS_SA_NewTabPromoBanner3', 'checked') == false
                OR Configuration::updateValue('PS_SA_TypePromoBanner3', 0) == false
                OR Configuration::updateValue('PS_SA_WidthPromoBanner3', 800) == false
                OR Configuration::updateValue('PS_SA_HeightPromoBanner3', 190) == false
                OR Configuration::updateValue('PS_SA_DisplayPromoBanner3', 'checked') == false

                OR Configuration::updateValue('PS_SA_LinkPromoBanner4', '') == false
                OR Configuration::updateValue('PS_SA_NewTabPromoBanner4', 'checked') == false
                OR Configuration::updateValue('PS_SA_TypePromoBanner4', 0) == false
                OR Configuration::updateValue('PS_SA_WidthPromoBanner4', 800) == false
                OR Configuration::updateValue('PS_SA_HeightPromoBanner4', 190) == false
                OR Configuration::updateValue('PS_SA_DisplayPromoBanner4', 'checked') == false

                OR Configuration::updateValue('PS_SA_LinkPromoBanner5', '') == false
                OR Configuration::updateValue('PS_SA_NewTabPromoBanner5', 'checked') == false
                OR Configuration::updateValue('PS_SA_TypePromoBanner5', 0) == false
                OR Configuration::updateValue('PS_SA_WidthPromoBanner5', 800) == false
                OR Configuration::updateValue('PS_SA_HeightPromoBanner5', 190) == false
                OR Configuration::updateValue('PS_SA_DisplayPromoBanner5', 'checked') == false

                OR Configuration::updateValue('PS_SA_LinkPromoBanner6', '') == false
                OR Configuration::updateValue('PS_SA_NewTabPromoBanner6', 'checked') == false
                OR Configuration::updateValue('PS_SA_TypePromoBanner6', 0) == false
                OR Configuration::updateValue('PS_SA_WidthPromoBanner6', 800) == false
                OR Configuration::updateValue('PS_SA_HeightPromoBanner6', 190) == false
                OR Configuration::updateValue('PS_SA_DisplayPromoBanner6', 'checked') == false

                OR Configuration::updateValue('PS_SA_LinkPromoBanner7', '') == false
                OR Configuration::updateValue('PS_SA_NewTabPromoBanner7', 'checked') == false
                OR Configuration::updateValue('PS_SA_TypePromoBanner7', 0) == false
                OR Configuration::updateValue('PS_SA_WidthPromoBanner7', 800) == false
                OR Configuration::updateValue('PS_SA_HeightPromoBanner7', 190) == false
                OR Configuration::updateValue('PS_SA_DisplayPromoBanner7', 'checked') == false

                OR Configuration::updateValue('PS_SA_LinkPromoBanner8', '') == false
                OR Configuration::updateValue('PS_SA_NewTabPromoBanner8', 'checked') == false
                OR Configuration::updateValue('PS_SA_TypePromoBanner8', 0) == false
                OR Configuration::updateValue('PS_SA_WidthPromoBanner8', 800) == false
                OR Configuration::updateValue('PS_SA_HeightPromoBanner8', 190) == false
                OR Configuration::updateValue('PS_SA_DisplayPromoBanner8', 'checked') == false
			)
			return false;
		return true;
		
    }
//	function install() {
//        if (parent::install() == false
//				OR $this->registerHook('leftColumn') == false
//				OR $this->registerHook('rightColumn') == false
//				OR $this->registerHook('footer') == false
//				OR $this->registerHook('header') == false
//				OR $this->registerHook('home') == false
//				OR $this->registerHook('top') == false
//
//				OR Configuration::updateValue('PS_SA_LinkLeft', '') == false
//				OR Configuration::updateValue('PS_SA_NewTabLeft', 'checked') == false
//				OR Configuration::updateValue('PS_SA_TypeLeft', 0) == false
//				OR Configuration::updateValue('PS_SA_WidthLeft', 190) == false
//				OR Configuration::updateValue('PS_SA_HeightLeft', 190) == false
//				OR Configuration::updateValue('PS_SA_DisplayLeft', 'checked') == false
//				OR Configuration::updateValue('PS_SA_DisplayBlockLeft', 'uncheked') == false
//
//				OR Configuration::updateValue('PS_SA_LinkRight', '') == false
//				OR Configuration::updateValue('PS_SA_NewTabRight', 'checked') == false
//				OR Configuration::updateValue('PS_SA_TypeRight', 0) == false
//				OR Configuration::updateValue('PS_SA_WidthRight', 190) == false
//				OR Configuration::updateValue('PS_SA_HeightRight', 190) == false
//				OR Configuration::updateValue('PS_SA_DisplayRight', 'checked') == false
//				OR Configuration::updateValue('PS_SA_DisplayBlockRight', 'uncheked') == false
//
//				OR Configuration::updateValue('PS_SA_LinkHome', '') == false
//				OR Configuration::updateValue('PS_SA_NewTabHome', 'checked') == false
//				OR Configuration::updateValue('PS_SA_TypeHome', 0) == false
//				OR Configuration::updateValue('PS_SA_WidthHome', 500) == false
//				OR Configuration::updateValue('PS_SA_HeightHome', 190) == false
//				OR Configuration::updateValue('PS_SA_DisplayHome', 'checked') == false
//
//				OR Configuration::updateValue('PS_SA_LinkHeader', '') == false
//				OR Configuration::updateValue('PS_SA_NewTabHeader', 'checked') == false
//				OR Configuration::updateValue('PS_SA_TypeHeader', 0) == false
//				OR Configuration::updateValue('PS_SA_WidthHeader', 800) == false
//				OR Configuration::updateValue('PS_SA_HeightHeader', 190) == false
//				OR Configuration::updateValue('PS_SA_DisplayHeader', 'checked') == false
//
//				OR Configuration::updateValue('PS_SA_LinkFooter', '') == false
//				OR Configuration::updateValue('PS_SA_NewTabFooter', 'checked') == false
//				OR Configuration::updateValue('PS_SA_TypeFooter', 0) == false
//				OR Configuration::updateValue('PS_SA_WidthFooter', 800) == false
//				OR Configuration::updateValue('PS_SA_HeightFooter', 190) == false
//				OR Configuration::updateValue('PS_SA_DisplayFooter', 'checked') == false
//
//				OR Configuration::updateValue('PS_SA_LinkTop', '') == false
//				OR Configuration::updateValue('PS_SA_NewTabTop', 'checked') == false
//				OR Configuration::updateValue('PS_SA_TypeTop', 0) == false
//				OR Configuration::updateValue('PS_SA_WidthTop', 600) == false
//				OR Configuration::updateValue('PS_SA_HeightTop', 190) == false
//				OR Configuration::updateValue('PS_SA_DisplayTop', 'checked') == false
//			)
//			return false;
//		return true;
//
//    }
//
	function uninstall() {
		$racine = '../modules/sotewsadds/files/';
		echo $racine;
		
		Configuration::deleteByName('PS_SA_LinkLeft');
		Configuration::deleteByName('PS_SA_NewTabLeft');
		Configuration::deleteByName('PS_SA_TypeLeft');
		Configuration::deleteByName('PS_SA_WidthLeft');
		Configuration::deleteByName('PS_SA_HeightLeft');
		Configuration::deleteByName('PS_SA_DisplayLeft');
		Configuration::deleteByName('PS_SA_DisplayBlockLeft');
		if (file_exists($racine . 'Left.html')) {unlink($racine . 'Left.html');}
		if (file_exists($racine . 'Left')) {unlink($racine . 'Left');}
		
		Configuration::deleteByName('PS_SA_LinkRight');
		Configuration::deleteByName('PS_SA_NewTabRight');
		Configuration::deleteByName('PS_SA_TypeRight');
		Configuration::deleteByName('PS_SA_WidthRight');
		Configuration::deleteByName('PS_SA_HeightRight');
		Configuration::deleteByName('PS_SA_DisplayRight');
		Configuration::deleteByName('PS_SA_DisplayBlockRight');
		if (file_exists($racine . 'Right.html')) {unlink($racine . 'Right.html');}
		if (file_exists($racine . 'Right')) {unlink($racine . 'Right');}
		
		Configuration::deleteByName('PS_SA_LinkHome');
		Configuration::deleteByName('PS_SA_NewTabHome');
		Configuration::deleteByName('PS_SA_TypeHome');
		Configuration::deleteByName('PS_SA_WidthHome');
		Configuration::deleteByName('PS_SA_HeightHome');
		Configuration::deleteByName('PS_SA_DisplayHome');
		if (file_exists($racine . 'Home.html')) {unlink($racine . 'Home.html');}
		if (file_exists($racine . 'Home')) {unlink($racine . 'Home');}
		
		Configuration::deleteByName('PS_SA_LinkHeader');
		Configuration::deleteByName('PS_SA_NewTabHeader');
		Configuration::deleteByName('PS_SA_TypeHeader');
		Configuration::deleteByName('PS_SA_WidthHeader');
		Configuration::deleteByName('PS_SA_HeightHeader');
		Configuration::deleteByName('PS_SA_DisplayHeader');
		if (file_exists($racine . 'Header.html')) {unlink($racine . 'Header.html');}
		if (file_exists($racine . 'Header')) {unlink($racine . 'Header');}
		
		Configuration::deleteByName('PS_SA_LinkFooter');
		Configuration::deleteByName('PS_SA_NewTabFooter');
		Configuration::deleteByName('PS_SA_TypeFooter');
		Configuration::deleteByName('PS_SA_WidthFooter');
		Configuration::deleteByName('PS_SA_HeightFooter');
		Configuration::deleteByName('PS_SA_DisplayFooter');
		if (file_exists($racine . 'Footer.html')) {unlink($racine . 'Footer.html');}
		if (file_exists($racine . 'Footer')) {unlink($racine . 'Footer');}
		
		Configuration::deleteByName('PS_SA_LinkTop');
		Configuration::deleteByName('PS_SA_NewTabTop');
		Configuration::deleteByName('PS_SA_TypeTop');
		Configuration::deleteByName('PS_SA_WidthTop');
		Configuration::deleteByName('PS_SA_HeightTop');
		Configuration::deleteByName('PS_SA_DisplayTop');
		if (file_exists($racine . 'Top.html')) {unlink($racine . 'Top.html');}
		if (file_exists($racine . 'Top')) {unlink($racine . 'Top');}
		return parent::uninstall();
	}

	function hookDisplayAdvantage1($params) {
        if( isset($params['img']) ) {
            $file = $this->_path.'files/Left';
            $content = '<img src="' . $file .'" />';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextLeft');
//		if (Configuration::get('PS_SA_DisplayLeft') == 'checked' && (file_exists(dirname(__FILE__).'/files/Left') || !empty($text))) {
			global $smarty;
			$newTabLeft = Configuration::get('PS_SA_newTabLeft');
			$type = Configuration::get('PS_SA_TypeLeft');
			$file = $this->_path.'files/Left';
			if ($type == 0) {
				$content = '<img src="' . $file .'" />';
				$linkLeft = Configuration::get('PS_SA_LinkLeft');
				if ($linkLeft != '') {
					if ($newTabLeft == 'checked') { 
						$linkLeft .= '" target="_new'; 
					}
					$content = '<a href="' . $linkLeft .'" class="box_220 fleft mr19">' . $content .'</a>';
				}
				
			} else if ($type == 1) {
				$width = Configuration::get('PS_SA_WidthLeft');
				$height = Configuration::get('PS_SA_HeightLeft');
				$content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
			} else {
//				$content = '';
//				if ($htmlFile = @fopen(dirname(__FILE__) . '/files/Left.html', "r")) {
//					while (!feof($htmlFile)) {
//						$content .= fgets($htmlFile, 4096);
//					}
//				}
//				@fclose($htmlFile);
                $content = Configuration::get('PS_SA_html');
//			}
			$smarty->assign('content', $content);
			$smarty->assign('text', $text);
			if (Configuration::get('PS_SA_DisplayBlockLeft') == 'checked') {
				$smarty->assign('withBlockLeft', true);
			}
			return $this->display(__FILE__, 'advantage1.tpl');
		}
		return '';
	}
	
	function hookDisplayAdvantage2($params) {
        if( isset($params['img']) ) {
            $file = $this->_path.'files/Right';
            $content = '<img src="' . $file .'" />';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextRight');
//		if (Configuration::get('PS_SA_DisplayRight') == 'checked' && (file_exists(dirname(__FILE__).'/files/Right') || !empty($text))) {
			global $smarty;
			$newTabRight = Configuration::get('PS_SA_newTabRight');
			$type = Configuration::get('PS_SA_TypeRight');
			$file = $this->_path.'files/Right';
			if ($type == 0) {
				$content = '<img src="' . $file .'" />';
				$linkRight = Configuration::get('PS_SA_LinkRight');
				if ($linkRight != '') {
					if ($newTabRight == 'checked') { 
						$linkRight .= '" target="_new'; 
					}
					$content = '<a href="' . $linkRight .'" class="box_220 fleft mr19">' . $content .'</a>';
				}
				
			} else if ($type == 1) {
				$width = Configuration::get('PS_SA_WidthRight');
				$height = Configuration::get('PS_SA_HeightRight');
				$content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
			} else {
//				$content = '';
//				if ($htmlFile = @fopen(dirname(__FILE__) . '/files/Right.html', "r")) {
//					while (!feof($htmlFile)) {
//						$content .= fgets($htmlFile, 4096);
//					}
//				}
//				@fclose($htmlFile);
                $content = Configuration::get('PS_SA_htmlRight');
			}
			$smarty->assign('content', $content);
			$smarty->assign('text', $text);
			if (Configuration::get('PS_SA_DisplayBlockRight') == 'checked') {
				$smarty->assign('withBlockRight', true);
			}
			return $this->display(__FILE__, 'advantage2.tpl');
//		}
		return '';
	}
	
	function hookDisplayAdvantage3($params) {

        if( isset($params['img']) ) {
            $file = $this->_path.'files/Top';
            $content = '<img src="' . $file .'" />';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextTop');
//		if (Configuration::get('PS_SA_DisplayTop') == 'checked' && (file_exists(dirname(__FILE__).'/files/Top') || !empty($text))) {
			global $smarty;
			$newTabHome = Configuration::get('PS_SA_newTabTop');
			$type = Configuration::get('PS_SA_TypeTop');
			$file = $this->_path.'files/Top';
			if ($type == 0) {
				$content = '<img src="' . $file .'" />';
				$linkHome = Configuration::get('PS_SA_LinkTop');
				if ($linkHome != '') {
					if ($newTabHome == 'checked') { 
						$linkHome .= '" target="_new'; 
					}
					$content = '<a href="' . $linkHome .'" class="box_220 fleft mr20">' . $content .'</a>';
				}
				
			} else if ($type == 1) {
				$width = Configuration::get('PS_SA_WidthHome');
				$height = Configuration::get('PS_SA_HeightHome');
				$content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
			} else {
//				$content = '';
//				if ($htmlFile = @fopen(dirname(__FILE__) . '/files/Top.html', "r")) {
//					while (!feof($htmlFile)) {
//						$content .= fgets($htmlFile, 4096);
//					}
//				}
//				@fclose($htmlFile);
                $content = Configuration::get('PS_SA_htmlTop');
			}
			$smarty->assign('content', $content);
			$smarty->assign('text', $text);
			return $this->display(__FILE__, 'advantage3.tpl');
//		}

        return '';
	}
	
	function hookDisplayAdvantage4($params) {
        if( isset($params['img']) ) {
            $file = $this->_path.'files/Home';
            $content = '<img src="' . $file .'" alt=""/>';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextHome');
//		if (Configuration::get('PS_SA_DisplayHome') == 'checked' && (file_exists(dirname(__FILE__).'/files/Home') || !empty($text))) {
			global $smarty;
			$newTabHeader = Configuration::get('PS_SA_newTabHome');
			$type = Configuration::get('PS_SA_TypeHome');
			$file = $this->_path.'files/Home';

			if ($type == 0) {
				$content = '<img src="' . $file .'"  alt=""/>';
				$linkHeader = Configuration::get('PS_SA_LinkHome');
				if ($linkHeader != '') {
					if ($newTabHeader == 'checked') { 
						$linkHeader .= '" target="_new'; 
					}
					$content = '<a href="' . $linkHeader .'" class="block">' . $content .'</a>';
				}
				
			} else if ($type == 1) {
				$width = Configuration::get('PS_SA_WidthHeader');
				$height = Configuration::get('PS_SA_HeightHeader');
				$content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
			} else {
//				$content = '';
//				if ($htmlFile = @fopen(dirname(__FILE__) . '/files/Home.html', "r")) {
//					while (!feof($htmlFile)) {
//						$content .= fgets($htmlFile, 4096);
//					}
//				}
//				@fclose($htmlFile);
                $content = Configuration::get('PS_SA_htmlHome');
			}
			$smarty->assign('content', $content);
			$smarty->assign('text', $text);
			return $this->display(__FILE__, 'advantage4.tpl');
//		}
		return '';
	}
	
	function hookDisplayPromoBanner1($params) {

        $content = '';
        if( isset($params['img']) ) {
            $file = $this->_path.'files/Header';
//            $content = '<img src="' . $file .'" />';
//            return $content;
        }
        $text =  Configuration::get('PS_SA_TextHeader');
        $linkFooter = '';
        $linkTarget1 = '';
//		if (Configuration::get('PS_SA_DisplayFooter') == 'checked' && (file_exists(dirname(__FILE__).'/files/Header') || !empty($text))) {
			global $smarty;
			$newTabFooter = Configuration::get('PS_SA_newTabHeader');
			$type = Configuration::get('PS_SA_TypeHeader');
			$file = $this->_path.'files/Header';
			if ($type == 0) {
				$linkFooter = Configuration::get('PS_SA_LinkHeader');
				if ($linkFooter != '') {
                    $linkTarget = '';
                    if ($newTabFooter == 'checked') {
						$linkTarget .=' target="_blank"';
					}
                    $smarty->assign('linkTarget1', $linkTarget);
				}
			} else if ($type == 1) {
				$width = Configuration::get('PS_SA_WidthFooter');
				$height = Configuration::get('PS_SA_HeightFooter');
				$content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
			} else {
				$content = Configuration::get('PS_SA_htmlHeader');
//				if ($htmlFile = @fopen(dirname(__FILE__) . '/files/Header.html', "r")) {
//					while (!feof($htmlFile)) {
//						$content .= fgets($htmlFile, 4096);
//					}
//				}
//				@fclose($htmlFile);
			}
            $smarty->assign('content1', $content);
            $smarty->assign('promolink1', $linkFooter);
            $smarty->assign('promofile1', $file);
            $smarty->assign('text', $text);
            return $this->display(__FILE__, 'promoBanner1.tpl');
//		}
		return '';
	}
	
	function hookDisplayPromoBanner2($params) {
        $content = '';
        if( isset($params['img']) ) {
            $file = $this->_path.'files/Footer';
//            $content = '<img src="' . $file .'" />';
//            return $content;
        }
        $text =  Configuration::get('PS_SA_TextFooter');
        $linkTop =  '';
//		if (Configuration::get('PS_SA_DisplayFooter') == 'checked' && (file_exists(dirname(__FILE__).'/files/Footer') || !empty($text))) {
			global $smarty;
			$newTabTop = Configuration::get('PS_SA_newTabFooter');
			$type = Configuration::get('PS_SA_TypeFooter');
			$file = $this->_path.'files/Footer';
			if ($type == 0) {
				$linkTop = Configuration::get('PS_SA_LinkFooter');
				if ($linkTop != '') {
                    $linkTarget = '';
                    if ($newTabTop == 'checked') {
                        $linkTarget .= ' target="_blank"';
					}
                    $smarty->assign('linkTarget2', $linkTarget);
				}

			} else if ($type == 1) {
				$width = Configuration::get('PS_SA_WidthTop');
				$height = Configuration::get('PS_SA_HeightTop');
				$content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
            } else {
                $content = Configuration::get('PS_SA_htmlFooter');
//				if ($htmlFile = @fopen(dirname(__FILE__) . '/files/Footer.html', "r")) {
//					while (!feof($htmlFile)) {
//						$content .= fgets($htmlFile, 4096);
//					}
//				}
//				@fclose($htmlFile);
            }
            $smarty->assign('content2', $content);
            $smarty->assign('text', $text);
            $smarty->assign('promolink2', $linkTop);
            $smarty->assign('promofile2', $file);
			return $this->display(__FILE__, 'promoBanner2.tpl');
//		}
		return '';
	}

    function hookDisplayPromoBanner3($params) {
        $content = '';
        if( isset($params['img']) ) {
            $file = $this->_path.'files/PromoBanner3';
//            $content = '<img src="' . $file .'" />';
//            return $content;
        }
        $text =  Configuration::get('PS_SA_TextPromoBanner3');
        $linkTop = '';
            global $smarty;
            $newTabTop = Configuration::get('PS_SA_NewTabPromoBanner3');
            $type = Configuration::get('PS_SA_TypePromoBanner3');
            $file = $this->_path.'files/PromoBanner3';
            if ($type == 0) {
                $linkTop = Configuration::get('PS_SA_LinkPromoBanner3');
                if ($linkTop != '') {
                    $linkTarget = '';
                    if ($newTabTop == 'checked') {
                        $linkTarget .= ' target="_blank"';
                    }
                    $smarty->assign('linkTarget3', $linkTarget);
                }

            } else if ($type == 1) {
                $width = Configuration::get('PS_SA_WidthTop');
                $height = Configuration::get('PS_SA_HeightTop');
                $content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
            } else {
                $content = Configuration::get('PS_SA_htmlPromoBanner3');
//                if ($htmlFile = @fopen(dirname(__FILE__) . '/files/PromoBanner3.html', "r")) {
//                    while (!feof($htmlFile)) {
//                        $content .= fgets($htmlFile, 4096);
//                    }
//                }
//                @fclose($htmlFile);
            }
            $smarty->assign('content3', $content);
            $smarty->assign('promolink3', $linkTop);
            $smarty->assign('promofile3', $file);
            $smarty->assign('text', $text);
            return $this->display(__FILE__, 'promoBanner3.tpl');
//        }
        return '';
    }

    function hookDisplayPromoBanner4($params) {
        $content = '';
        if( isset($params['img']) ) {
            $file = $this->_path.'files/PromoBanner4';
            //$content = '<img src="' . $file .'" />';
            //return $content;
        }
        $text =  Configuration::get('PS_SA_TextPromoBanner4');
        $linkTop = '';
//        if (Configuration::get('PS_SA_DisplayPromoBanner4') == 'checked' && (file_exists(dirname(__FILE__).'/files/PromoBanner4') || !empty($text))) {
            global $smarty;
            $newTabTop = Configuration::get('PS_SA_NewTabPromoBanner4');
            $type = Configuration::get('PS_SA_TypePromoBanner4');
            $file = $this->_path.'files/PromoBanner4';
            if ($type == 0) {

                $linkTop = Configuration::get('PS_SA_LinkPromoBanner4');
                if ($linkTop != '') {
                    $linkTarget = '';
                    if ($newTabTop == 'checked') {
                        $linkTarget .= ' target="_blank"';
                    }
                    $smarty->assign('linkTarget4', $linkTarget);
                }

            } else if ($type == 1) {
                $width = Configuration::get('PS_SA_WidthTop');
                $height = Configuration::get('PS_SA_HeightTop');
                $content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
            } else {
                $content = Configuration::get('PS_SA_htmlPromoBanner4');
//                $content = '';
//                if ($htmlFile = @fopen(dirname(__FILE__) . '/files/PromoBanner4.html', "r")) {
//                    while (!feof($htmlFile)) {
//                        $content .= fgets($htmlFile, 4096);
//                    }
//                }
//                @fclose($htmlFile);
            }
            $smarty->assign('content4', $content);
            $smarty->assign('promolink4', $linkTop);
            $smarty->assign('promofile4', $file);
            $smarty->assign('text', $text);
            return $this->display(__FILE__, 'promoBanner4.tpl');
//        }
        return '';
    }

    function hookDisplayPromoBanner5($params) {
        if( isset($params['img']) ) {
            $file = $this->_path.'files/PromoBanner5';
            $content = '<img src="' . $file .'" />';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextPromoBanner5');
        $linkTop = '';
//        if (Configuration::get('PS_SA_DisplayPromoBanner5') == 'checked' && (file_exists(dirname(__FILE__).'/files/PromoBanner5') || !empty($text))) {
            global $smarty;
            $newTabTop = Configuration::get('PS_SA_NewTabPromoBanner5');
            $type = Configuration::get('PS_SA_TypePromoBanner5');
            $file = $this->_path.'files/PromoBanner5';
            if ($type == 0) {
                $content = '<img src="' . $file .'" />';
                $linkTop = Configuration::get('PS_SA_LinkPromoBanner5');
                if ($linkTop != '') {
                    if ($newTabTop == 'checked') {
                        $linkTop .= '" target="_new';
                    }
                    $content = '<a href="' . $linkTop .'" class="box_460 fleft">' . $content .'</a>';
                }

            } else if ($type == 1) {
                $width = Configuration::get('PS_SA_WidthTop');
                $height = Configuration::get('PS_SA_HeightTop');
                $content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
            } else {
                $content = Configuration::get('PS_SA_htmlPromoBanner5');
//                $content = '';
//                if ($htmlFile = @fopen(dirname(__FILE__) . '/files/PromoBanner5.html', "r")) {
//                    while (!feof($htmlFile)) {
//                        $content .= fgets($htmlFile, 4096);
//                    }
//                }
//                @fclose($htmlFile);
            }
            $smarty->assign('content', $content);
            $smarty->assign('text', $content);
            return $this->display(__FILE__, 'promoBanner5.tpl');
//        }
        return '';
    }

    function hookDisplayPromoBanner6($params) {
        if( isset($params['img']) ) {
            $file = $this->_path.'files/PromoBanner6';
            $content = '<img src="' . $file .'" />';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextPromoBanner6');
        $linkTop = '';
//        if (Configuration::get('PS_SA_DisplayPromoBanner6') == 'checked' && (file_exists(dirname(__FILE__).'/files/PromoBanner6') || !empty($text))) {
            global $smarty;
            $newTabTop = Configuration::get('PS_SA_NewTabPromoBanner6');
            $type = Configuration::get('PS_SA_TypePromoBanner6');
            $file = $this->_path.'files/PromoBanner6';
            if ($type == 0) {
                $content = '<img src="' . $file .'" />';
                $linkTop = Configuration::get('PS_SA_LinkPromoBanner6');
                if ($linkTop != '') {
                    if ($newTabTop == 'checked') {
                        $linkTop .= '" target="_new';
                    }
                    $content = '<a href="' . $linkTop .'" class="box_460 fleft">' . $content .'</a>';
                }

            } else if ($type == 1) {
                $width = Configuration::get('PS_SA_WidthTop');
                $height = Configuration::get('PS_SA_HeightTop');
                $content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
            } else {
                $content =  Configuration::get('PS_SA_htmlPromoBanner6');
//                $content = '';
//                if ($htmlFile = @fopen(dirname(__FILE__) . '/files/PromoBanner6.html', "r")) {
//                    while (!feof($htmlFile)) {
//                        $content .= fgets($htmlFile, 4096);
//                    }
//                }
//                @fclose($htmlFile);
            }
            $smarty->assign('content', $content);
            $smarty->assign('text', $text);
            return $this->display(__FILE__, 'promoBanner6.tpl');
//        }
        return '';
    }

    function hookDisplayPromoBanner7($params) {
        if( isset($params['img']) ) {
            $file = $this->_path.'files/PromoBanner7';
            $content = '<img src="' . $file .'" />';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextPromoBanner7');
        $linkTop = '';
//        if (Configuration::get('PS_SA_DisplayPromoBanner7') == 'checked' && (file_exists(dirname(__FILE__).'/files/PromoBanner7') || !empty($text))) {
            global $smarty;
            $newTabTop = Configuration::get('PS_SA_NewTabPromoBanner7');
            $type = Configuration::get('PS_SA_TypePromoBanner7');
            $file = $this->_path.'files/PromoBanner7';
            if ($type == 0) {
                $content = '<img src="' . $file .'" />';
                $linkTop = Configuration::get('PS_SA_LinkPromoBanner7');
                if ($linkTop != '') {
                    if ($newTabTop == 'checked') {
                        $linkTop .= '" target="_new';
                    }
                    $content = '<a href="' . $linkTop .'" class="box_460 fleft">' . $content .'</a>';
                }

            } else if ($type == 1) {
                $width = Configuration::get('PS_SA_WidthTop');
                $height = Configuration::get('PS_SA_HeightTop');
                $content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
            } else {
//                $content = '';
//                if ($htmlFile = @fopen(dirname(__FILE__) . '/files/PromoBanner7.html', "r")) {
//                    while (!feof($htmlFile)) {
//                        $content .= fgets($htmlFile, 4096);
//                    }
//                }
//                @fclose($htmlFile);
                $content =  Configuration::get('PS_SA_htmlPromoBanner6');
            }
            $smarty->assign('content', $content);
            $smarty->assign('text', $text);
            return $this->display(__FILE__, 'promoBanner7.tpl');
//        }
        return '';
    }


    function hookDisplayPromoBanner8($params) {
        if( isset($params['img']) ) {
            $file = $this->_path.'files/PromoBanner8';
            $content = '<img src="' . $file .'" />';
            return $content;
        }
        $text =  Configuration::get('PS_SA_TextPromoBanner8');
        $linkTop= '';
//        if (Configuration::get('PS_SA_DisplayPromoBanner8') == 'checked' && (file_exists(dirname(__FILE__).'/files/PromoBanner8') || !empty($text))) {
            global $smarty;
            $newTabTop = Configuration::get('PS_SA_NewTabPromoBanner8');
            $type = Configuration::get('PS_SA_TypePromoBanner8');
            $file = $this->_path.'files/PromoBanner8';
            if ($type == 0) {
                $content = '<img src="' . $file .'" />';
                $linkTop = Configuration::get('PS_SA_LinkPromoBanner8');
                if ($linkTop != '') {
                    if ($newTabTop == 'checked') {
                        $linkTop .= '" target="_new';
                    }
                    $content = '<a href="' . $linkTop .'" class="box_460 fleft">' . $content .'</a>';
                }

            } else if ($type == 1) {
                $width = Configuration::get('PS_SA_WidthTop');
                $height = Configuration::get('PS_SA_HeightTop');
                $content = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://active.macromedia.com/flash4/cabs/swflash.cab#version=6,0,47,0" id="animation" width="' . $width . '" height="' . $height . '">
								<param name="movie" value="' . $file .'">
								<param name="quality" value="high">
								<param name="bgcolor" value="#FFFFFF">
								<param name="wmode" value="transparent">
								<embed src="' . $file . '" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="' . $width . '" height="' . $height . '" wmode="transparent"></embed>
							</object>';
            } else {
//                $content = '';
//                if ($htmlFile = @fopen(dirname(__FILE__) . '/files/PromoBanner8.html', "r")) {
//                    while (!feof($htmlFile)) {
//                        $content .= fgets($htmlFile, 4096);
//                    }
//                }
//                @fclose($htmlFile);
                $content =  Configuration::get('PS_SA_htmlPromoBanner8');
            }
            $smarty->assign('content', $content);
            $smarty->assign('text', $text);
            return $this->display(__FILE__, 'promoBanner8.tpl');
//        }
        return '';
    }

	function upload($nom) {
        $config = 'PS_SA_Type' . $nom;
        $type = Configuration::get($config);
        $racine= dirname(__FILE__) . '/files/';
        if(!file_exists($racine)){
            mkdir($racine,777);
        }
        if (isset($_FILES["Files" . $nom]['tmp_name'][$type]) AND !empty($_FILES["Files" . $nom]['tmp_name'][$type])) {
			Configuration::set('PS_IMAGE_GENERATION_METHOD', 1);
			$name = $_FILES["Files" . $nom]["name"][$type];
			$ext = strtolower(substr($name, strrpos($name, ".") + 1));
			if (($type == 0 && ($ext == 'png' || $ext == 'gif' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'bmp'))
					|| ($type == 1 && $ext == 'swf')) {
				if (file_exists($racine . $nom .'.html')) {unlink($racine . $nom . '.html');}
				if (@move_uploaded_file($_FILES["Files" . $nom]["tmp_name"][$type], "$racine$nom")) {
					@chmod("$racine/$nom", 0777);
                    if($type == 0){
                        // Костыль для advantage 4
                        if( $nom == 'Home' ){
                            ImageManager::resize($racine . $nom, $racine . $nom, 940, 78);
                        }else{
                            ImageManager::resize($racine . $nom, $racine . $nom, 220, 220);
                        }
                    }

				} else {
					$this->_html .= $this->displayError($this->l('Failed to upload this file'));
				}
			} else if ($type == 2 && $ext == 'html') {
				if (file_exists($racine . $nom)) {unlink($racine . $nom);}
				if (@move_uploaded_file($_FILES["Files" . $nom]["tmp_name"][$type], "$racine$nom.$ext")) {
					@chmod("$racine/$nom.$ext", 0777);
				} else {
					$this->_html .= $this->displayError($this->l('Failed to upload this file'));
				}			
			} else  {
				$this->_html .= $this->displayError($this->l('File not supported'));
			}
		}
	}

	
	function getContent() {
		$this->_html = '<h2>'.$this->displayName.' - V'. $this->version .'</h2>';
		//advantage1
		if (isset($_POST['submitBlockSotewsAddsLeft'])) {
			Configuration::updateValue('PS_SA_LinkLeft', $_POST['LinkLeft']);
			Configuration::updateValue('PS_SA_TextLeft', $_POST['TextLeft']);

			if (isset($_POST['NewTabLeft'])) {
				Configuration::updateValue('PS_SA_NewTabLeft', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_NewTabLeft', 'unchecked'); 
			}
			if (isset($_POST['AddTypeLeft'])) {
				if ($_POST['AddTypeLeft'] == 'pictureLeft') {
					Configuration::updateValue('PS_SA_TypeLeft', 0);
				} else if ($_POST['AddTypeLeft'] == 'swfLeft') {
					Configuration::updateValue('PS_SA_TypeLeft', 1);
					if (isset($_POST['WidthLeft'])) {
						if (is_int(intval($_POST['WidthLeft'])) && floatval($_POST['WidthLeft'] > 0)) {
							Configuration::updateValue('PS_SA_WidthLeft', $_POST['WidthLeft']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}			
					if (isset($_POST['HeightLeft'])) {
						if (is_int(intval($_POST['HeightLeft'])) && $_POST['HeightLeft'] > 0) {
							Configuration::updateValue('PS_SA_HeightLeft', $_POST['HeightLeft']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
				} else {					
					Configuration::updateValue('PS_SA_TypeLeft', 2);
                    Configuration::updateValue('PS_SA_html', $_POST['PS_SA_html'], true);
				}
			}
			$this->upload('Left');
			
			if (isset($_POST['DisplayBlockLeft'])) { 
				Configuration::updateValue('PS_SA_DisplayBlockLeft', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_DisplayBlockLeft', 'unchecked'); 
			}
			
			if (isset($_POST['DisplayLeft'])) { 
				Configuration::updateValue('PS_SA_DisplayLeft', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_DisplayLeft', 'unchecked'); 
			}
			$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Left Settings updated').'</div>'; 
		}
        //advantage2
		if (isset($_POST['submitBlockSotewsAddsRight'])) {
			Configuration::updateValue('PS_SA_LinkRight', $_POST['LinkRight']);
			Configuration::updateValue('PS_SA_TextRight', $_POST['TextRight']);
			if (isset($_POST['NewTabRight'])) {
				Configuration::updateValue('PS_SA_NewTabRight', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_NewTabRight', 'unchecked'); 
			}
			if (isset($_POST['AddTypeRight'])) {
				if ($_POST['AddTypeRight'] == 'pictureRight') {
					Configuration::updateValue('PS_SA_TypeRight', 0);
				} else if ($_POST['AddTypeRight'] == 'swfRight') {
					Configuration::updateValue('PS_SA_TypeRight', 1);
					if (isset($_POST['WidthRight'])) {
						if (is_int(intval($_POST['WidthRight'])) && floatval($_POST['WidthRight'] > 0)) {
							Configuration::updateValue('PS_SA_WidthRight', $_POST['WidthRight']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}			
					if (isset($_POST['HeightRight'])) {
						if (is_int(intval($_POST['HeightRight'])) && $_POST['HeightRight'] > 0) {
							Configuration::updateValue('PS_SA_HeightRight', $_POST['HeightRight']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
				} else {					
					Configuration::updateValue('PS_SA_TypeRight', 2);
                    Configuration::updateValue('PS_SA_htmlRight', $_POST['PS_SA_htmlRight'], true);
				}
			}
			$this->upload('Right');
			
			if (isset($_POST['DisplayBlockRight'])) { 
				Configuration::updateValue('PS_SA_DisplayBlockRight', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_DisplayBlockRight', 'unchecked'); 
			}
			
			if (isset($_POST['DisplayRight'])) { 
				Configuration::updateValue('PS_SA_DisplayRight', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_DisplayRight', 'unchecked'); 
			}
			$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Right Settings updated').'</div>'; 
		}

        //advantage4
		if (isset($_POST['submitBlockSotewsAddsHome'])) {
			Configuration::updateValue('PS_SA_LinkHome', $_POST['LinkHome']);
			Configuration::updateValue('PS_SA_TextHome', $_POST['TextHome']);
			if (isset($_POST['NewTabHome'])) {
				Configuration::updateValue('PS_SA_NewTabHome', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_NewTabHome', 'unchecked'); 
			}
			if (isset($_POST['AddTypeHome'])) {
				if ($_POST['AddTypeHome'] == 'pictureHome') {
					Configuration::updateValue('PS_SA_TypeHome', 0);
				} else if ($_POST['AddTypeHome'] == 'swfHome') {
					Configuration::updateValue('PS_SA_TypeHome', 1);
					if (isset($_POST['WidthHome'])) {
						if (is_int(intval($_POST['WidthHome'])) && floatval($_POST['WidthHome'] > 0)) {
							Configuration::updateValue('PS_SA_WidthHome', $_POST['WidthHome']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}			
					if (isset($_POST['HeightHome'])) {
						if (is_int(intval($_POST['HeightHome'])) && $_POST['HeightHome'] > 0) {
							Configuration::updateValue('PS_SA_HeightHome', $_POST['HeightHome']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
				} else {					
					Configuration::updateValue('PS_SA_TypeHome', 2);
                    Configuration::updateValue('PS_SA_htmlHome', $_POST['PS_SA_htmlHome'], true);
				}
			}
			$this->upload('Home');
			
			if (isset($_POST['DisplayHome'])) { 
				Configuration::updateValue('PS_SA_DisplayHome', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_DisplayHome', 'unchecked'); 
			}
			$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Home Settings updated').'</div>'; 
		}
        // promo banner 1
		if (isset($_POST['submitBlockSotewsAddsHeader'])) {
			Configuration::updateValue('PS_SA_LinkHeader', $_POST['LinkHeader']);
			Configuration::updateValue('PS_SA_TextHeader', $_POST['TextHeader']);
			if (isset($_POST['NewTabHeader'])) {
				Configuration::updateValue('PS_SA_NewTabHeader', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_NewTabHeader', 'unchecked'); 
			}
			if (isset($_POST['AddTypeHeader'])) {
				if ($_POST['AddTypeHeader'] == 'pictureHeader') {
					Configuration::updateValue('PS_SA_TypeHeader', 0);
				} else if ($_POST['AddTypeHeader'] == 'swfHeader') {
					Configuration::updateValue('PS_SA_TypeHeader', 1);
					if (isset($_POST['WidthHeader'])) {
						if (is_int(intval($_POST['WidthHeader'])) && floatval($_POST['WidthHeader'] > 0)) {
							Configuration::updateValue('PS_SA_WidthHeader', $_POST['WidthHeader']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}			
					if (isset($_POST['HeightHeader'])) {
						if (is_int(intval($_POST['HeightHeader'])) && $_POST['HeightHeader'] > 0) {
							Configuration::updateValue('PS_SA_HeightHeader', $_POST['HeightHeader']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
				} else {					
					Configuration::updateValue('PS_SA_TypeHeader', 2);
                    Configuration::updateValue('PS_SA_htmlHeader', $_POST['PS_SA_htmlHeader'], true);
				}
			}
			$this->upload('Header');
			
			if (isset($_POST['DisplayHeader'])) { 
				Configuration::updateValue('PS_SA_DisplayHeader', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_DisplayHeader', 'unchecked'); 
			}
			$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Header Settings updated').'</div>'; 
		}
		// promo banner 2
		if (isset($_POST['submitBlockSotewsAddsFooter'])) {
			Configuration::updateValue('PS_SA_LinkFooter', $_POST['LinkFooter']);
			Configuration::updateValue('PS_SA_TextFooter', $_POST['TextFooter']);
			if (isset($_POST['NewTabFooter'])) {
				Configuration::updateValue('PS_SA_NewTabFooter', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_NewTabFooter', 'unchecked'); 
			}
			if (isset($_POST['AddTypeFooter'])) {
				if ($_POST['AddTypeFooter'] == 'pictureFooter') {
					Configuration::updateValue('PS_SA_TypeFooter', 0);
				} else if ($_POST['AddTypeFooter'] == 'swfFooter') {
					Configuration::updateValue('PS_SA_TypeFooter', 1);
					if (isset($_POST['WidthFooter'])) {
						if (is_int(intval($_POST['WidthFooter'])) && floatval($_POST['WidthFooter'] > 0)) {
							Configuration::updateValue('PS_SA_WidthFooter', $_POST['WidthFooter']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}			
					if (isset($_POST['HeightFooter'])) {
						if (is_int(intval($_POST['HeightFooter'])) && $_POST['HeightFooter'] > 0) {
							Configuration::updateValue('PS_SA_HeightFooter', $_POST['HeightFooter']); 
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
				} else {					
					Configuration::updateValue('PS_SA_TypeFooter', 2);
                    Configuration::updateValue('PS_SA_htmlFooter', $_POST['PS_SA_htmlFooter'], true);
				}
			}
			$this->upload('Footer');
			
			if (isset($_POST['DisplayFooter'])) { 
				Configuration::updateValue('PS_SA_DisplayFooter', 'checked'); 
			} else { 
				Configuration::updateValue('PS_SA_DisplayFooter', 'unchecked'); 
			}
			$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Footer Settings updated').'</div>'; 
		}
        // promo banner 3
		if (isset($_POST['promoBanner3'])) {

			Configuration::updateValue('PS_SA_LinkPromoBanner3', $_POST['LinkPromoBanner3']);
			Configuration::updateValue('PS_SA_TextPromoBanner3', $_POST['TextPromoBanner3']);
			if (isset($_POST['NewTabPromoBanner3'])) {
				Configuration::updateValue('PS_SA_NewTabPromoBanner3', 'checked');
			} else {
				Configuration::updateValue('PS_SA_NewTabPromoBanner3', 'unchecked');
			}
			if (isset($_POST['AddTypePromoBanner3'])) {
				if ($_POST['AddTypePromoBanner3'] == 'picturePromoBanner3') {
					Configuration::updateValue('PS_SA_TypePromoBanner3', 0);
				} else if ($_POST['AddTypePromoBanner3'] == 'swfPromoBanner3') {
					Configuration::updateValue('PS_SA_TypePromoBanner3', 1);
					if (isset($_POST['WidthFooter'])) {
						if (is_int(intval($_POST['WidthFooter'])) && floatval($_POST['WidthPromoBanner3'] > 0)) {
							Configuration::updateValue('PS_SA_WidthPromoBanner3', $_POST['WidthPromoBanner3']);
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
					if (isset($_POST['HeightFooter'])) {
						if (is_int(intval($_POST['HeightPromoBanner3'])) && $_POST['HeightPromoBanner3'] > 0) {
							Configuration::updateValue('PS_SA_HeightPromoBanner3', $_POST['HeightPromoBanner3']);
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
				} else {
					Configuration::updateValue('PS_SA_TypePromoBanner3', 2);
                    Configuration::updateValue('PS_SA_htmlPromoBanner3', $_POST['PS_SA_htmlPromoBanner3'], true);
				}
			}
			$this->upload('PromoBanner3');
            if (isset($_POST['DisplayPromoBanner3'])) {
                Configuration::updateValue('PS_SA_DisplayPromoBanner3', 'checked');

            } else {
				Configuration::updateValue('PS_SA_DisplayPromoBanner3', 'unchecked');
			}
			$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Footer Settings updated').'</div>';
		}

        // promo banner 4
        if (isset($_POST['promoBanner4'])) {

            Configuration::updateValue('PS_SA_LinkPromoBanner4', $_POST['LinkPromoBanner4']);
            Configuration::updateValue('PS_SA_TextPromoBanner4', $_POST['TextPromoBanner4']);
            if (isset($_POST['NewTabPromoBanner4'])) {
                Configuration::updateValue('PS_SA_NewTabPromoBanner4', 'checked');
            } else {
                Configuration::updateValue('PS_SA_NewTabPromoBanner4', 'unchecked');
            }
            if (isset($_POST['AddTypePromoBanner4'])) {
                if ($_POST['AddTypePromoBanner4'] == 'picturePromoBanner4') {
                    Configuration::updateValue('PS_SA_TypePromoBanner4', 0);
                } else if ($_POST['AddTypePromoBanner4'] == 'swfPromoBanner4') {
                    Configuration::updateValue('PS_SA_TypePromoBanner4', 1);
                    if (isset($_POST['WidthFooter'])) {
                        if (is_int(intval($_POST['WidthFooter'])) && floatval($_POST['WidthPromoBanner4'] > 0)) {
                            Configuration::updateValue('PS_SA_WidthPromoBanner4', $_POST['WidthPromoBanner4']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                    if (isset($_POST['HeightFooter'])) {
                        if (is_int(intval($_POST['HeightPromoBanner4'])) && $_POST['HeightPromoBanner4'] > 0) {
                            Configuration::updateValue('PS_SA_HeightPromoBanner4', $_POST['HeightPromoBanner4']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                } else {
                    Configuration::updateValue('PS_SA_TypePromoBanner4', 2);
                    Configuration::updateValue('PS_SA_htmlPromoBanner4', $_POST['PS_SA_htmlPromoBanner4'], true);
                }
            }
            $this->upload('PromoBanner4');
            if (isset($_POST['DisplayPromoBanner4'])) {
                Configuration::updateValue('PS_SA_DisplayPromoBanner4', 'checked');

            } else {
                Configuration::updateValue('PS_SA_DisplayPromoBanner4', 'unchecked');
            }
            $this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Footer Settings updated').'</div>';
        }

        // promo banner 5
        if (isset($_POST['promoBanner5'])) {
            Configuration::updateValue('PS_SA_LinkPromoBanner5', $_POST['LinkPromoBanner5']);
            Configuration::updateValue('PS_SA_TextPromoBanner5', $_POST['TextPromoBanner5']);
            if (isset($_POST['NewTabPromoBanner5'])) {
                Configuration::updateValue('PS_SA_NewTabPromoBanner5', 'checked');
            } else {
                Configuration::updateValue('PS_SA_NewTabPromoBanner5', 'unchecked');
            }
            if (isset($_POST['AddTypePromoBanner5'])) {
                if ($_POST['AddTypePromoBanner5'] == 'picturePromoBanner5') {
                    Configuration::updateValue('PS_SA_TypePromoBanner5', 0);
                } else if ($_POST['AddTypePromoBanner5'] == 'swfPromoBanner5') {
                    Configuration::updateValue('PS_SA_TypePromoBanner5', 1);
                    if (isset($_POST['WidthFooter'])) {
                        if (is_int(intval($_POST['WidthFooter'])) && floatval($_POST['WidthPromoBanner5'] > 0)) {
                            Configuration::updateValue('PS_SA_WidthPromoBanner5', $_POST['WidthPromoBanner5']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                    if (isset($_POST['HeightFooter'])) {
                        if (is_int(intval($_POST['HeightPromoBanner5'])) && $_POST['HeightPromoBanner5'] > 0) {
                            Configuration::updateValue('PS_SA_HeightPromoBanner5', $_POST['HeightPromoBanner5']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                } else {
                    Configuration::updateValue('PS_SA_TypePromoBanner5', 2);
                    Configuration::updateValue('PS_SA_htmlPromoBanner5', $_POST['PS_SA_htmlPromoBanner5'], true);
                }
            }
            $this->upload('PromoBanner5');
            if (isset($_POST['DisplayPromoBanner5'])) {
                Configuration::updateValue('PS_SA_DisplayPromoBanner5', 'checked');

            } else {
                Configuration::updateValue('PS_SA_DisplayPromoBanner5', 'unchecked');
            }
            $this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Footer Settings updated').'</div>';
        }

        // promo banner 6
        if (isset($_POST['promoBanner6'])) {
            Configuration::updateValue('PS_SA_LinkPromoBanner6', $_POST['LinkPromoBanner6']);
            Configuration::updateValue('PS_SA_TextPromoBanner6', $_POST['TextPromoBanner6']);
            if (isset($_POST['NewTabPromoBanner6'])) {
                Configuration::updateValue('PS_SA_NewTabPromoBanner6', 'checked');
            } else {
                Configuration::updateValue('PS_SA_NewTabPromoBanner6', 'unchecked');
            }
            if (isset($_POST['AddTypePromoBanner6'])) {
                if ($_POST['AddTypePromoBanner6'] == 'picturePromoBanner6') {
                    Configuration::updateValue('PS_SA_TypePromoBanner6', 0);
                } else if ($_POST['AddTypePromoBanner6'] == 'swfPromoBanner6') {
                    Configuration::updateValue('PS_SA_TypePromoBanner6', 1);
                    if (isset($_POST['WidthFooter'])) {
                        if (is_int(intval($_POST['WidthFooter'])) && floatval($_POST['WidthPromoBanner6'] > 0)) {
                            Configuration::updateValue('PS_SA_WidthPromoBanner6', $_POST['WidthPromoBanner6']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                    if (isset($_POST['HeightFooter'])) {
                        if (is_int(intval($_POST['HeightPromoBanner6'])) && $_POST['HeightPromoBanner6'] > 0) {
                            Configuration::updateValue('PS_SA_HeightPromoBanner6', $_POST['HeightPromoBanner6']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                } else {
                    Configuration::updateValue('PS_SA_TypePromoBanner6', 2);
                    Configuration::updateValue('PS_SA_htmlPromoBanner6', $_POST['PS_SA_htmlPromoBanner6'], true);
                }
            }
            $this->upload('PromoBanner6');
            if (isset($_POST['DisplayPromoBanner6'])) {
                Configuration::updateValue('PS_SA_DisplayPromoBanner6', 'checked');

            } else {
                Configuration::updateValue('PS_SA_DisplayPromoBanner6', 'unchecked');
            }
            $this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Footer Settings updated').'</div>';
        }

        // promo banner 7
        if (isset($_POST['promoBanner7'])) {
            Configuration::updateValue('PS_SA_LinkPromoBanner7', $_POST['LinkPromoBanner7']);
            Configuration::updateValue('PS_SA_TextPromoBanner7', $_POST['TextPromoBanner7']);
            if (isset($_POST['NewTabPromoBanner7'])) {
                Configuration::updateValue('PS_SA_NewTabPromoBanner7', 'checked');
            } else {
                Configuration::updateValue('PS_SA_NewTabPromoBanner7', 'unchecked');
            }
            if (isset($_POST['AddTypePromoBanner7'])) {
                if ($_POST['AddTypePromoBanner7'] == 'picturePromoBanner7') {
                    Configuration::updateValue('PS_SA_TypePromoBanner7', 0);
                } else if ($_POST['AddTypePromoBanner7'] == 'swfPromoBanner7') {
                    Configuration::updateValue('PS_SA_TypePromoBanner7', 1);
                    if (isset($_POST['WidthFooter'])) {
                        if (is_int(intval($_POST['WidthFooter'])) && floatval($_POST['WidthPromoBanner7'] > 0)) {
                            Configuration::updateValue('PS_SA_WidthPromoBanner7', $_POST['WidthPromoBanner7']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                    if (isset($_POST['HeightFooter'])) {
                        if (is_int(intval($_POST['HeightPromoBanner7'])) && $_POST['HeightPromoBanner7'] > 0) {
                            Configuration::updateValue('PS_SA_HeightPromoBanner7', $_POST['HeightPromoBanner7']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                } else {
                    Configuration::updateValue('PS_SA_TypePromoBanner7', 2);
                    Configuration::updateValue('PS_SA_htmlPromoBanner7', $_POST['PS_SA_htmlPromoBanner7'], true);
                }
            }
            $this->upload('PromoBanner7');
            if (isset($_POST['DisplayPromoBanner7'])) {
                Configuration::updateValue('PS_SA_DisplayPromoBanner7', 'checked');

            } else {
                Configuration::updateValue('PS_SA_DisplayPromoBanner7', 'unchecked');
            }
            $this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Footer Settings updated').'</div>';
        }

        // promo banner 8
        if (isset($_POST['promoBanner8'])) {
            Configuration::updateValue('PS_SA_LinkPromoBanner8', $_POST['LinkPromoBanner8']);
            Configuration::updateValue('PS_SA_TextPromoBanner8', $_POST['TextPromoBanner8']);
            if (isset($_POST['NewTabPromoBanner8'])) {
                Configuration::updateValue('PS_SA_NewTabPromoBanner8', 'checked');
            } else {
                Configuration::updateValue('PS_SA_NewTabPromoBanner8', 'unchecked');
            }
            if (isset($_POST['AddTypePromoBanner8'])) {
                if ($_POST['AddTypePromoBanner8'] == 'picturePromoBanner8') {
                    Configuration::updateValue('PS_SA_TypePromoBanner8', 0);
                } else if ($_POST['AddTypePromoBanner8'] == 'swfPromoBanner8') {
                    Configuration::updateValue('PS_SA_TypePromoBanner8', 1);
                    if (isset($_POST['WidthFooter'])) {
                        if (is_int(intval($_POST['WidthFooter'])) && floatval($_POST['WidthPromoBanner8'] > 0)) {
                            Configuration::updateValue('PS_SA_WidthPromoBanner8', $_POST['WidthPromoBanner8']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                    if (isset($_POST['HeightFooter'])) {
                        if (is_int(intval($_POST['HeightPromoBanner8'])) && $_POST['HeightPromoBanner8'] > 0) {
                            Configuration::updateValue('PS_SA_HeightPromoBanner8', $_POST['HeightPromoBanner8']);
                        } else {
                            $this->_html .= $this->displayError($this->l('Wrong number'));
                        }
                    }
                } else {
                    Configuration::updateValue('PS_SA_TypePromoBanner8', 2);
                    Configuration::updateValue('PS_SA_htmlPromoBanner8', $_POST['PS_SA_htmlPromoBanner8'], true);
                }
            }
            $this->upload('PromoBanner8');
            if (isset($_POST['DisplayPromoBanner8'])) {
                Configuration::updateValue('PS_SA_DisplayPromoBanner8', 'checked');

            } else {
                Configuration::updateValue('PS_SA_DisplayPromoBanner8', 'unchecked');
            }
            $this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Footer Settings updated').'</div>';
        }

        //advantage3
		if (isset($_POST['submitBlockSotewsAddsTop'])) {
			Configuration::updateValue('PS_SA_LinkTop', $_POST['LinkTop']);
			Configuration::updateValue('PS_SA_TextTop', $_POST['TextTop']);
			if (isset($_POST['NewTabTop'])) {
				Configuration::updateValue('PS_SA_NewTabTop', 'checked');
			} else {
				Configuration::updateValue('PS_SA_NewTabTop', 'unchecked');
			}
			if (isset($_POST['AddTypeTop'])) {
				if ($_POST['AddTypeTop'] == 'pictureTop') {
					Configuration::updateValue('PS_SA_TypeTop', 0);
				} else if ($_POST['AddTypeTop'] == 'swfTop') {
					Configuration::updateValue('PS_SA_TypeTop', 1);
					if (isset($_POST['WidthTop'])) {
						if (is_int(intval($_POST['WidthTop'])) && floatval($_POST['WidthTop'] > 0)) {
							Configuration::updateValue('PS_SA_WidthTop', $_POST['WidthTop']);
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
					if (isset($_POST['HeightTop'])) {
						if (is_int(intval($_POST['HeightTop'])) && $_POST['HeightTop'] > 0) {
							Configuration::updateValue('PS_SA_HeightTop', $_POST['HeightTop']);
						} else {
							$this->_html .= $this->displayError($this->l('Wrong number'));
						}
					}
				} else {
					Configuration::updateValue('PS_SA_TypeTop', 2);
                    Configuration::updateValue('PS_SA_htmlTop', $_POST['PS_SA_htmlTop'], true);
				}
			}
			$this->upload('Top');

			if (isset($_POST['DisplayTop'])) {
				Configuration::updateValue('PS_SA_DisplayTop', 'checked');
			} else {
				Configuration::updateValue('PS_SA_DisplayTop', 'unchecked');
			}
			$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l(' Top Settings updated').'</div>';
		}
//
		
		$this->_displayForm();
		return $this->_html;
	}

	private function _displayForm()	{
		global $cookie;
		
		$typeLeft = Configuration::get('PS_SA_TypeLeft');
		$pictureLeft = 'unchecked';
		$swfLeft = 'unchecked';
		$htmlLeft = 'unchecked';
		if ($typeLeft == 0) {
			$pictureLeft = 'checked';
		} else if ($typeLeft == 1) {
			$swfLeft = 'checked';
		} else {
			$htmlLeft = 'checked';
		}
        $typeRight = Configuration::get('PS_SA_TypeRight');
        $pictureRight = 'unchecked';
		$swfRight = 'unchecked';
		$htmlRight = 'unchecked';
		if ($typeRight == 0) {
			$pictureRight = 'checked';
		} else if ($typeRight == 1) {
			$swfRight = 'checked';
		} else {
			$htmlRight = 'checked';
		}

		$typeHome = Configuration::get('PS_SA_TypeHome');
		$pictureHome = 'unchecked';
		$swfHome = 'unchecked';
		$htmlHome = 'unchecked';
		if ($typeHome == 0) {
			$pictureHome = 'checked';
		} else if ($typeHome == 1) {
			$swfHome = 'checked';
		} else {
			$htmlHome = 'checked';
		}

		$typeHeader = Configuration::get('PS_SA_TypeHeader');
		$pictureHeader = 'unchecked';
		$swfHeader = 'unchecked';
		$htmlHeader = 'unchecked';
		if ($typeHeader == 0) {
			$pictureHeader = 'checked';
		} else if ($typeHeader == 1) {
			$swfHeader = 'checked';
		} else {
			$htmlHeader = 'checked';
		}

		$typeFooter = Configuration::get('PS_SA_TypeFooter');
		$pictureFooter = 'unchecked';
		$swfFooter = 'unchecked';
		$htmlFooter = 'unchecked';
		if ($typeFooter == 0) {
			$pictureFooter = 'checked';
		} else if ($typeFooter == 1) {
			$swfFooter = 'checked';
		} else {
			$htmlFooter = 'checked';
		}

		$typeTop = Configuration::get('PS_SA_TypeTop');
		$pictureTop = 'unchecked';
		$swfTop = 'unchecked';
		$htmlTop = 'unchecked';
		if ($typeTop == 0) {
			$pictureTop = 'checked';
		} else if ($typeTop == 1) {
			$swfTop = 'checked';
		} else {
			$htmlTop = 'checked';
		}

        $PS_SA_TypePromoBanner3 = Configuration::get('PS_SA_TypePromoBanner3');
        $picturePromoBanner3 = 'unchecked';
        $swfPromoBanner3 = 'unchecked';
        $htmlPromoBanner3 = 'unchecked';
        if ($PS_SA_TypePromoBanner3 == 0) {
            $picturePromoBanner3 = 'checked';
        } else if ($typeTop == 1) {
            $swfPromoBanner3 = 'checked';
        } else {
            $htmlPromoBanner3 = 'checked';
        }

        $PS_SA_TypePromoBanner4 = Configuration::get('PS_SA_TypePromoBanner4');
        $picturePromoBanner4 = 'unchecked';
        $swfPromoBanner4 = 'unchecked';
        $htmlPromoBanner4 = 'unchecked';

        if ($PS_SA_TypePromoBanner4 == 0) {
            $picturePromoBanner4 = 'checked';
        } else if ($typeTop == 1) {
            $swfPromoBanner4 = 'checked';
        } else {
            $htmlPromoBanner4 = 'checked';
        }

        $PS_SA_TypePromoBanner5 = Configuration::get('PS_SA_TypePromoBanner5');
        $picturePromoBanner5 = 'unchecked';
        $swfPromoBanner5 = 'unchecked';
        $htmlPromoBanner5 = 'unchecked';
        if ($PS_SA_TypePromoBanner5 == 0) {
            $picturePromoBanner5 = 'checked';
        } else if ($typeTop == 1) {
            $swfPromoBanner5 = 'checked';
        } else {
            $htmlPromoBanner5 = 'checked';
        }

        $PS_SA_TypePromoBanner6 = Configuration::get('PS_SA_TypePromoBanner6');
        $picturePromoBanner6 = 'unchecked';
        $swfPromoBanner6 = 'unchecked';
        $htmlPromoBanner6 = 'unchecked';
        if ($PS_SA_TypePromoBanner6 == 0) {
            $picturePromoBanner6 = 'checked';
        } else if ($typeTop == 1) {
            $swfPromoBanner6 = 'checked';
        } else {
            $htmlPromoBanner6 = 'checked';
        }

        $PS_SA_TypePromoBanner7 = Configuration::get('PS_SA_TypePromoBanner7');
        $picturePromoBanner7 = 'unchecked';
        $swfPromoBanner7 = 'unchecked';
        $htmlPromoBanner7 = 'unchecked';
        if ($PS_SA_TypePromoBanner7 == 0) {
            $picturePromoBanner7 = 'checked';
        } else if ($typeTop == 1) {
            $swfPromoBanner7 = 'checked';
        } else {
            $htmlPromoBanner7 = 'checked';
        }


        $PS_SA_TypePromoBanner8 = Configuration::get('PS_SA_TypePromoBanner8');
        $picturePromoBanner8 = 'unchecked';
        $swfPromoBanner8 = 'unchecked';
        $htmlPromoBanner8 = 'unchecked';
        if ($PS_SA_TypePromoBanner8 == 0) {
            $picturePromoBanner8 = 'checked';
        } else if ($typeTop == 1) {
            $swfPromoBanner8 = 'checked';
        } else {
            $htmlPromoBanner8 = 'checked';
        }

		$this->_html .= '<br /><table border="0"><tr><td rowspan="3"><img src="'.$this->_path.'/img/logo_big.gif" style="float:left; margin-right:15px;"></td><td><b>'.$this->l('This module allows you to configure advertising in the header, footer, home page and in the left and right column.').'</b></td></tr><tr><td>
		'.$this->l('You can choose what kind of pictures or animation will be displayed.').'<br />
		'.$this->l('It is also possible to put whatever you want in a .html file').'</td></tr><tr><td>&nbsp;</td></tr></table><br/><br/>';
		$this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/column_left.gif" alt="" title="" />'.$this->l('Advantage1').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkLeft" value="'.Configuration::get('PS_SA_LinkLeft').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabLeft" value="on"'. Configuration::get('PS_SA_NewTabLeft') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextLeft" value="'.Configuration::get('PS_SA_TextLeft').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeLeft"  value= "pictureLeft" '. $pictureLeft .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesLeft[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeLeft"  value= "swfLeft" '. $swfLeft .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesLeft[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthLeft" value="'.Configuration::get('PS_SA_WidthLeft').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightLeft" value="'.Configuration::get('PS_SA_HeightLeft').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeLeft"  value= "htmlLeft" '. $htmlLeft .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">

										<textarea name="PS_SA_html" rows="7" cols="70">'.Configuration::get('PS_SA_html').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayBlockLeft" value="on"'. Configuration::get('PS_SA_DisplayBlockLeft') .' />
									</td>
									<td>
										<b>'.$this->l('Display in a block').'</b>
									</td>
								</tr>
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayLeft" value="on"'. Configuration::get('PS_SA_DisplayLeft') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="submitBlockSotewsAddsLeft" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
					<td width="400" valign="top">
						<center><b>'.''.'</b><br/><br/>
						'.$this->hookDisplayAdvantage1(($pictureLeft == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';
		$this->_html .='<br/><br/>';

		$this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/column_right.gif" alt="" title="" />'.$this->l('Advantage2').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkRight" value="'.Configuration::get('PS_SA_LinkRight').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabRight" value="on"'. Configuration::get('PS_SA_NewTabRight') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextRight" value="'.Configuration::get('PS_SA_TextRight').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeRight"  value= "pictureRight" '. $pictureRight .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesRight[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeRight"  value= "swfRight" '. $swfRight .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesRight[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthRight" value="'.Configuration::get('PS_SA_WidthRight').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightRight" value="'.Configuration::get('PS_SA_HeightRight').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeRight"  value= "htmlRight" '. $htmlRight .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlRight" rows="7" cols="70">'.Configuration::get('PS_SA_htmlRight').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayBlockRight" value="on"'. Configuration::get('PS_SA_DisplayBlockRight') .' />
									</td>
									<td>
										<b>'.$this->l('Display in a block').'</b>
									</td>
								</tr>
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayRight" value="on"'. Configuration::get('PS_SA_DisplayRight') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="submitBlockSotewsAddsRight" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayAdvantage2(($pictureRight == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';
		$this->_html .='<br/><br/>';

		$this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/top.gif" alt="" title="" />'.$this->l('Advantage3').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkTop" value="'.Configuration::get('PS_SA_LinkTop').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabTop" value="on"'. Configuration::get('PS_SA_NewTabTop') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextTop" value="'.Configuration::get('PS_SA_TextTop').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeTop"  value= "pictureTop" '. $pictureTop .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesTop[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeTop"  value= "swfTop" '. $swfTop .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesTop[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthTop" value="'.Configuration::get('PS_SA_WidthTop').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightTop" value="'.Configuration::get('PS_SA_HeightTop').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeTop"  value= "htmlTop" '. $htmlTop .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlTop" rows="7" cols="70">'.Configuration::get('PS_SA_htmlTop').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayTop" value="on"'. Configuration::get('PS_SA_DisplayTop') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="submitBlockSotewsAddsTop" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayAdvantage3(($pictureTop == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';
		$this->_html .='<br/><br/>';

		$this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/home.gif" alt="" title="" />'.$this->l('Advantage4').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkHome" value="'.Configuration::get('PS_SA_LinkHome').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabHome" value="on"'. Configuration::get('PS_SA_NewTabHome') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextHome" value="'.Configuration::get('PS_SA_TextHome').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeHome"  value= "pictureHome" '. $pictureHome .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesHome[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeHome"  value= "swfHome" '. $swfHome .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesHome[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthHome" value="'.Configuration::get('PS_SA_WidthHome').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightHome" value="'.Configuration::get('PS_SA_HeightHome').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeHome"  value= "htmlHome" '. $htmlHome .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlHome" rows="7" cols="70">'.Configuration::get('PS_SA_htmlHome').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayHome" value="on"'. Configuration::get('PS_SA_DisplayHome') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="submitBlockSotewsAddsHome" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayAdvantage4(($pictureHome == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';
		$this->_html .='<br/><br/>';

		$this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/header.gif" alt="" title="" />'.$this->l('PromoBanner1').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkHeader" value="'.Configuration::get('PS_SA_LinkHeader').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabHeader" value="on"'. Configuration::get('PS_SA_NewTabHeader') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextHeader" value="'.Configuration::get('PS_SA_TextHeader').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeHeader"  value= "pictureHeader" '. $pictureHeader .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesHeader[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeHeader"  value= "swfHeader" '. $swfHeader .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesHeader[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthHeader" value="'.Configuration::get('PS_SA_WidthHeader').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightHeader" value="'.Configuration::get('PS_SA_HeightHeader').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeHeader"  value= "htmlHeader" '. $htmlHeader .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlHeader" rows="7" cols="70">'.Configuration::get('PS_SA_htmlHeader').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayHeader" value="on"'. Configuration::get('PS_SA_DisplayHeader') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="submitBlockSotewsAddsHeader" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner1(($pictureHeader == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';
		$this->_html .='<br/><br/>';

		$this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/footer.gif" alt="" title="" />'.$this->l('PromoBanner2').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkFooter" value="'.Configuration::get('PS_SA_LinkFooter').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabFooter" value="on"'. Configuration::get('PS_SA_NewTabFooter') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextFooter" value="'.Configuration::get('PS_SA_TextFooter').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeFooter"  value= "pictureFooter" '. $pictureFooter .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesFooter[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeFooter"  value= "swfFooter" '. $swfFooter .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesFooter[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthFooter" value="'.Configuration::get('PS_SA_WidthFooter').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightFooter" value="'.Configuration::get('PS_SA_HeightFooter').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypeFooter"  value= "htmlFooter" '. $htmlFooter .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlFooter" rows="7" cols="70">'.Configuration::get('PS_SA_htmlFooter').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayFooter" value="on"'. Configuration::get('PS_SA_DisplayFooter') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="submitBlockSotewsAddsFooter" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner2(($pictureFooter == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';

        /*======================================================PromoBanner3==============================================*/
        $this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/footer.gif" alt="" title="" />'.$this->l('PromoBanner3').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkPromoBanner3" value="'.Configuration::get('PS_SA_LinkPromoBanner3').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabPromoBanner3" value="on"'. Configuration::get('PS_SA_NewTabPromoBanner3') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextPromoBanner3" value="'.Configuration::get('PS_SA_TextPromoBanner3').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner3"  value= "picturePromoBanner3" '. $picturePromoBanner3 .'" />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner3[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner3"  value= "swfPromoPromoBanner3" '. $swfPromoBanner3 .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner3[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthPromoBanner3" value="'.Configuration::get('PS_SA_WidthPromoBanner3').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightPromoBanner3" value="'.Configuration::get('PS_SA_HeightPromoBanner3').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner3"  value= "htmlPromoBanner3" '. $htmlPromoBanner3 .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlPromoBanner3" rows="7" cols="70">'.Configuration::get('PS_SA_htmlPromoBanner3').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayPromoBanner3" value="on"'. Configuration::get('PS_SA_DisplayPromoBanner3') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="promoBanner3" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner3(($picturePromoBanner3 == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';

        /*======================================================PromoBanner4==============================================*/
        $this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/footer.gif" alt="" title="" />'.$this->l('PromoBanner4').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkPromoBanner4" value="'.Configuration::get('PS_SA_LinkPromoBanner4').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabPromoBanner4" value="on"'. Configuration::get('PS_SA_NewTabPromoBanner4') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextPromoBanner4" value="'.Configuration::get('PS_SA_TextPromoBanner4').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner4"  value= "picturePromoBanner4" '. $picturePromoBanner4 .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner4[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner4"  value= "swfPromoBanner4" '. $swfPromoBanner4 .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner4[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthPromoBanner4" value="'.Configuration::get('PS_SA_WidthPromoBanner4').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightPromoBanner4" value="'.Configuration::get('PS_SA_HeightPromoBanner4').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner4"  value= "htmlPromoBanner4" '. $htmlPromoBanner4 .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlPromoBanner4" rows="7" cols="70">'.Configuration::get('PS_SA_htmlPromoBanner4').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayPromoBanner4" value="on"'. Configuration::get('PS_SA_DisplayPromoBanner4') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="promoBanner4" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner4(($picturePromoBanner4 == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';

        /*======================================================PromoBanner5==============================================*/
        $this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/footer.gif" alt="" title="" />'.$this->l('PromoBanner5').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkPromoBanner5" value="'.Configuration::get('PS_SA_LinkPromoBanner5').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabPromoBanner5" value="on"'. Configuration::get('PS_SA_NewTabPromoBanner5') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextPromoBanner5" value="'.Configuration::get('PS_SA_TextPromoBanner5').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner5"  value= "picturePromoBanner5" '. $picturePromoBanner5 .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner5[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner5"  value= "swfPromoBanner5" '. $swfPromoBanner5 .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner5[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthPromoBanner5" value="'.Configuration::get('PS_SA_WidthPromoBanner5').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightPromoBanner5" value="'.Configuration::get('PS_SA_HeightPromoBanner5').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner5"  value= "htmlPromoBanner5" '. $htmlPromoBanner5 .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlPromoBanner5" rows="7" cols="70">'.Configuration::get('PS_SA_htmlPromoBanner5').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayPromoBanner5" value="on"'. Configuration::get('PS_SA_DisplayPromoBanner5') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="promoBanner5" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner5(($picturePromoBanner5 == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';

        /*======================================================PromoBanner6==============================================*/
        $this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/footer.gif" alt="" title="" />'.$this->l('PromoBanner6').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkPromoBanner6" value="'.Configuration::get('PS_SA_LinkPromoBanner6').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabPromoBanner6" value="on"'. Configuration::get('PS_SA_NewTabPromoBanner6') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextPromoBanner6" value="'.Configuration::get('PS_SA_TextPromoBanner6').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner6"  value= "picturePromoBanner6" '. $picturePromoBanner6 .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner6[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner6"  value= "swfPromoBanner6" '. $swfPromoBanner6 .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner6[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthPromoBanner6" value="'.Configuration::get('PS_SA_WidthPromoBanner6').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightPromoBanner6" value="'.Configuration::get('PS_SA_HeightPromoBanner6').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner6"  value= "htmlPromoBanner6" '. $htmlPromoBanner6 .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlPromoBanner6" rows="7" cols="70">'.Configuration::get('PS_SA_htmlPromoBanner6').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayPromoBanner6" value="on"'. Configuration::get('PS_SA_DisplayPromoBanner6') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="promoBanner6" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner6(($picturePromoBanner6 == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';

        /*======================================================PromoBanner7==============================================*/
        $this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/footer.gif" alt="" title="" />'.$this->l('PromoBanner7').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkPromoBanner7" value="'.Configuration::get('PS_SA_LinkPromoBanner7').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabPromoBanner7" value="on"'. Configuration::get('PS_SA_NewTabPromoBanner7') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextPromoBanner7" value="'.Configuration::get('PS_SA_TextPromoBanner7').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner7"  value= "picturePromoBanner7" '. $picturePromoBanner7 .' />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner7[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner7"  value= "swfPromoBanner7" '. $swfPromoBanner7 .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner7[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthPromoBanner7" value="'.Configuration::get('PS_SA_WidthPromoBanner7').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightPromoBanner7" value="'.Configuration::get('PS_SA_HeightPromoBanner7').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner7"  value= "htmlPromoBanner7" '. $htmlPromoBanner7 .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlPromoBanner7" rows="7" cols="70">'.Configuration::get('PS_SA_htmlPromoBanner7').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayPromoBanner7" value="on"'. Configuration::get('PS_SA_DisplayPromoBanner7') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="promoBanner7" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner7(($picturePromoBanner7 == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';

        /*======================================================PromoBanner8==============================================*/
        $this->_html .= '
		<fieldset><legend><img src="'.$this->_path.'img/footer.gif" alt="" title="" />'.$this->l('PromoBanner8').'</legend>
			<table border="0" width="900" cellpadding="3" cellspacing="0">
				<tr>
					<td width="500">
						<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20"><img src="'.$this->_path.'img/link.gif" /></td>
									<td width="100"><b>'.$this->l('Link').'</b></td>
									<td colspan = "5">
										<input type="text" name="LinkPromoBanner8" value="'.Configuration::get('PS_SA_LinkPromoBanner8').'" size="60"/>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type="checkbox" name="NewTabPromoBanner8" value="on"'. Configuration::get('PS_SA_NewTabPromoBanner8') .' />
									</td>
										<td colspan=4">'.$this->l('Open in a new tab or new window'). '
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr style="margin-top: 10px;">
									<td width="20"></td>
									<td width="100"><b>Текст</b></td>
									<td colspan = "5">
										<input type="text" name="TextPromoBanner8" value="'.Configuration::get('PS_SA_TextPromoBanner8').'" size="60"/>
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td width="20"><img src="'.$this->_path.'img/picture.gif" /></td>
									<td width="100"><b>'.$this->l('File').'</b></td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner8"  value= "picturePromoBanner8" '. $picturePromoBanner8 .'" />
									</td>
									<td colspan="4">'.$this->l('.jpeg, .jpg, .png, .bmp or .gif') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner8[]" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner8"  value= "swfPromoBanner8" '. $swfPromoBanner8 .' />
									</td>
									<td colspan="4">
										' .$this->l('.swf (Flash)') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<input type="file" name="FilesPromoBanner8[]" />
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td width="90">
										'. $this->l('Width:') .'
									</td>
									<td width="90">
										<input type="text" name="WidthPromoBanner8" value="'.Configuration::get('PS_SA_WidthPromoBanner8').'"/>
									</td>
									<td width="90">
										'. $this->l('Height:') .'
									</td>
									<td width="90">
										<input type="text" name="HeightPromoBanner8" value="'.Configuration::get('PS_SA_HeightPromoBanner8').'" />
									</td>
								</tr>
								<tr><td>&nbsp;</td></tr>
								<tr>
									<td colspan="2">&nbsp;</td>
									<td width="20">
										<input type = "Radio" Name ="AddTypePromoBanner8"  value= "htmlPromoBanner8" '. $htmlPromoBanner8 .' />
									</td>
									<td colspan="4">
										' .$this->l('.html file') .'
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
									<td colspan="4">
										<textarea name="PS_SA_htmlPromoBanner8" rows="7" cols="70">'.Configuration::get('PS_SA_htmlPromoBanner8').'</textarea>
									</td>
								</tr>
								<tr>
							</table>
							<table border="0" width="500" cellpadding="3" cellspacing="0">
								<tr>
									<td width="20">
										<input type="checkbox" name="DisplayPromoBanner8" value="on"'. Configuration::get('PS_SA_DisplayPromoBanner8') .' />
									</td>
									<td>
										<b>'.$this->l('Display').'</b>
									</td>
								</tr>
								<tr>
									<td width="500" colspan="2">
										<center><img src="'.$this->_path.'img/save.gif" /><input type="submit" name="promoBanner8" value="'.$this->l('Save settings').'" class="button" /></center>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
				<tr>
					<td width="400" valign="top">
						<center><b>'.$this->l('Preview').'</b><br/><br/>
						'.$this->hookDisplayPromoBanner8(($picturePromoBanner8 == 'checked' ? array('img'=>1) : array())) .'
						</center>
					</td>
				</tr>
			</table>
		</fieldset>';
		$this->_html .='<br/><br/>';
	}
}


?>
