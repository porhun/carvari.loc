/**
 * Created by sergg on 11.05.2015.
 */
var checkLog = false, t;
var checkLogImg = false, ti;

$(document).ready(function(){
    var logBlock = "<span style=\'margin-left: 40px;\'class=\'js_log_block c_green\'>"+log_block_msg+"</span>";
    var logBlockAll = "<span style=\'margin-left: 40px;\'class=\'js_log_block_all c_green\'>"+log_block_all_msg+"</span>";
    var logBlockQ = "<span style=\'margin-left: 40px;\'class=\'js_log_block_q c_green\'></span>";
    var logBlockProduct = "<span style=\'margin-left: 40px;\'class=\'js_log_block_p c_green\'></span>";
    var logBlockImg = "<span style=\'margin-left: 40px;\'class=\'js_log_block_img c_green\'></span>";
    $(".js_parse_products").after(logBlock);
    $(".js_parse_all_products").after(logBlockAll);
    $(".js_quantity_adjust").after(logBlockQ);
    $(".js_product_adjust").after(logBlockProduct);
    $(".js_parse_images").after(logBlockImg);
    $(".js_parse_products").on("click",function(){
        if (confirm("Вы уверены в том, что хотите обновить состояние товаров на текущий день ?")){
            $(".js_log_block").text("Старт импорта...");
            parseProducts();
            t = setInterval(function(){
                checkLog = true;
                checkParseLog("");
            },10000);
        }
        return false;
    });

    $(".js_parse_all_products").on("click",function(){
        if (confirm("Вы уверены в том, что хотите сделать полный импорт товаров ? Это займет много времени!")){
            $(".js_log_block_all").text("Старт импорта всех продуктов...");
            parseAllProducts();
            t = setInterval(function(){
                checkLog = true;
                checkParseLog("_all");
            },10000);
        }
        return false;
    });

    $(".js_quantity_adjust").on("click",function(){
        if (confirm("Вы уверены в том, что хотите сделать синхронизацию количества товара в интрнет магазине?")){
            $(".js_log_block_q").text("Старт синхронизации...");
            adjustQuantity();
        }
        return false;
    });

    $(".js_product_adjust").on("click",function(){
        if (confirm("Вы уверены в том, что хотите сделать синхронизацию товаров в интрнет магазине?")){
            $(".js_log_block_p").text("Старт синхронизации...");
            adjustProduct();
            t = setInterval(function(){
                checkLog = true;
                checkParseLog("_p");
            },10000);
        }
        return false;
    });

    $(".js_parse_images").on("click",function(){
        $(".js_log_block_img").text("Старт импорта и нарезки...");
        CountImageCopy();
        parseImages();
        ti = setInterval(function(){
            checkLogImg = true;
            checkParseLogImg();
        },10000);
        return false;
    });
});
function CountImageCopy() //подсчёт картинок для копирования//без неё будет быстрее работать
{
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : {ajaxCountImgCopy : true},
        "success" : function(rsp)
        {
            $(".js_log_block_img").html("Осталось добавить <span class=\'js_curr_count_img\'>0 картинок</span> из "+rsp.count_all);
        }
    });
}
function parseImages(){
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : {ajaxImportImage : true},
        "beforeSend" : function(msg){
            $(".js_parse_images").attr("disabled", "disabled");
        },
        "success" : function(rsp){
            clearInterval(ti);
            $(".js_log_block_img").text(rsp.msg);
        },
        "complete" : function(jqXHR, textStatus){
            $(".js_parse_images").removeAttr("disabled");
        }
    });
}
function parseProducts(){
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : {ajaxImport : true},
        "success" : function(rsp){
            clearInterval(t);
            $(".js_log_block").text(rsp.msg);
        }
    });
}
function parseAllProducts(){
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : {ajaxImportAll : true},
        "success" : function(rsp){
            clearInterval(t);
            $(".js_log_block").text(rsp.msg);
        }
    });
}
function adjustQuantity(){
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : {ajaxUpdateProductsQ : true},
        "success" : function(rsp){
            $(".js_log_block_q").text(rsp.msg);
        }
    });
}
function adjustProduct(){
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : {ajaxUpdateProducts : true},
        "success" : function(rsp){
            $(".js_log_block_p").text(rsp.msg);
        }
    });
}
function checkParseLog(block){
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : { ajaxGetImportLog : true , allProducts : block},
        "success" : function(rsp){
            if(rsp.status == "stop" || (typeof rsp.end_product && rsp.end_product == true)){
                clearInterval(t);
            }
            $(".js_log_block"+block).text(rsp.msg);
        }
    });
}
function checkParseLogImg()
{
    $.ajax({
        "url" : window.location.href,
        "type" : "POST",
        "dataType" : "JSON",
        "data" : {ajaxGetImportLogImg : true},
        "success" : function(rsp)
        {
            var end_word = "ок";
            var cnt = (rsp.count).toString();
            var lastChar = cnt.charAt(cnt.length - 1);

            if(lastChar == "1" && cnt != "11")
                end_word = "ка";
            else if(cnt.charAt(cnt.length - 2) != "1" && (lastChar == "2" || lastChar == "3" || lastChar == "4"))
                end_word = "ки";
            $(".js_curr_count_img").text(rsp.count+" картин"+end_word);
            if(rsp.status == "stop")
            {
                clearInterval(ti);
            }
        }
    });
}