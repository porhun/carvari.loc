<?php
set_time_limit (0);
if (!defined('_PS_VERSION_'))
    exit;

/**
 * @return float microtime
 */
//function microtime_float()
//{
//    list($usec, $sec) = explode(" ", microtime());
//    return ((float)$usec + (float)$sec);
//}

class importcsv2 extends Module
{
    public static $column_mask;
    public $separator;
    public $multiple_value_separator;

    public $ftp_file_path_all = 'ftp://ftp.carvari.com/FTP/Gianni_Sites/Carvari/Carvari_rem/Carvari_rem_multilanguage.csv';//'Parpara_rem_ru.csv';
    public $csvfilename_all ="Carvari_rem_multilanguage.csv";
    public $ftp_file_path = 'ftp://ftp.carvari.com/FTP/Gianni_Sites/Carvari/Carvari_rem/Carvari_rem_multilanguage_changes.csv';//'Parpara_rem_ru.csv';
    public $csvfilename ="Carvari_rem_multilanguage_changes.csv";


    public $entities = array();

    public $available_fields = array();

    public $required_fields = array();

    public $cache_image_deleted = array();

    public static $default_values = array();

    public static $validators = array(
        'active' => array('self', 'getBoolean'),
        'tax_rate' => array('self', 'getPrice'),
        /** Tax excluded */
        'price_tex' => array('self', 'getPrice'),
        /** Tax included */
        'price_tin' => array('self', 'getPrice'),
        'reduction_price' => array('self', 'getPrice'),
        'reduction_percent' => array('self', 'getPrice'),
        'wholesale_price' => array('self', 'getPrice'),
        'ecotax' => array('self', 'getPrice'),
        'name' => array('self', 'createMultiLangField'),
        'description' => array('self', 'createMultiLangField'),
        'description_short' => array('self', 'createMultiLangField'),
        'meta_title' => array('self', 'createMultiLangField'),
        'meta_keywords' => array('self', 'createMultiLangField'),
        'meta_description' => array('self', 'createMultiLangField'),
        'link_rewrite' => array('self', 'createMultiLangField'),
        'available_now' => array('self', 'createMultiLangField'),
        'available_later' => array('self', 'createMultiLangField'),
        'category' => array('self', 'split'),
        'online_only' => array('self', 'getBoolean'),
    );

    const MAX_COLUMNS = 10;
    const MAX_LINE_SIZE = 1000;

    private $hooks = array();

    function __construct()
    {

        $this->name = 'importcsv2';
        $this->tab = 'front_office_features';
        $this->version = '1';
        $this->author = 'ForForce';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');

        parent::__construct();

        $this->displayName = $this->l('Забирает данные по ftp в формате CSV');
        $this->description = $this->l('Забирает данные по ftp в формате CSV и ипортирует в базу');
        $this->context->smarty->assign('module_name', $this->name);

        $this->bootstrap = true;



        // @since 1.5.0
        if (Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
        {
            $this->entities = array_merge(
                $this->entities,
                array(
                    $this->l('Supply Orders'),
                    $this->l('Supply Order Details'),
                )
            );
        }

        self::$validators['image'] = array(
            'AdminImportController',
            'split'
        );

        $this->available_fields = array(
            'no' => array('label' => $this->l('Ignore this column')),
            'id' => array('label' => $this->l('ID')),
            'active' => array('label' => $this->l('Active (0/1)')),
            'name' => array('label' => $this->l('Name *')),
            'category' => array('label' => $this->l('Categories (x,y,z...)')),
            'price_tex' => array('label' => $this->l('Price tax excluded')),
            'price_tin' => array('label' => $this->l('Price tax included')),
            'id_tax_rules_group' => array('label' => $this->l('Tax rules ID')),
            'wholesale_price' => array('label' => $this->l('Wholesale price')),
            'on_sale' => array('label' => $this->l('On sale (0/1)')),
            'reduction_price' => array('label' => $this->l('Discount amount')),
            'reduction_percent' => array('label' => $this->l('Discount percent')),
            'reduction_from' => array('label' => $this->l('Discount from (yyyy-mm-dd)')),
            'reduction_to' => array('label' => $this->l('Discount to (yyyy-mm-dd)')),
            'reference' => array('label' => $this->l('Reference #')),
            'supplier_reference' => array('label' => $this->l('Supplier reference #')),
            'supplier' => array('label' => $this->l('Supplier')),
            'manufacturer' => array('label' => $this->l('Manufacturer')),
            'ean13' => array('label' => $this->l('EAN13')),
            'upc' => array('label' => $this->l('UPC')),
            'ecotax' => array('label' => $this->l('Ecotax')),
            'width' => array('label' => $this->l('Width')),
            'height' => array('label' => $this->l('Height')),
            'depth' => array('label' => $this->l('Depth')),
            'weight' => array('label' => $this->l('Weight')),
            'quantity' => array('label' => $this->l('Quantity')),
            'minimal_quantity' => array('label' => $this->l('Minimal quantity')),
            'visibility' => array('label' => $this->l('Visibility')),
            'additional_shipping_cost' => array('label' => $this->l('Additional shipping cost')),
            'unity' => array('label' => $this->l('Unity')),
            'unit_price_ratio' => array('label' => $this->l('Unit price ratio')),
            'description_short' => array('label' => $this->l('Short description')),
            'description' => array('label' => $this->l('Description')),
            'tags' => array('label' => $this->l('Tags (x,y,z...)')),
            'meta_title' => array('label' => $this->l('Meta title')),
            'meta_keywords' => array('label' => $this->l('Meta keywords')),
            'meta_description' => array('label' => $this->l('Meta description')),
            'link_rewrite' => array('label' => $this->l('URL rewritten')),
            'available_now' => array('label' => $this->l('Text when in stock')),
            'available_later' => array('label' => $this->l('Text when backorder allowed')),
            'available_for_order' => array('label' => $this->l('Available for order (0 = No, 1 = Yes)')),
            'available_date' => array('label' => $this->l('Product available date')),
            'date_add' => array('label' => $this->l('Product creation date')),
            'show_price' => array('label' => $this->l('Show price (0 = No, 1 = Yes)')),
            'image' => array('label' => $this->l('Image URLs (x,y,z...)')),
            'delete_existing_images' => array(
                'label' => $this->l('Delete existing images (0 = No, 1 = Yes)')
            ),
            'features' => array('label' => $this->l('Feature(Name:Value:Position:Customized)')),
            'online_only' => array('label' => $this->l('Available online only (0 = No, 1 = Yes)')),
            'condition' => array('label' => $this->l('Condition')),
            'customizable' => array('label' => $this->l('Customizable (0 = No, 1 = Yes)')),
            'uploadable_files' => array('label' => $this->l('Uploadable files (0 = No, 1 = Yes)')),
            'text_fields' => array('label' => $this->l('Text fields (0 = No, 1 = Yes)')),
            'out_of_stock' => array('label' => $this->l('Action when out of stock')),
            'shop' => array(
                'label' => $this->l('ID / Name of shop'),
                'help' => $this->l('Ignore this field if you don\'t use the Multistore tool. If you leave this field empty, the default shop will be used.'),
            ),
            'advanced_stock_management' => array(
                'label' => $this->l('Advanced Stock Management'),
                'help' => $this->l('Enable Advanced Stock Management on product (0 = No, 1 = Yes).')
            ),
            'depends_on_stock' => array(
                'label' => $this->l('Depends on stock'),
                'help' => $this->l('0 = Use quantity set in product, 1 = Use quantity from warehouse.')
            ),
            'warehouse' => array(
                'label' => $this->l('Warehouse'),
                'help' => $this->l('ID of the warehouse to set as storage.')
            ),
        );

        self::$default_values = array(
//                    'id_category' => array((int)Configuration::get('PS_HOME_CATEGORY')),
            'id_category' => 958,
//                    'id_category_default' => (int)Configuration::get('PS_HOME_CATEGORY'),
            'active' => '1',
            'width' => 0.000000,
            'height' => 0.000000,
            'depth' => 0.000000,
            'weight' => 0.000000,
            'visibility' => 'both',
            'additional_shipping_cost' => 0.00,
            'unit_price_ratio' => 0.000000,
            'quantity' => 0,
            'minimal_quantity' => 1,
            'price' => 0,
            'id_tax_rules_group' => 0,
            'description_short' => array((int)Configuration::get('PS_LANG_DEFAULT') => ''),
            'link_rewrite' => array((int)Configuration::get('PS_LANG_DEFAULT') => ''),
            'online_only' => 0,
            'condition' => 'new',
            'available_date' => date('Y-m-d'),
            'date_add' => date('Y-m-d H:i:s'),
            'customizable' => 0,
            'uploadable_files' => 0,
            'text_fields' => 0,
            'out_of_stock' => '2',
            'advanced_stock_management' => 0,
            'depends_on_stock' => 0,
        );



        $this->separator = ($separator = Tools::substr(strval(trim(Tools::getValue('separator'))), 0, 1)) ? $separator :  ',';
        $this->multiple_value_separator = ($separator = Tools::substr(strval(trim(Tools::getValue('multiple_value_separator'))), 0, 1)) ? $separator :  '|';
    }



    public function install()
    {
        return (parent::install()
            && Configuration::updateValue('importcsv2', 'importCSV')
            && Configuration::updateValue('importcsv2_all_pid', '7777777')
            && Configuration::updateValue('importcsv2_pid', '7777777')
            && Configuration::updateValue('importcsv2_trigger', 0)
        );

    }

    public function uninstall()
    {

        return (parent::uninstall()
            && Configuration::deleteByName('importcsv2')
            && Configuration::deleteByName('importcsv2_all_pid')
            && Configuration::deleteByName('importcsv2_pid')
            && Configuration::deleteByName('importcsv2_trigger')
        );
    }

    private function trim($arg) {
        return trim(trim($arg),'"');
    }

    private function deactivateProductsWithoutImg(){
        DB::getInstance()->execute('UPDATE
              '._DB_PREFIX_.'product AS pr
              LEFT JOIN '._DB_PREFIX_.'image i
                ON (
                  pr.id_product = i.`id_product`
                ) SET pr.`active` = 0
            WHERE i.`id_image` IS NULL');

        DB::getInstance()->execute('UPDATE
              '._DB_PREFIX_.'product_shop AS pr
              LEFT JOIN '._DB_PREFIX_.'image i
                ON (
                  pr.id_product = i.`id_product`
                ) SET pr.`active` = 0
            WHERE i.`id_image` IS NULL');
    }

    private function reactivateProductsWithImg(){
        DB::getInstance()->execute('UPDATE
              '._DB_PREFIX_.'product AS pr
              LEFT JOIN '._DB_PREFIX_.'image i
                ON (
                  pr.id_product = i.`id_product`
                ) SET pr.`active` = 1
            WHERE i.`id_image` IS NOT NULL');

        DB::getInstance()->execute('UPDATE
              '._DB_PREFIX_.'product_shop AS pr
              LEFT JOIN '._DB_PREFIX_.'image i
                ON (
                  pr.id_product = i.`id_product`
                ) SET pr.`active` = 1
            WHERE i.`id_image` IS NOT NULL');
    }
    private function writeParseLog($msg , $all = null){
        if ($all)
            file_put_contents($this->getLogFileNameAll(),serialize($msg).PHP_EOL,FILE_APPEND);
        else
            file_put_contents($this->getLogFileName(),serialize($msg).PHP_EOL,FILE_APPEND);
    }
    public function shutdownParseFileHelper() {
        $e = error_get_last();
        if ($e && $e['type'] == 1) {
            ob_start();
            $o = ob_get_clean();
            file_put_contents($this->getLogFileName(),array('status'=>'error','msg'=>$o));
        }
    }
    private function getLogFileName(){
        return __DIR__.'/temp/log_'.date('Y-m-d').'.log';
    }
    private function getLogFileNameAll(){
        return __DIR__.'/temp/log_all_'.date('Y-m-d').'.log';
    }
    private function getLastImportInfo( $all = null, $return_bool = null ){
        if ( $last_line = $this->readLastLine( $all ? $this->getLogFileNameAll() : $this->getLogFileName()) ){
            $info_arr = unserialize( $last_line );
            if ($info_arr['status'] == 'stop')
                return 'Дата последнего импорта: '.$info_arr['date_start'].'. Время затраченное на ипорт: '.$info_arr['time_parse'].'. '.$info_arr['msg'];
            else
                return '<b>Внимание!!!</b> В данный момент импорт делается другим процессом. '.$info_arr['msg'];
        } else
            return 'Импорт никогда не инициализировался.';
    }

    public function getTmpParseFileName(){
        return __DIR__.'/temp/'.$this->csvfilename;
    }

    public function getTmpParseFileNameAll(){
        return __DIR__.'/temp/'.$this->csvfilename_all;
    }

    public static function readLastLine($fileName)
    {
        if ($lines  = @file($fileName)){
            $lastLine = array_pop($lines);
            return $lastLine;
        } else {
            return null;
        }
    }

    private function getLastElementLog($file){
        if( !file_exists($file) ){
            return array('status'=>'start','msg'=>'Файл лога не найден');
        }else{
            $log_file = file_get_contents($file);
            $arr_log = explode(PHP_EOL, $log_file);
            $end_element= "";
            if(is_array($arr_log) ){
                $cnt_arr_log = count($arr_log);
                $end_index = $cnt_arr_log-2;
                $end_element = $arr_log[$end_index];
            }

            if( !empty($end_element) )
                $parse_log_result = unserialize($end_element);
            else
                return array('status'=>'stop','msg'=>'Импорт закончен');
        }
        return $parse_log_result;
    }

    public function quietImport(){
        if (Configuration::get('importcsv2_all_pid') != '7777777' || Configuration::get('importcsv2_pid') != '7777777')
        {
            if (file_exists( '/proc/'.Configuration::get('importcsv2_all_pid') ) || file_exists( '/proc/'.Configuration::get('importcsv2_pid') )) {
                die('Процесс импорта уже запущен.');
            }
        }
        if( file_exists($this->getTmpParseFileName()) ){
//               die(json_encode(array('status'=>'error','msg'=>'Дождитесь завершения импорта текущего файла')));
        }
        // объявление переменных
        $local_file_path = __DIR__.'/temp/';
        @unlink($local_file_path.$this->csvfilename);
        $cmd = 'wget --user=gianni_sites --password=5IOOwXjavuqysxb -P '.$local_file_path.' '.$this->ftp_file_path;
        $r = exec($cmd, $a, $b);
//            copy(__DIR__.'/'.$this->csvfilename,$this->getTmpParseFileName()); // это для теста на локали, потом удалить и раскоментить вгет
        $file_name = $this->getTmpParseFileName();
        if( !is_file($file_name) ){
            die('Ошибка загрузки по ftp');
        }
        copy($file_name,__DIR__.'/'.$this->csvfilename);
        if( file_exists($this->getLogFileName()) ){
            unlink($this->getLogFileName());
        }
        if ($this->productImportNew('csvfilename'))
            return true;
        else
            return false;

    }

    public function updateImages(){
        $shops = array(1);
        $products = Db::getInstance()->executeS("SELECT id_product FROM " . _DB_PREFIX_ . "product");

        $images_arr = array(
            array(
                array('/themes/carvari-theme/img/test/2_01.jpg', 1),
                array('/themes/carvari-theme/img/test/2_02.jpg', 0),
                array('/themes/carvari-theme/img/test/2_03.jpg', 0),
                array('/themes/carvari-theme/img/test/2_04.jpg', 0),
            ),
            array(
                array('/themes/carvari-theme/img/test/0218E-158B_01.jpg', 1),
                array('/themes/carvari-theme/img/test/0218E-158B_02.jpg', 0),
                array('/themes/carvari-theme/img/test/0218E-158B_03.jpg', 0),
                array('/themes/carvari-theme/img/test/0218E-158B_04.jpg', 0),
            ),
            array(
                array('/themes/carvari-theme/img/test/3_01.jpg', 1),
                array('/themes/carvari-theme/img/test/3_02.jpg', 0),
                array('/themes/carvari-theme/img/test/3_03.jpg', 0),
                array('/themes/carvari-theme/img/test/3_04.jpg', 0),
            ),
            array(
                array('/themes/carvari-theme/img/test/006-1_01.jpg', 1),
                array('/themes/carvari-theme/img/test/006-1_02.jpg', 0),
                array('/themes/carvari-theme/img/test/006-1_03.jpg', 0),
                array('/themes/carvari-theme/img/test/006-1_04.jpg', 0),
            ),
            array(
                array('/themes/carvari-theme/img/test/8C13-202-1_01.jpg', 1),
                array('/themes/carvari-theme/img/test/8C13-202-1_02.jpg', 0),
                array('/themes/carvari-theme/img/test/8C13-202-1_03.jpg', 0),
                array('/themes/carvari-theme/img/test/8C13-202-1_04.jpg', 0),
            ),
            array(
                array('/themes/carvari-theme/img/test/13A74-502_01.jpg', 1),
                array('/themes/carvari-theme/img/test/13A74-502_02.jpg', 0),
                array('/themes/carvari-theme/img/test/13A74-502_03.jpg', 0),
                array('/themes/carvari-theme/img/test/13A74-502_04.jpg', 0),
            ),

        );

        $base = Tools::getHttpHost(true);

        $i = 0;
        foreach ($products as $id_pr) {
            $product = new Product($id_pr['id_product']);


            foreach ( $images_arr[$i] as $image_url) {
                $url = $base.$image_url[0];
                $url = str_replace(' ', '%20', $url);
                $image = new Image();
                $image->id_product = (int)$product->id;
                $image->position = Image::getHighestPosition($product->id) + 1;
                $image->cover = ($image_url[1]) ? true : false;
                // file_exists doesn't work with HTTP protocol
                if (($field_error = $image->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                    ($lang_field_error = $image->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $image->add())
                {
                    // associate image to selected shops
                    $image->associateTo($shops);
                    if (!self::copyImgFromURL($product->id, $image->id, $url, 'products'))
                    {
                        d($url);
                        $image->delete();
                        $this->warnings[] = sprintf(Tools::displayError('Error copying image: %s'), $url);
                    }
                }
                else
                    $error = true;
            }

            $i++;
            if ($i==6)
                $i = 0;

        }

    }



    protected static function copyImgFromURL($id_entity, $id_image = null, $url, $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity)
        {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_.(int)$id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_.(int)$id_entity;
                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_.(int)$id_entity;
                break;
        }

//        $url = str_replace(' ', '%20', trim($url));
//        $url = urldecode($url);
        $parced_url = $url;

//        if (isset($parced_url['path']))
//        {
//            $uri = ltrim($parced_url['path'], '/');
//            $parts = explode('/', $uri);
//            foreach ($parts as &$part)
//                $part = urlencode ($part);
//            unset($part);
//            $parced_url['path'] = '/'.implode('/', $parts);
//        }
//
//        if (isset($parced_url['query']))
//        {
//            $query_parts = array();
//            parse_str($parced_url['query'], $query_parts);
//            $parced_url['query'] = http_build_query($query_parts);
//        }
//
//        if (!function_exists('http_build_url'))
//            require_once(_PS_TOOL_DIR_.'http_build_url/http_build_url.php');
//
//        $url = http_build_url('', $parced_url);

        // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
        if (!ImageManager::checkImageMemoryLimit($url))
            return false;

        $orig_tmpfile = $tmpfile;

        // 'file_exists' doesn't work on distant file, and getimagesize makes the import slower.
        // Just hide the warning, the processing will be the same.
        if (Tools::copy($url, $tmpfile))
        {
//           d('here');
            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path.'.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                $src_width, $src_height);
            $images_types = ImageType::getImagesTypes($entity, true);

            if ($regenerate)
            {
                $previous_path = null;
                $path_infos = array();
                $path_infos[] = array($tgt_width, $tgt_height, $path.'.jpg');
                foreach ($images_types as $image_type)
                {
                    $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize($tmpfile, $path.'-'.stripslashes($image_type['name']).'.jpg', $image_type['width'],
                        $image_type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                        $src_width, $src_height))
                    {
                        // the last image should not be added in the candidate list if it's bigger than the original image
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height)
                            $path_infos[] = array($tgt_width, $tgt_height, $path.'-'.stripslashes($image_type['name']).'.jpg');
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types))
                        Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                }
            }
        }
        else
        {
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);
        return true;
    }

    private static function get_best_path($tgt_width, $tgt_height, $path_infos)
    {
        $path_infos = array_reverse($path_infos);
        $path = '';
        foreach($path_infos as $path_info)
        {
            list($width, $height, $path) = $path_info;
            if ($width >= $tgt_width && $height >= $tgt_height)
                return $path;
        }
        return $path;
    }

    public function fixDefaultCategories() {
        Db::getInstance()->execute('UPDATE `'. _DB_PREFIX_ .'product_shop` pp INNER JOIN `'. _DB_PREFIX_ .'category_product` pcp ON pcp.`id_product`=pp.`id_product` SET pp.`id_category_default`=pcp.`id_category`');
        Db::getInstance()->execute('UPDATE `'. _DB_PREFIX_ .'product` pp INNER JOIN `'. _DB_PREFIX_ .'category_product` pcp ON pcp.`id_product`=pp.`id_product` SET pp.`id_category_default`=pcp.`id_category`');
    }

    public function importImagesFF() {

        $shops = array(1);
        $catalog = __DIR__."/image_products";

        $allfiles_count = count(array_diff(scandir($catalog), array('.', '..')));

        $sql = "SELECT ean13, reference FROM " . _DB_PREFIX_ . "product";
        $row = Db::getInstance()->executeS($sql);
//                добавляем картинки
        $cover = 0;
        if(is_dir($catalog))
        {
            $our_all_img = 0;
            foreach($row as $good_ref) {
                $idpr = null;
                $allfiles = array_diff(scandir($catalog), array('.', '..'));
                $reference_clean = '';
                $reference_original = $good_ref['reference'];
                preg_match('/(.*)_[^_]*$/', $good_ref['reference'], $reference_clean);
                $good_ref['reference'] = $reference_clean[1] ? $reference_clean[1] : $reference_original;


                foreach ($allfiles as $file) {


                    $countpr = 0;
                    $fileoriginal = $file;
                    $file = explode('.', $file);
                    $file = $file[0];

                    // отрезаем последние 2 символа
                    if (substr($file, -3) == '_01') {
                        $cover = 1;
                        $file = substr($file, 0, -3);
                    } else {
                        $file = substr($file, 0, -3);
                        $cover = 0;
                    }
                    // если название артикула файла соответствует артикулу товара, то добавляем эту картинку
                    if (mb_strtolower($good_ref['reference']) == mb_strtolower($file)) {





                        // достанем id товара с найденным уникальным кодом
                        $sql = "SELECT * FROM " . _DB_PREFIX_ . "product WHERE ean13='" . $good_ref['ean13'] . "'";
                        $sqla = "SELECT * FROM " . _DB_PREFIX_ . "product_attribute WHERE ean13='" . $good_ref['ean13'] . "'";

                        if ($row = Db::getInstance()->getRow($sql)) {
                            $idpr = $row['id_product'];
                            // посчитаем сколько картинок у этого товара
                            $sql_count_pr_img = "SELECT COUNT(*) FROM " . _DB_PREFIX_ . "image WHERE id_product='" . $idpr . "'";
                            $countpr = Db::getInstance()->getValue($sql_count_pr_img);
                        } elseif ($row = Db::getInstance()->getRow($sqla)) {
                            $idpr = $row['id_product'];
                            // посчитаем сколько картинок у этого товара
                            $sql_count_pr_img = "SELECT COUNT(*) FROM " . _DB_PREFIX_ . "image WHERE id_product='" . $idpr . "'";
                            $countpr = Db::getInstance()->getValue($sql_count_pr_img);
                        }




                        // если id товара найден, то добавим картинку
                        if (!is_null($idpr)) {


                            $url = $catalog.'/'.$fileoriginal;
//                                $url = str_replace(' ', '%20', $url);
                            $image = new Image();
                            $image->id_product = $idpr;
                            $image->position = Image::getHighestPosition($idpr) + 1;
                            $image->cover = ($cover) ? true : false;
                            // file_exists doesn't work with HTTP protocol
                            if (($field_error = $image->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                                ($lang_field_error = $image->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $image->add())
                            {
                                // associate image to selected shops
                                $image->associateTo($shops);
                                if (self::copyImgFromURL($idpr, $image->id, $url, 'products'))
                                {
                                    $our_all_img++;
                                    unlink($url);
                                } else {
                                    $image->delete();
                                    $this->warnings[] = sprintf(Tools::displayError('Error copying image: %s'), $url);
                                }
                            }




//                                if ($cover) {
//                                    Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image SET cover=0 WHERE id_product='.(int)$idpr);
//                                    $ret_arr = Db::getInstance()->executeS('SELECT id_image FROM ' . _DB_PREFIX_ . 'image WHERE id_product='.(int)$idpr);
//                                    foreach($ret_arr as $arr){
//                                        $i_ids[] = $arr['id_image'];
//                                    }
//                                    Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image_shop SET cover=0 WHERE id_image IN ('.implode(',', $i_ids).')');
//                                }
//
//                                if (Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image (id_product, position, cover) VALUES (' . (int)$idpr . ',' . (int)($countpr + 1) . ','.$cover.')')) {
////                                        Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image_shop (id_product, id_shop, cover) VALUES (' . (int)$idpr . ',1,'.$cover.')');
//                                    $id_my_image = strval(Db::getInstance()->Insert_ID());
//
//                                    $bool_img_shop = Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image_shop (id_image, id_shop, cover)
//                                                            VALUES (' . (int)$id_my_image . ',1,'.$cover.')'); //у нас один магазин
//                                    $bool_img_lang = Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image_lang (id_image, id_lang, legend)
//                                                            VALUES (' . (int)$id_my_image . ',1,\'\')'); //у нас один магазин
//
//                                    if ($bool_img_shop && $bool_img_lang) {
//                                        $url = str_replace(' ', '%20', $catalog . "/" . $fileoriginal);//заменяем пустые символы c url
//
//                                        if ($this->copyImg($idpr, $id_my_image, $url, 'products')) {
////                                                @unlink($url);
//                                            $our_all_img++;
//                                        }
//                                    } else
//                                        die(json_encode(array('status' => 'stop', 'msg' => 'Некорректно занесено в БД. Запись остановлена на картинке с ID = ' . $id_my_image)));
//                                    //$result.= "Некорректно занесено в БД.";
//                                    //Hook::exec('actionWatermark', array('id_image' => $id_my_image, 'id_product' => $idpr));
//                                    /*$arr_path_img = array();
//                                    for($i = 0; $i < strlen($id_my_image); $i++)
//                                        $arr_path_img[] = $id_my_image[$i];
//                                    $path_img = $_SERVER['DOCUMENT_ROOT']."/img/p/".implode("/",$arr_path_img);
//                                    if(!file_exists($path_img))
//                                    {
//                                        mkdir($path_img, 0777, true);
//                                        chmod($path_img, 0777);
//                                    }
//                                    $arr_ext = explode(".",$file);*/
//                                    //copy($catalog ."/".$file, $path_img."/".$id_my_image.".".$arr_ext[1]);
//
//                                }
                            //$this->copyImg($idpr, null, $catalog ."/".$file,'products');
                        }
                    }
                }
            }
//                    удаляем добавленные файлы
//                    reset($au_good_ref);
//                    foreach($au_good_ref as $good_ref)
//                    {
//                        foreach(array_diff(scandir($catalog), array('.', '..')) as $file)
//                        {
//                            if(strrpos($file, $ean_art['art'])===0)
//                            {
//                                @unlink($catalog.$file);//потом раскомментировать
//                            }
//                        }
//                    }
            $this->deactivateProductsWithoutImg();
//            $this->reactivateProductsWithImg();


            die(json_encode(array('status'=>'ok','msg'=>'Импорт и нарезка завершены. Всего загружено '.((int)$our_all_img).' из '. $allfiles_count)));
        } else {
            die(json_encode(array('status'=>'err','msg'=>'Картинки для импорта - не найдены.')));
        }


        //$result = "Файл для импорта - не найден.";
        /*return "<div>HELLO</div>";*/
        //return $result;

    }


    public function getContent()
    {

//        $reference_clean = '';
//        $good_ref['reference'] = 'A680-301EA_білий_01';
//        $reference_original = $good_ref['reference'];
//        preg_match('/(.*)_[^_]*_.*$|(.*)_[^_]*$/', $good_ref['reference'], $reference_clean);
//        d($reference_clean);
//        $good_ref['reference'] = $reference_clean[1] ? $reference_clean[1] : $reference_original;
//
//        d($good_ref['reference']);


        $catalog = __DIR__."/image_products";
        if(Tools::isSubmit('ajaxCountImgCopy'))
        {
            if (is_dir($catalog)) {
                $allfiles = array_diff(scandir($catalog), array('.', '..'));
            }

            die(json_encode(array('count_all'=>count($allfiles))));
        }

        if(Tools::getValue('fixDefaultCategories'))
        {
            $this->fixDefaultCategories();
        }



        if(Tools::isSubmit('ajaxImportImage'))
        {
            $this->importImagesFF();

//            if(($handle = fopen(__DIR__."/".$this->csvfilename_all, "r")) !== false)//читаю файл импорта с папки модуля
//            {
//
//                $sql = "SELECT ean13, reference FROM " . _DB_PREFIX_ . "product";
//                $row = Db::getInstance()->executeS($sql);
////                добавляем картинки
//                $cover = 0;
//                if(is_dir($catalog))
//                {
//                    $our_all_img = 0;
//                    foreach($row as $good_ref) {
//                        $idpr = null;
//                        $allfiles = array_diff(scandir($catalog), array('.', '..'));
//                        $reference_clean = '';
//                        $reference_original = $good_ref['reference'];
//                        preg_match('/(.*)_[^_]*$/', $good_ref['reference'], $reference_clean);
//                        $good_ref['reference'] = $reference_clean[1] ? $reference_clean[1] : $reference_original;
//
//
//                        foreach ($allfiles as $file) {
//
//
//                            $countpr = 0;
//                            $fileoriginal = $file;
//                            $file = explode('.', $file);
//                            $file = $file[0];
//
//                            // отрезаем последние 2 символа
//                            if (substr($file, -3) == '_01') {
//                                $cover = 1;
//                                $file = substr($file, 0, -3);
//                            } else {
//                                $file = substr($file, 0, -3);
//                                $cover = 0;
//                            }
//                            // если название артикула файла соответствует артикулу товара, то добавляем эту картинку
//                            if (mb_strtolower($good_ref['reference']) == mb_strtolower($file)) {
//                                // достанем id товара с найденным уникальным кодом
//                                $sql = "SELECT * FROM " . _DB_PREFIX_ . "product WHERE ean13='" . $good_ref['ean13'] . "'";
//                                $sqla = "SELECT * FROM " . _DB_PREFIX_ . "product_attribute WHERE ean13='" . $good_ref['ean13'] . "'";
//
//                                if ($row = Db::getInstance()->getRow($sql)) {
//                                    $idpr = $row['id_product'];
//                                    // посчитаем сколько картинок у этого товара
//                                    $sql_count_pr_img = "SELECT COUNT(*) FROM " . _DB_PREFIX_ . "image WHERE id_product='" . $idpr . "'";
//                                    $countpr = Db::getInstance()->getValue($sql_count_pr_img);
//                                } elseif ($row = Db::getInstance()->getRow($sqla)) {
//                                    $idpr = $row['id_product'];
//                                    // посчитаем сколько картинок у этого товара
//                                    $sql_count_pr_img = "SELECT COUNT(*) FROM " . _DB_PREFIX_ . "image WHERE id_product='" . $idpr . "'";
//                                    $countpr = Db::getInstance()->getValue($sql_count_pr_img);
//                                }
//                                // если id товара найден, то добавим картинку
//                                if (!is_null($idpr)) {
//                                    if ($cover) {
//                                        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image SET cover=0 WHERE id_product='.(int)$idpr);
//                                        $ret_arr = Db::getInstance()->executeS('SELECT id_image FROM ' . _DB_PREFIX_ . 'image WHERE id_product='.(int)$idpr);
//                                        foreach($ret_arr as $arr){
//                                            $i_ids[] = $arr['id_image'];
//                                        }
//                                        Db::getInstance()->execute('UPDATE ' . _DB_PREFIX_ . 'image_shop SET cover=0 WHERE id_image IN ('.implode(',', $i_ids).')');
//                                    }
//
//                                    if (Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image (id_product, position, cover) VALUES (' . (int)$idpr . ',' . (int)($countpr + 1) . ','.$cover.')')) {
////                                        Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image_shop (id_product, id_shop, cover) VALUES (' . (int)$idpr . ',1,'.$cover.')');
//                                        $id_my_image = strval(Db::getInstance()->Insert_ID());
//
//                                        $bool_img_shop = Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image_shop (id_image, id_shop, cover)
//                                                            VALUES (' . (int)$id_my_image . ',1,'.$cover.')'); //у нас один магазин
//                                        $bool_img_lang = Db::getInstance()->execute('INSERT INTO ' . _DB_PREFIX_ . 'image_lang (id_image, id_lang, legend)
//                                                            VALUES (' . (int)$id_my_image . ',1,\'\')'); //у нас один магазин
//
//                                        if ($bool_img_shop && $bool_img_lang) {
//                                            $url = str_replace(' ', '%20', $catalog . "/" . $fileoriginal);//заменяем пустые символы c url
//
//                                            if ($this->copyImg($idpr, $id_my_image, $url, 'products')) {
////                                                @unlink($url);
//                                                $our_all_img++;
//                                            }
//                                        } else
//                                            die(json_encode(array('status' => 'stop', 'msg' => 'Некорректно занесено в БД. Запись остановлена на картинке с ID = ' . $id_my_image)));
//                                        //$result.= "Некорректно занесено в БД.";
//                                        //Hook::exec('actionWatermark', array('id_image' => $id_my_image, 'id_product' => $idpr));
//                                        /*$arr_path_img = array();
//                                        for($i = 0; $i < strlen($id_my_image); $i++)
//                                            $arr_path_img[] = $id_my_image[$i];
//                                        $path_img = $_SERVER['DOCUMENT_ROOT']."/img/p/".implode("/",$arr_path_img);
//                                        if(!file_exists($path_img))
//                                        {
//                                            mkdir($path_img, 0777, true);
//                                            chmod($path_img, 0777);
//                                        }
//                                        $arr_ext = explode(".",$file);*/
//                                        //copy($catalog ."/".$file, $path_img."/".$id_my_image.".".$arr_ext[1]);
//
//                                    }
//                                    //$this->copyImg($idpr, null, $catalog ."/".$file,'products');
//                                }
//                            }
//                        }
//                    }
////                    удаляем добавленные файлы
////                    reset($au_good_ref);
////                    foreach($au_good_ref as $good_ref)
////                    {
////                        foreach(array_diff(scandir($catalog), array('.', '..')) as $file)
////                        {
////                            if(strrpos($file, $ean_art['art'])===0)
////                            {
////                                @unlink($catalog.$file);//потом раскомментировать
////                            }
////                        }
////                    }
//                    fclose($handle);
//                    $sql_count_pr_img = "SELECT COUNT(*) FROM "._DB_PREFIX_."image";
//                    $this->context->cookie->begin_count_img = Db::getInstance()->getValue($sql_count_pr_img); //текущее количество картинок в БД
//
//                    die(json_encode(array('status'=>'ok','msg'=>'Импорт и нарезка завершены. Всего загружено '.((int)$our_all_img))));
//                } else {
//                    fclose($handle);
//                    die(json_encode(array('status'=>'err','msg'=>'Картинки для импорта - не найдены.')));
//                }
//
//            }
//            else
//                die(json_encode(array('status'=>'err','msg'=>'Файл с товарами для импорта - не найден.')));
//            //$result = "Файл для импорта - не найден.";
//            /*return "<div>HELLO</div>";*/
//            //return $result;
            $this->deactivateProductsWithoutImg();
//            $this->reactivateProductsWithImg();
        }
        if(Tools::isSubmit('ajaxGetImportLogImg'))
        {
            $context = Context::getContext();
            if (is_dir($catalog)) {
                $allfiles = array_diff(scandir($catalog), array('.', '..'));
            }

            $parsed = count($allfiles);

            die(json_encode(array('status'=>$context->cookie->parse_status,'count'=>$parsed)));
        }
        if(Tools::isSubmit('ajaxGetImportLog'))
        {
            if ($_POST['allProducts']== '_all' || $_POST['allProducts']== '_p') {
                $parse_log_result = $this->getLastElementLog($this->getLogFileNameAll());
                if( !file_exists($this->getTmpParseFileNameAll()) ){
                    $parse_log_result = array('status'=>'start','msg'=>'Идет загрузка CSV'.(isset($parse_log_result['msg']) ? ', '.$parse_log_result['msg'] : ''));
//              unlink($this->getLogFileName());
                }
                die(json_encode($parse_log_result));
            } else {
                $parse_log_result = $this->getLastElementLog($this->getLogFileName());
                if( !file_exists($this->getTmpParseFileName()) ){
                    $parse_log_result = array('status'=>'start','msg'=>'Идет загрузка CSV'.(isset($parse_log_result['msg']) ? ', '.$parse_log_result['msg'] : ''));
//              unlink($this->getLogFileName());
                }
                die(json_encode($parse_log_result));
            }

        }
        if(Tools::isSubmit('ajaxImport') || Tools::getIsset('ajaxImportTest'))
        {
            if( file_exists($this->getTmpParseFileName()) ){
//               die(json_encode(array('status'=>'error','msg'=>'Дождитесь завершения импорта текущего файла')));
            }
            // объявление переменных
            $local_file_path = __DIR__.'/temp/';
            @unlink($local_file_path.$this->csvfilename);
            $cmd = 'wget --user=gianni_sites --password=5IOOwXjavuqysxb -P '.$local_file_path.' '.$this->ftp_file_path;
            $r = exec($cmd, $a, $b);
//            copy(__DIR__.'/'.$this->csvfilename,$this->getTmpParseFileName()); // это для теста на локали, потом удалить и раскоментить вгет
            $file_name = $this->getTmpParseFileName();
            $result = array();
            if( !is_file($file_name) ){
                die(json_encode(array('status'=>'error','msg'=>'Ошибка загрузки по ftp')));
            }
            copy($file_name,__DIR__.'/'.$this->csvfilename);
            if( file_exists($this->getLogFileName()) ){
                unlink($this->getLogFileName());
            }
            $this->productImportNew('csvfilename');
            $parse_log_result = $this->getLastElementLog($this->getLogFileName());
            die(json_encode(array('status'=>'ok','msg'=>'Импорт завершен'.(isset($parse_log_result['msg']) ? ': '.$parse_log_result['msg'] : ''))));
        }
        if(Tools::isSubmit('ajaxImportAll'))
        {
            if( file_exists($this->getTmpParseFileNameAll()) ){
//               die(json_encode(array('status'=>'error','msg'=>'Дождитесь завершения импорта текущего файла')));
            }
            // объявление переменных
            $local_file_path = __DIR__.'/temp/';
            @unlink($local_file_path.$this->csvfilename_all);
            $cmd = 'wget --user=gianni_sites --password=5IOOwXjavuqysxb -P '.$local_file_path.' '.$this->ftp_file_path_all;
            $r = exec($cmd, $a, $b);
//            copy(__DIR__.'/'.$this->csvfilename,$this->getTmpParseFileName()); // это для теста на локали, потом удалить и раскоментить вгет
            $file_name = $this->getTmpParseFileNameAll();
            $result = array();
            if( !is_file($file_name) ){
                die(json_encode(array('status'=>'error','msg'=>'Ошибка загрузки по ftp')));
            }
            copy($file_name,__DIR__.'/'.$this->csvfilename_all);
            if( file_exists($this->getLogFileNameAll()) ){
                unlink($this->getLogFileNameAll());
            }
            $this->productImportNew('csvfilename_all', '_all');
            $parse_log_result = $this->getLastElementLog($this->getLogFileNameAll());
            die(json_encode(array('status'=>'ok','msg'=>'Импорт завершен'.(isset($parse_log_result['msg']) ? ': '.$parse_log_result['msg'] : ''))));
        }

        if(Tools::isSubmit('ajaxUpdateProductsQ'))
        {
            $local_file_path = __DIR__.'/temp/';
            @unlink($local_file_path.$this->csvfilename_all);
            $cmd = 'wget --user=gianni_sites --password=5IOOwXjavuqysxb -P '.$local_file_path.' '.$this->ftp_file_path_all;
            $r = exec($cmd, $a, $b);
//            copy(__DIR__.'/'.$this->csvfilename,$this->getTmpParseFileName()); // это для теста на локали, потом удалить и раскоментить вгет
            $file_name = $this->getTmpParseFileNameAll();
            $result = array();
            if( !is_file($file_name) ){
                die(json_encode(array('status'=>'error','msg'=>'Ошибка загрузки по ftp')));
            }
            copy($file_name,__DIR__.'/'.$this->csvfilename_all);
            die(json_encode( $this->productUpdateQ('csvfilename_all')));
        }

        if(Tools::isSubmit('ajaxUpdateProducts'))
        {

            $local_file_path = __DIR__.'/temp/';
            @unlink($local_file_path.$this->csvfilename_all);
            $cmd = 'wget --user=gianni_sites --password=5IOOwXjavuqysxb -P '.$local_file_path.' '.$this->ftp_file_path_all;
            $r = exec($cmd, $a, $b);
//            copy(__DIR__.'/'.$this->csvfilename,$this->getTmpParseFileName()); // это для теста на локали, потом удалить и раскоментить вгет
            $file_name = $this->getTmpParseFileNameAll();
            $result = array();
            if( !is_file($file_name) ){
                die(json_encode(array('status'=>'error','msg'=>'Ошибка загрузки по ftp')));
            }
            copy($file_name,__DIR__.'/'.$this->csvfilename_all);
            die(json_encode( $this->productImportNew('csvfilename_all', '_all', true)));
        }

        if(Tools::isSubmit('test'))
        {

            $this->productImportNew('csvfilename');
        }

        if(Tools::getIsset('parse_akciya'))
        {

            $this->parseAkciya();
        }

        if(Tools::isSubmit('triggerControll'))
        {
            Configuration::updateValue('active_trigger', Tools::getValue('active_trigger'));
            if ( Tools::getValue('active_trigger') == 1 ) {
                Db::getInstance()->execute('
                CREATE TRIGGER change_active_after_insert AFTER INSERT ON ps_image
                FOR EACH ROW
                BEGIN
                UPDATE ps_product_shop SET active=1 WHERE id_product IN (SELECT id_product FROM ps_image GROUP BY id_product);
                END
                ');
                Db::getInstance()->execute('
                CREATE TRIGGER change_active_after_delete AFTER DELETE ON ps_image
                FOR EACH ROW
                BEGIN
                UPDATE ps_product_shop SET active=0 WHERE id_product NOT IN (SELECT id_product FROM ps_image GROUP BY id_product);
                END
                ');
            } else {
                Db::getInstance()->execute('DROP TRIGGER change_active_after_insert');
                Db::getInstance()->execute('DROP TRIGGER change_active_after_delete');
            }
        }



        $output = '';
        $output .= '<script>';
        $output .= 'var log_block_msg = "'.$this->getLastImportInfo().'";';
        $output .= 'var log_block_all_msg = "'.$this->getLastImportInfo('all').'";';
        $output .= '</script>';

        $this->context->controller->addCSS($this->_path.'importcsv2.css');
        $this->context->controller->addJS($this->_path.'importcsv2.js');
        return $output.$this->displayForm().$this->displayConfigForm();
    }

    public function displayForm()
    {
        // Get default Language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Импорт товаров'),
                'enctype' => "multipart/form-data"
            ),
            'submit' => array(
                'title' => 'Импорт с FTP',
                'class' => 'button js_parse_products',
                'name' => 'ajaxImport'
            )
        );

        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Полный импорт товаров'),
                'enctype' => "multipart/form-data"
            ),
            'submit' => array(
                'title' => 'Импорт с FTP',
                'class' => 'button js_parse_all_products',
                'name' => 'ajaxImportAll'
            )
        );

        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Синхронизировать количество товара в интернет магазине'),
                'enctype' => "multipart/form-data"
            ),
            'submit' => array(
                'title' => 'Синхронизировать',
                'class' => 'button js_quantity_adjust',
                'name' => 'ajaxUpdateProductsQ'
            )
        );

        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Синхронизировать базу'),
                'enctype' => "multipart/form-data"
            ),
            'submit' => array(
                'title' => 'Синхронизировать',
                'class' => 'button js_product_adjust',
                'name' => 'ajaxUpdateProducts'
            )
        );

        $fields_form[]['form'] = array(
            'legend' => array(
                'title' => $this->l('Импорт картинок'),
                'enctype' => "multipart/form-data"
            ),
            'submit' => array(
                'title' => 'Импорт',
                'class' => 'button js_parse_images',
                'name' => 'ajaxImportImage'
            )
        );

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
                array(
                    'desc' => $this->l('Save'),
                    'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                        '&token='.Tools::getAdminTokenLite('AdminModules'),
                ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );

        // Load current value
        $helper->fields_value['file'] = Configuration::get('file');

        return $helper->generateForm($fields_form);
    }

    public function displayConfigForm()
    {


        $this->fields_options = array(
            'general' => array(
                'title' => 'Тригер для активации/деактивации товаров при отсутсвии картинок.',
                'fields' => array(

                    'active_trigger' => array(
                        'title' => 'Включить',
                        'cast' => 'strval',
                        'type' => 'bool',
                        'size' => '10'
                    )

                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'button',
                    'name' => 'triggerControll'
                )
            )
        );

        $helper = new HelperOptionsCore();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            )
        );
        return $helper->generateOptions($this->fields_options);
    }

    /**
     * copyImg copy an image located in $url and save it in a path
     * according to $entity->$id_entity .
     * $id_image is used if we need to add a watermark
     *
     * @param int $id_entity id of product or category (set in entity)
     * @param int $id_image (default null) id of the image if watermark enabled.
     * @param string $url path or url to use
     * @param string entity 'products' or 'categories'
     * @return boolean
     */
    public static function copyImg($id_entity, $id_image = null, $url, $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity)
        {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
                break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_.(int)$id_entity;
                break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_.(int)$id_entity;
                break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_.(int)$id_entity;
                break;
        }
        $url = str_replace(' ', '%20', trim($url));

        // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
        if (!ImageManager::checkImageMemoryLimit($url))
            return false;

        // 'file_exists' doesn't work on distant file, and getimagesize makes the import slower.
        // Just hide the warning, the processing will be the same.
        if (Tools::copy($url, $tmpfile))
        {
            ImageManager::resize($tmpfile, $path.'.jpg');
            $images_types = ImageType::getImagesTypes($entity);

            if ($regenerate)
                foreach ($images_types as $image_type)
                {
                    ImageManager::resize($tmpfile, $path.'-'.stripslashes($image_type['name']).'.jpg', $image_type['width'], $image_type['height']);
                    if (in_array($image_type['id_image_type'], $watermark_types))
                        Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                }
        }
        else
        {
            unlink($tmpfile);
            return false;
        }
        unlink($tmpfile);
        return true;
    }

    public function parseAkciya()
    {
        require_once(__DIR__.'/libs/simplexlsx/simplexlsx.class.php');

        $file_name = 'akciya.xlsx';
        $upload_dir = __DIR__ . '/temp/';

        $filename = $upload_dir.$file_name;

        if(!file_exists($filename))
            return false;

        $id_lang = Language::getIdByIso('uk');

        $category_man = Category::searchByNameAndParentCategoryId($id_lang, 'Чоловiкам', Configuration::get('PS_HOME_CATEGORY'));
        $category_woman = Category::searchByNameAndParentCategoryId($id_lang, 'Жiнкам', Configuration::get('PS_HOME_CATEGORY'));

        if (!$category_man || !$category_woman) {
            return false;
        }

        $category_newcollection_man = Category::searchByNameAndParentCategoryId($id_lang, 'Нова колекція', $category_man['id_category']);
        $category_newcollection_woman = Category::searchByNameAndParentCategoryId($id_lang, 'Нова колекція', $category_woman['id_category']);

        if (!$category_newcollection_man || !$category_newcollection_woman) {
            return false;
        }

        $Reader = new SimpleXLSX($filename);

        $current_line = 0;
        foreach ($Reader->rows() as $line) {
            $current_line++;
            if ($current_line < 2) continue;

            $name = trim($line[3]);
            if (mb_substr_count($name, 'жiночi')) {
                $id_category_to_add = $category_newcollection_woman['id_category'];
            } else {
                $id_category_to_add = $category_newcollection_man['id_category'];
            }

            $name = explode(' ', $name);
            $reference = array_reverse($name)[0];

            $id_product = Product::getIdByReference($reference.'_'.$line[5]);

            if ($id_product) {
                $product = new Product($id_product);
                $product->addToCategories($id_category_to_add);
            }
        }

    }

    public function productUpdateQ($from_file)
    {
        //МЕТОД ОБНОВЛЯЕТ КОЛИЧЕСТВО ТОВАРОВ В БАЗЕ ИЗ CSV

        //line[00] - category path UA
        //line[01] - category path RU
        //line[02] - ean13
        //line[03] - id supplier
        //line[04] - name supplier
        //line[05] - root category UA
        //line[06] - root category RU
        //line[07] - сезон UA
        //line[08] - сезон RU
        //line[09] - sex
        //line[10] - style UA
        //line[11] - style RU
        //line[12] - brand UA
        //line[13] - brand RU
        //line[14] - страна UA
        //line[15] - страна RU
        //line[16] - name UA
        //line[17] - name RU
        //line[18] - reference
        //line[19] - группа
        //line[20] - материал подкладки UA
        //line[21] - материал подкладки RU
        //line[22] - цвет UA
        //line[23] - цвет RU
        //line[24] - материал UA
        //line[25] - материал RU
        //line[26] - высота каблуку
        //line[27] - каблук UA
        //line[28] - каблук RU
        //line[29] - материал подошвы UA
        //line[30] - материал подошвы RU
        //line[31] - размер
        //line[32] - количество
        //line[33] - цена

        $array = array();
        $ean_array = array();
        $sql_array = array();
        $msg_array = array();

        $filename = __DIR__.'/'.$this->$from_file;
        $handle = fopen($filename,'r');

        $i=0;
        while ( !feof($handle) ) {
            $i++;
            $line = fgets($handle);
            $array_tmp = array_map(array($this,'trim'),explode(',', str_replace('#', '', mb_convert_encoding($line,'utf8','cp1251'))));
            if ( count($array_tmp) != 34  )
                continue;
            if ( $array_tmp[2] == '' )
                continue;
            $array[] = $array_tmp;
            $ean_array[] = $array_tmp[2];
            $sql_array[] = '('.$array_tmp[2].','.$array_tmp[32].')';
//            if ($i == 5)
//                d($array);
        }
//        d(Db::getInstance()->getValue('SELECT count(*) FROM ps_product_attribute WHERE ean13 NOT IN ('.implode(',', $ean_array).')'));
        $line_me = $array;
        $cnt_array = count($line_me);

        if ( $cnt_array > 0 )
        {
            Db::getInstance()->Execute("
					UPDATE `" . _DB_PREFIX_ . "product`
					SET `active`=0");
            Db::getInstance()->Execute("
					UPDATE `" . _DB_PREFIX_ . "product`
					SET `active`=1 WHERE ean13 IN (".implode(',', $ean_array).")");

            Db::getInstance()->Execute("
					UPDATE `" . _DB_PREFIX_ . "product_shop`
					SET `active`=0");
            Db::getInstance()->Execute("
					UPDATE `" . _DB_PREFIX_ . "product_shop` ps
					INNER JOIN `" . _DB_PREFIX_ . "product` p ON p.id_product=ps.`id_product`
					SET ps.`active`=1 WHERE p.`active`=1");

            Db::getInstance()->Execute("
					UPDATE `" . _DB_PREFIX_ . "product_attribute`
					SET `quantity`=0");



            Db::getInstance()->Execute("
              CREATE TABLE IF NOT EXISTS `" . _DB_PREFIX_ . "product_tmp` (
              `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
              `ean13` varchar(13) DEFAULT NULL,
              `quantity` int(10) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              KEY `ean13` (`ean13`)
              ) ENGINE=HEAP AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");

            Db::getInstance()->Execute("
              TRUNCATE TABLE `" . _DB_PREFIX_ . "product_tmp`");

            Db::getInstance()->Execute("
                INSERT INTO `" . _DB_PREFIX_ . "product_tmp`
                (`ean13`, `quantity`)
                VALUES
                ".implode(',', $sql_array));

            Db::getInstance()->Execute("
                UPDATE `" . _DB_PREFIX_ . "product_tmp` pt
                INNER JOIN `" . _DB_PREFIX_ . "product_attribute` pa ON pa.ean13=pt.`ean13`
                SET pa.`quantity`=pt.`quantity`");



            Db::getInstance()->Execute("
					UPDATE `" . _DB_PREFIX_ . "stock_available`
					SET `quantity`=0");
            Db::getInstance()->Execute("
                UPDATE `" . _DB_PREFIX_ . "stock_available` psa
                INNER JOIN `" . _DB_PREFIX_ . "product_attribute` pa ON pa.`id_product_attribute`=psa.`id_product_attribute` AND psa.`id_product_attribute`!=0
                SET psa.`quantity`=pa.`quantity`");
            Db::getInstance()->Execute("
	            UPDATE `" . _DB_PREFIX_ . "stock_available` psa
                INNER JOIN
                 (
                        SELECT id_product, SUM(quantity) as quantity
                        FROM `" . _DB_PREFIX_ . "stock_available`
                        WHERE id_product_attribute!=0
                        GROUP BY(id_product)
                 ) psa2
                  ON
                    psa.`id_product`=psa2.`id_product`
                    AND psa.`id_product_attribute`=0
                  SET psa.`quantity`=psa2.`quantity`");

            $this->deactivateProductsWithoutImg();
//            $this->reactivateProductsWithImg();

            return array('status' => 'ok', 'msg' => 'Синхронизация успешно завершена.');
        } else
            return array('status' => 'error', 'msg' => 'Произошла ошибка. Файл '.$filename.' отсутсвует либо пустой.');
    }

    public function productImportNew($from_file, $all = null, $only_exists = false)
    {

        $cnt = 0;
        $force_ids = false;
        $match_ean13 = 1;

        $time_start = time();
        $microtime = microtime(true);


        if (!defined('PS_MASS_PRODUCT_CREATION'))
            define('PS_MASS_PRODUCT_CREATION', true);

        $default_language = (int)Configuration::get('PS_LANG_DEFAULT');
        $id_lang_ru = Language::getIdByIso('ru');
        $id_lang_uk = Language::getIdByIso('uk');


        $groups = array();
        foreach (AttributeGroup::getAttributesGroups($default_language) as $group)
            $groups[$group['name']] = (int)$group['id_attribute_group'];

        $attributes = array();
        foreach (Attribute::getAttributes($default_language) as $attribute)
            $attributes[$attribute['attribute_group'].'_'.$attribute['name']] = (int)$attribute['id_attribute'];


        $this->receiveTab();
        $filename = __DIR__.'/'.$this->$from_file;
        $handle = fopen($filename,'r');

        self::setLocale();
        $shop_ids = Shop::getCompleteListOfShopsID();


        $shop_is_feature_active = Shop::isFeatureActive();
        Module::setBatchMode(true);

        //line[00] - category path UA
        //line[01] - category path RU
        //line[02] - ean13
        //line[03] - id supplier
        //line[04] - name supplier
        //line[05] - root category UA
        //line[06] - root category RU
        //line[07] - сезон UA
        //line[08] - сезон RU
        //line[09] - sex
        //line[10] - style UA
        //line[11] - style RU
        //line[12] - brand UA
        //line[13] - brand RU
        //line[14] - страна UA
        //line[15] - страна RU
        //line[16] - name UA
        //line[17] - name RU
        //line[18] - reference
        //line[19] - группа
        //line[20] - материал подкладки UA
        //line[21] - материал подкладки RU
        //line[22] - цвет UA
        //line[23] - цвет RU
        //line[24] - материал UA
        //line[25] - материал RU
        //line[26] - высота каблуку
        //line[27] - каблук UA
        //line[28] - каблук RU
        //line[29] - материал подошвы UA
        //line[30] - материал подошвы RU
        //line[31] - размер
        //line[32] - количество
        //line[33] - цена



        $cnt_array = 0;
        while(!feof($handle)){
            $line = fgets($handle);
            $cnt_array++;
        }
        rewind($handle);

        $parse_parse_time = microtime(true);
        $parse_parse_time = $parse_parse_time - $microtime;

        for ($current_line = 0; $line = fgetcsv($handle, MAX_LINE_SIZE, $this->separator); $current_line++) {

//            if ($line['1'] != 102825)
//                continue;

            $line = array_map('mb_convert_encoding' , $line, array_fill(0, count($line), 'utf-8'), array_fill(0, count($line), 'cp1251'));
            $line = array_map('trim',$line);
            $line = array_map('str_replace', array_fill(0, count($line), '#'),array_fill(0, count($line), ''), $line);


            $product_name = [
                $id_lang_ru =>  $line[17],
                $id_lang_uk =>  $line[16],
            ];

            $product_category_path = [
                $id_lang_ru =>  $line[1],
                $id_lang_uk =>  $line[0],
            ];

            $features_lang = [
                $id_lang_ru =>  [
                    'Сезон:'.$line[8].':1',
                    'Пол:'.$line[9].':2',
                    'Стиль:'.$line[11].':3',
                    'Страна:'.$line[15].':4',
                    'Материал подкладки:'.$line[21].':5',
                    'Каблук:'.$line[28].':6',
                    'Высота каблука:'.$line[26].':7',
                    'Материал подошвы:'.$line[30].':8',
                    'Материал:'.$line[25].':9',
                    'Цвет:'.$line[23].':10',
                ],
                $id_lang_uk =>  [
                    'Сезон:'.$line[7].':1',
                    'Стать:'.$line[9].':2',
                    'Стиль:'.$line[10].':3',
                    'Крайна:'.$line[14].':4',
                    'Матеріал підкладки:'.$line[20].':5',
                    'Каблук:'.$line[27].':6',
                    'Висота каблука:'.$line[26].':7',
                    'Матеріал подошви:'.$line[29].':8',
                    'Матеріал:'.$line[24].':9',
                    'Колір:'.$line[22].':10',
                ]
            ];

            $_POST['iso_lang'] = 'uk';
            $id_lang = $id_lang_uk;

            $info = array(
                'category_path' => $line[0],
                'ean13' => $line[2],
                'id_supplier' => $line[3],
                'supplier' => $line[4],
                'category' => $line[0],
                'manufacturer' => $line[12],
                'reference' => $line[18],
                'group' => $line[19],
                'quantity' => $line[32],
                'price' => $line[33],
                'name' => $line[16],
                'active' => 0,
            );


//            Для парсинга комбинаций атрибутов

            $atr['group'] = [
                'Size:radio:0',
//                'Color:color:1',
//                'Material:select:2',
            ];

            $size = ((int)$line[31] == 0 ) ? '0' : round( (float)$line[31], 1 );

            $atr['attribute'] = [
                $size.':0',
//                $line[14].':1',
//                $line[15].':2',
            ];

            if (array_key_exists('reference', $info))
            {
                $datas = Db::getInstance()->getRow('
						SELECT p.`id_product`
						FROM `'._DB_PREFIX_.'product` p
						'.Shop::addSqlAssociation('product', 'p').'
						WHERE p.`reference` = "'.pSQL($info['reference']).'"
					', false);
                if (isset($datas['id_product']) && $datas['id_product']) {

                    $product = new Product((int)$datas['id_product']);

                    $shops = Shop::getContextListShopID();
                    if ($shop_is_feature_active) {
                        foreach ($shops as $shop)
                            StockAvailable::setQuantity((int)$product->id, 0, (int)$info['quantity'], (int)$shop);
                    } else {
                        StockAvailable::setQuantity((int)$product->id, 0, (int)$info['quantity'], (int)$this->context->shop->id);
                    }

                    if (!$shop_is_feature_active)
                        $info['shop'] = 1;
                    elseif (!isset($info['shop']) || empty($info['shop']))
                        $info['shop'] = implode($this->multiple_value_separator, Shop::getContextListShopID());

                    // Get shops for each attributes
                    $info['shop'] = explode($this->multiple_value_separator, $info['shop']);

                    $id_shop_list = array();
                    foreach ($info['shop'] as $shop)
                        if (!empty($shop) && !is_numeric($shop))
                            $id_shop_list[] = (int)Shop::getIdByName($shop);
                        elseif (!empty($shop))
                            $id_shop_list[] = $shop;

                    $id_attribute_group = 0;
                    // groups
                    $groups_attributes = array();
                    if (isset($atr['group']))
                        foreach ($atr['group'] as $key => $group)
                        {
                            if (empty($group))
                                continue;
                            $tab_group = explode(':', $group);
                            $group = trim($tab_group[0]);
                            if (!isset($tab_group[1]))
                                $type = 'select';
                            else
                                $type = trim($tab_group[1]);

                            // sets group
                            $groups_attributes[$key]['group'] = $group;

                            // if position is filled
                            if (isset($tab_group[2]))
                                $position = trim($tab_group[2]);
                            else
                                $position = false;

                            if (!isset($groups[$group]))
                            {
                                $obj = new AttributeGroup();
                                $obj->is_color_group = false;
                                $obj->group_type = pSQL($type);
                                $obj->name[$default_language] = $group;
                                $obj->public_name[$default_language] = $group;
                                $obj->position = (!$position) ? AttributeGroup::getHigherPosition() + 1 : $position;

                                if (($field_error = $obj->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                                    ($lang_field_error = $obj->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true)
                                {
                                    $obj->add();
                                    $obj->associateTo($id_shop_list);
                                    $groups[$group] = $obj->id;
                                }
                                else
                                    $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');

                                // fills groups attributes
                                $id_attribute_group = $obj->id;
                                $groups_attributes[$key]['id'] = $id_attribute_group;
                            }
                            else // already exists
                            {
                                $id_attribute_group = $groups[$group];
                                $groups_attributes[$key]['id'] = $id_attribute_group;
                            }
                        }

                    // inits attribute
                    $id_product_attribute = 0;
                    $id_product_attribute_update = false;
                    $attributes_to_add = array();

                    $attribute_default = array(
                        'upc' => '',
                        'wholesale_price' => 0,
                        'ecotax' => 0,
                        'weight' => 0,
                        'default_on' => 0,
                        'available_date' => date('Y-m-d')
                    );

                    $info = array_merge($info,$attribute_default);

//            d($info);
                    // for each attribute
                    if (isset($atr['attribute']))
                        foreach ( $atr['attribute'] as $key => $attribute)
                        {
                            if (empty($attribute))
                                continue;
                            $tab_attribute = explode(':', $attribute);
                            $attribute = trim($tab_attribute[0]);
                            // if position is filled
                            if (isset($tab_attribute[1]))
                                $position = trim($tab_attribute[1]);
                            else
                                $position = false;

                            if (isset($groups_attributes[$key]))
                            {
                                $group = $groups_attributes[$key]['group'];
                                if (!isset($attributes[$group.'_'.$attribute]) && count($groups_attributes[$key]) == 2)
                                {
                                    $id_attribute_group = $groups_attributes[$key]['id'];
                                    $obj = new Attribute();
                                    // sets the proper id (corresponding to the right key)
                                    $obj->id_attribute_group = $groups_attributes[$key]['id'];
                                    $obj->name[$default_language] = str_replace('\n', '', str_replace('\r', '', $attribute));
                                    $obj->position = (!$position && isset($groups[$group])) ? Attribute::getHigherPosition($groups[$group]) + 1 : $position;

                                    if (($field_error = $obj->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                                        ($lang_field_error = $obj->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true)
                                    {
                                        $obj->add();
                                        $obj->associateTo($id_shop_list);
                                        $attributes[$group.'_'.$attribute] = $obj->id;
                                    }
                                    else
                                        $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');
                                }

                                $info['minimal_quantity'] = isset($info['minimal_quantity']) && $info['minimal_quantity'] ? (int)$info['minimal_quantity'] : 1;

                                $info['wholesale_price'] = str_replace(',', '.', $info['wholesale_price']);
                                $info['price'] = str_replace(',', '.', $info['price']);
                                $info['ecotax'] = str_replace(',', '.', $info['ecotax']);
                                $info['weight'] = str_replace(',', '.', $info['weight']);
                                $info['available_date'] = Validate::isDate($info['available_date']) ? $info['available_date'] : null;

                                if (!Validate::isEan13($info['ean13']))
                                {
                                    $this->warnings[] = sprintf(Tools::displayError('EAN13 "%1s" has incorrect value for product with id %2d.'), $info['ean13'], $product->id);
                                    $info['ean13'] = '';
                                }

                                if ($info['default_on'])
                                    $product->deleteDefaultAttributes();

                                // if a reference is specified for this product, get the associate id_product_attribute to UPDATE
                                if (isset($info['reference']) && !empty($info['reference']))
                                {
                                    $id_product_attribute = $this->getCombinationByEan13($product->id, intval($info['ean13']));

                                    // updates the attribute
                                    if ($id_product_attribute)
                                    {
                                        // gets all the combinations of this product
                                        $attribute_combinations = $product->getAttributeCombinations($default_language);
                                        foreach ($attribute_combinations as $attribute_combination)
                                        {
                                            if ($id_product_attribute && in_array($id_product_attribute, $attribute_combination))
                                            {
                                                $product->updateAttribute(
                                                    $id_product_attribute,
                                                    (float)$info['wholesale_price'],
                                                    0,
                                                    (float)$info['weight'],
                                                    0,
                                                    (Configuration::get('PS_USE_ECOTAX') ? (float)$info['ecotax'] : 0),
                                                    array(),
                                                    strval($info['reference']),
                                                    strval($info['ean13']),
                                                    (int)$info['default_on'],
                                                    0,
                                                    strval($info['upc']),
                                                    (int)$info['minimal_quantity'],
                                                    $info['available_date'],
                                                    0,
                                                    $id_shop_list
                                                );
                                                $id_product_attribute_update = true;
                                                if (isset($info['supplier_reference']) && !empty($info['supplier_reference']))
                                                    $product->addSupplierReference($product->id_supplier, $id_product_attribute, $info['supplier_reference']);
                                            }
                                        }
                                    }
                                }

                                // if no attribute reference is specified, creates a new one
                                if (!$id_product_attribute)
                                {
                                    $id_product_attribute = $product->addCombinationEntity(
                                        (float)$info['wholesale_price'],
                                        0,
                                        (float)$info['weight'],
                                        0,
                                        (Configuration::get('PS_USE_ECOTAX') ? (float)$info['ecotax'] : 0),
                                        (int)$info['quantity'],
                                        array(),
                                        strval($info['reference']),
                                        0,
                                        strval($info['ean13']),
                                        (int)$info['default_on'],
                                        0,
                                        strval($info['upc']),
                                        (int)$info['minimal_quantity'],
                                        $id_shop_list,
                                        $info['available_date']
                                    );

                                    if (isset($info['supplier_reference']) && !empty($info['supplier_reference']))
                                        $product->addSupplierReference($product->id_supplier, $id_product_attribute, $info['supplier_reference']);
                                }

                                // fills our attributes array, in order to add the attributes to the product_attribute afterwards
                                if (isset($attributes[$group.'_'.$attribute]))
                                    $attributes_to_add[] = (int)$attributes[$group.'_'.$attribute];

                                // after insertion, we clean attribute position and group attribute position
                                $obj = new Attribute();
                                $obj->cleanPositions((int)$id_attribute_group, false);
                                AttributeGroup::cleanPositions();
                            }
                        }

                    $product->checkDefaultAttributes();
                    if (!$product->cache_default_attribute)
                        Product::updateDefaultAttribute($product->id);

                    if ($id_product_attribute)
                    {
                        // now adds the attributes in the attribute_combination table
                        if ($id_product_attribute_update)
                        {
                            Db::getInstance()->execute('
						DELETE FROM '._DB_PREFIX_.'product_attribute_combination
						WHERE id_product_attribute = '.(int)$id_product_attribute);
                        }

                        foreach ($attributes_to_add as $attribute_to_add)
                        {
                            Db::getInstance()->execute('
						INSERT IGNORE INTO '._DB_PREFIX_.'product_attribute_combination (id_attribute, id_product_attribute)
						VALUES ('.(int)$attribute_to_add.','.(int)$id_product_attribute.')', false);
                        }

                        // set advanced stock managment
                        if (isset($info['advanced_stock_management']))
                        {
                            if ($info['advanced_stock_management'] != 1 && $info['advanced_stock_management'] != 0)
                                $this->warnings[] = sprintf(Tools::displayError('Advanced stock management has incorrect value. Not set for product with id %d.'), $product->id);
                            elseif (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $info['advanced_stock_management'] == 1)
                                $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, cannot enable on product with id %d.'), $product->id);
                            else
                                $product->setAdvancedStockManagement($info['advanced_stock_management']);
                            // automaticly disable depends on stock, if a_s_m set to disabled
                            if (StockAvailable::dependsOnStock($product->id) == 1 && $info['advanced_stock_management'] == 0)
                                StockAvailable::setProductDependsOnStock($product->id, 0, null, $id_product_attribute);
                        }

                        // Check if warehouse exists
                        if (isset($info['warehouse']) && $info['warehouse'])
                        {
                            if (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
                                $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, warehouse is not set on product with id %d.'), $product->id);
                            else
                            {
                                if (Warehouse::exists($info['warehouse']))
                                {
                                    $warehouse_location_entity = new WarehouseProductLocation();
                                    $warehouse_location_entity->id_product = $product->id;
                                    $warehouse_location_entity->id_product_attribute = $id_product_attribute;
                                    $warehouse_location_entity->id_warehouse = $info['warehouse'];
                                    if (WarehouseProductLocation::getProductLocation($product->id, $id_product_attribute, $info['warehouse']) !== false)
                                        $warehouse_location_entity->update();
                                    else
                                        $warehouse_location_entity->save();
                                    StockAvailable::synchronize($product->id);
                                }
                                else
                                    $this->warnings[] = sprintf(Tools::displayError('Warehouse did not exist, cannot set on product %1$s.'), $product->name[$default_language]);
                            }
                        }

                        // stock available
                        if (isset($info['depends_on_stock']))
                        {
                            if ($info['depends_on_stock'] != 0 && $info['depends_on_stock'] != 1)
                                $this->warnings[] = sprintf(Tools::displayError('Incorrect value for depends on stock for product %1$s '), $product->name[$default_language]);
                            elseif ((!$info['advanced_stock_management'] || $info['advanced_stock_management'] == 0) && $info['depends_on_stock'] == 1)
                                $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, cannot set depends on stock %1$s '), $product->name[$default_language]);
                            else
                                StockAvailable::setProductDependsOnStock($product->id, $info['depends_on_stock'], null, $id_product_attribute);

                            // This code allows us to set qty and disable depends on stock
                            if (isset($info['quantity']) && (int)$info['quantity'])
                            {
                                // if depends on stock and quantity, add quantity to stock
                                if ($info['depends_on_stock'] == 1)
                                {
                                    $stock_manager = StockManagerFactory::getManager();
                                    $price = str_replace(',', '.', $info['wholesale_price']);
                                    if ($price == 0)
                                        $price = 0.000001;
                                    $price = round(floatval($price), 6);
                                    $warehouse = new Warehouse($info['warehouse']);
                                    if ($stock_manager->addProduct((int)$product->id, $id_product_attribute, $warehouse, (int)$info['quantity'], 1, $price, true))
                                        StockAvailable::synchronize((int)$product->id);
                                }
                                else
                                {
                                    if ($shop_is_feature_active)
                                        foreach ($id_shop_list as $shop)
                                            StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], (int)$shop);
                                    else
                                        StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], $this->context->shop->id);
                                }

                            }
                        }
                        // if not depends_on_stock set, use normal qty
                        else
                        {
                            if ($shop_is_feature_active)
                                foreach ($id_shop_list as $shop)
                                    StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], (int)$shop);
                            else
                                StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], $this->context->shop->id);
                        }

                    }

                    $product->price = $info['price'];
                    $product->update();
                    continue;

                } else {
                    if($only_exists) {
                        continue;
                    }
                    $product = new Product();
                }
            }
            else {
                if($only_exists) {
                    continue;
                }
                $product = new Product();
            }


            $update_advanced_stock_management_value = false;
            if (isset($product->id) && $product->id && Product::existsInDatabase((int)$product->id, 'product'))
            {
                $product->loadStockData();
                $update_advanced_stock_management_value = true;
                $category_data = Product::getProductCategories((int)$product->id);

                if (is_array($category_data))
                    foreach ($category_data as $tmp)
                        if (!isset($product->category) || !$product->category || is_array($product->category))
                            $product->category[] = $tmp;
            }

            self::setEntityDefaultValues($product);
            self::arrayWalk($info, array('self', 'fillInfo'), $product);
            $product->name = $product_name;


            if (!$shop_is_feature_active)
                $product->shop = (int)Configuration::get('PS_SHOP_DEFAULT');
            elseif (!isset($product->shop) || empty($product->shop))
                $product->shop = implode($this->multiple_value_separator, Shop::getContextListShopID());

            if (!$shop_is_feature_active)
                $product->id_shop_default = (int)Configuration::get('PS_SHOP_DEFAULT');
            else
                $product->id_shop_default = (int)Context::getContext()->shop->id;

            // link product to shops
            $product->id_shop_list = array();
            foreach (explode($this->multiple_value_separator, $product->shop) as $shop)
                if (!empty($shop) && !is_numeric($shop))
                    $product->id_shop_list[] = Shop::getIdByName($shop);
                elseif (!empty($shop))
                    $product->id_shop_list[] = $shop;

            if ((int)$product->id_tax_rules_group != 0)
            {
                if (Validate::isLoadedObject(new TaxRulesGroup($product->id_tax_rules_group)))
                {
                    $address = $this->context->shop->getAddress();
                    $tax_manager = TaxManagerFactory::getManager($address, $product->id_tax_rules_group);
                    $product_tax_calculator = $tax_manager->getTaxCalculator();
                    $product->tax_rate = $product_tax_calculator->getTotalRate();
                }
                else
                    $this->addProductWarning(
                        'id_tax_rules_group',
                        $product->id_tax_rules_group,
                        Tools::displayError('Invalid tax rule group ID. You first need to create a group with this ID.')
                    );
            }

            if (isset($product->manufacturer) && is_numeric($product->manufacturer) && Manufacturer::manufacturerExists((int)$product->manufacturer))
                $product->id_manufacturer = (int)$product->manufacturer;
            elseif (isset($product->manufacturer) && is_string($product->manufacturer) && !empty($product->manufacturer))
            {
                if ($manufacturer = Manufacturer::getIdByName($product->manufacturer))
                    $product->id_manufacturer = (int)$manufacturer;
                else
                {
                    $manufacturer = new Manufacturer();
                    $manufacturer->name = $product->manufacturer;
                    $manufacturer->active = true;

                    if (($field_error = $manufacturer->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                        ($lang_field_error = $manufacturer->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $manufacturer->add())
                    {
                        $product->id_manufacturer = (int)$manufacturer->id;
                        $manufacturer->associateTo($product->id_shop_list);
                    }
                    else
                    {
                        $this->errors[] = sprintf(
                            Tools::displayError('%1$s (ID: %2$s) cannot be saved'),
                            $manufacturer->name,
                            (isset($manufacturer->id) && !empty($manufacturer->id))? $manufacturer->id : 'null'
                        );
                        $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '').
                            Db::getInstance()->getMsgError();
                    }
                }
            }

            if (isset($product->supplier) && is_numeric($product->supplier) && Supplier::supplierExists((int)$product->supplier))
                $product->id_supplier = (int)$product->supplier;
            elseif (isset($product->supplier) && is_string($product->supplier) && !empty($product->supplier))
            {
                if ($supplier = Supplier::getIdByName($product->supplier))
                    $product->id_supplier = (int)$supplier;
                else
                {
                    $supplier = new Supplier();
                    $supplier->name = $product->supplier;
                    $supplier->active = true;

                    if (($field_error = $supplier->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                        ($lang_field_error = $supplier->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $supplier->add())
                    {
                        $product->id_supplier = (int)$supplier->id;
                        $supplier->associateTo($product->id_shop_list);
                    }
                    else
                    {
                        $this->errors[] = sprintf(
                            Tools::displayError('%1$s (ID: %2$s) cannot be saved'),
                            $supplier->name,
                            (isset($supplier->id) && !empty($supplier->id))? $supplier->id : 'null'
                        );
                        $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '').
                            Db::getInstance()->getMsgError();
                    }
                }
            }

            if (isset($product->price_tex) && !isset($product->price_tin))
                $product->price = $product->price_tex;
            elseif (isset($product->price_tin) && !isset($product->price_tex))
            {
                $product->price = $product->price_tin;
                // If a tax is already included in price, withdraw it from price
                if ($product->tax_rate)
                    $product->price = (float)number_format($product->price / (1 + $product->tax_rate / 100), 6, '.', '');
            }
            elseif (isset($product->price_tin) && isset($product->price_tex))
                $product->price = $product->price_tex;

            if (!Configuration::get('PS_USE_ECOTAX'))
                $product->ecotax = 0;

            if (isset($product->category) && is_array($product->category) && count($product->category))
            {
                $product->id_category = array(); // Reset default values array
                foreach ($product->category as $value)
                {
                    if (is_numeric($value))
                    {
                        if (Category::categoryExists((int)$value))
                            $product->id_category[] = (int)$value;
                        else
                        {
                            $category_to_create = new Category();
                            $category_to_create->id = (int)$value;
                            $category_to_create->name = sef::createMultiLangField($value);
                            $category_to_create->active = 0;
                            $category_to_create->id_parent = Configuration::get('PS_HOME_CATEGORY'); // Default parent is home for unknown category to create
                            $category_link_rewrite = Tools::link_rewrite($category_to_create->name[$default_language]);
                            $category_to_create->link_rewrite = self::createMultiLangField($category_link_rewrite);
                            if (($field_error = $category_to_create->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                                ($lang_field_error = $category_to_create->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $category_to_create->add())
                                $product->id_category[] = (int)$category_to_create->id;
                            else
                            {
                                $this->errors[] = sprintf(
                                    Tools::displayError('%1$s (ID: %2$s) cannot be saved'),
                                    $category_to_create->name[$default_language],
                                    (isset($category_to_create->id) && !empty($category_to_create->id))? $category_to_create->id : 'null'
                                );
                                $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '').
                                    Db::getInstance()->getMsgError();
                            }
                        }
                    }
                    elseif (is_string($value) && !empty($value))
                    {
                        $category = self::searchByPath($default_language, trim($value), $this, 'productImportCreateCat', $product_category_path);
                        if ($category['id_category'])
                            $product->id_category[] = (int)$category['id_category'];
                        else
                            $this->errors[] = sprintf(Tools::displayError('%1$s cannot be saved'), trim($value));
                    }
                }
                $product->id_category = array_values(array_unique($product->id_category));
            }

            if (!isset($product->id_category_default) || !$product->id_category_default)
                $product->id_category_default = isset($product->id_category[0]) ? (int)$product->id_category[0] : (int)Configuration::get('PS_HOME_CATEGORY');
            $link_rewrite = (is_array($product->link_rewrite) && isset($product->link_rewrite[$id_lang])) ? trim($product->link_rewrite[$id_lang]) : '';
            $valid_link = Validate::isLinkRewrite($link_rewrite);



            if ((isset($product->link_rewrite[$id_lang]) && empty($product->link_rewrite[$id_lang])) || !$valid_link)
            {
                $link_rewrite = Tools::link_rewrite($product->name[$id_lang]);
                if ($link_rewrite == '')
                    $link_rewrite = 'friendly-url-autogeneration-failed';
            }

            if (!$valid_link)
                $this->warnings[] = sprintf(
                    Tools::displayError('Rewrite link for %1$s (ID: %2$s) was re-written as %3$s.'),
                    $product->name[$id_lang],
                    (isset($info['id']) && !empty($info['id']))? $info['id'] : 'null',
                    $link_rewrite
                );

            if (!(is_array($product->link_rewrite) && count($product->link_rewrite) && !empty($product->link_rewrite[$id_lang])))
                $product->link_rewrite = self::createMultiLangField($link_rewrite);

            // replace the value of separator by coma
            if ($this->multiple_value_separator != ',')
                if (is_array($product->meta_keywords))
                    foreach ($product->meta_keywords as &$meta_keyword)
                        if (!empty($meta_keyword))
                            $meta_keyword = str_replace($this->multiple_value_separator, ',', $meta_keyword);

            // Convert comma into dot for all floating values
            foreach (Product::$definition['fields'] as $key => $array)
                if ($array['type'] == Product::TYPE_FLOAT)
                    $product->{$key} = str_replace(',', '.', $product->{$key});
            // Indexation is already 0 if it's a new product, but not if it's an update
            $product->indexed = 0;

            $res = false;
//            $field_error = $product->validateFields(UNFRIENDLY_ERROR, true);
//            $lang_field_error = $product->validateFieldsLang(UNFRIENDLY_ERROR, true);
//            if ($field_error == true)
//            {
            // check quantity
            if ($product->quantity == null)
                $product->quantity = 0;


            // If id product && id product already in base, trying to update
            if ($product->id && Product::existsInDatabase((int)$product->id, 'product'))
            {
                $datas = Db::getInstance()->getRow('
						SELECT product_shop.`date_add`
						FROM `'._DB_PREFIX_.'product` p
						'.Shop::addSqlAssociation('product', 'p').'
						WHERE p.`id_product` = '.(int)$product->id, false);
                $product->date_add = pSQL($datas['date_add']);
                $res = $product->update();
            }
            // If no id_product or update failed
            $product->force_id = (bool)$force_ids;

            if (!$res)
            {
                if (isset($product->date_add) && $product->date_add != '')
                    $res = $product->add(false);
                else
                    $res = $product->add();
            }

            if ($product->getType() == Product::PTYPE_VIRTUAL)
                StockAvailable::setProductOutOfStock((int)$product->id, 1);
            else
                StockAvailable::setProductOutOfStock((int)$product->id, (int)$product->out_of_stock);

//            }

            $shops = array();
            $product_shop = explode($this->multiple_value_separator, $product->shop);
            foreach ($product_shop as $shop)
            {
                if (empty($shop))
                    continue;
                $shop = trim($shop);
                if (!empty($shop) && !is_numeric($shop))
                    $shop = Shop::getIdByName($shop);

                if (in_array($shop, $shop_ids))
                    $shops[] = $shop;
                else
                    $this->addProductWarning(Tools::safeOutput($info['name']), $product->id, $this->l('Shop is not valid'));
            }
            if (empty($shops))
                $shops = Shop::getContextListShopID();
            // If both failed, mysql error

            if (!$res)
            {
                $this->errors[] = sprintf(
                    Tools::displayError('%1$s (ID: %2$s) cannot be saved'),
                    (isset($info['name']) && !empty($info['name']))? Tools::safeOutput($info['name']) : 'No Name',
                    (isset($info['id']) && !empty($info['id']))? Tools::safeOutput($info['id']) : 'No ID'
                );
                $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '').
                    Db::getInstance()->getMsgError();

            }
            else
            {
                // Product supplier
                if (isset($product->id) && $product->id && isset($product->id_supplier) && property_exists($product, 'supplier_reference'))
                {
                    $id_product_supplier = (int)ProductSupplier::getIdByProductAndSupplier((int)$product->id, 0, (int)$product->id_supplier);
                    if ($id_product_supplier)
                        $product_supplier = new ProductSupplier($id_product_supplier);
                    else
                        $product_supplier = new ProductSupplier();

                    $product_supplier->id_product = (int)$product->id;
                    $product_supplier->id_product_attribute = 0;
                    $product_supplier->id_supplier = (int)$product->id_supplier;
                    $product_supplier->product_supplier_price_te = $product->wholesale_price;
                    $product_supplier->product_supplier_reference = $product->supplier_reference;
                    $product_supplier->save();
                }

                // SpecificPrice (only the basic reduction feature is supported by the import)
                if (!$shop_is_feature_active)
                    $info['shop'] = 1;
                elseif (!isset($info['shop']) || empty($info['shop']))
                    $info['shop'] = implode($this->multiple_value_separator, Shop::getContextListShopID());

                // Get shops for each attributes
                $info['shop'] = explode($this->multiple_value_separator, $info['shop']);

                $id_shop_list = array();
                foreach ($info['shop'] as $shop)
                    if (!empty($shop) && !is_numeric($shop))
                        $id_shop_list[] = (int)Shop::getIdByName($shop);
                    elseif (!empty($shop))
                        $id_shop_list[] = $shop;



                if ((isset($info['reduction_price']) && $info['reduction_price'] > 0) || (isset($info['reduction_percent']) && $info['reduction_percent'] > 0))
                    foreach ($id_shop_list as $id_shop)
                    {
                        $specific_price = SpecificPrice::getSpecificPrice($product->id, $id_shop, 0, 0, 0, 1, 0, 0, 0, 0);

                        if (is_array($specific_price) && isset($specific_price['id_specific_price']))
                            $specific_price = new SpecificPrice((int)$specific_price['id_specific_price']);
                        else
                            $specific_price = new SpecificPrice();
                        $specific_price->id_product = (int)$product->id;
                        $specific_price->id_specific_price_rule = 0;
                        $specific_price->id_shop = $id_shop;
                        $specific_price->id_currency = 0;
                        $specific_price->id_country = 0;
                        $specific_price->id_group = 0;
                        $specific_price->price = -1;
                        $specific_price->id_customer = 0;
                        $specific_price->from_quantity = 1;
                        $specific_price->reduction = (isset($info['reduction_price']) && $info['reduction_price']) ? $info['reduction_price'] : $info['reduction_percent'] / 100;
                        $specific_price->reduction_type = (isset($info['reduction_price']) && $info['reduction_price']) ? 'amount' : 'percentage';
                        $specific_price->from = (isset($info['reduction_from']) && Validate::isDate($info['reduction_from'])) ? $info['reduction_from'] : '0000-00-00 00:00:00';
                        $specific_price->to = (isset($info['reduction_to']) && Validate::isDate($info['reduction_to']))  ? $info['reduction_to'] : '0000-00-00 00:00:00';
                        if (!$specific_price->save())
                            $this->addProductWarning(Tools::safeOutput($info['name']), $product->id, $this->l('Discount is invalid'));
                    }

                if (isset($product->tags) && !empty($product->tags))
                {
                    if (isset($product->id) && $product->id)
                    {
                        $tags = Tag::getProductTags($product->id);
                        if (is_array($tags) && count($tags))
                        {
                            if (!empty($product->tags))
                                $product->tags = explode($this->multiple_value_separator, $product->tags);
                            if (is_array($product->tags) && count($product->tags))
                            {
                                foreach ($product->tags as $key => $tag)
                                    if (!empty($tag))
                                        $product->tags[$key] = trim($tag);
                                $tags[$id_lang] = $product->tags;
                                $product->tags = $tags;
                            }
                        }
                    }
                    // Delete tags for this id product, for no duplicating error
                    Tag::deleteTagsForProduct($product->id);
                    if (!is_array($product->tags) && !empty($product->tags))
                    {
                        $product->tags = self::createMultiLangField($product->tags);
                        foreach ($product->tags as $key => $tags)
                        {
                            $is_tag_added = Tag::addTags($key, $product->id, $tags, $this->multiple_value_separator);
                            if (!$is_tag_added)
                            {
                                $this->addProductWarning(Tools::safeOutput($info['name']), $product->id, $this->l('Tags list is invalid'));
                                break;
                            }
                        }
                    }
                    else
                    {
                        foreach ($product->tags as $key => $tags)
                        {
                            $str = '';
                            foreach ($tags as $one_tag)
                                $str .= $one_tag.$this->multiple_value_separator;
                            $str = rtrim($str, $this->multiple_value_separator);

                            $is_tag_added = Tag::addTags($key, $product->id, $str, $this->multiple_value_separator);
                            if (!$is_tag_added)
                            {
                                $this->addProductWarning(Tools::safeOutput($info['name']), (int)$product->id, 'Invalid tag(s) ('.$str.')');
                                break;
                            }
                        }
                    }
                }

                //delete existing images if "delete_existing_images" is set to 1
                if (isset($product->delete_existing_images))
                    if ((bool)$product->delete_existing_images)
                        $product->deleteImages();

                if (isset($product->image) && is_array($product->image) && count($product->image))
                {
                    $product_has_images = (bool)Image::getImages($this->context->language->id, (int)$product->id);
                    foreach ($product->image as $key => $url)
                    {
                        $url = trim($url);
                        $error = false;
                        if (!empty($url))
                        {
                            $url = str_replace(' ', '%20', $url);

                            $image = new Image();
                            $image->id_product = (int)$product->id;
                            $image->position = Image::getHighestPosition($product->id) + 1;
                            $image->cover = (!$key && !$product_has_images) ? true : false;
                            // file_exists doesn't work with HTTP protocol
                            if (($field_error = $image->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                                ($lang_field_error = $image->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $image->add())
                            {
                                // associate image to selected shops
                                $image->associateTo($shops);
                                if (!self::copyImg($product->id, $image->id, $url, 'products', !$regenerate))
                                {
                                    $image->delete();
                                    $this->warnings[] = sprintf(Tools::displayError('Error copying image: %s'), $url);
                                }
                            }
                            else
                                $error = true;
                        }
                        else
                            $error = true;

                        if ($error)
                            $this->warnings[] = sprintf(Tools::displayError('Product #%1$d: the picture (%2$s) cannot be saved.'), $image->id_product, $url);
                    }
                }

                if (isset($product->id_category) && is_array($product->id_category))
                    $product->updateCategoriesInImport(array_map('intval', $product->id_category));

//                d($product);
                $product->checkDefaultAttributes();
                if (!$product->cache_default_attribute)
                    Product::updateDefaultAttribute($product->id);

                // Features import
                foreach ($features_lang[$id_lang_uk] as $key=>$single_feature)
                {
                    if (empty($single_feature))
                        continue;
                    $tab_feature = explode(':', $single_feature);
                    $feature_name = isset($tab_feature[0]) ? trim($tab_feature[0]) : '';
                    $feature_value = isset($tab_feature[1]) ? trim($tab_feature[1]) : '';
                    $position = isset($tab_feature[2]) ? (int)$tab_feature[2] - 1 : false;
                    $custom = isset($tab_feature[3]) ? (int)$tab_feature[3] : false;


                    $tab_feature_ru = explode(':', $features_lang[$id_lang_ru][$key]);
                    $feature_name_ru = isset($tab_feature_ru[0]) ? trim($tab_feature_ru[0]) : '';
                    $feature_value_ru = isset($tab_feature_ru[1]) ? trim($tab_feature_ru[1]) : '';
                    $position_ru = isset($tab_feature_ru[2]) ? (int)$tab_feature_ru[2] - 1 : false;
                    $custom_ru = isset($tab_feature_ru[3]) ? (int)$tab_feature_ru[3] : false;

                    if (!empty($feature_name) && !empty($feature_value))
                    {
                        $feature_name_multilang = [
                            $id_lang_uk => $feature_name,
                            $id_lang_ru => $feature_name_ru,
                        ];

                        $feature_value_multilang = [
                             $id_lang_uk => $feature_value,
                            $id_lang_ru => $feature_value_ru,
                        ];

                        $id_feature = (int)self::addFeatureImport($feature_name, $position, $feature_name_multilang);
                        $id_product = (int)$product->id;
                        $id_feature_value = (int)self::addFeatureValueImport($id_feature, $feature_value, $id_product, $id_lang, $custom, $feature_value_multilang);



                        Product::addFeatureProductImport($product->id, $id_feature, $id_feature_value);
                    }
                }
                // clean feature positions to avoid conflict
                Feature::cleanPositions();

                // set advanced stock managment
                if (isset($product->advanced_stock_management))
                {
                    if ($product->advanced_stock_management != 1 && $product->advanced_stock_management != 0)
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management has incorrect value. Not set for product %1$s '), $product->name[$default_language]);
                    elseif (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $product->advanced_stock_management == 1)
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, cannot enable on product %1$s '), $product->name[$default_language]);
                    elseif ($update_advanced_stock_management_value)
                        $product->setAdvancedStockManagement($product->advanced_stock_management);
                    // automaticly disable depends on stock, if a_s_m set to disabled
                    if (StockAvailable::dependsOnStock($product->id) == 1 && $product->advanced_stock_management == 0)
                        StockAvailable::setProductDependsOnStock($product->id, 0);
                }

                // Check if warehouse exists
                if (isset($product->warehouse) && $product->warehouse)
                {
                    if (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, warehouse not set on product %1$s '), $product->name[$default_language]);
                    else
                    {
                        if (Warehouse::exists($product->warehouse))
                        {
                            // Get already associated warehouses
                            $associated_warehouses_collection = WarehouseProductLocation::getCollection($product->id);
                            // Delete any entry in warehouse for this product
                            foreach ($associated_warehouses_collection as $awc)
                                $awc->delete();
                            $warehouse_location_entity = new WarehouseProductLocation();
                            $warehouse_location_entity->id_product = $product->id;
                            $warehouse_location_entity->id_product_attribute = 0;
                            $warehouse_location_entity->id_warehouse = $product->warehouse;
                            if (WarehouseProductLocation::getProductLocation($product->id, 0, $product->warehouse) !== false)
                                $warehouse_location_entity->update();
                            else
                                $warehouse_location_entity->save();
                            StockAvailable::synchronize($product->id);
                        }
                        else
                            $this->warnings[] = sprintf(Tools::displayError('Warehouse did not exist, cannot set on product %1$s.'), $product->name[$default_language]);
                    }
                }

                // stock available
                if (isset($product->depends_on_stock))
                {
                    if ($product->depends_on_stock != 0 && $product->depends_on_stock != 1)
                        $this->warnings[] = sprintf(Tools::displayError('Incorrect value for "depends on stock" for product %1$s '), $product->name[$default_language]);
                    elseif ((!$product->advanced_stock_management || $product->advanced_stock_management == 0) && $product->depends_on_stock == 1)
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management not enabled, cannot set "depends on stock" for product %1$s '), $product->name[$default_language]);
                    else
                        StockAvailable::setProductDependsOnStock($product->id, $product->depends_on_stock);

                    // This code allows us to set qty and disable depends on stock
                    if (isset($product->quantity) && (int)$product->quantity)
                    {
                        // if depends on stock and quantity, add quantity to stock
                        if ($product->depends_on_stock == 1)
                        {
                            $stock_manager = StockManagerFactory::getManager();
                            $price = str_replace(',', '.', $product->wholesale_price);
                            if ($price == 0)
                                $price = 0.000001;
                            $price = round(floatval($price), 6);
                            $warehouse = new Warehouse($product->warehouse);
                            if ($stock_manager->addProduct((int)$product->id, 0, $warehouse, (int)$product->quantity, 1, $price, true))
                                StockAvailable::synchronize((int)$product->id);
                        }
                        else
                        {
                            if ($shop_is_feature_active)
                                foreach ($shops as $shop)
                                    StockAvailable::setQuantity((int)$product->id, 0, (int)$product->quantity, (int)$shop);
                            else
                                StockAvailable::setQuantity((int)$product->id, 0, (int)$product->quantity, (int)$this->context->shop->id);
                        }
                    }
                }
                else // if not depends_on_stock set, use normal qty
                {
                    if ($shop_is_feature_active)
                        foreach ($shops as $shop)
                            StockAvailable::setQuantity((int)$product->id, 0, (int)$product->quantity, (int)$shop);
                    else
                        StockAvailable::setQuantity((int)$product->id, 0, (int)$product->quantity, (int)$this->context->shop->id);
                }
            }

            $id_attribute_group = 0;
            // groups
            $groups_attributes = array();
            if (isset($atr['group']))
                foreach ($atr['group'] as $key => $group)
                {
                    if (empty($group))
                        continue;
                    $tab_group = explode(':', $group);
                    $group = trim($tab_group[0]);
                    if (!isset($tab_group[1]))
                        $type = 'select';
                    else
                        $type = trim($tab_group[1]);

                    // sets group
                    $groups_attributes[$key]['group'] = $group;

                    // if position is filled
                    if (isset($tab_group[2]))
                        $position = trim($tab_group[2]);
                    else
                        $position = false;

                    if (!isset($groups[$group]))
                    {
                        $obj = new AttributeGroup();
                        $obj->is_color_group = false;
                        $obj->group_type = pSQL($type);
                        $obj->name[$default_language] = $group;
                        $obj->public_name[$default_language] = $group;
                        $obj->position = (!$position) ? AttributeGroup::getHigherPosition() + 1 : $position;

                        if (($field_error = $obj->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                            ($lang_field_error = $obj->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true)
                        {
                            $obj->add();
                            $obj->associateTo($id_shop_list);
                            $groups[$group] = $obj->id;
                        }
                        else
                            $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');

                        // fills groups attributes
                        $id_attribute_group = $obj->id;
                        $groups_attributes[$key]['id'] = $id_attribute_group;
                    }
                    else // already exists
                    {
                        $id_attribute_group = $groups[$group];
                        $groups_attributes[$key]['id'] = $id_attribute_group;
                    }
                }

            // inits attribute
            $id_product_attribute = 0;
            $id_product_attribute_update = false;
            $attributes_to_add = array();

            $attribute_default = array(
                'upc' => '',
                'wholesale_price' => 0,
                'ecotax' => 0,
                'weight' => 0,
                'default_on' => 0,
                'available_date' => date('Y-m-d')
            );

            $info = array_merge($info,$attribute_default);

//            d($info);
            // for each attribute
            if (isset($atr['attribute']))
                foreach ( $atr['attribute'] as $key => $attribute)
                {
                    if (empty($attribute))
                        continue;
                    $tab_attribute = explode(':', $attribute);
                    $attribute = trim($tab_attribute[0]);
                    // if position is filled
                    if (isset($tab_attribute[1]))
                        $position = trim($tab_attribute[1]);
                    else
                        $position = false;

                    if (isset($groups_attributes[$key]))
                    {
                        $group = $groups_attributes[$key]['group'];
                        if (!isset($attributes[$group.'_'.$attribute]) && count($groups_attributes[$key]) == 2)
                        {
                            $id_attribute_group = $groups_attributes[$key]['id'];
                            $obj = new Attribute();
                            // sets the proper id (corresponding to the right key)
                            $obj->id_attribute_group = $groups_attributes[$key]['id'];
                            $obj->name[$default_language] = str_replace('\n', '', str_replace('\r', '', $attribute));
                            $obj->position = (!$position && isset($groups[$group])) ? Attribute::getHigherPosition($groups[$group]) + 1 : $position;

                            if (($field_error = $obj->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                                ($lang_field_error = $obj->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true)
                            {
                                $obj->add();
                                $obj->associateTo($id_shop_list);
                                $attributes[$group.'_'.$attribute] = $obj->id;
                            }
                            else
                                $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');
                        }

                        $info['minimal_quantity'] = isset($info['minimal_quantity']) && $info['minimal_quantity'] ? (int)$info['minimal_quantity'] : 1;

                        $info['wholesale_price'] = str_replace(',', '.', $info['wholesale_price']);
                        $info['price'] = str_replace(',', '.', $info['price']);
                        $info['ecotax'] = str_replace(',', '.', $info['ecotax']);
                        $info['weight'] = str_replace(',', '.', $info['weight']);
                        $info['available_date'] = Validate::isDate($info['available_date']) ? $info['available_date'] : null;

                        if (!Validate::isEan13($info['ean13']))
                        {
                            $this->warnings[] = sprintf(Tools::displayError('EAN13 "%1s" has incorrect value for product with id %2d.'), $info['ean13'], $product->id);
                            $info['ean13'] = '';
                        }

                        if ($info['default_on'])
                            $product->deleteDefaultAttributes();

                        // if a reference is specified for this product, get the associate id_product_attribute to UPDATE
                        if (isset($info['reference']) && !empty($info['reference']))
                        {
                            $id_product_attribute = $this->getCombinationByEan13($product->id, intval($info['ean13']));

                            // updates the attribute
                            if ($id_product_attribute)
                            {
                                // gets all the combinations of this product
                                $attribute_combinations = $product->getAttributeCombinations($default_language);
                                foreach ($attribute_combinations as $attribute_combination)
                                {
                                    if ($id_product_attribute && in_array($id_product_attribute, $attribute_combination))
                                    {
                                        $product->updateAttribute(
                                            $id_product_attribute,
                                            (float)$info['wholesale_price'],
                                            0,
                                            (float)$info['weight'],
                                            0,
                                            (Configuration::get('PS_USE_ECOTAX') ? (float)$info['ecotax'] : 0),
                                            array(),
                                            strval($info['reference']),
                                            strval($info['ean13']),
                                            (int)$info['default_on'],
                                            0,
                                            strval($info['upc']),
                                            (int)$info['minimal_quantity'],
                                            $info['available_date'],
                                            0,
                                            $id_shop_list
                                        );
                                        $id_product_attribute_update = true;
                                        if (isset($info['supplier_reference']) && !empty($info['supplier_reference']))
                                            $product->addSupplierReference($product->id_supplier, $id_product_attribute, $info['supplier_reference']);
                                    }
                                }
                            }
                        }

                        // if no attribute reference is specified, creates a new one
                        if (!$id_product_attribute)
                        {
                            $id_product_attribute = $product->addCombinationEntity(
                                (float)$info['wholesale_price'],
                                0,
                                (float)$info['weight'],
                                0,
                                (Configuration::get('PS_USE_ECOTAX') ? (float)$info['ecotax'] : 0),
                                (int)$info['quantity'],
                                array(),
                                strval($info['reference']),
                                0,
                                strval($info['ean13']),
                                (int)$info['default_on'],
                                0,
                                strval($info['upc']),
                                (int)$info['minimal_quantity'],
                                $id_shop_list,
                                $info['available_date']
                            );

                            if (isset($info['supplier_reference']) && !empty($info['supplier_reference']))
                                $product->addSupplierReference($product->id_supplier, $id_product_attribute, $info['supplier_reference']);
                        }

                        // fills our attributes array, in order to add the attributes to the product_attribute afterwards
                        if (isset($attributes[$group.'_'.$attribute]))
                            $attributes_to_add[] = (int)$attributes[$group.'_'.$attribute];

                        // after insertion, we clean attribute position and group attribute position
                        $obj = new Attribute();
                        $obj->cleanPositions((int)$id_attribute_group, false);
                        AttributeGroup::cleanPositions();
                    }
                }

            $product->checkDefaultAttributes();
            if (!$product->cache_default_attribute)
                Product::updateDefaultAttribute($product->id);

            if ($id_product_attribute)
            {
                // now adds the attributes in the attribute_combination table
                if ($id_product_attribute_update)
                {
                    Db::getInstance()->execute('
						DELETE FROM '._DB_PREFIX_.'product_attribute_combination
						WHERE id_product_attribute = '.(int)$id_product_attribute);
                }

                foreach ($attributes_to_add as $attribute_to_add)
                {
                    Db::getInstance()->execute('
						INSERT IGNORE INTO '._DB_PREFIX_.'product_attribute_combination (id_attribute, id_product_attribute)
						VALUES ('.(int)$attribute_to_add.','.(int)$id_product_attribute.')', false);
                }

                // set advanced stock managment
                if (isset($info['advanced_stock_management']))
                {
                    if ($info['advanced_stock_management'] != 1 && $info['advanced_stock_management'] != 0)
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management has incorrect value. Not set for product with id %d.'), $product->id);
                    elseif (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') && $info['advanced_stock_management'] == 1)
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, cannot enable on product with id %d.'), $product->id);
                    else
                        $product->setAdvancedStockManagement($info['advanced_stock_management']);
                    // automaticly disable depends on stock, if a_s_m set to disabled
                    if (StockAvailable::dependsOnStock($product->id) == 1 && $info['advanced_stock_management'] == 0)
                        StockAvailable::setProductDependsOnStock($product->id, 0, null, $id_product_attribute);
                }

                // Check if warehouse exists
                if (isset($info['warehouse']) && $info['warehouse'])
                {
                    if (!Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT'))
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, warehouse is not set on product with id %d.'), $product->id);
                    else
                    {
                        if (Warehouse::exists($info['warehouse']))
                        {
                            $warehouse_location_entity = new WarehouseProductLocation();
                            $warehouse_location_entity->id_product = $product->id;
                            $warehouse_location_entity->id_product_attribute = $id_product_attribute;
                            $warehouse_location_entity->id_warehouse = $info['warehouse'];
                            if (WarehouseProductLocation::getProductLocation($product->id, $id_product_attribute, $info['warehouse']) !== false)
                                $warehouse_location_entity->update();
                            else
                                $warehouse_location_entity->save();
                            StockAvailable::synchronize($product->id);
                        }
                        else
                            $this->warnings[] = sprintf(Tools::displayError('Warehouse did not exist, cannot set on product %1$s.'), $product->name[$default_language]);
                    }
                }

                // stock available
                if (isset($info['depends_on_stock']))
                {
                    if ($info['depends_on_stock'] != 0 && $info['depends_on_stock'] != 1)
                        $this->warnings[] = sprintf(Tools::displayError('Incorrect value for depends on stock for product %1$s '), $product->name[$default_language]);
                    elseif ((!$info['advanced_stock_management'] || $info['advanced_stock_management'] == 0) && $info['depends_on_stock'] == 1)
                        $this->warnings[] = sprintf(Tools::displayError('Advanced stock management is not enabled, cannot set depends on stock %1$s '), $product->name[$default_language]);
                    else
                        StockAvailable::setProductDependsOnStock($product->id, $info['depends_on_stock'], null, $id_product_attribute);

                    // This code allows us to set qty and disable depends on stock
                    if (isset($info['quantity']) && (int)$info['quantity'])
                    {
                        // if depends on stock and quantity, add quantity to stock
                        if ($info['depends_on_stock'] == 1)
                        {
                            $stock_manager = StockManagerFactory::getManager();
                            $price = str_replace(',', '.', $info['wholesale_price']);
                            if ($price == 0)
                                $price = 0.000001;
                            $price = round(floatval($price), 6);
                            $warehouse = new Warehouse($info['warehouse']);
                            if ($stock_manager->addProduct((int)$product->id, $id_product_attribute, $warehouse, (int)$info['quantity'], 1, $price, true))
                                StockAvailable::synchronize((int)$product->id);
                        }
                        else
                        {
                            if ($shop_is_feature_active)
                                foreach ($id_shop_list as $shop)
                                    StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], (int)$shop);
                            else
                                StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], $this->context->shop->id);
                        }

                    }
                }
                // if not depends_on_stock set, use normal qty
                else
                {
                    if ($shop_is_feature_active)
                        foreach ($id_shop_list as $shop)
                            StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], (int)$shop);
                    else
                        StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], $this->context->shop->id);
                }

            }

//            d($product);
            $this->writeParseLog(array(
                'status'=>'ok',
                'msg'=>'Обработано строк '.($current_line+1).' из '.$cnt_array.'.',
                'date'=>date('Y-m-d H:i:s'),
                'product_id'=>$product->id,
                'product_quantity'=>$product->quantity,
                'end_product'=>$current_line==$cnt_array,
                'parse_file'=>$parse_parse_time,
                'parse_cat_time'=>$parse_cattime,
                'parse_manuf_time'=>$parse_manuftime,
                'parse_product_time'=>$parse_prodtime,
                'parse_feaut_time'=>$parse_feauttime,
                'parse_attr_time'=>$parse_attrtime
            ),
                $all
            );

        }




        $this->writeParseLog(array(
            'status'=>'stop',
            'msg'=>'Обработано строк '.($current_line+1).' из '.$cnt_array.'.',
            'date_start'=>date('Y-m-d H:i:s', $time_start),
            'date_end'=>date('Y-m-d H:i:s'),
            'time_parse'=>gmdate("H:i:s", time() - $time_start),
        ),
            $all
        );


        fclose($handle);
        Module::processDeferedFuncCall();
        Module::processDeferedClearCache();
        Tag::updateTagCount();

        $this->deactivateProductsWithoutImg();
//        $this->reactivateProductsWithImg();
    }




    public function attributeImport($line_this,$id_my = false,$first = false,$fo_attr_price = 0)
    {

        ini_set('display_errors', 'On');

        $default_language = Configuration::get('PS_LANG_DEFAULT');

        $groups = array();
        foreach (AttributeGroup::getAttributesGroups($default_language) as $group)
            $groups[$group['name']] = (int)$group['id_attribute_group'];

        $attributes = array();
        foreach (Attribute::getAttributes($default_language) as $attribute)
            $attributes[$attribute['attribute_group'].'_'.$attribute['name']] = (int)$attribute['id_attribute'];

        $this->receiveTab();
        // $handle = $this->openCsvFile();
        $this->setLocale();


        $line_me = array($line_this);

        //for ($current_line = 0; $line = fgetcsv($handle, 0, $this->separator); $current_line++)
        for ($current_line = 0; $current_line < count($line_me); $current_line++)
        {

            if(!trim($line_this[1])) {
                break;
            }

            $line = $line_me;

            if (Tools::getValue('convert'))
                $line = $this->utf8EncodeArray($line[$current_line]);

            $line = $line[$current_line];
            $info = $this->getMaskedRow($line);
            $info = array_map('trim', $info);
            $info['shop'] = 1;
            $info['id_product'] = $id_my;
            $info['delete_existing_images'] = '';

            // if($line_this[19] && $first == false) {
            //     $info['image_url'] = $line_this[19];
            // } else {
            $info['image_url'] = '';
            // }

            $info['image_position'] = '';
            $info['group'] = 'Material:Material:2,Color:color:1,Size:size:0';
//            $info['attribute'] = trim($line_this[11]).','.trim($line_this[12]).','.intval($line_this[15]);
            $info['attribute'] = trim($line_this[13]).','.trim($line_this[14]).','.intval($line_this[19]);
            $info['reference'] = $line_this[1];
            $info['minimal_quantity'] = isset($info['minimal_quantity']) && $info['minimal_quantity'] ? (int)$info['minimal_quantity'] : 1;
            $info['wholesale_price'] = 0;

            if($fo_attr_price) {
                $info['price'] = (int)$fo_attr_price - (int)$line_this[21];
            } else {
                $info['price'] = $line_this[21];
            }

            $info['ecotax'] = 0;
            $info['weight'] = 0;
            $info['ean13'] = $line_this[1];
            $info['default_on'] = 1;
            $info['upc'] = '';
            $info['minimal_quantity'] = 1;
            $info['warehouse'] = '';
            $info['quantity'] = $line_this[20];

            $this->setDefaultValues($info);


            $info['shop'] = 1;


            // Get shops for each attributes

            $id_shop_list[] = $info['shop'];


            if(isset($info['id_product']))
                $product = new Product((int)$info['id_product'], false, $default_language);
            else
                continue;

            $id_image = null;

            //delete existing images if "delete_existing_images" is set to 1




            $id_attribute_group = 0;
            // groups
            $groups_attributes = array();
            if(isset($info['group']))
                foreach (explode($this->multiple_value_separator, $info['group']) as $key => $group)
                {
                    if (empty($group))
                        continue;
                    $tab_group = explode(':', $group);
                    $group = trim($tab_group[0]);
                    if (!isset($tab_group[1]))
                        $type = 'select';
                    else
                        $type = trim($tab_group[1]);

                    // sets group
                    $groups_attributes[$key]['group'] = $group;

                    // if position is filled
                    if (isset($tab_group[2]))
                        $position = trim($tab_group[2]);
                    else
                        $position = false;

                    if (!isset($groups[$group]))
                    {
                        $obj = new AttributeGroup();
                        $obj->is_color_group = false;
                        $obj->group_type = pSQL($type);
                        $obj->name[$default_language] = $group;
                        $obj->public_name[$default_language] = $group;
                        $obj->position = (!$position) ? AttributeGroup::getHigherPosition() + 1 : $position;

                        if (($field_error = $obj->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                            ($lang_field_error = $obj->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true)
                        {
                            $obj->add();
                            $obj->associateTo($id_shop_list);
                            $groups[$group] = $obj->id;
                        }
                        else
                            $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');

                        // fills groups attributes
                        $id_attribute_group = $obj->id;
                        $groups_attributes[$key]['id'] = $id_attribute_group;
                    }
                    else // already exists
                    {
                        $id_attribute_group = $groups[$group];
                        $groups_attributes[$key]['id'] = $id_attribute_group;
                    }
                }

            // inits attribute
            $id_product_attribute = 0;
            $id_product_attribute_update = false;
            $attributes_to_add = array();

            // for each attribute
            if(isset($info['attribute']))
                foreach (explode($this->multiple_value_separator, $info['attribute']) as $key => $attribute)
                {
                    if (empty($attribute))
                        continue;
                    $tab_attribute = explode(':', $attribute);
                    $attribute = trim($tab_attribute[0]);
                    // if position is filled
                    if (isset($tab_attribute[1]))
                        $position = trim($tab_attribute[1]);
                    else
                        $position = false;

                    if (isset($groups_attributes[$key]))
                    {
                        $group = $groups_attributes[$key]['group'];
                        if (!isset($attributes[$group.'_'.$attribute]) && count($groups_attributes[$key]) == 2)
                        {
                            $id_attribute_group = $groups_attributes[$key]['id'];
                            $obj = new Attribute();
                            // sets the proper id (corresponding to the right key)
                            $obj->id_attribute_group = $groups_attributes[$key]['id'];
                            $obj->name[$default_language] = str_replace('\n', '', str_replace('\r', '', $attribute));
                            $obj->position = (!$position && isset($groups[$group])) ? Attribute::getHigherPosition($groups[$group]) + 1 : $position;

                            if (($field_error = $obj->validateFields(UNFRIENDLY_ERROR, true)) === true &&
                                ($lang_field_error = $obj->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true)
                            {
                                $obj->add();
                                $obj->associateTo($id_shop_list);
                                $attributes[$group.'_'.$attribute] = $obj->id;
                            }
                            else
                                $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '');
                        }

                        $info['minimal_quantity'] = isset($info['minimal_quantity']) && $info['minimal_quantity'] ? (int)$info['minimal_quantity'] : 1;

                        $info['wholesale_price'] = str_replace(',', '.', $info['wholesale_price']);
                        $info['price'] = str_replace(',', '.', $info['price']);
                        $info['ecotax'] = str_replace(',', '.', $info['ecotax']);
                        $info['weight'] = str_replace(',', '.', $info['weight']);

                        // if a reference is specified for this product, get the associate id_product_attribute to UPDATE
                        if (isset($info['reference']) && !empty($info['reference']))
                        {
                            $id_product_attribute = Combination::getIdByReference($product->id, strval($info['reference']));

                            // updates the attribute
                            if ($id_product_attribute)
                            {
                                // gets all the combinations of this product
                                $attribute_combinations = $product->getAttributeCombinations($default_language);
                                foreach ($attribute_combinations as $attribute_combination)
                                {
                                    if ($id_product_attribute && in_array($id_product_attribute, $attribute_combination))
                                    {
                                        $product->updateAttribute(
                                            $id_product_attribute,
                                            (float)$info['wholesale_price'],
                                            (float)$info['price'],
                                            (float)$info['weight'],
                                            0,
                                            (float)$info['ecotax'],
                                            $id_image,
                                            strval($info['reference']),
                                            strval($info['ean13']),
                                            (int)$info['default_on'],
                                            0,
                                            strval($info['upc']),
                                            (int)$info['minimal_quantity'],
                                            0,
                                            null,
                                            $id_shop_list
                                        );
                                        $id_product_attribute_update = true;
                                        if (isset($info['supplier_reference']) && !empty($info['supplier_reference']))
                                            $product->addSupplierReference($product->id_supplier, $id_product_attribute, $info['supplier_reference']);
                                    }
                                }
                            }
                        }

                        // if no attribute reference is specified, creates a new one
                        if (!$id_product_attribute)
                        {
                            $id_product_attribute = $product->addCombinationEntity(
                                (float)$info['wholesale_price'],
                                (float)$info['price'],
                                (float)$info['weight'],
                                0,
                                (float)$info['ecotax'],
                                (int)$info['quantity'],
                                $id_image,
                                strval($info['reference']),
                                0,
                                strval($info['ean13']),
                                (int)$info['default_on'],
                                0,
                                strval($info['upc']),
                                (int)$info['minimal_quantity'],
                                $id_shop_list
                            );
                            if (isset($info['supplier_reference']) && !empty($info['supplier_reference']))
                                $product->addSupplierReference($product->id_supplier, $id_product_attribute, $info['supplier_reference']);
                        }

                        // fills our attributes array, in order to add the attributes to the product_attribute afterwards
                        if(isset($attributes[$group.'_'.$attribute]))
                            $attributes_to_add[] = (int)$attributes[$group.'_'.$attribute];

                        // after insertion, we clean attribute position and group attribute position
                        $obj = new Attribute();
                        $obj->cleanPositions((int)$id_attribute_group, false);
                        AttributeGroup::cleanPositions();
                    }
                }

            $product->checkDefaultAttributes();
            if (!$product->cache_default_attribute)
                Product::updateDefaultAttribute($product->id);
            if ($id_product_attribute)
            {
                // now adds the attributes in the attribute_combination table
                if ($id_product_attribute_update)
                {
                    Db::getInstance()->execute('
                        DELETE FROM '._DB_PREFIX_.'product_attribute_combination
                        WHERE id_product_attribute = '.(int)$id_product_attribute);
                }

                foreach ($attributes_to_add as $attribute_to_add)
                {
                    Db::getInstance()->execute('
                        INSERT IGNORE INTO '._DB_PREFIX_.'product_attribute_combination (id_attribute, id_product_attribute)
                        VALUES ('.(int)$attribute_to_add.','.(int)$id_product_attribute.')');
                }

                // set advanced stock managment


                // Check if warehouse exists


                // stock available

                StockAvailable::setQuantity((int)$product->id, $id_product_attribute, (int)$info['quantity'], $this->context->shop->id);


            }
        }

        //$this->closeCsvFile($handle);
    }

    public static function setDefaultValues(&$info)
    {
        foreach (self::$default_values as $k => $v)
            if (!isset($info[$k]) || $info[$k] == '')
                $info[$k] = $v;
    }

    public static function setEntityDefaultValues(&$entity)
    {
        $members = get_object_vars($entity);
        foreach (self::$default_values as $k => $v)
            if ((array_key_exists($k, $members) && $entity->$k === null) || !array_key_exists($k, $members))
                $entity->$k = $v;
    }

    public static function fillInfo($infos, $key, &$entity)
    {
        $infos = trim($infos);
        if (isset(self::$validators[$key][1]) && self::$validators[$key][1] == 'createMultiLangField' && Tools::getValue('iso_lang'))
        {
            $id_lang = Language::getIdByIso(Tools::getValue('iso_lang'));
            $tmp = call_user_func(self::$validators[$key], $infos);
            foreach ($tmp as $id_lang_tmp => $value)
                if (empty($entity->{$key}[$id_lang_tmp]) || $id_lang_tmp == $id_lang)
                    $entity->{$key}[$id_lang_tmp] = $value;
        }
        else
            if (!empty($infos) || $infos == '0') // ($infos == '0') => if you want to disable a product by using "0" in active because empty('0') return true
                $entity->{$key} = isset(self::$validators[$key]) ? call_user_func(self::$validators[$key], $infos) : $infos;

        return true;
    }

    /**
     * @param $array
     * @param $funcname
     * @param mixed $user_data
     * @return bool
     */
    public static function arrayWalk(&$array, $funcname, &$user_data = false)
    {
        if (!is_callable($funcname)) return false;

        foreach ($array as $k => $row)
            if (!call_user_func_array($funcname, array($row, $k, $user_data)))
                return false;
        return true;
    }

    public static function setLocale()
    {
        $iso_lang  = trim(Tools::getValue('iso_lang'));
        setlocale(LC_COLLATE, strtolower($iso_lang).'_'.strtoupper($iso_lang).'.UTF-8');
        setlocale(LC_CTYPE, strtolower($iso_lang).'_'.strtoupper($iso_lang).'.UTF-8');
    }

    public static function getMaskedRow($row)
    {
        $res = array();
        if (is_array(self::$column_mask))
            foreach (self::$column_mask as $type => $nb)
                $res[$type] = isset($row[$nb]) ? $row[$nb] : null;

        if (Tools::getValue('forceIds')) // if you choose to force table before import the column id is removed from the CSV file.
            unset($res['id']);

        return $res;
    }


    public function receiveTab()
    {
        $type_value = Tools::getValue('type_value') ? Tools::getValue('type_value') : array();
        foreach ($type_value as $nb => $type)
            if ($type != 'no')
                self::$column_mask[$type] = $nb;
    }

    public function utf8EncodeArray($array)
    {
        return (is_array($array) ? array_map('utf8_encode', $array) : utf8_encode($array));
    }

    public function addProductWarning($product_name, $product_id = null, $message = '')
    {
        $this->warnings[] = $product_name.(isset($product_id) ? ' (ID '.$product_id.')' : '').' '
            .Tools::displayError($message);
    }

    public function ajaxProcessSaveImportMatchs()
    {
        if ($this->tabAccess['edit'] === '1')
        {
            $match = implode('|', Tools::getValue('type_value'));
            Db::getInstance()->execute('INSERT IGNORE INTO  `'._DB_PREFIX_.'import_match` (
                                        `id_import_match` ,
                                        `name` ,
                                        `match`,
                                        `skip`
                                        )
                                        VALUES (
                                        NULL ,
                                        \''.pSQL(Tools::getValue('newImportMatchs')).'\',
                                        \''.pSQL($match).'\',
                                        \''.pSQL(Tools::getValue('skip')).'\'
                                        )');

            die('{"id" : "'.Db::getInstance()->Insert_ID().'"}');
        }
    }

    public function ajaxProcessLoadImportMatchs()
    {
        if ($this->tabAccess['edit'] === '1')
        {
            $return = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'import_match` WHERE `id_import_match` = '
                .(int)Tools::getValue('idImportMatchs'));
            die('{"id" : "'.$return[0]['id_import_match'].'", "matchs" : "'.$return[0]['match'].'", "skip" : "'
                .$return[0]['skip'].'"}');
        }
    }

    public function ajaxProcessDeleteImportMatchs()
    {
        if ($this->tabAccess['edit'] === '1')
        {
            Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'import_match` WHERE `id_import_match` = '
                .(int)Tools::getValue('idImportMatchs'));
            die;
        }
    }

    public static function getPath($file = '')
    {
        return (defined('_PS_HOST_MODE_') ? _PS_ROOT_DIR_ : _PS_ADMIN_DIR_).DIRECTORY_SEPARATOR.'import'
        .DIRECTORY_SEPARATOR.$file;
    }

    public static function createMultiLangField($field)
    {
        $languages = Language::getLanguages(false);
        $res = array();
        foreach ($languages as $lang)
            $res[$lang['id_lang']] = $field;
        return $res;
    }

    public function existsEan13InDatabase($ean13)
    {
        $row = Db::getInstance()->getRow('
		SELECT `ean13`
		FROM `'._DB_PREFIX_.'product` p
		WHERE p.ean13 = "'.pSQL($ean13).'"');

        return isset($row['ean13']);
    }

    /**
     * For a given product_attribute reference, returns the corresponding id
     *
     * @param int $id_product
     * @param string $reference
     * @return int id
     */
    public static function getCombinationByEan13($id_product, $ean13)
    {
        if (empty($ean13))
            return 0;

        $query = new DbQuery();
        $query->select('pa.id_product_attribute');
        $query->from('product_attribute', 'pa');
        $query->where('pa.ean13 = '.pSQL($ean13));
        $query->where('pa.id_product = '.(int)$id_product);

        return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($query);
    }

    protected static function searchByPath($id_lang, $path, $object_to_create = false, $method_to_create = false, $paths_lang)
    {
        $categories = explode('|', trim($path));
        $category = false;
        $id_parent_category = 2;

        foreach ($paths_lang as $id_path_lang=>$path_lang) {
            $paths_lang[$id_path_lang] = explode('|', trim($path_lang));
        }

        if (is_array($categories) && count($categories))
            foreach ($categories as $index=>$category_name)
            {
                if ($id_parent_category)
                    $category = Category::searchByNameAndParentCategoryId($id_lang, $category_name, $id_parent_category);
                else
                    $category = Category::searchByName($id_lang, $category_name, true, true);

                if (!$category && $object_to_create && $method_to_create)
                {
                    $multilang_category_name = [];
                    foreach ($paths_lang as $id_path_lang=>$path_lang) {
                        $multilang_category_name[$id_path_lang] = $path_lang[$index];
                    }

                    call_user_func_array(array($object_to_create, $method_to_create), array($id_lang, $category_name , $id_parent_category, $multilang_category_name));
                    $category = Category::searchByNameAndParentCategoryId($id_lang, $category_name, $id_parent_category);
                }
                if (isset($category['id_category']) && $category['id_category'])
                    $id_parent_category = (int)$category['id_category'];
            }

        return $category;
    }

    public function productImportCreateCat($default_language_id, $category_name, $id_parent_category = null, $multilang_category_name)
    {

        $category_to_create = new Category();
        $shop_is_feature_active = Shop::isFeatureActive();
        if (!$shop_is_feature_active) {
            $category_to_create->id_shop_default = 1;
        } else {
            $category_to_create->id_shop_default = (int)Context::getContext()->shop->id;
        }
        $category_to_create->name = $multilang_category_name;
        $category_to_create->active = 0;
        $category_to_create->id_parent = (int)$id_parent_category ? (int)$id_parent_category : (int)Configuration::get('PS_HOME_CATEGORY'); // Default parent is home for unknown category to create
        $category_link_rewrite = Tools::link_rewrite($category_to_create->name[$default_language_id]);
        $category_to_create->link_rewrite = self::createMultiLangField($category_link_rewrite);

        if (($field_error = $category_to_create->validateFields(UNFRIENDLY_ERROR, true)) === true &&
            ($lang_field_error = $category_to_create->validateFieldsLang(UNFRIENDLY_ERROR, true)) === true && $category_to_create->add()) {
            /**
             * @see AdminImportController::productImport() @ Line 1480
             * @TODO Refactor if statement
             */
            // $product->id_category[] = (int)$category_to_create->id;
        } else {
            $this->errors[] = sprintf(
                Tools::displayError('%1$s (ID: %2$s) cannot be saved'),
                $category_to_create->name[$default_language_id],
                (isset($category_to_create->id) && !empty($category_to_create->id))? $category_to_create->id : 'null'
            );
            $this->errors[] = ($field_error !== true ? $field_error : '').(isset($lang_field_error) && $lang_field_error !== true ? $lang_field_error : '').
                Db::getInstance()->getMsgError();
        }
    }

    protected static function split($field)
    {
        if (empty($field))
            return array();

        $separator = Tools::getValue('multiple_value_separator');
        if (is_null($separator) || trim($separator) == '')
            $separator = ',';

        do
        $uniqid_path = _PS_UPLOAD_DIR_.uniqid();
        while (file_exists($uniqid_path));
        file_put_contents($uniqid_path, $field);
        $tab = '';
        if (!empty($uniqid_path))
        {
            $fd = fopen($uniqid_path, 'r');
            $tab = fgetcsv($fd, MAX_LINE_SIZE, $separator);
            fclose($fd);
            if (file_exists($uniqid_path))
                @unlink($uniqid_path);
        }

        if (empty($tab) || (!is_array($tab)))
            return array();
        return $tab;
    }

    protected static function getPrice($field)
    {
        $field = ((float)str_replace(',', '.', $field));
        $field = ((float)str_replace('%', '', $field));
        return $field;
    }

    protected static function getBoolean($field)
    {
        return (bool)$field;
    }

    public static function addFeatureImport($name, $position = false, $feature_name_multilang)
    {
        $rq = Db::getInstance()->getRow('
			SELECT `id_feature`
			FROM '._DB_PREFIX_.'feature_lang
			WHERE `name` = \''.pSQL($name).'\'
			GROUP BY `id_feature`
		');
        if (empty($rq)) {
            // Feature doesn't exist, create it
            $feature = new Feature();
            $feature->name = $feature_name_multilang;
            if ($position) {
                $feature->position = (int)$position;
            } else {
                $feature->position = Feature::getHigherPosition() + 1;
            }
            $feature->add();
            return $feature->id;
        } elseif (isset($rq['id_feature']) && $rq['id_feature']) {
            if (is_numeric($position) && $feature = new Feature((int)$rq['id_feature'])) {
                $feature->position = (int)$position;
                if (Validate::isLoadedObject($feature)) {
                    $feature->update();
                }
            }

            return (int)$rq['id_feature'];
        }
    }

    public static function addFeatureValueImport($id_feature, $value, $id_product = null, $id_lang = null, $custom = false, $feature_value_multilang = null)
    {
        $id_feature_value = false;
        if (!is_null($id_product) && $id_product) {
            $id_feature_value = Db::getInstance()->getValue('
				SELECT fp.`id_feature_value`
				FROM '._DB_PREFIX_.'feature_product fp
				INNER JOIN '._DB_PREFIX_.'feature_value fv USING (`id_feature_value`)
				WHERE fp.`id_feature` = '.(int)$id_feature.'
				AND fv.`custom` = '.(int)$custom.'
				AND fp.`id_product` = '.(int)$id_product);

            if ($custom && $id_feature_value && !is_null($id_lang) && $id_lang) {
                Db::getInstance()->execute('
				UPDATE '._DB_PREFIX_.'feature_value_lang
				SET `value` = \''.pSQL($value).'\'
				WHERE `id_feature_value` = '.(int)$id_feature_value.'
				AND `value` != \''.pSQL($value).'\'
				AND `id_lang` = '.(int)$id_lang);
            }
        }

        if (!$custom) {
            $id_feature_value = Db::getInstance()->getValue('
				SELECT fv.`id_feature_value`
				FROM '._DB_PREFIX_.'feature_value fv
				LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.`id_feature_value` = fv.`id_feature_value` AND fvl.`id_lang` = '.(int)$id_lang.')
				WHERE `value` = \''.pSQL($value).'\'
				AND fv.`id_feature` = '.(int)$id_feature.'
				AND fv.`custom` = 0
				GROUP BY fv.`id_feature_value`');
        }

        if ($id_feature_value) {
            return (int)$id_feature_value;
        }

        // Feature doesn't exist, create it
        $feature_value = new FeatureValue();
        $feature_value->id_feature = (int)$id_feature;
        $feature_value->custom = (bool)$custom;
        $feature_value->value = $feature_value_multilang;
        $feature_value->add();

        return (int)$feature_value->id;
    }

}