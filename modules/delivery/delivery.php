<?php

class delivery extends Module
{
	function __construct()
	{
		$this->name = 'delivery';
		$this->tab = 'Blocks';
		$this->version = '0.1';
        $this->author = 'Bond';
        $this->tab = 'front_office_features';
		parent::__construct(); // The parent construct is required for translations

		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Доставка');
		$this->description = $this->l('Доставка');
	}

	function install()
	{
		if (!parent::install())
			return false;
		if (!$this->registerHook('extraRight'))
			return false;
		return true;
	}

	/**
	* Returns module content
	*
	* @param array $params Parameters
	* @return string Content
	*/
	function hookextraRight($params)
	{
		if (file_exists(dirname(__FILE__).'/values.xml'))
		{
			if ($xml = simplexml_load_file(dirname(__FILE__).'/values.xml'))
			{
				global $cookie, $smarty;
				$smarty->assign(array(
					'title1' => $xml->{'title1_'.$cookie->id_lang},
                    'title2' => $xml->{'title2_'.$cookie->id_lang},
                    'title3' => $xml->{'title3_'.$cookie->id_lang},
                    'title4' => $xml->{'title4_'.$cookie->id_lang},
                    'title5' => $xml->{'title5_'.$cookie->id_lang},
                    'title6' => $xml->{'title6_'.$cookie->id_lang},
                    'text1' => $xml->{'text1_'.$cookie->id_lang},
                    'text2' => $xml->{'text2_'.$cookie->id_lang},
                    'text3' => $xml->{'text3_'.$cookie->id_lang},
                    'text4' => $xml->{'text4_'.$cookie->id_lang},
                    'text5' => $xml->{'text5_'.$cookie->id_lang},
                    'text6' => $xml->{'text6_'.$cookie->id_lang},
                    
                    
					'this_path' => $this->_path
				));
				return $this->display(__FILE__, 'delivery.tpl');
			}
		}
		return false;
	}



	function getContent()
	{
		/* display the module name */
		$this->_html = '<h2>'.$this->displayName.'</h2>';

		/* update the editorial xml */
		if (isset($_POST['submitUpdate']))
		{
			// Forbidden key
			$forbidden = array('submitUpdate');

			// Generate new XML data
			$newXml = '<?xml version=\'1.0\' encoding=\'utf-8\' ?>'."\n";
			$newXml .= '<html>'."\n";
			// Making header data
			foreach ($_POST AS $key => $field)
			{
				if (_PS_MAGIC_QUOTES_GPC_)
					$field = stripslashes($field);
				if ($line = $this->putContent($newXml, $key, $field, $forbidden, 'header'))
					$newXml .= $line;
			}
			$newXml .= "\n</html>\n";

			/* write it into the editorial xml file */
			if ($fd = @fopen(dirname(__FILE__).'/values.xml', 'w'))
			{
				if (!@fwrite($fd, $newXml))
					$this->_html .= $this->displayError($this->l('Unable to write to the text file.'));
				if (!@fclose($fd))
					$this->_html .= $this->displayError($this->l('Can\'t close the text file.'));
			}
			else
				$this->_html .= $this->displayError($this->l('Unable to update the text file.<br />Please check the text file\'s writing permissions.'));
		}

		/* display the editorial's form */
		$this->_displayForm();

		return $this->_html;
	}

	private function _displayForm()
	{
		/* Languages preliminaries */
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		$iso = Language::getIsoById($defaultLanguage);
		$divLangName = 'text_left¤text_right';

		/* xml loading */
		$xml = false;
		if (file_exists(dirname(__FILE__).'/values.xml'))
				if (!$xml = @simplexml_load_file(dirname(__FILE__).'/values.xml'))
					$this->_html .= $this->displayError($this->l('Your text file is empty.'));

		$this->_html .= '
		<script language="javascript">id_language = Number('.$defaultLanguage.');</script>
		<form method="post" action="'.$_SERVER['REQUEST_URI'].'">
			<fieldset>
				<legend><img src="'.$this->_path.'logo.gif" alt="" title="" /> '.$this->displayName.'</legend>
				<label>'.$this->l('Оглавление видов доставок').'</label>
				<div class="margin-form">';

				foreach ($languages as $language)
				{
					$this->_html .= '
					<div id="text_title1_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Оглавление доставки1').'</label>
                        <textarea cols="64" rows="1" id="title1_'.$language['id_lang'].'" name="title1_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'title1_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    <div id="text_title2_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Оглавление доставки2').'</label>
                        <textarea cols="64" rows="1" id="title1_'.$language['id_lang'].'" name="title2_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'title2_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    <div id="text_title3_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Оглавление доставки3').'</label>
                        <textarea cols="64" rows="1" id="title1_'.$language['id_lang'].'" name="title3_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'title3_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    <div id="text_title4_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Оглавление доставки4').'</label>
                        <textarea cols="64" rows="1" id="title1_'.$language['id_lang'].'" name="title4_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'title4_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    <div id="text_title5_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Оглавление доставки5').'</label>
                        <textarea cols="64" rows="1" id="title1_'.$language['id_lang'].'" name="title5_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'title5_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    <div id="text_title6_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Оглавление доставки6').'</label>
                        <textarea cols="64" rows="1" id="title1_'.$language['id_lang'].'" name="title6_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'title6_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    
                    ';
                    
				 }

				$this->_html .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'text_left', true);

				$this->_html .= '
				</div>
				<label>'.$this->l('Описание доставки').'</label>
				<div class="margin-form">';

				foreach ($languages as $language)
				{
					$this->_html .= '
					<div id="text_text1_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Описание доставки1').'</label>
                        <textarea cols="64" rows="2" id="text1_'.$language['id_lang'].'" name="text1_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'text1_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    
                   	<div id="text_text2_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Описание доставки2').'</label>
                        <textarea cols="64" rows="2" id="text2_'.$language['id_lang'].'" name="text2_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'text2_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    
                    	<div id="text_text3_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Описание доставки3').'</label>
                        <textarea cols="64" rows="2" id="text3_'.$language['id_lang'].'" name="text3_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'text3_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    
                    	<div id="text_text4_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Описание доставки4').'</label>
                        <textarea cols="64" rows="2" id="text4_'.$language['id_lang'].'" name="text4_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'text4_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    	<div id="text_text5_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Описание доставки5').'</label>
                        <textarea cols="64" rows="2" id="text5_'.$language['id_lang'].'" name="text5_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'text5_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    	<div id="text_text6_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
						<label>'.$this->l('Описание доставки6').'</label>
                        <textarea cols="64" rows="2" id="text6_'.$language['id_lang'].'" name="text6_'.$language['id_lang'].'">'.($xml ? stripslashes(htmlspecialchars($xml->{'text6_'.$language['id_lang']})) : '').'</textarea>
					</div>
                    
                    
                    
                    ';
				 }

				$this->_html .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'text_right', true);

				$this->_html .= '
					
				</div>
				<div class="clear pspace"></div>
				<div class="margin-form clear"><input type="submit" name="submitUpdate" value="'.$this->l('Обновить текст').'" class="button" /></div>
			</fieldset>
		</form>';
	}

	function putContent($xml_data, $key, $field, $forbidden)
	{
		foreach ($forbidden AS $line)
			if ($key == $line)
				return 0;
		$field = htmlspecialchars($field);
		if (!$field)
			return 0;
		return ("\n".'		<'.$key.'>'.$field.'</'.$key.'>');
	}

}
?>
