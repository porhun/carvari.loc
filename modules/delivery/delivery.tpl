<!-- Block delivery module -->
<script type="text/javascript" language="JavaScript" src="/modules/delivery/js/window2.js"></script>
<link rel="stylesheet" type="text/css" href="/modules/delivery/css/window2.css"/>
<link rel="stylesheet" type="text/css" href="/modules/delivery/css/delivery.css"/>


	
<!-- HTML code -->
<div id="block_delivery" class="delivery">

    
    
    
    
    
    <div class="r-corner">
	<div class="delivery-payment-wrap">
        <div class="row half">
        <div class="cell" id="deliveries_methods">
            <h4 class="sprite1 delivery-hdr">Доставка<i></i></h4>
										
          <ul>
					 <li>
							<a class="xhr" href="#?w=500" rel="popup_courier" name="kiev">{$title1}</a>
						</li>
							<li>
							<a class="xhr" href="#?w=500" rel="popup_ukraine" name="2">{$title2} <span style="white-space:nowrap;"> 30 грн.</span></a>
							</li>
							<li>
							<a class="xhr" href="#?w=500" rel="popup_courier_ukraine" name="courier">{$title3}</a>
							</li>
		</ul>
							</div>


							<div class="cell" id="payments_methods">
							<h4 class="sprite2 payment-hdr">Оплата<i></i></h4>
            <ul>
							<li><a class="xhr" href="#?w=500" rel="popup_obtaining" >{$title4}</a></li>
							<li><a class="xhr" href="#?w=500" rel="popup_non-cash" >{$title5}</a></li>
							<li><a class="xhr" href="#?w=500" rel="popup_bank" >{$title6}</a></li>
            </ul>
											</div>
										</div>
									</div>
								
								</div>
    
    
    

            <div id="popup_courier" class="popup_block">
                <div name="content">
                {$text1}
                </div>

            </div>

             <div id="popup_ukraine" class="popup_block">
                        <div name="content">
                        {$text2}
                        </div>
             </div>

             <div id="popup_courier_ukraine" class="popup_block">
                        <div name="content"> 
                        {$text3}
                        </div>
            </div>


                  <div id="popup_obtaining" class="popup_block">
                        <div name="content">
                        {$text4}
                        </div>
            </div>
            
                  <div id="popup_non-cash" class="popup_block">
                        <div name="content">
                        {$text5}
                        </div>
            </div>


                  <div id="popup_bank" class="popup_block">
                        <div name="content"> 
                        {$text6}
                        </div>
            </div>









</div>
<!-- /  Block delivery module -->
