var FfEcommerceAnalytics = {
    event_page_types: {
        product: 'detailView',
        order_confirmed: 'transaction'
    },
    ajax_urls: {
        cart_update: '/module/ffecommerceanalytics/cart_update/',
    },
    actionProductAddToCart: function (id_product, qty, id_attribute) {
        var qty = (qty) ? qty : 1;
        var id_attribute = (id_attribute && id_attribute != null) ? id_attribute : 0;
        var data = { id_product: id_product, update_way: 'add', quantity: qty, id_attribute: id_attribute };
        this.getEcommerceInfo(this.ajax_urls.cart_update, data, function (ecommerce_info) {
            this.pushEcommerceInfo(ecommerce_info.event_type, ecommerce_info.event_info);
        }.bind(this));
    },
    actionProductRemoveFromCart: function (id_product, qty, id_attribute) {
        var data = { id_product: id_product, update_way: 'delete', quantity: qty, id_attribute: id_attribute };
        this.getEcommerceInfo(this.ajax_urls.cart_update, data, function (ecommerce_info) {
            this.pushEcommerceInfo(ecommerce_info.event_type, ecommerce_info.event_info);
        }.bind(this));
    },

    actionOrderConfirmed: function (event_type, ecommerce_info) {
        if (!event_type || !ecommerce_info.currencyCode || !ecommerce_info.purchase) {
            console.error('No required info given');
            return;
        }
        this.pushEcommerceInfo(event_type, ecommerce_info);
    },
    actionProductItemDetail: function (event_type, ecommerce_info) {
        if (!ecommerce_info.detail || !event_type) {
            console.error('No required info given');
            return;
        }
        this.pushEcommerceInfo(event_type, ecommerce_info);
    },

    pushEcommerceInfo: function (event_type, ecommerce_info) {
        if (dataLayer == 'undefined' || dataLayer == null) {
            return;
        }
        dataLayer.push({
            'event': event_type,
            'ecommerce': ecommerce_info
        });
    },

    getEcommerceInfo: function (ajax_url, ajaxData, successCallback) {
        $.ajax({
            url: ajax_url,
            type: 'POST',
            dataType: 'JSON',
            data: {data: ajaxData, ajax: 1},
            success: function (rsp) {
                if (!rsp || !rsp.status || rsp.status != 'ok' || !rsp.ecommerce_info) {
                    console.error('Cant get e-commerse info');
                    return false;
                }
                successCallback(rsp.ecommerce_info);
            },
            error: function (rsp) {
                console.error('Cannot get e-commerce info');
            }
        });
    },

    initEvents: function () {
        eeGlobal.addListener('cart-productAdd', function (args) {
            var info = args[0];
            if (info.id_product) {
                FfEcommerceAnalytics.actionProductAddToCart(info.id_product, info.quantity, info.id_attribute);
            }
        });
        eeGlobal.addListener('cart-productRemove', function (args) {
            var info = args[0];
            if (info.id_product) {
                FfEcommerceAnalytics.actionProductRemoveFromCart(info.id_product, info.quantity, info.id_attribute);
            }
        });
    }
};
$(document).ready(function () {
    if (eeGlobal !== 'undefined') {
        FfEcommerceAnalytics.initEvents();
    }
    if (ecommerce_event_type && ecommerce_info) {
        switch (ecommerce_event_type) {
            case FfEcommerceAnalytics.event_page_types.product:
                FfEcommerceAnalytics.actionProductItemDetail(ecommerce_event_type, ecommerce_info);
                break;
            case FfEcommerceAnalytics.event_page_types.order_confirmed:
                FfEcommerceAnalytics.actionOrderConfirmed(ecommerce_event_type, ecommerce_info);
                break;
        }
    }
});