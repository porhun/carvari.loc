<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 19.12.16
 * Time: 11:30
 */

class FfEcommerceEventInfo
{
    protected $event_info_skeleton = ['id' => 0, 'name' => '', 'price' => 0, 'brand' => '', 'category' => '', 'variant' => '', 'quantity' => 0, 'coupon' => ''];

    /**
     * Return special info for given event type
     * Method is constructed using event type name from class FfEcommerceEventTypes
     *
     * @param string $event_type    Event type from FFEcommerceEventTypes
     * @param array $products
     * @param Context|null $context
     * @return array|mixed+
     */
    public function getInfoForEvent($event_type, array $products, Context $context)
    {
        $method = $this->prepareEventMethod($event_type);
        if (!$method || !method_exists($this, $method)) {
            return [];
        }
        return call_user_func([$this, $method], $products, $context);
    }

    /**
     * Create method to retrieve event info
     *
     * @param string $event_type    Event type from FFEcommerceEventTypes
     * @return string
     */
    protected function prepareEventMethod($event_type)
    {
        return sprintf('get%sEventInfo', ucfirst($event_type));
    }

    protected function getTransactionEventInfo(array $products, Context $context)
    {
        /**
         * @var Order $order
         */
        $order = $context->order;
        $order_info = $this->formatOrderInfo($order);
        return [
            'currencyCode' => (new Currency($order->id_currency))->iso_code,
            'purchase'     => ['actionField' => $order_info, 'products' => $this->formatProducts($products)],
        ];
    }

    protected function getAddToCartEventInfo(array $products)
    {
        $event_info = ['add' => ['products' => $this->formatProducts($products)]];
        return $event_info;
    }

    protected function getRemoveFromCartEventInfo(array $products)
    {
        $event_info = ['remove' => ['products' => $this->formatProducts($products)]];
        return $event_info;
    }

    protected function getDetailViewEventInfo(array $products)
    {
        $event_info = ['detail' => ['products' => $this->formatProducts($products)]];
        return $event_info;
    }

    /**
     * Format order info for e-commerce
     *
     * @param Order $order  Current confirmed order
     * @return array
     */
    protected function formatOrderInfo(Order $order)
    {
        $cart_rules_coupon = array_pop($order->getCartRules());
        $order_info = [
            'id' => $order->id,
            'affiliation' => '',
            'revenue' => sprintf('%01.2f', (float)$order->total_paid),
            'tax' => '',
            'shipping' => sprintf('%01.2f', (float)$order->total_shipping),
            'coupon' => ($cart_rules_coupon) ? $cart_rules_coupon['name'] : ''
        ];
        return $order_info;
    }

    /**
     * Formatting products by skeleton
     *
     * @param array $products
     * @return array
     */
    protected function formatProducts(array $products)
    {
        $formatted_products = [];
        array_walk($products, function (Product $product) use (&$formatted_products) {
            $formatted_product = [];
            $cat_names = $this->getCategoriesNames($product->getParentCategories());
            $product_price = (float)$product->getPrice(false, null, 2);
            $product_quantity = ((int)$product->ecomm_quantity) ? (int)$product->ecomm_quantity : 1;
            $formatted_product['id'] = $product->id;
            $formatted_product['name'] = (is_array($product->name)) ? array_pop($product->name) : $product->name;
            $formatted_product['price'] = $product_price;
            $formatted_product['brand'] = $product->getWsManufacturerName();
            $formatted_product['category'] = implode('/', $cat_names);
            $formatted_product['variant'] = ($product->ecomm_attribute) ?: '';
            $formatted_product['quantity'] = $product_quantity;
            $formatted_product['coupon'] = '';  // Coupon are being used for order only
            $formatted_product = array_merge($this->event_info_skeleton, $formatted_product);
            $formatted_products[] = $formatted_product;
        });
        return $formatted_products;
    }

    protected function getCategoriesNames(array $categories)
    {
        $categories = array_filter($categories, function ($category) { return !in_array($category['id_category'], [1,2]); });
        return array_column($categories, 'name');
    }
}