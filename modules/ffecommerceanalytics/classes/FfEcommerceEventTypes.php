<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 19.12.16
 * Time: 11:27
 */
class FfEcommerceEventTypes
{
    const ORDER_CONFIRMED = 'transaction';
    const CART_PRODUCT_ADD = 'addToCart';
    const CART_PRODUCT_REMOVE = 'removeFromCart';
    const PRODUCT_ITEM_DETAIL = 'detailView';
}