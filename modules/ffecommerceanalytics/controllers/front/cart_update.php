<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 19.12.16
 * Time: 12:27
 */
class ffecommerceanalyticscart_updateModuleFrontController extends ModuleFrontControllerCore
{
    public function checkAccess()
    {
        return $this->ajax && Tools::getIsset('data');
    }

    public function postProcess()
    {
        $data = Tools::getValue('data');
        $id_product = (int)$data['id_product'];
        $quantity = (int)$data['quantity'];
        $id_attribute = ($data['id_attribute']) ?: 0;
        $update_way = $data['update_way'];
        if (!$id_product || !$update_way) {
            die(Tools::jsonEncode(['status' => 'error']));
        }
        $event_info = HookCore::exec('actionEcommerceCartUpdate', compact('id_product', 'update_way', 'quantity', 'id_attribute'), Module::getModuleIdByName($this->module->name), true);
        die(Tools::jsonEncode(['status' => 'ok', 'ecommerce_info' => $event_info[$this->module->name]]));
    }
}