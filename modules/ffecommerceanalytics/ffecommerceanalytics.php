<?php

if (!defined('_PS_VERSION_'))
    exit;

require_once __DIR__.'/classes/FfEcommerceEventTypes.php';
class FfEcommerceAnalytics extends Module
{

    const FF_ECA_CID = 'ff_eca_cid';
    const CART_WAY_REMOVE = 'delete';
    const CART_WAY_ADD = 'add';

    protected $event_info_service = null;

    public function __construct()
    {
        $this->name = 'ffecommerceanalytics';
        $this->tab = 'front_office_features';
        $this->version = '1.0';
        $this->author = 'shandur';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Google Ecommerce Analytics for Tag Manager');
        $this->description = $this->l('Google Ecommerce Analytics for site pages via Tag Manager');
        $this->confirmUninstall = $this->l('Вы уверены что хотите деинсталировать модуль?');

    }


    public function install()
    {
        if (!parent::install() || !$this->registerHook('displayEcommerceAnalytics') || !$this->registerHook('header')
            || !$this->registerHook('displayProductContent') || !$this->registerHook('actionOrderDetail')
            || !$this->registerHook('moduleRoutes') || !$this->registerHook('actionEcommerceCartUpdate')
        )
            return false;

        Configuration::updateValue(self::FF_ECA_CID, '');

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;

        Configuration::deleteByName(self::FF_ECA_CID);
        return true;
    }

    public function displayConfigForm()
    {
        $this->fields_options = array(
            'general' => array(
                'title' => $this->l('Настройка модуля'),
                'fields' =>
                    array(
                        self::FF_ECA_CID => array(
                            'title' => $this->l('Tag Manager ID'),
                            'type' => 'text',
                            'cast' => 'strval',
                        ),
                    ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'button',
                    'required' => true,
                    'name' => 'submitOptions'
                ),
            )
        );

        $helper = new HelperOptions();
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->module = $this;
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = array(
            'save' => array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                    '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ),
        );
        return $helper->generateOptions($this->fields_options);
    }

    protected function getEventService()
    {
        if (is_null($this->event_info_service)) {
            require_once __DIR__.'/classes/FfEcommerceEventInfo.php';
            $this->event_info_service = new FfEcommerceEventInfo();
        }
        return $this->event_info_service;
    }

    protected function getInfoForEvent($event, array $products)
    {
        return $this->getEventService()->getInfoForEvent($event, $products, $this->context);
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitOptions')) {
            ConfigurationCore::updateValue(self::FF_ECA_CID, Tools::getValue(self::FF_ECA_CID, ''));
        }
        return $this->displayConfigForm();
    }

    public function hookHeader($params)
    {
        $this->context->controller->addjs($this->getLocalPath().'/views/js/ffecommerceanalytics.js');
    }

    public function hookDisplayEcommerceAnalytics($params)
    {
        $this->context->smarty->assign('google_tag_manager_id', Configuration::get(self::FF_ECA_CID, ''));
        return $this->display(__FILE__, 'ecommerce_code.tpl');
    }

    /**
     * Product page
     * @param $params
     */
    public function hookDisplayProductContent($params)
    {
        $ecommerce_event_type = FfEcommerceEventTypes::PRODUCT_ITEM_DETAIL;
        $ecommerce_info = $this->getEventService()->getInfoForEvent($ecommerce_event_type, [$params['product']], $this->context);
        $this->context->smarty->assign(compact('ecommerce_event_type', 'ecommerce_info'));
    }

    /**
     * Order confirmed page
     * @param $params
     */
    public function hookActionOrderDetail($params)
    {
        /**
         * @var Order $order
         */
        $order = $params['order'];
        $ecommerce_event_type = FfEcommerceEventTypes::ORDER_CONFIRMED;
        $context = clone $this->context;
        $context->order = $order;
        $order_products = array_map(function ($order_product) use ($params) {
            $product = new Product($order_product['id_product']);
            $params['quantity'] = (int)$order_product['product_quantity'];
            $params['id_attribute'] = (int)$order_product['product_attribute_id'];
            $this->addAdditionalProductFields($product, $params);
            return $product;
        }, $order->getProducts());
        $ecommerce_info = $this->getEventService()->getInfoForEvent($ecommerce_event_type, $order_products, $context);
        $this->context->smarty->assign(compact('ecommerce_event_type', 'ecommerce_info'));
        return;
    }

    protected function addAdditionalProductFields(Product $product, $params)
    {
        $product->ecomm_quantity = ($params['quantity']) ? (int)$params['quantity'] : 1;
        $attr_obj = ($params['id_attribute']) ? $product->getAttributeCombinationsById($params['id_attribute'], $this->context->language->id) : '';
        $product->ecomm_attribute = ($attr_obj) ? $attr_obj[0]['attribute_name'] : '';
    }

    /**
     * Action Add/update product in the cart (for ajax)
     * @param $params
     * @return array
     */
    public function hookActionEcommerceCartUpdate($params)
    {
        if (!($update_way = $params['update_way']) || !($id_product = (int)$params['id_product'])) {
            return [];
        }
        $product = new Product($id_product);
        $update_cart_event = ($update_way == self::CART_WAY_REMOVE) ? FfEcommerceEventTypes::CART_PRODUCT_REMOVE : FfEcommerceEventTypes::CART_PRODUCT_ADD;
        $this->addAdditionalProductFields($product, $params);
        $event_service_info = $this->getInfoForEvent($update_cart_event, [$product]);
        return ['event_type' => $update_cart_event, 'event_info' => $event_service_info];
    }

    /**
     * For simple pages (not ajax)
     * @param array $event_info
     * @param $event_type
     */
    protected function assignEventInfo(array $event_info, $event_type)
    {
        $this->context->smarty->assign('ecommerce_event_type', $event_type, true);
        $this->context->smarty->assign('ecommerce_info', $event_info, true);
    }

    public function hookModuleRoutes($params)
    {
        return [
            "module-{$this->name}-cart_update" => [
                'controller' => 'cart_update',
                'rule' => "module/{$this->name}/cart_update/",
                'keywords' => [],
                'params' => [
                    'fc' => 'module',
                    'module' => $this->name,
                ],
            ]
        ];
    }


}