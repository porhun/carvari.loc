    <div class="footer">
        <div class="phone">
            0 800 30-55-54
        </div>
        <div class="footer_social_list">
            <ul>
                <li>
                    <a href="#" class="social fb">
                        <i class="icon">
                            <svg>
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#facebook_icon">
                                </use></svg>
                        </i>
                    </a>
                </li>
                <li>
                    <a href="#" class="social vk">
                        <i class="icon">
                            <svg>
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#vk_icon">
                                </use></svg>
                        </i>
                    </a>
                </li>
                <li>
                    <a href="#" class="social tt">
                        <i class="icon">
                            <svg>
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#tt_icon">
                                </use></svg>
                        </i>
                    </a>
                </li>
                <li>
                    <a href="#" class="social yt">
                        <i class="icon">
                            <svg>
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#yt_icon">
                                </use></svg>
                        </i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="change_lang">
            <a href="#" class="footer_lang_control">Росiйскою</a>
        </div>
        <div class="copyright">
            &copy; 2015, Carvari мережа фiрмових магазинiв та аксесуарiв
        </div>
    </div>
</div>

<div class="slide_to_top">
    <i class="icon">
        <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#dropdown_arrow">
            </use></svg>
    </i>
</div>
</body>
</html>