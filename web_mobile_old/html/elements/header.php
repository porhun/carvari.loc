<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title>Old Carvari</title>

        <meta name="keywords" content="" />
        <meta name="description" content="" />
        <meta content="width=640, initial-scale=0.5, user-scalable=no" name="viewport">
<!--        <meta content="width=640, initial-scale=1, user-scalable=no" name="viewport">-->
<!--        <meta content="width=device-width, initial-scale=1, user-scalable=no" name="viewport">-->
        <meta name="format-detection" content="telephone=no" />


        <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />

        <link rel="stylesheet" type="text/css" href="../plugins/slickslider/slick.css" />
        <link rel="stylesheet" type="text/css" href="../plugins/slickslider/slick-theme.css" />
        <link rel="stylesheet" type="text/css" href="../plugins/formstyler/jquery.formstyler.css" />
        <link rel="stylesheet" type="text/css" href="../plugins/mcustomscrollbar/jquery.mCustomScrollbar.css" />
        <link rel="stylesheet/less" type="text/css" href="../css/main.less?<?php echo rand(0, 99999) ?>" />

        <script src="../js/less-1.3.3.min.js"></script>
        <script src="../js/jquery-1.11.3.min.js"></script>
        <script src="../plugins/mcustomscrollbar/jquery.mCustomScrollbar.js"></script>
        <script src="../plugins/formstyler/jquery.formstyler.min.js"></script>
        <script src="../plugins/slickslider/slick.min.js"></script>
        <script src="../plugins/slickslider/slick.min.js"></script>
        <script src="../js/main.js"></script>
    </head>
<body>
<div class="body_wrapper">
    <svg xmlns="http://www.w3.org/2000/svg" style="display:none">
        <symbol id="dropdown_arrow" viewBox="0 0 18 10">
            <polyline fill="none" stroke-width="3" stroke-miterlimit="10" points="1,1 9,9 17,1 "/>
        </symbol>
        <symbol id="facebook_icon" viewBox="0 0 13.833 25">
            <path d="M0.833,9h3V7.278V6.221V6.045c0-1.019,0.587-2.591,1.326-3.563C5.938,1.451,7.292,0.75,9.132,0.75
	c2.997,0,4.401,0.427,4.401,0.427L13.009,4.7c0,0-1.246-0.284-2.168-0.284c-0.926,0-2.008,0.328-2.008,1.254v0.375v1.233V9h4.37
	l-0.746,4H8.833v11h-5V13h-3V9L0.833,9z"/>
        </symbol>
        <symbol id="vk_icon" viewBox="0 0 19.833 25">
            <path d="M18.702,17.32c0,1.084-0.205,2.036-0.615,2.856s-0.962,1.499-1.655,2.036
			c-0.82,0.645-1.721,1.104-2.703,1.377C12.748,23.863,11.5,24,9.986,24H1.833V2h7.127c1.602,0,2.814,0.155,3.641,0.277
			c0.824,0.122,1.599,0.426,2.321,0.816c0.781,0.42,1.364,1.005,1.751,1.708c0.385,0.703,0.578,1.525,0.578,2.443
			c0,1.064-0.474,2.018-1.007,2.848c-0.531,0.83-1.411,1.446-2.411,1.837v0.117c2,0.283,2.634,0.862,3.444,1.736
			S18.702,15.836,18.702,17.32z M11.686,8.355c0-0.352-0.091-0.713-0.271-1.084s-0.461-0.64-0.842-0.806
			C10.211,6.31,9.783,6.13,9.291,6.116C8.797,6.101,8.063,6,7.086,6H6.833v5h0.56c0.938,0,1.594-0.109,1.97-0.128
			s0.765-0.171,1.165-0.357c0.439-0.205,0.742-0.513,0.908-0.875S11.686,8.834,11.686,8.355z M13.063,17.218
			c0-0.674-0.137-1.201-0.41-1.582s-0.684-0.669-1.23-0.864c-0.332-0.127-0.789,0.034-1.369,0.019C9.471,14.776,8.692,15,7.716,15
			H6.833v5h0.165c1.426,0,2.412-0.009,2.959-0.029s1.104-0.151,1.67-0.395c0.498-0.215,0.861-0.53,1.092-0.945
			C12.947,18.216,13.063,17.745,13.063,17.218z"/>
        </symbol>
        <symbol id="tt_icon" viewBox="0 0 28.833 25">
            <path d="M27,4.485c-0.951,0.422-1.973,0.707-3.045,0.835c1.094-0.656,1.936-1.695,2.33-2.933
	c-1.023,0.607-2.158,1.048-3.367,1.287C21.951,2.644,20.574,2,19.049,2c-2.928,0-5.302,2.373-5.302,5.301
	c0,0.415,0.048,0.82,0.138,1.208C9.479,8.288,5.571,6.178,2.958,2.97C2.501,3.753,2.24,4.664,2.24,5.635
	c0,1.839,0.752,3.462,2.176,4.412c-0.87-0.027-1.583-0.265-2.583-0.663c0,0.022,0,0.045,0,0.067c0,2.568,2.009,4.711,4.433,5.199
	c-0.443,0.121-0.82,0.186-1.304,0.186c-0.342,0-0.63-0.033-0.952-0.095c0.673,2.106,2.655,3.639,4.974,3.681
	c-1.813,1.422-4.088,2.27-6.572,2.27c-0.428,0-0.843-0.025-1.259-0.074C3.5,22.122,6.288,23,9.282,23
	c9.752,0,15.086-8.079,15.086-15.084c0-0.229-0.003-0.458-0.015-0.686C25.39,6.483,26.289,5.549,27,4.485z"/>
        </symbol>
        <symbol id="yt_icon" viewBox="0 0 29.833 25">
            <path d="M24.833,3h-20c-2.2,0-4,1.8-4,4v12c0,2.2,1.8,4,4,4h20c2.201,0,4-1.8,4-4V7C28.833,4.8,27.035,3,24.833,3z
	 M11.833,18V8l8.98,5L11.833,18z"/>
        </symbol>
        <symbol id="go_back_icon" viewBox="0 0 21.576 33.25">
            <line stroke-width="3" stroke-miterlimit="10" x1="1375.11" y1="203.956" x2="1360" y2="219.066"/>
            <line stroke-width="3" stroke-miterlimit="10" x1="1376.115" y1="232.961" x2="1360" y2="216.845"/>
            <line fill="none" stroke="#373636" stroke-width="3" stroke-miterlimit="10" x1="15.11" y1="1.956" x2="0" y2="17.067"/>
            <line fill="none" stroke="#373636" stroke-width="3" stroke-miterlimit="10" x1="16.115" y1="30.961" x2="0" y2="14.845"/>
        </symbol>
        <symbol id="cart_icon" viewBox="0 0 29.833 33">
            <g>
                <path stroke="none" d="M26.538,9.299c-0.78,3.733-2.704,13.2-2.704,15.701c0,0.806,0.516,4.111,0.822,6H5.012
		c0.306-1.889,0.822-5.194,0.822-6c0-2.501-1.924-11.968-2.704-15.701C5.896,9.872,11.736,11,14.834,11S23.772,9.872,26.538,9.299
		 M27.834,8c0,0-9,2-13,2s-13-2-13-2s3,14,3,17c0,1-1,7-1,7h22c0,0-1-6-1-7C24.834,22,27.834,8,27.834,8L27.834,8z"/>
            </g>
            <path fill="none" stroke-linecap="square" stroke-miterlimit="10" d="M9.834,14l-1-5c0,0-1-7,6-7s6,7,6,7l-1,5"/>
            <circle stroke-linecap="square" stroke-miterlimit="10" cx="9.834" cy="14" r="1"/>
            <circle stroke-linecap="square" stroke-miterlimit="10" cx="19.834" cy="14" r="1"/>
        </symbol>
        <symbol id="subscribe_icon" viewBox="0 0 33 33">
            <rect x="0" y="0" width="33" height="33"/>
            <polyline fill="none" stroke="#FFFFFF" stroke-width="2" stroke-miterlimit="10" points="9,18 14,22 23,9 "/>
        </symbol>
        <symbol id="close_icon" viewBox="0 0 11 11">
            <path d="M0.881,0l4.28,4.381L9.442,0l0.88,0.88L6.021,5.261l4.301,4.38l-0.88,0.881L5.161,6.142l-4.28,4.381
		L0,9.642l4.281-4.38L0,0.88L0.881,0z"/>
        </symbol>

    </svg>
    <div class="header">
        <div class="logo_holder">
            <a href="index.php"></a>
        </div>
        <span class="js_menu_item menu_burger">
            <div></div>
            <div></div>
            <div></div>
            <ul style="display: none;">
                <li><a href="#">Увійти в аккаунт</a></li>
                <li><a href="#">Про магазин</a></li>
                <li><a href="#">Доставка</a></li>
                <li><a href="#">Оплата</a></li>
                <li><a href="#">Гарантія</a></li>
                <li><a href="#">Обмін та повернення</a></li>
            </ul>
        </span>
        <div class="menu">
            <div class="menu_item js_menu_item">Чоловікам
                <ul style="display: none">
                    <li><a href="#">Черевики</a></li>
                    <li><a href="#">Чоботи</a></li>
                    <li><a href="#">Туфлі</a></li>
                    <li><a href="#">Мокасини</a></li>
                    <li><a href="#">Сандалі</a></li>
                    <li><a href="#">Еспадриліі</a></li>
                    <li><a href="#">Кросівки</a></li>
                    <li><a href="#">Вузття для дому</a></li>
                    <li><a href="#">Сумки</a></li>
                    <li><a href="#">Аксесуари</a></li>
                    <li><a href="#">Топ продаж</a></li>
                    <li><a href="#">Акції</a></li>
                </ul>
            </div>
            <div class="menu_item js_menu_item">Жінкам
                <ul style="display: none">
                    <li><a href="#">Босоножечки</a></li>
                    <li><a href="#">Ботиночки</a></li>
                    <li><a href="#">Туфельки</a></li>
                    <li><a href="#">Спожечки</a></li>
                    <li><a href="#">Аксессуарчики</a></li>
                    <li><a href="#">Обмін та повернення</a></li>
                </ul>
            </div>
        </div>
        <a href="#" class="cart">
            <i class="icon">
                <svg>
                    <use xlink:href="#cart_icon">
                </svg>
            </i>

            2
        </a>
    </div>
    <div class="dropdown_menu_wrapper">
        <div class="dropdown_menu" style="display: none;">
            <ul class="dd_content">

            </ul>
        </div>
        <div class="mobile_height_spike"></div>
    </div>