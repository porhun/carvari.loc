<? include "elements/header.php" ?>
<div class="main_content checkout-page">
    <h1>Замовлення</h1>
    <div class="order_list">
        <ul class="order_items">
            <li>
                <div class="item clearfix">
                    <div class="item_img">
                        <a href="#">
                            <img src="../images/temp/order_item.png" alt="">
                        </a>
                    </div>
                    <div class="item_descr">
                        <div class="title"><a href="#">Мокасини жЫночЫ</a></div>
                        <div class="code">JB007</div>
                        <div class="size">Розмір: 45.5</div>
                        <div class="price">1 482 грн</div>
                    </div>
                    <div class="delete_item">
                        <i class="icon">
                            <svg>
                                <use xlink:href="#close_icon">
                            </svg>
                        </i>
                    </div>
                </div>
            </li>
            <li>
                <div class="item clearfix">
                    <div class="item_img">
                        <a href="#">
                            <img src="../images/temp/order_item.png" alt="">
                        </a>
                    </div>
                    <div class="item_descr">
                        <div class="title"><a href="#">Мокасини жЫночЫ</a></div>
                        <div class="code">JB007</div>
                        <div class="size">Розмір: 45.5</div>
                        <div class="price">1 482 грн</div>
                    </div>
                    <div class="delete_item">
                        <i class="icon">
                            <svg>
                                <use xlink:href="#close_icon">
                            </svg>
                        </i>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="padded_block">
        <div class="discount">
            <div class="text">Використовуй купон на знижку</div>
            <input class="js_discount_input" type="text">
            <a href="#" class="discount_button">
                <i class="icon">
                    <svg>
                        <use xlink:href="#subscribe_icon">
                    </svg>
                </i>
            </a>
        </div>
        <div class="total">
            <div class="price_text clearfix">
                <span class="price_title">Всього</span>
                <span class="price">7 680 грн</span>
            </div>
            <div class="price_text clearfix">
                <span class="price_title">Доставка</span>
                <span class="price">7 680 грн</span>
            </div>
            <div class="price_text clearfix">
                <span class="price_title">Знижка</span>
                <span class="price">--</span>
            </div>
            <div class="price_text to_pay clearfix">
                <span class="price_title">До сплати</span>
                <span class="price">7 600 грн</span>
            </div>
        </div>
        <h3>Реєстрація та оформленн</h3>
        <div class="profile_nav">
                <span data-tab="tab1" class="profile_nav_link active">Постійний покупець
                    <span class="underline"></span>
                </span>
                <span data-tab="tab2" class="profile_nav_link">Купую вперше
                    <span class="underline"></span>
                </span>
        </div>
        <div class="tab_wrapper">
            <div id="tab1" class="tab_content">
                <div class="input_block">
                    <label>Телефон</label>
                    <input type="text">
                </div>
                <div class="input_block">
                    <label>Пароль</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="btn_holder login">
                    <input type="button" class="button big" value="Увійти">
                    <a href="#">Нагадати пароль</a>
                </div>
            </div>
            <div id="tab2" class="tab_content" style="display: none;">
                <div class="input_block">
                    <label>Ім'я</label>
                    <input type="text">
                </div>
                <div class="input_block wrong">
                    <label>Прізвище</label>
                    <input type="text">
                    <span class="error_msg">Какой то текст ошибки</span>
                </div>
                <div class="input_block">
                    <label>Телефон</label>
                    <input type="text">
                </div>
                <h2>
                    Доставка
                </h2>
                <div class="myself">
                    <div class="label_with_check">
                        <input type="radio" id="myself" name="myself">
                        <label class="radio_label" for="myself">Самовивіз</label>
                    </div>
                    <div class="input_block">
                        <label>З точки видачі</label>
                        <select>
                            <option>Київ</option>
                            <option>Дніпропетровськ</option>
                            <option>Харків</option>
                            <option>Житомир</option>
                        </select>
                        <i class="icon">
                            <svg>
                                <use xlink:href="#dropdown_arrow">
                            </svg>
                        </i>
                    </div>
                </div>
                <div class="post_delivery">
                    <div class="label_with_check">
                        <input type="radio" id="post_delivery" name="myself">
                        <label class="radio_label" for="post_delivery">Нова пошта</label>
                    </div>
                    <div class="input_block">
                        <label>Місто</label>
                        <select>
                            <option>Київська область</option>
                            <option>Дніпропетровська область</option>
                            <option>Харківська область</option>
                            <option>Житомирська область</option>
                        </select>
                        <i class="icon">
                            <svg>
                                <use xlink:href="#dropdown_arrow">
                            </svg>
                        </i>
                    </div>
                    <div class="input_block">
                        <label>Місто</label>
                        <select>
                            <option>Київ</option>
                            <option>Дніпропетровськ</option>
                            <option>Харків</option>
                            <option>Житомир</option>
                        </select>
                        <i class="icon">
                            <svg>
                                <use xlink:href="#dropdown_arrow">
                            </svg>
                        </i>
                    </div>
                    <div class="input_block">
                        <label>Відділення</label>
                        <select>
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                            <option>40</option>
                        </select>
                        <i class="icon">
                            <svg>
                                <use xlink:href="#dropdown_arrow">
                            </svg>
                        </i>
                    </div>
                </div>
                <h2>
                    Способ оплаты
                </h2>
                <div class="payment">
                    <div class="label_with_check">
                        <input type="radio" id="cash" name="payment">
                        <label class="radio_label" for="cash">Готівкою кур'єру</label>
                        <div class="payment_description">Оплата готівкою при отриманні замовлення кур'єрові.</div>
                    </div>
                </div>
                <div class="payment">
                    <div class="label_with_check">
                        <input type="radio" id="liqpay" name="payment">
                        <label class="radio_label" for="liqpay">Карткою LiqPay</label>
                        <div class="payment_description">Оплата готівкою при отриманні замовлення кур'єрові.</div>
                    </div>
                </div>
                <div class="btn_holder">
                    <input type="button" class="button big" value="Підтвердити">
                </div>
            </div>
        </div>
    </div>
</div>
<? include "elements/footer.php" ?>
