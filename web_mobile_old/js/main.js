$(document).ready(function(){
    //$('.js_menu_item').on('click',function(){
    //    $(this).toggleClass('active');
    //    $('.dropdown_menu').slideToggle(200);
    //    $('.body_wrapper').toggleClass('oh')
    //})
    var cur_scroll_top;

    $('.js_menu_item').click(function(){
        if($(this).hasClass('active')){
            $('.body_wrapper').css({'overflow': 'initial', 'height': 'auto'});
            $(window).scrollTop(cur_scroll_top);
            $('.dropdown_menu_wrapper').css('height', '100px');
            $(this).removeClass('active');
        }
        else{
            $('.dd_content').html($(this).find('ul').html());
            cur_scroll_top = $(window).scrollTop();
            $(window).scrollTop(0);
            var menu_height = $('.dropdown_menu > ul > li').length*90 + 350;
            if( menu_height > $('.mobile_height_spike').height() ){
                var max_height = menu_height;
            }
            else{
                var max_height = $('.mobile_height_spike').height();
            }
            $('.body_wrapper').css({'overflow': 'hidden', 'height': max_height+'px'});
            $('.dropdown_menu').css({'height': '100%','display':'block'});
            $('.dropdown_menu_wrapper').css('height', '100%');
            $('.js_menu_item.active').removeClass('active');
            $(this).addClass('active');
        }
    });

    $('.filter_wrapper').click(function(){
        if($(this).hasClass('active')){
            $('.body_wrapper').css({'overflow': 'initial', 'height': 'auto'});

            $(window).scrollTop(cur_scroll_top);

            $('.filter_list_wrapper').css('height', '100px');
            $(this).removeClass('active');
            $('.header').css('display','block');
            $('.filter_wrapper').css('top','135px');
            $('.filter_btns_wrapper').css('display','none')
        }
        else{
            cur_scroll_top = $(window).scrollTop();
            $('.header').css('display','none');
            $('.filter_wrapper').css('top','0px');
            $('.filter_btns_wrapper').css('display','block');

            $(window).scrollTop(0);

            //var menu_height = $('.filter_list > li').length*90 + 350;
            var menu_height = ($('.filter_list li').length*83) + ($('.filter_title').length*60);

            if( menu_height > $('.mobile_height_spike').height() ){
                var max_height = menu_height;
            }
            else{
                var max_height = $('.mobile_height_spike').height();
            }

            $('.body_wrapper').css({'overflow': 'hidden', 'height': max_height+'px'});

            $('.filter_list').css({'height': '100%','display':'block'});
            $('.filter_list_wrapper').css('height', '100%');

            $(this).addClass('active');
        }
    });

    if ($('.main_content.checkout-page').length) {
        setTimeout(function() {
            $('input:radio').styler();
        }, 100);
    }

    $('.profile_nav_link').on('click',function(){
        $('.profile_nav_link').removeClass('active');
        $(this).addClass('active');
        $('.tab_content').fadeOut(200);
        var tab = $(this).attr('data-tab');
        setTimeout(function(){
            $('#'+tab).fadeIn(200);
        },200)
    });


    $(window).scroll(function(){
       if ( $('body').scrollTop() > 500){
            $('.slide_to_top').fadeIn(500);
       }else{
           $('.slide_to_top').fadeOut(500);
       }
    });

    $('.slide_to_top').on('click', function(){
        $('HTML, BODY').animate({scrollTop: 0}, 500);
    });

    if( $('.body').hasClass('product-page') ){
        $('.product_image .slider').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });
    }

    if( $('.body').hasClass('trends-page') ){
        $('.slider_container').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });
    }


    if(  $('.body').hasClass('map-page')   ){
        var slider_timer = false;

        $('.slider').slick({
            slidesToShow: 1,
            arrows: false,
            dots: true,
            slidesToScroll: 1,
            swipeToSlide: true
        });

        $('.slider').on('afterChange', function(slick, slider){
            clearTimeout(slider_timer);
            var $curSlide = $('.slider .store_block[data-slick-index="'+slider.currentSlide+'"]'),
                lat = $curSlide.attr('data-lat'),
                long = $curSlide.attr('data-long');

            console.log($curSlide);
            console.log(lat +', '+long);

            slider_timer = setTimeout(function(){
                console.log('Здесь будет рилод карты с задержкой, чтобы не плодить вызовы');
                load_map(lat, long);
            }, 1000)
        });


    }
});