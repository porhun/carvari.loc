<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Prestashop SA <contact@prestashop.com>
*  @copyright  2007-2010 Prestashop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class WebserviceRequest extends WebserviceRequestCore
{

    public function getNbPointsByPrice($price)
    {
        /* Prevent division by zero */
        $points = 0;
        if ($pointRate = (float)(Configuration::get('PS_LOYALTY_POINT_RATE')))
            $points = floor(number_format($price, 2, '.', '') / $pointRate);

        return (int)$points;
    }

    /**
     * Set a webservice error
     *
     * @param int $status
     * @param string $label
     * @param int $code
     * @return void
     */
    public function setError($status, $label, $code)
    {
        global $display_errors;
        if (!isset($display_errors)) {
            $display_errors = strtolower(ini_get('display_errors')) != 'off';
        }
        if (isset($this->objOutput)) {
            $this->objOutput->setStatus($status);
        }
        $display_errors = 1;
        $this->errors[] = $display_errors ? array($code, $label) : 'Internal error. To see this error please display the PHP errors.';
    }

    /**
     * save Entity Object from XML
     *
     * @param int $successReturnCode
     * @return bool
     */
    protected function saveEntityFromXml($successReturnCode)
    {
        try {
            $xml = new SimpleXMLElement($this->_inputXml);
        } catch (Exception $error) {
            $this->setError(500, 'XML error : '.$error->getMessage()."\n".'XML length : '.strlen($this->_inputXml)."\n".'Original XML : '.$this->_inputXml, 127);
            return;
        }




        /** @var SimpleXMLElement|Countable $xmlEntities */
        $xmlEntities = $xml->children();
        $object = null;

        $ids = array();
        foreach ($xmlEntities as $entity) {
            // To cast in string allow to check null values
            if ((string)$entity->id != '') {
                $ids[] = (int)$entity->id;
            }
        }
        if ($this->method == 'PUT') {
            $ids2 = array();
            $ids2 = array_unique($ids);
            if (count($ids2) != count($ids)) {
                $this->setError(400, 'id is duplicate in request', 89);
                return false;
            }
            if ($this->resourceConfiguration['retrieveData']['className'] != 'Order') {
                if (count($xmlEntities) != count($ids)) {
                    $this->setError(400, 'id is required when modifying a resource', 90);
                    return false;
                }
            }
        } elseif ($this->method == 'POST' && count($ids) > 0) {
            $this->setError(400, 'id is forbidden when adding a new resource', 91);
            return false;
        }

        foreach ($xmlEntities as $xmlEntity) {
            /** @var SimpleXMLElement $xmlEntity */
            $attributes = $xmlEntity->children();

            if (isset($attributes->carvari)) {
                if ($this->resourceConfiguration['retrieveData']['className'] == 'Order') {
                    if ($this->method == 'POST') {
                        if (!((int)$attributes->carvari_card)) {
                            $this->setError(400, 'There is no param "carvari_card" in request', 89);
                            return false;
                        }
                        if (Order::isExistByCarvariId($attributes->carvari_order_id)) {
                            if ($attributes->bonus_used > 0){

                                include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
                                include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

                                $order = Order::getByCarvariId($attributes->carvari_order_id);

                                if (!Validate::isLoadedObject($loyalty = new LoyaltyModule(LoyaltyModule::getByOrderId($order->id))))
                                    return false;

                                $loyalty->points = (int)($this->getNbPointsByPrice($order->total_paid) - $attributes->bonus_used);
                                if ($loyalty->points>0)
                                    $loyalty->id_loyalty_state = LoyaltyStateModule::getValidationId();
                                else
                                    $loyalty->id_loyalty_state = LoyaltyStateModule::getCancelId();

                                $loyalty->save();

                                $this->objOutput->setStatus($successReturnCode);
                                return true;
                            }
                            $this->setError(400, 'There is exis order with such "carvari_order_id"', 89);
                            return false;
                        }

                        $customer = new Customer();

                        if ((Customer::isExistByCarvariCardNumber((int)$attributes->carvari_card))) {
                            $customer = Customer::getByCarvariCardNumber((int)$attributes->carvari_card);
                        } else {
                            if (!$customer->setPhoneLogin($attributes->phone_login)){
                                $this->setError(400, 'Customer exist with this phone:'.$attributes->phone_login, 89);
                                return false;
                            }
                            $customer->setWsCarvariCard((int)$attributes->carvari_card);
                            $customer->setWsOfflineOnly(1);
                            $customer->email = $attributes->email ? $attributes->email : $attributes->phone_login . '@carvari.com';
                            $customer->setWsPasswd(Customer::generatePin());
                            $customer->firstname = $attributes->firstname ? $attributes->firstname :'Ім\'я';
                            $customer->lastname = $attributes->lastname ? $attributes->lastname : 'Фамілія';
                            $customer->add();
                        }

                        if (!$customer->id){
                            $this->setError(400, 'The customer not created', 89);
                            return false;
                        }

                        if (!($id_address = Address::getFirstCustomerAddressId($customer->id))) {
                            $address = new Address();
                            $address->id_customer = $customer->id;
                            $address->id_country = (int)Configuration::get('PS_COUNTRY_DEFAULT');
                            $address->alias = 'Моя Адреса';
                            $address->lastname = $attributes->lastname ? $attributes->lastname : 'Фамілія';
                            $address->firstname = $attributes->firstname ? $attributes->firstname :'Ім\'я';
                            $address->address1 = 'Адреса';
                            $address->phone_mobile = $attributes->phone_login;
                            $address->city = $attributes->city ? $attributes->city : 'Місто';
                            $address->add();
                            $id_address = $address->id;
                        }

                        $cart = new Cart();
                        $cart->id_currency = (int)Configuration::get('PS_CURRENCY_DEFAULT');
                        $cart->id_lang = Context::getContext()->language->id;
                        $cart->secure_key = $customer->secure_key;

                        $cart->addWs();

                        $xmlEntity->addChild('id_address_delivery', $id_address);
                        $xmlEntity->addChild('id_address_invoice', $id_address);
                        $xmlEntity->addChild('id_cart', $cart->id);
                        $xmlEntity->addChild('id_currency', (int)Configuration::get('PS_CURRENCY_DEFAULT'));
                        $xmlEntity->addChild('id_lang', Context::getContext()->language->id);
                        $xmlEntity->addChild('id_customer', $customer->id);
                        $xmlEntity->addChild('id_carrier', 999);
                        $xmlEntity->addChild('payment', 'Оплата у магазині');
                        $xmlEntity->addChild('module', 'advancedcheckout');
                        $xmlEntity->addChild('total_paid', $attributes->sum);
                        $xmlEntity->addChild('total_paid_real', 0);
                        $xmlEntity->addChild('total_products', $attributes->sum);
                        $xmlEntity->addChild('total_products_wt', $attributes->sum);
                        $xmlEntity->addChild('conversion_rate', 1);
                        $xmlEntity->addChild('secure_key', $customer->secure_key);
                        $xmlEntity->addChild('reference', Order::generateReference());
                        $xmlEntity->addChild('carvari_order_id', $attributes->carvari_order_id);
                        $xmlEntity->addChild('id_shop', 1);
                        $xmlEntity->addChild('id_shop_group', 1);

                        unset($xmlEntity->firstname);
                        unset($xmlEntity->lastname);
                        unset($xmlEntity->email);
                        unset($xmlEntity->city);
                    } elseif ($this->method == 'PUT'){
                        if (isset($attributes->carvari_order_id)) {
                            $order = Order::getByCarvariId($attributes->carvari_order_id);

                            if ($order){
                                $xmlEntity->addChild('id', $order->id);
                                $xmlEntity->addChild('total_paid', $order->total_paid - $attributes->sum);
                                $xmlEntity->addChild('total_paid_real', $order->total_paid_real - $attributes->sum);
                                $xmlEntity->addChild('total_products', $order->total_products - $attributes->sum);
                                $xmlEntity->addChild('total_products_wt', $order->total_products_wt - $attributes->sum);

                                $xmlEntity->addChild('reference', $order->reference);
                                $xmlEntity->addChild('current_state', 7);
                                $xmlEntity->addChild('invoice_number', $order->invoice_number);
                                $xmlEntity->addChild('invoice_date', $order->invoice_date);
                                $xmlEntity->addChild('valid', $order->valid);
                                $xmlEntity->addChild('secure_key', $order->secure_key);
                                $xmlEntity->addChild('id_shop', 1);
                                $xmlEntity->addChild('id_shop_group', 1);
                                $xmlEntity->addChild('return_sum', $attributes->sum);
                            } else {
                                $this->setError(400, 'There is no such order with carvari_order_id:'.$attributes->carvari_order_id, 90);
                                return false;
                            }



                        }
                    }

                }
            }




            $attributes = $xmlEntity->children();

            /** @var ObjectModel $object */
            if ($this->method == 'POST') {
                $object = new $this->resourceConfiguration['retrieveData']['className']();

                if ($this->resourceConfiguration['retrieveData']['className'] == 'Customer') {
                    $object->setWsOfflineOnly(1);
                }

            } elseif ($this->method == 'PUT') {
                $object = new $this->resourceConfiguration['retrieveData']['className']((int)$attributes->id);

                if (!$object->id) {
                    $this->setError(404, 'Invalid ID', 92);
                    return false;
                }
            }
            $this->objects[] = $object;
            $i18n = false;
            // attributes


            foreach ($this->resourceConfiguration['fields'] as $fieldName => $fieldProperties) {
                $sqlId = $fieldProperties['sqlId'];

                if ($fieldName == 'id') {
                    $sqlId = $fieldName;
                }
                if (isset($attributes->$fieldName) && isset($fieldProperties['sqlId']) && (!isset($fieldProperties['i18n']) || !$fieldProperties['i18n'])) {
                    if (isset($fieldProperties['setter'])) {
                        // if we have to use a specific setter
                        if (!$fieldProperties['setter']) {
                            // if it's forbidden to set this field
                            $this->setError(400, 'parameter "'.$fieldName.'" not writable. Please remove this attribute of this XML', 93);
                            return false;
                        } else {
                            $object->$fieldProperties['setter']((string)$attributes->$fieldName);
                        }
                    } elseif (property_exists($object, $sqlId)) {
                        $object->$sqlId = (string)$attributes->$fieldName;
                    } else {
                        $this->setError(400, 'Parameter "'.$fieldName.'" can\'t be set to the object "'.$this->resourceConfiguration['retrieveData']['className'].'"', 123);
                    }
                } elseif (isset($fieldProperties['required']) && $fieldProperties['required'] && !$fieldProperties['i18n'] && $this->method != 'PUT') {
                    if ( !isset($attributes->carvari) ) {
                        $this->setError(400, 'parameter "'.$fieldName.'" required', 41);
                        return false;
                    }
                    $object->$sqlId = null;
                } elseif ((!isset($fieldProperties['required']) || !$fieldProperties['required']) && property_exists($object, $sqlId)) {
                    $object->$sqlId = null;
                }
                if (isset($fieldProperties['i18n']) && $fieldProperties['i18n']) {
                    $i18n = true;
                    if (isset($attributes->$fieldName, $attributes->$fieldName->language)) {
                        foreach ($attributes->$fieldName->language as $lang) {
                            /** @var SimpleXMLElement $lang */
                            $object->{$fieldName}[(int)$lang->attributes()->id] = (string)$lang;
                        }
                    } else {
                        $object->{$fieldName} = (string)$attributes->$fieldName;
                    }
                }
            }


            // Apply the modifiers if they exist
            foreach ($this->resourceConfiguration['fields'] as $fieldName => $fieldProperties) {
                if (isset($fieldProperties['modifier']) && isset($fieldProperties['modifier']['modifier']) && $fieldProperties['modifier']['http_method'] & constant('WebserviceRequest::HTTP_'.$this->method)) {
                    $object->{$fieldProperties['modifier']['modifier']}();
                }
            }


            if (!$this->hasErrors()) {


                if ($i18n && ($retValidateFieldsLang = $object->validateFieldsLang(false, true)) !== true) {
                    $this->setError(400, 'Validation error: "'.$retValidateFieldsLang.'"', 84);
                    return false;
                } elseif (($retValidateFields = $object->validateFields(false, true)) !== true) {
                    $this->setError(400, 'Validation error: "'.$retValidateFields.'"', 85);
                    return false;
                } else {

                    // Call alternative method for add/update
                    $objectMethod = ($this->method == 'POST' ? 'add' : 'update');
                    if (isset($this->resourceConfiguration['objectMethods']) && array_key_exists($objectMethod, $this->resourceConfiguration['objectMethods'])) {
                        $objectMethod = $this->resourceConfiguration['objectMethods'][$objectMethod];
                    }
                    $result = $object->{$objectMethod}();

//                    d($this->errors);
                    if ($result) {
                        if (isset($attributes->associations)) {
                            foreach ($attributes->associations->children() as $association) {
                                /** @var SimpleXMLElement $association */
                                // associations
                                if (isset($this->resourceConfiguration['associations'][$association->getName()])) {
                                    $assocItems = $association->children();
                                    $values = array();
                                    foreach ($assocItems as $assocItem) {
                                        /** @var SimpleXMLElement $assocItem */
                                        $fields = $assocItem->children();
                                        $entry = array();
                                        foreach ($fields as $fieldName => $fieldValue) {
                                            $entry[$fieldName] = (string)$fieldValue;
                                        }
                                        $values[] = $entry;
                                    }
                                    $setter = $this->resourceConfiguration['associations'][$association->getName()]['setter'];
                                    if (!is_null($setter) && $setter && method_exists($object, $setter) && !$object->$setter($values)) {
                                        $this->setError(500, 'Error occurred while setting the '.$association->getName().' value', 85);
                                        return false;
                                    }
                                } elseif ($association->getName() != 'i18n') {
                                    $this->setError(400, 'The association "'.$association->getName().'" does not exists', 86);
                                    return false;
                                }
                            }
                        }
                        $assoc = Shop::getAssoTable($this->resourceConfiguration['retrieveData']['table']);
                        if ($assoc !== false && $assoc['type'] != 'fk_shop') {
                            // PUT nor POST is destructive, no deletion
                            $sql = 'INSERT IGNORE INTO `'.bqSQL(_DB_PREFIX_.$this->resourceConfiguration['retrieveData']['table'].'_'.$assoc['type']).'` (id_shop, '.pSQL($this->resourceConfiguration['fields']['id']['sqlId']).') VALUES ';
                            foreach (self::$shopIDs as $id) {
                                $sql .= '('.(int)$id.','.(int)$object->id.')';
                                if ($id != end(self::$shopIDs)) {
                                    $sql .= ', ';
                                }
                            }
                            Db::getInstance()->execute($sql);
                        }
                    } else {
                        $this->setError(500, 'Unable to save resource', 46);
                    }
                }
            }
        }
        if (!$this->hasErrors()) {
            $this->objOutput->setStatus($successReturnCode);
            return true;
        }
    }

    /**
     * Execute DELETE method on a PrestaShop entity
     *
     * @return bool
     */
    protected function executeEntityDelete()
    {
        $objects = array();
        $arr_avoid_id = array();
        $ids = array();
        if (isset($this->urlFragments['id'])) {
            preg_match('#^\[(.*)\]$#Ui', $this->urlFragments['id'], $matches);
            if (count($matches) > 1) {
                $ids = explode(',', $matches[1]);
            }
        } else {
            $ids[] = (int)$this->urlSegment[1];
        }
        if (!empty($ids)) {
            foreach ($ids as $id) {
                if ($this->resourceConfiguration['retrieveData']['className'] == 'Order' && $this->urlFragments['carvari'] == 1) {
                    $object = Order::getByCarvariId($id);
                } else {
                    $object = new $this->resourceConfiguration['retrieveData']['className']((int)$id);
                }
                if (!$object->id) {
                    $arr_avoid_id[] = $id;
                } else {
                    $objects[] = $object;
                }
            }
        }

        if (!empty($arr_avoid_id) || empty($ids)) {
            $this->setError(404, 'Id(s) not exists: '.implode(', ', $arr_avoid_id), 87);
            $this->_outputEnabled = true;
        } else {
            foreach ($objects as $object) {
                /** @var ObjectModel $object */
                if (isset($this->resourceConfiguration['objectMethods']) && isset($this->resourceConfiguration['objectMethods']['delete'])) {
                    $result = $object->{$this->resourceConfiguration['objectMethods']['delete']}();
                } else {
                    $result = $object->delete();
                }

                if (!$result) {
                    $arr_avoid_id[] = $object->id;
                }
            }
            if (!empty($arr_avoid_id)) {
                $this->setError(500, 'Id(s) wasn\'t deleted: '.implode(', ', $arr_avoid_id), 88);
                $this->_outputEnabled = true;
            } else {
                $this->_outputEnabled = false;
            }
        }
    }


}
