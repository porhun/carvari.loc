<?php
class Tools extends  ToolsCore
{
    public static function getActiveLangForUrl()
    {
        $defaultLanguage  = Language::getIsoById((int)Configuration::get('PS_LANG_DEFAULT'));
        $activeLanguage   = Tools::getValue('isolang', '');
        return (!$activeLanguage || $defaultLanguage === $activeLanguage) ? '' : '/'.$activeLanguage;
    }
    public static function switchLanguage(Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }
        if (!isset($context->cookie)) {
            return;
        }
        $configuration_id_lang = Configuration::get('PS_LANG_DEFAULT');
        $id_lang = ($iso = Tools::getValue('isolang')) && Validate::isLanguageIsoCode($iso) ? (int)Language::getIdByIso($iso) : $configuration_id_lang;
        $_GET['id_lang'] = $id_lang;
        $cookie_id_lang = $context->cookie->id_lang;
        if ((($id_lang = (int)Tools::getValue('id_lang')) && Validate::isUnsignedId($id_lang) && $cookie_id_lang != (int)$id_lang)
            || (($id_lang == $configuration_id_lang) && Validate::isUnsignedId($id_lang) && $id_lang != $cookie_id_lang)) {
            $context->cookie->id_lang = $id_lang;
            $language = new Language($id_lang);
            if (Validate::isLoadedObject($language) && $language->active) {
                $context->language = $language;
            }
            $params = $_GET;
            if (Configuration::get('PS_REWRITING_SETTINGS') || !Language::isMultiLanguageActivated()) {
                unset($params['id_lang']);
            }
        }
    }
    public static function getPath($id_category, $path = '', $link_on_the_item = false, $category_type = 'products', Context $context = null)
    {
        if (!$context)
            $context = Context::getContext();
        $id_category = (int)$id_category;
        if ($id_category == 1)
            return '<span class="navigation_end">' . $path . '</span>';
        if($path)
            $path = '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><span itemprop="title">'.trim($path).'</span></li>';
        $pipe = Configuration::get('PS_NAVIGATION_PIPE');
        if (empty($pipe))
            $pipe = '>';
        $full_path = '';
        if ($category_type === 'products') {
            $interval = Category::getInterval($id_category);
            $id_root_category = $context->shop->getCategory();
            $interval_root = Category::getInterval($id_root_category);
            if ($interval) {
                $sql = 'SELECT c.id_category, cl.name, cl.link_rewrite
						FROM ' . _DB_PREFIX_ . 'category c
						LEFT JOIN ' . _DB_PREFIX_ . 'category_lang cl ON (cl.id_category = c.id_category' . Shop::addSqlRestrictionOnLang('cl') . ')
						' . Shop::addSqlAssociation('category', 'c') . '
						WHERE c.nleft <= ' . $interval['nleft'] . '
							AND c.nright >= ' . $interval['nright'] . '
							AND c.nleft >= ' . $interval_root['nleft'] . '
							AND c.nright <= ' . $interval_root['nright'] . '
							AND cl.id_lang = ' . (int)$context->language->id . '
							AND c.active = 1
							AND c.level_depth > ' . (int)$interval_root['level_depth'] . '
						ORDER BY c.level_depth ASC';
                $categories = Db::getInstance()->executeS($sql);
                $n = 1;
                $n_categories = count($categories);
                foreach ($categories as $category) {
                    $full_path .=
                        (($n < $n_categories || $link_on_the_item) ? '<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb"><a itemprop="url" href="' . Tools::safeOutput($context->link->getCategoryLink((int)$category['id_category'], $category['link_rewrite'])) . '" title="' . htmlentities($category['name'], ENT_NOQUOTES, 'UTF-8') . '" data-gg="">' : '<span  itemprop="title">') .
                        htmlentities($category['name'], ENT_NOQUOTES, 'UTF-8') .
                        (($n < $n_categories || $link_on_the_item) ? '</a></li>' : '</span></li>');
                        $n++;
                }
                return $full_path . $path;
            }
        } elseif ($category_type === 'CMS') {
            $category = new CMSCategory($id_category, $context->language->id);
            if (!Validate::isLoadedObject($category))
                die(Tools::displayError());
            $category_link = $context->link->getCMSCategoryLink($category);
            if ($path != $category->name)
                $full_path .= '<a href="' . Tools::safeOutput($category_link) . '" data-gg="">' . htmlentities($category->name, ENT_NOQUOTES, 'UTF-8') . '</a><span class="navigation-pipe">' . $pipe . '</span>' . $path;
            else
                $full_path = ($link_on_the_item ? '<a href="' . Tools::safeOutput($category_link) . '" data-gg="">' : '') . htmlentities($path, ENT_NOQUOTES, 'UTF-8') . ($link_on_the_item ? '</a>' : '');
            return Tools::getPath($category->id_parent, $full_path, $link_on_the_item, $category_type);
        }
    }
    /*
    * module: advancedcheckout
    * date: 2017-04-20 09:09:30
    * version: 3.1.4
    */
    public static function cleanNonDigitChars($string)
    {
        if (!is_string($string) || !self::strlen($string)) {
            return '';
        }
        return preg_replace('/[. ()-]*/','',$string);
    }
}