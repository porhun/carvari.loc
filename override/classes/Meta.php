<?php

class Meta extends MetaCore
{

    /**
     * Get product meta tags
     *
     * @since 1.5.0
     * @param int $id_product
     * @param int $id_lang
     * @param string $page_name
     * @return array
     */
//    public static function getProductMetas($id_product, $id_lang, $page_name)
//    {
//
//        $sql = 'SELECT `name`, `meta_title`, `meta_description`, `meta_keywords`, `description_short`
//				FROM `'._DB_PREFIX_.'product` p
//				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.`id_product` = p.`id_product`'.Shop::addSqlRestrictionOnLang('pl').')
//				'.Shop::addSqlAssociation('product', 'p').'
//				WHERE pl.id_lang = '.(int)$id_lang.'
//					AND pl.id_product = '.(int)$id_product.'
//					AND product_shop.active = 1';
//        if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
//            if (empty($row['meta_description'])) {
//                $row['meta_description'] = strip_tags($row['description_short']);
//            }
//            $row['meta_title'] =  $row['name'] . ' купити, ціна';
//            $row['meta_description'] =  "Купити ".$row['name']." в інтернет-магазині взуття і аксессуарів L'CARVARI.";
//            $row['meta_keywords'] =  $row['name'].', '.$row['name'].' купити , '.$row['name'].' ціна, купити '.$row['name'];
//
//            return Meta::completeMetaTags($row, $row['name']);
//        }
//
//
//
//        return Meta::getHomeMetas($id_lang, $page_name);
//    }
//
//    public static function getHomeMetas($id_lang, $page_name)
//    {
//        $metas = Meta::getMetaByPage($page_name, $id_lang);
//        $ret['meta_title'] = (isset($metas['title']) && $metas['title']) ? $metas['title'] : Configuration::get('PS_SHOP_NAME');
//        $ret['meta_description'] = (isset($metas['description']) && $metas['description']) ? $metas['description'] : '';
//        $ret['meta_keywords'] = (isset($metas['keywords']) && $metas['keywords']) ? $metas['keywords'] :  '';
//        return $ret;
//    }

    public static function getHomeMetas($id_lang, $page_name)
    {
        $metas = Meta::getMetaByPage($page_name, $id_lang);
        $ret['meta_title'] =  $metas['title'];
        $ret['meta_description'] = (isset($metas['description']) && $metas['description']) ? $metas['description'] : '';
        $ret['meta_keywords'] = (isset($metas['keywords']) && $metas['keywords']) ? $metas['keywords'] :  '';
        return $ret;
    }

    public static function getCmsMetas($id_cms, $id_lang, $page_name)
    {
        $sql = 'SELECT `meta_title`, `meta_description`, `meta_keywords`
				FROM `'._DB_PREFIX_.'cms_lang`
				WHERE id_lang = '.(int)$id_lang.'
					AND id_cms = '.(int)$id_cms.
            ((int)Context::getContext()->shop->id ?
                ' AND id_shop = '.(int)Context::getContext()->shop->id : '');

        if ($row = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql)) {
            $row['meta_title'] = $row['meta_title'];
            return Meta::completeMetaTags($row, $row['meta_title']);
        }

        return Meta::getHomeMetas($id_lang, $page_name);
    }

    public static function completeMetaTags($meta_tags, $default_value, Context $context = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if (empty($meta_tags['meta_title'])) {
            $meta_tags['meta_title'] = $default_value;
        }
        if (empty($meta_tags['meta_description'])) {
            $meta_tags['meta_description'] = Configuration::get('PS_META_DESCRIPTION', $context->language->id) ? Configuration::get('PS_META_DESCRIPTION', $context->language->id) : '';
        }
        if (empty($meta_tags['meta_keywords'])) {
            $meta_tags['meta_keywords'] = Configuration::get('PS_META_KEYWORDS', $context->language->id) ? Configuration::get('PS_META_KEYWORDS', $context->language->id) : '';
        }
        return $meta_tags;
    }

}
