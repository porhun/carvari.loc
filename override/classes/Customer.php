<?php
class Customer extends CustomerCore
{
	public $phone_login;
	public $is_offline_only;
	public $carvari_card_tmp;
	protected $webserviceParameters = array(
		'fields' => array(
			'id_default_group' => array('xlink_resource' => 'groups'),
			'id_lang' => array('xlink_resource' => 'languages'),
			'newsletter_date_add' => array(),
			'ip_registration_newsletter' => array(),
			'last_passwd_gen' => array('setter' => null),
			'secure_key' => array('setter' => null),
			'deleted' => array(),
			'passwd' => array('setter' => 'setWsPasswd'),
			'bonus' => array('getter' => 'getBonus', 'setter' => 'setWsBonus'),
			'phone_login' => array('setter' => 'setPhoneLogin'),
			'email' => array('setter' => 'setWsEmail'),
			'carvari_card' => array('getter' => 'getWsCarvariCard', 'setter' => 'setWsCarvariCard'),
			'is_offline_only' => array('setter' => 'setWsOfflineOnly'),
		),
		'associations' => array(
			'groups' => array('resource' => 'group'),
		),
		'linked_tables' => array(
			'carvari_card_number' => array(
				'table' => 'ffusercards',
				'fields' => array(
					'number' => array(
						'sqlId' => 'number'
					)
				)
			),
		)
	);
	public static $definition = array(
		'table' => 'customer',
		'primary' => 'id_customer',
		'fields' => array(
			'secure_key' =>                array('type' => self::TYPE_STRING, 'validate' => 'isMd5', 'copy_post' => false),
			'lastname' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
			'firstname' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isName', 'required' => true, 'size' => 32),
			'phone_login' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 13),
			'email' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isEmail', 'required' => true, 'size' => 128),
			'passwd' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isPasswd', 'required' => true, 'size' => 32),
			'last_passwd_gen' =>            array('type' => self::TYPE_STRING, 'copy_post' => false),
			'id_gender' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'birthday' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isBirthDate'),
			'newsletter' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'newsletter_date_add' =>        array('type' => self::TYPE_DATE,'copy_post' => false),
			'ip_registration_newsletter' =>    array('type' => self::TYPE_STRING, 'copy_post' => false),
			'optin' =>                        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'is_offline_only' =>              array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'website' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isUrl'),
			'company' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'siret' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isSiret'),
			'ape' =>                        array('type' => self::TYPE_STRING, 'validate' => 'isApe'),
			'outstanding_allow_amount' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'copy_post' => false),
			'show_public_prices' =>            array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'id_risk' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
			'max_payment_days' =>            array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'copy_post' => false),
			'active' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'deleted' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'note' =>                        array('type' => self::TYPE_HTML, 'validate' => 'isCleanHtml', 'size' => 65000, 'copy_post' => false),
			'is_guest' =>                    array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'id_shop' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
			'id_shop_group' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
			'id_default_group' =>            array('type' => self::TYPE_INT, 'copy_post' => false),
			'id_lang' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'copy_post' => false),
			'date_add' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
			'date_upd' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
		),
	);
	public function setWsOfflineOnly($value)
	{
		$this->is_offline_only = $value;
	}
	public function setWsBonus()
	{
		$this->bonus = 'MEGA BONUS!!!';
	}
	public function getCityName()
	{
		$id_address = Address::getFirstCustomerAddressId( $this->id );
		if ($id_address) {
			$address = new Address($id_address);
			return $address->city;
		}
		return 'NoCity';
	}
	public function getBonus()
	{
		$validity_period = Configuration::get('PS_LOYALTY_VALIDITY_PERIOD');
		$sql_period = '';
		if ((int)$validity_period > 0)
			$sql_period = ' AND datediff(NOW(),f.date_add) <= '.$validity_period;
		return
			Db::getInstance()->getValue('
		SELECT SUM(f.points) points
		FROM `'._DB_PREFIX_.'loyalty` f
		WHERE f.id_customer = '.(int)($this->id).'
		AND f.id_loyalty_state IN (2, 5)
		'.$sql_period)
			+
			Db::getInstance()->getValue('
		SELECT SUM(f.points) points
		FROM `'._DB_PREFIX_.'loyalty` f
		WHERE f.id_customer = '.(int)($this->id).'
		AND f.id_loyalty_state = 3
		AND points < 0
		'.$sql_period);
	}
	public function changeCarvariCard($cardnum, $customerid) {
	    return Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'ffusercards`
            SET `number` ='.$cardnum.' WHERE `id_customer` ='.$customerid);
    }
	public function getWsCarvariCard()
	{
		return Db::getInstance()->getValue( 'SELECT `number` FROM `'._DB_PREFIX_.'ffusercards` WHERE id_customer='.(int)($this->id) );
	}
	public function setWsCarvariCard($value)
	{
		$this->carvari_card_tmp = $value;
	}
	public static function upadtePasswordByPhone( $phone , $password )
	{
		$phone = preg_replace('/[. ()-+]*/','',$phone);
		$phone = '+'.$phone;
		DB::getInstance()->update('customer',
			array( 'passwd' => Tools::encrypt($password), 'last_passwd_gen' => date("Y-m-d G:i:s") ),
			'phone_login="'.$phone.'"');
	}
    public static function generatePin()
    {
        return rand(0,9).rand(0,9).rand(0,9).rand(0,9);
    }
    public static function updatePasswordByPhone( $phone , $password )
    {
        $phone = preg_replace('/[. +()-]*/','',$phone);
        $phone = '+'.$phone;
        DB::getInstance()->update('customer',
            array( 'passwd' => Tools::encrypt($password), 'last_passwd_gen' => date("Y-m-d G:i:s") ),
            'phone_login="'.pSQL($phone).'"');
    }
    public function getByPhone($phone, $passwd = null, $ignore_guest = true)
    {
        $phone = preg_replace('/[. +()-]*/','',$phone);
        $phone = '+'.$phone;
        $qb = new DbQuery();
        $qb->select('*');
        $qb->from('customer');
        $qb->where('phone_login = '.pSQL($phone) . Shop::addSqlRestriction(Shop::SHARE_CUSTOMER) . ' AND `deleted` = 0');
        if (!is_null($passwd) && $passwd) {
            $qb->where('`passwd` = "'.pSQL(Tools::encrypt($passwd)) .'"');
        }
        if ($ignore_guest) {
            $qb->where('is_guest = 0');
        }
        if (!$result = Db::getInstance()->getRow($qb->build()))
            return false;
        $this->id = $result['id_customer'];
        foreach ($result as $key => $value)
            if (property_exists($this, $key))
                $this->{$key} = $value;
        return $this;
    }
    public function getByEmailOrPhone( $email, $phone_login, $ignore_guest = true )
    {
        if (!Validate::isEmail($email) || ($phone_login && !Validate::isPhoneNumber($phone_login))) {
            die(Tools::displayError());
        }
        $result = Db::getInstance()->getRow('
		SELECT *
		FROM `'._DB_PREFIX_.'customer`
		WHERE ( `email` = \''.pSQL($email).'\' OR `phone_login` = \''.pSQL($phone_login).'\' )
		'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
		'.(isset($passwd) ? 'AND `passwd` = \''.pSQL(Tools::encrypt($passwd)).'\'' : '').'
		AND `deleted` = 0
		'.($ignore_guest ? ' AND `is_guest` = 0' : ''));
        if (!$result) {
            return false;
        }
        $this->id = $result['id_customer'];
        foreach ($result as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
        return $this;
    }
    public static function isCustomerExistsByPhone( $phone , $return_name = false)
    {
        $phone = preg_replace('/[. +()-]*/','',$phone);
        $phone = '+'.$phone;
        $qb = new DbQuery();
        $qb->select('*');
        $qb->from('customer');
        $qb->where('phone_login = ' . pSQL($phone));
        $qb->where('deleted = 0');
        $result = Db::getInstance()->getRow($qb->build());
        if (!$result)
            return false;
        if ($return_name)
            return $result['firstname'];
        return true;
    }
	public function getByPhone2($phone, $passwd = null, $ignore_guest = true)
	{
		$phone = preg_replace('/[. +()-]*/','',$phone);
		$phone = '+'.$phone;
		$result = Db::getInstance()->getRow('
		SELECT *
		FROM `'._DB_PREFIX_.'customer`
		WHERE `phone_login` = \''.pSQL($phone).'\'
		'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
		'.(isset($passwd) ? 'AND `passwd` = \''.pSQL(Tools::encrypt($passwd)).'\'' : '').'
		AND `deleted` = 0
		'.($ignore_guest ? ' AND `is_guest` = 0' : ''));
		if (!$result)
			return false;
		$this->id = $result['id_customer'];
		foreach ($result as $key => $value)
			if (property_exists($this, $key))
				$this->{$key} = $value;
		return $this;
	}
	public static function getByCarvariCardNumber($number)
	{
		$id_customer = Db::getInstance()->getValue( 'SELECT `id_customer` FROM `'._DB_PREFIX_.'ffusercards` WHERE `number`='.$number );
		if ( $id_customer ) {
			$customer = new Customer($id_customer);
			if ($customer->id)
				return $customer;
		}
		return false;
	}
	public static function getCarvariCardNumberById($id_customer)
	{
		$carvari_card = Db::getInstance()->getValue( 'SELECT `number` FROM `'._DB_PREFIX_.'ffusercards` WHERE `id_customer`='.$id_customer );
		if ( $carvari_card ) {
			return $carvari_card;
		}
		return false;
	}
	public static function isExistByCarvariCardNumber($number)
	{
		$id_customer = Db::getInstance()->getValue( 'SELECT count(*) as count FROM `'._DB_PREFIX_.'ffusercards` WHERE `number`='.$number );
		return (bool)$id_customer;
	}
	public static function isOfflineOnly($phone, $is_offline_only = true)
	{
        $phone = preg_replace('/[. +()-]*/','',$phone);
		$phone = '+'.$phone;
		$result = Db::getInstance()->getRow('
		SELECT *
		FROM `'._DB_PREFIX_.'customer`
		WHERE `phone_login` = \''.pSQL($phone).'\'
		'.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).'
		AND `deleted` = 0
		'.($is_offline_only ? ' AND `is_offline_only` = 1' : ''));
		if (!$result)
			return false;
		return true;
	}
	public function setPhoneLogin($phone_login)
	{
		$phone_login = '+'.preg_replace('/[. +()-]*/','',$phone_login);
		if ( Customer::isCustomerExistsByPhone($phone_login)) {
			if (defined('_PS_MODE_DEV_') && _PS_MODE_DEV_) {
				die(Tools::displayError('Customer exist with this phone: '.$phone_login));
			}
			return false;
		}
		$this->phone_login = $phone_login;
		return true;
	}
	public function setWsEmail($email)
	{
		$email=trim($email);
		if ( Customer::customerExists($email)) {
			die(Tools::displayError('Customer exist with this email: '.$email));
		}
		$this->email = $email;
	}
	/*
    * module: advancedcheckout
    * date: 2017-04-28 12:00:24
    * version: 3.1.4
    */
    public function __construct($id = null, $mod = null)
	{
		parent::__construct($id);
		if ($mod)
		{
			$res = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'advcheckout` a
											LEFT JOIN `'._DB_PREFIX_.'advcheckout_lang` al ON (a.`id_field` = al.`id_field`
											AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
											WHERE a.`required` = 1
											ORDER BY a.`position`');
											$arr = array();
			foreach ($res as $r)
				$arr[] = $r['name'];
			$this->fieldsRequired = array();
			if (in_array('birthday', $arr))
			{
				$this->def['fields']['birthday']['required'] = 1;
				$this->fieldsRequired[] = 'birthday';
			}
			else
				$this->def['fields']['birthday']['required'] = 0;
			if (in_array('gender', $arr))
			{
				$this->def['fields']['id_gender']['required'] = 1;
				$this->fieldsRequired[] = 'id_gender';
			}
			else
				$this->def['fields']['id_gender']['required'] = 0;
			if (in_array('newsletter', $arr))
			{
				$this->def['fields']['newsletter']['required'] = 1;
				$this->fieldsRequired[] = 'newsletter';
			}
			else
				$this->def['fields']['newsletter']['required'] = 0;
			if (in_array('firstname', $arr))
			{
				$this->def['fields']['firstname']['required'] = 1;
				$this->fieldsRequired[] = 'firstname';
			}
			else
				$this->def['fields']['firstname']['required'] = 0;
			if (in_array('lastname', $arr))
			{
				$this->def['fields']['lastname']['required'] = 1;
				$this->fieldsRequired[] = 'lastname';
			}
			else
				$this->def['fields']['lastname']['required'] = 0;
			if (in_array('email', $arr))
			{
				$this->def['fields']['email']['required'] = 1;
				$this->fieldsRequired[] = 'email';
			}
			else
				$this->def['fields']['email']['required'] = 0;
			if (in_array('passwd', $arr))
			{
				$this->def['fields']['passwd']['required'] = 1;
				$this->fieldsRequired[] = 'passwd';
			}
			else
				$this->def['fields']['passwd']['required'] = 0;
		}
	}

	public static function searchByNameOrPhone($query, $limit = null)
	{
		$sql_base = 'SELECT *
				FROM `'._DB_PREFIX_.'customer`';
		$sql = '('.$sql_base.' WHERE `email` LIKE \'%'.pSQL($query).'%\' '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).')';
		$sql .= ' UNION ('.$sql_base.' WHERE `id_customer` = '.(int)$query.' '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).')';
		$sql .= ' UNION ('.$sql_base.' WHERE `lastname` LIKE \'%'.pSQL($query).'%\' '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).')';
		$sql .= ' UNION ('.$sql_base.' WHERE `firstname` LIKE \'%'.pSQL($query).'%\' '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).')';
		$sql .= ' UNION ('.$sql_base.' WHERE `phone_login` LIKE \'%'.pSQL($query).'%\' '.Shop::addSqlRestriction(Shop::SHARE_CUSTOMER).')';

		if ($limit) {
			$sql .= ' LIMIT 0, '.(int)$limit;
		}

		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}

    /**
     * Validates submitted values and returns an array of errors, if any.
     *
     * @param bool $htmlentities If true, uses htmlentities() for field name translations in errors.
     *
     * @return array
     */
    public function validateController($htmlentities = true)
    {
        $this->cacheFieldsRequiredDatabase();
        $errors = array();
        $required_fields_database = (isset(self::$fieldsRequiredDatabase[get_class($this)])) ? self::$fieldsRequiredDatabase[get_class($this)] : array();

        foreach ($this->def['fields'] as $field => $data) {
            $value = trim(Tools::getValue($field, $this->{$field}));

            // Check if field is required by user
            if (in_array($field, $required_fields_database)) {
                $data['required'] = true;
            }

            // Checking for required fields
            if (isset($data['required']) && $data['required'] && empty($value) && $value !== '0') {
                if (!$this->id || $field != 'passwd') {
                    $errors[$field] = '<b>'.self::displayFieldName($field, get_class($this), $htmlentities).'</b> '.Tools::displayError('is required.');
                }
            }

            // Checking for maximum fields sizes
            if (isset($data['size']) && !empty($value) && Tools::strlen($value) > $data['size']) {
                $errors[$field] = sprintf(
                    Tools::displayError('%1$s is too long. Maximum length: %2$d'),
                    self::displayFieldName($field, get_class($this), $htmlentities),
                    $data['size']
                );
            }

            // Checking for fields validity
            // Hack for postcode required for country which does not have postcodes
            if (!empty($value) || $value === '0' || ($field == 'postcode' && $value == '0')) {
                $validation_error = false;
                if (isset($data['validate'])) {
                    $data_validate = $data['validate'];
                    if (!Validate::$data_validate($value) && (!empty($value) || $data['required'])) {
                        $errors[$field] = '<b>'.self::displayFieldName($field, get_class($this), $htmlentities).
                            '</b> '.Tools::displayError('is invalid.');
                        $validation_error = true;
                    }
                }

                if (!$validation_error) {
                    if (isset($data['copy_post']) && !$data['copy_post']) {
                        continue;
                    }
                    if ($field == 'passwd') {
                        if ($value = Tools::getValue($field)) {
                            $this->{$field} = Tools::encrypt($value);
                        }
                    } else {
                        $this->{$field} = $value;
                    }
                }
            }
        }

        return $errors;
    }
}
