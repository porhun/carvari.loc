<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 09.03.17
 * Time: 10:47
 */
class Link extends LinkCore
{
    protected function getLangLink($id_lang = null, Context $context = null, $id_shop = null)
    {
        if (!$context) {
            $context = Context::getContext();
        }

        if ((!$this->allow && in_array($id_shop, array($context->shop->id,  null))) || !Language::isMultiLanguageActivated($id_shop) || !(int)Configuration::get('PS_REWRITING_SETTINGS', null, null, $id_shop)) {
            return '';
        }

        if (!$id_lang) {
            $id_lang = $context->language->id;
        }
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');
        // If default language is current, don't show it in urls
        if ($defaultLang == $id_lang) {
            return '';
        }

        return Language::getIsoById($id_lang).'/';
    }
}