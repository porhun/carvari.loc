<?php
class Address extends AddressCore
{
	
	public static $definition = array(
		'table' => 'address',
		'primary' => 'id_address',
		'fields' => array(
			'id_customer' => 		array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
			'id_manufacturer' => 	array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
			'id_supplier' => 		array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
			'id_warehouse' => 		array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId', 'copy_post' => false),
			'id_country' => 		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_state' => 			array('type' => self::TYPE_INT, 'validate' => 'isNullOrUnsignedId'),
			'alias' => 				array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 32),
			'company' => 			array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'size' => 64),
			'lastname' => 			array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 32),
			'firstname' => 			array('type' => self::TYPE_STRING, 'validate' => 'isName', 'size' => 32),
			'vat_number' =>	 		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
			'address1' => 			array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
			'address2' => 			array('type' => self::TYPE_STRING, 'validate' => 'isAddress', 'size' => 128),
			'postcode' => 			array('type' => self::TYPE_STRING, 'validate' => 'isPostCode', 'size' => 12),
			'city' => 				array('type' => self::TYPE_STRING, 'validate' => 'isCityName', 'size' => 64),
			'other' => 				array('type' => self::TYPE_STRING, 'validate' => 'isMessage', 'size' => 300),
			'phone' => 				array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
			'phone_mobile' => 		array('type' => self::TYPE_STRING, 'validate' => 'isPhoneNumber', 'size' => 32),
			'dni' => 				array('type' => self::TYPE_STRING, 'validate' => 'isDniLite', 'size' => 16),
			'deleted' => 			array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'copy_post' => false),
			'date_add' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
			'date_upd' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDate', 'copy_post' => false),
		),
	);
	/*
	* module: advancedcheckout
	* date: 2015-08-28 10:22:53
	* version: 3.1.4
	*/
	public function validateController($htmlentities = true, $mod = null)
	{
		if ($mod)
		{
			$errors = array();
			foreach ($this->def['fields'] as $field => $data)
			{
				$value = $this->{$field};
				if (isset($data['required']) && $data['required'] && empty($value) && $value !== '0')
					if (!$this->id || $field != 'passwd')
						$errors[$field] = '<b>'.self::displayFieldName($field, get_class($this), $htmlentities).'</b> '.Tools::displayError('is required.');
				if (isset($data['size']) && !empty($value) && Tools::strlen($value) > $data['size'])
					$errors[$field] = sprintf(
						Tools::displayError('%1$s is too long. Maximum length: %2$d'),
						self::displayFieldName($field, get_class($this), $htmlentities),
						$data['size']
					);
				if (isset($data['validate']) && !Validate::$data['validate']($value) && (!empty($value) || (isset($data['required']) && $data['required'])))
						$errors[$field] = '<b>'.self::displayFieldName($field, get_class($this), $htmlentities).'</b> '.Tools::displayError('is invalid.');
			}
			return $errors;
		}
		else
			return parent::validateController($htmlentities);
	}

    public static function getWareAndCityByRef($city_ref,$ware_ref)
    {
        return Db::getInstance()->executeS('SELECT `city`,`address` FROM `'._DB_PREFIX_.'ecm_newpost_warehouse` WHERE `city_ref`="'.$city_ref.'"
        AND `ref`="'.$ware_ref.'"')[0];
    }

    public static function existCustomerAddress($id_customer,$address1,$city)
    {
        return Db::getInstance()->getValue('SELECT `id_address` FROM `'._DB_PREFIX_.'address` WHERE `id_customer`='.$id_customer.'
           AND `address1`=\''.$address1.'\' AND `city`=\''.$city.'\'');
    }
	/*
	* module: advancedcheckout
	* date: 2015-08-28 10:22:53
	* version: 3.1.4
	*/
	public function __construct($id_address = null, $id_lang = null, $set = null, $mod = null)
	{
		parent::__construct($id_address, $id_lang);
		if ($mod)
			if (!$set)
			{
				foreach ($this->def['fields'] as &$df)
					$df['copy_post'] = false;
				$res = Db::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'advcheckout` a
												LEFT JOIN `'._DB_PREFIX_.'advcheckout_lang` al ON (a.`id_field` = al.`id_field` 
												AND al.`id_lang` = '.(int)Context::getContext()->language->id.')
												WHERE a.`required` = 1
												ORDER BY a.`position`');
												$arr = array();
				foreach ($res as $r)
					$arr[] = $r['name'];
				$this->fieldsRequired = array();
				if (in_array('dni', $arr))
				{
					$this->def['fields']['dni']['required'] = 1;
					$this->fieldsRequired[] = 'firstname';
				}
				else
					$this->def['fields']['dni']['required'] = 0;
				if (in_array('vat_number', $arr))
				{
					$this->def['fields']['vat_number']['required'] = 1;
					$this->fieldsRequired[] = 'vat_number';
				}
				else
					$this->def['fields']['firstname']['required'] = 0;
				if (in_array('firstname', $arr))
				{
					$this->def['fields']['firstname']['required'] = 1;
					$this->fieldsRequired[] = 'firstname';
				}
				else
					$this->def['fields']['firstname']['required'] = 0;
				if (in_array('lastname', $arr))
				{
					$this->def['fields']['lastname']['required'] = 1;
					$this->fieldsRequired[] = 'lastname';
				}
				else
					$this->def['fields']['lastname']['required'] = 0;
				if (in_array('city', $arr))
				{
					$this->def['fields']['city']['required'] = 1;
					$this->fieldsRequired[] = 'city';
				}
				else
					$this->def['fields']['city']['required'] = 0;
				if (in_array('company', $arr))
				{
					$this->def['fields']['company']['required'] = 1;
					$this->fieldsRequired[] = 'company';
				}
				else
					$this->def['fields']['company']['required'] = 0;
				if (in_array('phone', $arr))
				{
					$this->def['fields']['phone']['required'] = 1;
					$this->fieldsRequired[] = 'phone';
				}
				else
					$this->def['fields']['phone']['required'] = 0;
				if (in_array('phone_mobile', $arr))
				{
					$this->def['fields']['phone_mobile']['required'] = 1;
					$this->fieldsRequired[] = 'phone_mobile';
				}
				else
					$this->def['fields']['phone_mobile']['required'] = 0;
				if (in_array('id_country', $arr))
				{
					$this->def['fields']['id_country']['required'] = 1;
					$this->fieldsRequired[] = 'id_country';
				}
				else
					$this->def['fields']['id_country']['required'] = 0;
				if (in_array('id_state', $arr))
				{
					$this->def['fields']['id_state']['required'] = 1;
					$this->fieldsRequired[] = 'id_state';
				}
				else
					$this->def['fields']['id_state']['required'] = 0;
				if (in_array('address1', $arr))
				{
					$this->def['fields']['address1']['required'] = 1;
					$this->fieldsRequired[] = 'address1';
				}
				else
					$this->def['fields']['address1']['required'] = 0;
				if (in_array('address2', $arr))
				{
					$this->def['fields']['address2']['required'] = 1;
					$this->fieldsRequired[] = 'address2';
				}
				else
					$this->def['fields']['address2']['required'] = 0;
				if (in_array('postcode', $arr))
				{
					$this->def['fields']['postcode']['required'] = 1;
					$this->fieldsRequired[] = 'postcode';
				}
				else
					$this->def['fields']['postcode']['required'] = 0;
				if (in_array('other', $arr))
				{
					$this->def['fields']['other']['required'] = 1;
					$this->fieldsRequired[] = 'other';
				}
				else
					$this->def['fields']['other']['required'] = 0;
			}
			elseif ($set)
			{
				$this->fieldsRequired = array();
				foreach ($this->def['fields'] as $k => &$fl)
				{
					$fl['required'] = 0;
					unset($this->def['fields'][$k]['validate']);
					unset($this->def['fields'][$k]['size']);
				}
			}
	}
}
