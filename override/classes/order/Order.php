<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license	http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class Order extends OrderCore
{

	public $bonus_used;
	public $carvari_order_id;
	public $return_sum;
	public $id_employee;

	public static $definition = array(
		'table' => 'orders',
		'primary' => 'id_order',
		'fields' => array(
			'id_address_delivery' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_address_invoice' =>        array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_cart' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_currency' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_shop_group' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'id_shop' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'id_lang' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_customer' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'id_employee' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'id_carrier' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
			'current_state' =>                array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'secure_key' =>                array('type' => self::TYPE_STRING, 'validate' => 'isMd5'),
			'payment' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
			'module' =>                    array('type' => self::TYPE_STRING, 'validate' => 'isModuleName', 'required' => true),
			'recyclable' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'gift' =>                        array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'gift_message' =>                array('type' => self::TYPE_STRING, 'validate' => 'isMessage'),
			'mobile_theme' =>                array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'total_discounts' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_discounts_tax_incl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_discounts_tax_excl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_paid' =>                array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
			'total_paid_tax_incl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_paid_tax_excl' =>        array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_paid_real' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
			'total_products' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
			'total_products_wt' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
			'total_shipping' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_shipping_tax_incl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_shipping_tax_excl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'carrier_tax_rate' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
			'total_wrapping' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_wrapping_tax_incl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'total_wrapping_tax_excl' =>    array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
			'round_mode' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'round_type' =>                    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'shipping_number' =>            array('type' => self::TYPE_STRING, 'validate' => 'isTrackingNumber'),
			'conversion_rate' =>            array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat', 'required' => true),
			'invoice_number' =>            array('type' => self::TYPE_INT),
			'delivery_number' =>            array('type' => self::TYPE_INT),
			'invoice_date' =>                array('type' => self::TYPE_DATE),
			'delivery_date' =>                array('type' => self::TYPE_DATE),
			'valid' =>                        array('type' => self::TYPE_BOOL),
			'reference' =>                    array('type' => self::TYPE_STRING),
			'carvari_order_id' =>                    array('type' => self::TYPE_STRING),
			'date_add' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
			'date_upd' =>                    array('type' => self::TYPE_DATE, 'validate' => 'isDate'),
		),
	);



	protected $webserviceParameters = array(
		'objectMethods' => array('add' => 'addWs', 'delete' => 'deleteWs', 'update' => 'updateWs'),
		'objectNodeName' => 'order',
		'objectsNodeName' => 'orders',
		'fields' => array(
			'id_address_delivery' => array('xlink_resource'=> 'addresses'),
			'id_address_invoice' => array('xlink_resource'=> 'addresses'),
			'id_cart' => array('xlink_resource'=> 'carts'),
			'id_currency' => array('xlink_resource'=> 'currencies'),
			'id_lang' => array('xlink_resource'=> 'languages'),
			'id_customer' => array('xlink_resource'=> 'customers'),
			'id_carrier' => array('xlink_resource'=> 'carriers'),
			'current_state' => array(
				'xlink_resource'=> 'order_states',
				'setter' => 'setWsCurrentState'
			),
			'module' => array('required' => true),
			'invoice_number' => array(),
			'invoice_date' => array(),
			'delivery_number' => array(),
			'delivery_date' => array(),
			'valid' => array(),
			'date_add' => array(),
			'date_upd' => array(),
			'shipping_number' => array(
				'getter' => 'getWsShippingNumber',
				'setter' => 'setWsShippingNumber'
			),
			'bonus_used' => array(),
			'carvari_order_id' => array(),
			'return_sum' => array(),
		),
		'associations' => array(
			'order_rows' => array('resource' => 'order_row', 'setter' => false, 'virtual_entity' => true,
				'fields' => array(
					'id' =>  array(),
					'product_id' => array('required' => true),
					'product_attribute_id' => array('required' => true),
					'product_quantity' => array('required' => true),
					'product_name' => array('setter' => false),
					'product_reference' => array('setter' => false),
					'product_ean13' => array('setter' => false),
					'product_upc' => array('setter' => false),
					'product_price' => array('setter' => false),
					'unit_price_tax_incl' => array('setter' => false),
					'unit_price_tax_excl' => array('setter' => false),
				)),
		),

	);

	public function addWs($autodate = true, $null_values = false)
	{
		$result = $this->add();
		if ($this->id) {
			$cusomer =  new Customer($this->id_customer);
			Hook::exec('newOrder', ['order' =>$this, 'customer' => $cusomer, 'from_ws' => 1 ], Module::getModuleIdByName('loyalty'));
		}
		return $result;
	}

	public function deleteWs()
	{
		Hook::exec('orderReturn', ['orderReturn' =>$this, 'from_ws' => 1 ], Module::getModuleIdByName('loyalty'));
		return $this->delete();
	}

	public function updateWs()
	{
		Hook::exec('orderReturn', ['orderReturn' =>$this, 'from_ws' => 1], Module::getModuleIdByName('loyalty'));
		return $this->update();
	}

	/** Set current order status
	 * @param int $id_order_state
	 * @param int $id_employee (/!\ not optional except for Webservice.
	 */
	public function setCurrentState($id_order_state, $id_employee = 0)
	{

		if (empty($id_order_state)) {
			return false;
		}

		if(empty($this->id_employee) && $id_employee) {
			$this->id_employee = $id_employee;
		}

		$history = new OrderHistory();
		$history->id_order = (int)$this->id;
		$history->id_employee = (int)$id_employee;
		$history->changeIdOrderState((int)$id_order_state, $this);
		$res = Db::getInstance()->getRow('
			SELECT `invoice_number`, `invoice_date`, `delivery_number`, `delivery_date`
			FROM `'._DB_PREFIX_.'orders`
			WHERE `id_order` = '.(int)$this->id);
		$this->invoice_date = $res['invoice_date'];
		$this->invoice_number = $res['invoice_number'];
		$this->delivery_date = $res['delivery_date'];
		$this->delivery_number = $res['delivery_number'];
		$this->update();

		if ($this->carvari_order_id){
			$history->addWoutHistory();
		} else{
			$history->addWithemail();
		}


	}

	public function setCurrentStateFF($id_order_state, $id_employee = 0)
	{

		if (empty($id_order_state)) {
			return false;
		}
		$history = new OrderHistory();
		$history->id_order = (int)$this->id;
		$history->id_employee = (int)$id_employee;
		$history->changeIdOrderState((int)$id_order_state, $this);

		$res = Db::getInstance()->getRow('
			SELECT `invoice_number`, `invoice_date`, `delivery_number`, `delivery_date`
			FROM `'._DB_PREFIX_.'orders`
			WHERE `id_order` = '.(int)$this->id);
		$this->invoice_date = $res['invoice_date'];
		$this->invoice_number = $res['invoice_number'];
		$this->delivery_date = $res['delivery_date'];
		$this->delivery_number = $res['delivery_number'];
//		$this->update();
		$history->addWoutHistory();
	}

	/**
	 * Get order products
	 *
	 * @return array Products with price, quantity (with taxe and without)
	 */
	public function getProducts($products = false, $selected_products = false, $selected_qty = false)
	{
		if (!$products)
			$products = $this->getProductsDetail();

		$customized_datas = Product::getAllCustomizedDatas($this->id_cart);

		$result_array = array();
		foreach ($products as $row)
		{
			// Change qty if selected
			if ($selected_qty)
			{
				$row['product_quantity'] = 0;
				foreach ($selected_products as $key => $id_product)
					if ($row['id_order_detail'] == $id_product)
						$row['product_quantity'] = (int)$selected_qty[$key];
				if (!$row['product_quantity'])
					continue;
			}

			$this->setProductImageInformations($row);
			$this->setProductCurrentStock($row);

			// Backward compatibility 1.4 -> 1.5
			$this->setProductPrices($row);

			$this->setProductCustomizedDatas($row, $customized_datas);

			// Add information for virtual product
			if ($row['download_hash'] && !empty($row['download_hash']))
			{
				$row['filename'] = ProductDownload::getFilenameFromIdProduct((int)$row['product_id']);
				// Get the display filename
				$row['display_filename'] = ProductDownload::getFilenameFromFilename($row['filename']);
			}

			$row['id_address_delivery'] = $this->id_address_delivery;

			$name_arr = explode('-', $row['product_name']);
			$name_arr = array_map('trim', $name_arr);

			if (isset($name_arr[0]))
				$row['name'] = $name_arr[0];
			if (isset($name_arr[1]))
				$row['attributes'] = $name_arr[1];

			/* Stock product */
			$result_array[(int)$row['id_order_detail']] = $row;
		}

		if ($customized_datas)
			Product::addCustomizationPrice($result_array, $customized_datas);

		return $result_array;
	}

	/**
	 * Get customer orders
	 *
	 * @param int $id_customer Customer id
	 * @param bool $show_hidden_status Display or not hidden order statuses
	 * @return array Customer orders
	 */
	public static function getCustomerOrders($id_customer, $show_hidden_status = false, Context $context = null , $offset = null, $limit = 3, $with_address = false)
	{
		if (!$context) {
			$context = Context::getContext();
		}
        if($limit)
            $limit = isset($offset) ? ' LIMIT '.($offset*$limit).','.$limit : ( ( $context->getMobileDevice() ) ? ' LIMIT '.$limit : '');
		$res = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT o.*, (SELECT SUM(od.`product_quantity`) FROM `'._DB_PREFIX_.'order_detail` od WHERE od.`id_order` = o.`id_order`) nb_products
		FROM `'._DB_PREFIX_.'orders` o
		WHERE o.`id_customer` = '.(int)$id_customer.'
		GROUP BY o.`id_order`
		ORDER BY o.`date_add` DESC'.$limit
		);
		if (!$res) {
			return array();
		}

		foreach ($res as $key => $val) {
			$res2 = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT os.`id_order_state`, osl.`name` AS order_state, os.`invoice`, os.`color` as order_state_color
				FROM `'._DB_PREFIX_.'order_history` oh
				LEFT JOIN `'._DB_PREFIX_.'order_state` os ON (os.`id_order_state` = oh.`id_order_state`)
				INNER JOIN `'._DB_PREFIX_.'order_state_lang` osl ON (os.`id_order_state` = osl.`id_order_state` AND osl.`id_lang` = '.(int)$context->language->id.')
			WHERE oh.`id_order` = '.(int)$val['id_order'].(!$show_hidden_status ? ' AND os.`hidden` != 1' : '').'
				ORDER BY oh.`date_add` DESC, oh.`id_order_history` DESC
			LIMIT 1');
			if ($res2) {
				$res[$key] = array_merge($res[$key], $res2[0]);
			}

			if ($with_address) {
				switch ($val['id_carrier']) {
					case Configuration::get('CARVARI_CARRIER_ID'):
						$res[$key]['address'] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT CONCAT_WS(", ", s.city, s.name, s.`address1`) as address FROM `'._DB_PREFIX_.'ffcarvaricarrier` f LEFT JOIN `'._DB_PREFIX_.'store` s ON s.id_store=f.id_store WHERE f.id_order='.$val['id_order']);
						break;
                    case Configuration::get('FF_CARVARI_CARRIER_ID'):
                        $res[$key]['address'] = Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT fc.`address` FROM `' . _DB_PREFIX_ . 'ffcarvaricourier` fc WHERE fc.id_order='.$val['id_order']);
                        break;
					case Configuration::get('ecm_ecm_novaposhta'):
						$res[$key]['address'] =  Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('SELECT CONCAT("Нова Пошта: ", enw.`city`,", ",enw.`address`) as address FROM `'._DB_PREFIX_.'ecm_newpost_orders` eno LEFT JOIN `'._DB_PREFIX_.'ecm_newpost_warehouse` enw ON eno.ware=enw.ref WHERE eno.id_order='.$val['id_order']);
						break;
					default:
						$address = new Address((int)$val['id_address_delivery']);
						$res[$key]['address'] = $address->address1;
						break;
				}


			}

		}
		return $res;
	}



	public static function getByCarvariId( $carvar_id )
	{
		$ord_id = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE carvari_order_id="'.$carvar_id.'"');
		if ($ord_id){
			return new Order($ord_id);
		} else {
			return false;
		}
	}

	public static function isExistByCarvariId( $carvar_id )
	{
		$ord_id = Db::getInstance()->getValue('SELECT id_order FROM '._DB_PREFIX_.'orders WHERE carvari_order_id="'.$carvar_id.'"');
		return (bool)$ord_id;
	}

}
