<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 09.12.16
 * Time: 17:29
 */
class Dispatcher extends DispatcherCore
{

    private function strictRedirects()
    {
        if($this->request_uri=='/trends')
            Tools::redirect(_PS_BASE_URL_.'/blog/',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");

        if($this->request_uri=='/sitemap')
            Tools::redirect('sitemap',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");

        if($this->request_uri=='/my-account')
            Tools::redirect('my-account',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");
        if($this->request_uri=='/authentication')
            Tools::redirect('authentication',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");
        if($this->request_uri=='/password-recovery') {
            Tools::redirect('password', __PS_BASE_URI__, null, "HTTP/1.1 301 Moved Permanently");
        }

    }

    public function dispatch() {
        $controller_class = '';
        $this->strictRedirects();
        // Get current controller
        $this->getController();
        if (!$this->controller) {
            $this->controller = $this->useDefaultController();
        }
        Hook::exec('actionControllerInited', ['controller' => new FrontController()]);

        if(substr($this->request_uri, -5) == '.html')
            Tools::redirect(_PS_BASE_URL_.$this->productsRedirect(),__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");

//        if($this->request_uri[strlen($this->request_uri) - 1] != '/' && !substr_count($this->request_uri, "admin")
//            && !substr_count($this->request_uri, "/?rand=")  && !substr_count($this->request_uri, "ajax.php")
//            && !substr_count($this->request_uri, "search") && !substr_count($this->request_uri, "module/loyalty"))
//            Tools::redirect(_PS_BASE_URL_.$_SERVER['REQUEST_URI']. '/',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");

        if(substr_count($this->request_uri, "/novini")==1) {
            $activeLangForUrl = Tools::getActiveLangForUrl();
            $explode = explode('/', $this->request_uri);
            $this->request_uri = '/blog/news/'.$explode[3];
            Tools::redirect(_PS_BASE_URL_ .$activeLangForUrl. $this->request_uri, __PS_BASE_URI__, null, "HTTP/1.1 301 Moved Permanently");
        }

        if(substr_count($this->request_uri, "/about")==1 && $this->controller == 'pagenotfound')
            Tools::redirect('index',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");

        if($this->request_uri!='/page-not-found/' && ($this->controller == 'pagenotfound' || $this->controller == '404'))
            Tools::redirect('pagenotfound',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");

        if($this->controller=='product' && !$this->productExist(Tools::getValue('id_product')))
            Tools::redirect('pagenotfound',__PS_BASE_URI__,null,"HTTP/1.1 301 Moved Permanently");


        // Dispatch with right front controller
        switch ($this->front_controller) {
            // Dispatch front office controller
            case self::FC_FRONT :
                $controllers = Dispatcher::getControllers(array(_PS_FRONT_CONTROLLER_DIR_, _PS_OVERRIDE_DIR_ . 'controllers/front/'));
                $controllers['index'] = 'IndexController';
                if (isset($controllers['auth'])) {
                    $controllers['authentication'] = $controllers['auth'];
                }
                if (isset($controllers['compare'])) {
                    $controllers['productscomparison'] = $controllers['compare'];
                }
                if (isset($controllers['contact'])) {
                    $controllers['contactform'] = $controllers['contact'];
                }

                if (!isset($controllers[strtolower($this->controller)])) {
                    $this->controller = $this->controller_not_found;
                }
                $controller_class = $controllers[strtolower($this->controller)];
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT, 'controller_class' => $controller_class, 'is_module' => 0);
                break;

            // Dispatch module controller for front office
            case self::FC_MODULE :
                $module_name = Validate::isModuleName(Tools::getValue('module')) ? Tools::getValue('module') : '';
                $module = Module::getInstanceByName($module_name);
                $controller_class = 'PageNotFoundController';
                if (Validate::isLoadedObject($module) && $module->active) {
                    $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_ . $module_name . '/controllers/front/');

                    if (isset($controllers[strtolower($this->controller)])) {
                        include_once(_PS_MODULE_DIR_ . $module_name . '/controllers/front/' . $this->controller . '.php');

                        if (file_exists(_PS_OVERRIDE_DIR_ . 'modules/' . $module_name . '/controllers/front/' . $this->controller . '.php')) {
                            include_once(_PS_OVERRIDE_DIR_ . 'modules/' . $module_name . '/controllers/front/' . $this->controller . '.php');
                            $controller_class = $module_name . $this->controller . 'ModuleFrontControllerOverride';
                        } else {
                            $controller_class = $module_name . $this->controller . 'ModuleFrontController';
                        }
                    }
                }
                $params_hook_action_dispatcher = array('controller_type' => self::FC_FRONT, 'controller_class' => $controller_class, 'is_module' => 1);
                break;

            // Dispatch back office controller + module back office controller
            case self::FC_ADMIN :
                if ($this->use_default_controller && !Tools::getValue('token') && Validate::isLoadedObject(Context::getContext()->employee) && Context::getContext()->employee->isLoggedBack()) {
                    Tools::redirectAdmin('index.php?controller=' . $this->controller . '&token=' . Tools::getAdminTokenLite($this->controller));
                }

                $tab = Tab::getInstanceFromClassName($this->controller, Configuration::get('PS_LANG_DEFAULT'));
                $retrocompatibility_admin_tab = null;

                if ($tab->module) {
                    if (file_exists(_PS_MODULE_DIR_ . $tab->module . '/' . $tab->class_name . '.php')) {
                        $retrocompatibility_admin_tab = _PS_MODULE_DIR_ . $tab->module . '/' . $tab->class_name . '.php';
                    } else {
                        $controllers = Dispatcher::getControllers(_PS_MODULE_DIR_ . $tab->module . '/controllers/admin/');
                        if (!isset($controllers[strtolower($this->controller)])) {
                            $this->controller = $this->controller_not_found;
                            $controller_class = 'AdminNotFoundController';
                        } else {
                            // Controllers in modules can be named AdminXXX.php or AdminXXXController.php
                            include_once(_PS_MODULE_DIR_ . $tab->module . '/controllers/admin/' . $controllers[strtolower($this->controller)] . '.php');
                            $controller_class = $controllers[strtolower($this->controller)] . (strpos($controllers[strtolower($this->controller)], 'Controller') ? '' : 'Controller');
                        }
                    }
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN, 'controller_class' => $controller_class, 'is_module' => 1);
                } else {
                    $controllers = Dispatcher::getControllers(array(_PS_ADMIN_DIR_ . '/tabs/', _PS_ADMIN_CONTROLLER_DIR_, _PS_OVERRIDE_DIR_ . 'controllers/admin/'));
                    if (!isset($controllers[strtolower($this->controller)])) {
                        // If this is a parent tab, load the first child
                        if (Validate::isLoadedObject($tab) && $tab->id_parent == 0 && ($tabs = Tab::getTabs(Context::getContext()->language->id, $tab->id)) && isset($tabs[0])) {
                            Tools::redirectAdmin(Context::getContext()->link->getAdminLink($tabs[0]['class_name']));
                        }
                        $this->controller = $this->controller_not_found;
                    }

                    $controller_class = $controllers[strtolower($this->controller)];
                    $params_hook_action_dispatcher = array('controller_type' => self::FC_ADMIN, 'controller_class' => $controller_class, 'is_module' => 0);

                    if (file_exists(_PS_ADMIN_DIR_ . '/tabs/' . $controller_class . '.php')) {
                        $retrocompatibility_admin_tab = _PS_ADMIN_DIR_ . '/tabs/' . $controller_class . '.php';
                    }
                }

                // @retrocompatibility with admin/tabs/ old system
                if ($retrocompatibility_admin_tab) {
                    include_once($retrocompatibility_admin_tab);
                    include_once(_PS_ADMIN_DIR_ . '/functions.php');
                    runAdminTab($this->controller, !empty($_REQUEST['ajaxMode']));
                    return;
                }
                break;

            default :
                throw new PrestaShopException('Bad front controller chosen');
        }

        // Instantiate controller
        try {
            // Loading controller
            $controller = Controller::getController($controller_class);

            // Execute hook dispatcher
            if (isset($params_hook_action_dispatcher)) {
                Hook::exec('actionDispatcher', $params_hook_action_dispatcher);
            }

            // Running controller
            $controller->run();
        } catch (PrestaShopException $e) {
            $e->displayMessage();
        }

//        if ($this->front_controller == self::FC_FRONT
//            && Module::isInstalled('ffredirects')
//            && Module::isEnabled('ffredirects')
//        ) {
//            require_once __DIR__.'/../../modules/ffredirects/models/FfRedirect.php';
//            $toUrl = FfRedirect::getToUrl(rtrim($_SERVER['REQUEST_URI'], '/'));
//            if ($toUrl) {
//                header("Location: {$toUrl}");
//                exit(0);
//            }
//        }
    }

    /**
     * Load default routes group by languages
     */
    protected function loadRoutes($id_shop = null)
    {
        $context = Context::getContext();

        // Load custom routes from modules
        $modules_routes = Hook::exec('moduleRoutes', array('id_shop' => $id_shop), null, true, false);
        if (is_array($modules_routes) && count($modules_routes)) {
            foreach ($modules_routes as $module_route) {
                if (is_array($module_route) && count($module_route)) {
                    foreach ($module_route as $route => $route_details) {
                        if (array_key_exists('controller', $route_details) && array_key_exists('rule', $route_details)
                            && array_key_exists('keywords', $route_details) && array_key_exists('params', $route_details)) {
                            if (!isset($this->default_routes[$route])) {
                                $this->default_routes[$route] = array();
                            }
                            $this->default_routes[$route] = array_merge($this->default_routes[$route], $route_details);
                        }
                    }
                }
            }
        }

        $language_ids = Language::getIDs();

        if (isset($context->language) && !in_array($context->language->id, $language_ids)) {
            $language_ids[] = (int)$context->language->id;
        }

        // Set default routes
        foreach ($language_ids as $id_lang) {
            foreach ($this->default_routes as $id => $route) {
                $this->addRoute(
                    $id,
                    $route['rule'],
                    $route['controller'],
                    $id_lang,
                    $route['keywords'],
                    isset($route['params']) ? $route['params'] : array(),
                    $id_shop
                );
            }
        }

        // Load the custom routes prior the defaults to avoid infinite loops
        if ($this->use_routes) {
            // Load routes from meta table
            $sql = 'SELECT m.page, ml.url_rewrite, ml.id_lang
					FROM `'._DB_PREFIX_.'meta` m
					LEFT JOIN `'._DB_PREFIX_.'meta_lang` ml ON (m.id_meta = ml.id_meta'.Shop::addSqlRestrictionOnLang('ml', $id_shop).')
					ORDER BY LENGTH(ml.url_rewrite) DESC';
            if ($results = Db::getInstance()->executeS($sql)) {
                foreach ($results as $row) {
                    if ($row['url_rewrite']) {
                        $this->addRoute($row['page'], $row['url_rewrite'].'/', $row['page'], $row['id_lang'], array(), array(), $id_shop);
                    }
                }
            }

            // Set default empty route if no empty route (that's weird I know)
            if (!$this->empty_route) {
                $this->empty_route = array(
                    'routeID' =>    'index',
                    'rule' =>        '',
                    'controller' =>    'index',
                );
            }

            // Load custom routes
            foreach ($this->default_routes as $route_id => $route_data) {
                if ($custom_route = Configuration::get('PS_ROUTE_'.$route_id, null, null, $id_shop)) {
                    if (isset($context->language) && !in_array($context->language->id, $language_ids)) {
                        $language_ids[] = (int)$context->language->id;
                    }

                    foreach ($language_ids as $id_lang) {
                        $this->addRoute(
                            $route_id,
                            $custom_route,
                            $route_data['controller'],
                            $id_lang,
                            $route_data['keywords'],
                            isset($route_data['params']) ? $route_data['params'] : array(),
                            $id_shop
                        );
                    }
                }
            }
        }
    }

    public function productsRedirect(){
        // delete '.html' from uri
        $this->request_uri = substr($this->request_uri,0, strlen($this->request_uri)-5);

        $this->request_uri = split('/',$this->request_uri);

        // {category:/} change to 'product'
        $this->request_uri[1] = 'product';

        //{rewrite}{-:ean13}-{id}
        $rewrite = split('-',$this->request_uri[2]);

        // delete {-:ean13}
        unset($rewrite[2]);

        //{rewrite}-{id}
        $rewrite = implode ('-',$rewrite);
        $this->request_uri[2] = $rewrite;

        // link product/{rewrite}-{id}
        $this->request_uri =  implode ('/',$this->request_uri);

        return $this->request_uri;
    }

    public function productExist($id)
    {
        $result = Db::getInstance()->getValue('
		SELECT p.`id_product` 
		FROM `'._DB_PREFIX_.'product` p
		WHERE p.`id_product` = '.$id.'
		AND p.`active` = 1');

        return $result;
    }
}