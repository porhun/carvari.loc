<?php
class FrontController extends FrontControllerCore {

    /**
     * Initializes common front page content: header, footer and side columns
     */
    public function initContent()
    {

        $this->process();

        if (!isset($this->context->cart)) {
            $this->context->cart = new Cart();
        }

        if (!$this->useMobileTheme()) {
            // These hooks aren't used for the mobile theme.
            // Needed hooks are called in the tpl files.
            $this->context->smarty->assign(array(
                'HOOK_HEADER'       => Hook::exec('displayHeader'),
                'HOOK_TOP'          => Hook::exec('displayTop'),
                'HOOK_LEFT_COLUMN'  => ($this->display_column_left  ? Hook::exec('displayLeftColumn') : ''),
                'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),
            ));
        } else {
            $this->context->smarty->assign(array(
                'HOOK_HEADER'       => Hook::exec('displayHeader'),
                'HOOK_TOP'          => Hook::exec('displayTop'),
                'HOOK_MOBILE_HEADER'  => Hook::exec('displayMobileHeader'),
            ));
        }
        $phones = explode(',', Configuration::get('PS_SHOP_PHONE'));
        $footerPhone = Configuration::get('BLOCKCONTACT_TELNUMBER');
        $this->context->smarty->assign(array('shop_phones'=> $phones, 'footerPhone' => $footerPhone));
    }

    public function setMobileMedia()
    {
        $this->addJquery();

        $this->addJqueryPlugin('formstyler');
        $this->addJqueryPlugin('mcustomscrollbar');
        $this->addjqueryPlugin('fancybox');

        if (!file_exists($this->getThemeDir().'js/autoload/')) {

            $this->addJS(_THEME_MOBILE_JS_DIR_.'jquery.mobile-1.3.0.min.js');
            $this->addJS(_THEME_MOBILE_JS_DIR_.'jqm-docs.js');
            $this->addJS(_PS_JS_DIR_.'tools.js');
            $this->addJS(_THEME_MOBILE_JS_DIR_.'global.js');
            $this->addJqueryPlugin('fancybox');
        }

        if (!file_exists($this->getThemeDir().'css/autoload/')) {
            $this->addCSS(_THEME_MOBILE_CSS_DIR_.'jquery.mobile-1.3.0.min.css', 'all');
            $this->addCSS(_THEME_MOBILE_CSS_DIR_.'jqm-docs.css', 'all');
            $this->addCSS(_THEME_MOBILE_CSS_DIR_.'global.css', 'all');
        }


        // Automatically add js files from js/autoload directory in the template
        if (@filemtime($this->getThemeDir().'js/autoload/'))
            foreach (scandir($this->getThemeDir().'js/autoload/', 0) as $file)
                if (preg_match('/^[^.].*\.js$/', $file))
                    $this->addJS($this->getThemeDir().'js/autoload/'.$file);
        // Automatically add css files from css/autoload directory in the template
        if (@filemtime($this->getThemeDir().'css/autoload/'))
            foreach (scandir($this->getThemeDir().'css/autoload', 0) as $file)
                if (preg_match('/^[^.].*\.css$/', $file))
                    $this->addCSS($this->getThemeDir().'css/autoload/'.$file);

	    $this->addJS(_THEME_MOBILE_JS_DIR_.'jquery.mask_plugin.min.js');
        $this->addCSS(_THEME_CSS_DIR_.'fonts/fonts.css', 'all');
        $this->addJS(_THEME_JS_DIR_.'my_scripts.js');
        $this->addJS(_THEME_MOBILE_JS_DIR_.'main.js');

    }

    public function setMedia(){

        /**
         * If website is accessed by mobile device
         * @see FrontControllerCore::setMobileMedia()
         */
        if ($this->useMobileTheme())
        {
            $this->setMobileMedia();
            return true;
        }

        $this->addCSS(_THEME_CSS_DIR_.'grid_prestashop.css', 'all');  // retro compat themes 1.5.0.1
        $this->addCSS(_THEME_CSS_DIR_.'fonts/fonts.css', 'all');
//		$this->addCSS(_THEME_CSS_DIR_.'global.css', 'all');
        $this->addJquery();
        $this->addJqueryPlugin('easing');
        $this->addJS(_PS_JS_DIR_.'tools.js');
	    $this->addJS(_THEME_JS_DIR_.'jquery.mask_plugin.min.js');
        $this->addJS(_THEME_JS_DIR_.'global.js');
        $this->addJS(_THEME_JS_DIR_.'main.js');
        $this->addJS(_THEME_JS_DIR_.'my_scripts.js');

        // Automatically add js files from js/autoload directory in the template
        if (@filemtime($this->getThemeDir().'js/autoload/'))
            foreach (scandir($this->getThemeDir().'js/autoload/', 0) as $file)
                if (preg_match('/^[^.].*\.js$/', $file))
                    $this->addJS($this->getThemeDir().'js/autoload/'.$file);
        // Automatically add css files from css/autoload directory in the template
        if (@filemtime($this->getThemeDir().'css/autoload/'))
            foreach (scandir($this->getThemeDir().'css/autoload', 0) as $file)
                if (preg_match('/^[^.].*\.css$/', $file))
                    $this->addCSS($this->getThemeDir().'css/autoload/'.$file);

        if (Tools::isSubmit('live_edit') && Tools::getValue('ad') && Tools::getAdminToken('AdminModulesPositions'.(int)Tab::getIdFromClassName('AdminModulesPositions').(int)Tools::getValue('id_employee')))
        {
            $this->addJqueryUI('ui.sortable');
            $this->addjqueryPlugin('fancybox');
            $this->addJS(_PS_JS_DIR_.'hookLiveEdit.js');
        }

        if (Configuration::get('PS_QUICK_VIEW'))
            $this->addjqueryPlugin('fancybox');

        if (Configuration::get('PS_COMPARATOR_MAX_ITEM') > 0)
            $this->addJS(_THEME_JS_DIR_.'products-comparison.js');

        // Carvari additions


        $this->addJqueryPlugin('formstyler');
        $this->addJqueryPlugin('mcustomscrollbar');



        // Execute Hook FrontController SetMedia
        Hook::exec('actionFrontControllerSetMedia', array());

        return true;
    }

    public function init()
    {
   //     $this->setEmployee();
 //       $this->restrictCustomerAccessToLanguage('ru');
        parent::init();
        $activeLangForUrl = Tools::getActiveLangForUrl();
        $protocol_link = (Configuration::get('PS_SSL_ENABLED') || Tools::usingSecureMode()) ? 'https://' : 'http://';
        $this->context->smarty->assign([
            'base_dir'            => _PS_BASE_URL_.$activeLangForUrl.__PS_BASE_URI__, '/',
            'base_dir_ssl'        => $protocol_link.Tools::getShopDomainSsl().$activeLangForUrl.__PS_BASE_URI__,
        ]);
        /** Set current active lang url for front pages: if it's default language, show nothing. Otherwise, show current language */
        $this->context->smarty->assign('active_url_lang', $activeLangForUrl, true);
    }

    private function setEmployee()
    {
        $cookie = new Cookie('psAdmin', '', (int)Configuration::get('PS_COOKIE_LIFETIME_BO'));
        $employee = new Employee((int)$cookie->id_employee);
//        ddd($this->context->getAdminStatus());
        if (!$this->context->getAdminStatus() && Validate::isLoadedObject($employee) && $employee->checkPassword((int)$cookie->id_employee, $cookie->passwd)
            && (!isset($cookie->remote_addr) || $cookie->remote_addr == ip2long(Tools::getRemoteAddr()) || !Configuration::get('PS_COOKIE_CHECKIP')))
        {
            $this->context->setAdminStatus(true);
        }
        $this->context->smarty->assign('is_admin_logged', (bool)$this->context->getAdminStatus(), true);
    }

    private function restrictCustomerAccessToLanguage($isoLanguage)
    {
        $request_lang = Tools::getValue('isolang', '');
        if (!$request_lang || !$isoLanguage || !Validate::isLanguageCode($request_lang) || !Validate::isLanguageCode($isoLanguage)) {
            return false;
        }
        if ($request_lang === $isoLanguage && !$this->context->getAdminStatus()) {
            $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
            header('Location: '.sprintf('/%s/', Language::getIsoById($default_lang)));
        }
    }

}
