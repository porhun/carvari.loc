<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author 	PrestaShop SA <contact@prestashop.com>
 *  @copyright  2007-2016 PrestaShop SA
 *  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class AdminController extends AdminControllerCore
{
    /**
     * Assign smarty variables for the header
     */
    public function initHeader()
    {
        header('Cache-Control: no-store, no-cache');

        // Multishop
        $is_multishop = Shop::isFeatureActive();

        // Quick access
        if ((int)$this->context->employee->id) {
            $quick_access = QuickAccess::getQuickAccesses($this->context->language->id);
            foreach ($quick_access as $index => $quick) {
                if ($quick['link'] == '../' && Shop::getContext() == Shop::CONTEXT_SHOP) {
                    $url = $this->context->shop->getBaseURL();
                    if (!$url) {
                        unset($quick_access[$index]);
                        continue;
                    }
                    $quick_access[$index]['link'] = $url;
                } else {
                    preg_match('/controller=(.+)(&.+)?$/', $quick['link'], $admin_tab);
                    if (isset($admin_tab[1])) {
                        if (strpos($admin_tab[1], '&')) {
                            $admin_tab[1] = substr($admin_tab[1], 0, strpos($admin_tab[1], '&'));
                        }

                        $token = Tools::getAdminToken($admin_tab[1].(int)Tab::getIdFromClassName($admin_tab[1]).(int)$this->context->employee->id);
                        $quick_access[$index]['target'] = $admin_tab[1];
                        $quick_access[$index]['link'] .= '&token='.$token;
                    }
                }
            }
        }

        // Tab list
        $tabs = Tab::getTabs($this->context->language->id, 0);
        $current_id = Tab::getCurrentParentId();
        foreach ($tabs as $index => $tab) {
            if (!Tab::checkTabRights($tab['id_tab'])
                || ($tab['class_name'] == 'AdminStock' && Configuration::get('PS_ADVANCED_STOCK_MANAGEMENT') == 0)
                || $tab['class_name'] == 'AdminCarrierWizard') {
                unset($tabs[$index]);
                continue;
            }

            $img_cache_url = 'themes/'.$this->context->employee->bo_theme.'/img/t/'.$tab['class_name'].'.png';
            $img_exists_cache = Tools::file_exists_cache(_PS_ADMIN_DIR_.$img_cache_url);
            // retrocompatibility : change png to gif if icon not exists
            if (!$img_exists_cache) {
                $img_exists_cache = Tools::file_exists_cache(_PS_ADMIN_DIR_.str_replace('.png', '.gif', $img_cache_url));
            }

            if ($img_exists_cache) {
                $path_img = $img = $img_exists_cache;
            } else {
                $path_img = _PS_IMG_DIR_.'t/'.$tab['class_name'].'.png';
                // Relative link will always work, whatever the base uri set in the admin
                $img = '../img/t/'.$tab['class_name'].'.png';
            }

            if (trim($tab['module']) != '') {
                $path_img = _PS_MODULE_DIR_.$tab['module'].'/'.$tab['class_name'].'.png';
                // Relative link will always work, whatever the base uri set in the admin
                $img = '../modules/'.$tab['module'].'/'.$tab['class_name'].'.png';
            }

            // retrocompatibility
            if (!file_exists($path_img)) {
                $img = str_replace('png', 'gif', $img);
            }
            // tab[class_name] does not contains the "Controller" suffix
            $tabs[$index]['current'] = ($tab['class_name'].'Controller' == get_class($this)) || ($current_id == $tab['id_tab']);
            $tabs[$index]['img'] = $img;
            $tabs[$index]['href'] = $this->context->link->getAdminLink($tab['class_name']);

            $sub_tabs = Tab::getTabs($this->context->language->id, $tab['id_tab']);
            foreach ($sub_tabs as $index2 => $sub_tab) {
                //check if module is enable and
                if (isset($sub_tab['module']) && !empty($sub_tab['module'])) {
                    $module = Module::getInstanceByName($sub_tab['module']);
                    if (is_object($module) && !$module->isEnabledForShopContext()) {
                        unset($sub_tabs[$index2]);
                        continue;
                    }
                }

                // class_name is the name of the class controller
                if (Tab::checkTabRights($sub_tab['id_tab']) === true && (bool)$sub_tab['active'] && $sub_tab['class_name'] != 'AdminCarrierWizard') {
                    $sub_tabs[$index2]['href'] = $this->context->link->getAdminLink($sub_tab['class_name']);
                    $sub_tabs[$index2]['current'] = ($sub_tab['class_name'].'Controller' == get_class($this) || $sub_tab['class_name'] == Tools::getValue('controller'));
                } elseif ($sub_tab['class_name'] == 'AdminCarrierWizard' && $sub_tab['class_name'].'Controller' == get_class($this)) {
                    foreach ($sub_tabs as $i => $tab) {
                        if ($tab['class_name'] == 'AdminCarriers') {
                            break;
                        }
                    }

                    $sub_tabs[$i]['current'] = true;
                    unset($sub_tabs[$index2]);
                } else {
                    unset($sub_tabs[$index2]);
                }
            }
            $tabs[$index]['sub_tabs'] = array_values($sub_tabs);
        }

        if (Validate::isLoadedObject($this->context->employee)) {
            $accesses = Profile::getProfileAccesses($this->context->employee->id_profile, 'class_name');
            $helperShop = new HelperShop();
            /* Hooks are voluntary out the initialize array (need those variables already assigned) */
            $bo_color = empty($this->context->employee->bo_color) ? '#FFFFFF' : $this->context->employee->bo_color;
            $this->context->smarty->assign(array(
                'autorefresh_notifications' => Configuration::get('PS_ADMINREFRESH_NOTIFICATION'),
                'help_box' => Configuration::get('PS_HELPBOX'),
                'round_mode' => Configuration::get('PS_PRICE_ROUND_MODE'),
                'brightness' => Tools::getBrightness($bo_color) < 128 ? 'white' : '#383838',
                'bo_width' => (int)$this->context->employee->bo_width,
                'bo_color' => isset($this->context->employee->bo_color) ? Tools::htmlentitiesUTF8($this->context->employee->bo_color) : null,
                'show_new_orders' => Configuration::get('PS_SHOW_NEW_ORDERS') && isset($accesses['AdminOrders']) && $accesses['AdminOrders']['view'],
                'show_new_customers' => Configuration::get('PS_SHOW_NEW_CUSTOMERS') && isset($accesses['AdminCustomers']) && $accesses['AdminCustomers']['view'],
                'show_new_messages' => Configuration::get('PS_SHOW_NEW_MESSAGES') && isset($accesses['AdminCustomerThreads']) && $accesses['AdminCustomerThreads']['view'],
                'employee' => $this->context->employee,
                'search_type' => Tools::getValue('bo_search_type'),
                'bo_query' => Tools::safeOutput(Tools::stripslashes(Tools::getValue('bo_query'))),
                'quick_access' => $quick_access,
                'multi_shop' => Shop::isFeatureActive(),
                'shop_list' => $helperShop->getRenderedShopList(),
                'shop' => $this->context->shop,
                'shop_group' => new ShopGroup((int)Shop::getContextShopGroupID()),
                'is_multishop' => $is_multishop,
                'multishop_context' => $this->multishop_context,
                'default_tab_link' => $this->context->link->getAdminLink(Tab::getClassNameById((int)Context::getContext()->employee->default_tab)),
                'login_link' => $this->context->link->getAdminLink('AdminLogin'),
                'collapse_menu' => isset($this->context->cookie->collapse_menu) ? (int)$this->context->cookie->collapse_menu : 0,
            ));
        } else {
            $this->context->smarty->assign('default_tab_link', $this->context->link->getAdminLink('AdminDashboard'));
        }

        // Shop::initialize() in config.php may empty $this->context->shop->virtual_uri so using a new shop instance for getBaseUrl()
        $this->context->shop = new Shop((int)$this->context->shop->id);

        $this->context->smarty->assign(array(
            'img_dir' => _PS_IMG_,
            'iso' => $this->context->language->iso_code,
            'class_name' => $this->className,
            'iso_user' => $this->context->language->iso_code,
            'country_iso_code' => $this->context->country->iso_code,
            'version' => _PS_VERSION_,
            'lang_iso' => $this->context->language->iso_code,
            'full_language_code' => $this->context->language->language_code,
            'link' => $this->context->link,
            'shop_name' => Configuration::get('PS_SHOP_NAME'),
            'base_url' => $this->context->shop->getBaseURL(),
            'tab' => isset($tab) ? $tab : null, // Deprecated, this tab is declared in the foreach, so it's the last tab in the foreach
            'current_parent_id' => (int)Tab::getCurrentParentId(),
            'tabs' => $tabs,
            'install_dir_exists' => file_exists(_PS_ADMIN_DIR_.'/../install'),
            'pic_dir' => _THEME_PROD_PIC_DIR_,
            'controller_name' => htmlentities(Tools::getValue('controller')),
            'currentIndex' => self::$currentIndex,
            'bootstrap' => $this->bootstrap,
            'default_language' => (int)Configuration::get('PS_LANG_DEFAULT'),
            'display_addons_connection' => Tab::checkTabRights(Tab::getIdFromClassName('AdminModulesController'))
        ));

//        $module = Module::getInstanceByName('themeconfigurator');
//        if (is_object($module) && $module->active && (int)Configuration::get('PS_TC_ACTIVE') == 1 && $this->context->shop->getBaseURL()) {
//            $request =
//            'live_configurator_token='.$module->getLiveConfiguratorToken()
//            .'&id_employee='.(int)$this->context->employee->id
//            .'&id_shop='.(int)$this->context->shop->id
//            .(Configuration::get('PS_TC_THEME') != '' ? '&theme='.Configuration::get('PS_TC_THEME') : '')
//            .(Configuration::get('PS_TC_FONT') != '' ? '&theme_font='.Configuration::get('PS_TC_FONT') : '');
//            $this->context->smarty->assign('base_url_tc', $this->context->link->getPageLink('index', null, $id_lang = null, $request));
//        }
    }

}
