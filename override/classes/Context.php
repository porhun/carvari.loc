<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 16.12.16
 * Time: 17:56
 */
class Context extends ContextCore
{
    private $is_admin_logged = false;

    public function setAdminStatus($is_logged)
    {
        $this->is_admin_logged = (bool)$is_logged;
    }

    public function getAdminStatus()
    {
        return (bool)$this->is_admin_logged;
    }
}