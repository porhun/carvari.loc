<?php
class Media extends MediaCore {
    /**
     * return jquery plugin css path if exist.
     *
     * @param mixed $name
     *
     * @return string|boolean
     */
    public static function getJqueryPluginCSSPath($name, $folder = null)
    {
        global $context;
        if ($folder === null) {
            $folder = _PS_JS_DIR_.'jquery/plugins/';
        } //set default folder
        if( $context->getMobileDevice() && @file_exists(_PS_ROOT_DIR_.$folder.$name.'/jquery.'.$name.'.mobile.css' )  ) {
            $file = 'jquery.'.$name.'.mobile.css';
        } else
        {
            $file = 'jquery.'.$name.'.css';
        }
        $url_data = parse_url($folder);
        $file_uri = _PS_ROOT_DIR_.Tools::str_replace_once(__PS_BASE_URI__, DIRECTORY_SEPARATOR, $url_data['path']);
        $file_uri_host_mode = _PS_CORE_DIR_.Tools::str_replace_once(__PS_BASE_URI__, DIRECTORY_SEPARATOR, $url_data['path']);

        if (@file_exists($file_uri.$file) || (defined('_PS_HOST_MODE_') && @file_exists($file_uri_host_mode.$file))) {
            return Media::getCSSPath($folder.$file);
        } elseif (@file_exists($file_uri.$name.'/'.$file) || (defined('_PS_HOST_MODE_') && @file_exists($file_uri_host_mode.$name.'/'.$file))) {
            return Media::getCSSPath($folder.$name.'/'.$file);
        } else {
            return false;
        }
    }
}