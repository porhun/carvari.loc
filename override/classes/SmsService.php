<?php

class SmsService {

    private $_login;

    private $_password;

    private $_sender;

    private $_tpl;

    private $_server_url = 'http://turbosms.in.ua/api/wsdl.html';

    private $_client = null;

    public function __construct() {

        $this->_login = Configuration::get('smsinfo_login');
        $this->_password = Configuration::get('smsinfo_password');
        $this->_sender = Configuration::get('smsinfo_sender');
        $this->_tpl = Configuration::get('smsinfo_order_tpl');

        $this->_client = new SoapClient ($this->_server_url);
        $this->_connect();
    }

    private function _connect() {
        header ('Content-type: text/html; charset=utf-8');

        $this->_client->Auth(
            Array (
                'login' => $this->_login,
                'password' => $this->_password
            )
        );
    }

    public function sendSMS( $message, $phone ) {
        $charset = mb_detect_encoding( $message, array('UTF-8', 'Windows-1251') );
        if( $charset=='Windows-1251' ) {
            $message = iconv('windows-1251', 'UTF-8', $message);
        }
        $sms = Array (
            'sender' => $this->_sender,
            'destination' => $phone,
            'text' => $message
        );

        $result = $this->_client->SendSMS( $sms );
        Logger::AddLog('SMS ' .$result->SendSMSResult->ResultArray . $result->SendSMSResult->ResultArray[0]);
        if (trim($result->SendSMSResult->ResultArray[0]) == 'Сообщения успешно отправлены')
            return true;
        else
            return false;
    }
}
