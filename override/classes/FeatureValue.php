<?php

class FeatureValue extends FeatureValueCore {

    public static function getFeatureValuesWithLangActive($id_lang, $id_feature, $custom = false)
    {
        return Db::getInstance()->executeS('
			SELECT *
			FROM `'._DB_PREFIX_.'feature_value` v
			LEFT JOIN `'._DB_PREFIX_.'feature_value_lang` vl
				ON (v.`id_feature_value` = vl.`id_feature_value` AND vl.`id_lang` = '.(int)$id_lang.')
			WHERE v.`id_feature` = '.(int)$id_feature.'
				'.(!$custom ? 'AND (v.`custom` IS NULL OR v.`custom` = 0)' : '').'
				AND (v.`id_feature_value` IN (SELECT fp.`id_feature_value` FROM `'._DB_PREFIX_.'feature_product` fp INNER JOIN `'._DB_PREFIX_.'product_shop` ps ON (ps.`id_product` = fp.`id_product` AND ps.`active`=1) INNER JOIN `'._DB_PREFIX_.'stock_available` sa ON (sa.`id_product` = fp.`id_product` AND sa.`id_product_attribute`=0 AND sa.`quantity`>0)  GROUP BY fp.`id_feature_value`))
			ORDER BY vl.`value` ASC
		');
    }

    public static function addFeatureValueImport($id_feature, $value, $id_product = null, $id_lang = null, $custom = false, $feature_value_multilang = null)
    {
        $id_feature_value = false;
        if (!is_null($id_product) && $id_product) {
            $id_feature_value = Db::getInstance()->getValue('
				SELECT fp.`id_feature_value`
				FROM '._DB_PREFIX_.'feature_product fp
				INNER JOIN '._DB_PREFIX_.'feature_value fv USING (`id_feature_value`)
				WHERE fp.`id_feature` = '.(int)$id_feature.'
				AND fv.`custom` = '.(int)$custom.'
				AND fp.`id_product` = '.(int)$id_product);

            if ($custom && $id_feature_value && !is_null($id_lang) && $id_lang) {
                Db::getInstance()->execute('
				UPDATE '._DB_PREFIX_.'feature_value_lang
				SET `value` = \''.pSQL($value).'\'
				WHERE `id_feature_value` = '.(int)$id_feature_value.'
				AND `value` != \''.pSQL($value).'\'
				AND `id_lang` = '.(int)$id_lang);
            }
        }

        if (!$custom) {
            $id_feature_value = Db::getInstance()->getValue('
				SELECT fv.`id_feature_value`
				FROM '._DB_PREFIX_.'feature_value fv
				LEFT JOIN '._DB_PREFIX_.'feature_value_lang fvl ON (fvl.`id_feature_value` = fv.`id_feature_value` AND fvl.`id_lang` = '.(int)$id_lang.')
				WHERE `value` = \''.pSQL($value).'\'
				AND fv.`id_feature` = '.(int)$id_feature.'
				AND fv.`custom` = 0
				GROUP BY fv.`id_feature_value`');
        }

        if ($id_feature_value) {
            return (int)$id_feature_value;
        }

        // Feature doesn't exist, create it
        $feature_value = new FeatureValue();
        $feature_value->id_feature = (int)$id_feature;
        $feature_value->custom = (bool)$custom;
        $feature_value->value = array_fill_keys(Language::getIDs(false), $value);
        $feature_value->add();

        return (int)$feature_value->id;
    }

}