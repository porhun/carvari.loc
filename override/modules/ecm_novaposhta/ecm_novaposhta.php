<?php
//НоваПошта
define ("_NP_ADMIN_", "adminnp");
if (!defined('_NP_TIMEOUT_')) define ("_NP_TIMEOUT_", 2);
if (!defined('_PS_VERSION_'))
	exit;

require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/json.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/load_files.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/api2.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/exec.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/exec_.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/areaRu.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/split.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/klogger.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/classes/npmail.php');
require_once(_PS_MODULE_DIR_ . 'ecm_novaposhta/upgrade.php');

$log = new klogger("log.txt", klogger::DEBUG);

class ecm_novaposhtaOverride extends ecm_novaposhta  {
	const PREFIX = 'ecm_';
	private $_last_updated = '';



	private
	function _displayabout() {
		$this->_html .= '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
        <fieldset class="space">
        <legend><img src="../img/admin/email.gif" /> ' . $this->l('Information') . '</legend>
        <div id="dev_div">
        <span><b>' . $this->l('Version') . ':</b> ' . $this->version . '</span><br>
        <span><b>' . $this->l('Developer') . ':</b> <a class="link" href="mailto:admin@elcommerce.com.ua" target="_blank">Mice  </a>
        <span><b>' . $this->l('Decription') . ':</b> <a class="link" href="http://elcommerce.com.ua" target="_blank">ElCommerce.com.ua</a><br>
        <span><b>' . $this->l('Licension key') . ':</b> ' . $this->l(Configuration::get(self::PREFIX . 'np_LIC_KEY')) . '</span><br><br>
        <p style="text-align:center"><a href="http://elcommerce.com.ua/"><img src="http://elcommerce.com.ua/img/m/logo.png" alt="Электронный учет коммерческой деятельности" /></a>
        </div>
        </fieldset>
        ';

	}
	private
	function _cron() {
		$secureKey = md5(_COOKIE_KEY_.Configuration::get('PS_SHOP_NAME'));
		$domain = Tools::getHttpHost(true);
		$cron_url = $domain.__PS_BASE_URI__.'modules/ecm_novaposhta/cron.php?secure_key='.$secureKey;
		$class    = version_compare(_PS_VERSION_, '1.6', '>=') ? 'alert alert-info' : 'description';
		$this->_html .= '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
		<fieldset class="product-tab-content" style="clear: both;margin-top: 15px;">
		<p class="'.$class.'">
		'.$this->l('Добавьте этот адрес в планировщик cron ').': <a href="'.$cron_url.'">'.$cron_url.'</a><br/>
		'.$this->l('для получения списка городов и отделений Новой Почты по расписанию.').'</p>

		';
	}

	private
	function _displayupgrade() {
		$this->_html .= '<form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
        <fieldset class="space">
        <legend><img src="../img/admin/module_warning.png" /> ' . $this->l('Upgrade') . '</legend>
        <div id="dev_div">
        <span style="color:red"><b>' . $this->l('New version is available!!! Please, click button "Upgrade" to install it!') . ':</b></span>
        <center><hr>
        <input class="button" type="submit" name="submitUPGR" value="' . $this->l('Upgrade') . '" />
        </center>
        </div>
        </fieldset>
        ';
	}

	private
	function _settings() {
		global $cookie;
		if (!function_exists('curl_version')){
			$this->_html .= '
                <div class="bootstrap">
                <div class="alert alert-danger">
                <btn btn-default button type="btn btn-default button" class="close" data-dismiss="alert">×</btn btn-default button>
                Не установлено расширение <b>curl</b>, Работа модуля невозможна !!!
                </div>
                </div>
                ';
			return;
		}
		if (!Configuration::get(self::PREFIX . 'np_LIC_KEY')) {
			$this->_html .= '<fieldset class="space">
            <legend><img src="../img/admin/cog.gif" alt="" class="middle" />' . $this->l('Settings') . '</legend>
            <div class="col-xs-12">
            <label>' . $this->l('Licension key') . '</label>
            <div class="margin-form">
            <input type="text" name="LIC_KEY" placeholder="' . $this->l('Licension key') . '" required
            value=""/>
            <p class="clear">' . $this->l('Enter your Licension key ') . '</p>
            </div>
            <center><hr>
            <input class="button" type="submit" name="submitLIC" value="' . $this->l('Save') . '" />
            </center>
            ';
			return;
		}
		$by_order = Configuration::get(self::PREFIX . 'np_by_order') ? 'checked="checked" ' : '';

		if (!Configuration::get(self::PREFIX . 'np_API_KEY')) {
			$this->_html .= '<fieldset class="space">
            <legend><img src="../img/admin/cog.gif" alt="" class="middle" />' . $this->l('Settings') . '</legend>
            <input id="customer" name="customer" type="hidden" value ="-{$cookie->id_employee}"/>
            <input id="employee" name="employee" type="hidden" value ="-{$cookie->id_employee}"/>
            <input type="hidden" name="TrimMsg" value="101"/>
            <input type="hidden" name="comiso" value="2"/>
            <input type="hidden" name="insurance" value="0"/>
            <input type="hidden" name="ServiceType" value="WarehouseWarehouse"/>
            <div class="col-xs-12">
            <label>' . $this->l('API key') . '</label>
            <div class="margin-form">
            <input type="text" name="API_KEY" placeholder="' . $this->l('API key') . '" required
            value=""/>
            <p class="clear">' . $this->l('Enter your API key (in the particular office at site "Nova poshta")') . '</p>
            </div>
            <label>' . $this->l('By oder') . '</label>
            <div class="margin-form">
            <input type="checkbox" name="by_order"  value="1" '.$by_order.' />
            <p class="clear">' . $this->l('Check if you work by "Order", else "Loyality programm"') . '</p>
            </div>
            <center><hr>
            <input class="button" type="submit" name="submitAPI" value="' . $this->l('Save') . '" />
            </center>
            ';
			return;
		}


		if ($errors = exec::getLists()) {
			$errorlist = '';
			foreach ($errors as $error)
				$errorlist .= '<li>' . $error . '</li>';
			$this->_html .= '
            <div class="bootstrap">
            <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <h4>' . $this->l('Errors detect" !!!') . '</h4>
            <ul class="list-unstyled">' . $errorlist . '</ul>
            </div>
            </div>
            ';
		}
		$countwh = Db::getInstance()->GetValue("SELECT COUNT(`ref`) FROM `" . _DB_PREFIX_ . "ecm_newpost_warehouse`");
		$this->context->smarty->assign(array(
			'out_company' => Configuration::get(self::PREFIX . 'np_out_company'),
			'out_name' => Configuration::get(self::PREFIX . 'np_out_name'),
			'out_phone' => Configuration::get(self::PREFIX . 'np_out_phone'),
			'out_email' => Configuration::get(self::PREFIX . 'np_out_email'),
			'API_KEY' => Configuration::get(self::PREFIX . 'np_API_KEY'),
			'LIC_KEY' => Configuration::get(self::PREFIX . 'np_LIC_KEY'),
			'countwh' => $countwh,
			'percentage' => Configuration::get(self::PREFIX . 'np_percentage'),
			'comiso' => Configuration::get(self::PREFIX . 'np_comiso'),
			'modulesdir' => _MODULE_DIR_ . $this->name,
			'counterparty' => exec::getout('counterparty', -$cookie->id_employee),
			'payment_form' => Configuration::get(self::PREFIX . 'np_payment_method'),
			'payment_forms' => Exec::simpleList('PaymentForms'),
			'Ownership' => Configuration::get(self::PREFIX . 'np_Ownership'),
			'OwnershipFormsList' => Exec::simpleList('OwnershipFormsList', 'FullName'),
			'ServiceType' => Configuration::get(self::PREFIX . 'np_ServiceType'),
			'ServiceTypes' => Exec::simpleList('ServiceTypes'),
			'CargoType' => Configuration::get(self::PREFIX . 'np_CargoType'),
			'CargoTypes' => Exec::simpleList('CargoTypes'),
			'description' => Configuration::get(self::PREFIX . 'np_description'),
			'pack' => Configuration::get(self::PREFIX . 'np_pack'),
			'insurance' => Configuration::get(self::PREFIX . 'np_insurance'),
			'time' => Configuration::get(self::PREFIX . 'np_time'),
			'format' => Configuration::get(self::PREFIX . 'np_format'),
			'formats' => array(
				'html' => 'HTML',
				'pdf' => 'PDF'
			),
			'InfoRegClientBarcode' => Configuration::get(self::PREFIX . 'np_InfoRegClientBarcode'),
			'InfoRegClientBarcodes' => array(
				'id_order' => $this->l('Id order'),
				'reference' => $this->l('Reference'),
				'' => $this->l('None')
			),
			'redelivery' => (Configuration::get(self::PREFIX . 'np_redelivery') ? 'checked="checked" ' : ''),
			'senderpay' => (Configuration::get(self::PREFIX . 'np_senderpay') ? 'checked="checked" ' : ''),
			'senderpay_redelivery' => (Configuration::get(self::PREFIX . 'np_senderpay_redelivery') ? 'checked="checked" ' : ''),
			'AfterpaymentOnGoods' => (Configuration::get(self::PREFIX . 'np_AfterpaymentOnGoods') ? 'checked="checked" ' : ''),
			'add_msg' => (Configuration::get(self::PREFIX . 'np_add_msg') ? 'checked="checked" ' : ''),
			'fill' => (Configuration::get(self::PREFIX . 'np_fill') ? 'checked="checked" ' : ''),
			'show' => (Configuration::get(self::PREFIX . 'np_show') ? 'checked="checked" ' : ''),
			'ac' => (Configuration::get(self::PREFIX . 'np_ac') ? 'checked="checked" ' : ''),
			'SendNPmail' => (Configuration::get(self::PREFIX . 'np_SendNPmail') ? 'checked="checked" ' : ''),
			'SendNPadminmail' => Configuration::get(self::PREFIX . 'np_SendNPadminmail'),
			'weght' => Configuration::get(self::PREFIX . 'np_weght'),
			'vweght' => Configuration::get(self::PREFIX . 'np_vweght'),
			'fixcost' => Configuration::get(self::PREFIX . 'np_fixcost'),
			'chk_fixcost' => (Configuration::get(self::PREFIX . 'np_chk_fixcost') ? 'checked="checked" ' : ''),
			'by_order' => $by_order,
			'addtoorder' => (Configuration::get(self::PREFIX . 'np_addtoorder') ? 'checked="checked" ' : ''),
			'Area' => exec::getout('area',-$cookie->id_employee),
			'City' => exec::getout('city',-$cookie->id_employee),
			'Ware' => exec::getout('ref',-$cookie->id_employee),
			'Areas' => exec::areaList2(),
			'Citys' => exec::cityList2(exec::getout('area',-$cookie->id_employee)),
			'Wares' => exec::wareList2(exec::getout('city',-$cookie->id_employee)),
			'BlockedWarehouse' =>  json_decode(Configuration::get(self::PREFIX . 'np_BlockedWarehouse')),
			'WarehouseTypes' =>  Exec::simpleList('WarehouseTypes'),
			'FreeLimit' => Configuration::get(self::PREFIX . 'np_FreeLimit'),
			'TrimMsg' => Configuration::get(self::PREFIX . 'np_TrimMsg')

		));
		$this->_html .= $this->display(__FILE__, 'views/settings.tpl');

	}
	public
	function hookdisplayAdminOrder($params) {
		global $cookie;
		//p($params['cart']);
		//$id_order = $params['id_order'];
		$order_details = exec::GetOrderDetails($params['id_order']);
        $order_details['phone'] = str_replace('+','',$order_details['phone']);
		$this->context->smarty->assign(array(
			'np_id' => Configuration::get(self::PREFIX . $this->name),
			'id_order' => $params['id_order'],
			'order_details' => $order_details,
			'address' => exec::GetAddresDelivery($order_details['id_address_delivery'], $params['id_order']),
			'api_key' => Configuration::get('ecm_np_API_KEY'),
			'weght' => Configuration::get(self::PREFIX . 'np_weght'),
			'vweght' => Configuration::get(self::PREFIX . 'np_vweght'),
			'format' => Configuration::get(self::PREFIX . 'np_format'),
			//'modulesdir' => _MODULE_DIR_ . $this->name,
			'Areas' => exec::areaList2(),
			'Citys' => exec::cityList2($order_details['area']),
			'Wares' => exec::wareList2($order_details['city']),
			'outWares' => exec::wareList2(exec::getout('city',-$cookie->id_employee)),
			'ware' => exec::getwarename($order_details['ware']),
			'outware' => exec::getout('ref',-$cookie->id_employee),
			'ServiceType' => Configuration::get(self::PREFIX . 'np_ServiceType'),
			'ServiceTypes' => Exec::simpleList('ServiceTypes'),
			'CargoType' => Configuration::get(self::PREFIX . 'np_CargoType'),
			'CargoTypes' => Exec::simpleList('CargoTypes'),
			'data' => (date("H:i") >= Configuration::get(self::PREFIX.'np_time'))? date("d.m.Y",(time()+3600*24)) : date("d.m.Y"),
			'TrimMsg' => Configuration::get(self::PREFIX . 'np_TrimMsg')
		));
		return $this->display(__FILE__, 'displayAdminOrder.tpl');
	}
	private
	function upgradeCheck($module) {

		global $cookie;

		$context = Context::getContext();
		if (!isset($context->employee) || !$context->employee->isLoggedBack())
			return;

		// Get module version info
		$mod_info = $this->version;
		$time = time();
		if ($this->_last_updated <= 0) {
			$time_up = Configuration::get('ECM_NP_UP');
			if (!Configuration::get('ECM_NP_UP')) {
				Configuration::updateValue('ECM_NP_UP', $time);
				$this->_last_updated = $time;
			} else {
				$this->_last_updated = $time_up;
			}
		}
		if ($time - $this->_last_updated > 86400) {
			$url = 'http://update.elcommerce.com.ua/version.xml';

			if (function_exists('curl_init')) {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$mod = curl_exec($ch);
			}
			$xml = simplexml_load_string($mod);
			@$current_version = $xml->novaposhta->version;

			if ((string) $current_version > $this->version)
				return true;
			else
				return false;
		}
	}
	

}
