<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 24.01.17
 * Time: 15:19
 */
class BlockSearchOverride extends BlockSearch
{
    public function install()
    {
        return parent::install() && $this->unregisterHook('top') && $this->registerHook('displayMobileSearch');
    }

    public function hookDisplaySearch($params)
    {
        return $this->hookTop($params);
    }

    public function hookDisplayMobileSearch($params)
    {
        if (Tools::getValue('search_query') || !$this->isCached('blocksearch-mobile-top.tpl', $this->getCacheId()))
        {
            $this->calculHookCommon($params);
            $this->smarty->assign(array(
                    'blocksearch_type' => 'top',
                    'search_query' => (string)Tools::getValue('search_query')
                )
            );
        }

        Media::addJsDef(array('blocksearch_type' => 'top'));
        return $this->display(__FILE__, 'blocksearch-mobile-top.tpl', Tools::getValue('search_query') ? null : $this->getCacheId());
    }

    public function hookHeader($params)
    {
        if (Configuration::get('PS_SEARCH_AJAX') || Configuration::get('PS_INSTANT_SEARCH'))
        {
            Media::addJsDef(array('search_url' => $this->context->link->getPageLink('search', Tools::usingSecureMode())));
            $this->context->controller->addJS(($this->_path).'blocksearch.js');
        }
    }

}