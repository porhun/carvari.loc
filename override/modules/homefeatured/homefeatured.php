<?php
if (!defined('_CAN_LOAD_FILES_'))
    exit;

class HomeFeaturedOverride extends HomeFeatured
{
    public function install()
    {

        if (!parent::install()
            || !$this->registerHook('blogProducts')
        )
            return false;

        return true;
    }

    public function hookBlogProducts($params)
    {
        $id_blog =  ( $params['id_blog'] ? $params['id_blog'] : null );
        $id_category =  ( $params['id_category'] ? $params['id_category'] : $params['cookie']->last_visited_category );
        $id_color =  ( $params['id_color'] ? $params['id_color'] : null );


        if (!$this->isCached('homefeatured_blog.tpl', $this->getCacheId($id_blog.'|'.$id_category.'|'.$id_color.'|'.date('d:H'))))
        {


            $this->_cacheProducts($id_category, $id_color, $id_blog);



            $this->smarty->assign(
                array(
                    'products' => HomeFeatured::$cache_products,
                    'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
                    'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
                )
            );
        }

        return $this->display(__FILE__, 'homefeatured_blog.tpl', $this->getCacheId($id_blog.'|'.$id_category.'|'.$id_color.'|'.date('d:H')));
    }

    public function _cacheProducts($id_category = null, $id_color = null, $id_blog = null)
    {
        if (!isset(HomeFeatured::$cache_products))
        {

            if ($id_blog && $result = DB::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'simpleblog_post_product` WHERE id_post='.$id_blog) ){
                $ids = [];
                foreach ($result as $row){
                    $ids[] =   $row['id_product'];
                }
                $result = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'product` WHERE id_product IN ('.implode(',', $ids ).')');
            } else {
                $nb = (int)Configuration::get('HOME_FEATURED_NBR');
                if (Configuration::get('HOME_FEATURED_RANDOMIZE')) {
                    $result = Product::getProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 8), 'RAND()', 'asc', $id_category ? $id_category : (int)Configuration::get('HOME_FEATURED_CAT'), true, null, $id_color, true );
                }
                else {
                    $result = Product::getProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 8), 'id_product', 'desc', $id_category ? $id_category : (int)Configuration::get('HOME_FEATURED_CAT'), true, null, $id_color, true );
                }

                if (!$result) {
                    $result = Product::getProducts((int)Context::getContext()->language->id, 0, ($nb ? $nb : 8), 'RAND()', 'desc', $id_category ? $id_category : (int)Configuration::get('HOME_FEATURED_CAT'), true, null, null, true);
                }
            }
            foreach ($result as &$row) {
                $row['id_image'] = Product::getCover($row['id_product'])['id_image'];
            }

            HomeFeatured::$cache_products = Product::getProductsProperties((int)Context::getContext()->language->id, $result);
        }

        if (HomeFeatured::$cache_products === false || empty(HomeFeatured::$cache_products))
            return false;

        return true;
    }

    protected function getCacheId($name = null)
    {
        $cache_id = parent::getCacheId();

        if ($name !== null)
            $cache_id .= '|'.$name;

        return $cache_id;
    }

}