<?php
if (!defined('_PS_VERSION_'))   exit;

class ecm_paypartsOverride extends ecm_payparts
{
	public function install()
	{
		return parent::install() && $this->registerHook('payment') && $this->registerHook('header') && $this->registerHook('paymentReturn') && $this->registerHook('displayBackOfficeHeader');
	}

	public function hookHeader()
	{
		$this->context->controller->addCSS($this->_path.'views/css/ecm_payparts.css');
	}
}
