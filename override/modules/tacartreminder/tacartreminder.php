<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 26.07.16
 * Time: 18:51
 */

/**
 * Class TACartReminderOverride
 *
 * To override private methods you should change access modifier to 'protected' in the original class
 * Method TACartReminderJournal::performReminder was overridden:
 *  - add fields 'sms_info', 'with_products' and `send_admin_emails` for $row query;
 *  - add resolving for defining of type of the message: email or sms
 *
 * Add public property $sms_info and 'sms_info' field in $definition for TACartReminderRule
 *
 * Override tpl: shopping-cart-product-line.tpl; add new tpl recent_product.tpl
 */
class TACartReminderOverride extends TACartReminder {

    /**
     * To override postProcess, we should change accessModifiers to protected
     */
    protected $tab_configure = 'mail';
    protected $form_submit = '';
    protected $success_conf = '';
    protected $html_return = '';
    protected $post_errors = [];
    protected $accesslog = true;

    /**
     * Override: add checkbox to define is it for sms info and 'with_products' to define if recently viewed products should be added to email
     *
     * @see TACartReminder::renderRuleStep1
     */
    protected function renderRuleStep1($rule)
    {
        $fields_form_1 = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('General'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_rule',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Rule name'),
                        'name' => 'name',
                    ),
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6.0', '<') === true ? 'radio' : 'switch'),
                        'is_bool' => true, // retro compat 1.5
                        'label' => $this->l('Status'),
                        'name' => 'status',
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'status_on',
                                'value' => 1,
                                'label' => $this->l('Enabled'),
                            ),
                            array(
                                'id' => 'status_off',
                                'value' => 0,
                                'label' => $this->l('Disabled'),
                            ),
                        ),
                    ),

                    /* START OVERRIDE */
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6.0', '<') === true ? 'radio' : 'switch'),
                        'is_bool' => true, // retro compat 1.5
                        'label' => $this->l('SMS info'),
                        'name' => 'sms_info',
                        'desc' => $this->l("Only 'Email txt' field from chosen template will be used for sms info"),
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'sms_info_on',
                                'value' => 1,
                                'label' => $this->l('Enabled'),
                            ),
                            array(
                                'id' => 'sms_info_off',
                                'value' => 0,
                                'label' => $this->l('Disabled'),
                            ),
                        ),
                    ),
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6.0', '<') === true ? 'radio' : 'switch'),
                        'is_bool' => true, // retro compat 1.5
                        'label' => $this->l('Add recent customer products'),
                        'name' => 'with_products',
                        'desc' => $this->l("Add recently customer's viewed products. Only for emails."),
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'with_products_on',
                                'value' => 1,
                                'label' => $this->l('Enabled'),
                            ),
                            array(
                                'id' => 'with_products_off',
                                'value' => 0,
                                'label' => $this->l('Disabled'),
                            ),
                        ),
                    ),
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6.0', '<') === true ? 'radio' : 'switch'),
                        'is_bool' => true, // retro compat 1.5
                        'label' => $this->l('Send admin\'s notification email'),
                        'name' => 'send_admin_email',
                        'desc' => $this->l("Send notification email to admins"),
                        'class' => 't',
                        'values' => array(
                            array(
                                'id' => 'send_admin_email_on',
                                'value' => 1,
                                'label' => $this->l('Enabled'),
                            ),
                            array(
                                'id' => 'send_admin_email_off',
                                'value' => 0,
                                'label' => $this->l('Disabled'),
                            ),
                        ),
                    ),
                    /* END OVERRIDE */

                ),
            ),
        );
        if (Shop::isFeatureActive()) {
            $fields_form_1['form']['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }
        $tpl_vars = array(
            'languages' => $this->context->controller->getLanguages(),
            'tacartreminder_ajax_url' => $module_url.'tacartreminder_ajax_admin_call.php?token='.
                Tools::substr(Tools::encrypt('tacartreminder/index'), 0, 10),
            'id_language' => $this->context->language->id,
        );

        return $this->renderGenericForm(
            array(
                $fields_form_1,
            ),
            $this->getRuleFieldsValues($rule->id),
            $tpl_vars
        );
    }

    /**
     * Override: add new description for variable replacing {recent_products}
     *
     * @see TACartReminder::renderMailTemplateForm
     */
    public function renderMailTemplateForm($id_mail_template = 0)
    {
        $this->context->controller->addjQueryPlugin(
            array(
                'ajaxfileupload',
            )
        );
        $this->context->controller->addJS(($this->_path).'views/js/vendor/bootstrap-colorpicker.min.js');
        $this->context->controller->addCSS(($this->_path).'views/css/vendor/bootstrap-colorpicker.min.css');
        $module_url = Tools::getProtocol(Tools::usingSecureMode()).$_SERVER['HTTP_HOST'].$this->getPathUri();
        $cart_mails = self::getCartPreview();
        $fields_form_1 = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Add new'),
                    'icon' => 'icon-envelope',
                ),
                'input' => array(
                    array(
                        'type' => 'hidden',
                        'name' => 'id_mail_template',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Template name'),
                        'name' => 'name',
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->l('Subject'),
                        'name' => 'subject',
                    ),
                    array(
                        'type' => 'text',
                        'lang' => true,
                        'label' => $this->l('Title'),
                        'name' => 'title',
                    ),
                    array(
                        'type' => 'example_mails',
                        'name' => 'example_mails',
                    ),
                    array( // important is 3 because remove autoload_rte
                        'type' => 'textarea',
                        'label' => $this->l('Email html'),
                        'name' => 'content_html',
                        'class' => 'rte autoload_rte',
                        'autoload_rte' => true,
                        'lang' => true,
                        'rows' => 5,
                        'cols' => 40,
                        'hint' => $this->l('Invalid characters:').' <>;=#',
                        'desc' => $this->l('Below are the variables you can include in your email.').'<br/>'.
                            $this->l('The following variables will be replaced by the customer and cart information.').
							'<br/><b>{current_time}</b> : '.$this->l('Приветствие пользователя - Добрый день и тд.').
                            '<br/><b>{customer_firstname}</b> : '.$this->l('customer first name').
                            '<br/><b>{customer_lastname}</b> : '.$this->l('customer last name').

                            /**
                             * OVERRIDE START
                             */
                            '<br/><b>{recent_products}</b> : '.$this->l("customer's recently viewed products (available only if 'Add recent products' option is set on in the current rule)").
                            /**
                             * OVERRIDE END
                             */

                            '<br/><b>{cart_products}</b> : '.$this->l('customer cart contents').
                            '<br/><b>{cart_products_txt}</b> : '.$this->l('customer cart contents - formatted txt').
                            '<br/><b>{shop_link_start} {shop_link_end}</b> : '.$this->l(
                                'this is the link to your store'
                            ).
                            $this->l('i.e : Click').' {shop_link_start}'.
                            $this->l('here').'{shop_link_end} '.$this->l('to access our shop').
                            '<br/><b>{shop_link_url}</b> : '.$this->l('this is the URL to your store').
                            $this->l('i.e : Click').' &lt;a href="{shop_link_url}"&gt;'.
                            $this->l('here').'&lt;/a&gt; '.$this->l('to access our shop').
                            '<br/><b>{cart_link_start} {cart_link_end}</b> : '.
                            $this->l('Link to your store to complete the order').
                            ''.$this->l('i.e : Click').' {cart_link_start}'.
                            $this->l('here').'{cart_link_end}'.$this->l('to complete your order').
                            '<br/><b>{cart_url}</b> : '.$this->l('URL to your store to complete the order (step 3)').
                            '<br/><b>{cart_url_s1}</b> : '.$this->l('URL to your store to complete the order (step 1)').
                            '<br/><b>{cart_url_s2}</b> : '.$this->l('URL to your store to complete the order (step 2)').
                            '<br/><b>{unscribe_link_start} {unscribe_link_end}</b> :'.
                            $this->l('i.e : Click ').'{unscribe_link_start}'.$this->l('here').
                            '{unscribe_link_end} '.$this->l('to receive no further reminders').
                            '<br/><b>{unscribe_url}</b> : '.$this->l('Unsubscribe URL').
                            '<br/><b>{voucher_code}</b> : '.
                            $this->l('Coupon code (only if the rule create a coupon)').''.
                            '<br/><b>{voucher_expirate_date}</b> : '.$this->l('Coupon expiry date').'<br/>',
                    ),
                    array(
                        'type' => 'convert_html_to_txt',
                        'name' => 'convert_html_to_txt',
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Email txt'),
                        'name' => 'content_txt',
                        'lang' => true,
                        'rows' => 16,
                        'cols' => 100,
                        'hint' => $this->l('Invalid characters:').' <>;=#{}',
                    ),
                    array(
                        'type' => 'preview_mail',
                        'name' => 'preview_mail',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right',
                    'name' => 'submitMailTemplate',
                ),
            ),
        );
        /*
		 * if(version_compare(_PS_VERSION_, '1.6.0', '<')) unset($fields_form_1['form']['input'][4]['autoload_rte']);
		 */
        if (Shop::isFeatureActive()) {
            $fields_form_1['form']['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = 'ta_cartreminder_mail_template';
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->id = $id_mail_template;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
            Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = 'id_mail_template';
        $helper->submit_action = 'submitEditMailTemplate';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
            '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&tab_configure=mail';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'type_render' => 'mail_template',
            'ps_version' => _PS_VERSION_,
            'egmails' => TAEgMail::getEgMails(),
            'cart_mails' => $cart_mails,
            'ta_img_url' => $this->img_url,
            'fields_value' => $this->getMailTemplateFieldsValues($id_mail_template),
            'tacartreminder_ajax_url' => $module_url.'tacartreminder_ajax_admin_call.php?token='.
                Tools::substr(Tools::encrypt('tacartreminder/index'), 0, 10),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(
            array(
                $fields_form_1,
            )
        );
    }

    /**
     * Add 'sms_info' and 'with_products' fields
     *
     * @see TACartReminder::getRuleFieldsValues
     */
    public function getRuleFieldsValues($id_rule)
    {
        $rule = new TACartReminderRule((int)$id_rule);
        $cart_rule_filter = '';
        if ($rule->id && $rule->id_cart_rule) {
            $cart_rule = new CartRule((int)$rule->id_cart_rule, $this->context->language->id);
            $cart_rule_filter = $cart_rule->id.' '.$cart_rule->name;
        }

        return array(
            'name' => $rule->name,
            'date_from' => $rule->date_from,
            'date_to' => $rule->date_to,
            'create_cart_rule' => $rule->create_cart_rule,
            'id_cart_rule' => $rule->id_cart_rule,
            'cart_rule_nbday_validity' => $rule->cart_rule_nbday_validity,
            'cart_rule_filter' => $cart_rule_filter,
            'force_reminder' => $rule->force_reminder,
            'status' => $rule->status,
            'id_rule' => $rule->id,
            // OVERRIDE
            'sms_info' => $rule->sms_info,
            'with_products' => $rule->with_products,
            'send_admin_email' => $rule->send_admin_email
        );
    }

    /**
     * Override:
     * -- add sms fields to template and rule tables;
     * -- register hook for sending email
     * -- add admin emails variable in config
     *
     * @return bool
     */
    public function install($delete_params = true)
    {
        if (!parent::install($delete_params)
            || !$this->addAdditionalFields(TACartReminderRule::$definition['table'])
            || !$this->registerHook('actionSendSmsInfo')
            || !$this->registerHook('actionAddRecentProducts')
            || !$this->registerHook('actionSendAdminReminderEmails')
            || !Configuration::updateValue('TA_CARTR_ADMIN_EMAILS', '')
        ) {
            return false;
        }
        return true;
    }

    public function uninstall($delete_params = true)
    {
        if (!parent::uninstall($delete_params)
            || !ConfigurationCore::deleteByName('TA_CARTR_ADMIN_EMAILS')
        )
            return false;
        return true;
    }

    /**
     * Sending sms info
     *
     * @param $params
     * @return bool
     */
    public function hookActionSendSmsInfo($params)
    {
        if (!$message = (string)$params['message'] || !$phone = (string)$params['phone'])
            return false;
        $sender = new SmsService();
        return (bool)$sender->sendSMS($params['message'], $params['phone']);
    }

    /**
     * Send notification emails to admin
     *
     * @param array $params     Expected params: (int)`id_customer` and CartCore `cart` object
     * @return bool
     */
    public function hookActionSendAdminReminderEmails($params)
    {
        if (!isset($params['id_customer']) || !isset($params['cart'])) {
            return false;
        }
        $cart = $params['cart'];
        $customer = new Customer((int)$params['id_customer']);
        if (!($cart instanceof CartCore) || !$customer->id) {
            return false;
        }

        $content_html = sprintf(
//            $this->l(
                'User: %1$s <br /> '.
                'Phone: %2$s <br /> '.
                'Cart ID: %3$d <br /> '.
                'Cart summary: %4$s <br /> '.
                'Created: %5$s',
//            ),
            "{$customer->firstname} {$customer->lastname}",
            (string)$customer->phone_login,
            (int)$cart->id,
            (string)CartCore::getOrderTotalUsingTaxCalculationMethod($cart->id),
            (string)$cart->date_add
        );

        $content_txt = sprintf(
            $this->l(
                'User: %1$s '. "\r\n" .
                'Phone: %2$s '. "\r\n" .
                'Cart ID: %3$d '. "\r\n" .
                'Cart summary: %4$s '. "\r\n" .
                'Created: %5$s'
            ),
            "{$customer->firstname} {$customer->lastname}",
            (string)$customer->phone_login,
            (int)$cart->id,
            (string)CartCore::getOrderTotalUsingTaxCalculationMethod($cart->id),
            (string)$cart->date_add
        );

        if ($admin_emails = $this->getAdminEmails(true)) {
            $subject = $this->l('Cart reminder notification from').' '.Configuration::get('PS_SHOP_NAME');
            // send email
            @TACartReminderTools::send(
                $content_txt,
                $content_html,
                $subject,
                [],
                $admin_emails
            );
        }

    }

    /**
     * Return tpl with customer's recently viewed products or random products
     *
     * @param array $params     Assuming to get id_customer
     * @return array
     */
    public function hookActionAddRecentProducts($params)
    {
        if (!isset($params['id_customer']) || !$id_customer = (int)$params['id_customer'])
            return [];

        $customer = new Customer($id_customer);
        $products = $this->loadProducts($customer);
        $show_taxes = Configuration::get('PS_TAX_DISPLAY') == 1 && (int)Configuration::get('PS_TAX');
        $this->context->smarty->assign(
            [
                'products'      => $products,
                'id_lang'       => (int)$customer->id_lang,
                'link'          => $this->context->link,
                'priceDisplay'  => Product::getTaxCalculationMethod(),
                'use_taxes'     => (int)Configuration::get('PS_TAX'),
                'tpl_mt_path'   => $this->local_path.'views/templates/admin/mail_template',
                'show_taxes'    => (int)$show_taxes,
            ]
        );
        return $this->context->smarty->fetch($this->local_path.'views/templates/admin/mail_template/recent_products.tpl');
    }

    /**
     * Retrieve products from the previous customer's carts
     *
     * @param CustomerCore $customer  Customer ID
     * @param int $limit        Limit of products
     * @return array
     */
    private function loadProducts(CustomerCore $customer, $limit = 5)
    {
        $cart_products_ids = $this->getCustomerCartRelativeProductIds((int)$customer->id, $limit);
        $viewed_products = count($cart_products_ids)
            ? $this->getProductsByIds($cart_products_ids, $customer->id_lang, $limit)
            : false;
        $found_products     = ($viewed_products) ?: $this->getRandomProducts($customer->id_lang, $limit);
        $formatted_products = (is_array($found_products) && count($found_products)) ? $this->formatProducts($found_products) : [];
        return $formatted_products;
    }

    /**
     * Retrieve product ids relative to the last customer's cart products
     *
     * @param int $customer_id      Customer ID
     * @param int $limit
     * @return array|mixed
     */
    private function getCustomerCartRelativeProductIds($customer_id, $limit = 5)
    {
        $query = new DbQueryCore();
        $query->select('cat_p_2.id_product')
            ->from(Cart::$definition['table'], 'c')
            ->leftJoin('cart_product', 'c_p', 'c.id_cart = c_p.id_cart')
            ->leftJoin('category_product', 'cat_p', 'c_p.id_product = cat_p.id_product')
            ->leftJoin('category_product', 'cat_p_2', 'cat_p.id_category = cat_p_2.id_category')
            ->where('c.id_customer = '.(int)$customer_id);
        $product_ids = Db::getInstance()->executeS($query, true, false);
        if (!$product_ids || !is_array($product_ids) || !count($product_ids) || !shuffle($product_ids))
            return [];
        return array_column($product_ids, 'id_product');
    }

    /**
     * Retrieve random products IDs
     *
     * @param int $limit        Products limit
     * @param int $id_lang
     * @return array
     */
    private function getRandomProducts($id_lang, $limit = 5)
    {
        $prod_table = _DB_PREFIX_.'product';
        $prod_ids = Db::getInstance()->executeS('SELECT id_product FROM '.$prod_table . ' WHERE active = 1');
        $rand_ids = array_rand(array_column($prod_ids, 'id_product'), $limit);
        return (count($rand_ids))
            ? $this->getProductsByIds($rand_ids, $id_lang)
            : [];
    }

    /**
     * Add additional information for each product for displaying in the template
     *
     * @param array $products   List of products
     * @return array
     */
    private function formatProducts(array $products)
    {
        if (empty($products))
            return [];

        foreach ($products as $key => &$product) {
            $nothing = $specific_price_output = null;
            $product['price_without_specific_price'] = Product::getPriceStatic( $product['id_product'], !Product::getTaxCalculationMethod(), $product['id_product_attribute'], 2, null, false, false, 1, false, null, null, null, $nothing, true, true, $this->context );
            $product['price_wt'] = Product::getPriceStatic((int)$product['id_product'], true, isset($product['id_product_attribute']) ? (int)$product['id_product_attribute'] : null, 6, null, false, true, 1, false, null, null, null, $specific_price_output, true, true, $this->context );
            $product['is_discounted'] = (Product::getTaxCalculationMethod())
                ? $product['price_without_specific_price'] != $product['price']
                : $product['price_without_specific_price'] != $product['price_wt'];

            if ($image = Product::getCover($product['id_product'], $this->context)) {
                $product['id_image'] = (int)$image['id_image'];
            }
        }
        return $products;
    }

    /**
     * Load list of products
     *
     * @param array $prod_ids       Product IDs
     * @param int $id_lang
     * @param int|null $limit
     * @return array|false|mysqli_result|null|PDOStatement|resource
     */
    private function getProductsByIds(array $prod_ids, $id_lang, $limit = null)
    {
        $query = new DbQuery();
        $query->select('p.*, p_l.*, p.id_category_default AS category, p.cache_default_attribute AS id_product_attribute');
        $query->from('product', 'p');
        $query->leftJoin('product_lang', 'p_l', 'p.id_product = p_l.id_product');
        $query->where('p.id_product IN ("'.implode('","', $prod_ids).'") AND p.active = 1 AND p_l.id_lang = '.(int)$id_lang);
        if (!is_null($limit) & $limit) $query->limit((int)$limit);
        return Db::getInstance()->executeS($query);
    }

    /**
     * Add additional fields to the table
     *
     * @param string $rule_table    Table name for rules
     * @return bool
     */
    private function addAdditionalFields($rule_table)
    {
        $sql_fields = 'ALTER TABLE '._DB_PREFIX_.$rule_table .
            ' ADD COLUMN `sms_info`         TINYINT(1) UNSIGNED NOT NULL DEFAULT 0, 
              ADD COLUMN `with_products`    TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
              ADD COLUMN `send_admin_email` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0';
        return (Db::getInstance()->execute($sql_fields));
    }

    protected function postProcess()
    {
        /* RULE ADMINISTRATION */
        if (Tools::isSubmit('deleteta_cartreminder_rule')) {
            $this->tab_configure = 'rule';
            $id_rule = (int)Tools::getValue('id_rule');
            $rule = new TACartReminderRule($id_rule);
            $rule->delete();
        } elseif (Tools::isSubmit('statusta_cartreminder_rule')) {
            $this->tab_configure = 'rule';
            $rule = new TACartReminderRule((int)Tools::getValue('id_rule'));
            if ($rule->id) {
                $rule->status = (int)!$rule->status;
                $rule->save();
            }
        } elseif (Tools::isSubmit('check_reminder_befor_delete')) {
            $id_reminder = Tools::getValue('id_reminder');
            $info_jreminders = TACartReminderJournal::getJRRunningByExecuted($id_reminder);
            $return = array(
                'has_error' => false,
            );
            if ($info_jreminders && count($info_jreminders) > 0) {
                $this->context->smarty->assign('info_jreminders', $info_jreminders);
                $this->context->smarty->assign('id_reminder', $id_reminder);
                $page_check_cartreminders = $this->context->smarty->fetch(
                    $this->local_path.'views/templates/admin/_configure/check_cartreminders.tpl'
                );
                $return = array(
                    'has_error' => true,
                    'page_content' => $page_check_cartreminders,
                );
            }
            die(Tools::jsonEncode($return));
        } elseif (Tools::isSubmit('submitEditRule')) {
            $this->tab_configure = 'rule';
            $this->validateRuleForm();
            $return = array(
                'has_error' => false,
            );
            $rule = new TACartReminderRule((int)Tools::getValue('id_rule'));
            $rule->name = Tools::getValue('name');
            $rule->status = (int)Tools::getValue('status');
            $rule->create_cart_rule = (int)Tools::getValue('create_cart_rule', 0);

            /**
             * OVERRIDE START
             */
            $rule->sms_info         = (int)Tools::getValue('sms_info', 0);
            $rule->with_products    = (int)Tools::getValue('with_products', 0);
            $rule->send_admin_email = (int)Tools::getValue('send_admin_email', 0);
            /**
             * END OVERRIDE
             */

            $rule->force_reminder = (int)Tools::getValue('force_reminder', 0);
            if ($rule->create_cart_rule) {
                $rule->id_cart_rule = (int)Tools::getValue('id_cart_rule');
                $rule->cart_rule_nbday_validity = (int)Tools::getValue('cart_rule_nbday_validity');
            }
            $rule->date_from = Tools::getValue('date_from');
            $rule->date_to = Tools::getValue('date_to');
            if (isset($rule->id) && (int)$rule->id) {
                Db::getInstance()->execute(
                    'DELETE FROM `'._DB_PREFIX_.'ta_cartreminder_rule_groupcondition` WHERE `id_rule` = '.$rule->id
                );
                Db::getInstance()->execute(
                    'DELETE FROM `'._DB_PREFIX_.'ta_cartreminder_rule_condition`
						WHERE `id_groupcondition`
						NOT IN (SELECT `id_groupcondition` FROM `'._DB_PREFIX_.'ta_cartreminder_rule_groupcondition`)'
                );
                Db::getInstance()->execute(
                    'DELETE FROM `'._DB_PREFIX_.'ta_cartreminder_rule_condition_value`
						WHERE `id_condition`
						NOT IN (SELECT `id_condition` FROM `'._DB_PREFIX_.'ta_cartreminder_rule_condition`)'
                );
            }

            if (($rule->id && $rule->update()) || (!$rule->id && $rule->add())) {
                $this->updateAssoShop('ta_cartreminder_rule', $rule->id, 'id_rule');
                if (Tools::getValue('condition_group') &&
                    is_array($condition_group_array = Tools::getValue('condition_group'))
                    && count($condition_group_array)) {
                    foreach ($condition_group_array as $condition_group_id) {
                        Db::getInstance()->execute(
                            'INSERT INTO `'._DB_PREFIX_.'ta_cartreminder_rule_groupcondition` (`id_rule`)
							VALUES ('.(int)$rule->id.')'
                        );
                        $id_group = Db::getInstance()->Insert_ID();
                        if (is_array($condition_array = Tools::getValue('condition_'.$condition_group_id)) &&
                            count($condition_array)
                        ) {
                            foreach ($condition_array as $condition_id) {
                                $type_condition = Tools::getValue(
                                    'condition_'.$condition_group_id.'_'.$condition_id.'_type'
                                );
                                Db::getInstance()->execute(
                                    'INSERT INTO `'._DB_PREFIX_.'ta_cartreminder_rule_condition`
                                    (`id_groupcondition`, `type`)
                                    VALUES ('.(int)$id_group.', "'.pSQL($type_condition).'")'
                                );
                                $id_condition = Db::getInstance()->Insert_ID();
                                if (Tools::getValue('condition_'.$condition_group_id.'_'.$condition_id.'_typevalue')
                                    == 'list') {
                                    $values = array();
                                    $cond_select_param = 'condition_select_'.$condition_group_id.'_'.$condition_id;
                                    $values_params = Tools::getValue($cond_select_param);
                                    foreach ($values_params as $id) {
                                        $values[] = '('.(int)$id_condition.','.(int)$id.',\'list\')';
                                    }
                                    $values = array_unique($values);
                                    if (count($values)) {
                                        Db::getInstance()->execute(
                                            'INSERT INTO `'._DB_PREFIX_.'ta_cartreminder_rule_condition_value`'.
                                            '(`id_condition`, `id_item`,`typevalue`) VALUES '.implode(',', $values)
                                        );
                                    }
                                } else {
                                    $value = Tools::getValue(
                                        'condition_'.$condition_group_id.'_'.$condition_id.'_value'
                                    );
                                    $typevalue = Tools::getValue(
                                        'condition_'.$condition_group_id.'_'.$condition_id.'_typevalue'
                                    );
                                    $sign = Tools::getValue('condition_'.$condition_group_id.'_'.$condition_id.'_sign');
                                    if ($typevalue == 'bool' && !Validate::isBool($value)) {
                                        $return['errors'][] = sprintf(
                                            $this->l('Condition %1$s value is not in a valid format, group number %2s'),
                                            $type_condition,
                                            $condition_group_id
                                        );
                                    } elseif ($typevalue == 'integer' && !Validate::isInt($value)) {
                                        $return['errors'][] = sprintf(
                                            $this->l('Condition %1$s value is not in a valid format, group number %2s'),
                                            $type_condition,
                                            $condition_group_id
                                        );
                                    } elseif ($typevalue == 'price' && !Validate::isPrice($value)) {
                                        $return['errors'][] = sprintf(
                                            $this->l('Condition %1$s value is not in a valid format, group number %2s'),
                                            $type_condition,
                                            $condition_group_id
                                        );
                                    } elseif ($typevalue == 'string' && empty($value)) {
                                        $return['errors'][] = sprintf(
                                            $this->l('Condition %1$s is required, group number %2$s'),
                                            $type_condition,
                                            $condition_group_id
                                        );
                                    } else {
                                        Db::getInstance()->execute(
                                            'INSERT INTO `'._DB_PREFIX_.'ta_cartreminder_rule_condition_value`
                                            (`id_condition`, `value`,`typevalue`,`sign`)
											VALUES ('.(int)$id_condition.',\''.$value.'\',
													\''.$typevalue.'\',\''.$sign.'\')'
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
                $reminders = $rule->getReminders();
                $reminder_id_deletes = array();
                // check reminder and delete
                foreach ($reminders as $reminder) {
                    $i = 1;
                    $foundreminder = false;
                    while (Tools::isSubmit('reminder_'.$i.'_id')) {
                        if ((int)$reminder['id_reminder'] == (int)Tools::getValue('reminder_'.$i.'_id')) {
                            $foundreminder = true;
                        }
                        $i++;
                    }
                    if (!$foundreminder) {
                        $reminder_id_deletes[] = (int)$reminder['id_reminder'];
                    }
                }
                // MARK CLOSED REMINDER because is deleted
                foreach ($reminder_id_deletes as $reminder_id_delete) {
                    $jrs_to_closed = TACartReminderJournal::getJRRunningByExecuted($reminder_id_delete);
                    foreach ($jrs_to_closed as $jr_to_closed) {
                        $journal = new TACartReminderJournal((int)$jr_to_closed['id_journal']);
                        if (Validate::isLoadedObject($journal) && $journal->id && $journal->state == 'RUNNING') {
                            $journal->state = 'CANCELED';
                            $journal->update();
                            $mess = new TACartReminderMessage();
                            $mess->id_journal = (int)$journal->id;
                            $mess->message =
                                $this->l('The cart reminder has been closed because the rule is updated and launched reminder has been deleted by employee');
                            $mess->id_employee = $this->context->employee->id;
                            $mess->add();
                        }
                    }
                }
                if (count($reminder_id_deletes)) {
                    Db::getInstance()->execute(
                        'DELETE FROM `'._DB_PREFIX_.'ta_cartreminder_rule_reminder`
							WHERE `id_rule`='.$rule->id.'
							AND `id_reminder` IN ('.implode(',', $reminder_id_deletes).')'
                    );
                }
                if (Tools::isSubmit('reminder_1_id')) {
                    $reminder_update_ids = array();
                    $pos = 1;
                    while (Tools::isSubmit('reminder_'.$pos.'_id')) {
                        $id_reminder = (int)Tools::getValue('reminder_'.$pos.'_id');
                        $data_reminder = array(
                            'id_mail_template' => (int)Tools::getValue('reminder_'.$pos.'_id_mail_template'),
                            'nb_hour' => (float)Tools::getValue('reminder_'.$pos.'_nb_hour'),
                            'manual_process' => (int)Tools::getValue('reminder_'.$pos.'_manual_process'),
                            'admin_mails' => (string)Tools::getValue('reminder_'.$pos.'_admin_mails'),
                            'position' => $pos,
                            'id_rule' => (int)$rule->id,
                        );
                        if ($id_reminder) {
                            Db::getInstance()->update(
                                'ta_cartreminder_rule_reminder',
                                $data_reminder,
                                '`id_reminder` = '.(int)$id_reminder
                            );
                            $reminder_update_ids[] = $id_reminder;
                        } else {
                            Db::getInstance()->insert('ta_cartreminder_rule_reminder', $data_reminder);
                            $reminder_update_ids[] = Db::getInstance()->Insert_ID();
                        }
                        $pos++;
                    }
                    Db::getInstance()->execute(
                        'DELETE FROM `'._DB_PREFIX_.'ta_cartreminder_rule_reminder`
                        WHERE `id_rule` = '.$rule->id.
                        (count($reminder_update_ids) ?
                            ' AND id_reminder not in ('.implode(',', $reminder_update_ids).')' : '')
                    );
                }
                TACartReminderCleans::noReminderToLaunch();
            }
            if (isset($return['errors']) && count($return['errors'])) {
                $return['has_error'] = true;
            }
            die(Tools::jsonEncode($return));
        } elseif (Tools::isSubmit('rule') && Tools::isSubmit('action') &&
            Tools::getValue('action') == 'updatePositions'
        ) {
            $this->ajaxProcessRulePositions();
        } elseif (Tools::isSubmit('convertHTMLTOTXT')) {
            $id_lang = (int)Tools::getValue('id_lang');
            $content_html = Tools::getValue('content_html_'.$id_lang);
            $content_html = str_replace('{cart_products}', '{cart_products_txt}', $content_html);
            if (empty($content_html)) {
                $language = new Language($id_lang);

                return sprintf($this->l('The email content for the lang %s is empty'), $language->iso_code);
            }
            die(TACartReminderTools::convertHtmlToText($content_html));
        } elseif (Tools::isSubmit('getExampleMailTemplate')) {
            $template_file_path = _PS_ROOT_DIR_.'/modules/tacartreminder/data/mail_templates/'.
                Tools::getValue('getExampleMailTemplate').'.html';
            if (file_exists($template_file_path)) {
                die(Tools::file_get_contents($template_file_path));
            } else {
                die('');
            }
        } elseif (Tools::isSubmit('submitEditMailTemplate')) {
            $this->tab_configure = 'mail';
            $this->form_submit = 'mail_template';
            $mail_template = new TACartReminderMailTemplate((int)Tools::getValue('id_mail_template'));
            $languages = Language::getLanguages(false);
            $lang_def = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
            $subject = array();
            $title = array();
            $content_html = array();
            $content_txt = array();
            foreach ($languages as $key => $value) {
                $subject[$value['id_lang']] = Tools::getValue('subject_'.$value['id_lang']);
                $title[$value['id_lang']] = Tools::getValue('title_'.$value['id_lang']);
                $content_html[$value['id_lang']] = Tools::getValue('content_html_'.$value['id_lang']);
                $content_txt[$value['id_lang']] = Tools::getValue('content_txt_'.$value['id_lang']);
            }
            $content_html_safe = trim(Tools::safeOutput($content_html[(int)$lang_def->id]));
            if (!isset($content_html[(int)$lang_def->id]) || empty($content_html[(int)$lang_def->id])
                || empty($content_html_safe)
            ) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %1$s is required in default lang %2$s'),
                    $this->l('Content html'),
                    $lang_def->iso_code
                );
            }
            if (!isset($content_txt[(int)$lang_def->id]) || empty($content_txt[(int)$lang_def->id])) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %1$s is required in default lang %2$s'),
                    $this->l('Content txt'),
                    $lang_def->iso_code
                );
            }
            if (!isset($subject[(int)$lang_def->id]) || empty($subject[(int)$lang_def->id])) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %1$s is required in default lang %2$s'),
                    $this->l('Subject'),
                    $lang_def->iso_code
                );
            }
            if (!isset($title[(int)$lang_def->id]) || empty($title[(int)$lang_def->id])) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %1$s is required in default lang %2$s'),
                    $this->l('Title'),
                    $lang_def->iso_code
                );
            }
            if (!count($this->post_errors)) {
                $mail_template->name = Tools::getValue('name');
                $mail_template->subject = $subject;
                $mail_template->title = $title;
                $mail_template->content_html = $content_html;
                $mail_template->content_txt = $content_txt;
                if ($mail_template->save() &&
                    $this->updateAssoShop('ta_cartreminder_mail_template', $mail_template->id, 'id_mail_template')) {
                    $this->success_conf = $this->l('Settings updated');
                } else {
                    $this->html_return .= '<div class="conf confirm ta-alert alert-danger">'.
                        $this->l('The email template could not be saved.').'</div>';
                }
            }
        } elseif (Tools::isSubmit('deleteta_cartreminder_mail_template')) {
            $this->tab_configure = 'mail';
            $id_mail_template = (int)Tools::getValue('id_mail_template');
            $rules_use_mail = TACartReminderRule::getRulesByMailTemplate($id_mail_template);
            if ($rules_use_mail && count($rules_use_mail) > 0) {
                $error_delete = $this->l('You cannot delete this email template because it is in use.').'<br/>';
                $error_delete .=
                    $this->l('If you want to delete this email template, you must first exchange this email with another one for this/these rule(s)').
                    '<br/>';
                $error_delete .= '<ul>';
                foreach ($rules_use_mail as $rule_use_mail) {
                    $error_delete .= '<li>'.$rule_use_mail['name'].'</li>';
                }
                $error_delete .= '</ul>';
                $this->post_errors[] = $error_delete;
            } else {
                $mail_template = new TACartReminderMailTemplate($id_mail_template);
                $mail_template->delete();
                $this->success_conf = $this->l('Settings updated');
            }
        } elseif (Tools::isSubmit('submitSettings')) {
            $this->tab_configure = 'configuration';
            /**
             * OVERRIDE START
             */
                // Validate and save admin emails
                $this->saveAdminEmails();
            /**
             * END OVERRIDE
             */
            if (!Validate::isFloat(Tools::getValue('TA_CARTR_ABANDONNED_NB_HOUR')) ||
                (float)Tools::getValue('TA_CARTR_ABANDONNED_NB_HOUR') < 0
            ) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %s is not in a valid format'),
                    $this->l('Number of hours after which to consider a cart abandoned')
                );
            } elseif ((float)Tools::getValue('TA_CARTR_ABANDONNED_NB_HOUR') <
                (float)Tools::getValue('TA_CARTR_STOPREMINDER_NB_HOUR')
            ) {
                Configuration::updateValue(
                    'TA_CARTR_ABANDONNED_NB_HOUR',
                    (float)Tools::getValue('TA_CARTR_ABANDONNED_NB_HOUR')
                );
            }
            if (!Validate::isInt(Tools::getValue('TA_CARTR_STOPREMINDER_NB_HOUR'))) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %s is not in a valid format'),
                    $this->l('Number of hours after which to stop the reminder')
                );
            } elseif ((int)Tools::getValue('TA_CARTR_ABANDONNED_NB_HOUR') >
                (int)Tools::getValue('TA_CARTR_STOPREMINDER_NB_HOUR')
            ) {
                $this->post_errors[] = $this->l('Cancel time-lapse must exceed cart abandoned time-lapse');
            } else {
                Configuration::updateValue(
                    'TA_CARTR_STOPREMINDER_NB_HOUR',
                    (int)Tools::getValue('TA_CARTR_STOPREMINDER_NB_HOUR')
                );
            }

            $cr_prefix = Tools::getValue('TA_CARTR_CR_PREFIX');
            if (empty($cr_prefix)) {
                $this->post_errors[] .= sprintf($this->l('Field %s is required'), $this->l('Coupon prefix'));
            } else {
                Configuration::updateValue('TA_CARTR_CR_PREFIX', $cr_prefix);
            }
            if (!count($this->post_errors)) {
                $this->success_conf = $this->l('Settings updated');
            }
            if (!Validate::isInt(Tools::getValue('TA_CARTR_AFTERREMINDER_NB_DAY'))) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %s is not in a valid format'),
                    $this->l('Number of days since last reminder')
                );
            } else {
                Configuration::updateValue(
                    'TA_CARTR_AFTERREMINDER_NB_DAY',
                    (int)Tools::getValue('TA_CARTR_AFTERREMINDER_NB_DAY')
                );
            }
            Configuration::updateValue('TA_CARTR_AUTO_ADD_CR', (int)Tools::getValue('TA_CARTR_AUTO_ADD_CR'));
            $code_format = (string)Tools::getValue('TA_CARTR_CODE_FORMAT');
            if (empty($code_format)) {
                $this->post_errors[] = $this->l('Format code is required');
            } else {
                if (Tools::strlen($code_format) < 10) {
                    $this->post_errors[] = $this->l('Format code min length is 10');
                }
                if (Tools::strlen($code_format) > 70) {
                    $this->post_errors[] = $this->l('Format code max length is 70');
                } else {
                    if (!preg_match('/^[NL]+$/', $code_format)) {
                        $this->post_errors[] = $this->l('Format code is invalid');
                    } else {
                        Configuration::updateValue('TA_CARTR_CODE_FORMAT', $code_format);
                    }
                }
            }
            if (!Validate::isInt(Tools::getValue('TA_CARTR_CLEANCARTRULE_NB_DAY'))) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %s is not in a valid format'),
                    $this->l('Number of days after which to delete expired coupons')
                );
            } else {
                Configuration::updateValue(
                    'TA_CARTR_CLEANCARTRULE_NB_DAY',
                    (int)Tools::getValue('TA_CARTR_CLEANCARTRULE_NB_DAY')
                );
            }
            if (!Validate::isInt(Tools::getValue('TA_CARTR_DEBUG'))) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %s is not in a valid format'),
                    $this->l('Debug')
                );
            } else {
                Configuration::updateValue(
                    'TA_CARTR_DEBUG',
                    (int)Tools::getValue('TA_CARTR_DEBUG')
                );
            }
            if (!Validate::isInt(Tools::getValue('TA_CARTR_SHOPLOGO'))) {
                $this->post_errors[] = sprintf(
                    $this->l('Field %s is not in a valid format'),
                    $this->l('Shop logo')
                );
            } else {
                Configuration::updateValue(
                    'TA_CARTR_SHOPLOGO',
                    (int)Tools::getValue('TA_CARTR_SHOPLOGO')
                );
            }
        }
    }

    /**
     * Override: add admins' email field
     *
     * Return html content for config form
     * @param $rule
     * @return mixed
     */
    public function renderConfigForm()
    {
        $module_url = Tools::getProtocol(Tools::usingSecureMode()).$_SERVER['HTTP_HOST'].$this->getPathUri();
        $fields_form = array();
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Configuration'),
                'icon' => 'icon-cogs',
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Abandoned cart time-lapse'),
                    'name' => 'TA_CARTR_ABANDONNED_NB_HOUR',
                    'class' => 'fixed-width-xs',
                    'suffix' => $this->l('Hour'),
                    'desc' =>
                        $this->l('The cart will be considered abandoned after this many hours (8 hours is a recommended value)'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Time-lapse for cancel or do not remind'),
                    'name' => 'TA_CARTR_STOPREMINDER_NB_HOUR',
                    'class' => 'fixed-width-xs',
                    'suffix' => $this->l('Hour'),
                    'desc' =>
                        $this->l('Do not remind a cart if the last cart update exceeds this time, or if reminders exist and have not yet been executed for a cart. After that time it will be canceled; 96 hours is a recommended value'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Tim-lapse after last cart reminder'),
                    'name' => 'TA_CARTR_AFTERREMINDER_NB_DAY',
                    'class' => 'fixed-width-xs',
                    'suffix' => $this->l('Day'),
                    'desc' =>
                        $this->l('If the customer has already received a cart reminder, it is the number of days after which he/she can receive a new cart reminder.'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Time-lapse after which to delete expired coupons'),
                    'name' => 'TA_CARTR_CLEANCARTRULE_NB_DAY',
                    'class' => 'fixed-width-xs',
                    'suffix' => $this->l('Day'),
                    'desc' => $this->l('Delete expired cart rules and not used after n days'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Coupon prefix'),
                    'name' => 'TA_CARTR_CR_PREFIX',
                    'desc' => $this->l('Prefix used for coupon code (e.g. CREMINDER-****************)'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Code format'),
                    'name' => 'TA_CARTR_CODE_FORMAT',
                    'desc' => $this->l('Use L for a Letter, N for a Number (e.g. LLLNLNNNLLL)'),
                    'size' => 50,
                ),
                // Admin email field
                array(
                    'type' => 'text',
                    'label' => $this->l('Admin emails'),
                    'name' => 'TA_CARTR_ADMIN_EMAILS',
                    'desc' => $this->l('Admin emails used for sending notification. Use a comma(,) to divide emails'),
                ),

                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6.0', '<') === true ? 'radio' : 'switch'),
                    'is_bool' => true, // retro compat 1.5
                    'label' => $this->l('Auto add coupon'),
                    'name' => 'TA_CARTR_AUTO_ADD_CR',
                    'class' => 't',
                    'desc' =>
                        $this->l('Add the coupon to the shopping cart when the customer click on the link to complete the order.'),
                    'values' => array(
                        array(
                            'id' => 'TA_CARTR_AUTO_ADD_CR_ON',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id' => 'TA_CARTR_AUTO_ADD_CR_OFF',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6.0', '<') === true ? 'radio' : 'switch'),
                    'is_bool' => true, // retro compat 1.5
                    'label' => $this->l('Shop logo'),
                    'name' => 'TA_CARTR_SHOPLOGO',
                    'class' => 't',
                    'desc' =>
                        $this->l('Indicate here if you want attach the logo in email send to your customer'),
                    'values' => array(
                        array(
                            'id' => 'TA_CARTR_SHOPLOGO_ON',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id' => 'TA_CARTR_SHOPLOGO_OFF',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                ),
                array(
                    'type' => (version_compare(_PS_VERSION_, '1.6.0', '<') === true ? 'radio' : 'switch'),
                    'is_bool' => true, // retro compat 1.5
                    'label' => $this->l('Debug mode'),
                    'name' => 'TA_CARTR_DEBUG',
                    'class' => 't',
                    'desc' =>
                        $this->l('Only to be used to analyze (developer support)'),
                    'values' => array(
                        array(
                            'id' => 'TA_CARTR_DEBUG_ON',
                            'value' => 1,
                            'label' => $this->l('Yes'),
                        ),
                        array(
                            'id' => 'TA_CARTR_DEBUG_OFF',
                            'value' => 0,
                            'label' => $this->l('No'),
                        ),
                    ),
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right',
                'name' => 'submitSettings',
            ),
        );
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->name;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang =
            Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ?
                Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitTACartReminderConfiguration';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
            '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'tacartreminder_ajax_url' => $module_url.'tacartreminder_ajax_admin_call.php?token='.
                Tools::substr(Tools::encrypt('tacartreminder/index'), 0, 10),
            'currencies' => Currency::getCurrencies(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm($fields_form);
    }

    /**
     * Override: add admin emails values
     *
     * @return array
     */
    public function getConfigFieldsValues()
    {
        $config_values = parent::getConfigFieldsValues();
        $config_values['TA_CARTR_ADMIN_EMAILS'] = $this->getAdminEmails();
        return $config_values;
    }

    /**
     * Save admin emails after validating
     */
    protected function saveAdminEmails()
    {
        $emails_value = '';
        if ($this->validateAdminEmails()) {
            $filtered_emails = array_map('trim', explode(',', Tools::getValue('TA_CARTR_ADMIN_EMAILS')));
            $emails_value = implode(', ', $filtered_emails);
        }
        ConfigurationCore::updateValue('TA_CARTR_ADMIN_EMAILS', $emails_value);
    }

    /**
     * Return exploded admin emails
     *
     * @param bool $exploded    Should string be divided
     * @return array
     */
    protected function getAdminEmails($exploded = false)
    {
        return ($exploded) ? explode(',', ConfigurationCore::get('TA_CARTR_ADMIN_EMAILS', [])) : ConfigurationCore::get('TA_CARTR_ADMIN_EMAILS', '');
    }

    /**
     * Validate admin emails
     *
     * @return bool
     */
    protected function validateAdminEmails()
    {
        if (!$admin_raw_emails = trim(Tools::getValue('TA_CARTR_ADMIN_EMAILS', ''))) {
            return true;
        }
        if (!$admin_emails = explode(',', $admin_raw_emails)) {
            $this->post_errors[] = sprintf(
                $this->l('Field %1$s has incorrect divider. Please, use commas to divide admin emails'),
                $this->l('Admin emails')
            );
            return false;
        }
        $filtered_emails = filter_var_array(array_filter($admin_emails, 'trim'), FILTER_VALIDATE_EMAIL);
        if (!array_filter($filtered_emails)) {
            $this->post_errors[] = $this->l('Admin emails are incorrect');
            return false;
        }
        return true;
    }
}