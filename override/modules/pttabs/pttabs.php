<?php

if (!defined('_CAN_LOAD_FILES_')) {
    exit;
}

class PttabsOverride extends PtTabs
{

    /**
     * @see Module::install()
     */
    public function install()
    {
        if (parent::install() == false OR !$this->registerHook('actionUpdateQuantity'))
            return false;
        return true;
    }

    public function _getProducts($id_category = false, $params, $count_product = false, $product_filter = null)
    {
        $context = Context::getContext();
        $id_lang = (int)$context->language->id;
        if (!$product_filter) {
            $order_by = $params['sort_by'];
            $order_way = $params['sort_direction'];
        } else {
            $order_by = $product_filter;
            $order_way = $params['field_direction'];
        }

        if (empty($id_category)) {
            return;
        }
        if ($id_category == 'all') {
            $id_category = $this->getCatIds($params);
        } else {
            $child_category_products = 'exclude';
            $level_depth = 9999;
            $id_category = ($child_category_products == 'include')?$this->getChildenCategories($id_category,
                $level_depth, true):$id_category;
        }
        $start = 0;
        $limit = (int)$params['product_limit'];
        $only_active = true;
        $number_days_new_product = 9999;
        if ($number_days_new_product == 0) {
            $number_days_new_product = -1;
        }
        $front = true;
        if (!in_array($context->controller->controller_type, array( 'front', 'modulefront' ))) {
            $front = false;
        }

        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way)) {
            die(Tools::displayError());
        }
        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add' || $order_by == 'date_upd') {
            $order_by_prefix = 'p';
        } elseif ($order_by == 'name') {
            $order_by_prefix = 'pl';
        }

        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by_prefix = $order_by[0];
            $order_by = $order_by[1];
        }

        if ($order_by == 'sales' || $order_by == 'rand') {
            $order_by_prefix = '';
        }
        $sql = 'SELECT DISTINCT  p.`id_product`, p.*, product_shop.*, pl.* , m.`name` AS manufacturer_name, s.`name` AS supplier_name,
				MAX(product_attribute_shop.id_product_attribute) id_product_attribute,
				  MAX(image_shop.`id_image`) id_image,  il.`legend`,
				   ps.`quantity` AS sales, cl.`link_rewrite` AS category,
				    IFNULL(stock.quantity,0) as quantity,
				     IFNULL(pa.minimal_quantity, p.minimal_quantity) as minimal_quantity,
				      stock.out_of_stock, product_shop.`date_add` > "'
            .date('Y-m-d', strtotime('-'.($number_days_new_product?(int)$number_days_new_product:20).' DAY')).'" as new, product_shop.`on_sale`
				FROM `'._DB_PREFIX_.'product` p
				'.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` '.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'product_sale` ps ON (p.`id_product` = ps.`id_product` '.Shop::addSqlAssociation('product_sale', 'ps').')
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
				ON cl.`id_category` = product_shop.`id_category_default`
				AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
            Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
				LEFT JOIN `'._DB_PREFIX_.'supplier` s ON (s.`id_supplier` = p.`id_supplier`)'.
            ($id_category?' LEFT JOIN `'._DB_PREFIX_.'category_product` c ON (c.`id_product` = p.`id_product`)':'').'
				WHERE pl.`id_lang` = '.(int)$id_lang.
            ($id_category?' AND c.`id_category` IN ('.implode(',', $id_category).')':'').
            ($front?' AND product_shop.`visibility` IN ("both", "catalog")':'').
            ($only_active?' AND product_shop.`active` = 1':'').
                ' AND IFNULL(stock.quantity, 0) > 0'.'
				GROUP BY  p.`id_product`
				ORDER BY '.(isset($order_by_prefix)?(($order_by_prefix != '')?pSQL($order_by_prefix).'.':''):'')
            .($order_by == 'rand'?' rand() ':'`'.pSQL($order_by).'`').pSQL($order_way);
        if (!$count_product) {
            $sql .= ($limit > 0?' LIMIT '.(int)$start.','.(int)$limit:'');
        }
        $rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        if ($count_product) {
            return count($rq);
        }
        if ($order_by == 'price') {
            Tools::orderbyPrice($rq, $order_way);
        }
        $products_ids = array();
        foreach ($rq as $row) {
            $products_ids[] = $row['id_product'];
        }

        Product::cacheFrontFeatures($products_ids, $id_lang);
        return Product::getProductsProperties((int)$id_lang, $rq);
    }

    public function hookActionUpdateQuantity($params)
    {
        $this->_clearCache('default.tpl');
    }
}
