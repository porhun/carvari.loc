<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author	PrestaShop SA <contact@prestashop.com>
* @copyright	2007-2015 PrestaShop SA
* @license	http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
* International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class ThemeConfiguratorOverride extends ThemeConfigurator
{

	public function __construct()
	{
		$this->name = 'themeconfigurator';
		parent::__construct();

		$this->admin_tpl_path = _PS_THEME_DIR_.'modules/'.$this->name.'/templates/admin/';
		$this->hooks_tpl_path = _PS_THEME_DIR_.'modules/'.$this->name.'/templates/hooks/';
	}


	public function install()
	{


		if (!parent::install() ||
			!$this->registerHook('displayGenderBanner') ||
			!$this->registerHook('displayBigBanner') ||
			!$this->registerHook('displaySmallBanners')
		)
			return false;

		return true;
	}

	public function hookDisplayGenderBanner()
	{
		$this->context->smarty->assign(array(
			'htmlitems' => $this->getItemsFromHook('GenderBanner'),
			'hook' => 'GenderBanner'
		));

		return $this->display(__FILE__, 'hook.tpl');
	}

	public function hookDisplayBigBanner()
	{
		$this->context->smarty->assign(array(
			'htmlitems' => $this->getItemsFromHook('BigBanner'),
			'hook' => 'BigBanner'
		));

		return $this->display(__FILE__, 'hook.tpl');
	}

	public function hookDisplaySmallBanners()
	{
		$this->context->smarty->assign(array(
			'htmlitems' => $this->getItemsFromHook('SmallBanners'),
			'hook' => 'SmallBanners'
		));

		return $this->display(__FILE__, 'hook.tpl');
	}

	protected function renderThemeConfiguratorForm()
	{
		$id_shop = (int)$this->context->shop->id;
		$items = array();
		$hooks = array();

		$this->context->smarty->assign('htmlcontent', array(
			'admin_tpl_path' => $this->admin_tpl_path,
			'hooks_tpl_path' => $this->hooks_tpl_path,

			'info' => array(
				'module' => $this->name,
				'name' => $this->displayName,
				'version' => $this->version,
				'psVersion' => _PS_VERSION_,
				'context' => (Configuration::get('PS_MULTISHOP_FEATURE_ACTIVE') == 0) ? 1 : ($this->context->shop->getTotalShops() != 1) ? $this->context->shop->getContext() : 1
			)
		));

		foreach ($this->languages as $language)
		{
			$hooks[$language['id_lang']] = array(
				'GenderBanner',
				'BigBanner',
				'SmallBanners',
				'home',
				'top',
				'left',
				'right',
				'footer'
			);

			foreach ($hooks[$language['id_lang']] as $hook)
				$items[$language['id_lang']][$hook] = Db::getInstance()->ExecuteS('
					SELECT * FROM `'._DB_PREFIX_.'themeconfigurator`
					WHERE id_shop = '.(int)$id_shop.'
					AND id_lang = '.(int)$language['id_lang'].'
					AND hook = \''.pSQL($hook).'\'
					ORDER BY item_order ASC'
				);
		}

		$this->context->smarty->assign('htmlitems', array(
			'items' => $items,
			'theme_url' => $this->context->link->getAdminLink('AdminThemeConfigurator'),
			'lang' => array(
				'default' => $this->default_language,
				'all' => $this->languages,
				'lang_dir' => _THEME_LANG_DIR_,
				'user' => $this->context->language->id
			),
			'postAction' => 'index.php?tab=AdminModules&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&tab_module=other&module_name='.$this->name.'',
			'id_shop' => $id_shop
		));

		$this->context->controller->addJqueryUI('ui.sortable');
		return $this->display(__FILE__, 'views/templates/admin/admin.tpl');
	}


	public function display($file, $template, $cache_id = null, $compile_id = null)
	{
		if (($overloaded = Module::_isTemplateOverloadedStatic(basename($file, '.php'), $template)) === null) {
			return Tools::displayError('No template found for module').' '.basename($file, '.php');
		} else {
			if (Tools::getIsset('live_edit') || Tools::getIsset('live_configurator_token')) {
				$cache_id = null;
			}

			$this->smarty->assign(array(
				'module_dir' =>    __PS_BASE_URI__.'modules/'.basename($file, '.php').'/',
				'module_template_dir' => ($overloaded ? _THEME_DIR_ : __PS_BASE_URI__).'modules/'.basename($file, '.php').'/',
				'allow_push' => $this->allow_push
			));

			if ($cache_id !== null) {
				Tools::enableCache();
			}

			$result = $this->getCurrentSubTemplate($template, $cache_id, $compile_id)->fetch();

			if ($cache_id !== null) {
				Tools::restoreCacheSettings();
			}

			$this->resetCurrentSubTemplate($template, $cache_id, $compile_id);

			return $result;
		}
	}

    protected function getConfigurableModules()
    {
        // Construct the description for the 'Enable Live Configurator' switch
        if ($this->context->shop->getBaseURL())
        {
            $request =
                'live_configurator_token='.$this->getLiveConfiguratorToken()
                .'&id_employee='.(int)$this->context->employee->id
                .'&id_shop='.(int)$this->context->shop->id
                .(Configuration::get('PS_TC_THEME') != '' ? '&theme='.Configuration::get('PS_TC_THEME') : '')
                .(Configuration::get('PS_TC_FONT') != '' ? '&theme_font='.Configuration::get('PS_TC_FONT') : '');
            $url = $this->context->link->getPageLink('index', null, $id_lang = null, $request);

            $desc = '<a class="btn btn-default" href="'.$url.'" onclick="return !window.open($(this).attr(\'href\'));" id="live_conf_button">'
                .$this->l('View').' <i class="icon-external-link"></i></a><br />'
                .$this->l('Only you can see this on your front office - your visitors will not see this tool.');
        }
        else
            $desc = $this->l('Only you can see this on your front office - your visitors will not see this tool.');

        return array(
            array(
                'label' => $this->l('Display links to your store\'s social accounts (Twitter, Facebook, etc.)'),
                'name' => 'blocksocial',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('blocksocial')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display your contact information'),
                'name' => 'blockcontactinfos',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('blockcontactinfos')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display social sharing buttons on the product\'s page'),
                'name' => 'socialsharing',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('socialsharing')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display the Facebook block on the home page'),
                'name' => 'blockfacebook',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('blockfacebook')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display the label "Credit" on the product\'s page'),
                'name' => 'blocklabelprivatbank',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('blocklabelprivatbank')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display the custom CMS information block'),
                'name' => 'blockcmsinfo',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('blockcmsinfo')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display quick view window on homepage and category pages'),
                'name' => 'quick_view',
                'value' => (int)Tools::getValue('PS_QUICK_VIEW', Configuration::get('PS_QUICK_VIEW'))
            ),
            array(
                'label' => $this->l('Display categories as a list of products instead of the default grid-based display'),
                'name' => 'grid_list',
                'value' => (int)Configuration::get('PS_GRID_PRODUCT'),
                'desc' => $this->l('Works only for first-time users. This setting is overridden by the user\'s choice as soon as the user cookie is set.'),
            ),
            array(
                'label' => $this->l('Display top banner'),
                'name' => 'blockbanner',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('blockbanner')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display logos of available payment methods'),
                'name' => 'productpaymentlogos',
                'value' => (int)Validate::isLoadedObject($module = Module::getInstanceByName('productpaymentlogos')) && $module->isEnabledForShopContext(),
                'is_module' => true,
            ),
            array(
                'label' => $this->l('Display Live Configurator'),
                'name' => 'live_conf',
                'value' => (int)Tools::getValue('PS_TC_ACTIVE', Configuration::get('PS_TC_ACTIVE')),
                'hint' => $this->l('This customization tool allows you to make color and font changes in your theme.'),
                'desc' => $desc
            ),
            array(
                'label' => $this->l('Display subcategories'),
                'name' => 'sub_cat',
                'value' => (int)Tools::getValue('PS_SET_DISPLAY_SUBCATEGORIES', Configuration::get('PS_SET_DISPLAY_SUBCATEGORIES')),
            )
        );
    }

}
