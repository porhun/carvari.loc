<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 24.01.17
 * Time: 15:43
 */

/**
 * @OVERRIDE: add VK social block
 */
class blocksocialOverride extends blocksocial
{
    public function install()
    {
        return parent::install()
            && $this->registerHook('displaySocials')
            && $this->unregisterHook('displayFooter')
            && Configuration::updateValue('BLOCKSOCIAL_VK', '');
    }

    public function uninstall()
    {
        return parent::uninstall() && Configuration::deleteByName('BLOCKSOCIAL_VK');
    }

    public function getContent()
    {
        if (Tools::isSubmit('submitModule')) {
            Configuration::updateValue('BLOCKSOCIAL_VK', Tools::getValue('blocksocial_vk', ''));
        }
        return parent::getContent();
    }

    /**
     * @OVERRIDE: add VK field
     *
     * @return mixed
     */
    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Facebook URL'),
                        'name' => 'blocksocial_facebook',
                        'desc' => $this->l('Your Facebook fan page.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Vkontakte URL'),
                        'name' => 'blocksocial_vk',
                        'desc' => $this->l('Your VK page.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Twitter URL'),
                        'name' => 'blocksocial_twitter',
                        'desc' => $this->l('Your official Twitter account.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('RSS URL'),
                        'name' => 'blocksocial_rss',
                        'desc' => $this->l('The RSS feed of your choice (your blog, your store, etc.).'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('YouTube URL'),
                        'name' => 'blocksocial_youtube',
                        'desc' => $this->l('Your official YouTube account.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Google+ URL:'),
                        'name' => 'blocksocial_google_plus',
                        'desc' => $this->l('Your official Google+ page.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Pinterest URL:'),
                        'name' => 'blocksocial_pinterest',
                        'desc' => $this->l('Your official Pinterest account.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Vimeo URL:'),
                        'name' => 'blocksocial_vimeo',
                        'desc' => $this->l('Your official Vimeo account.'),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $this->l('Instagram URL:'),
                        'name' => 'blocksocial_instagram',
                        'desc' => $this->l('Your official Instagram account.'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValues()
    {
        $configs = parent::getConfigFieldsValues();
        $configs['blocksocial_vk'] = Tools::getValue('blocksocial_vk', Configuration::get('BLOCKSOCIAL_VK'));
        return $configs;
    }

    public function hookDisplayFooter()
    {
        $this->smarty->assign(['vk_url' => Configuration::get('BLOCKSOCIAL_VK')]);
        return parent::hookDisplayFooter();
    }

    public function hookDisplaySocials($params)
    {
        return $this->hookDisplayFooter();
    }
}