<?php
/*
* @author    Krystian Podemski <podemski.krystian@gmail.com>
* @site
* @copyright  Copyright (c) 2013-2015 Krystian Podemski - www.PrestaHome.com
* @license    You only can use module, nothing more!
*/

if(!defined('THUMBLIB_BASE_PATH'))
    require_once _PS_MODULE_DIR_ . 'ph_simpleblog/assets/phpthumb/ThumbLib.inc.php';

require_once _PS_MODULE_DIR_ . 'ph_simpleblog/models/SimpleBlogHelper.php';
require_once _PS_MODULE_DIR_ . 'ph_simpleblog/models/SimpleBlogCategory.php';
require_once _PS_MODULE_DIR_ . 'ph_simpleblog/models/SimpleBlogPost.php';
require_once _PS_MODULE_DIR_ . 'ph_simpleblog/models/SimpleBlogTag.php';

if (!defined('_PS_VERSION_')) {
    exit;
}

class ph_simpleblogOverride extends ph_simpleblog
{

    public function hookModuleRoutes($params)
    {
        $context = Context::getContext();
        $controller = Tools::getValue('controller', 0);

        if($controller == 'AdminSimpleBlogPosts' && isset($_GET['updatesimpleblog_post']))
            return array();

        $blog_slug = Configuration::get('PH_BLOG_SLUG');

        $my_routes = array(
            /**
            Home
             **/
            // Home list
            'module-ph_simpleblog-list' => array(
                'controller' => 'list',
                'rule' => $blog_slug.'/',
                'keywords' => array(),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ph_simpleblog',
                ),
            ),
            // Home pagination
            'module-ph_simpleblog-page' => array(
                'controller' => 'page',
                'rule' => $blog_slug.'/page/{p}/',
                'keywords' => array(
                    'p' =>        array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'p'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ph_simpleblog',
                ),
            ),

            /**
            Category
             **/

            // Category list
            'module-ph_simpleblog-category' => array(
                'controller' => 'category',
                'rule' =>       $blog_slug.'/{sb_category}',
                'keywords' => array(
                    'sb_category' => array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'sb_category'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ph_simpleblog',
                ),
            ),
            // Category pagination
            'module-ph_simpleblog-categorypage' => array(
                'controller' => 'categorypage',
                'rule' => $blog_slug.'/{sb_category}/page/{p}',
                'keywords' => array(
                    'p' =>        array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'p'),
                    'sb_category' =>        array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'sb_category'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ph_simpleblog',
                ),
            ),

            'module-ph_simpleblog-single' => array(
                'controller' => 'single',
                'rule' =>       $blog_slug.'/{sb_category}/{rewrite}/',
                'keywords' => array(
                    'sb_category' =>       array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'sb_category'),
                    'rewrite' =>        array('regexp' => '[_a-zA-Z0-9-\pL]*', 'param' => 'rewrite'),
                ),
                'params' => array(
                    'fc' => 'module',
                    'module' => 'ph_simpleblog',
                ),
            ),
        );

        return $my_routes;
    }
}
