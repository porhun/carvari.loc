<?php
/**
* Module is prohibited to sales! Violation of this condition leads to the deprivation of the license!
*
* @category  Front Office Features
* @package   Advanced Checkout Module
* @author    Maxim Bespechalnih <2343319@gmail.com>
* @copyright 2013-2015 Max
* @license   license.txt in the module folder.
*/

class AdvancedcheckoutPaymentModuleFrontControllerOverride extends ModuleFrontController
{
	public $display_column_right = false;
	public $ssl = true;

    public $php_self = 'module-advancedcheckout-confirm';

	public function initContent()
	{
//		parent::initContent();
//
//		$cart = $this->context->cart;
//
//		require_once(dirname(__FILE__).'/../../classes/Unpay.php');
//		$paysistem = new Unpay((int)Tools::getValue('id_unpay'), $this->context->cookie->id_lang);
//
//		if (!Validate::isLoadedObject($paysistem))
//			return;
//
//		$paysistem->description = str_replace(
//			array('%total%'),
//			array(Tools::DisplayPrice($cart->getOrderTotal(true, Cart::BOTH))),
//			$paysistem->description
//		);
//
//		$this->context->smarty->assign(array(
//			'nbProducts' => $cart->nbProducts(),
//			'paysistem' => $paysistem,
//			'this_path' => $this->module->getPathUri(),
//			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/'
//		));
//
//		$this->setTemplate('payment_execution.tpl');


        parent::initContent();
        /**@var $cart Cart  */
        $cart = $this->context->cart;

        if ($cart->id == 0 || $cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active)
            Tools::redirect('index.php?controller=order&step=1');

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer))
            Tools::redirect('index.php?controller=order&step=1');

        $currency = $this->context->currency;
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);

        require_once(dirname(__FILE__).'/../../classes/Unpay.php');
        $paysistem = new Unpay(1, $this->context->cookie->id_lang);
        if (!Validate::isLoadedObject($paysistem))
            return;

        $mail_vars =	array(
            '{paysistem_name}' => $paysistem->name
        );

        $or = new AdvOrderCreate();
        $or->name = $this->linkrewrite(Tools::str2url($paysistem->name));
        $or->validateOrder((int)$cart->id, $paysistem->id_order_state, $total, $paysistem->name, null,
            $mail_vars, (int)$currency->id, false, ($cart->secure_key ? $cart->secure_key : false));

        if ($paysistem->description_success || $or->currentOrder)
        {
            $order = new Order($or->currentOrder);

            if ($this->context->customer->is_guest)
            {
                $this->context->smarty->assign(array(
                    'id_order' => $order->id,
                    'reference_order' => $order->reference,
                    'id_order_formatted' => sprintf('#%06d', $order->id),
                    'email' => $this->context->customer->email
                ));
                $this->context->customer->logout();
            }
            $params = array();
            $currency = new Currency($order->id_currency);
            $params['total_to_pay'] = $order->getOrdersTotalPaid();
            $params['currency'] = $currency->sign;
            $params['objOrder'] = $order;
            $params['currencyObj'] = $currency;

            if (Validate::isLoadedObject($order) && $order->id_customer == $this->context->customer->id)
            {
                $id_order_state = (int)$order->getCurrentState();
                $carrier = new Carrier((int)$order->id_carrier, (int)$order->id_lang);
                $addressInvoice = new Address((int)$order->id_address_invoice);
                $addressDelivery = new Address((int)$order->id_address_delivery);

                if ($order->total_discounts > 0)
                    $this->context->smarty->assign('total_old', (float)$order->total_paid - $order->total_discounts);
                $products = $order->getProducts();

                /* DEPRECATED: customizedDatas @since 1.5 */
                $customizedDatas = Product::getAllCustomizedDatas((int)$order->id_cart);
                Product::addCustomizationPrice($products, $customizedDatas);

                OrderReturn::addReturnedQuantity($products, $order->id);

                $customer = new Customer($order->id_customer);
                $link = new Link();

                if ( isset($this->context->cookie->id_store) && !empty( $this->context->cookie->id_store )){
                    Db::getInstance()->insert('ffcarvaricarrier', array(
                        'id_order' => $order->id,
                        'id_store' => $this->context->cookie->id_store,
                    ));
                }
                if ( isset($this->context->cookie->address) && !empty( $this->context->cookie->address ) ){
                    Db::getInstance()->insert('ffcarvaricourier', array(
                        'id_order' => $order->id,
                        'address' => $this->context->cookie->address,
                    ));
                }

                $this->context->smarty->assign(array(
                    'order' => $order,
                    'products' => $products,
                    'discounts' => $order->getCartRules(),
                    'use_tax' => Configuration::get('PS_TAX'),
                    'group_use_tax' => (Group::getPriceDisplayMethod($customer->id_default_group) == PS_TAX_INC),
                    'link' => $link,
                    'total' => $total,
                    'img_url' => _PS_BASE_URL_._THEME_PROD_DIR_,
                    'cart_qties' => 0,  // set cart products number to 0 because customer has already purchased it successfully

                ));

                unset($carrier, $addressInvoice, $addressDelivery);
            }
            else
                $this->errors[] = Tools::displayError('This order cannot be found.');
        }
        else
            Tools::redirect('index.php?controller=order-confirmation&id_cart='.(int)$cart->id.'&id_module='.
                (int)$this->module->id.'&id_order='.$this->module->currentOrder.'&key='.$customer->secure_key);


//        $this->display(__FILE__, 'views/templates/front/payment_execution.tpl');
        $this->setTemplate('payment_execution.tpl');
	}

    public function linkrewrite($insert)
    {
        $insert = mb_strtolower($insert);    // Если работаем с юникодными строками
        $insert = Tools::strtolower($insert); $replase = array(
        // Буквы
        'а'=>'a',
        'б'=>'b',
        'в'=>'v',
        'г'=>'g',
        'д'=>'d',
        'е'=>'e',
        'ё'=>'yo',
        'ж'=>'zh',
        'з'=>'z',
        'и'=>'i',
        'й'=>'j',
        'к'=>'k',
        'л'=>'l',
        'м'=>'m',
        'н'=>'n',
        'о'=>'o',
        'п'=>'p',
        'р'=>'r',
        'с'=>'s',
        'т'=>'t',
        'у'=>'u',
        'ф'=>'f',
        'х'=>'h',
        'ц'=>'c',
        'ч'=>'ch',
        'ш'=>'sh',
        'щ'=>'shh',
        'ъ'=>'j',
        'ы'=>'y',
        'ь'=>'',
        'э'=>'e',
        'ю'=>'yu',
        'я'=>'ya',
        ' '=>'_',
        ' - '=>'_',
        '-'=>'_',
        '.'=>'',
        ':'=>'',
        ';'=>'',
        ','=>'',
        '!'=>'',
        '?'=>'',
        '>'=>'',
        '<'=>'',
        '&'=>'',
        '*'=>'',
        '%'=>'',
        '$'=>'',
        '"'=>'',
        '\''=>'',
        '('=>'',
        ')'=>'',
        '`'=>'',
        '+'=>'',
        '/'=>'',
        '\\'=>'',
    );
        $insert = preg_replace('/  +/', ' ', $insert); // Удаляем лишние пробелы
        $insert = strtr($insert, $replase);
        return $insert;
    }
}

class AdvOrderCreate extends PaymentModule
{
    public $active = true;
    public $module = 'advcheckout';
    public $name = 'advcheckout';
}