<?php

if (!defined('_PS_VERSION_')) {
    exit;
}
//Овверайд не подключился. Эти изменения были добавленны в основной класс
class TmoneclickorderOverride extends Tmoneclickorder
{
    public function createCustomer($cart)
    {
        $customer = new Customer();

        $customer->firstname = Tools::getValue('firstname', $customer->firstname);
        $customer->lastname = Tools::getValue('lastname', $customer->lastname);
        $customer->phone_login = preg_replace('/[. ()-]*/', '', Tools::getValue('number', $customer->phone_login));
        $customer->email = Tools::getValue('email', $customer->email);
        $customer->passwd = md5(_COOKIE_KEY_.Tools::getValue('passwd', $customer->passwd));

        if ((bool)Tools::getValue('random')) {
            $rand = Tools::passwdGen(4);
            if (empty($customer->firstname)) {
                $customer->firstname = 'firstname';
            }

            if (empty($customer->lastname)) {
                $customer->lastname = 'lastname';
            }

            if (empty($customer->phone_login)) {
                $customer->phone_login = '+380000000000';
            }

            if (empty($customer->email)) {
                $customer->email = 'email' . $rand. '@rand.net';
            }

            if (empty($customer->firstname)) {
                $customer->passwd = md5(_COOKIE_KEY_.$rand);
            }
        }

        if (!$customer->add()) {
            return false;
        } elseif (!(bool)Tools::getValue('random')) {
            $vars = array(
                '{firstname}' => $customer->firstname,
                '{lastname}' => $customer->lastname,
                '{email}' => $customer->email,
                '{passwd}' => Tools::getValue('passwd')
            );

            Mail::Send(
                (int)$cart->id_lang,
                'guest_to_customer',
                Mail::l('Your guest account has been transformed into a customer account', (int)$cart->id_lang),
                $vars,
                $customer->email,
                $customer->firstname.' '.$customer->lastname,
                null,
                null,
                null,
                null,
                _PS_MAIL_DIR_,
                false,
                (int)$this->id_shop
            );
        }

        return $customer;
    }

//    public function renderNewOrderForm($id_order)
//    {
//        if (Context::getContext()->shop->getContext() != Shop::CONTEXT_SHOP && Shop::isFeatureActive()) {
//            $this->errors[] = $this->l('You have to select a shop before creating new orders.');
//        }
//
//        $order = new TMOneClickOrderOrders($id_order);
//
//        $cart = new Cart((int)$order->id_cart);
//
//        if ($cart->id_customer != 0) {
//            $order->customer = new Customer($cart->id_customer);
//            $order->customer->gender = new Gender($order->customer->id_gender, $this->id_lang, $this->id_shop);
//            $order->customer->addresses = $order->customer->getAddresses($this->id_lang);
//        }
//        if ($order->id_cart && !Validate::isLoadedObject($cart)) {
//            $this->errors[] = $this->l('This cart does not exist');
//        }
//        if ($order->id_cart && Validate::isLoadedObject($cart) && !$cart->id_customer) {
//            $this->errors[] = $this->l('The cart must have a customer');
//        }
//        if (count($this->_errors)) {
//            return false;
//        }
//
//        $cod_validation = Configuration::get('PS_OS_COD_VALIDATION');
//        $preparation = Configuration::get('PS_OS_PREPARATION');
//        $defaults_order_state = array('cheque' => (int)Configuration::get('PS_OS_CHEQUE'),
//            'bankwire' => (int)Configuration::get('PS_OS_BANKWIRE'),
//            'cashondelivery' => $cod_validation ? (int)$cod_validation : (int)$preparation,
//            'other' => (int)Configuration::get('PS_OS_PAYMENT'));
//        $payment_modules = array();
//        foreach (PaymentModule::getInstalledPaymentModules() as $p_module) {
//            $payment_modules[] = Module::getInstanceById((int)$p_module['id_module']);
//        }
//
//        $this->context->smarty->assign(array(
//            'recyclable_pack' => (int)Configuration::get('PS_RECYCLABLE_PACK'),
//            'gift_wrapping' => (int)Configuration::get('PS_GIFT_WRAPPING'),
//            'cart' => $cart,
//            'currencies' => Currency::getCurrenciesByIdShop($this->id_shop),
//            'langs' => $this->langs,
//            'payment_modules' => $payment_modules,
//            'order_states' => OrderState::getOrderStates((int)$this->id_lang),
//            'defaults_order_state' => $defaults_order_state,
//            'PS_CATALOG_MODE' => Configuration::get('PS_CATALOG_MODE'),
//            'title' => array($this->l('Orders'), $this->l('Create order')),
//            'link' => new Link(),
//            'order' => $order,
//            'pic_dir' => _THEME_PROD_PIC_DIR_,
//            'customer_info' => TMOneClickOrderCustomers::selectAllFields($id_order),
//            'countries' => CountryCore::getCountries($this->id_lang, true),
//            'states' => $this->getStates((int)$this->context->country->id),
//            'products' => $cart->getProducts(),
//            'total_price' => $this->getOrderTotalPrice($order->id_cart)
//        ));
//
//        return $this->display($this->_path, 'views/templates/admin/new_order_info.tpl');
//    }
//
//    public function getStates($id_country)
//    {
//        return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
//		SELECT `id_state`, `id_country`, `id_zone`, `iso_code`, `name`, `active`
//		FROM `'._DB_PREFIX_.'state`
//		WHERE `active` = 1
//		AND `id_country` = '.$id_country.'
//		ORDER BY `name` ASC');
//    }

}
