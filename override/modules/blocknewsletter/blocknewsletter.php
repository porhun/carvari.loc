<?php
if (!defined('_CAN_LOAD_FILES_'))
    exit;

class BlocknewsletterOverride extends Blocknewsletter
{

    public function install()
    {
        if (!parent::install() || !$this->registerHook('displayPopup') || !$this->registerHook('displaySubscribeForm'))
            return false;

        return true;
    }

    public function hookDisplayHome($params)
    {
        if ($this->context->cookie->popup_close || $this->context->isMobile()) {
            return '';
        }

        if (!isset($this->prepared) || !$this->prepared)
            $this->_prepareHook($params);
        $this->prepared = true;
        return $this->display(__FILE__, 'blocknewsletter_home.tpl');
    }

    public function hookDisplaySubscribeForm($params)
    {

        $this->_prepareHook($params);
        return $this->display(__FILE__, 'blocknewsletter_subscribe.tpl');
    }

    public function hookDisplayPopup($params)
    {
        if ($this->context->cookie->popup_close) {
            return '';
        }

        if (!isset($this->prepared) || !$this->prepared)
            $this->_prepareHook($params);
        $this->prepared = true;
        return $this->display(__FILE__, 'blocknewsletter_home.tpl');
    }
    

    public function publicRegistration()
    {
        if (empty($_POST['email']) || !Validate::isEmail($_POST['email']))
            return $this->error = $this->l('Invalid email address.');

        /* Unsubscription */
        else if ($_POST['action'] == '1')
        {
            $register_status = $this->isNewsletterRegistered($_POST['email']);

            if ($register_status < 1)
                return $this->error = $this->l('This email address is not registered.');

            if (!$this->unregister($_POST['email'], $register_status))
                return $this->error = $this->l('An error occurred while attempting to unsubscribe.');

            return $this->valid = $this->l('Unsubscription successful.');
        }
        /* Subscription */
        else if ($_POST['action'] == '0')
        {
            $register_status = $this->isNewsletterRegistered($_POST['email']);
            if ($register_status > 0)
                return $this->error = $this->l('This email address is already registered.');

            $email = pSQL($_POST['email']);
            $gender =  $_POST['gender'];
            if (!$this->isRegistered($register_status))
            {
                if (Configuration::get('NW_VERIFICATION_EMAIL'))
                {

                    // create an unactive entry in the newsletter database
                    if ($register_status == self::GUEST_NOT_REGISTERED)
                        $this->registerGuest($email, $gender, false);

                    if (!$token = $this->getToken($email, $register_status))
                        return $this->error = $this->l('An error occurred during the subscription process.');

                    $this->sendVerificationEmail($email, $token);

                    return $this->valid = $this->l('A verification email has been sent. Please check your inbox.');
                }
                else
                {
                    if ($this->register($email, $gender, $register_status))
                        $this->valid = $this->l('You have successfully subscribed to this newsletter.');
                    else
                        return $this->error = $this->l('An error occurred during the subscription process.');

                    if ($code = Configuration::get('NW_VOUCHER_CODE'))
                        $this->sendVoucher($email, $code);

                    if (Configuration::get('NW_CONFIRMATION_EMAIL'))
                        $this->sendConfirmationEmail($email);
                }
            }
        }
    }


    /**
     * Subscribe a guest to the newsletter
     *
     * @param string $email
     * @param bool   $active
     *
     * @return bool
     */
    protected function registerGuest($email, $gender = NULL, $active = true)
    {

        $sql = 'INSERT INTO '._DB_PREFIX_.'newsletter (id_shop, id_shop_group, email, newsletter_date_add, ip_registration_newsletter, http_referer, active, gender)
				VALUES
				('.$this->context->shop->id.',
				'.$this->context->shop->id_shop_group.',
				\''.pSQL($email).'\',
				NOW(),
				\''.pSQL(Tools::getRemoteAddr()).'\',
				(
					SELECT c.http_referer
					FROM '._DB_PREFIX_.'connections c
					WHERE c.id_guest = '.(int)$this->context->customer->id.'
					ORDER BY c.date_add DESC LIMIT 1
				),
				'.(int)$active.',
				\''.$gender.'\'
				)';

        return Db::getInstance()->execute($sql);
    }


    public function getSubscribers()
    {
        $dbquery = new DbQuery();
        $dbquery->select('c.`id_customer` AS `id`, s.`name` AS `shop_name`, gl.`name` AS `gender`, c.`lastname`, c.`firstname`, c.`email`, c.`newsletter` AS `subscribed`, c.`newsletter_date_add`');
        $dbquery->from('customer', 'c');
        $dbquery->leftJoin('shop', 's', 's.id_shop = c.id_shop');
        $dbquery->leftJoin('gender', 'g', 'g.id_gender = c.id_gender');
        $dbquery->leftJoin('gender_lang', 'gl', 'g.id_gender = gl.id_gender AND gl.id_lang = '.(int)$this->context->employee->id_lang);
        $dbquery->where('c.`newsletter` = 1');
        if ($this->_searched_email)
            $dbquery->where('c.`email` LIKE \'%'.pSQL($this->_searched_email).'%\' ');

        $customers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($dbquery->build());

        $dbquery = new DbQuery();
        $dbquery->select('CONCAT(\'N\', n.`id`) AS `id`, s.`name` AS `shop_name`, n.`gender` AS `gender`, NULL AS `lastname`, NULL AS `firstname`, n.`email`, n.`active` AS `subscribed`, n.`newsletter_date_add`');
        $dbquery->from('newsletter', 'n');
        $dbquery->leftJoin('shop', 's', 's.id_shop = n.id_shop');
        $dbquery->where('n.`active` = 1');
        if ($this->_searched_email)
            $dbquery->where('n.`email` LIKE \'%'.pSQL($this->_searched_email).'%\' ');

        $non_customers = Db::getInstance()->executeS($dbquery->build());

        $subscribers = array_merge($customers, $non_customers);

        return $subscribers;
    }

    /**
     * Subscribe an email to the newsletter. It will create an entry in the newsletter table
     * or update the customer table depending of the register status
     *
     * @param string $email
     * @param int    $register_status
     */
    protected function register($email, $gender, $register_status)
    {
        if ($register_status == self::GUEST_NOT_REGISTERED)
            return $this->registerGuest($email,$gender);

        if ($register_status == self::CUSTOMER_NOT_REGISTERED)
            return $this->registerUser($email);

        return false;
    }

    /**
     * Check if this mail is registered for newsletters
     *
     * @param string $customer_email
     *
     * @return int -1 = not a customer and not registered
     *                0 = customer not registered
     *                1 = registered in block
     *                2 = registered in customer
     */
    public function isNewsletterRegistered($customer_email)
    {
        $sql = 'SELECT `email`
				FROM '._DB_PREFIX_.'newsletter
				WHERE `email` = \''.pSQL($customer_email).'\'
				AND id_shop = '.$this->context->shop->id;

        if (Db::getInstance()->getRow($sql))
            return self::GUEST_REGISTERED;

        $sql = 'SELECT `newsletter`
				FROM '._DB_PREFIX_.'customer
				WHERE `email` = \''.pSQL($customer_email).'\'
				AND id_shop = '.$this->context->shop->id;

        if (!$registered = Db::getInstance()->getRow($sql))
            return self::GUEST_NOT_REGISTERED;

        if ($registered['newsletter'] == '1')
            return self::CUSTOMER_REGISTERED;

        return self::CUSTOMER_NOT_REGISTERED;
    }
}