<?php

class GanalyticsOverride extends Ganalytics
{

	protected function _getGoogleAnalyticsTag($back_office = false, $params = false)
	{
		$additional_code = '';
		if ($params) {
			$additional_code .= 'ga(\'require\', \'displayfeatures\');';

			$controller_name = $this->context->controller->php_self;
//			p($params);
//			p('fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff');
//			d($this->context->controller);

			switch ($controller_name){
				case 'index':
					$additional_code .= 'ga(\'set\',\'dimension1\', \'\');';
					$additional_code .= 'ga(\'set\',\'dimension2\', \'home\');';
					$additional_code .= 'ga(\'set\',\'dimension3\', \'\');';
					break;
				case 'product':
					$id_product = $this->context->controller->getProduct()->id;
					$price = $this->context->controller->getProduct()->getPrice();


					$additional_code .= 'ga(\'set\',\'dimension1\', \''.$id_product.'\');';
					$additional_code .= 'ga(\'set\',\'dimension2\', \'offerdetail\');';
					$additional_code .= 'ga(\'set\',\'dimension3\', \''.$price.'\');';
					break;
				case 'order-opc':
					$ids_arr = array();
					$prices_arr = array();
					foreach ($params['cart']->getProducts() as $cart_prod)
					{
						$ids_arr[] = $cart_prod['id_product'];
						$prices_arr[] = $cart_prod['price_with_reduction'];
					}
					$additional_code .= 'ga(\'set\',\'dimension1\', \'['.implode(',', $ids_arr).']\');';
					$additional_code .= 'ga(\'set\',\'dimension2\', \'conversionintent\');';
					$additional_code .= 'ga(\'set\',\'dimension3\', \'['.implode(',', $prices_arr).']\');';

					break;
				case 'module-advancedcheckout-confirm':
					$ids_arr = array();
					$prices_arr = array();
					foreach ($params['cart']->getProducts() as $cart_prod)
					{
						$ids_arr[] = $cart_prod['id_product'];
						$prices_arr[] = $cart_prod['price_with_reduction'];
					}
					$additional_code .= 'ga(\'set\',\'dimension1\', \'['.implode(',', $ids_arr).']\');';
					$additional_code .= 'ga(\'set\',\'dimension2\', \'conversion\');';
					$additional_code .= 'ga(\'set\',\'dimension3\', \'['.implode(',', $prices_arr).']\');';
					break;
				default:
					$additional_code .= 'ga(\'set\',\'dimension1\', \'\');';
					$additional_code .= 'ga(\'set\',\'dimension2\', \'other\');';
					$additional_code .= 'ga(\'set\',\'dimension3\', \'\');';
					break;
			}



			$additional_code .= 'ga(\'send\', \'pageview\');';
		}


		return '
			<script type="text/javascript">
				(window.gaDevIds=window.gaDevIds||[]).push(\'d6YPbH\');
				(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');
				ga(\'create\', \''.Tools::safeOutput(Configuration::get('GA_ACCOUNT_ID')).'\', \'auto\');
				ga(\'require\', \'ec\');
				'.$additional_code
				.($back_office ? 'ga(\'set\', \'nonInteraction\', true);' : '').'
			</script>';
	}

	public function hookHeader($params)
	{
		if (Configuration::get('GA_ACCOUNT_ID'))
		{
			$this->context->controller->addJs($this->_path.'views/js/GoogleAnalyticActionLib.js');

			return $this->_getGoogleAnalyticsTag(false, $params);
		}
	}
}
