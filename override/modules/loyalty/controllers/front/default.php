<?php
/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 09.12.16
 * Time: 17:31
 */
class LoyaltyDefaultModuleFrontControllerOverride extends LoyaltyDefaultModuleFrontController
{
    /**
     * Transform loyalty point to a voucher
     */
    public function processTransformPoints()
    {
        $customer_points = (int)LoyaltyModule::getPointsByCustomer((int)$this->context->customer->id);
        if ($customer_points > 0)
        {
            /* Generate a voucher code */
            $voucher_code = null;
            do
            $voucher_code = 'FID'.rand(1000, 100000);
            while (CartRule::cartRuleExists($voucher_code));

            // Voucher creation and affectation to the customer
            $cart_rule = new CartRule();
            $cart_rule->code = $voucher_code;
            $cart_rule->id_customer = (int)$this->context->customer->id;
            $cart_rule->reduction_currency = (int)$this->context->currency->id;
            $cart_rule->reduction_amount = LoyaltyModule::getVoucherValue((int)$customer_points);
            $cart_rule->quantity = 1;
            $cart_rule->highlight = 1;
            $cart_rule->quantity_per_user = 1;
            $cart_rule->reduction_tax = (bool)Configuration::get('PS_LOYALTY_TAX');

            // If merchandise returns are allowed, the voucher musn't be usable before this max return date
            $date_from = Db::getInstance()->getValue('
			SELECT UNIX_TIMESTAMP(date_add) n
			FROM '._DB_PREFIX_.'loyalty
			WHERE id_cart_rule = 0 AND id_customer = '.(int)$this->context->cookie->id_customer.'
			ORDER BY date_add DESC');

            if (Configuration::get('PS_ORDER_RETURN'))
                $date_from += 60 * 60 * 24 * (int)Configuration::get('PS_ORDER_RETURN_NB_DAYS');

            $cart_rule->date_from = date('Y-m-d H:i:s', $date_from);
            /**
             * OVERRIDE: change 1 year to 6 months
             */
            $cart_rule->date_to = date('Y-m-d H:i:s', strtotime($cart_rule->date_from.' +6 months'));

            $cart_rule->minimum_amount = (float)Configuration::get('PS_LOYALTY_MINIMAL');
            $cart_rule->minimum_amount_currency = (int)$this->context->currency->id;
            $cart_rule->active = 1;

            $categories = Configuration::get('PS_LOYALTY_VOUCHER_CATEGORY');
            if ($categories != '' && $categories != 0)
                $categories = explode(',', Configuration::get('PS_LOYALTY_VOUCHER_CATEGORY'));
            else
                die (Tools::displayError());

            $languages = Language::getLanguages(true);
            $default_text = Configuration::get('PS_LOYALTY_VOUCHER_DETAILS', (int)Configuration::get('PS_LANG_DEFAULT'));

            foreach ($languages as $language)
            {
                $text = Configuration::get('PS_LOYALTY_VOUCHER_DETAILS', (int)$language['id_lang']);
                $cart_rule->name[(int)$language['id_lang']] = $text ? strval($text) : strval($default_text);
            }


            $contains_categories = is_array($categories) && count($categories);
            if ($contains_categories)
                $cart_rule->product_restriction = 1;

            $cart_rule->add();

            //Restrict cartRules with categories
            if ($contains_categories)
            {

                //Creating rule group
                $id_cart_rule = (int)$cart_rule->id;
                $sql = "INSERT INTO "._DB_PREFIX_."cart_rule_product_rule_group (id_cart_rule, quantity) VALUES ('$id_cart_rule', 1)";
                Db::getInstance()->execute($sql);
                $id_group = (int)Db::getInstance()->Insert_ID();

                //Creating product rule
                $sql = "INSERT INTO "._DB_PREFIX_."cart_rule_product_rule (id_product_rule_group, type) VALUES ('$id_group', 'categories')";
                Db::getInstance()->execute($sql);
                $id_product_rule = (int)Db::getInstance()->Insert_ID();

                //Creating restrictions
                $values = array();
                foreach ($categories as $category) {
                    $category = (int)$category;
                    $values[] = "('$id_product_rule', '$category')";
                }
                $values = implode(',', $values);
                $sql = "INSERT INTO "._DB_PREFIX_."cart_rule_product_rule_value (id_product_rule, id_item) VALUES $values";
                Db::getInstance()->execute($sql);
            }



            // Register order(s) which contributed to create this voucher
            if (!LoyaltyModule::registerDiscount($cart_rule))
                $cart_rule->delete();

        }

        Tools::redirect($this->context->link->getModuleLink('loyalty', 'default', array('process' => 'summary')));
    }
}