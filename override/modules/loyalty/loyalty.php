<?php

/**
 * Created by PhpStorm.
 * User: sergg
 * Date: 12.12.2015
 * Time: 19:31
 */
class LoyaltyOverride extends Loyalty
{

    public function install()
    {
        if (!parent::install() || !$this->registerHook('actionOrderEdited'))
            return false;

        return true;
    }

    public function hookActionOrderEdited($params)
    {
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        if (!Validate::isLoadedObject($loyalty = new LoyaltyModule(LoyaltyModule::getByOrderId($params['order']->id))))
            return false;
        if ((int)Configuration::get('PS_LOYALTY_NONE_AWARD') && $loyalty->id_loyalty_state == LoyaltyStateModule::getNoneAwardId())
            return true;

        $loyalty->points = $this->getNbPointsByPrice($params['order']->total_products);

        $loyalty->save();
    }

    public function hookAdminCustomers($params)
    {
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        $customer = new Customer((int)$params['id_customer']);
        if ($customer && !Validate::isLoadedObject($customer))
            die($this->l('Incorrect Customer object.'));

        $details = LoyaltyModule::getAllByIdCustomer((int)$params['id_customer'], (int)$params['cookie']->id_lang);
        $points = (int)LoyaltyModule::getPointsByCustomer((int)$params['id_customer']);

        $html = '<div class="col-lg-12"><div class="panel">
			<div class="panel-heading">'.sprintf($this->l('Loyalty points (%d points)'), $points).'</div>';

        if (!isset($points) || count($details) == 0)
            return $html.' '.$this->l('This customer has no points').'</div></div>';

        $html .= '
		<div class="panel-body">
		<table cellspacing="0" cellpadding="0" class="table">
			<tr style="background-color:#F5E9CF; padding: 0.3em 0.1em;">
				<th>'.$this->l('Order').'</th>
				<th>'.$this->l('Carvari Order ID').'</th>
				<th>'.$this->l('Date').'</th>
				<th>'.$this->l('Total (without shipping)').'</th>
				<th>'.$this->l('Points').'</th>
				<th>'.$this->l('Points Status').'</th>
			</tr>';
        foreach ($details as $key => $loyalty)
        {
            $url = 'index.php?tab=AdminOrders&id_order='.$loyalty['id'].'&vieworder&token='.Tools::getAdminToken('AdminOrders'.(int)Tab::getIdFromClassName('AdminOrders').(int)$params['cookie']->id_employee);
            $html .= '
			<tr style="background-color: '.($key % 2 != 0 ? '#FFF6CF' : '#FFFFFF').';">
				<td>'.((int)$loyalty['id'] > 0 ? '<a style="color: #268CCD; font-weight: bold; text-decoration: underline;" href="'.$url.'">'.sprintf($this->l('#%d'), $loyalty['id']).'</a>' : '--').'</td>
				<td>'.(int)$loyalty['carvari_order_id'].'</td>
				<td>'.Tools::displayDate($loyalty['date']).'</td>
				<td>'.((int)$loyalty['id'] > 0 ? $loyalty['total_without_shipping'] : '--').'</td>
				<td>'.(int)$loyalty['points'].'</td>
				<td>'.$loyalty['state'].'</td>
			</tr>';
        }
        $html .= '
			<tr>
				<td>&nbsp;</td>
				<td colspan="3" class="bold" style="text-align: right;">'.$this->l('Total points available:').'</td>
				<td>'.$points.'</td>
				<td>'.$this->l('Voucher value:').' '.Tools::displayPrice(
                LoyaltyModule::getVoucherValue((int)$points, (int)Configuration::get('PS_CURRENCY_DEFAULT')),
                new Currency((int)Configuration::get('PS_CURRENCY_DEFAULT'))
            ).'</td>
			</tr>
		</table>
		</div>
		</div></div>';

        return $html;
    }


    /* Hook called when a new order is created */
    public function hookNewOrder($params)
    {
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        if (!Validate::isLoadedObject($params['customer']) || !Validate::isLoadedObject($params['order']))
            die($this->l('Missing parameters'));
        $loyalty = new LoyaltyModule();
        $loyalty->id_customer = (int)$params['customer']->id;
        $loyalty->id_order = (int)$params['order']->id;
        if (isset($params['from_ws'])) {

            if ($params['order']->bonus_used > 0) {
                $loyalty->points = (int)($this->getNbPointsByPrice($params['order']->total_paid) - $params['order']->bonus_used);
                if ($loyalty->points>0)
                    $loyalty->id_loyalty_state = LoyaltyStateModule::getValidationId();
                else
                    $loyalty->id_loyalty_state = LoyaltyStateModule::getCancelId();
            }
            else {
                $loyalty->points = $this->getNbPointsByPrice($params['order']->total_paid);
                $loyalty->id_loyalty_state = LoyaltyStateModule::getValidationId();
            }

        }
        else {
            $loyalty->points = LoyaltyModule::getOrderNbPoints($params['order']);
            if (!Configuration::get('PS_LOYALTY_NONE_AWARD') && (int)$loyalty->points == 0)
                $loyalty->id_loyalty_state = LoyaltyStateModule::getNoneAwardId();
            else
                $loyalty->id_loyalty_state = LoyaltyStateModule::getDefaultId();
        }

        return $loyalty->save();
    }

    /* Catch product returns and substract loyalty points */
    public function hookOrderReturn($params)
    {
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        if (isset($params['from_ws'])) {
            $total_price = $params['orderReturn']->return_sum;
        } else {
            $total_price = 0;
            $taxesEnabled = Product::getTaxCalculationMethod();
            $details = OrderReturn::getOrdersReturnDetail((int)$params['orderReturn']->id);
            foreach ($details as $detail)
            {
                if ($taxesEnabled == PS_TAX_EXC)
                    $total_price += Db::getInstance()->getValue('
                    SELECT ROUND(total_price_tax_excl, 2)
                    FROM '._DB_PREFIX_.'order_detail od
                    WHERE id_order_detail = '.(int)$detail['id_order_detail']);
                else
                    $total_price += Db::getInstance()->getValue('
                    SELECT ROUND(total_price_tax_incl, 2)
                    FROM '._DB_PREFIX_.'order_detail od
                    WHERE id_order_detail = '.(int)$detail['id_order_detail']);
            }
        }


        $loyalty_new = new LoyaltyModule();
        $loyalty_new->points = (int)(-1 * $this->getNbPointsByPrice($total_price));
        $loyalty_new->id_loyalty_state = (int)LoyaltyStateModule::getCancelId();
        $loyalty_new->id_customer = (int)$params['orderReturn']->id_customer;
        if (isset($params['from_ws'])) {
            $loyalty_new->id_order = (int)$params['orderReturn']->id;
        } else {
            $loyalty_new->id_order = (int)$params['orderReturn']->id_order;
        }

        if (!$params['orderReturn']->carvari_order_id) {
            $this->sendToCarvari($loyalty_new->id_customer, $loyalty_new->id_order, $loyalty_new->points);
        }

        $loyalty_new->save();
    }

    /* Hook called when an order change its status */
    public function hookUpdateOrderStatus($params)
    {
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        if (!Validate::isLoadedObject($params['newOrderStatus']))
            die($this->l('Missing parameters'));
        $new_order = $params['newOrderStatus'];
        $order = new Order((int)$params['id_order']);
        if ($order && !Validate::isLoadedObject($order))
            die($this->l('Incorrect Order object.'));
        $this->instanceDefaultStates();

        if ($new_order->id == $this->loyaltyStateValidation->id_order_state || $new_order->id == $this->loyaltyStateCancel->id_order_state)
        {
            if (!Validate::isLoadedObject($loyalty = new LoyaltyModule(LoyaltyModule::getByOrderId($order->id))))
                return false;
            if ((int)Configuration::get('PS_LOYALTY_NONE_AWARD') && $loyalty->id_loyalty_state == LoyaltyStateModule::getNoneAwardId())
                return true;

            if ($new_order->id == $this->loyaltyStateValidation->id_order_state){
                if ((int)$loyalty->points < 0)
                    $loyalty->points = abs((int)$loyalty->points);

                if(!$order->carvari_order_id && ($loyalty->id_loyalty_state == LoyaltyStateModule::getDefaultId())) {
                    $this->sendToCarvari($loyalty->id_customer, $loyalty->id_order, $loyalty->points);
                }
                $loyalty->id_loyalty_state = LoyaltyStateModule::getValidationId();
            }
            elseif ($new_order->id == $this->loyaltyStateCancel->id_order_state){
                if(!$order->carvari_order_id && ($loyalty->id_loyalty_state == LoyaltyStateModule::getValidationId())) {
                    $this->sendToCarvari($loyalty->id_customer, $loyalty->id_order, (-1)*$loyalty->points);
                }
                $loyalty->id_loyalty_state = LoyaltyStateModule::getCancelId();
                $loyalty->points = 0;
            }

            return $loyalty->save();
        }
        return true;
    }

    public function hookCancelProduct($params)
    {
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyStateModule.php');
        include_once(_PS_MODULE_DIR_.'/loyalty/LoyaltyModule.php');

        if (!Validate::isLoadedObject($params['order']) || !Validate::isLoadedObject($order_detail = new OrderDetail((int)$params['id_order_detail']))
            || !Validate::isLoadedObject($loyalty = new LoyaltyModule((int)LoyaltyModule::getByOrderId((int)$params['order']->id))))
            return false;

        $taxesEnabled = Product::getTaxCalculationMethod();
        $loyalty_new = new LoyaltyModule();
        if ($taxesEnabled == PS_TAX_EXC)
            $loyalty_new->points = -1 * LoyaltyModule::getNbPointsByPrice(number_format($order_detail->total_price_tax_excl, 2, '.', ''));
        else
            $loyalty_new->points = -1 * LoyaltyModule::getNbPointsByPrice(number_format($order_detail->total_price_tax_incl, 2, '.', ''));
        $loyalty_new->id_loyalty_state = (int)LoyaltyStateModule::getCancelId();
        $loyalty_new->id_order = (int)$params['order']->id;
        $loyalty_new->id_customer = (int)$loyalty->id_customer;
        $loyalty_new->add();

        if (!$params['order']->carvari_order_id) {
            $this->sendToCarvari($loyalty_new->id_customer, $loyalty_new->id_order, $loyalty_new->points);
        }

        return true;
    }

    public function getNbPointsByPrice($price)
    {
        /* Prevent division by zero */
        $points = 0;
        if ($pointRate = (float)(Configuration::get('PS_LOYALTY_POINT_RATE')))
            $points = floor(number_format($price, 2, '.', '') / $pointRate);

        return (int)$points;
    }

    public function sendToCarvari($id_customer, $id_order, $bonus){
        $carvari_card = Customer::getCarvariCardNumberById($id_customer);
        require_once(_PS_MODULE_DIR_.'/ffcarvaricard/override/classes/CarvariConnector.php');
        $connector = new CarvariConnector();
        $connector->postBonusUsed($carvari_card, $id_order, $bonus);
    }
}