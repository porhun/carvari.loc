<?php
class AdminStoresController extends AdminStoresControllerCore
{
    public function __construct()
    {
        parent::__construct();

        $this->img_dir = 'st';
        $this->multiple_fieldsets = true;
    }

    protected function get_image_url($id, $name){
        $image = _PS_STORE_IMG_DIR_.$id.'-'.$name.'.jpg';
        return ImageManager::thumbnail($image, $this->table.'_'.(int)$id.'-'.$name.'.'.$this->imageType, 350,
            $this->imageType, true, true);
    }
    protected function get_image_size($id, $name){
        $image = _PS_STORE_IMG_DIR_.$id.'-'.$name.'.jpg';
        return file_exists($image) ? filesize($image) / 1000 : false;
    }


    public function renderForm()
    {
        if (!($obj = $this->loadObject(true)))
            return;


//        d(ImageType::getImagesTypes('stores'));
        if ( Tools::getValue('deleteImg') ) {
            $images_allowed = array( 'image', 'image1', 'image2');
            if (in_array(Tools::getValue('deleteImg'), $images_allowed )) {
                $folder = _PS_IMG_DIR_.$this->img_dir.'/';
                $images_types = ImageType::getImagesTypes('stores');
                foreach ($images_types as $k => $image_type)
                {
                    unlink($folder.$obj->id.'-'.Tools::getValue('deleteImg').'-'.$image_type['name'].'.jpg');
                }
                unlink($folder.$obj->id.'-'.Tools::getValue('deleteImg').'.jpg');
                Tools::redirectAdmin(self::$currentIndex.'&'.$this->identifier.'='.$obj->id.'&updatestore&token='.$this->token);
            }


//            $image = _PS_STORE_IMG_DIR_.$obj->id.'-'.$name.'.jpg';
        }

        $image_url = $this->get_image_url( $obj->id, 'image');
        $image_url1 = $this->get_image_url( $obj->id, 'image1');
        $image_url2 = $this->get_image_url( $obj->id, 'image2');

        $image_size = $this->get_image_size( $obj->id, 'image');
        $image_size1 = $this->get_image_size( $obj->id, 'image1');
        $image_size2 = $this->get_image_size( $obj->id, 'image2');

        $tmp_addr = new Address();
        $res = $tmp_addr->getFieldsRequiredDatabase();
        $required_fields = array();
        foreach ($res as $row)
            $required_fields[(int)$row['id_required_field']] = $row['field_name'];

        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Stores'),
                'icon' => 'icon-home'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Name'),
                    'name' => 'name',
                    'required' => false,
                    'hint' => array(
                        $this->l('Store name (e.g. City Center Mall Store).'),
                        $this->l('Allowed characters: letters, spaces and %s')
                    )
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Address'),
                    'name' => 'address1',
                    'required' => true
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Address (2)'),
                    'name' => 'address2'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Zip/postal Code'),
                    'name' => 'postcode',
                    'required' => in_array('postcode', $required_fields)
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('City'),
                    'name' => 'city',
                    'required' => true
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('Country'),
                    'name' => 'id_country',
                    'required' => true,
                    'default_value' => (int)$this->context->country->id,
                    'options' => array(
                        'query' => Country::getCountries($this->context->language->id),
                        'id' => 'id_country',
                        'name' => 'name',
                    )
                ),
                array(
                    'type' => 'select',
                    'label' => $this->l('State'),
                    'name' => 'id_state',
                    'required' => true,
                    'options' => array(
                        'id' => 'id_state',
                        'name' => 'name',
                        'query' => null
                    )
                ),
                array(
                    'type' => 'latitude',
                    'label' => $this->l('Latitude / Longitude'),
                    'name' => 'latitude',
                    'required' => true,
                    'maxlength' => 12,
                    'hint' => $this->l('Store coordinates (e.g. 45.265469/-47.226478).')
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Phone'),
                    'name' => 'phone'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Fax'),
                    'name' => 'fax'
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Email address'),
                    'name' => 'email'
                ),
                array(
                    'type' => 'textarea',
                    'label' => $this->l('Note'),
                    'name' => 'note',
                    'cols' => 42,
                    'rows' => 4
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Status'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                    'hint' => $this->l('Whether or not to display this store.')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Picture').' 1',
                    'name' => 'image',
                    'display_image' => true,
                    'image' => $image_url ? $image_url : false,
                    'size' => $image_size,
                    'delete_url' => self::$currentIndex.'&'.$this->identifier.'='.$obj->id.'&updatestore&token='.$this->token.'&deleteImg=image',
                    'hint' => $this->l('Storefront picture.')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Picture').' 2',
                    'name' => 'image1',
                    'display_image' => true,
                    'image' => $image_url1 ? $image_url1 : false,
                    'size' => $image_size1,
                    'delete_url' => self::$currentIndex.'&'.$this->identifier.'='.$obj->id.'&updatestore&token='.$this->token.'&deleteImg=image1',
                    'hint' => $this->l('Storefront picture.')
                ),
                array(
                    'type' => 'file',
                    'label' => $this->l('Picture').' 3',
                    'name' => 'image2',
                    'display_image' => true,
                    'image' => $image_url2 ? $image_url2 : false,
                    'size' => $image_size2,
                    'delete_url' => self::$currentIndex.'&'.$this->identifier.'='.$obj->id.'&updatestore&token='.$this->token.'&deleteImg=image2',
                    'hint' => $this->l('Storefront picture.')
                )
            ),
            'hours' => array(
            ),
            'submit' => array(
                'title' => $this->l('Save'),
            )
        );

        if (Shop::isFeatureActive())
        {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association'),
                'name' => 'checkBoxShopAsso',
            );
        }

        $days = array();
        $days[1] = $this->l('Monday');
        $days[2] = $this->l('Tuesday');
        $days[3] = $this->l('Wednesday');
        $days[4] = $this->l('Thursday');
        $days[5] = $this->l('Friday');
        $days[6] = $this->l('Saturday');
        $days[7] = $this->l('Sunday');

        $hours = $this->getFieldValue($obj, 'hours');
        if (!empty($hours))
            $hours_unserialized = Tools::unSerialize($hours);

        $this->fields_value = array(
            'latitude' => $this->getFieldValue($obj, 'latitude') ? $this->getFieldValue($obj, 'latitude') : Configuration::get('PS_STORES_CENTER_LAT'),
            'longitude' => $this->getFieldValue($obj, 'longitude') ? $this->getFieldValue($obj, 'longitude') : Configuration::get('PS_STORES_CENTER_LONG'),
            'days' => $days,
            'hours' => isset($hours_unserialized) ? $hours_unserialized : false
        );

        return AdminControllerCore::renderForm();
    }

    protected function postImage($id)
    {
//		d($_FILES);
        $ret = true;
        $generate_hight_dpi_images = (bool)Configuration::get('PS_HIGHT_DPI');

        foreach ($_FILES as $filed_name=>$file) {
            if(!$file['error']){
                $ret &= $this->uploadImage($id.'-'.$filed_name, $filed_name, $this->img_dir.'/');

                $images_types = ImageType::getImagesTypes('stores');
                foreach ($images_types as $k => $image_type)
                {
                    ImageManager::resize(_PS_STORE_IMG_DIR_.$id.'-'.$filed_name.'.jpg',
                        _PS_STORE_IMG_DIR_.$id.'-'.$filed_name.'-'.stripslashes($image_type['name']).'.jpg',
                        (int)$image_type['width'], (int)$image_type['height']
                    );

                    if ($generate_hight_dpi_images)
                        ImageManager::resize(_PS_STORE_IMG_DIR_.$id.'-'.$filed_name.'.jpg',
                            _PS_STORE_IMG_DIR_.$id.'-'.$filed_name.'-'.stripslashes($image_type['name']).'2x.jpg',
                            (int)$image_type['width']*2, (int)$image_type['height']*2
                        );
                }

            }
        }
        return $ret;
    }

}