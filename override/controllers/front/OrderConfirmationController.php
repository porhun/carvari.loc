<?php

class OrderConfirmationController extends OrderConfirmationControllerCore
{
    public $ssl = true;
    public $php_self = 'order-confirmation';
    public $id_cart;
    public $id_module;
    public $id_order;
    public $reference;
    public $secure_key;

    /**
     * Initialize order confirmation controller
     * @see FrontController::init()
     */
    public function init()
    {
        FrontController::init();

        $this->id_cart = (int)(Tools::getValue('id_cart', 0));
        $is_guest = false;

        /* check if the cart has been made by a Guest customer, for redirect link */
        if (Cart::isGuestCartByCartId($this->id_cart)) {
            $is_guest = true;
            $redirectLink = 'index.php?controller=guest-tracking';
        } else {
            $redirectLink = 'index.php?controller=history';
        }

        $this->id_module = (int)(Tools::getValue('id_module', 0));
        $this->id_order = Order::getOrderByCartId((int)($this->id_cart));
        $this->secure_key = Tools::getValue('key', false);
        $order = new Order((int)($this->id_order));
        if ($is_guest) {
            $customer = new Customer((int)$order->id_customer);
            $redirectLink .= '&id_order='.$order->reference.'&email='.urlencode($customer->email);
        }
        if (!$this->id_order || !$this->id_module || !$this->secure_key || empty($this->secure_key)) {
            Tools::redirect($redirectLink.(Tools::isSubmit('slowvalidation') ? '&slowvalidation' : ''));
        }
        $this->reference = $order->reference;
        if (!Validate::isLoadedObject($order) || $order->id_customer != $this->context->customer->id || $this->secure_key != $order->secure_key) {
            Tools::redirect($redirectLink);
        }
        $module = Module::getInstanceById((int)($this->id_module));
        if ($order->module != $module->name) {
            Tools::redirect($redirectLink);
        }
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $order = new Order($this->id_order);
        $cart = new Cart($this->id_cart);
        $products = $order->getProducts();
        $total = (float)$cart->getOrderTotal(true, Cart::BOTH);
        $customer = new Customer($order->id_customer);

        $customizedDatas = Product::getAllCustomizedDatas((int)$order->id_cart);
        Product::addCustomizationPrice($products, $customizedDatas);
        OrderReturn::addReturnedQuantity($products, $order->id);


        $this->context->smarty->assign(array(
            'order' => $order,
            'products' => $products,
            'discounts' => $order->getCartRules(),
            'use_tax' => Configuration::get('PS_TAX'),
            'group_use_tax' => (Group::getPriceDisplayMethod($customer->id_default_group) == PS_TAX_INC),
            'link' => $this->context->link,
            'total' => $total,
            'img_url' => _PS_BASE_URL_._THEME_PROD_DIR_,
            'cart_qties' => 0,  // set cart products number to 0 because customer has already purchased it successfully
        ));

        $this->setTemplate(_PS_THEME_DIR_.'order-confirmation_f.tpl');
    }


}
