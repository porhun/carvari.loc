<?php
class StoresController extends StoresControllerCore
{
    public function initContent()
    {
        $stores = array();

        FrontController::initContent();

        $result = Db::getInstance()->executeS('
			SELECT s.*, cl.name country, st.iso_code state
			FROM '._DB_PREFIX_.'store s
			'.Shop::addSqlAssociation('store', 's').'
			LEFT JOIN '._DB_PREFIX_.'country_lang cl ON (cl.id_country = s.id_country)
			LEFT JOIN '._DB_PREFIX_.'state st ON (st.id_state = s.id_state)
			WHERE s.active = 1 AND cl.id_lang = '.(int)$this->context->language->id);


        $i = 0;
        $store_name = '';
        foreach ($result as $item) {
            if ($i == 0)
                $store_name = $item['city'];
            $item['hours_uns'] = unserialize($item['hours']);
            $item['img'] = $this->get_image($item['id_store'], 'image', true);
            $item['img1'] = $this->get_image($item['id_store'], 'image1', true);
            $item['img2'] = $this->get_image($item['id_store'], 'image2', true);
            $item['imgfull'] = $this->get_image($item['id_store'], 'image');
            $item['imgfull1'] = $this->get_image($item['id_store'], 'image1');
            $item['imgfull2'] = $this->get_image($item['id_store'], 'image2');
            $stores[$item['city']][] = $item;
            $i++;
        }

//        d($stores);

        $this->context->smarty->assign(array(
            'stores_by_city' => $stores,
            'store_name' => $store_name,
        ));

        $this->setTemplate(_PS_THEME_DIR_.'stores.tpl');
    }

    public function setMedia()
    {
        FrontController::setMedia();
//        $this->addCSS(_THEME_CSS_DIR_.'stores_ff.css');
//        $this->addCSS(_PS_JS_DIR_.'jquery/fancybox/jquery.fancybox.css');
//        $this->addJS(_PS_JS_DIR_.'jquery/fancybox/jquery.fancybox.css');
        $this->addJS(_THEME_JS_DIR_.'map.js');

        $this->addJqueryPlugin('slickslider');
    }


    protected function get_image($id, $name, $thumb = false){
        $image = _PS_STORE_IMG_DIR_.$id.'-'.$name.'.jpg';
        $image_origin_src = '/img/st/'.$id.'-'.$name.'.jpg';
        $image_thumb_src = '/img/st/'.$id.'-'.$name.'-store_thumb.jpg';
//        $image_src =  '/img/st/'.$id.'-'.$name.'-medium_default.jpg';
//        $html = '<a class="fancybox" rel="group" href="'.$image_origin_src.'"><img src="'.$image_src.'" alt="" /></a>';
        if ( $thumb ){
            return file_exists($image) ? $image_thumb_src : false;
        }else{
            return file_exists($image) ? $image_origin_src : false;
        }
//        return ImageManager::thumbnail($image, 'store_'.(int)$id.'-'.$name.'-medium_default.jpg', 350, 'jpg', true, true);
    }

    protected function displayAjax()
    {

        $stores = $this->getStores();
        $parnode = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><markers></markers>');

        foreach ($stores as $store)
        {
            $other = '';
            $newnode = $parnode->addChild('marker');
            $newnode->addAttribute('name', $store['name']);
            $address = $this->processStoreAddress($store);

            $other .= $this->renderStoreWorkingHours($store);
            $newnode->addAttribute('addressNoHtml', strip_tags(str_replace('<br />', ' ', $address)));
            $newnode->addAttribute('address', $address);
            $newnode->addAttribute('other', $other);
            $newnode->addAttribute('phone', $store['phone']);
            $newnode->addAttribute('id_store', (int)$store['id_store']);
            $newnode->addAttribute('has_store_picture', file_exists(_PS_STORE_IMG_DIR_.(int)$store['id_store'].'-image.jpg'));
            $newnode->addAttribute('store_picture', $this->get_image($store['id_store'], 'image') );
            $newnode->addAttribute('store_picture1', $this->get_image($store['id_store'], 'image1') );
            $newnode->addAttribute('store_picture2', $this->get_image($store['id_store'], 'image2') );
            $newnode->addAttribute('lat', (float)$store['latitude']);
            $newnode->addAttribute('lng', (float)$store['longitude']);
            if (isset($store['distance']))
                $newnode->addAttribute('distance', (int)$store['distance']);
        }

        header('Content-type: text/xml');
        die($parnode->asXML());
    }
}