<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class HistoryController extends HistoryControllerCore
{

	public $limit_mobile = 3;

	public function init() {

		parent::init();

		if ($this->ajax) {
			echo $this->ajaxCall();
			exit;
		}

	}

	public function initContent()
	{
		FrontController::initContent();

        $orders_all = Order::getCustomerOrders( $this->context->customer->id , false, null, null, false, true);
		if ($orders = Order::getCustomerOrders( $this->context->customer->id , false, null, null, $this->limit_mobile, true))
			foreach ($orders as &$order)
			{
				$myOrder = new Order((int)$order['id_order']);
				if (Validate::isLoadedObject($myOrder))
					$order['virtual'] = $myOrder->isVirtual(false);


				$order['order_details'] = $myOrder->getProductsDetail();
			}

		$this->context->smarty->assign(array(
			'orders' => $orders,
            'orders_all' => count($orders_all),
            'limit_mobile' => $this->limit_mobile,
			'invoiceAllowed' => (int)Configuration::get('PS_INVOICE'),
			'reorderingAllowed' => !(bool)Configuration::get('PS_DISALLOW_HISTORY_REORDERING'),
			'slowValidation' => Tools::isSubmit('slowvalidation')
		));

		$this->context->smarty->assign('HOOK_CUSTOMER_ACCOUNT', Hook::exec('displayCustomerAccount'));

		$this->setTemplate(_PS_THEME_DIR_.'history.tpl');

	}

	public function ajaxCall()
	{
		$offset = Tools::getValue('offset');


		if ($orders = Order::getCustomerOrders($this->context->customer->id , false, null, $offset, $this->limit_mobile))
			foreach ($orders as &$order)
			{
				$myOrder = new Order((int)$order['id_order']);
				if (Validate::isLoadedObject($myOrder))
					$order['virtual'] = $myOrder->isVirtual(false);

				$address = new Address((int)$order['id_address_delivery']);
				$order['address'] = $address->address1;

				$order['order_details'] = $myOrder->getProductsDetail();
			}
            $quanity = count($orders)>=$this->limit_mobile ? true : false;
		$this->context->smarty->assign(array(
			'ajax_call'		=> 1,
			'orders'		=> $orders,
		));


		return Tools::jsonEncode(array('orders_list' => $this->context->smarty->fetch(_PS_THEME_MOBILE_DIR_.'history.tpl'),'continue'=>$quanity));
	}
}
