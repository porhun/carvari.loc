<?php

/**
 * Created by PhpStorm.
 * User: shandur
 * Date: 13.12.16
 * Time: 11:54
 */
class PasswordController extends PasswordControllerCore
{
    /**
     * Start forms process
     * @see FrontController::postProcess()
     */
    public function postProcess()
    {
        if (Tools::isSubmit('phone')) {
            $phone = Tools::GetValue('phone');
            $phone = preg_replace('/[. +()-]*/','',$phone);
            $phone = '+'.$phone;
            if ( !Validate::isPhoneNumber($phone) ) {
                $this->errors['phone'] = Tools::displayError('Неверный формат телефона.');
            } else {
                $customer = new Customer();
                $customer->getByPhone($phone);
                if (!Validate::isLoadedObject($customer)) {
                    $this->errors['phone'] = Tools::displayError('Пользователя с таким телефоном не существует.');
                } elseif (!$customer->active) {
                    $this->errors['phone'] = Tools::displayError('You cannot regenerate the password for this account.');
                } elseif ((strtotime($customer->last_passwd_gen.'+'.($min_time = (int)Configuration::get('PS_PASSWD_TIME_FRONT')).' minutes') - time()) > 0) {
                    $this->errors['phone'] = sprintf(Tools::displayError('You can regenerate your password only every %d minute(s)'), (int)$min_time);
                } else {
                    $pin = Customer::generatePin();

                    if (Module::isEnabled('ffsmsinfo')) {
                        $sms = new SmsService();
                        if ( $sms->sendSMS('Пароль для входа : '.$pin, $phone) ) {
//                            $customer->setWsPasswd($pin);
//                            $customer->save();
                            Customer::updatePasswordByPhone( $phone, $pin );
//                            $this->context->smarty->assign(array('confirmation' => 1, 'customer_phone' => $phone));
                            Tools::redirect('/authentication?'.http_build_query(['password_reset' => 1, 'login_phone' => $phone]));
//                            Tools::redirect('/authentication?login_phone='.$phone);
                        } else {
                            $this->errors['phone'] = Tools::displayError('Виникла помилка при відправці СМС.');
                        }
                    } else {
                        $this->errors['phone'] = Tools::displayError('Виникла помилка при відправці СМС.');
                    }

                }
            }
        } elseif (($token = Tools::getValue('token')) && ($id_customer = (int)Tools::getValue('id_customer'))) {
            $email = Db::getInstance()->getValue('SELECT `email` FROM '._DB_PREFIX_.'customer c WHERE c.`secure_key` = \''.pSQL($token).'\' AND c.id_customer = '.(int)$id_customer);
            if ($email) {
                $customer = new Customer();
                $customer->getByEmail($email);
                if (!Validate::isLoadedObject($customer)) {
                    $this->errors['email'] = Tools::displayError('Customer account not found');
                } elseif (!$customer->active) {
                    $this->errors['email'] = Tools::displayError('You cannot regenerate the password for this account.');
//                } elseif ((strtotime($customer->last_passwd_gen.'+'.(int)Configuration::get('PS_PASSWD_TIME_FRONT').' minutes') - time()) > 0) {
//                    Tools::redirect('index.php?controller=authentication&error_regen_pwd');
                } else {
                    $customer->passwd = Tools::encrypt($password = Tools::passwdGen(MIN_PASSWD_LENGTH, 'ALPHANUMERIC'));
                    $customer->last_passwd_gen = date('Y-m-d H:i:s', time());
                    if ($customer->update()) {
                        Hook::exec('actionPasswordRenew', array('customer' => $customer, 'password' => $password));
                        $mail_params = array(
                            '{email}' => $customer->email,
                            '{phone_login}' => $customer->phone_login,
                            '{lastname}' => $customer->lastname,
                            '{firstname}' => $customer->firstname,
                            '{passwd}' => $password
                        );
                        if (Mail::Send($this->context->language->id, 'password', Mail::l('Your new password'), $mail_params, $customer->email, $customer->firstname.' '.$customer->lastname)) {
                            $this->context->smarty->assign(array('confirmation' => 1, 'customer_email' => $customer->email));
                        } else {
                            $this->errors['email'] = Tools::displayError('An error occurred while sending the email.');
                        }
                    } else {
                        $this->errors['email'] = Tools::displayError('An error occurred with your account, which prevents us from sending you a new password. Please report this issue using the contact form.');
                    }
                }
            } else {
                $this->errors['phone'] = Tools::displayError('We cannot regenerate your password with the data you\'ve submitted.');
            }
        } elseif (Tools::getValue('token') || Tools::getValue('id_customer')) {
            $this->errors['phone'] = Tools::displayError('We cannot regenerate your password with the data you\'ve submitted.');
        }
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        if ( Tools::getValue('login_phone') ) {
            $this->context->smarty->assign(array(
                'login_phone' => '+'.trim(Tools::getValue('login_phone'))
            ));
        }
    }
}