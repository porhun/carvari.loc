<?php
/*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class MyAccountController extends MyAccountControllerCore
{
	/** @var Customer */
	protected $customer;

	public function init()
	{
		parent::init();
		$this->customer = $this->context->customer;
	}

	public function setMedia()
	{
		FrontController::setMedia();
		$this->addJS(_PS_JS_DIR_.'validate.js');
//		$this->addCSS(_THEME_CSS_DIR_.'my-account.css');
	}


	public function postProcess() {
		if (Tools::isSubmit('submitAccount'))
		{
			if ($this->context->customer->isLogged() && !$this->isTokenValid())
				$this->errors[] = Tools::displayError('Invalid token.');

			if (Tools::getValue('phone_login'))
				$this->errors[] = Tools::displayError('Phone number is prohibited to change.');

			if (Tools::getValue('months') != '' && Tools::getValue('days') != '' && Tools::getValue('years') != '') {
				$this->customer->birthday = (int)Tools::getValue('years').'-'.(int)Tools::getValue('months').'-'.(int)Tools::getValue('days');
			} elseif (Tools::getValue('months') == '' && Tools::getValue('days') == '' && Tools::getValue('years') == '') {
				$this->customer->birthday = null;
			} else {
				$this->errors[] = Tools::displayError('Invalid date of birth.');
			}


			$email = trim(Tools::getValue('email'));
//			$phone_login = trim(Tools::getValue('phone_login'));

			if (Tools::getIsset('old_passwd'))
				$old_passwd = trim(Tools::getValue('old_passwd'));

			if (!Validate::isEmail($email))
				$this->errors[] = Tools::displayError('This email address is not valid');
//			elseif ($this->customer->phone_login != $phone_login && Customer::isCustomerExistsByPhone($phone_login))
//				$this->errors[] = Tools::displayError('Користувач з таким телефоном вже зареєстрован.');
			elseif ($this->customer->email != $email && Customer::customerExists($email, true))
				$this->errors[] = Tools::displayError('An account using this email address has already been registered.');
			elseif (!Tools::getIsset('old_passwd') || (Tools::encrypt($old_passwd) != $this->context->cookie->passwd))
				$this->errors[] = Tools::displayError('The password you entered is incorrect.');
			else
			{
				$prev_id_default_group = $this->customer->id_default_group;

				// Merge all errors of this file and of the Object Model
				$this->errors = array_merge($this->errors, $this->customer->validateController());
			}



			//			Address
			$post_addr = Tools::getValue('address');

			$addresses = $this->context->customer->getAddresses($this->context->language->id);

			if ($addresses) {
				$address = new Address($addresses['0']['id_address']);
			} else {
				$address = new Address();
				$address->id_customer = $this->customer->id;
				$address->id_country = $this->context->country->id;
			}
//			$address->phone_mobile = pSQL($phone_login);
			$address->id_state = (int)$post_addr['id_state'];
			$address->city = pSQL($post_addr['city']);
			$address->address1 = pSQL($post_addr['street'].', '.$post_addr['house'].', '.$post_addr['flat']);
			$address->postcode = pSQL(Tools::getValue('postcode') ? Tools::getValue('postcode') : '00000');

//			d($address);
//			d($this->context);
			$this->errors = array_merge($this->errors, $address->validateController());


			if (!count($this->errors))
			{
				$this->customer->id_default_group = (int)$prev_id_default_group;
				$this->customer->firstname = Tools::ucwords($this->customer->firstname);

				if (Tools::getValue('passwd'))
					$this->context->cookie->passwd = $this->customer->passwd;
				if ($this->customer->update())
				{
					$this->context->cookie->customer_lastname = $this->customer->lastname;
					$this->context->cookie->customer_firstname = $this->customer->firstname;
					$this->context->smarty->assign('confirmation', 1);
				}
				else
					$this->errors[] = Tools::displayError('The information cannot be updated.');


				if( $address->save() ){
					$this->context->cart->autosetProductAddress();
					$this->context->cart->update();
				}

				Tools::redirect('/my-account');
			}

		}
	}

	/**
	 * Assign template vars related to page content
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();

		if ($this->customer->birthday) {
			$birthday = explode('-', $this->customer->birthday);
		} else {
			$birthday = array('-', '-', '-');
		}

		$addresses = $this->context->customer->getAddresses($this->context->language->id);
		$this->context->smarty->assign(array(
			'years' => Tools::dateYears(),
			'ulogin_new_registration' => Tools::getValue('ulogin_new_registration', 0),
			'sl_year' => $birthday[0],
			'months' => Tools::dateMonths(),
			'sl_month' => $birthday[1],
			'days' => Tools::dateDays(),
			'sl_day' => $birthday[2],
			'customer' => $this->context->customer,
			'customer_address' => $addresses[0],
			'states' => State::getStatesByIdCountry($this->context->country->id),
			'address1' => array_map('trim' ,explode(',',$addresses[0]['address1'])),
			'token' => Tools::getToken(false),
		));

	}
}

