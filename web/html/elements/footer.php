</div>
<div class="footer_wrapper">
        <div class="footer">
        <div class="min_width">
            <div class="footer_top clearfix">
                <div class="left_foot_block">
                    <a href="#" class="logo"></a>
                    <div class="subscribe_block">
                        <span class="">Подпишіться на наші новини</span>
                        <input type="text" placeholder="Email" class="email_input">
                        <button class="sub_button">
                            <i class="icon">
                                <svg>
                                    <use xlink:href="#subscribe_icon">
                                </svg>
                            </i>
                        </button>
                    </div>
                </div>
                <div class="foot_menu">
                    <ul>
                        <li><a href="#">Про магазин</a></li>
                        <li><a href="#">Контакти</a></li>
                        <li><a href="#">Оплата</a></li>
                        <li><a href="#">Гарантія</a></li>
                    </ul>
                    <ul>
                        <li><a href="#">Про магазин</a></li>
                        <li><a href="#">Контакти</a></li>
                        <li><a href="#">Оплата</a></li>
                        <li><a href="#">Гарантія</a></li>
                    </ul>
                </div>
                <div class="foot_right_block">
                    <div class="phone_number">
                        0 800 999-99-99
                    </div>
                    <div class="footer_social_list">
                        <ul>
                            <li>
                                <a href="#" class="social fb">
                                    <i class="icon">
                                        <svg>
                                            <use xlink:href="#facebook_icon">
                                        </svg>
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="social vk">
                                    <i class="icon">
                                        <svg>
                                            <use xlink:href="#vk_icon">
                                        </svg>
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="social tt">
                                    <i class="icon">
                                        <svg>
                                            <use xlink:href="#tt_icon">
                                        </svg>
                                    </i>
                                </a>
                            </li>
                            <li>
                                <a href="#" class="social yt">
                                    <i class="icon">
                                        <svg>
                                            <use xlink:href="#yt_icon">
                                        </svg>
                                    </i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <a class="footer_lang_control" href="#">Російською</a>
                </div>
            </div>
            <div class="copyrights">
                <div class="copyright_text">
                    © 2015, L` Carvari — мережа фірмових магазинів взуття та аксесуарів. Всі права на матеріали, розміщені на сайті, охороняються відповідно до законодавства України.
                    Матеріали не можуть бути використані без письмового дозволу ФОП Подопригора.
                </div>
                <div class="forforce">
                    Зроблено в
                    <a href="#"></a>
                </div>
            </div>
        </div>
    </div>
    </div>

    </div> <!-- Body wrapper-->
    </body>
</html>