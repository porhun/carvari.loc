<? include "elements/header.php" ?>

<div class="main_content checkout thx">
        <div class="left_content">
            <h1>Вдячні за покупку!</h1>
            <h1>Замовлення успішно оформлено №2235423 на суму 7 500 грн.</h1>
            <div class="thx_text">Протягом години наш менеджер вам зателефонує.</div>
            <div class="btn_holder">
                <input type="button" class="button big mt50" value="Продовжити покупки">
            </div>
        </div>
        <div class="right_content">
            <h1>Замовили</h1>
            <div class="order_list">
                <ul class="order_items">
                    <li>
                        <div class="item">
                            <div class="item_img">
                                <a href="#">
                                    <img src="../img/main_page_content/order_item.png" alt="">
                                </a>
                            </div>
                            <div class="item_descr">
                                <div class="title"><a href="#">Мокасини жЫночЫ</a></div>
                                <div class="code">JB007</div>
                                <div class="size">Розмір:45.5</div>
                                <div class="price">1 482 грн</div>
                            </div>
                            <div class="delete_item">
                                <i class="icon">
                                    <svg>
                                        <use xlink:href="#close_icon">
                                    </svg>
                                </i>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="item_img">
                                <a href="#">
                                    <img src="../img/main_page_content/order_item.png" alt="">
                                </a>
                            </div>
                            <div class="item_descr">
                                <div class="title"><a href="#">Мокасини жЫночЫ</a></div>
                                <div class="code">JB007</div>
                                <div class="size">Розмір:45.5</div>
                                <div class="price">1 482 грн</div>
                            </div>
                            <div class="delete_item">
                                <i class="icon">
                                    <svg>
                                        <use xlink:href="#close_icon">
                                    </svg>
                                </i>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="history_link">
                <a href="#">Історія і статус замовлень</a>
            </div>
        </div>
</div>

<? include "elements/footer.php" ?>
