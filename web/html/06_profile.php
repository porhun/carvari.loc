<? include "elements/header.php" ?>
    <div class="main_content profile">
            <div class="clearfix">
                <h1>Мій аккаунт</h1>
                <a class="logout_link" href="#">Вийти</a>
            </div>
            <div class="profile_nav">
                <span data-tab="tab1" class="profile_nav_link active">Персональні дані
                    <span class="underline"></span>
                </span>
                <span data-tab="tab2" class="profile_nav_link">Статуси замовлень
                    <span class="underline"></span>
                </span>
                <span data-tab="tab3" class="profile_nav_link">Карта Carvari
                    <span class="underline"></span>
                </span>
            </div>
            <div class="tab_wrapper">
                <div id="tab1" class="tab_content">
                    <div class="input_block">
                        <label>Ім'я</label>
                        <input type="text">
                    </div>
                    <div class="input_block">
                        <label>Прізвище</label>
                        <input type="text">
                    </div>
                    <div class="input_block">
                        <label>Телефон</label>
                        <input type="text">
                    </div>
                    <div class="input_block">
                        <label>E-mail</label>
                        <input type="text">
                    </div>
                    <div class="input_block">
                        <label>Пароль</label>
                        <input type="password">
                    </div>
                    <div class="profile_section_title">
                        Адреса для точної доставки
                    </div>
                    <div class="input_block">
                        <label>Область</label>
                        <select>
                            <option>Київська область</option>
                            <option>Дніпропетровська область</option>
                            <option>Харківська область</option>
                            <option>Житомирська область</option>
                        </select>
                    </div>
                    <div class="input_block">
                        <label>Місто</label>
                        <select>
                            <option>Київ</option>
                            <option>Дніпропетровськ</option>
                            <option>Харків</option>
                            <option>Житомир</option>
                        </select>
                    </div>
                    <div class="input_block">
                        <label>Вулиця</label>
                        <input type="text">
                    </div>
                    <div class="input_block">
                        <label>Будинок</label>
                        <input type="text">
                    </div>
                    <div class="input_block">
                        <label>Квартира</label>
                        <input type="text">
                    </div>
                    <div class="btn_holder">
<!--                        remove class disable to undisable, disable not work at all, just style-->
                        <input type="button" class="button disabled big" value="Зберегти зміни">
                    </div>
                </div>
                <div id="tab2" class="tab_content" style="display: none;">
                    <table class="order_status">
                        <thead>
                            <tr>
                                <th width="90">Дата</th>
                                <th width="165">Вид</th>
                                <th width="110">Артикул</th>
                                <th width="110">Сума, грн</th>
                                <th width="315">Адреса</th>
                                <th width="90">Статус</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td width="90">19.07.15</td>
                                <td width="165">Балетки жіночі<br>
                                    Чоботи<br>
                                    Ботильйони жіночі<br>
                                    Балетки жіночі<br>
                                </td>
                                <td width="110">2132158<br>213`223131<br>32dgsdfsd<br>asdasd</td>
                                <td width="110">4 750</td>
                                <td width="315">вул. Героїв Гражданської Війни, 17\20</td>
                                <td width="90">В обробці</td>
                            </tr>
                            <tr>
                                <td width="90">22.03.14</td>
                                <td width="165">Туфлі жіночі</td>
                                <td width="110">А125876</td>
                                <td width="110">1 111</td>
                                <td width="315">вул. Героїв Гражданської Війни, 17\20</td>
                                <td width="90">Доставлено</td>
                            </tr>
                            <tr>
                                <td width="90">19.07.15</td>
                                <td width="165">Сапоги</td>
                                <td width="110">99989898</td>
                                <td width="110">13 666</td>
                                <td width="315">вул. Героїв Гражданської Війни, 17\20</td>
                                <td width="90">Доставлено</td>
                            </tr>
                            <tr>
                                <td width="90">19.07.15</td>
                                <td width="165">Балетки жіночі</td>
                                <td width="110">2132158</td>
                                <td width="110">4 750</td>
                                <td width="315">вул. Героїв Гражданської Війни, 17\20</td>
                                <td width="90">Доставлено</td>
                            </tr>
                            <tr>
                                <td width="90">11.11.11</td>
                                <td width="165">Ботильйони</td>
                                <td width="110">2222222</td>
                                <td width="110">333</td>
                                <td width="315">вул. Героїв Гражданської Війни, 17\20</td>
                                <td width="90">Доставлено</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div id="tab3" class="tab_content" style="display: none;">
                    <div class="carvari_card">
                        <div class="card_number">5823&nbsp;&nbsp;&nbsp;4905&nbsp;&nbsp;&nbsp;6007&nbsp;&nbsp;&nbsp;1873</div>
                        <div class="holder_name">Константин Константинопольский</div>
                        <div class="holder_phone">8(097) 653-03-72</div>
                    </div>
                    <div class="blogs_list">
                        <a href="#" class="blog_link">
                            <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                            <div class="blog_title">Модне взуття весна-літо 2015</div>
                            <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                            <div class="blog_date">Новини, 29.07.2015</div>
                        </a>
                        <a href="#" class="blog_link">
                            <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                            <div class="blog_title">Модне взуття весна-літо 2015</div>
                            <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                            <div class="blog_date">Новини, 29.07.2015</div>
                        </a>
                        <a href="#" class="blog_link">
                            <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                            <div class="blog_title">Модне взуття весна-літо 2015</div>
                            <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                            <div class="blog_date">Новини, 29.07.2015</div>
                        </a>
                        <a href="#" class="blog_link">
                            <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                            <div class="blog_title">Модне взуття весна-літо 2015</div>
                            <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                            <div class="blog_date">Новини, 29.07.2015</div>
                        </a>
                        <a href="#" class="blog_link">
                            <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                            <div class="blog_title">Модне взуття весна-літо 2015</div>
                            <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                            <div class="blog_date">Новини, 29.07.2015</div>
                        </a>
                        <a href="#" class="blog_link">
                            <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                            <div class="blog_title">Модне взуття весна-літо 2015</div>
                            <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                            <div class="blog_date">Новини, 29.07.2015</div>
                        </a>
                    </div>
                </div>
            </div>
    </div>
<? include "elements/footer.php" ?>
