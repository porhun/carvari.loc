<? include "elements/header.php" ?>
    <div class="main_content product-page">
            <div class="row row-full this_product">
                <div class="col-md-4 left_column">
                    <div class="navigation fbc clearfix">
                        <a href="#" class="follow_back">Назад</a>

                        <div class="fade_bread_crumbs" style="display: none">
                            <ul class="clearfix">
                                <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a href="#" itemprop="url"><span itemprop="title">Жiнкам</span></a>
                                </li>
                                <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <a href="#" itemprop="url"><span itemprop="title">Босонiжки</span></a>
                                </li>
                                <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                                    <span itemprop="title">Босонiжки WH1506A99KA1026TI</span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="product_image">
                        <a href="../img/sample/bashmak.png">
                            <img src="../img/sample/bashmak.png" alt=""/>
                        </a>
                    </div>

                    <div class="product_images">
                        <div class="slider">
                            <div class="slide">
                                <a href="../img/sample/bashmak.png" data-ind="0" class="active gallery-link">
                                    <img src="../img/sample/tapok1.jpg" alt=""/>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="../img/sample/tapok2.jpg" data-ind="1" class="gallery-link">
                                    <img src="../img/sample/tapok2.jpg" alt=""/>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="../img/sample/bashmak.jpg" data-ind="2" class="gallery-link">
                                    <img src="../img/sample/bashmak.jpg" alt=""/>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="../img/sample/tapok4.jpg" data-ind="3" class="gallery-link">
                                    <img src="../img/sample/tapok4.jpg" alt=""/>
                                </a>
                            </div>
                            <div class="slide">
                                <a href="../img/sample/bashmak.png" data-ind="4" class="gallery-link">
                                    <img src="../img/sample/tapok1.jpg" alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 right_column">
                    <div class="product_head">
                        <h1>Босонiжки жiночi</h1>
                        <div class="price_block">
                            <div class="price inline-block">1 937 грн</div>
                            <div class="old_price inline-block">2 265 грн</div>
                        </div>
                    </div>

                    <div class="sizes_block">
                        <div class="title-md">Розмiр:</div>
                        <ul class="clearfix">
                            <li><a href="#">33,5</a></li>
                            <li><a href="#">34</a></li>
                            <li><a href="#">35</a></li>
                            <li class="active"><a href="#">35,5</a></li>
                            <li><a href="#">36,5</a></li>
                            <li><a href="#">37</a></li>
                            <li><a href="#">38</a></li>
                        </ul>
                    </div>

                    <div class="order_button">
<!--                        <div class="m-b-sm"><a href="#" class="button btn-lg" id="buy_product" data-inbasket-translate="У кошику">Замовити</a></div>-->
                        <div class="m-b-sm"><a href="#" class="button btn-lg" id="buy_product" data-inbasket-translate="У кошику">Замовити</a></div>
                    </div>

                    <div class="attr_block m-b-lg">
                        <div class="title-md">Деталi:</div>
                        <div class="attr_list">
                            <div class="row">
                                <div class="col-md-1">Артикул:</div><div class="col-md-1">WH1506A99KA1026TI</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">Сезон:</div><div class="col-md-1">Весна-Лето 2015</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">Стиль:</div><div class="col-md-1">Casual/Comfort</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">Страна:</div><div class="col-md-1">Италия</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">Подкладка:</div><div class="col-md-1">искусственная кожа</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">Основной материал:</div><div class="col-md-1">резина</div>
                            </div>
                            <div class="row">
                                <div class="col-md-1">Подошва:</div><div class="col-md-1">резина</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-full related_products">
                <div class="col-md-6">
                    <div class="related_title">
                        Схожi моделi
                    </div>

                    <div class="slider">
                        <div class="slide">
                            <a href="#">
                                <img src="../img/sample/big_tapok1.jpg" alt=""/>
                                <span>7 500 грн</span>
                            </a>
                        </div>

                        <div class="slide">
                            <a href="#">
                                <img src="../img/sample/big_tapok2.jpg" alt=""/>
                                <span>7 500 грн</span>
                            </a>
                        </div>

                        <div class="slide">
                            <a href="#">
                                <img src="../img/sample/big_tapok3.jpg" alt=""/>
                                <span>7 500 грн</span>
                            </a>
                        </div>

                        <div class="slide">
                            <a href="#">
                                <img src="../img/sample/big_tapok1.jpg" alt=""/>
                                <span>7 500 грн</span>
                            </a>
                        </div>

                        <div class="slide">
                            <a href="#">
                                <img src="../img/sample/big_tapok1.jpg" alt=""/>
                                <span>7 500 грн</span>
                            </a>
                        </div>
                        <div class="slide">
                            <a href="#">
                                <img src="../img/sample/big_tapok1.jpg" alt=""/>
                                <span>7 500 грн</span>
                            </a>
                        </div>
                        <div class="slide">
                            <a href="#">
                                <img src="../img/sample/big_tapok1.jpg" alt=""/>
                                <span>7 500 грн</span>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
    </div>

<? include "popups/product.php" ?>

<? include "elements/footer.php" ?>