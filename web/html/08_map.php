<? include "elements/header.php" ?>
    <div class="main_content map-page">
            <h1>Нашi магазини в мiстах України — Днiпропетровськ</h1>

            <div class="map_block">
<!--                <div class="imgs">-->
<!--                    <a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>-->
<!--                    <a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>-->
<!--                    <a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>-->
<!--                </div>-->
                <div id="is_map"></div>
                <div class="info_layer">
                    <select class="city_chooser">
                        <option>Днiпропетровськ</option>
                        <option>Львiв</option>
                        <option>Одеса</option>
                    </select>


                    <div class="js-stores">
                        <div class="store_block active"
                             data-img1="../img/main_page_content/temp_big_img.png"
                             data-img2="../img/main_page_content/temp_big_img.png"
                             data-img3="../img/main_page_content/temp_big_img.png"
                            >
                            <div class="title">ТРК «МОСТ –Сіті центр»</div>
                            <div class="info" data-lat="55.760374" data-lng="37.62175">
                                вул. Глінки, 2<br/>
                                +38 (056) 790-29-25<br/>
                                з10:00 до 22:00<br/>
                            </div>
                        </div>

                        <div class="store_block"
                             data-img1="../img/main_page_content/find_pair.png"
                             data-img2="../img/main_page_content/ladies.png"
                             data-img3="../img/main_page_content/trend_img.png"
                            >
                            <div class="title">ТЦ «Passage»</div>
                            <div class="info" data-lat="54.760374" data-lng="36.62175">
                                вул. К. Маркса, 50<br/>
                                +3 (8056) 376-08-83<br/>
                                з 10:00 до 22:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">ТЦ Променада</div>
                            <div class="info" data-lat="53.760374" data-lng="35.62175">
                                вул. Овруцька, 18<br/>
                                +38 (067) 63-46-252<br/>
                                з 10:00 до 21:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">ТРЦ «Dream Town»</div>
                            <div class="info">
                                пр. Оболонский 1б<br/>
                                +38 (044) 426-86-72<br/>
                                з 10:00 до 22:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">ТРЦ Sky Mall</div>
                            <div class="info">
                                пр. Генерала Ватутина, 2-т<br/>
                                +38 (044) 371-19-44<br/>
                                з 10:00 до 22:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">Магазин Luciano Carvari</div>
                            <div class="info">
                                пр. Леніна, 149<br/>
                                +38 (061) 213-56-35<br/>
                                з 10:00 до 20:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">ТЦ Променада</div>
                            <div class="info">
                                вул. Овруцька, 18<br/>
                                +38 (067) 63-46-252<br/>
                                з 10:00 до 21:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">ТРЦ «Dream Town»</div>
                            <div class="info">
                                пр. Оболонский 1б<br/>
                                +38 (044) 426-86-72<br/>
                                з 10:00 до 22:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">ТРЦ Sky Mall</div>
                            <div class="info">
                                пр. Генерала Ватутина, 2-т<br/>
                                +38 (044) 371-19-44<br/>
                                з 10:00 до 22:00<br/>
                            </div>
                        </div>

                        <div class="store_block">
                            <div class="title">Магазин Luciano Carvari</div>
                            <div class="info">
                                пр. Леніна, 149<br/>
                                +38 (061) 213-56-35<br/>
                                з 10:00 до 20:00<br/>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
    </div>

<!--    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCHYfXB4pYh-IQ4U8A8p7gShMtkF8VXQmg&sensor=false&v=3.exp&signed_in=true&language=en"></script>-->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="../js/map.js"></script>


<? include "elements/footer.php" ?>