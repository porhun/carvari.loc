<? include "elements/header.php" ?>

    <div class="main_content trends">
            <h1>Тренди</h1>
            <div class="blogs_list">
                <a href="#" class="blog_link">
                    <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                    <div class="blog_title">Модне взуття весна-літо 2015</div>
                    <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                    <div class="blog_date">Новини, 29.07.2015</div>
                </a>
                <a href="#" class="blog_link">
                    <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                    <div class="blog_title">Модне взуття весна-літо 2015</div>
                    <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                    <div class="blog_date">Новини, 29.07.2015</div>
                </a>
                <a href="#" class="blog_link">
                    <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                    <div class="blog_title">Модне взуття весна-літо 2015</div>
                    <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                    <div class="blog_date">Новини, 29.07.2015</div>
                </a>
                <a href="#" class="blog_link">
                    <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                    <div class="blog_title">Модне взуття весна-літо 2015</div>
                    <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                    <div class="blog_date">Новини, 29.07.2015</div>
                </a>
                <a href="#" class="blog_link">
                    <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                    <div class="blog_title">Модне взуття весна-літо 2015</div>
                    <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                    <div class="blog_date">Новини, 29.07.2015</div>
                </a>
                <a href="#" class="blog_link">
                    <div class="img_holder" style="background: url(../img/main_page_content/trend_img.png)"></div>
                    <div class="blog_title">Модне взуття весна-літо 2015</div>
                    <div class="blog_text">Якщо ви раптом подумали, що вже пора прибирати зимові черевики, заявляємо — поспішати не варто.</div>
                    <div class="blog_date">Новини, 29.07.2015</div>
                </a>
            </div>
            <div class="pager">
                <ul class="pager_list">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li class="active"><span>3</span></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li><a href="#">7</a></li>
                    <li><a href="#">8</a></li>
                    <li><a href="#">9</a></li>
                    <li><a href="#">10</a></li>
                    <li><a href="#">11</a></li>
                    <li><a href="#">12</a></li>
                    <li class="dots">...</li>
                    <li><a href="#">24</a></li>
                </ul>
            </div>
    </div>
<? include "elements/footer.php" ?>
