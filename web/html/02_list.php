<? include "elements/header.php" ?>
<script>
    $(document).ready(function(){
        $('.container').addClass('big')
    })
</script>
    <div class="main_content list">
    <div class="filters">
        <div class="filters_wrapper">
            <div class="min_width">
                <div class="filter_list clearfix">
                    <div class="filters_exp">
                        <div class="title">Фільтр:</div>
                        <div class="filter_block">
                            <div class="empty_filter">Нічого не вибрано</div>
<!--                            <div class="filter_name">-->
<!--                                <span class="filt_item">35.5</span>-->
<!--                            </div>-->
<!--                            <div class="filter_name">-->
<!--                                <span class="filt_item">Босоніжки</span>-->
<!--                            </div>-->
<!--                            <div class="filter_name">-->
<!--                                <span class="filt_item">Шкіра</span>-->
<!--                            </div>-->
<!--                            <div class="filter_name">-->
<!--                                <span class="filt_item">Білий</span>-->
<!--                            </div>-->
                            <div class="filter_name price_filt">
                                <div class="filter_items">
                                    <span class="filt_item price_sort">Від дешевих до дорогих</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table_exp" style="display: none">
<!--                        <div class="filter_exp_title clearfix">-->
<!--                            <div class="filter_exp title ">Матеріал</div>-->
<!--                            <div class="filter_exp title ">Колір</div>-->
<!--                            <div class="filter_exp title ">Відобразити</div>-->
<!--                        </div>-->
                        <div class="filter_exp_cont">
                            <div class="someID">
                                <div class="filter_wrap">
                                    <div class="filter_exp title size">Розмір</div>
                                    <div class="filter_exp js_list_over size">
                                        <ul class="filter_options">
                                            <li><span class="item">35</span></li>
                                            <li><span class="item">35.5</span></li>
                                            <li><span class="item">36</span></li>
                                            <li><span class="item">36.5</span></li>
                                            <li><span class="item">37</span></li>
                                            <li><span class="item">37.5</span></li>
                                            <li><span class="item">38</span></li>
                                            <li><span class="item">39</span></li>
                                            <li><span class="item">39.5</span></li>
                                            <li><span class="item">40</span></li>
                                            <li><span class="item">40.5</span></li>
                                            <li><span class="item">41</span></li>
                                            <li><span class="item">42</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter_wrap">
                                    <div class="filter_exp title size">Вид</div>
                                    <div class="filter_exp js_list_over">
                                        <ul class="filter_options">
                                            <li><span class="item">Балетки</span></li>
                                            <li><span class="item">Босоніжки</span></li>
                                            <li><span class="item">Ботильони</span></li>
                                            <li><span class="item">Черевики</span></li>
                                            <li><span class="item">Кросівки</span></li>
                                            <li><span class="item active">Мокасини</span></li>
                                            <li><span class="item">Сабо</span></li>
                                            <li><span class="item">Взуття для дому</span></li>
                                            <li><span class="item">Чоботи</span></li>
                                            <li><span class="item">Туфлі</span></li>
                                            <li><span class="item">Сандалі</span></li>
                                            <li><span class="item">Балетки</span></li>
                                            <li><span class="item">Босоніжки</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter_wrap">
                                    <div class="filter_exp title size">Матеріал</div>
                                    <div class="filter_exp js_list_over">
                                        <ul class="filter_options">
                                            <li><span class="item">Шкіра</span></li>
                                            <li><span class="item">Замша</span></li>
                                            <li><span class="item">Велюр</span></li>
                                            <li><span class="item">Замша\шкіра</span></li>
                                            <li><span class="item">Замша\тканина</span></li>
                                            <li><span class="item">Штучна шкіра</span></li>
                                            <li><span class="item">Шкіра\лак</span></li>
                                            <li><span class="item">Замша</span></li>
                                            <li><span class="item">Велюр</span></li>
                                            <li><span class="item">Замша\шкіра</span></li>
                                            <li><span class="item">Замша\тканина</span></li>
                                            <li><span class="item">Штучна шкіра</span></li>
                                            <li><span class="item">Шкіра\лак</span></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="filter_wrap">
                                    <div class="filter_exp title size">Колір</div>
                                    <div class="filter_exp js_list_over">
                                    <ul class="filter_options">
                                        <li><span class="item">Бежевий</span></li>
                                        <li><span class="item">Блакитний</span></li>
                                        <li><span class="item">Червоний</span></li>
                                        <li><span class="item">Помаранчевий</span></li>
                                        <li><span class="item">Коричневий</span></li>
                                        <li><span class="item">Білий</span></li>
                                        <li><span class="item">Бірюзовий</span></li>
                                        <li><span class="item">Бронзовий</span></li>
                                        <li><span class="item">Чорний</span></li>
                                        <li><span class="item">Жовтий</span></li>
                                        <li><span class="item">Сірий</span></li>
                                        <li><span class="item">Зелений</span></li>
                                        <li><span class="item">Бежевий</span></li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                            <div class="filter_wrap">
                                <div class="filter_exp title size">Відобразити</div>
                                <div class="filter_exp">
                                <ul class="filter_options">
                                    <li><span class="item">Новинки</span></li>
                                    <li><span class="item active">Від дешевих до дорогих</span></li>
                                    <li><span class="item">Від дорогих до дешевих</span></li>
                                </ul>
                            </div>
                            </div>
                            <div class="btn_holder">
                                <a href="#" class="button small js_show_filter">Показати</a>
                                <a href="#" class="clean_filter">Очистити</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="expand_filter_btn">
                    <i class="icon expand_filter">
                        <svg pointer-events="all">
                            <use xlink:href="#dropdown_arrow">
                        </svg>
                    </i>
                </div>
            </div>
        </div>
    </div>
        <div class="top_content">
            <h1>Босоніжки</h1>
            <div class="bread_crumbs">
                <ul>
                    <li itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                        <a itemprop="url" href="#">Головна</a>
                    </li>
                    <li itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                        <a itemprop="url" href="#">Жінкам</a>
                    </li>
                    <li itemtype="http://data-vocabulary.org/Breadcrumb" itemscope="">
                        <span itemprop="title">Босоніжки</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="big_img">
            <div class="temp_img"></div>
        </div>
        <div class="item_list">
            <ul class="items">
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
                <li><a href="#" class="item">
                        <img src="../img/main_page_content/item_img.png" alt="">
                        <span class="name">Босоніжки</span>
                        <span class="item_price">
                            <span class="price">100 грн</span>
                            <span class="old_price">50 грн</span>
                        </span>
                    </a></li>
            </ul>
        </div>
        <div class="pager">
            <ul class="pager_list">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li class="active"><span>3</span></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">6</a></li>
                <li><a href="#">7</a></li>
                <li><a href="#">8</a></li>
                <li><a href="#">9</a></li>
                <li><a href="#">10</a></li>
                <li><a href="#">11</a></li>
                <li><a href="#">12</a></li>
                <li class="dots">...</li>
                <li><a href="#">24</a></li>
            </ul>
        </div>
    </div>
<? include "elements/footer.php" ?>
