<? include "elements/header.php" ?>

<div class="main_content checkout">
        <div class="left_content">
            <h1>Увійти в аккаунт</h1>
            <div class="tab_wrapper">
                <div id="tab1" class="tab_content">
                    <div class="input_block">
                        <label>Телефон</label>
                        <input type="tel" value="+380">
                    </div>
                    <div class="input_block">
                        <label>Пароль</label>
                        <input type="password">
                        <span class="error_msg">Какой то текст ошибки</span>
                    </div>
                    <div class="btn_holder login">
                        <input type="button" class="button disabled big" value="Увійти">
                        <a href="#">Нагадати пароль</a>
                    </div>
                </div>

            </div>
        </div>
</div>

<? include "elements/footer.php" ?>
