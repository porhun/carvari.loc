<? include "elements/header.php" ?>

<div class="main_content checkout">
        <div class="left_content">
            <h1>Реєстрація та офрмлення</h1>
            <div class="profile_nav">
                <span data-tab="tab1" class="profile_nav_link active">Постійний покупець
                    <span class="underline"></span>
                </span>
                <span data-tab="tab2" class="profile_nav_link">Купую вперше
                    <span class="underline"></span>
                </span>
            </div>
            <div class="tab_wrapper">
                <div id="tab1" class="tab_content">
                    <div class="input_block">
                        <label>Телефон</label>
                        <input type="tel" value="+380">
                    </div>
                    <div class="input_block">
                        <label>Пароль</label>
                        <input type="password">
                        <span class="error_msg">Какой то текст ошибки</span>
                    </div>
                    <div class="btn_holder login">
                        <input type="button" class="button big" value="Увійти">
                        <a href="#">Нагадати пароль</a>
                    </div>
                </div>
                <div id="tab2" class="tab_content" style="display: none;">
                    <div class="input_block">
                        <label>Ім'я</label>
                        <input type="text">
                    </div>
                    <div class="input_block wrong">
                        <label>Прізвище</label>
                        <input type="text">
                        <span class="error_msg">Какой то текст ошибки</span>
                    </div>
                    <div class="input_block">
                        <label>Телефон</label>
                        <input type="tel" value="+380">
                    </div>
                    <div class="profile_section_title">
                        Доставка
                    </div>
                    <div class="myself">
                        <div class="label_with_check">
                            <input type="radio" id="myself" name="myself" checked>
                            <label class="radio_label" for="myself">Самовивіз</label>
                        </div>
                        <div class="input_block">
                            <select>
                                <option>Київська</option>
                                <option>Дніпропетровська</option>
                                <option>Харківська</option>
                                <option>Житомирська</option>
                            </select>
                        </div>
                    </div>
                    <div class="post_delivery">
                        <div class="post_radio">
                            <input type="radio" id="post_delivery" name="myself">
                            <label class="radio_label" for="post_delivery">Нова пошта</label>
                        </div>
                        <div class="input_block">
                            <label>Місто</label>
                            <select>
                                <option>Київська область</option>
                                <option>Дніпропетровська область</option>
                                <option>Харківська область</option>
                                <option>Житомирська область</option>
                            </select>
                        </div>
                        <div class="input_block">
                            <label>Місто</label>
                            <select>
                                <option>Київ</option>
                                <option>Дніпропетровськ</option>
                                <option>Харків</option>
                                <option>Житомир</option>
                            </select>
                        </div>
                        <div class="input_block">
                            <label>Відділення</label>
                            <select>
                                <option>10</option>
                                <option>20</option>
                                <option>30</option>
                                <option>40</option>
                            </select>
                        </div>
                    </div>
                    <div class="profile_section_title">
                        Способ оплаты
                    </div>
                    <div class="payment">
                        <div class="label_with_check">
                            <input type="radio" id="cash" name="payment">
                            <label class="radio_label" for="cash">
                                Готівкою кур'єру
                                <div class="payment_description">Оплата готівкою при отриманні замовлення кур'єрові.</div>
                            </label>
                        </div>
                    </div>
                    <div class="payment">
                        <div class="label_with_check">
                            <input type="radio" id="liqpay" name="payment">
                            <label class="radio_label" for="liqpay">
                                Карткою LiqPay
                                <div class="payment_description">Оплата готівкою при отриманні замовлення кур'єрові.</div>
                            </label>
                        </div>
                    </div>
                    <div class="btn_holder">
                        <input type="button" class="button big" value="Підтвердити">
                    </div>
                </div>
            </div>
        </div>
        <div class="right_content">
            <h1>Замовлення</h1>
            <div class="error_note">
                Мокасини жiночi L15007 у вашому кошику
                не доступні! Видаліть і підтвердіть замовлення.
            </div>
            <div class="order_list">
                <ul class="order_items">
                    <li>
                        <div class="item errorr">
                            <div class="item_img">
                                <a href="#">
                                    <img src="../img/main_page_content/order_item.png" alt="">
                                </a>
                            </div>
                            <div class="item_descr">
                                <div class="title"><a href="#">Мокасини жЫночЫ</a></div>
                                <div class="code">JB007</div>
                                <div class="size">Розмір:45.5</div>
                                <div class="price">1 482 грн</div>
                            </div>
                            <div class="delete_item">
                                <i class="icon">
                                    <svg>
                                        <use xlink:href="#close_icon">
                                    </svg>
                                </i>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item">
                            <div class="item_img">
                                <a href="#">
                                    <img src="../img/main_page_content/order_item.png" alt="">
                                </a>
                            </div>
                            <div class="item_descr">
                                <div class="title"><a href="#">Мокасини жЫночЫ</a></div>
                                <div class="code">JB007</div>
                                <div class="size">Розмір:45.5</div>
                                <div class="price">1 482 грн</div>
                            </div>
                            <div class="delete_item">
                                <i class="icon">
                                    <svg>
                                        <use xlink:href="#close_icon">
                                    </svg>
                                </i>
                            </div>
                        </div>
                    </li>
                </ul>


<!--                hide this block when discount coupon was used-->
<!--                to show input error add class wrong to div with class discount-->
                <div class="discount wrong">
                    <div class="text">Використовуй купон на знижку</div>
                    <input placeholder="Одноразовий шестизначний код" class="js_discount_input" type="text">
                    <a href="#" class="discount_button">
                        <i class="icon">
                            <svg>
                                <use xlink:href="#subscribe_icon"></use>
                            </svg>
                        </i>
                    </a>
                    <div class="error_msg">Текст какой то ошибки</div>
                </div>
<!--                -->

<!--                show this block when coupon was used -->
                <div class="discount_used">
                    <div class="big_title">Купон на знижку використано!</div>
                    <div class="discount_percent">Знижка становить -15%</div>
                    <div class="cancel_discount">Отменить скидку</div>
                </div>
<!--                -->
                <div class="total">
                    <div class="price_text clearfix">
                        <span class="price_title">Всього</span>
                        <span class="price">7 680 грн</span>
                    </div>
                    <div class="price_text clearfix">
                        <span class="price_title">Всього</span>
                        <span class="price">7 680 грн</span>
                    </div>
                    <div class="price_text clearfix">
                        <span class="price_title">Всього</span>
                        <span class="price">--</span>
                    </div>
                    <div class="price_text to_pay clearfix">
                        <span class="price_title">До сплати</span>
                        <span class="price">7 600 грн</span>
                    </div>
                </div>
            </div>
        </div>
</div>

<? include "elements/footer.php" ?>
