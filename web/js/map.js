var map,marker,infowindow,contentString;
function load_map(lat, long) {
    var mapCanvas = document.getElementById('is_map');
    var mapOptions = {
        center: new google.maps.LatLng(lat, long),
        zoom: 17,
        disableDefaultUI: true,
        //scrollwheel: false,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "saturation": "2"
                    },
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels",
                "stylers": [
                    {
                        "saturation": "-28"
                    },
                    {
                        "lightness": "-10"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#363636"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "saturation": "-1"
                    },
                    {
                        "lightness": "-12"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "lightness": "-31"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "lightness": "-74"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "lightness": "65"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry",
                "stylers": [
                    {
                        "lightness": "-15"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": "0"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "saturation": "0"
                    },
                    {
                        "lightness": "-9"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "lightness": "-14"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels",
                "stylers": [
                    {
                        "lightness": "-35"
                    },
                    {
                        "gamma": "1"
                    },
                    {
                        "weight": "1.39"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "lightness": "-19"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "lightness": "46"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "lightness": "-13"
                    },
                    {
                        "weight": "1.23"
                    },
                    {
                        "invert_lightness": true
                    },
                    {
                        "visibility": "simplified"
                    },
                    {
                        "hue": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        //"color": "#c4c4c4"
                        "color": "#b0b0b0"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            }
        ],
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(mapCanvas, mapOptions);
    var myIcon = new google.maps.MarkerImage("../img/facebook_placeholder_marker.svg", null, null, null, new google.maps.Size(29, 40));

    marker = new google.maps.Marker({
        position: new google.maps.LatLng(48.4656225, 35.0114423),
        map: map,
        icon: myIcon,
        optimized: false,
        flat: true
    });

    contentString =

        '<div id="content">'+
        '<div class="imgs">'+
        '<a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>'+
        '<a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>'+
        '<a class="stores_preview" href="../img/main_page_content/temp_big_img.png"><img src="../img/main_page_content/temp_big_img.png" alt=""/></a>'+
        '</div>'+
        '</div>';

    infowindow = new google.maps.InfoWindow({
        content: contentString
    });

    infowindow.open(map, marker);

}

$(document).ready(function() {

    $('select').styler();
    //google.maps.event.addDomListener(window, 'load', load_map(48.4535484, 35.0433709));
    load_map(48.4656225, 35.0114423);

    $('.store_block').on('click',function(){
        infowindow.close();
        $('.store_block').removeClass('active');
        $(this).addClass('active');
        var lat = $(this).find('.info').attr('data-lat');
        var lng = $(this).find('.info').attr('data-lng');
        marker.setPosition( new google.maps.LatLng(lat,lng) );
        map.panTo( new google.maps.LatLng(lat,lng) );
        contentString =
            '<div id="content">'+
            '<div class="imgs">'+
            '<a class="stores_preview" href="'+$(this).attr('data-img1-full')+'"><img src="'+$(this).attr('data-img1')+'" alt=""/></a>'+
            '<a class="stores_preview" href="'+$(this).attr('data-img2-full')+'"><img src="'+$(this).attr('data-img2')+'" alt=""/></a>'+
            '<a class="stores_preview" href="'+$(this).attr('data-img3-full')+'"><img src="'+$(this).attr('data-img3')+'" alt=""/></a>'+
            '</div>'+
            '</div>';

        infowindow = new google.maps.InfoWindow({
            content: contentString
        });
        infowindow.open(map, marker);
    });

    $('#city-chooser').change(function(e) {
        panel_id = $(this).attr('value');
        $('.tab-pane').removeClass('active');
        $('#'+panel_id).addClass('active');
    });

});