$(document).ready(function(){

    //var mouse_in_head = 0;
    //$( ".hovered_logo" )
    //    .mouseenter(function() {
    //        if (!mouse_in_head) {
    //            mouse_in_head = 1;
    //            $('.hovered_logo').addClass('animate');
    //            setTimeout(function(){
    //                mouse_in_head = 0;
    //                $('.hovered_logo').removeClass('animate')
    //            },1900)
    //        }
    //    })
    //    .mouseleave(function() {
    //
    //    });

    //$("a.stores_preview").fancybox();

    $('a').click(function(){
       if( $(this).attr('href') == '#' ){
           return false;
       }
    });

    var r = /^[\w-_]+(?:\.[\w-_]+)*@(?:[\w-_]+\.)+[a-zA-Z]{2,7}$/;
    $('.email_input').on('keyup',function(){
        if ($(this).val() != "") {
            if (!r.test($('.email_input').val().trim())) {
                $('.sub_button').removeClass('visible');
            } else {
                $('.sub_button').addClass('visible')
            }
        }
    });

    $('.js_discount_input').on('keyup change',function(){
        if ($(this).val() != "") {
            $('.discount_button').css('display','block')
        } else {
            $('.discount_button').css('display','none')
        }
    });


    $('.expand_filter_btn, #ebala, .price_sort, .js_show_filter, .empty_filter').on('click',function(){
        if (menu.is_expand_menu) {
            menu.changeMenu('close');
        }
        $('.expand_filter_btn').toggleClass('close');
        $('.filters_exp').slideToggle('400');
        $('.table_exp').slideToggle('400');
    });
    $('.js_list_over').mCustomScrollbar({
        mouseWheel:{ preventDefault: true }
    });


    if ($('.main_content.profile').length || $('.main_content.checkout').length) {
        setTimeout(function() {
            $('select').styler({
                selectSmartPositioning: false
            });
        }, 100)
    }
    if ($('.main_content.checkout').length) {
        setTimeout(function() {
            $('input:radio').styler();
        }, 100);
    }

    $('.profile_nav_link').on('click',function(){
        if (!$(this).hasClass('active')) {
            $('.profile_nav_link.active').removeClass('active');
            $(this).addClass('active');
            $('.tab_content').fadeOut(200);
            var tab = $(this).attr('data-tab');
            setTimeout(function(){
                $('#'+tab).fadeIn(200);
            },200)
        }
    });

    $('.js_menu_item').on('click',function(){
        //clickMenu(this)
    });

    function clickMenu(e) {
        if (!$(e).hasClass('active')){

            if ($(e).hasClass('menu_burger')){
                $(e).toggleClass('opened');
            } else {
                $('.menu_burger').removeClass('opened')
            }

            $('.js_menu_item').removeClass('active');
            $(e).addClass('active');
            $('.js_menu_cont_inner').html($(e).parent('li').find('.js_menu_cont').html());
            if ($('.dropdown_menu').css('display')=='none') {
                $('.dropdown_menu').slideToggle('200');
                $('.main_content').animate({
                    'padding-top': 385
                },370);
            }
        } else {
            $(e).removeClass('active');
            $('.menu_burger').removeClass('opened');
            $('.dropdown_menu').slideToggle('200');
            $('.main_content').animate({
                'padding-top': 155
            },370);
        }
    }

    //var flag=0;
    //function scrollTopMenu() {
    //    var top = $(document).scrollTop();
    //    if (top > 1) {
    //        if (!$('.header').hasClass('fixed')) {
    //            $('.header').addClass('fixed');
    //        }
    //        if ($('.dropdown_menu').css('display')!='none' && !flag) {
    //                flag = 1;
    //            $('.main_content').animate({
    //                'padding-top': 155
    //            },370,function(){
    //                $('.js_menu_item').removeClass('active');
    //            });
    //            $('.dropdown_menu').slideToggle('200',function(){
    //                flag = 0;
    //            })
    //        }
    //        if(top >50) {
    //            $('.filters_wrapper').addClass('fixed');
    //            $('.header').addClass('imp')
    //        } else {
    //            $('.filters_wrapper').removeClass('fixed');
    //            $('.header').removeClass('imp');
    //        }
    //    } else {
    //        $('.header, .filters_wrapper').removeClass('fixed');
    //        $('.header').removeClass('imp');
    //    }
    //}




    if( $('.fbc').length == 1 ){
        (function(){
            var $parent = $('.fbc'),
                $bc = $parent.find('.fade_bread_crumbs'),
                is_animation_active = false;
                is_mouse_in = false;

            $('.fbc .follow_back').on('mouseenter', function(){
                is_mouse_in = true;
                //var $parrent = $(this).parent('.fbc'),
                if( !$parent.hasClass('active') ){
                    if(is_animation_active) return;
                    is_animation_active = true;
                    $parent.addClass('active')
                    $bc.fadeIn(400, function(){
                        is_animation_active = false;
                        if( !is_mouse_in ){
                            $parent.removeClass('active');
                            $bc.fadeOut(100);
                        }
                    });

                    //$bc.css({'display':'block'});
                    //setTimeout( function(){
                    //    $parent.addClass('active');
                    //}, 50 );
                }
            });

            $('.fbc').on('mouseleave', function(){
                is_mouse_in = false;
                if(is_animation_active) return;
                is_animation_active = true;
                $parent.removeClass('active');
                $bc.fadeOut(400, function(){
                    is_animation_active = false;
                    if( is_mouse_in ){
                        $parent.addClass('active');
                        $bc.fadeIn(100);
                    }
                });
                //$parent.removeClass('active');
                //setTimeout( function(){
                //    $bc.css({'display':'none'});
                //}, 50 );
            });

            //$('body').on('click', function(event){
            //    if(!$(event.target).closest('.fbc.active').length) {
            //        $parent.removeClass('active');
            //    }
            //});
        })();
    }

    function MenuControl(){
        this.menu = $('.dropdown_menu');
        this.main_container = $('.main_content');
        this.menu_list = $('.js_menu_cont_inner');
        this.clicked_menu = 0;
        this.is_expand_menu = 0;
        this.menu_content;

        this.menuStatus = function(){
            var myArray = new Array();
            menu.menu_content = menu.clicked_menu.parents('li').find('.js_menu_cont').html();

            if (menu.is_expand_menu && this.clicked_menu.hasClass('active')) {
                myArray['padding'] = 155;
                myArray['is_expand'] = 0;

                this.clicked_menu.removeClass('active');

                menu.menu_content = menu.clicked_menu.parents('li').find('.js_menu_cont').html();

                this.menu_list.html(menu.menu_content)
            } else {
                myArray['padding'] = 385;
                myArray['is_expand'] = 1;

                if (menu.is_expand_menu) {
                    menu.menu.slideUp('200');
                    //menu.main_container.animate({
                    //    'padding-top': 155
                    //},370);
                    setTimeout(function(){
                        menu.menu_list.html(menu.menu_content);
                    },300)
                } else {
                    this.menu_list.html(menu.menu_content);
                }

                $('.js_menu_item.active').removeClass('active');
                this.clicked_menu.addClass('active');
            }

            return myArray;
        };

        //as parameters 'close' or 'open'
        this.changeMenu = function(what_do){
            var data = this.menuStatus();
            if (what_do) {
                what_do == 'close' ? this.menu.slideUp('200') : this.menu.slideDown('200');
            } else {
                data.is_expand ? this.menu.slideDown('200') : this.menu.slideUp('200');
            }
            this.main_container.animate({
                'padding-top': data.padding
            },370);
            this.is_expand_menu = data.is_expand;
        }
    }

    menu = new MenuControl();

    $('.js_menu_item').on('click',function(){
        menu.clicked_menu = $(this);
        menu.changeMenu();
    });

    function scrollTopMenu() {
        var top = $(document).scrollTop();
        if (top > 1) {
            if (!$('.header').hasClass('fixed')) {
                $('.header').addClass('fixed');
            }
            if (menu.is_expand_menu) {
                menu.changeMenu('close')
            }
            if(top >50) {
                $('.filters_wrapper').addClass('fixed');
                $('.header').addClass('imp')
            } else {
                $('.filters_wrapper').removeClass('fixed');
                $('.header').removeClass('imp');
            }
        } else {
            $('.header, .filters_wrapper').removeClass('fixed');
            $('.header').removeClass('imp');
        }
    }

    $(window).scroll(function () {
        if ($('.main_content.list').length) {
            scrollTopMenu();
        }
    });


    if ($('.main_content.list').length) {
        scrollTopMenu();
        $('.js_list_over').mCustomScrollbar({
            mouseWheel:{ preventDefault: true }
        });
    }

    function Product_Popup(){
        var ex = this;
        this.index = 0;
        this.height = 0;
        this.popup = $('#product_popup');
        this.content = this.popup.find('.popup_content');

        //var wind = $(window).height(),
        //    need = 109 + 650;
        //this.height = (wind < need)?need:wind;
        this.height = 109 + 650;
        this.min_height = 630;
        this.load_count = 0;

        //(function(){
        //    var wind = $(window).height(),
        //        need = 109 + 700;
        //    console.log(wind);
        //    this.height = (wind < need)?need:wind;
        //    console.log(this.height);
        //})();

        this.open = function(content){

            $('html, body').animate({scrollTop: 0}, 200, function(){
                $('.body_wrapper').css({'height': ex.min_height, 'overflow': 'hidden'});
                $('.footer_wrapper').hide();
                ex.popup.fadeIn(300);
            });

            //ex.content.on('init', function(){
            //    console.log('was initialized');
            //});

            if(this.load_count == 0){
                setTimeout(function(){
                    var res = '',
                        initial_index = parseInt(ex.index);
                    $('.product_images .gallery-link').each(function(){
                        res += '<div><img src="'+$(this).attr('href')+'"/></div>';
                    });
                    ex.content.html(res);
                    ex.content.slick({
                        //infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        initialSlide: initial_index,
                        dots: true,
                    });

                    ex.calc_slider_position();
                }, 250);
                //ex.content.html(content);
            }
            else{
                ex.calc_slider_position();
                ex.content.slick('slickGoTo', ex.index);
            }

            ex.load_count++;

            //ex.content.slick('slickGoTo', ex.index);

            //$('html,.main_content').animate({scrollTop: 0}, 200, function(){
            //    $('.body_wrapper').css({'height': ex.height, 'overflow': 'hidden'});
            //    $('.footer_wrapper').hide();
            //    ex.popup.fadeIn(300);
            //});
            //ex.load_count++;
        };

        this.close = function(){
            $('.body_wrapper').css({'height': 'auto', 'overflow': 'visible'});
            $('.footer_wrapper').show();
            //$('.footer').show();
            this.popup.fadeOut(300);
            ex.reset_slider_position();
        };

        this.calc_slider_position = function(){
            if($(window).height() > ex.min_height){
                var top_marg = ($(window).height() - ex.min_height)/2;
                if(top_marg>50) top_marg = top_marg - 50;
                ex.popup.find('.slick-list').css({'margin-top':top_marg});
            }
            else{
                ex.popup.find('.slick-list').css({'margin-top':0});
            }
        }
        this.reset_slider_position = function(){
            setTimeout(function(){
                this.popup.find('.slick-list').css({'margin-top':0})
            }, 300);
        }

    }

    if (  $('.main_content').hasClass('product-page') ){
        product_popup = new Product_Popup();

        //Кнопка купить
        $('#add_to_cart .order_button').on('click', function(){
            if( ! $(this).hasClass('btn-outline') ){
                $(this).addClass('btn-outline');
                $(this).find('span').text($(this).find('span').attr('data-inbasket-translate'))
            }
        });

        $('.sizes_block li a').on('click', function(){
           $('.sizes_block li.active').removeClass('active');
           $(this).parent('li').addClass('active');
        });

        $('.product_image a').on('click', function(){
            var content = '<img src="'+$(this).attr("href")+'" alt=""/>'
            product_popup.open(content);
            return false;
        });

        $('.product_popup .popup_closer').on('click', function(){
           product_popup.close();
        });

        $('.product_images a').on('click', function(){
           if( $(this).hasClass('active') ) return false;

           //if(product_popup.load_count > 0){
               product_popup.index = $(this).attr('data-ind');
           //};

           $('.product_images a.active').removeClass('active');
           $(this).addClass('active');

           var $href = $(this).attr('href');
           $('.product_image img').fadeOut(400);

            $('.product_image img').load(function(){
                console.log('ebala')
                $(this).fadeIn(200)
            })
           setTimeout(function(){
               $('.product_image img').attr('src', $href)
           }, 400);
           return false;
        });


        if( $('.product_images .slide').length > 5 ){
            $('.product_images .slider').slick({
                slidesToShow: 5,
                arrows: false,
                slidesToScroll: 1,
                swipeToSlide: true,
                autoplay: true,
                autoplaySpeed: 2000,
                variableWidth: true
            });
        }

        if( $('.related_products .slide').length > 5 ) {
            $('.related_products .slider').slick({
                slidesToShow: 5,
                arrows: false,
                autoplay: true,
                swipeToSlide: true,
                autoplaySpeed: 2000,
                variableWidth: true
            });
        }
    }

    if (  $('.main_content').hasClass('blogpost-page') ){
        if( $('.is_post .slider').length > 0 ){
            $('.is_post .slider').each(function(){
                $(this).slick({
                    slidesToShow: 1,
                    arrows: false,
                    dots: true,
                    slidesToScroll: 1,
                    swipeToSlide: true,
                    //autoplay: true,
                    //autoplaySpeed: 2000
                });
            });
        }
    }

    if(  $('.main_content').hasClass('map-page')   ){
        setTimeout(function() {
            $('select').styler({
                selectSmartPositioning: false
            });
        }, 100)
        $('.info_layer').mCustomScrollbar()
    }

    var offset;
    var position;
    $('.js_dot').mousedown(function(e){
        offset = $('.js_dot').offset();
        position = 0;
        e.originalEvent.preventDefault();
        $( "body" ).mousemove(function( event ) {
            position = event;
            $('.js_dot').css({
                transform:'translateX('+(-offset.left + event.pageX - 5)+'px) translateY('+(-offset.top + event.pageY -5)+'px)',
                '-webkit-transform':'translateX('+(-offset.left + event.pageX - 5)+'px) translateY('+(-offset.top + event.pageY -5)+'px)'
            })
        });
    });
    var ceed_count = 1;
    $('.js_dot').mouseup(function(){
        $('body').unbind('mousemove');
        if(position){
            if ((-60 < (-offset.left + position.pageX - 5) && (-offset.left + position.pageX - 5) < -20) && (10 < (-offset.top + position.pageY -5) && (-offset.top + position.pageY -5) < 50)) {
                $(this).css({
                    transform:'translateX(-40px) translateY(28px)',
                    '-webkit-transform':'translateX(-40px) translateY(28px)'
                });
                ceed_count++;
                setTimeout(function(){
                    $('.bird .body').addClass('animate')
                },500);
                setTimeout(function(){
                    $('.bird .body').removeClass('animate');
                    $('.js_dot').animate({
                        'opacity':0
                    },100,function(){
                        setTimeout(function() {
                            $('.js_dot').removeAttr('style')
                        },500)
                    })
                },750)
                if (ceed_count==6) {
                    ceed_count = 1;
                    setTimeout(function(){
                        $('.bird').addClass('dead');
                    },800);
                    setTimeout(function(){
                        $('.bird').css('opacity',0);
                    },1500);
                    setTimeout(function(){
                        $('.bird').removeClass('dead');
                        setTimeout(function() {
                            $('.bird').removeAttr('style');
                        },300)
                    },2500)
                }
            } else {
                $('.js_dot').removeAttr('style')
            }
        }

    })

});